--------------------------------------------------------
--  DDL for Table CHUNKED_UPLOAD
--------------------------------------------------------

  CREATE TABLE "SPIDERS_DATA"."CHUNKED_UPLOAD" 
   (	"FILENAME" VARCHAR2(255 BYTE), 
	"BLOB_DATA" BLOB, 
	"OFFSET" NUMBER, 
	"CONTENT_TYPE" VARCHAR2(255 BYTE)
   ) SEGMENT CREATION DEFERRED 
  PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 
 NOCOMPRESS LOGGING
  TABLESPACE "SPIDERS_DATA_TABLES" 
 LOB ("BLOB_DATA") STORE AS BASICFILE (
  TABLESPACE "SPIDERS_DATA_TABLES" ENABLE STORAGE IN ROW CHUNK 8192 RETENTION 
  NOCACHE LOGGING ) ;
