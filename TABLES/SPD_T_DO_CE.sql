--------------------------------------------------------
--  DDL for Table SPD_T_DO_CE
--------------------------------------------------------

  CREATE TABLE "SPIDERS_DATA"."SPD_T_DO_CE" 
   (	"DOCEID" NUMBER(*,0), 
	"ADOID" NUMBER(*,0), 
	"OVERHEAD_RATE" NUMBER(*,0), 
	"PROFIT_RATE" NUMBER(*,0), 
	"FILESKEY" NUMBER(*,0)
   ) SEGMENT CREATION IMMEDIATE 
  PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 
 NOCOMPRESS LOGGING
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1
  BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "SPIDERS_DATA_TABLES" ;

   COMMENT ON TABLE "SPIDERS_DATA"."SPD_T_DO_CE"  IS '<Delivery Order Cost Estimate> Base table for the Delivery Order Cost Estimate. This table is filled in automatically from imported XML data for the Cost Estimate following negotiations.';
