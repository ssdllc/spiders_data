--------------------------------------------------------
--  DDL for Table SPD_T_JOB_TITLES
--------------------------------------------------------

  CREATE TABLE "SPIDERS_DATA"."SPD_T_JOB_TITLES" 
   (	"JOBTITLEID" NUMBER, 
	"CONTRACTCONTRACTORID" NUMBER, 
	"JOB_TITLE" VARCHAR2(255 BYTE), 
	"JOB_TITLE_DESCRIPTION" VARCHAR2(255 BYTE), 
	"VISUAL_ORDER" VARCHAR2(255 BYTE), 
	"BASE_RATE" VARCHAR2(255 BYTE), 
	"OPTION_1_RATE" NUMBER, 
	"OPTION_2_RATE" NUMBER, 
	"OPTION_3_RATE" NUMBER, 
	"OPTION_4_RATE" NUMBER, 
	"CREATED_BY" VARCHAR2(255 BYTE), 
	"CREATED_TIMESTAMP" DATE, 
	"UPDATED_BY" VARCHAR2(255 BYTE), 
	"UPDATED_TIMESTAMP" DATE
   ) SEGMENT CREATION IMMEDIATE 
  PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 
 NOCOMPRESS LOGGING
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1
  BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "SPIDERS_DATA_TABLES" ;

   COMMENT ON TABLE "SPIDERS_DATA"."SPD_T_JOB_TITLES"  IS '<SPD_T_JOB_TITLES> This is the table of job titles';
