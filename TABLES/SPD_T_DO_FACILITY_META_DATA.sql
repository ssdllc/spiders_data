--------------------------------------------------------
--  DDL for Table SPD_T_DO_FACILITY_META_DATA
--------------------------------------------------------

  CREATE TABLE "SPIDERS_DATA"."SPD_T_DO_FACILITY_META_DATA" 
   (	"AFMETAID" NUMBER(*,0), 
	"DOFID" NUMBER(*,0), 
	"DISPLAYVALUE" VARCHAR2(255 BYTE), 
	"VALUE" VARCHAR2(255 BYTE)
   ) SEGMENT CREATION IMMEDIATE 
  PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 
 NOCOMPRESS LOGGING
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1
  BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "SPIDERS_DATA_TABLES" ;

   COMMENT ON TABLE "SPIDERS_DATA"."SPD_T_DO_FACILITY_META_DATA"  IS '<Delivery Order Facility Meta Data> Table that holds name value pairs for information about a delivery order facility that we do not need to constantly search for. This gives each facility n number of attributes without sacrificing referential integrity or bloating the tables with unused fields.';
