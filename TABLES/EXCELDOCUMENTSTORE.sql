--------------------------------------------------------
--  DDL for Table EXCELDOCUMENTSTORE
--------------------------------------------------------

  CREATE GLOBAL TEMPORARY TABLE "SPIDERS_DATA"."EXCELDOCUMENTSTORE" 
   (	"OBJECT_ID" NUMBER(12,0), 
	"SEG_INDEX" NUMBER(12,0), 
	"SEGMENT" VARCHAR2(60 BYTE), 
	"SEG_LENGTH" NUMBER(12,0), 
	"VALUE" VARCHAR2(4000 BYTE)
   ) ON COMMIT DELETE ROWS ;
