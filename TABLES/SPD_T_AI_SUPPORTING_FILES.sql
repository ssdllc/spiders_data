--------------------------------------------------------
--  DDL for Table SPD_T_AI_SUPPORTING_FILES
--------------------------------------------------------

  CREATE TABLE "SPIDERS_DATA"."SPD_T_AI_SUPPORTING_FILES" 
   (	"AIPID" NUMBER(*,0), 
	"AIID" NUMBER(*,0), 
	"LOCAL_ID" VARCHAR2(255 BYTE), 
	"FILE_NAME" VARCHAR2(255 BYTE), 
	"CAPTION" VARCHAR2(4000 BYTE), 
	"FILE_SECTION_LISTID" NUMBER(*,0), 
	"FILE_TYPE_LISTID" NUMBER(*,0), 
	"FILESKEY" NUMBER(*,0), 
	"FILE_TYPE_STRING" VARCHAR2(255 BYTE), 
	"NAVFAC_DRAWING_NUMBER" VARCHAR2(255 BYTE), 
	"FOLDERID" NUMBER, 
	"THE_BFILE" BFILE, 
	"THUMBNAILID" NUMBER, 
	"DOFSFID" NUMBER
   ) SEGMENT CREATION IMMEDIATE 
  PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 
 NOCOMPRESS LOGGING
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1
  BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "SPIDERS_DATA_TABLES" ;

   COMMENT ON TABLE "SPIDERS_DATA"."SPD_T_AI_SUPPORTING_FILES"  IS '<Asset Inventory Supporting Files> Table to hold references and attributes for all files that support asset inventory. This table is automatically updated from imported XML data for the data requirements of the productline.';
