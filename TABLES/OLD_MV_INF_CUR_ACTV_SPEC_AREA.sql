--------------------------------------------------------
--  DDL for Table OLD_MV_INF_CUR_ACTV_SPEC_AREA
--------------------------------------------------------

  CREATE TABLE "SPIDERS_DATA"."OLD_MV_INF_CUR_ACTV_SPEC_AREA" 
   (	"UIC" VARCHAR2(24 BYTE), 
	"SPECIAL_AREA_CODE" VARCHAR2(2 CHAR), 
	"SPECIAL_AREA_NAME" VARCHAR2(88 BYTE), 
	"LATITUDE" CHAR(1 BYTE), 
	"LONGITUDE" CHAR(1 BYTE), 
	"CITY_CODE" VARCHAR2(16 BYTE), 
	"STATE_CODE" VARCHAR2(8 BYTE), 
	"COUNTRY_CODE" VARCHAR2(12 BYTE)
   ) SEGMENT CREATION IMMEDIATE 
  PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 
 NOCOMPRESS LOGGING
  STORAGE(INITIAL 131072 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1
  BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "SPIDERS_DATA_TABLES" ;
