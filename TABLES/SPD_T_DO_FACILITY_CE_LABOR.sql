--------------------------------------------------------
--  DDL for Table SPD_T_DO_FACILITY_CE_LABOR
--------------------------------------------------------

  CREATE TABLE "SPIDERS_DATA"."SPD_T_DO_FACILITY_CE_LABOR" 
   (	"AFCEID" NUMBER(*,0), 
	"DOFID" NUMBER(*,0), 
	"LABORCATEGORY_LISTID" NUMBER(*,0), 
	"JOBTITLES_LISTID" NUMBER(*,0), 
	"AMOUNT_GCE" NUMBER(*,0), 
	"AMOUNT_KTR" NUMBER(*,0), 
	"AMOUNT_PNP" NUMBER(*,0), 
	"AMOUNT_FINAL" NUMBER(*,0), 
	"UNITS_GCE" NUMBER(*,0), 
	"UNITS_KTR" NUMBER(*,0), 
	"UNITS_PNP" NUMBER(*,0), 
	"UNITS_FINAL" NUMBER(*,0), 
	"RATE_GCE" NUMBER(*,0), 
	"RATE_KTR" NUMBER(*,0), 
	"RATE_PNP" NUMBER(*,0), 
	"RATE_FINAL" NUMBER(*,0)
   ) SEGMENT CREATION IMMEDIATE 
  PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 
 NOCOMPRESS LOGGING
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1
  BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "SPIDERS_DATA_TABLES" ;

   COMMENT ON TABLE "SPIDERS_DATA"."SPD_T_DO_FACILITY_CE_LABOR"  IS '<Delivery Order Facility Cost Estimate Labor Costs> Table to hold the labor cost line items of a Cost Estimate for an individual facility. This table is filled in automatically from imported XML data for the Cost Estimate following negotiations.';
