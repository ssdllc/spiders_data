--------------------------------------------------------
--  DDL for Table SPD_T_TELE_CUSTOM_SHAPES
--------------------------------------------------------

  CREATE TABLE "SPIDERS_DATA"."SPD_T_TELE_CUSTOM_SHAPES" 
   (	"CUSTOMSHAPEID" NUMBER(*,0), 
	"SCENEMODELID" NUMBER(*,0), 
	"CODE" VARCHAR2(255 BYTE), 
	"DESCRIPTION" VARCHAR2(255 BYTE), 
	"LENGTH" NUMBER(*,2), 
	"WIDTH" NUMBER(*,2), 
	"HEIGHT" NUMBER(*,2), 
	"RADIUS" NUMBER(*,2), 
	"RED" NUMBER(*,0), 
	"GREEN" NUMBER(*,0), 
	"BLUE" NUMBER(*,0), 
	"TRANSPARENCY" NUMBER(*,0), 
	"RELATED_PLATFORM" VARCHAR2(255 BYTE), 
	"CREATED_BY" VARCHAR2(255 BYTE)
   ) SEGMENT CREATION IMMEDIATE 
  PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 
 NOCOMPRESS LOGGING
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1
  BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "SPIDERS_DATA_TABLES" ;

   COMMENT ON TABLE "SPIDERS_DATA"."SPD_T_TELE_CUSTOM_SHAPES"  IS 'CUSTOM SHAPES stores all cuboid shapes that are used to represent among other things, ground support equipment and any single object of any size.';
