--------------------------------------------------------
--  DDL for Table SPD_T_DO_FAC_SPEC_INST
--------------------------------------------------------

  CREATE TABLE "SPIDERS_DATA"."SPD_T_DO_FAC_SPEC_INST" 
   (	"AFSIID" NUMBER(*,0), 
	"DOFID" NUMBER(*,0), 
	"SPEC_INSTR_LISTID" NUMBER(*,0), 
	"SPEC_INSTR_VALUE" VARCHAR2(255 BYTE)
   ) SEGMENT CREATION IMMEDIATE 
  PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 
 NOCOMPRESS LOGGING
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1
  BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "SPIDERS_DATA_TABLES" ;

   COMMENT ON TABLE "SPIDERS_DATA"."SPD_T_DO_FAC_SPEC_INST"  IS '<Delivery Order Facility Special Instructions> Table that holds the information about the inspection instructions. The instruction types are in spd_t_lists, the values are in lists also. To add a new on, a displayname and value are required but also a new dropdown with set answers would be requested if it is something that requires it.';
