--------------------------------------------------------
--  DDL for Table SPD_T_FACILITIES
--------------------------------------------------------

  CREATE TABLE "SPIDERS_DATA"."SPD_T_FACILITIES" 
   (	"FACID" NUMBER(*,0), 
	"INFADS_FACILITY_ID" VARCHAR2(255 BYTE), 
	"PRODUCT_LINE_LISTID" NUMBER(*,0), 
	"SPD_FAC_NAME" VARCHAR2(255 BYTE), 
	"SPD_FAC_NO" VARCHAR2(255 BYTE), 
	"FILESKEY" NUMBER, 
	"SPD_UIC" VARCHAR2(255 BYTE)
   ) SEGMENT CREATION IMMEDIATE 
  PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 
 NOCOMPRESS LOGGING
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1
  BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "SPIDERS_DATA_TABLES" ;

   COMMENT ON TABLE "SPIDERS_DATA"."SPD_T_FACILITIES"  IS '<Facilities> Table to hold all facility records that have some information stored in SPIDERS. A facility will be added to this table when it is scoped. The majority of facilities will be direct pulls from iNFADS, but we have the ability to add non-iNFADS facilities if needed.';
