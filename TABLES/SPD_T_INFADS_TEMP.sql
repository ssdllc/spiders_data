--------------------------------------------------------
--  DDL for Table SPD_T_INFADS_TEMP
--------------------------------------------------------

  CREATE TABLE "SPIDERS_DATA"."SPD_T_INFADS_TEMP" 
   (	"INFADSTEMPID" NUMBER, 
	"FACILITY_ID" VARCHAR2(25 BYTE), 
	"INCORRECT_FACILITY_ID" VARCHAR2(25 BYTE), 
	"FACILITY_NAME" VARCHAR2(255 BYTE), 
	"FACILITY_NO" VARCHAR2(255 BYTE), 
	"PRIME_USE_CATEGORY_CODE" VARCHAR2(5 BYTE), 
	"MISSION_DEPENDENCY_INDEX" VARCHAR2(2 BYTE), 
	"ACTIVITY_UIC" VARCHAR2(6 BYTE), 
	"FORMER_ACTIVITY_UIC" VARCHAR2(6 BYTE), 
	"MAINTENANCE_RESP_UIC" VARCHAR2(6 BYTE), 
	"REPORTING_CLAIMANT_UIC" VARCHAR2(6 BYTE), 
	"SPECIAL_AREA_CODE" VARCHAR2(2 BYTE)
   ) SEGMENT CREATION IMMEDIATE 
  PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 
 NOCOMPRESS LOGGING
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1
  BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "SPIDERS_DATA_TABLES" ;
