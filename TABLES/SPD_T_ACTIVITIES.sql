--------------------------------------------------------
--  DDL for Table SPD_T_ACTIVITIES
--------------------------------------------------------

  CREATE TABLE "SPIDERS_DATA"."SPD_T_ACTIVITIES" 
   (	"ACTIVITIESID" NUMBER(*,0), 
	"SPD_ACTIVITY_NAME" VARCHAR2(255 BYTE), 
	"SPD_UIC" VARCHAR2(255 BYTE), 
	"CITY" VARCHAR2(255 BYTE), 
	"STATE" VARCHAR2(255 BYTE), 
	"POCKEY" VARCHAR2(255 BYTE), 
	"FILESKEY" NUMBER(*,0), 
	"COUNTRY" VARCHAR2(255 BYTE)
   ) SEGMENT CREATION IMMEDIATE 
  PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 
 NOCOMPRESS LOGGING
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1
  BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "SPIDERS_DATA_TABLES" ;

   COMMENT ON TABLE "SPIDERS_DATA"."SPD_T_ACTIVITIES"  IS '<Activities> Table to hold all activity (installation location/property owner) records that have some information stored in SPIDERS. An activity will be added to this table when a facility that is owned by that activity is scoped. The majority of activities will be direct pulls from iNFADS, but we have the ability to add non-iNFADS activities if needed.';
