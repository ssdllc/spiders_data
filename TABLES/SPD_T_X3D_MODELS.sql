--------------------------------------------------------
--  DDL for Table SPD_T_X3D_MODELS
--------------------------------------------------------

  CREATE TABLE "SPIDERS_DATA"."SPD_T_X3D_MODELS" 
   (	"X3DMODELID" NUMBER, 
	"CLASSNAMEID" NUMBER, 
	"OBJECT_DISPLAY_TEXT" VARCHAR2(255 BYTE), 
	"OBJECT_DESCRIPTION" VARCHAR2(255 BYTE), 
	"SOURCE_INFORMATION" VARCHAR2(255 BYTE), 
	"CONFIDENCE_INFO" VARCHAR2(255 BYTE), 
	"CONFIDENCE_RATING" NUMBER, 
	"MODEL_CORRECT_HEIGHT" NUMBER, 
	"MODEL_CORRECT_SCALE" NUMBER, 
	"MODEL_CORRECT_ORIENTATION" NUMBER, 
	"MODEL_UNDER_2MB" NUMBER, 
	"MODEL_CORRECT_CENTER" NUMBER, 
	"MODEL_INCLUDES_DATA_HOOKS" NUMBER, 
	"MODEL_INCLUDES_ANIMATIONS" NUMBER, 
	"MODEL_INCLUDES_IMAGES" NUMBER, 
	"CREATED_BY" VARCHAR2(255 BYTE), 
	"CREATED_TIMESTAMP" DATE, 
	"UPDATED_BY" VARCHAR2(255 BYTE), 
	"UPDATED_TIMESTAMP" DATE, 
	"X3D_THUMBNAIL" VARCHAR2(255 BYTE), 
	"XMLFILEID" NUMBER, 
	"X3D_FILENAME" VARCHAR2(1000 BYTE), 
	"MODEL_FILEID" NUMBER, 
	"IMAGES_FILEID" NUMBER, 
	"SOURCE_FILEID" NUMBER, 
	"THUMBNAIL_FILEID" NUMBER
   ) SEGMENT CREATION IMMEDIATE 
  PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 
 NOCOMPRESS LOGGING
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1
  BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "SPIDERS_DATA_TABLES" ;

   COMMENT ON TABLE "SPIDERS_DATA"."SPD_T_X3D_MODELS"  IS '<SPD_T_X3D_MODELS> This table is used to store all the x3d models';
