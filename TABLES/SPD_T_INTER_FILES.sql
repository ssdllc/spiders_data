--------------------------------------------------------
--  DDL for Table SPD_T_INTER_FILES
--------------------------------------------------------

  CREATE TABLE "SPIDERS_DATA"."SPD_T_INTER_FILES" 
   (	"FILESINTERID" NUMBER(*,0), 
	"FILESID" NUMBER(*,0), 
	"FILESKEY" NUMBER(*,0)
   ) SEGMENT CREATION IMMEDIATE 
  PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 
 NOCOMPRESS LOGGING
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1
  BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "SPIDERS_DATA_TABLES" ;

   COMMENT ON TABLE "SPIDERS_DATA"."SPD_T_INTER_FILES"  IS '<Intermediate Table for Files> Intermediate table that holds a reference to a spiders_docs.spd_t_files.filesie and a reference to (any table).fileskey Referential Integrity is handled at the (any table) level on delete triggers';
