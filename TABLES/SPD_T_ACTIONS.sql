--------------------------------------------------------
--  DDL for Table SPD_T_ACTIONS
--------------------------------------------------------

  CREATE TABLE "SPIDERS_DATA"."SPD_T_ACTIONS" 
   (	"ACTIONID" NUMBER, 
	"PRODUCTLINEID" NUMBER, 
	"FUNDSOURCE_LISTID" NUMBER, 
	"ACTIVITIESID" NUMBER, 
	"TITLE" VARCHAR2(256 CHAR), 
	"DESCRIPTION" VARCHAR2(512 CHAR), 
	"FY" NUMBER, 
	"QTR" NUMBER, 
	"INHOUSE_AMT" NUMBER, 
	"KTR_AMT" NUMBER, 
	"CREATED_BY" VARCHAR2(255 CHAR), 
	"CREATED_TIMESTAMP" DATE, 
	"UPDATED_BY" VARCHAR2(255 CHAR), 
	"UPDATED_TIMESTAMP" DATE
   ) SEGMENT CREATION IMMEDIATE 
  PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 
 NOCOMPRESS LOGGING
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1
  BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "SPIDERS_DATA_TABLES" ;

   COMMENT ON TABLE "SPIDERS_DATA"."SPD_T_ACTIONS"  IS '<SPD_T_ACTIONS> This table is used to store all of the actions that will eventually be related to delivery orders via Annual Execution.';
