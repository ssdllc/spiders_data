--------------------------------------------------------
--  DDL for Table SPD_T_CATEGORY_CODES
--------------------------------------------------------

  CREATE TABLE "SPIDERS_DATA"."SPD_T_CATEGORY_CODES" 
   (	"CATEGORYCODEID" NUMBER, 
	"CATEGORY_CODE" NUMBER, 
	"MAJOR_CAT_CODE" NUMBER, 
	"SHORT_DESC" VARCHAR2(255 BYTE), 
	"MEDIUM_DESC" VARCHAR2(255 BYTE), 
	"LONG_DESC" VARCHAR2(2000 BYTE), 
	"FAC_TYPE_CODE" NUMBER, 
	"PRI_UNIT_MEAS" VARCHAR2(255 BYTE), 
	"AREA_UNIT_MEAS" VARCHAR2(255 BYTE), 
	"ALT_UNIT_MEAS" VARCHAR2(255 BYTE), 
	"OTHER_UNIT_MEAS" VARCHAR2(255 BYTE), 
	"MAINT_COST_CODE" VARCHAR2(255 BYTE), 
	"USEFUL_LIFE_PERM" NUMBER, 
	"USEFUL_LIFE_SEMI" NUMBER, 
	"USEFUL_LIFE_TEMP" NUMBER, 
	"RQMTS_RPTG_IND" VARCHAR2(255 BYTE), 
	"P80_STATUS_CODE" NUMBER, 
	"DOD_FAC_CODE" NUMBER, 
	"RSIP_CODE" VARCHAR2(255 BYTE), 
	"SUST_COST_CODE" VARCHAR2(255 BYTE), 
	"RM_COST_CODE" VARCHAR2(255 BYTE), 
	"ALLOW_TYPE_CODE" VARCHAR2(255 BYTE), 
	"ALLOW_ALGORITHM" VARCHAR2(2000 BYTE), 
	"ALLOW_METHOD" VARCHAR2(2000 BYTE), 
	"LOADING_SOURCE" VARCHAR2(255 BYTE), 
	"TEMPLATE_URL" VARCHAR2(255 BYTE), 
	"SHORE_CAP_AREA" VARCHAR2(255 BYTE), 
	"SHORE_TASK" VARCHAR2(255 BYTE), 
	"CREATED_BY" VARCHAR2(255 BYTE), 
	"CREATED_TIMESTAMP" DATE, 
	"UPDATED_BY" VARCHAR2(255 BYTE), 
	"UPDATED_TIMESTAMP" DATE
   ) SEGMENT CREATION IMMEDIATE 
  PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 
 NOCOMPRESS LOGGING
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1
  BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "SPIDERS_DATA_TABLES" ;

   COMMENT ON TABLE "SPIDERS_DATA"."SPD_T_CATEGORY_CODES"  IS '<SPD_T_CATEGORY_CODES> ';
