--------------------------------------------------------
--  DDL for Table SPD_T_DRY_DOCK_CI_META
--------------------------------------------------------

  CREATE TABLE "SPIDERS_DATA"."SPD_T_DRY_DOCK_CI_META" 
   (	"DRYDOCKCIMETAID" NUMBER, 
	"DRYDOCKCIID" NUMBER, 
	"FIELD_NAME" VARCHAR2(255 BYTE), 
	"FIELD_VALUE" VARCHAR2(2000 BYTE)
   ) SEGMENT CREATION DEFERRED 
  PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 
 NOCOMPRESS LOGGING
  TABLESPACE "SPIDERS_DATA_TABLES" ;

   COMMENT ON TABLE "SPIDERS_DATA"."SPD_T_DRY_DOCK_CI_META"  IS '<SPD_T_DRY_DOCK_CI_meta> Table to hold all dry dock data found in condition excel files that are not part of the minimum required fields.';
