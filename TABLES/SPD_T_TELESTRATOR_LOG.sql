--------------------------------------------------------
--  DDL for Table SPD_T_TELESTRATOR_LOG
--------------------------------------------------------

  CREATE TABLE "SPIDERS_DATA"."SPD_T_TELESTRATOR_LOG" 
   (	"LOGID" NUMBER, 
	"LOCALID" VARCHAR2(255 BYTE), 
	"TIMESTAMP" DATE DEFAULT sysdate, 
	"ACTIONTYPE" VARCHAR2(20 BYTE), 
	"COLOR" VARCHAR2(255 BYTE), 
	"OPACITY" VARCHAR2(255 BYTE), 
	"TRANSLATION" VARCHAR2(255 BYTE), 
	"ROTATION" VARCHAR2(255 BYTE), 
	"CREATED_BY" VARCHAR2(255 BYTE), 
	"SESSIONID" NUMBER, 
	"NODE_TYPE" VARCHAR2(255 BYTE), 
	"X3D_NODE" CLOB, 
	"OBJECT_LABEL" VARCHAR2(255 BYTE), 
	"LOCAL_PARENT_ID" VARCHAR2(255 BYTE), 
	"PITCH" VARCHAR2(255 BYTE), 
	"ROLL" VARCHAR2(255 BYTE)
   ) SEGMENT CREATION IMMEDIATE 
  PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 
 NOCOMPRESS LOGGING
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1
  BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "SPIDERS_DATA_TABLES" 
 LOB ("X3D_NODE") STORE AS BASICFILE (
  TABLESPACE "SPIDERS_DATA_TABLES" ENABLE STORAGE IN ROW CHUNK 8192 PCTVERSION 10
  NOCACHE LOGGING 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1
  BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)) ;
