--------------------------------------------------------
--  DDL for Table SPD_T_DO_PROGRESS
--------------------------------------------------------

  CREATE TABLE "SPIDERS_DATA"."SPD_T_DO_PROGRESS" 
   (	"DOPROGID" NUMBER(*,0), 
	"ADOID" NUMBER(*,0), 
	"MILESTONE_LISTID" NUMBER(*,0), 
	"MILESTONE_DATE" DATE, 
	"MILESTONE_DUE_DATE" DATE
   ) SEGMENT CREATION IMMEDIATE 
  PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 
 NOCOMPRESS LOGGING
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1
  BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "SPIDERS_DATA_TABLES" ;

   COMMENT ON TABLE "SPIDERS_DATA"."SPD_T_DO_PROGRESS"  IS '<Delivery Order Progress> Table to hold the progress values for a delivery order. Progress items are added automatically as much as possible. Some progress items are related to deliverable documents, links to those documents are captured in this table.';
