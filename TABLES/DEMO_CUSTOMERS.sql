--------------------------------------------------------
--  DDL for Table DEMO_CUSTOMERS
--------------------------------------------------------

  CREATE TABLE "SPIDERS_DATA"."DEMO_CUSTOMERS" 
   (	"CUSTOMER_ID" NUMBER, 
	"CUST_FIRST_NAME" VARCHAR2(20 CHAR), 
	"CUST_LAST_NAME" VARCHAR2(20 CHAR), 
	"CUST_STREET_ADDRESS1" VARCHAR2(60 CHAR), 
	"CUST_STREET_ADDRESS2" VARCHAR2(60 CHAR), 
	"CUST_CITY" VARCHAR2(30 CHAR), 
	"CUST_STATE" VARCHAR2(2 CHAR), 
	"CUST_POSTAL_CODE" VARCHAR2(10 CHAR), 
	"CUST_EMAIL" VARCHAR2(30 CHAR), 
	"PHONE_NUMBER1" VARCHAR2(25 CHAR), 
	"PHONE_NUMBER2" VARCHAR2(25 CHAR), 
	"URL" VARCHAR2(100 CHAR), 
	"CREDIT_LIMIT" NUMBER(9,2), 
	"TAGS" VARCHAR2(4000 CHAR)
   ) SEGMENT CREATION IMMEDIATE 
  PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 
 NOCOMPRESS LOGGING
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1
  BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "SPIDERS_DATA_TABLES" ;
