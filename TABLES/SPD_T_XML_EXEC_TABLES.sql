--------------------------------------------------------
--  DDL for Table SPD_T_XML_EXEC_TABLES
--------------------------------------------------------

  CREATE TABLE "SPIDERS_DATA"."SPD_T_XML_EXEC_TABLES" 
   (	"EXECTABLEID" NUMBER, 
	"FACILITYID" NUMBER, 
	"FACNAME" VARCHAR2(4000 BYTE), 
	"FACID" NUMBER, 
	"NFAID" VARCHAR2(4000 BYTE), 
	"PRN" NUMBER, 
	"INSPECTYPE" VARCHAR2(4000 BYTE), 
	"INSPECTED" NUMBER, 
	"ENGRATING" VARCHAR2(4000 BYTE), 
	"CIRATING" NUMBER, 
	"FIVEYEARCI" NUMBER, 
	"TENYEARCI" NUMBER, 
	"OPRATING" VARCHAR2(4000 BYTE), 
	"RESTRICTIONS" VARCHAR2(4000 BYTE), 
	"LOADING" VARCHAR2(4000 BYTE), 
	"MOORING" VARCHAR2(4000 BYTE), 
	"BERTHING" VARCHAR2(4000 BYTE), 
	"REPAIRREC" VARCHAR2(4000 BYTE), 
	"USAGEDESC" VARCHAR2(4000 BYTE), 
	"CREATED_BY" VARCHAR2(255 BYTE), 
	"CREATED_TIMESTAMP" DATE, 
	"UPDATED_BY" VARCHAR2(255 BYTE), 
	"UPDATED_TIMESTAMP" DATE
   ) SEGMENT CREATION IMMEDIATE 
  PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 
 NOCOMPRESS LOGGING
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1
  BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "SPIDERS_DATA_TABLES" ;

   COMMENT ON TABLE "SPIDERS_DATA"."SPD_T_XML_EXEC_TABLES"  IS '<SPD_T_XML_EXEC_TABLES> This table is used to store all of the executive tables of facilities';
