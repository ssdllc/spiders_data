--------------------------------------------------------
--  DDL for Table SPD_T_3D_SESSIONS
--------------------------------------------------------

  CREATE TABLE "SPIDERS_DATA"."SPD_T_3D_SESSIONS" 
   (	"SESSIONID" NUMBER, 
	"SCENEID" NUMBER, 
	"DESCRIPTION" VARCHAR2(2000 BYTE), 
	"SHORT_DESCRIPTION" VARCHAR2(2000 BYTE), 
	"OWNER_EDIT_ONLY" VARCHAR2(2000 BYTE), 
	"LOCKED" VARCHAR2(2000 BYTE), 
	"LOCKED_DATE" VARCHAR2(2000 BYTE), 
	"SESSION_KEY" VARCHAR2(2000 BYTE), 
	"CREATED_BY" VARCHAR2(255 BYTE), 
	"CREATED_TIMESTAMP" DATE, 
	"UPDATED_BY" VARCHAR2(255 BYTE), 
	"UPDATED_TIMESTAMP" DATE
   ) SEGMENT CREATION IMMEDIATE 
  PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 
 NOCOMPRESS LOGGING
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1
  BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "SPIDERS_DATA_TABLES" ;

   COMMENT ON TABLE "SPIDERS_DATA"."SPD_T_3D_SESSIONS"  IS '<SPD_T_3D_SESSIONS> ';
