--------------------------------------------------------
--  DDL for Table SPD_T_DT_DOWNLOADS
--------------------------------------------------------

  CREATE TABLE "SPIDERS_DATA"."SPD_T_DT_DOWNLOADS" 
   (	"DTDOWNLOADID" NUMBER, 
	"DATATABLENAME" VARCHAR2(255 BYTE), 
	"AODATA" CLOB, 
	"CUSTOMFILTER" CLOB, 
	"THE_BFILE" BFILE, 
	"CREATED_BY" VARCHAR2(255 BYTE), 
	"CREATED_TIMESTAMP" DATE, 
	"UPDATED_BY" VARCHAR2(255 BYTE), 
	"UPDATED_TIMESTAMP" DATE
   ) SEGMENT CREATION IMMEDIATE 
  PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 
 NOCOMPRESS LOGGING
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1
  BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "SPIDERS_DATA_TABLES" 
 LOB ("AODATA") STORE AS BASICFILE (
  TABLESPACE "SPIDERS_DATA_TABLES" ENABLE STORAGE IN ROW CHUNK 8192 RETENTION 
  NOCACHE LOGGING 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1
  BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)) 
 LOB ("CUSTOMFILTER") STORE AS BASICFILE (
  TABLESPACE "SPIDERS_DATA_TABLES" ENABLE STORAGE IN ROW CHUNK 8192 RETENTION 
  NOCACHE LOGGING 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1
  BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)) ;

   COMMENT ON TABLE "SPIDERS_DATA"."SPD_T_DT_DOWNLOADS"  IS '<SPD_T_DT_DOWNLOADS> This table is used to store all datatable downloads';
