--------------------------------------------------------
--  DDL for Trigger SPD_I_JSON_TESTSUITE
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE TRIGGER "SPIDERS_DATA"."SPD_I_JSON_TESTSUITE" 
   before insert on "SPIDERS_DATA"."SPD_T_JSON_TESTSUITE"
   for each row
begin
   if inserting then
      if :NEW."JSONTESTID" is null then
         select spiders_data.spd_s_JSON_TESTSUITE.nextval into :NEW."JSONTESTID" from dual;
      end if;
   end if;
end;
/
ALTER TRIGGER "SPIDERS_DATA"."SPD_I_JSON_TESTSUITE" ENABLE;
