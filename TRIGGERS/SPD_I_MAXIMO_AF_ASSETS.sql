--------------------------------------------------------
--  DDL for Trigger SPD_I_MAXIMO_AF_ASSETS
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE TRIGGER "SPIDERS_DATA"."SPD_I_MAXIMO_AF_ASSETS" 
  BEFORE INSERT ON  spiders_data.SPD_T_MAXIMO_AF_ASSETS FOR EACH ROW

  BEGIN
    IF :new.MAXIMOAFASSETID IS NULL THEN
      select spiders_data.spd_s_MAXIMO_AF_ASSETS.nextval INTO :new.MAXIMOAFASSETID FROM dual;

    END IF;

      :new.CREATED_BY := nvl(v('APP_USER'),'NONE');
      :new.CREATED_TIMESTAMP := SYSTIMESTAMP;
  END;
/
ALTER TRIGGER "SPIDERS_DATA"."SPD_I_MAXIMO_AF_ASSETS" ENABLE;
