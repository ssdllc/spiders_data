--------------------------------------------------------
--  DDL for Trigger SPD_U_SIA_DATA
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE TRIGGER "SPIDERS_DATA"."SPD_U_SIA_DATA" 
  BEFORE UPDATE ON SPD_T_SIA_DATA FOR EACH ROW

  BEGIN
    :new.UPDATED_BY := nvl(v('APP_USER'),'NONE');
    :new.UPDATED_TIMESTAMP := SYSTIMESTAMP;
  END;
/
ALTER TRIGGER "SPIDERS_DATA"."SPD_U_SIA_DATA" ENABLE;
