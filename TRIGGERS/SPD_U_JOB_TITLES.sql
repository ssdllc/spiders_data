--------------------------------------------------------
--  DDL for Trigger SPD_U_JOB_TITLES
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE TRIGGER "SPIDERS_DATA"."SPD_U_JOB_TITLES" 
BEFORE UPDATE ON spiders_data.SPD_T_JOB_TITLES FOR EACH ROW

BEGIN
  :new.UPDATED_BY := nvl(v('APP_USER'),'NONE');
  :new.UPDATED_TIMESTAMP := SYSTIMESTAMP;
END;
/
ALTER TRIGGER "SPIDERS_DATA"."SPD_U_JOB_TITLES" ENABLE;
