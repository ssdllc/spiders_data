--------------------------------------------------------
--  DDL for Trigger SPD_I_TELESTRATOR_SRC_MODELS
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE TRIGGER "SPIDERS_DATA"."SPD_I_TELESTRATOR_SRC_MODELS" before
  INSERT ON spiders_data.spd_t_telestrator_src_models FOR EACH row
  BEGIN IF :new.sourcemodelid IS NULL THEN
    select spiders_data.spd_s_telestrator_src_models.nextval INTO :new.sourcemodelid FROM dual;
    END IF;

END;
/
ALTER TRIGGER "SPIDERS_DATA"."SPD_I_TELESTRATOR_SRC_MODELS" ENABLE;
