--------------------------------------------------------
--  DDL for Trigger SPD_I_ACTIVITIES
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE TRIGGER "SPIDERS_DATA"."SPD_I_ACTIVITIES" 
  BEFORE INSERT ON spiders_data.spd_t_activities
  FOR EACH ROW
begin
         if :new.activitiesid  is null
         then select spiders_data.spd_s_activities.nextval
            into :new.activitiesid
            from dual;
         end if;
         if :new.FILESKEY is null
         then select spiders_data.spd_s_fileskey.nextval
            into :new.FILESKEY
            from dual;
         end if;
         if :new.POCKEY is null
         then select spiders_data.spd_s_pockey.nextval
            into :new.POCKEY
            from dual;
         end if;
end;
/
ALTER TRIGGER "SPIDERS_DATA"."SPD_I_ACTIVITIES" ENABLE;
