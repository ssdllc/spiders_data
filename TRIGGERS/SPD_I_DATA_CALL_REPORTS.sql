--------------------------------------------------------
--  DDL for Trigger SPD_I_DATA_CALL_REPORTS
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE TRIGGER "SPIDERS_DATA"."SPD_I_DATA_CALL_REPORTS" BEFORE INSERT ON spiders_data.spd_t_data_call_reports
FOR EACH ROW
begin
         if :new.dcrid is null
         then select spiders_data.spd_s_data_call_reports.nextval
            into :new.dcrid
            from dual;
         end if;
         :new.timestamp := sysdate;
end;
/
ALTER TRIGGER "SPIDERS_DATA"."SPD_I_DATA_CALL_REPORTS" ENABLE;
