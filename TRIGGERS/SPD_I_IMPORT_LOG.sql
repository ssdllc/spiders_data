--------------------------------------------------------
--  DDL for Trigger SPD_I_IMPORT_LOG
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE TRIGGER "SPIDERS_DATA"."SPD_I_IMPORT_LOG" before
  INSERT ON "SPIDERS_DATA"."SPD_T_IMPORT_LOG"
  FOR EACH row
   BEGIN
    IF inserting THEN
     IF :NEW."IMPORTLOGID" IS NULL THEN
      select spiders_data.spd_s_IMPORT_LOG.nextval INTO :NEW."IMPORTLOGID" FROM dual;
   END IF;
     IF :NEW."TIMESTAMP" IS NULL THEN
      SELECT systimestamp INTO :NEW."TIMESTAMP" FROM dual;
   END IF;
  END IF;
END;
/
ALTER TRIGGER "SPIDERS_DATA"."SPD_I_IMPORT_LOG" ENABLE;
