--------------------------------------------------------
--  DDL for Trigger SPD_I_DO_CE_DC
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE TRIGGER "SPIDERS_DATA"."SPD_I_DO_CE_DC" BEFORE INSERT ON spiders_data.spd_t_do_ce_dc
FOR EACH ROW
begin
         if :new.docedcid  is null
         then select spiders_data.spd_s_do_ce_dc.nextval
            into :new.docedcid
            from dual;
         end if;
end;
/
ALTER TRIGGER "SPIDERS_DATA"."SPD_I_DO_CE_DC" ENABLE;
