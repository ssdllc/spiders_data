--------------------------------------------------------
--  DDL for Trigger SPD_I_ACTN_DOS
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE TRIGGER "SPIDERS_DATA"."SPD_I_ACTN_DOS" BEFORE INSERT ON spiders_data.spd_t_actn_dos
FOR EACH ROW
begin
    if :new.adoid  is null
    then select spiders_data.spd_s_actn_dos.nextval
      into :new.adoid
      from dual;
    end if;

    if :new.noteskey is null
      then select spiders_data.spd_s_noteskey.nextval into :new.noteskey from dual;
    end if;
end;
/
ALTER TRIGGER "SPIDERS_DATA"."SPD_I_ACTN_DOS" ENABLE;
