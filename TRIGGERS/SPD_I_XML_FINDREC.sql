--------------------------------------------------------
--  DDL for Trigger SPD_I_XML_FINDREC
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE TRIGGER "SPIDERS_DATA"."SPD_I_XML_FINDREC" 
   BEFORE INSERT ON SPD_T_XML_FINDREC FOR EACH ROW
   BEGIN
    IF :new.RECID IS NULL THEN
      SELECT SPD_S_XML_FINDREC.nextval INTO :new.RECID FROM dual;
    END IF;
  :new.CREATED_BY := nvl(v('APP_USER'),'NONE');
  :new.CREATED_TIMESTAMP := SYSTIMESTAMP;
  END;
/
ALTER TRIGGER "SPIDERS_DATA"."SPD_I_XML_FINDREC" ENABLE;
