--------------------------------------------------------
--  DDL for Trigger SPD_I_3D_MODEL_LOGS
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE TRIGGER "SPIDERS_DATA"."SPD_I_3D_MODEL_LOGS" 
   BEFORE INSERT ON SPD_T_3D_MODEL_LOGS FOR EACH ROW
   BEGIN
    IF :new.MODELLOGID IS NULL THEN
      SELECT SPD_S_3D_MODEL_LOGS.nextval INTO :new.MODELLOGID FROM dual;
    END IF;
  :new.CREATED_BY := nvl(v('APP_USER'),'NONE');
  :new.UPDATED_BY := nvl(v('APP_USER'),'NONE'); 
  :new.CREATED_TIMESTAMP := SYSTIMESTAMP;
  :new.UPDATED_TIMESTAMP := SYSTIMESTAMP; 
  END;
/
ALTER TRIGGER "SPIDERS_DATA"."SPD_I_3D_MODEL_LOGS" ENABLE;
