--------------------------------------------------------
--  DDL for Trigger SPD_I_POC
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE TRIGGER "SPIDERS_DATA"."SPD_I_POC" BEFORE INSERT ON spiders_data.spd_t_poc
FOR EACH ROW
begin
         if :new.pocid  is null
         then select spiders_data.spd_s_poc.nextval
            into :new.pocid
            from dual;
         end if;
end;
/
ALTER TRIGGER "SPIDERS_DATA"."SPD_I_POC" ENABLE;
