--------------------------------------------------------
--  DDL for Trigger SPD_I_XML_FILES
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE TRIGGER "SPIDERS_DATA"."SPD_I_XML_FILES" BEFORE INSERT ON SPIDERS_DATA.SPD_T_XML_FILES
FOR EACH ROW
begin
         if :new.XMLFILEID  is null
         then select SPIDERS_DATA.SPD_S_XML_FILES.nextval
            into :new.XMLFILEID
            from dual;
         end if;
end;
/
ALTER TRIGGER "SPIDERS_DATA"."SPD_I_XML_FILES" ENABLE;
