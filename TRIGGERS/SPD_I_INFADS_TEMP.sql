--------------------------------------------------------
--  DDL for Trigger SPD_I_INFADS_TEMP
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE TRIGGER "SPIDERS_DATA"."SPD_I_INFADS_TEMP" BEFORE INSERT ON SPIDERS_DATA.SPD_T_INFADS_TEMP
FOR EACH ROW
declare
lc_nNumber number :=0;
begin
         if :new.INFADSTEMPID  is null
         then
         select SPIDERS_DATA.SPD_S_INFADS_TEMP.nextval
            into lc_nNumber
            from dual;

         :new.INFADSTEMPID := lc_nNumber;
         if :new.INFADSTEMPID  is null  then
          :new.FACILITY_ID := 'SPDNFA4' || lc_nNumber;
         end if;
         end if;
end;
/
ALTER TRIGGER "SPIDERS_DATA"."SPD_I_INFADS_TEMP" ENABLE;
