--------------------------------------------------------
--  DDL for Trigger SPD_U_XML_ASSET_SUMMARY
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE TRIGGER "SPIDERS_DATA"."SPD_U_XML_ASSET_SUMMARY" 
  BEFORE UPDATE ON SPD_T_XML_ASSET_SUMMARY FOR EACH ROW

  BEGIN
    :new.UPDATED_BY := nvl(v('APP_USER'),'NONE');
    :new.UPDATED_TIMESTAMP := SYSTIMESTAMP;
  END;
/
ALTER TRIGGER "SPIDERS_DATA"."SPD_U_XML_ASSET_SUMMARY" ENABLE;
