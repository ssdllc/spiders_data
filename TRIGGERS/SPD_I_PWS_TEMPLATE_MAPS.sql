--------------------------------------------------------
--  DDL for Trigger SPD_I_PWS_TEMPLATE_MAPS
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE TRIGGER "SPIDERS_DATA"."SPD_I_PWS_TEMPLATE_MAPS" 
   BEFORE INSERT ON spiders_data.SPD_T_PWS_TEMPLATE_MAPS FOR EACH ROW
   BEGIN
    IF :new.PWSTEMPLATEMAPID IS NULL THEN
      select spiders_data.spd_s_PWS_TEMPLATE_MAPS.nextval INTO :new.PWSTEMPLATEMAPID FROM dual;
    END IF;
  :new.CREATED_BY := nvl(v('APP_USER'),'NONE');
  :new.CREATED_TIMESTAMP := SYSTIMESTAMP;
  END;
/
ALTER TRIGGER "SPIDERS_DATA"."SPD_I_PWS_TEMPLATE_MAPS" ENABLE;
