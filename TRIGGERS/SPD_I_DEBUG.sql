--------------------------------------------------------
--  DDL for Trigger SPD_I_DEBUG
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE TRIGGER "SPIDERS_DATA"."SPD_I_DEBUG" before insert on "SPIDERS_DATA"."DEBUG"    for each row begin     if inserting then       if :NEW."DEBUGID" is null then          select spiders_data.spd_s_DEBUG.nextval into :NEW."DEBUGID" from dual;       end if;    end if; end;
/
ALTER TRIGGER "SPIDERS_DATA"."SPD_I_DEBUG" ENABLE;
