--------------------------------------------------------
--  DDL for Trigger SPD_T_DO_FAC_MOORING_FITTINGS
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE TRIGGER "SPIDERS_DATA"."SPD_T_DO_FAC_MOORING_FITTINGS" BEFORE INSERT ON spiders_data.spd_t_do_fac_mooring_fittings
FOR EACH ROW
begin
         if :new.dofacmfid  is null
         then select spiders_data.spd_s_do_fac_mooring_fittings.nextval
            into :new.dofacmfid
            from dual;
         end if;
end;
/
ALTER TRIGGER "SPIDERS_DATA"."SPD_T_DO_FAC_MOORING_FITTINGS" ENABLE;
