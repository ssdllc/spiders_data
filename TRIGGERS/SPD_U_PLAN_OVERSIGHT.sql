--------------------------------------------------------
--  DDL for Trigger SPD_U_PLAN_OVERSIGHT
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE TRIGGER "SPIDERS_DATA"."SPD_U_PLAN_OVERSIGHT" 
  BEFORE UPDATE ON SPD_T_PLAN_OVERSIGHT FOR EACH ROW

  BEGIN
    :new.UPDATED_BY := nvl(v('APP_USER'),'NONE');
    :new.UPDATED_TIMESTAMP := SYSTIMESTAMP;
  END;
/
ALTER TRIGGER "SPIDERS_DATA"."SPD_U_PLAN_OVERSIGHT" ENABLE;
