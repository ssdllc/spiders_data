--------------------------------------------------------
--  DDL for Trigger SPD_I_XML_IMPORT_TYPES
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE TRIGGER "SPIDERS_DATA"."SPD_I_XML_IMPORT_TYPES" 
   BEFORE INSERT ON SPD_T_XML_IMPORT_TYPES FOR EACH ROW
   BEGIN
    IF :new.IMPORTID IS NULL THEN
      SELECT SPD_S_XML_IMPORT_TYPES.nextval INTO :new.IMPORTID FROM dual;
    END IF;
  --:new.CREATED_BY := nvl(v('APP_USER'),'NONE');
  :new.CREATED_TIMESTAMP := SYSTIMESTAMP;
  END;
/
ALTER TRIGGER "SPIDERS_DATA"."SPD_I_XML_IMPORT_TYPES" ENABLE;
