--------------------------------------------------------
--  DDL for Trigger SPD_I_QUERY_DEFS
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE TRIGGER "SPIDERS_DATA"."SPD_I_QUERY_DEFS" 
   BEFORE INSERT ON spiders_data.SPD_T_QUERY_DEFS FOR EACH ROW
   BEGIN
    IF :new.QUERYDEFINITIONID IS NULL THEN
      select spiders_data.spd_s_QUERY_DEFS.nextval INTO :new.QUERYDEFINITIONID FROM dual;
    END IF;
  :new.CREATED_BY := nvl(v('APP_USER'),'NONE');
  :new.CREATED_TIMESTAMP := SYSTIMESTAMP;
  END;
/
ALTER TRIGGER "SPIDERS_DATA"."SPD_I_QUERY_DEFS" ENABLE;
