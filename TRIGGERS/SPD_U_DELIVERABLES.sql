--------------------------------------------------------
--  DDL for Trigger SPD_U_DELIVERABLES
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE TRIGGER "SPIDERS_DATA"."SPD_U_DELIVERABLES" 
BEFORE UPDATE ON spiders_data.SPD_T_DELIVERABLES FOR EACH ROW

BEGIN
  :new.UPDATED_BY := nvl(v('APP_USER'),'NONE');
  :new.UPDATED_TIMESTAMP := SYSTIMESTAMP;
END;
/
ALTER TRIGGER "SPIDERS_DATA"."SPD_U_DELIVERABLES" ENABLE;
