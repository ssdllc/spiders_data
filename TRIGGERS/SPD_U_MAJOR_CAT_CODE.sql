--------------------------------------------------------
--  DDL for Trigger SPD_U_MAJOR_CAT_CODE
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE TRIGGER "SPIDERS_DATA"."SPD_U_MAJOR_CAT_CODE" 
  BEFORE UPDATE ON SPD_T_MAJOR_CAT_CODE FOR EACH ROW

  BEGIN
    :new.UPDATED_BY := nvl(v('APP_USER'),'NONE');
    :new.UPDATED_TIMESTAMP := SYSTIMESTAMP;
  END;
/
ALTER TRIGGER "SPIDERS_DATA"."SPD_U_MAJOR_CAT_CODE" ENABLE;
