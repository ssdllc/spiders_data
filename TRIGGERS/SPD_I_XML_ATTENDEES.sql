--------------------------------------------------------
--  DDL for Trigger SPD_I_XML_ATTENDEES
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE TRIGGER "SPIDERS_DATA"."SPD_I_XML_ATTENDEES" 
   BEFORE INSERT ON SPD_T_XML_ATTENDEES FOR EACH ROW
   BEGIN
    IF :new.ATTENDEEID IS NULL THEN
      SELECT SPD_S_XML_ATTENDEES.nextval INTO :new.ATTENDEEID FROM dual;
    END IF;
  --:new.CREATED_BY := nvl(v('APP_USER'),'NONE');
  :new.CREATED_TIMESTAMP := SYSTIMESTAMP;
  END;
/
ALTER TRIGGER "SPIDERS_DATA"."SPD_I_XML_ATTENDEES" ENABLE;
