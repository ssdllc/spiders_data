--------------------------------------------------------
--  DDL for Trigger SPD_I_DO_FAC_SPEC_INST
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE TRIGGER "SPIDERS_DATA"."SPD_I_DO_FAC_SPEC_INST" BEFORE INSERT ON spiders_data.spd_t_do_fac_spec_inst
FOR EACH ROW
begin
         if :new.afsiid  is null
         then select spiders_data.spd_s_do_fac_spec_inst.nextval
            into :new.afsiid
            from dual;
         end if;
end;
/
ALTER TRIGGER "SPIDERS_DATA"."SPD_I_DO_FAC_SPEC_INST" ENABLE;
