--------------------------------------------------------
--  DDL for Trigger SPD_U_BUOY_TOP_MEASUREMENTS
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE TRIGGER "SPIDERS_DATA"."SPD_U_BUOY_TOP_MEASUREMENTS" 
  BEFORE UPDATE ON spiders_data.SPD_T_BUOY_TOP_MEASUREMENTS FOR EACH ROW

  BEGIN
    :new.UPDATED_BY := nvl(v('APP_USER'),'NONE');
    :new.UPDATED_TIMESTAMP := SYSTIMESTAMP;
  END;
/
ALTER TRIGGER "SPIDERS_DATA"."SPD_U_BUOY_TOP_MEASUREMENTS" ENABLE;
