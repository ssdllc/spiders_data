--------------------------------------------------------
--  DDL for Trigger SPD_I_TELESTRATOR_LOG
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE TRIGGER "SPIDERS_DATA"."SPD_I_TELESTRATOR_LOG" before
  INSERT ON "SPIDERS_DATA"."SPD_T_TELESTRATOR_LOG" FOR EACH row BEGIN IF inserting THEN IF :NEW."LOGID" IS NULL THEN
  select spiders_data.spd_s_TELESTRATOR_LOG.nextval INTO :NEW."LOGID" FROM dual;
END IF;
END IF;
END;
/
ALTER TRIGGER "SPIDERS_DATA"."SPD_I_TELESTRATOR_LOG" ENABLE;
