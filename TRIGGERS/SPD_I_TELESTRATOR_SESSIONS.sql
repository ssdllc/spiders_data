--------------------------------------------------------
--  DDL for Trigger SPD_I_TELESTRATOR_SESSIONS
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE TRIGGER "SPIDERS_DATA"."SPD_I_TELESTRATOR_SESSIONS" before
  INSERT ON spiders_data.spd_t_telestrator_sessions FOR EACH row
  BEGIN IF :new.sessionid IS NULL THEN
    select spiders_data.spd_s_telestrator_sessions.nextval INTO :new.sessionid FROM dual;

    --select dbms_random.string('X', 3) || '-' || dbms_random.string('X', 3) || '-' || dbms_random.string('X', 3) into :new.SESSIONKEY from dual;
    END IF;

END;
/
ALTER TRIGGER "SPIDERS_DATA"."SPD_I_TELESTRATOR_SESSIONS" ENABLE;
