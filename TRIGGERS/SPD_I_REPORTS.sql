--------------------------------------------------------
--  DDL for Trigger SPD_I_REPORTS
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE TRIGGER "SPIDERS_DATA"."SPD_I_REPORTS" before
  INSERT ON spiders_data.spd_t_reports FOR EACH row BEGIN IF :new.reportid IS NULL THEN
  select spiders_data.spd_s_reports.nextval INTO :new.reportid FROM dual;
END IF;
IF inStr(:new.REPORTNUMBER,'-#-')>0 THEN
  SELECT REPLACE(:new.REPORTNUMBER , '-#-' , '-'
    ||:new.reportid
    ||'-')
  INTO :new.REPORTNUMBER
  FROM dual;
END IF;
IF :new.CREATEDDATE IS NULL THEN
  SELECT SYSDATE INTO :new.CREATEDDATE FROM dual;
END IF;
IF :new.NOTESKEY IS NULL THEN
 select spiders_data.spd_s_noteskey.nextval into :new.NOTESKEY from dual;
END IF;
END;
/
ALTER TRIGGER "SPIDERS_DATA"."SPD_I_REPORTS" ENABLE;
