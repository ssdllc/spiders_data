--------------------------------------------------------
--  DDL for Trigger SPD_I_DO_FACILITIES
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE TRIGGER "SPIDERS_DATA"."SPD_I_DO_FACILITIES" BEFORE INSERT ON spiders_data.spd_t_do_facilities
FOR EACH ROW
begin
         if :new.dofid  is null then 
            
            insert into debug (message) values ('no dofid on insert, making a new one');
            
            select spiders_data.spd_s_do_facilities.nextval
            into :new.dofid
            from dual;
         end if;
         if :new.FILESKEY is null
         then select spiders_data.spd_s_fileskey.nextval
            into :new.FILESKEY
            from dual;
         end if;
end;
/
ALTER TRIGGER "SPIDERS_DATA"."SPD_I_DO_FACILITIES" ENABLE;
