--------------------------------------------------------
--  DDL for Trigger SPD_D_DELIVERY_ORDERS
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE TRIGGER "SPIDERS_DATA"."SPD_D_DELIVERY_ORDERS" 
BEFORE DELETE ON SPD_T_DELIVERY_ORDERS
BEGIN
  delete from spd_t_xml_actions where adoid = adoid;
END;
/
ALTER TRIGGER "SPIDERS_DATA"."SPD_D_DELIVERY_ORDERS" ENABLE;
