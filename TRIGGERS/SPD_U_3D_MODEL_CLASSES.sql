--------------------------------------------------------
--  DDL for Trigger SPD_U_3D_MODEL_CLASSES
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE TRIGGER "SPIDERS_DATA"."SPD_U_3D_MODEL_CLASSES" 
  BEFORE UPDATE ON SPD_T_3D_MODEL_CLASSES FOR EACH ROW

  BEGIN
    :new.UPDATED_BY := nvl(v('APP_USER'),'NONE');
    :new.UPDATED_TIMESTAMP := SYSTIMESTAMP;
  END;
/
ALTER TRIGGER "SPIDERS_DATA"."SPD_U_3D_MODEL_CLASSES" ENABLE;
