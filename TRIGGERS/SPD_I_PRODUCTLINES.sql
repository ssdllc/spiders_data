--------------------------------------------------------
--  DDL for Trigger SPD_I_PRODUCTLINES
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE TRIGGER "SPIDERS_DATA"."SPD_I_PRODUCTLINES" 
   BEFORE INSERT ON spiders_data.SPD_T_PRODUCTLINES FOR EACH ROW
   BEGIN
    IF :new.PRODUCTLINEID IS NULL THEN
      select spiders_data.spd_s_PRODUCTLINES.nextval INTO :new.PRODUCTLINEID FROM dual;
    END IF;
  :new.CREATED_BY := nvl(v('APP_USER'),'NONE');
  :new.CREATED_TIMESTAMP := SYSTIMESTAMP;
  END;
/
ALTER TRIGGER "SPIDERS_DATA"."SPD_I_PRODUCTLINES" ENABLE;
