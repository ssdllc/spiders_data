--------------------------------------------------------
--  DDL for Trigger SPD_I_PWSS
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE TRIGGER "SPIDERS_DATA"."SPD_I_PWSS" 
  BEFORE INSERT ON  spiders_data.SPD_T_PWSS FOR EACH ROW

  BEGIN
    IF :new.PWSID IS NULL THEN
      select spiders_data.spd_s_PWSS.nextval INTO :new.PWSID FROM dual;

    END IF;

      :new.CREATED_BY := nvl(v('APP_USER'),'NONE');
      :new.CREATED_TIMESTAMP := SYSTIMESTAMP;
  END;
/
ALTER TRIGGER "SPIDERS_DATA"."SPD_I_PWSS" ENABLE;
