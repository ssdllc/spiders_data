--------------------------------------------------------
--  DDL for Trigger SPD_I_DRY_DOCK_CI
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE TRIGGER "SPIDERS_DATA"."SPD_I_DRY_DOCK_CI" 
BEFORE INSERT ON spiders_data.SPD_T_DRY_DOCK_CI FOR EACH ROW

BEGIN IF :new.DRYDOCKCIID IS NULL THEN
  SELECT spiders_data.spd_s_dry_dock_ci.nextval INTO :new.DRYDOCKCIID FROM dual;
END IF;

END;
/
ALTER TRIGGER "SPIDERS_DATA"."SPD_I_DRY_DOCK_CI" ENABLE;
