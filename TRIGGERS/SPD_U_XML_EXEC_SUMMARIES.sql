--------------------------------------------------------
--  DDL for Trigger SPD_U_XML_EXEC_SUMMARIES
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE TRIGGER "SPIDERS_DATA"."SPD_U_XML_EXEC_SUMMARIES" 
  BEFORE UPDATE ON SPD_T_XML_EXEC_SUMMARIES FOR EACH ROW

  BEGIN
    :new.UPDATED_BY := nvl(v('APP_USER'),'NONE');
    :new.UPDATED_TIMESTAMP := SYSTIMESTAMP;
  END;
/
ALTER TRIGGER "SPIDERS_DATA"."SPD_U_XML_EXEC_SUMMARIES" ENABLE;
