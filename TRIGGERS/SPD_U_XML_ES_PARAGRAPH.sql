--------------------------------------------------------
--  DDL for Trigger SPD_U_XML_ES_PARAGRAPH
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE TRIGGER "SPIDERS_DATA"."SPD_U_XML_ES_PARAGRAPH" 
  BEFORE UPDATE ON SPD_T_XML_ES_PARAGRAPH FOR EACH ROW

  BEGIN
    :new.UPDATED_BY := nvl(v('APP_USER'),'NONE');
    :new.UPDATED_TIMESTAMP := SYSTIMESTAMP;
  END;
/
ALTER TRIGGER "SPIDERS_DATA"."SPD_U_XML_ES_PARAGRAPH" ENABLE;
