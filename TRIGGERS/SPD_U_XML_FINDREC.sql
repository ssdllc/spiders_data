--------------------------------------------------------
--  DDL for Trigger SPD_U_XML_FINDREC
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE TRIGGER "SPIDERS_DATA"."SPD_U_XML_FINDREC" 
  BEFORE UPDATE ON SPD_T_XML_FINDREC FOR EACH ROW

  BEGIN
    :new.UPDATED_BY := nvl(v('APP_USER'),'NONE');
    :new.UPDATED_TIMESTAMP := SYSTIMESTAMP;
  END;
/
ALTER TRIGGER "SPIDERS_DATA"."SPD_U_XML_FINDREC" ENABLE;
