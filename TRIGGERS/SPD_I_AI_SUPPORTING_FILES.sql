--------------------------------------------------------
--  DDL for Trigger SPD_I_AI_SUPPORTING_FILES
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE TRIGGER "SPIDERS_DATA"."SPD_I_AI_SUPPORTING_FILES" BEFORE INSERT ON spiders_data.spd_t_ai_supporting_files
FOR EACH ROW
begin
         if :new.aipid  is null
         then select spiders_data.spd_s_ai_supporting_files.nextval
            into :new.aipid
            from dual;
         end if;
         if :new.FILESKEY is null
         then select spiders_data.spd_s_fileskey.nextval
            into :new.FILESKEY
            from dual;
         end if;
end;
/
ALTER TRIGGER "SPIDERS_DATA"."SPD_I_AI_SUPPORTING_FILES" ENABLE;
