--------------------------------------------------------
--  DDL for Trigger SPD_D_PWSS
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE TRIGGER "SPIDERS_DATA"."SPD_D_PWSS" 
   BEFORE DELETE ON spiders_data.SPD_T_PWSS  FOR EACH ROW
   BEGIN
    DELETE FROM spiders_data.SPD_T_PWS_TEMPLATE_MAPS WHERE PWSID = :old.PWSID;
  END;
/
ALTER TRIGGER "SPIDERS_DATA"."SPD_D_PWSS" ENABLE;
