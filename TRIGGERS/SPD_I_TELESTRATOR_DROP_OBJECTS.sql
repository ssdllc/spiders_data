--------------------------------------------------------
--  DDL for Trigger SPD_I_TELESTRATOR_DROP_OBJECTS
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE TRIGGER "SPIDERS_DATA"."SPD_I_TELESTRATOR_DROP_OBJECTS" before
  INSERT ON spiders_data.spd_t_telestrator_drop_objects FOR EACH row
  BEGIN IF :new.objectid IS NULL THEN
    select spiders_data.spd_s_telestrator_drop_objects.nextval INTO :new.objectid FROM dual;
    END IF;

END;
/
ALTER TRIGGER "SPIDERS_DATA"."SPD_I_TELESTRATOR_DROP_OBJECTS" ENABLE;
