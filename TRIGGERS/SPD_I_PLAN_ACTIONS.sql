--------------------------------------------------------
--  DDL for Trigger SPD_I_PLAN_ACTIONS
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE TRIGGER "SPIDERS_DATA"."SPD_I_PLAN_ACTIONS" 
   BEFORE INSERT ON SPD_T_PLAN_ACTIONS FOR EACH ROW 
   BEGIN 
    IF :new.PLANACTIONID IS NULL THEN 
      SELECT SPD_S_PLAN_ACTIONS.nextval INTO :new.PLANACTIONID FROM dual; 
    END IF; 
  :new.CREATED_BY := nvl(v('APP_USER'),'NONE'); 
  :new.CREATED_TIMESTAMP := SYSTIMESTAMP; 
  END;
/
ALTER TRIGGER "SPIDERS_DATA"."SPD_I_PLAN_ACTIONS" ENABLE;
