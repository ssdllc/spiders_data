--------------------------------------------------------
--  DDL for Trigger SPD_I_XML_AI_METADATA
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE TRIGGER "SPIDERS_DATA"."SPD_I_XML_AI_METADATA" 
   BEFORE INSERT ON SPD_T_XML_AI_METADATA FOR EACH ROW
   BEGIN
    IF :new.METADATAID IS NULL THEN
      SELECT SPD_S_XML_AI_METADATA.nextval INTO :new.METADATAID FROM dual;
    END IF;
  --:new.CREATED_BY := nvl(v('APP_USER'),'NONE');
  :new.CREATED_TIMESTAMP := SYSTIMESTAMP;
  END;
/
ALTER TRIGGER "SPIDERS_DATA"."SPD_I_XML_AI_METADATA" ENABLE;
