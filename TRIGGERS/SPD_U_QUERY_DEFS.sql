--------------------------------------------------------
--  DDL for Trigger SPD_U_QUERY_DEFS
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE TRIGGER "SPIDERS_DATA"."SPD_U_QUERY_DEFS" 
  BEFORE UPDATE ON spiders_data.SPD_T_QUERY_DEFS FOR EACH ROW

  BEGIN
    :new.UPDATED_BY := nvl(v('APP_USER'),'NONE');
    :new.UPDATED_TIMESTAMP := SYSTIMESTAMP;
  END;
/
ALTER TRIGGER "SPIDERS_DATA"."SPD_U_QUERY_DEFS" ENABLE;
