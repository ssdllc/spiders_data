--------------------------------------------------------
--  DDL for Trigger SPD_U_UNIT_TEST_RESULTS
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE TRIGGER "SPIDERS_DATA"."SPD_U_UNIT_TEST_RESULTS" 
  BEFORE UPDATE ON SPD_T_UNIT_TEST_RESULTS FOR EACH ROW

  BEGIN
    :new.UPDATED_BY := nvl(v('APP_USER'),'NONE');
    :new.UPDATED_TIMESTAMP := SYSTIMESTAMP;
  END;
/
ALTER TRIGGER "SPIDERS_DATA"."SPD_U_UNIT_TEST_RESULTS" ENABLE;
