--------------------------------------------------------
--  DDL for Trigger SPD_I_SIA_DATA
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE TRIGGER "SPIDERS_DATA"."SPD_I_SIA_DATA" 
   BEFORE INSERT ON SPD_T_SIA_DATA FOR EACH ROW
   BEGIN
    IF :new.SIAID IS NULL THEN
      SELECT SPD_S_SIA_DATA.nextval INTO :new.SIAID FROM dual;
    END IF;
  :new.CREATED_BY := nvl(v('APP_USER'),'NONE');
  :new.CREATED_TIMESTAMP := SYSTIMESTAMP;
  END;
/
ALTER TRIGGER "SPIDERS_DATA"."SPD_I_SIA_DATA" ENABLE;
