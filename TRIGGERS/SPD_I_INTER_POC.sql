--------------------------------------------------------
--  DDL for Trigger SPD_I_INTER_POC
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE TRIGGER "SPIDERS_DATA"."SPD_I_INTER_POC" BEFORE INSERT ON spiders_data.spd_t_inter_poc
FOR EACH ROW
begin
         if :new.pocinterid  is null
         then select spiders_data.spd_s_inter_poc.nextval
            into :new.pocinterid
            from dual;
         end if;
end;
/
ALTER TRIGGER "SPIDERS_DATA"."SPD_I_INTER_POC" ENABLE;
