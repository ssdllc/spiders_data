--------------------------------------------------------
--  DDL for Trigger SPD_I_DO_PROGRESS
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE TRIGGER "SPIDERS_DATA"."SPD_I_DO_PROGRESS" BEFORE INSERT ON spiders_data.spd_t_do_progress
FOR EACH ROW
begin
         if :new.doprogid  is null
         then select spiders_data.spd_s_do_progress.nextval
            into :new.doprogid
            from dual;
         end if;
end;
/
ALTER TRIGGER "SPIDERS_DATA"."SPD_I_DO_PROGRESS" ENABLE;
