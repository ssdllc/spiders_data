--------------------------------------------------------
--  DDL for Trigger SPD_I_DO_FACILITY_META_DATA
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE TRIGGER "SPIDERS_DATA"."SPD_I_DO_FACILITY_META_DATA" BEFORE INSERT ON spiders_data.spd_t_do_facility_meta_data
FOR EACH ROW
begin
         if :new.afmetaid  is null
         then select spiders_data.spd_s_do_facility_meta_data.nextval
            into :new.afmetaid
            from dual;
         end if;
end;
/
ALTER TRIGGER "SPIDERS_DATA"."SPD_I_DO_FACILITY_META_DATA" ENABLE;
