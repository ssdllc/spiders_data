--------------------------------------------------------
--  DDL for Trigger SPD_I_3D_SESSIONS
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE TRIGGER "SPIDERS_DATA"."SPD_I_3D_SESSIONS" 
BEFORE INSERT ON SPD_T_3D_SESSIONS FOR EACH ROW
DECLARE
	l_session_key varchar2(32);
	l_count number;
BEGIN
	--CREATE NEW SESSION KEY
	select dbms_random.string('X', 6) into l_session_key from dual;
	select count(sessionid) into l_count from spd_t_3d_sessions where session_key=l_session_key;
	--CONFIRM SESSION KEY UNIQUENESS
	WHILE l_count > 0 LOOP
		select dbms_random.string('X', 6) into l_session_key from dual;
		select count(sessionid) into l_count from spd_t_3d_sessions where session_key=l_session_key;
	END LOOP;

	:new.SESSION_KEY:=l_session_key;

	IF :new.SESSIONID IS NULL THEN
		SELECT SPD_S_3D_SESSIONS.nextval INTO :new.SESSIONID FROM dual;
	END IF;
	:new.CREATED_BY := nvl(v('APP_USER'),'NONE');
	:new.CREATED_TIMESTAMP := SYSTIMESTAMP;
END;
/
ALTER TRIGGER "SPIDERS_DATA"."SPD_I_3D_SESSIONS" ENABLE;
