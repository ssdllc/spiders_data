--------------------------------------------------------
--  DDL for Trigger SPD_I_XML_FLATTEN
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE TRIGGER "SPIDERS_DATA"."SPD_I_XML_FLATTEN" 
   BEFORE INSERT ON SPD_T_XML_FLATTEN FOR EACH ROW
   BEGIN
    IF :new.XMLFLATTENID IS NULL THEN
      SELECT SPD_S_XML_FLATTEN.nextval INTO :new.XMLFLATTENID FROM dual;
    END IF;
  --:new.CREATED_BY := nvl(v('APP_USER'),'NONE');
  :new.CREATED_TIMESTAMP := SYSTIMESTAMP;
  END;
/
ALTER TRIGGER "SPIDERS_DATA"."SPD_I_XML_FLATTEN" ENABLE;
