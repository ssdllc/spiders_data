--------------------------------------------------------
--  DDL for Trigger SPD_I_CONTRACTS
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE TRIGGER "SPIDERS_DATA"."SPD_I_CONTRACTS" 
  BEFORE INSERT ON  spiders_data.SPD_T_CONTRACTS FOR EACH ROW

  BEGIN
    IF :new.CONTRACTID IS NULL THEN
      select spiders_data.spd_s_CONTRACTS.nextval INTO :new.CONTRACTID FROM dual;

    END IF;

      :new.CREATED_BY := nvl(v('APP_USER'),'NONE');
      :new.CREATED_TIMESTAMP := SYSTIMESTAMP;
  END;
/
ALTER TRIGGER "SPIDERS_DATA"."SPD_I_CONTRACTS" ENABLE;
