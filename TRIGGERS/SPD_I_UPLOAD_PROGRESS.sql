--------------------------------------------------------
--  DDL for Trigger SPD_I_UPLOAD_PROGRESS
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE TRIGGER "SPIDERS_DATA"."SPD_I_UPLOAD_PROGRESS" BEFORE INSERT ON spiders_data.spd_t_upload_progress
FOR EACH ROW
begin
         if :new.timestamp  is null
         then select sysdate
            into :new.timestamp
            from dual;
         end if;
end;
/
ALTER TRIGGER "SPIDERS_DATA"."SPD_I_UPLOAD_PROGRESS" ENABLE;
