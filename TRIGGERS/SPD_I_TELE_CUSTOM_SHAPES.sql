--------------------------------------------------------
--  DDL for Trigger SPD_I_TELE_CUSTOM_SHAPES
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE TRIGGER "SPIDERS_DATA"."SPD_I_TELE_CUSTOM_SHAPES" BEFORE INSERT ON spiders_data.SPD_T_TELE_CUSTOM_SHAPES
  FOR EACH ROW
  begin
     if :new.CUSTOMSHAPEID  is null
     then select spiders_data.SPD_S_TELE_CUSTOM_SHAPES.nextval
        into :new.CUSTOMSHAPEID
        from dual;
     end if;
  end;
/
ALTER TRIGGER "SPIDERS_DATA"."SPD_I_TELE_CUSTOM_SHAPES" ENABLE;
