--------------------------------------------------------
--  DDL for Trigger SPD_I_TELESTRATOR_MDL_VERSIONS
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE TRIGGER "SPIDERS_DATA"."SPD_I_TELESTRATOR_MDL_VERSIONS" BEFORE INSERT ON spiders_data.spd_t_telestrator_mdl_versions
FOR EACH ROW
begin
         if :new.modelversionid  is null
         then select spiders_data.spd_s_telestrator_mdl_versions.nextval
            into :new.modelversionid
            from dual;
         end if;
end;
/
ALTER TRIGGER "SPIDERS_DATA"."SPD_I_TELESTRATOR_MDL_VERSIONS" ENABLE;
