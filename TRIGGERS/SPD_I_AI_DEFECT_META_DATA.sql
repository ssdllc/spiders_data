--------------------------------------------------------
--  DDL for Trigger SPD_I_AI_DEFECT_META_DATA
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE TRIGGER "SPIDERS_DATA"."SPD_I_AI_DEFECT_META_DATA" BEFORE INSERT ON spiders_data.spd_t_ai_defect_meta_data
FOR EACH ROW
begin
         if :new.aidmetaid  is null
         then select spiders_data.spd_s_ai_defect_meta_data.nextval
            into :new.aidmetaid
            from dual;
         end if;
end;
/
ALTER TRIGGER "SPIDERS_DATA"."SPD_I_AI_DEFECT_META_DATA" ENABLE;
