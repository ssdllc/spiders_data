--------------------------------------------------------
--  DDL for Trigger SPD_I_FILTER_SETS
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE TRIGGER "SPIDERS_DATA"."SPD_I_FILTER_SETS" 
   BEFORE INSERT ON spiders_data.SPD_T_FILTER_SETS FOR EACH ROW
   BEGIN
    IF :new.FILTERSETID IS NULL THEN
      select spiders_data.spd_s_FILTER_SETS.nextval INTO :new.FILTERSETID FROM dual;
    END IF;
  :new.CREATED_BY := nvl(v('APP_USER'),'NONE');
  :new.CREATED_TIMESTAMP := SYSTIMESTAMP;
  END;
/
ALTER TRIGGER "SPIDERS_DATA"."SPD_I_FILTER_SETS" ENABLE;
