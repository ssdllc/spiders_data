--------------------------------------------------------
--  DDL for Trigger SPD_U_3D_SESSION_MEMBERS
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE TRIGGER "SPIDERS_DATA"."SPD_U_3D_SESSION_MEMBERS" 
  BEFORE UPDATE ON SPD_T_3D_SESSION_MEMBERS FOR EACH ROW

  BEGIN
    :new.UPDATED_BY := nvl(v('APP_USER'),'NONE');
    :new.UPDATED_TIMESTAMP := SYSTIMESTAMP;
  END;
/
ALTER TRIGGER "SPIDERS_DATA"."SPD_U_3D_SESSION_MEMBERS" ENABLE;
