--------------------------------------------------------
--  DDL for Trigger SPD_I_TELESTRATOR_SES_MODELS
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE TRIGGER "SPIDERS_DATA"."SPD_I_TELESTRATOR_SES_MODELS" before
  INSERT ON spiders_data.spd_t_telestrator_ses_models FOR EACH row
  BEGIN IF :new.sessionmodelid IS NULL THEN
    select spiders_data.spd_s_telestrator_ses_models.nextval INTO :new.sessionmodelid FROM dual;
    END IF;

END;
/
ALTER TRIGGER "SPIDERS_DATA"."SPD_I_TELESTRATOR_SES_MODELS" ENABLE;
