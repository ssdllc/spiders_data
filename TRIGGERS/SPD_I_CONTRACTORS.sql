--------------------------------------------------------
--  DDL for Trigger SPD_I_CONTRACTORS
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE TRIGGER "SPIDERS_DATA"."SPD_I_CONTRACTORS" 
  BEFORE INSERT ON  spiders_data.SPD_T_CONTRACTORS FOR EACH ROW

  BEGIN
    IF :new.CONTRACTORID IS NULL THEN
      select spiders_data.spd_s_CONTRACTORS.nextval INTO :new.CONTRACTORID FROM dual;

    END IF;

      :new.CREATED_BY := nvl(v('APP_USER'),'NONE');
      :new.CREATED_TIMESTAMP := SYSTIMESTAMP;
  END;
/
ALTER TRIGGER "SPIDERS_DATA"."SPD_I_CONTRACTORS" ENABLE;
