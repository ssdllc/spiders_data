--------------------------------------------------------
--  DDL for Trigger SPD_I_CONTRACTORS_PER_CONTRACT
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE TRIGGER "SPIDERS_DATA"."SPD_I_CONTRACTORS_PER_CONTRACT" 
  BEFORE INSERT ON  spiders_data.SPD_T_CONTRACTORS_PER_CONTRACT FOR EACH ROW

  BEGIN
    IF :new.CPCID IS NULL THEN
      select spiders_data.spd_s_CONTRACTORS_PER_CONTRACT.nextval INTO :new.CPCID FROM dual;

    END IF;

      :new.CREATED_BY := nvl(v('APP_USER'),'NONE');
      :new.CREATED_TIMESTAMP := SYSTIMESTAMP;
  END;
/
ALTER TRIGGER "SPIDERS_DATA"."SPD_I_CONTRACTORS_PER_CONTRACT" ENABLE;
