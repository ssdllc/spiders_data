--------------------------------------------------------
--  DDL for Trigger SPD_U_SPIDERS_MAXIMO_SR
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE TRIGGER "SPIDERS_DATA"."SPD_U_SPIDERS_MAXIMO_SR" 
  BEFORE UPDATE ON SPD_T_SPIDERS_MAXIMO_SR FOR EACH ROW

  BEGIN
    :new.UPDATED_BY := nvl(v('APP_USER'),'NONE');
    :new.UPDATED_TIMESTAMP := SYSTIMESTAMP;
  END;
/
ALTER TRIGGER "SPIDERS_DATA"."SPD_U_SPIDERS_MAXIMO_SR" ENABLE;
