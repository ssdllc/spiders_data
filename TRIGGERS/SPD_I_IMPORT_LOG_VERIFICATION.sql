--------------------------------------------------------
--  DDL for Trigger SPD_I_IMPORT_LOG_VERIFICATION
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE TRIGGER "SPIDERS_DATA"."SPD_I_IMPORT_LOG_VERIFICATION" 
before insert on "SPIDERS_DATA"."SPD_T_IMPORT_LOG_VERIFICATION"
for each row begin
    if inserting then
        if :NEW."IMPORTLOGVERIFICATIONID" is null then
            select spiders_data.spd_s_IMPORT_LOG_VERIFICATION.nextval into :NEW."IMPORTLOGVERIFICATIONID" from dual;
        end if;
    end if;
end;
/
ALTER TRIGGER "SPIDERS_DATA"."SPD_I_IMPORT_LOG_VERIFICATION" ENABLE;
