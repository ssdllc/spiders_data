--------------------------------------------------------
--  DDL for Trigger SPD_I_ACTION_NOTES
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE TRIGGER "SPIDERS_DATA"."SPD_I_ACTION_NOTES" 
   BEFORE INSERT ON SPD_T_ACTION_NOTES FOR EACH ROW 
   BEGIN 
    IF :new.ACTIONNOTEID IS NULL THEN 
      SELECT SPD_S_ACTION_NOTES.nextval INTO :new.ACTIONNOTEID FROM dual; 
    END IF; 
  :new.CREATED_BY := nvl(v('APP_USER'),'NONE'); 
  :new.CREATED_TIMESTAMP := SYSTIMESTAMP; 
  END;
/
ALTER TRIGGER "SPIDERS_DATA"."SPD_I_ACTION_NOTES" ENABLE;
