--------------------------------------------------------
--  DDL for Trigger SPD_I_XML_ACTSUPPORT_FILES
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE TRIGGER "SPIDERS_DATA"."SPD_I_XML_ACTSUPPORT_FILES" 
   BEFORE INSERT ON SPD_T_XML_ACTSUPPORT_FILES FOR EACH ROW
   BEGIN
    IF :new.FILEID IS NULL THEN
      SELECT SPD_S_XML_ACTSUPPORT_FILES.nextval INTO :new.FILEID FROM dual;
    END IF;
  --:new.CREATED_BY := nvl(v('APP_USER'),'NONE');
  :new.CREATED_TIMESTAMP := SYSTIMESTAMP;
  END;
/
ALTER TRIGGER "SPIDERS_DATA"."SPD_I_XML_ACTSUPPORT_FILES" ENABLE;
