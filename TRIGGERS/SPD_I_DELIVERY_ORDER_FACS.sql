--------------------------------------------------------
--  DDL for Trigger SPD_I_DELIVERY_ORDER_FACS
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE TRIGGER "SPIDERS_DATA"."SPD_I_DELIVERY_ORDER_FACS" 
  BEFORE INSERT ON  spiders_data.SPD_T_DELIVERY_ORDER_FACS FOR EACH ROW

  declare
  lc_nDOFID number;
  lc_nADOID number;

  BEGIN
    /*
      I'm doing to much in this trigger right now, but we will fix it in a bit.
      we need to get the ADOID from the delivery_order record first.
    */

    select adoid into lc_nADOID from spiders_data.spd_t_delivery_orders where deliveryorderid = :new.DELIVERYORDERID;



    IF :new.DELIVERYORDERFACILITYID IS NULL THEN
      select spiders_data.spd_s_DELIVERY_ORDER_FACS.nextval INTO :new.DELIVERYORDERFACILITYID FROM dual;

    END IF;
/* --tc removed this 4/2/2018 --refactor - remove legacy dependencies 
    --put it back for now but fixed the dofic issue below

*/
    if :new.DOFID is NULL then
      select spiders_data.spd_s_do_facilities.nextval into :new.DOFID from dual; --tc changed this 4/2/2018
      --select spiders_data.spd_s_do_facilities.nextval into lc_nDOFID from dual;
    else
      lc_nDOFID := :new.DOFID;
    end if;
    insert into debug (message) values ('inserting into spd_t_do_facilities with dofid = '||lc_nDOFID);

    insert into SPIDERS_DATA.spd_t_do_facilities (dofid,adoid,facid) values (lc_nDOFID,lc_nADOID, :new.FACID);
--
    :new.CREATED_BY := nvl(v('APP_USER'),'NONE');
    :new.CREATED_TIMESTAMP := SYSTIMESTAMP;
  END;
/
ALTER TRIGGER "SPIDERS_DATA"."SPD_I_DELIVERY_ORDER_FACS" ENABLE;
