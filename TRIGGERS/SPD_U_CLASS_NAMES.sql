--------------------------------------------------------
--  DDL for Trigger SPD_U_CLASS_NAMES
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE TRIGGER "SPIDERS_DATA"."SPD_U_CLASS_NAMES" 
  BEFORE UPDATE ON spiders_data.SPD_T_CLASS_NAMES FOR EACH ROW

  BEGIN
    :new.UPDATED_BY := nvl(v('APP_USER'),'NONE');
    :new.UPDATED_TIMESTAMP := SYSTIMESTAMP;
  END;
/
ALTER TRIGGER "SPIDERS_DATA"."SPD_U_CLASS_NAMES" ENABLE;
