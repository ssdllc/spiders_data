--------------------------------------------------------
--  DDL for Trigger SPD_I_FILTERS
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE TRIGGER "SPIDERS_DATA"."SPD_I_FILTERS" 
   BEFORE INSERT ON spiders_data.SPD_T_FILTERS FOR EACH ROW
   BEGIN
    IF :new.FILTERID IS NULL THEN
      select spiders_data.spd_s_FILTERS.nextval INTO :new.FILTERID FROM dual;
    END IF;
  :new.CREATED_BY := nvl(v('APP_USER'),'NONE');
  :new.CREATED_TIMESTAMP := SYSTIMESTAMP;
  END;
/
ALTER TRIGGER "SPIDERS_DATA"."SPD_I_FILTERS" ENABLE;
