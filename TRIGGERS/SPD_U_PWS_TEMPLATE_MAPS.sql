--------------------------------------------------------
--  DDL for Trigger SPD_U_PWS_TEMPLATE_MAPS
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE TRIGGER "SPIDERS_DATA"."SPD_U_PWS_TEMPLATE_MAPS" 
  BEFORE UPDATE ON spiders_data.SPD_T_PWS_TEMPLATE_MAPS FOR EACH ROW

  BEGIN
    :new.UPDATED_BY := nvl(v('APP_USER'),'NONE');
    :new.UPDATED_TIMESTAMP := SYSTIMESTAMP;
  END;
/
ALTER TRIGGER "SPIDERS_DATA"."SPD_U_PWS_TEMPLATE_MAPS" ENABLE;
