--------------------------------------------------------
--  DDL for Trigger SPD_I_MAXIMO_AIRFIELD_SRS
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE TRIGGER "SPIDERS_DATA"."SPD_I_MAXIMO_AIRFIELD_SRS" 
  BEFORE INSERT ON spiders_data.SPD_T_MAXIMO_AIRFIELD_SRS FOR EACH ROW

  BEGIN
    IF :new.MAXIMOAIRFIELDSRID IS NULL THEN
      select spiders_data.spd_s_MAXIMO_AIRFIELD_SRS.nextval INTO :new.MAXIMOAIRFIELDSRID FROM dual;
    END IF;
  END;
/
ALTER TRIGGER "SPIDERS_DATA"."SPD_I_MAXIMO_AIRFIELD_SRS" ENABLE;
