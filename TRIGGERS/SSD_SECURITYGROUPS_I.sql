--------------------------------------------------------
--  DDL for Trigger SSD_SECURITYGROUPS_I
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE TRIGGER "SPIDERS_DATA"."SSD_SECURITYGROUPS_I" before insert on spiders_data.SSD_SECURITY_GROUPS for each row begin
     if inserting then
       if :NEW.SECURITYGROUPID is null then

     select spiders_data.SSD_S_SECURITYGROUPID.nextval into :NEW.SECURITYGROUPID from dual;
       end if;    end if; end;
/
ALTER TRIGGER "SPIDERS_DATA"."SSD_SECURITYGROUPS_I" ENABLE;
