--------------------------------------------------------
--  DDL for Trigger SPD_I_LIST_SET_PLANS
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE TRIGGER "SPIDERS_DATA"."SPD_I_LIST_SET_PLANS" BEFORE INSERT ON spiders_data.spd_t_list_set_plans
FOR EACH ROW
begin
         if :new.setplanid  is null
         then select spiders_data.spd_s_list_set_plans.nextval
            into :new.setplanid
            from dual;
         end if;

         if :new.noteskey  is null
         then select spiders_data.spd_s_noteskey.nextval
            into :new.noteskey
            from dual;
         end if;

         if :new.created_on is null
         then :new.created_on := sysdate;
         end if;
end;
/
ALTER TRIGGER "SPIDERS_DATA"."SPD_I_LIST_SET_PLANS" ENABLE;
