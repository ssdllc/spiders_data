--------------------------------------------------------
--  DDL for Trigger SPD_I_LISTS
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE TRIGGER "SPIDERS_DATA"."SPD_I_LISTS" BEFORE INSERT ON spiders_data.spd_t_lists
FOR EACH ROW
begin
         if :new.listid  is null
         then select spiders_data.spd_s_lists.nextval
            into :new.listid
            from dual;
         end if;
end;
/
ALTER TRIGGER "SPIDERS_DATA"."SPD_I_LISTS" ENABLE;
