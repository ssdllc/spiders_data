--------------------------------------------------------
--  DDL for Trigger SPD_I_X3D_MODELS
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE TRIGGER "SPIDERS_DATA"."SPD_I_X3D_MODELS" 
   BEFORE INSERT ON spiders_data.SPD_T_X3D_MODELS FOR EACH ROW
   BEGIN
    IF :new.X3DMODELID IS NULL THEN
      select spiders_data.spd_s_X3D_MODELS.nextval INTO :new.X3DMODELID FROM dual;
    END IF;
  :new.CREATED_BY := nvl(v('APP_USER'),'NONE');
  :new.CREATED_TIMESTAMP := SYSTIMESTAMP;
  END;
/
ALTER TRIGGER "SPIDERS_DATA"."SPD_I_X3D_MODELS" ENABLE;
