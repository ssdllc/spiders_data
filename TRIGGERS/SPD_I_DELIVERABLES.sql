--------------------------------------------------------
--  DDL for Trigger SPD_I_DELIVERABLES
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE TRIGGER "SPIDERS_DATA"."SPD_I_DELIVERABLES" 
  BEFORE INSERT ON  spiders_data.SPD_T_DELIVERABLES FOR EACH ROW

  BEGIN
    IF :new.DELIVERABLEID IS NULL THEN
      select spiders_data.spd_s_DELIVERABLES.nextval INTO :new.DELIVERABLEID FROM dual;

    END IF;

      :new.CREATED_BY := nvl(v('APP_USER'),'NONE');
      :new.CREATED_TIMESTAMP := SYSTIMESTAMP;
  END;
/
ALTER TRIGGER "SPIDERS_DATA"."SPD_I_DELIVERABLES" ENABLE;
