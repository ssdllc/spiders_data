--------------------------------------------------------
--  DDL for Trigger SPD_I_THUMBNAILS
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE TRIGGER "SPIDERS_DATA"."SPD_I_THUMBNAILS" before insert on "SPIDERS_DATA"."SPD_T_THUMBNAILS"    for each row begin     if inserting then       if :NEW."THUMBNAILID" is null then          select spiders_data.spd_s_THUMBNAILS.nextval into :NEW."THUMBNAILID" from dual;       end if;    end if; end;
/
ALTER TRIGGER "SPIDERS_DATA"."SPD_I_THUMBNAILS" ENABLE;
