--------------------------------------------------------
--  DDL for Trigger SPD_U_SIA_STRUCTURES
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE TRIGGER "SPIDERS_DATA"."SPD_U_SIA_STRUCTURES" 
  BEFORE UPDATE ON SPD_T_SIA_STRUCTURES FOR EACH ROW

  BEGIN
    :new.UPDATED_BY := nvl(v('APP_USER'),'NONE');
    :new.UPDATED_TIMESTAMP := SYSTIMESTAMP;
  END;
/
ALTER TRIGGER "SPIDERS_DATA"."SPD_U_SIA_STRUCTURES" ENABLE;
