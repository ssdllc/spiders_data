--------------------------------------------------------
--  DDL for Trigger SPD_U_UNIT_TESTS
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE TRIGGER "SPIDERS_DATA"."SPD_U_UNIT_TESTS" 
  BEFORE UPDATE ON SPD_T_UNIT_TESTS FOR EACH ROW

  BEGIN
    :new.UPDATED_BY := nvl(v('APP_USER'),'NONE');
    :new.UPDATED_TIMESTAMP := SYSTIMESTAMP;
  END;
/
ALTER TRIGGER "SPIDERS_DATA"."SPD_U_UNIT_TESTS" ENABLE;
