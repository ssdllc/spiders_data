--------------------------------------------------------
--  DDL for Trigger SPD_U_XML_ACTIONS
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE TRIGGER "SPIDERS_DATA"."SPD_U_XML_ACTIONS" 
  BEFORE UPDATE ON SPD_T_XML_ACTIONS FOR EACH ROW

  BEGIN
    :new.UPDATED_BY := nvl(v('APP_USER'),'NONE');
    :new.UPDATED_TIMESTAMP := SYSTIMESTAMP;
  END;
/
ALTER TRIGGER "SPIDERS_DATA"."SPD_U_XML_ACTIONS" ENABLE;
