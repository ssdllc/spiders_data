--------------------------------------------------------
--  DDL for Trigger SPD_U_REST_TO_TABLE_MAPS
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE TRIGGER "SPIDERS_DATA"."SPD_U_REST_TO_TABLE_MAPS" 
BEFORE UPDATE ON spiders_data.SPD_T_REST_TO_TABLE_MAPS FOR EACH ROW

BEGIN
  :new.UPDATED_BY := nvl(v('APP_USER'),'NONE');
  :new.UPDATED_TIMESTAMP := SYSTIMESTAMP;
END;
/
ALTER TRIGGER "SPIDERS_DATA"."SPD_U_REST_TO_TABLE_MAPS" ENABLE;
