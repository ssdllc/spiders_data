--------------------------------------------------------
--  DDL for Trigger SPD_I_REPORT_KEYWORDS
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE TRIGGER "SPIDERS_DATA"."SPD_I_REPORT_KEYWORDS" 
   before insert on "SPIDERS_DATA"."SPD_T_REPORT_KEYWORDS"
   for each row
begin
   if inserting then
      if :NEW."REPORTKEYWORDID" is null then
         select spiders_data.spd_s_REPORT_KEYWORDS.nextval into :NEW."REPORTKEYWORDID" from dual;
      end if;
   end if;
end;
/
ALTER TRIGGER "SPIDERS_DATA"."SPD_I_REPORT_KEYWORDS" ENABLE;
