--------------------------------------------------------
--  DDL for Trigger SPD_I_DO_CE_LABOR
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE TRIGGER "SPIDERS_DATA"."SPD_I_DO_CE_LABOR" BEFORE INSERT ON spiders_data.spd_t_do_ce_labor
FOR EACH ROW
begin
         if :new.docelid  is null
         then select spiders_data.spd_s_do_ce_labor.nextval
            into :new.docelid
            from dual;
         end if;
end;
/
ALTER TRIGGER "SPIDERS_DATA"."SPD_I_DO_CE_LABOR" ENABLE;
