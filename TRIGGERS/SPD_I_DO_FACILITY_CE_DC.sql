--------------------------------------------------------
--  DDL for Trigger SPD_I_DO_FACILITY_CE_DC
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE TRIGGER "SPIDERS_DATA"."SPD_I_DO_FACILITY_CE_DC" BEFORE INSERT ON spiders_data.spd_t_do_facility_ce_dc
FOR EACH ROW
begin
         if :new.afceid  is null
         then select spiders_data.spd_s_do_facility_ce_dc.nextval
            into :new.afceid
            from dual;
         end if;
end;
/
ALTER TRIGGER "SPIDERS_DATA"."SPD_I_DO_FACILITY_CE_DC" ENABLE;
