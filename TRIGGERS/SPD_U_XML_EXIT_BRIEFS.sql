--------------------------------------------------------
--  DDL for Trigger SPD_U_XML_EXIT_BRIEFS
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE TRIGGER "SPIDERS_DATA"."SPD_U_XML_EXIT_BRIEFS" 
  BEFORE UPDATE ON SPD_T_XML_EXIT_BRIEFS FOR EACH ROW

  BEGIN
    :new.UPDATED_BY := nvl(v('APP_USER'),'NONE');
    :new.UPDATED_TIMESTAMP := SYSTIMESTAMP;
  END;
/
ALTER TRIGGER "SPIDERS_DATA"."SPD_U_XML_EXIT_BRIEFS" ENABLE;
