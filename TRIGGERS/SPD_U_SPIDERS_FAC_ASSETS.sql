--------------------------------------------------------
--  DDL for Trigger SPD_U_SPIDERS_FAC_ASSETS
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE TRIGGER "SPIDERS_DATA"."SPD_U_SPIDERS_FAC_ASSETS" 
  BEFORE UPDATE ON SPD_T_SPIDERS_FAC_ASSETS FOR EACH ROW

  BEGIN
    :new.UPDATED_BY := nvl(v('APP_USER'),'NONE');
    :new.UPDATED_TIMESTAMP := SYSTIMESTAMP;
  END;
/
ALTER TRIGGER "SPIDERS_DATA"."SPD_U_SPIDERS_FAC_ASSETS" ENABLE;
