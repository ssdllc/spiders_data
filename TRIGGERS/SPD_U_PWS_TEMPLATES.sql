--------------------------------------------------------
--  DDL for Trigger SPD_U_PWS_TEMPLATES
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE TRIGGER "SPIDERS_DATA"."SPD_U_PWS_TEMPLATES" 
  BEFORE UPDATE ON spiders_data.SPD_T_PWS_TEMPLATES FOR EACH ROW

  BEGIN
    :new.UPDATED_BY := nvl(v('APP_USER'),'NONE');
    :new.UPDATED_TIMESTAMP := SYSTIMESTAMP;
  END;
/
ALTER TRIGGER "SPIDERS_DATA"."SPD_U_PWS_TEMPLATES" ENABLE;
