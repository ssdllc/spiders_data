--------------------------------------------------------
--  DDL for Trigger SPD_I_PLAN_NOTES
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE TRIGGER "SPIDERS_DATA"."SPD_I_PLAN_NOTES" 
   BEFORE INSERT ON SPD_T_PLAN_NOTES FOR EACH ROW 
   BEGIN 
    IF :new.PLANNOTEID IS NULL THEN 
      SELECT SPD_S_PLAN_NOTES.nextval INTO :new.PLANNOTEID FROM dual; 
    END IF; 
  :new.CREATED_BY := nvl(v('APP_USER'),'NONE'); 
  :new.CREATED_TIMESTAMP := SYSTIMESTAMP; 
  END;
/
ALTER TRIGGER "SPIDERS_DATA"."SPD_I_PLAN_NOTES" ENABLE;
