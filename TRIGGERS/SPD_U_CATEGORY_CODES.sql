--------------------------------------------------------
--  DDL for Trigger SPD_U_CATEGORY_CODES
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE TRIGGER "SPIDERS_DATA"."SPD_U_CATEGORY_CODES" 
  BEFORE UPDATE ON SPD_T_CATEGORY_CODES FOR EACH ROW

  BEGIN
    :new.UPDATED_BY := nvl(v('APP_USER'),'NONE');
    :new.UPDATED_TIMESTAMP := SYSTIMESTAMP;
  END;
/
ALTER TRIGGER "SPIDERS_DATA"."SPD_U_CATEGORY_CODES" ENABLE;
