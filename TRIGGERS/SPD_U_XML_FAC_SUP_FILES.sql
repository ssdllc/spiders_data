--------------------------------------------------------
--  DDL for Trigger SPD_U_XML_FAC_SUP_FILES
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE TRIGGER "SPIDERS_DATA"."SPD_U_XML_FAC_SUP_FILES" 
  BEFORE UPDATE ON SPD_T_XML_FAC_SUP_FILES FOR EACH ROW

  BEGIN
    :new.UPDATED_BY := nvl(v('APP_USER'),'NONE');
    :new.UPDATED_TIMESTAMP := SYSTIMESTAMP;
  END;
/
ALTER TRIGGER "SPIDERS_DATA"."SPD_U_XML_FAC_SUP_FILES" ENABLE;
