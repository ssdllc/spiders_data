--------------------------------------------------------
--  DDL for Trigger SPD_U_MAXIMO_AF_ASSETS
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE TRIGGER "SPIDERS_DATA"."SPD_U_MAXIMO_AF_ASSETS" 
BEFORE UPDATE ON spiders_data.SPD_T_MAXIMO_AF_ASSETS FOR EACH ROW

BEGIN
  :new.UPDATED_BY := nvl(v('APP_USER'),'NONE');
  :new.UPDATED_TIMESTAMP := SYSTIMESTAMP;
END;
/
ALTER TRIGGER "SPIDERS_DATA"."SPD_U_MAXIMO_AF_ASSETS" ENABLE;
