--------------------------------------------------------
--  DDL for Trigger SPD_U_XML_AI_FITTINGS
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE TRIGGER "SPIDERS_DATA"."SPD_U_XML_AI_FITTINGS" 
  BEFORE UPDATE ON SPD_T_XML_AI_FITTINGS FOR EACH ROW

  BEGIN
    :new.UPDATED_BY := nvl(v('APP_USER'),'NONE');
    :new.UPDATED_TIMESTAMP := SYSTIMESTAMP;
  END;
/
ALTER TRIGGER "SPIDERS_DATA"."SPD_U_XML_AI_FITTINGS" ENABLE;
