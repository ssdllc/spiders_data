--------------------------------------------------------
--  DDL for Trigger SPD_I_JOB_TITLES
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE TRIGGER "SPIDERS_DATA"."SPD_I_JOB_TITLES" 
  BEFORE INSERT ON  spiders_data.SPD_T_JOB_TITLES FOR EACH ROW

  BEGIN
    IF :new.JOBTITLEID IS NULL THEN
      select spiders_data.spd_s_JOB_TITLES.nextval INTO :new.JOBTITLEID FROM dual;

    END IF;

      :new.CREATED_BY := nvl(v('APP_USER'),'NONE');
      :new.CREATED_TIMESTAMP := SYSTIMESTAMP;
  END;
/
ALTER TRIGGER "SPIDERS_DATA"."SPD_I_JOB_TITLES" ENABLE;
