--------------------------------------------------------
--  DDL for Trigger SPD_I_INSTRUCTIONS
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE TRIGGER "SPIDERS_DATA"."SPD_I_INSTRUCTIONS" 
  BEFORE INSERT ON  spiders_data.SPD_T_INSTRUCTIONS FOR EACH ROW

  BEGIN
    IF :new.INSTRUCTIONID IS NULL THEN
      select spiders_data.spd_s_INSTRUCTIONS.nextval INTO :new.INSTRUCTIONID FROM dual;

    END IF;

      :new.CREATED_BY := nvl(v('APP_USER'),'NONE');
      :new.CREATED_TIMESTAMP := SYSTIMESTAMP;
  END;
/
ALTER TRIGGER "SPIDERS_DATA"."SPD_I_INSTRUCTIONS" ENABLE;
