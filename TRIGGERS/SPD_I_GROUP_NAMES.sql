--------------------------------------------------------
--  DDL for Trigger SPD_I_GROUP_NAMES
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE TRIGGER "SPIDERS_DATA"."SPD_I_GROUP_NAMES" 
   BEFORE INSERT ON spiders_data.SPD_T_GROUP_NAMES FOR EACH ROW
   BEGIN
    IF :new.GROUPNAMEID IS NULL THEN
      select spiders_data.spd_s_GROUP_NAMES.nextval INTO :new.GROUPNAMEID FROM dual;
    END IF;
  :new.CREATED_BY := nvl(v('APP_USER'),'NONE');
  :new.CREATED_TIMESTAMP := SYSTIMESTAMP;
  END;
/
ALTER TRIGGER "SPIDERS_DATA"."SPD_I_GROUP_NAMES" ENABLE;
