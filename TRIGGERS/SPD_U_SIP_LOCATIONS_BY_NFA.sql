--------------------------------------------------------
--  DDL for Trigger SPD_U_SIP_LOCATIONS_BY_NFA
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE TRIGGER "SPIDERS_DATA"."SPD_U_SIP_LOCATIONS_BY_NFA" 
  BEFORE UPDATE ON SPD_T_SIP_LOCATIONS_BY_NFA FOR EACH ROW

  BEGIN
    :new.UPDATED_BY := nvl(v('APP_USER'),'NONE');
    :new.UPDATED_TIMESTAMP := SYSTIMESTAMP;
  END;
/
ALTER TRIGGER "SPIDERS_DATA"."SPD_U_SIP_LOCATIONS_BY_NFA" ENABLE;
