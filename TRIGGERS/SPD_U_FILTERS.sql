--------------------------------------------------------
--  DDL for Trigger SPD_U_FILTERS
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE TRIGGER "SPIDERS_DATA"."SPD_U_FILTERS" 
  BEFORE UPDATE ON spiders_data.SPD_T_FILTERS FOR EACH ROW

  BEGIN
    :new.UPDATED_BY := nvl(v('APP_USER'),'NONE');
    :new.UPDATED_TIMESTAMP := SYSTIMESTAMP;
  END;
/
ALTER TRIGGER "SPIDERS_DATA"."SPD_U_FILTERS" ENABLE;
