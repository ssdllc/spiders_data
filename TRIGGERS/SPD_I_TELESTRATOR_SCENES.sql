--------------------------------------------------------
--  DDL for Trigger SPD_I_TELESTRATOR_SCENES
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE TRIGGER "SPIDERS_DATA"."SPD_I_TELESTRATOR_SCENES" before
  INSERT ON spiders_data.spd_t_telestrator_scenes FOR EACH row
  BEGIN IF :new.sceneid IS NULL THEN
    select spiders_data.spd_s_telestrator_scenes.nextval INTO :new.sceneid FROM dual;
    END IF;

END;
/
ALTER TRIGGER "SPIDERS_DATA"."SPD_I_TELESTRATOR_SCENES" ENABLE;
