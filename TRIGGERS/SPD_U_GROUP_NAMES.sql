--------------------------------------------------------
--  DDL for Trigger SPD_U_GROUP_NAMES
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE TRIGGER "SPIDERS_DATA"."SPD_U_GROUP_NAMES" 
  BEFORE UPDATE ON spiders_data.SPD_T_GROUP_NAMES FOR EACH ROW

  BEGIN
    :new.UPDATED_BY := nvl(v('APP_USER'),'NONE');
    :new.UPDATED_TIMESTAMP := SYSTIMESTAMP;
  END;
/
ALTER TRIGGER "SPIDERS_DATA"."SPD_U_GROUP_NAMES" ENABLE;
