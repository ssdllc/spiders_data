--------------------------------------------------------
--  DDL for Trigger SPD_I_CONFIRMATION
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE TRIGGER "SPIDERS_DATA"."SPD_I_CONFIRMATION" BEFORE INSERT ON spiders_data.spd_t_confirmation
FOR EACH ROW
begin
         if :new.cid is null
         then select spiders_data.spd_s_confirmation.nextval
            into :new.cid
            from dual;
         end if;
         :new.timestamp := sysdate;
end;
/
ALTER TRIGGER "SPIDERS_DATA"."SPD_I_CONFIRMATION" ENABLE;
