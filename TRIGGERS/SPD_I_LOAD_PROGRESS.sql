--------------------------------------------------------
--  DDL for Trigger SPD_I_LOAD_PROGRESS
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE TRIGGER "SPIDERS_DATA"."SPD_I_LOAD_PROGRESS" BEFORE INSERT ON spiders_data.spd_t_load_progress
FOR EACH ROW
begin
         if :new.loadid  is null
         then select spiders_data.spd_s_loadid.nextval
            into :new.loadid
            from dual;
         end if;



         if :new.timestamp is null
         then :new.timestamp := sysdate;
         end if;
end;
/
ALTER TRIGGER "SPIDERS_DATA"."SPD_I_LOAD_PROGRESS" ENABLE;
