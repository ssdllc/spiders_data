--------------------------------------------------------
--  DDL for Trigger SPD_U_DO_FAC_INSTRUCTIONS
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE TRIGGER "SPIDERS_DATA"."SPD_U_DO_FAC_INSTRUCTIONS" 
BEFORE UPDATE ON spiders_data.SPD_T_DO_FAC_INSTRUCTIONS FOR EACH ROW

BEGIN
  :new.UPDATED_BY := nvl(v('APP_USER'),'NONE');
  :new.UPDATED_TIMESTAMP := SYSTIMESTAMP;
END;
/
ALTER TRIGGER "SPIDERS_DATA"."SPD_U_DO_FAC_INSTRUCTIONS" ENABLE;
