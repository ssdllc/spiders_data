--------------------------------------------------------
--  DDL for Trigger SPD_S_DRNK_HISTORY
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE TRIGGER "SPIDERS_DATA"."SPD_S_DRNK_HISTORY" before
  INSERT ON "SPIDERS_DATA"."SPD_T_TELESTRATOR_DRNK_HISTORY" FOR EACH row BEGIN IF inserting THEN IF :NEW."DRUNKHISTORYID" IS NULL THEN
  select spiders_data.spd_s_DRNK_HISTORY.nextval INTO :NEW."DRUNKHISTORYID" FROM dual;
END IF;
END IF;
END;
/
ALTER TRIGGER "SPIDERS_DATA"."SPD_S_DRNK_HISTORY" ENABLE;
