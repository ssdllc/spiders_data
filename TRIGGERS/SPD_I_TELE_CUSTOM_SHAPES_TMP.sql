--------------------------------------------------------
--  DDL for Trigger SPD_I_TELE_CUSTOM_SHAPES_TMP
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE TRIGGER "SPIDERS_DATA"."SPD_I_TELE_CUSTOM_SHAPES_TMP" BEFORE INSERT ON spiders_data.SPD_T_TELE_CUSTOM_SHAPES_TMP
  FOR EACH ROW

  declare
  lc_nValidLine number := 1;


  begin
     if :new.CUSTOMSHAPETMPID  is null
     then select spiders_data.SPD_S_TELE_CUSTOM_SHAPES_TMP.nextval
        into :new.CUSTOMSHAPETMPID
        from dual;
     end if;

      if :new.length is null then
        lc_nValidLine := 0;
      end if;
      if :new.width is null then
        lc_nValidLine := 0;
      end if;
      if :new.height is null then
        lc_nValidLine := 0;
      end if;

      if :new.CODE = '' then
        if :new.DESCRIPTION = '' then
          lc_nValidLine := 0;
        end if;
      end if;

      if :new.DESCRIPTION = '' then
        if :new.CODE = '' then
          lc_nValidLine := 0;
        end if;
      end if;

      if :new.CODE is null then
        if :new.DESCRIPTION is null then
          lc_nValidLine := 0;
        end if;
      end if;

      if :new.DESCRIPTION is null then
        if :new.CODE is null then
          lc_nValidLine := 0;
        end if;
      end if;

     :new.VALID_LINE := lc_nValidLine;

  end;
/
ALTER TRIGGER "SPIDERS_DATA"."SPD_I_TELE_CUSTOM_SHAPES_TMP" ENABLE;
