--------------------------------------------------------
--  DDL for Trigger SPD_I_CLASS_NAMES
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE TRIGGER "SPIDERS_DATA"."SPD_I_CLASS_NAMES" 
   BEFORE INSERT ON spiders_data.SPD_T_CLASS_NAMES FOR EACH ROW
   BEGIN
    IF :new.CLASSNAMEID IS NULL THEN
      select spiders_data.spd_s_CLASS_NAMES.nextval INTO :new.CLASSNAMEID FROM dual;
    END IF;
  :new.CREATED_BY := nvl(v('APP_USER'),'NONE');
  :new.CREATED_TIMESTAMP := SYSTIMESTAMP;
  END;
/
ALTER TRIGGER "SPIDERS_DATA"."SPD_I_CLASS_NAMES" ENABLE;
