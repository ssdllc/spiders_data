--------------------------------------------------------
--  DDL for Trigger SPD_I_INTER_SET_PLANS
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE TRIGGER "SPIDERS_DATA"."SPD_I_INTER_SET_PLANS" BEFORE INSERT ON spiders_data.spd_t_inter_set_plans
FOR EACH ROW
begin
         if :new.setplaninterid  is null
         then select spiders_data.spd_s_inter_set_plans.nextval
            into :new.setplaninterid
            from dual;
         end if;
end;
/
ALTER TRIGGER "SPIDERS_DATA"."SPD_I_INTER_SET_PLANS" ENABLE;
