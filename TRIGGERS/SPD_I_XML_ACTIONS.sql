--------------------------------------------------------
--  DDL for Trigger SPD_I_XML_ACTIONS
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE TRIGGER "SPIDERS_DATA"."SPD_I_XML_ACTIONS" 
   BEFORE INSERT ON SPD_T_XML_ACTIONS FOR EACH ROW
   BEGIN
    IF :new.XMLACTIONID IS NULL THEN
      SELECT SPD_S_XML_ACTIONS.nextval INTO :new.XMLACTIONID FROM dual;
    END IF;
  --:new.CREATED_BY := nvl(v('APP_USER'),'NONE');
  :new.CREATED_TIMESTAMP := SYSTIMESTAMP;
  END;
/
ALTER TRIGGER "SPIDERS_DATA"."SPD_I_XML_ACTIONS" ENABLE;
