--------------------------------------------------------
--  DDL for Trigger SPD_U_XML_ET_METADATA
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE TRIGGER "SPIDERS_DATA"."SPD_U_XML_ET_METADATA" 
  BEFORE UPDATE ON SPD_T_XML_ET_METADATA FOR EACH ROW

  BEGIN
    :new.UPDATED_BY := nvl(v('APP_USER'),'NONE');
    :new.UPDATED_TIMESTAMP := SYSTIMESTAMP;
  END;
/
ALTER TRIGGER "SPIDERS_DATA"."SPD_U_XML_ET_METADATA" ENABLE;
