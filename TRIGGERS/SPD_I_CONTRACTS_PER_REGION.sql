--------------------------------------------------------
--  DDL for Trigger SPD_I_CONTRACTS_PER_REGION
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE TRIGGER "SPIDERS_DATA"."SPD_I_CONTRACTS_PER_REGION" BEFORE INSERT ON spiders_data.spd_t_contracts_per_region
FOR EACH ROW
begin
         if :new.cprid  is null
         then select spiders_data.spd_s_contracts_per_region.nextval
            into :new.cprid
            from dual;
         end if;
end;
/
ALTER TRIGGER "SPIDERS_DATA"."SPD_I_CONTRACTS_PER_REGION" ENABLE;
