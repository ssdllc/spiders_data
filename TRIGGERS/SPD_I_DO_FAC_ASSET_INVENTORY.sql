--------------------------------------------------------
--  DDL for Trigger SPD_I_DO_FAC_ASSET_INVENTORY
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE TRIGGER "SPIDERS_DATA"."SPD_I_DO_FAC_ASSET_INVENTORY" BEFORE INSERT ON spiders_data.spd_t_do_fac_asset_inventory
FOR EACH ROW
begin
         if :new.aiid  is null
         then select spiders_data.spd_s_do_fac_asset_inventory.nextval
            into :new.aiid
            from dual;
         end if;
end;
/
ALTER TRIGGER "SPIDERS_DATA"."SPD_I_DO_FAC_ASSET_INVENTORY" ENABLE;
