--------------------------------------------------------
--  DDL for Trigger SPD_I_DRY_DOCK_CI_TMP_META
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE TRIGGER "SPIDERS_DATA"."SPD_I_DRY_DOCK_CI_TMP_META" 
BEFORE INSERT ON spiders_data.spd_t_dry_dock_ci_tmp_meta FOR EACH ROW

BEGIN IF :new.DRYDOCKCIMETAID IS NULL THEN
  SELECT spiders_data.spd_s_dry_dock_ci_tmp_meta.nextval INTO :new.DRYDOCKCIMETAID FROM dual;
END IF;

END;
/
ALTER TRIGGER "SPIDERS_DATA"."SPD_I_DRY_DOCK_CI_TMP_META" ENABLE;
