--------------------------------------------------------
--  DDL for Trigger SPD_I_XML_FACILITIES_MATCH
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE TRIGGER "SPIDERS_DATA"."SPD_I_XML_FACILITIES_MATCH" 
before insert on "SPIDERS_DATA"."SPD_T_XML_FACILITIES_MATCH"
for each row begin
    if inserting then
        if :NEW."XMLFACILITYMATCHID" is null then
            select spiders_data.spd_s_XML_FACILITIES_MATCH.nextval into :NEW."XMLFACILITYMATCHID" from dual;
        end if;
    end if;
end;
/
ALTER TRIGGER "SPIDERS_DATA"."SPD_I_XML_FACILITIES_MATCH" ENABLE;
