--------------------------------------------------------
--  DDL for Trigger SPD_I_FACILITY_META_DATA
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE TRIGGER "SPIDERS_DATA"."SPD_I_FACILITY_META_DATA" BEFORE INSERT ON spiders_data.spd_t_facility_meta_data
FOR EACH ROW
begin
         if :new.facmetaid  is null
         then select spiders_data.spd_s_facility_meta_data.nextval
            into :new.facmetaid
            from dual;
         end if;
end;
/
ALTER TRIGGER "SPIDERS_DATA"."SPD_I_FACILITY_META_DATA" ENABLE;
