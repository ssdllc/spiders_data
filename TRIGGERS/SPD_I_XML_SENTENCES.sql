--------------------------------------------------------
--  DDL for Trigger SPD_I_XML_SENTENCES
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE TRIGGER "SPIDERS_DATA"."SPD_I_XML_SENTENCES" 
   BEFORE INSERT ON SPD_T_XML_SENTENCES FOR EACH ROW
   BEGIN
    IF :new.SENTENCEID IS NULL THEN
      SELECT SPD_S_XML_SENTENCES.nextval INTO :new.SENTENCEID FROM dual;
    END IF;
  --:new.CREATED_BY := nvl(v('APP_USER'),'NONE');
  :new.CREATED_TIMESTAMP := SYSTIMESTAMP;
  END;
/
ALTER TRIGGER "SPIDERS_DATA"."SPD_I_XML_SENTENCES" ENABLE;
