--------------------------------------------------------
--  DDL for Trigger SPD_I_LIST_SET_PLAN_TYPES
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE TRIGGER "SPIDERS_DATA"."SPD_I_LIST_SET_PLAN_TYPES" BEFORE INSERT ON spiders_data.spd_t_list_set_plan_types
FOR EACH ROW
begin
         if :new.listid is null
         then select spiders_data.spd_s_list_set_plan_types.nextval
            into :new.listid
            from dual;
         end if;
end;
/
ALTER TRIGGER "SPIDERS_DATA"."SPD_I_LIST_SET_PLAN_TYPES" ENABLE;
