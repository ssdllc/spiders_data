--------------------------------------------------------
--  DDL for Trigger SPD_U_X3D_MODELS
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE TRIGGER "SPIDERS_DATA"."SPD_U_X3D_MODELS" 
  BEFORE UPDATE ON spiders_data.SPD_T_X3D_MODELS FOR EACH ROW

  BEGIN
    :new.UPDATED_BY := nvl(v('APP_USER'),'NONE');
    :new.UPDATED_TIMESTAMP := SYSTIMESTAMP;
  END;
/
ALTER TRIGGER "SPIDERS_DATA"."SPD_U_X3D_MODELS" ENABLE;
