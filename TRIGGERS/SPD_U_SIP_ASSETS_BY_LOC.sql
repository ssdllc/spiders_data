--------------------------------------------------------
--  DDL for Trigger SPD_U_SIP_ASSETS_BY_LOC
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE TRIGGER "SPIDERS_DATA"."SPD_U_SIP_ASSETS_BY_LOC" 
  BEFORE UPDATE ON SPD_T_SIP_ASSETS_BY_LOC FOR EACH ROW

  BEGIN
    :new.UPDATED_BY := nvl(v('APP_USER'),'NONE');
    :new.UPDATED_TIMESTAMP := SYSTIMESTAMP;
  END;
/
ALTER TRIGGER "SPIDERS_DATA"."SPD_U_SIP_ASSETS_BY_LOC" ENABLE;
