--------------------------------------------------------
--  DDL for Trigger SPD_I_UNIT_TESTS
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE TRIGGER "SPIDERS_DATA"."SPD_I_UNIT_TESTS" 
   BEFORE INSERT ON SPD_T_UNIT_TESTS FOR EACH ROW
   BEGIN
    IF :new.UNITTESTID IS NULL THEN
      SELECT SPD_S_UNIT_TESTS.nextval INTO :new.UNITTESTID FROM dual;
    END IF;
  :new.CREATED_BY := nvl(v('APP_USER'),'NONE');
  :new.CREATED_TIMESTAMP := SYSTIMESTAMP;
  END;
/
ALTER TRIGGER "SPIDERS_DATA"."SPD_I_UNIT_TESTS" ENABLE;
