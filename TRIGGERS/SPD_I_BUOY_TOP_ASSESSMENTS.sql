--------------------------------------------------------
--  DDL for Trigger SPD_I_BUOY_TOP_ASSESSMENTS
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE TRIGGER "SPIDERS_DATA"."SPD_I_BUOY_TOP_ASSESSMENTS" 
   BEFORE INSERT ON spiders_data.SPD_T_BUOY_TOP_ASSESSMENTS FOR EACH ROW
   BEGIN
    IF :new.BUOYTOPASSESSMENTID IS NULL THEN
      select spiders_data.spd_s_BUOY_TOP_ASSESSMENTS.nextval INTO :new.BUOYTOPASSESSMENTID FROM dual;
    END IF;
  :new.CREATED_BY := nvl(v('APP_USER'),'NONE');
  :new.CREATED_TIMESTAMP := SYSTIMESTAMP;
  END;
/
ALTER TRIGGER "SPIDERS_DATA"."SPD_I_BUOY_TOP_ASSESSMENTS" ENABLE;
