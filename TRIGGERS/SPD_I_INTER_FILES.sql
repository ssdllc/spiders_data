--------------------------------------------------------
--  DDL for Trigger SPD_I_INTER_FILES
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE TRIGGER "SPIDERS_DATA"."SPD_I_INTER_FILES" BEFORE INSERT ON spiders_data.spd_t_inter_files
FOR EACH ROW
begin
         if :new.filesinterid  is null
         then select spiders_data.spd_s_inter_files.nextval
            into :new.filesinterid
            from dual;
         end if;
end;
/
ALTER TRIGGER "SPIDERS_DATA"."SPD_I_INTER_FILES" ENABLE;
