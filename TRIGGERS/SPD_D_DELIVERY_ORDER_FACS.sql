set define off;
/
CREATE OR REPLACE TRIGGER SPD_D_DELIVERY_ORDER_FACS
BEFORE DELETE ON  spiders_data.SPD_T_DELIVERY_ORDER_FACS FOR EACH ROW

DECLARE

	lc_count number;
	n_adoid number;
	n_dofid number := 0;
	
BEGIN

	if :old.dofid is null then
		--GET ADOID FROM spd_t_delivery_orders
		select count(adoid) into lc_count 
		from spd_t_delivery_orders
		where deliveryorderid = :old.deliveryorderid;
		
    insert into debug (message) values (':old.deliveryorderid = '||:old.deliveryorderid);
    
		if lc_count = 1 then
			select adoid into n_adoid 
			from spd_t_delivery_orders
			where deliveryorderid = :old.deliveryorderid;
		
			--GET DOFID TO DELETE FROM spd_t_do_facilities WITH ADOID & FACID
			select count(dofid) into lc_count 
			from spd_t_do_facilities
			where adoid = n_adoid
			  and facid = :old.facid;
			  
        insert into debug (message) values (':old.facid = '||:old.facid || ' and n_adoid = '||n_adoid);
			if lc_count = 1 then
				select dofid into n_dofid 
				from spd_t_do_facilities
				where adoid = n_adoid
				  and facid = :old.facid;
			end if;
		end if;
		  
	else
		n_dofid := :old.dofid;	  
	end if;
	
	if n_dofid > 0 then
    insert into debug (message) values ('deleting dofid '||n_dofid);
		DELETE FROM spd_t_do_facilities WHERE dofid = n_dofid;
	end if;
	
END;