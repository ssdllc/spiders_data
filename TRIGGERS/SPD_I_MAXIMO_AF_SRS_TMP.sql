--------------------------------------------------------
--  DDL for Trigger SPD_I_MAXIMO_AF_SRS_TMP
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE TRIGGER "SPIDERS_DATA"."SPD_I_MAXIMO_AF_SRS_TMP" 
   BEFORE INSERT ON spiders_data.SPD_T_MAXIMO_AF_SRS_TMP FOR EACH ROW
   BEGIN
    IF :new.MAXIMOAFSRID IS NULL THEN
      select spiders_data.spd_s_MAXIMO_AF_SRS_TMP.nextval INTO :new.MAXIMOAFSRID FROM dual;
    END IF;
  :new.CREATED_BY := nvl(v('APP_USER'),'NONE');
  :new.CREATED_TIMESTAMP := SYSTIMESTAMP;
  END;
/
ALTER TRIGGER "SPIDERS_DATA"."SPD_I_MAXIMO_AF_SRS_TMP" ENABLE;
