--------------------------------------------------------
--  DDL for Trigger SPD_I_LABOR_CATEGORIES
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE TRIGGER "SPIDERS_DATA"."SPD_I_LABOR_CATEGORIES" 
  BEFORE INSERT ON  spiders_data.SPD_T_LABOR_CATEGORIES FOR EACH ROW

  BEGIN
    IF :new.LABORCATEGORYID IS NULL THEN
      select spiders_data.spd_s_LABOR_CATEGORIES.nextval INTO :new.LABORCATEGORYID FROM dual;

    END IF;

      :new.CREATED_BY := nvl(v('APP_USER'),'NONE');
      :new.CREATED_TIMESTAMP := SYSTIMESTAMP;
  END;
/
ALTER TRIGGER "SPIDERS_DATA"."SPD_I_LABOR_CATEGORIES" ENABLE;
