--------------------------------------------------------
--  DDL for Trigger SPD_I_COMMENT_RESULTS
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE TRIGGER "SPIDERS_DATA"."SPD_I_COMMENT_RESULTS" 
   BEFORE INSERT ON spiders_data.SPD_T_COMMENT_RESULTS FOR EACH ROW
   BEGIN
    IF :new.COMMENTRESULTID IS NULL THEN
      select spiders_data.spd_s_COMMENT_RESULTS.nextval INTO :new.COMMENTRESULTID FROM dual;
    END IF;
  :new.CREATED_BY := nvl(v('APP_USER'),'NONE');
  :new.CREATED_TIMESTAMP := SYSTIMESTAMP;
  END;
/
ALTER TRIGGER "SPIDERS_DATA"."SPD_I_COMMENT_RESULTS" ENABLE;
