--------------------------------------------------------
--  DDL for Trigger SPD_I_REST_TO_TABLE_MAPS
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE TRIGGER "SPIDERS_DATA"."SPD_I_REST_TO_TABLE_MAPS" 
  BEFORE INSERT ON  spiders_data.SPD_T_REST_TO_TABLE_MAPS FOR EACH ROW

  BEGIN
    IF :new.RESTTOTABLEMAPID IS NULL THEN
      select spiders_data.spd_s_REST_TO_TABLE_MAPS.nextval INTO :new.RESTTOTABLEMAPID FROM dual;
    END IF;
    
    IF :new.USER_PERMISSION IS NULL THEN
      :new.USER_PERMISSION := 0;
    END IF;

    :new.CREATED_BY := nvl(v('APP_USER'),'NONE');
    :new.CREATED_TIMESTAMP := SYSTIMESTAMP;
  END;
/
ALTER TRIGGER "SPIDERS_DATA"."SPD_I_REST_TO_TABLE_MAPS" ENABLE;
