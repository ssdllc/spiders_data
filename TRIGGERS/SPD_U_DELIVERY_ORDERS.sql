--------------------------------------------------------
--  DDL for Trigger SPD_U_DELIVERY_ORDERS
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE TRIGGER "SPIDERS_DATA"."SPD_U_DELIVERY_ORDERS" 
BEFORE UPDATE ON spiders_data.SPD_T_DELIVERY_ORDERS FOR EACH ROW

BEGIN
  :new.UPDATED_BY := nvl(v('APP_USER'),'NONE');
  :new.UPDATED_TIMESTAMP := SYSTIMESTAMP;
END;
/
ALTER TRIGGER "SPIDERS_DATA"."SPD_U_DELIVERY_ORDERS" ENABLE;
