--------------------------------------------------------
--  DDL for Trigger SPD_I_DO_FAC_INSTRUCTIONS
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE TRIGGER "SPIDERS_DATA"."SPD_I_DO_FAC_INSTRUCTIONS" 
  BEFORE INSERT ON  spiders_data.SPD_T_DO_FAC_INSTRUCTIONS FOR EACH ROW

  BEGIN
    IF :new.DOFACINSTRUCTIONID IS NULL THEN
      select spiders_data.spd_s_DO_FAC_INSTRUCTIONS.nextval INTO :new.DOFACINSTRUCTIONID FROM dual;

    END IF;

      :new.CREATED_BY := nvl(v('APP_USER'),'NONE');
      :new.CREATED_TIMESTAMP := SYSTIMESTAMP;
  END;
/
ALTER TRIGGER "SPIDERS_DATA"."SPD_I_DO_FAC_INSTRUCTIONS" ENABLE;
