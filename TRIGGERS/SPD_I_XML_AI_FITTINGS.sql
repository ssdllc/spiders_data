--------------------------------------------------------
--  DDL for Trigger SPD_I_XML_AI_FITTINGS
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE TRIGGER "SPIDERS_DATA"."SPD_I_XML_AI_FITTINGS" 
   BEFORE INSERT ON SPD_T_XML_AI_FITTINGS FOR EACH ROW
   BEGIN
    IF :new.FITTINGID IS NULL THEN
      SELECT SPD_S_XML_AI_FITTINGS.nextval INTO :new.FITTINGID FROM dual;
    END IF;
  --:new.CREATED_BY := nvl(v('APP_USER'),'NONE');
  :new.CREATED_TIMESTAMP := SYSTIMESTAMP;
  END;
/
ALTER TRIGGER "SPIDERS_DATA"."SPD_I_XML_AI_FITTINGS" ENABLE;
