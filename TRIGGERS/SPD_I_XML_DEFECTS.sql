--------------------------------------------------------
--  DDL for Trigger SPD_I_XML_DEFECTS
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE TRIGGER "SPIDERS_DATA"."SPD_I_XML_DEFECTS" 
   BEFORE INSERT ON SPD_T_XML_DEFECTS FOR EACH ROW
   BEGIN
    IF :new.DEFECTID IS NULL THEN
      SELECT SPD_S_XML_DEFECTS.nextval INTO :new.DEFECTID FROM dual;
    END IF;
  --:new.CREATED_BY := nvl(v('APP_USER'),'NONE');
  :new.CREATED_TIMESTAMP := SYSTIMESTAMP;
  END;
/
ALTER TRIGGER "SPIDERS_DATA"."SPD_I_XML_DEFECTS" ENABLE;
