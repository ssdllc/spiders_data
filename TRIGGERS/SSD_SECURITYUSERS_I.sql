--------------------------------------------------------
--  DDL for Trigger SSD_SECURITYUSERS_I
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE TRIGGER "SPIDERS_DATA"."SSD_SECURITYUSERS_I" before insert on spiders_data.SSD_SECURITY_USERS for each row begin
     if inserting then
       if :NEW.SECURITYUSERID is null then

     select SSD_S_SECURITYUSERID.nextval into :NEW.SECURITYUSERID from dual;
       end if;    end if; end;
/
ALTER TRIGGER "SPIDERS_DATA"."SSD_SECURITYUSERS_I" ENABLE;
