--------------------------------------------------------
--  DDL for Trigger SPD_I_3D_SCENES
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE TRIGGER "SPIDERS_DATA"."SPD_I_3D_SCENES" 
   BEFORE INSERT ON SPD_T_3D_SCENES FOR EACH ROW
   BEGIN
    IF :new.SCENEID IS NULL THEN
      SELECT SPD_S_3D_SCENES.nextval INTO :new.SCENEID FROM dual;
    END IF;
  :new.CREATED_BY := nvl(v('APP_USER'),'NONE');
  :new.CREATED_TIMESTAMP := SYSTIMESTAMP;
  END;
/
ALTER TRIGGER "SPIDERS_DATA"."SPD_I_3D_SCENES" ENABLE;
