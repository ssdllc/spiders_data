--------------------------------------------------------
--  DDL for Trigger SPD_I_COLLECTION_ORDER
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE TRIGGER "SPIDERS_DATA"."SPD_I_COLLECTION_ORDER" 
  BEFORE INSERT ON spiders_data.SPD_T_COLLECTION_ORDERS FOR EACH ROW

  BEGIN
    IF :new.COLECTIONORDERID IS NULL THEN
      select spiders_data.spd_s_COLLECTION_ORDER.nextval INTO :new.COLECTIONORDERID FROM dual;
    END IF;
  END;
/
ALTER TRIGGER "SPIDERS_DATA"."SPD_I_COLLECTION_ORDER" ENABLE;
