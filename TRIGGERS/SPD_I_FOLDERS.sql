--------------------------------------------------------
--  DDL for Trigger SPD_I_FOLDERS
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE TRIGGER "SPIDERS_DATA"."SPD_I_FOLDERS" before insert on "SPIDERS_DATA"."SPD_T_FOLDERS"    for each row begin     if inserting then       if :NEW."FOLDERID" is null then          select spiders_data.spd_s_FOLDERS.nextval into :NEW."FOLDERID" from dual;       end if;    end if; end;
/
ALTER TRIGGER "SPIDERS_DATA"."SPD_I_FOLDERS" ENABLE;
