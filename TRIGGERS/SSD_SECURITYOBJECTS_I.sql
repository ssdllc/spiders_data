--------------------------------------------------------
--  DDL for Trigger SSD_SECURITYOBJECTS_I
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE TRIGGER "SPIDERS_DATA"."SSD_SECURITYOBJECTS_I" before insert on spiders_data.SSD_SECURITY_OBJECTS for each row begin
     if inserting then
       if :NEW.SECURITYOBJECTID is null then

     select SSD_S_SECURITYOBJECTID.nextval into :NEW.SECURITYOBJECTID from dual;
       end if;    end if; end;
/
ALTER TRIGGER "SPIDERS_DATA"."SSD_SECURITYOBJECTS_I" ENABLE;
