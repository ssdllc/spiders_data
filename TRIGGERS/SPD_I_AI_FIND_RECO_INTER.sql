--------------------------------------------------------
--  DDL for Trigger SPD_I_AI_FIND_RECO_INTER
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE TRIGGER "SPIDERS_DATA"."SPD_I_AI_FIND_RECO_INTER" before
  INSERT ON "SPIDERS_DATA"."SPD_T_AI_FIND_RECO_INTER"
  FOR EACH row
   BEGIN
    IF inserting THEN
     IF :NEW."AIFRIID" IS NULL THEN
      select spiders_data.spd_s_AI_FIND_RECO_INTER.nextval INTO :NEW."AIFRIID" FROM dual;
   END IF;
  END IF;
END;
/
ALTER TRIGGER "SPIDERS_DATA"."SPD_I_AI_FIND_RECO_INTER" ENABLE;
