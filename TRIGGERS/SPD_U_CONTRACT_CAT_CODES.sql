--------------------------------------------------------
--  DDL for Trigger SPD_U_CONTRACT_CAT_CODES
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE TRIGGER "SPIDERS_DATA"."SPD_U_CONTRACT_CAT_CODES" 
  BEFORE UPDATE ON SPD_T_contract_cat_codes FOR EACH ROW

  BEGIN
    :new.UPDATED_BY := nvl(v('APP_USER'),'NONE');
    :new.UPDATED_TIMESTAMP := SYSTIMESTAMP;
  END;
/
ALTER TRIGGER "SPIDERS_DATA"."SPD_U_CONTRACT_CAT_CODES" ENABLE;
