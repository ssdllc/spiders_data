--------------------------------------------------------
--  DDL for Trigger SPD_I_TELESTRATOR_SCENE_CONFIG
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE TRIGGER "SPIDERS_DATA"."SPD_I_TELESTRATOR_SCENE_CONFIG" before
  INSERT ON spiders_data.SPD_T_TELESTRATOR_SCENE_CONFIG FOR EACH row BEGIN IF :new.SCENECONFIGID IS NULL THEN
  select spiders_data.spd_s_telestrator_scene_config.nextval
  INTO :new.SCENECONFIGID
  FROM dual;
END IF;
END;
/
ALTER TRIGGER "SPIDERS_DATA"."SPD_I_TELESTRATOR_SCENE_CONFIG" ENABLE;
