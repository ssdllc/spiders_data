--------------------------------------------------------
--  DDL for Trigger SPD_I_CONTRACT_CAT_CODES
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE TRIGGER "SPIDERS_DATA"."SPD_I_CONTRACT_CAT_CODES" 
   BEFORE INSERT ON SPD_T_contract_cat_codes FOR EACH ROW
   BEGIN
    IF :new.contractcatcodeid IS NULL THEN
      SELECT SPD_S_contract_cat_codes.nextval INTO :new.contractcatcodeid FROM dual;
    END IF;
  :new.CREATED_BY := nvl(v('APP_USER'),'NONE');
  :new.CREATED_TIMESTAMP := SYSTIMESTAMP;
  END;
/
ALTER TRIGGER "SPIDERS_DATA"."SPD_I_CONTRACT_CAT_CODES" ENABLE;
