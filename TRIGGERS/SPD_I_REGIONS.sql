--------------------------------------------------------
--  DDL for Trigger SPD_I_REGIONS
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE TRIGGER "SPIDERS_DATA"."SPD_I_REGIONS" 
   BEFORE INSERT ON SPD_T_REGIONS FOR EACH ROW
   BEGIN
    IF :new.REGIONID IS NULL THEN
      SELECT SPD_S_REGIONS.nextval INTO :new.REGIONID FROM dual;
    END IF;
  :new.CREATED_BY := nvl(v('APP_USER'),'NONE');
  :new.CREATED_TIMESTAMP := SYSTIMESTAMP;
  END;
/
ALTER TRIGGER "SPIDERS_DATA"."SPD_I_REGIONS" ENABLE;
