--------------------------------------------------------
--  DDL for Trigger SPD_I_MAJOR_COMPONENTS
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE TRIGGER "SPIDERS_DATA"."SPD_I_MAJOR_COMPONENTS" 
   BEFORE INSERT ON spiders_data.SPD_T_MAJOR_COMPONENTS FOR EACH ROW
   BEGIN
    IF :new.MAJORCOMPONENTID IS NULL THEN
      select spiders_data.spd_s_MAJOR_COMPONENTS.nextval INTO :new.MAJORCOMPONENTID FROM dual;
    END IF;
  :new.CREATED_BY := nvl(v('APP_USER'),'NONE');
  :new.CREATED_TIMESTAMP := SYSTIMESTAMP;
  END;
/
ALTER TRIGGER "SPIDERS_DATA"."SPD_I_MAJOR_COMPONENTS" ENABLE;
