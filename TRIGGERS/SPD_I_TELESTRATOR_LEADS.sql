--------------------------------------------------------
--  DDL for Trigger SPD_I_TELESTRATOR_LEADS
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE TRIGGER "SPIDERS_DATA"."SPD_I_TELESTRATOR_LEADS" before
  INSERT ON SPIDERS_DATA.SPD_T_TELESTRATOR_LEADS FOR EACH row BEGIN IF :NEW.LEADID IS NULL THEN
  select spiders_data.spd_s_TELESTRATOR_LEADS.nextval INTO :NEW.LEADID FROM dual; END IF; END;
/
ALTER TRIGGER "SPIDERS_DATA"."SPD_I_TELESTRATOR_LEADS" ENABLE;
