--------------------------------------------------------
--  DDL for Trigger SPD_I_BFILES
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE TRIGGER "SPIDERS_DATA"."SPD_I_BFILES" 
   BEFORE INSERT ON SPD_T_BFILES FOR EACH ROW
   BEGIN
    IF :new.BFILEID IS NULL THEN
      SELECT SPD_S_BFILES.nextval INTO :new.BFILEID FROM dual;
    END IF;
  :new.CREATED_BY := nvl(v('APP_USER'),'NONE');
  :new.CREATED_TIMESTAMP := SYSTIMESTAMP;
  END;
/
ALTER TRIGGER "SPIDERS_DATA"."SPD_I_BFILES" ENABLE;
