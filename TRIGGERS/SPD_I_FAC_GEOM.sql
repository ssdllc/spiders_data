--------------------------------------------------------
--  DDL for Trigger SPD_I_FAC_GEOM
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE TRIGGER "SPIDERS_DATA"."SPD_I_FAC_GEOM" BEFORE INSERT ON spiders_data.spd_t_fac_geom
FOR EACH ROW
begin
         if :new.geomid  is null
         then select spiders_data.spd_s_fac_geom.nextval
            into :new.geomid
            from dual;
         end if;
end;
/
ALTER TRIGGER "SPIDERS_DATA"."SPD_I_FAC_GEOM" ENABLE;
