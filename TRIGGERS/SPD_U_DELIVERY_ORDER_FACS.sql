--------------------------------------------------------
--  DDL for Trigger SPD_U_DELIVERY_ORDER_FACS
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE TRIGGER "SPIDERS_DATA"."SPD_U_DELIVERY_ORDER_FACS" 
BEFORE UPDATE ON spiders_data.SPD_T_DELIVERY_ORDER_FACS FOR EACH ROW

BEGIN
  :new.UPDATED_BY := nvl(v('APP_USER'),'NONE');
  :new.UPDATED_TIMESTAMP := SYSTIMESTAMP;
END;
/
ALTER TRIGGER "SPIDERS_DATA"."SPD_U_DELIVERY_ORDER_FACS" ENABLE;
