--------------------------------------------------------
--  DDL for Trigger SPD_I_AI_DEFECTS
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE TRIGGER "SPIDERS_DATA"."SPD_I_AI_DEFECTS" BEFORE INSERT ON spiders_data.spd_t_ai_defects
FOR EACH ROW
begin
         if :new.aidefectid  is null
         then select spiders_data.spd_s_ai_defects.nextval
            into :new.aidefectid
            from dual;
         end if;
end;
/
ALTER TRIGGER "SPIDERS_DATA"."SPD_I_AI_DEFECTS" ENABLE;
