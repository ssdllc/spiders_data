--------------------------------------------------------
--  DDL for Trigger SPD_I_ACTION_PLANNER
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE TRIGGER "SPIDERS_DATA"."SPD_I_ACTION_PLANNER" 
  BEFORE INSERT ON spiders_data.spd_t_action_planner
  FOR EACH ROW
begin
         if :new.apid  is null
         then select spiders_data.spd_s_action_planner.nextval
            into :new.apid
            from dual;
         end if;
         if :new.FILESKEY is null
         then select spiders_data.spd_s_fileskey.nextval
            into :new.FILESKEY
            from dual;
         end if;
         if :new.POCKEY is null
         then select spiders_data.spd_s_pockey.nextval
            into :new.POCKEY
            from dual;
         end if;
end;
/
ALTER TRIGGER "SPIDERS_DATA"."SPD_I_ACTION_PLANNER" ENABLE;
