--------------------------------------------------------
--  DDL for Trigger SPD_U_SIA_QUESTIONS
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE TRIGGER "SPIDERS_DATA"."SPD_U_SIA_QUESTIONS" 
  BEFORE UPDATE ON SPD_T_SIA_QUESTIONS FOR EACH ROW

  BEGIN
    :new.UPDATED_BY := nvl(v('APP_USER'),'NONE');
    :new.UPDATED_TIMESTAMP := SYSTIMESTAMP;
  END;
/
ALTER TRIGGER "SPIDERS_DATA"."SPD_U_SIA_QUESTIONS" ENABLE;
