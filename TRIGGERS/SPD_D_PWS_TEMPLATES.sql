--------------------------------------------------------
--  DDL for Trigger SPD_D_PWS_TEMPLATES
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE TRIGGER "SPIDERS_DATA"."SPD_D_PWS_TEMPLATES" 
   BEFORE DELETE ON spiders_data.SPD_T_PWS_TEMPLATES
   FOR EACH ROW
  BEGIN
    DELETE FROM spiders_data.SPD_T_PWS_TEMPLATE_MAPS WHERE PWSTEMPLATEID = :old.PWSTEMPLATEID;
  END;
/
ALTER TRIGGER "SPIDERS_DATA"."SPD_D_PWS_TEMPLATES" ENABLE;
