--------------------------------------------------------
--  DDL for Trigger SPD_U_PLAN_ACTION_NOTES
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE TRIGGER "SPIDERS_DATA"."SPD_U_PLAN_ACTION_NOTES" 
  BEFORE UPDATE ON SPD_T_PLAN_ACTION_NOTES FOR EACH ROW 
  
  BEGIN 
    :new.UPDATED_BY := nvl(v('APP_USER'),'NONE');
    :new.UPDATED_TIMESTAMP := SYSTIMESTAMP;
  END;
/
ALTER TRIGGER "SPIDERS_DATA"."SPD_U_PLAN_ACTION_NOTES" ENABLE;
