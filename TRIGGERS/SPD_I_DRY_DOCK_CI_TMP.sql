--------------------------------------------------------
--  DDL for Trigger SPD_I_DRY_DOCK_CI_TMP
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE TRIGGER "SPIDERS_DATA"."SPD_I_DRY_DOCK_CI_TMP" 
BEFORE INSERT ON spiders_data.spd_t_dry_dock_ci_tmp FOR EACH ROW

BEGIN IF :new.DRYDOCKTMPCIID IS NULL THEN
  SELECT spiders_data.spd_s_dry_dock_ci_tmp.nextval INTO :new.DRYDOCKTMPCIID FROM dual;
END IF;

END;
/
ALTER TRIGGER "SPIDERS_DATA"."SPD_I_DRY_DOCK_CI_TMP" ENABLE;
