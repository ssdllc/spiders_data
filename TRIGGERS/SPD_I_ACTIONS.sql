--------------------------------------------------------
--  DDL for Trigger SPD_I_ACTIONS
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE TRIGGER "SPIDERS_DATA"."SPD_I_ACTIONS" 
   BEFORE INSERT ON SPD_T_ACTIONS FOR EACH ROW 
   BEGIN 
    IF :new.ACTIONID IS NULL THEN 
      SELECT SPD_S_ACTIONS.nextval INTO :new.ACTIONID FROM dual; 
    END IF; 
    IF :new.SPECIAL_AREA_CODE IS NULL THEN
      :new.SPECIAL_AREA_CODE := 'host';
    END IF;
  :new.CREATED_BY := nvl(v('APP_USER'),'NONE'); 
  :new.CREATED_TIMESTAMP := SYSTIMESTAMP; 
  END;
/
ALTER TRIGGER "SPIDERS_DATA"."SPD_I_ACTIONS" ENABLE;
