--------------------------------------------------------
--  DDL for Trigger SPD_I_LISTS_FOR_REVIEW
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE TRIGGER "SPIDERS_DATA"."SPD_I_LISTS_FOR_REVIEW" BEFORE INSERT ON spiders_data.spd_t_lists_for_review
FOR EACH ROW
begin
         if :new.listid  is null
         then select spiders_data.spd_s_lists_for_review.nextval
            into :new.listid
            from dual;
         end if;
end;
/
ALTER TRIGGER "SPIDERS_DATA"."SPD_I_LISTS_FOR_REVIEW" ENABLE;
