--------------------------------------------------------
--  DDL for Trigger SPD_I_DO_CE_FILES
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE TRIGGER "SPIDERS_DATA"."SPD_I_DO_CE_FILES" BEFORE INSERT ON spiders_data.spd_t_do_ce_files
FOR EACH ROW
begin
         if :new.cefilesid  is null
         then select spiders_data.spd_s_do_ce_files.nextval
            into :new.cefilesid
            from dual;
         end if;
         if :new.FILESKEY is null
         then select spiders_data.spd_s_fileskey.nextval
            into :new.FILESKEY
            from dual;
         end if;
end;
/
ALTER TRIGGER "SPIDERS_DATA"."SPD_I_DO_CE_FILES" ENABLE;
