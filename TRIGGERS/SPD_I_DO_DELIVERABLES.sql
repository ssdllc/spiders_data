--------------------------------------------------------
--  DDL for Trigger SPD_I_DO_DELIVERABLES
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE TRIGGER "SPIDERS_DATA"."SPD_I_DO_DELIVERABLES" BEFORE INSERT ON spiders_data.spd_t_do_deliverables
FOR EACH ROW
begin
         if :new.dodelgid  is null
         then select spiders_data.spd_s_do_deliverables.nextval
            into :new.dodelgid
            from dual;
         end if;
         if :new.FILESKEY is null
         then select spiders_data.spd_s_fileskey.nextval
            into :new.FILESKEY
            from dual;
         end if;
end;
/
ALTER TRIGGER "SPIDERS_DATA"."SPD_I_DO_DELIVERABLES" ENABLE;
