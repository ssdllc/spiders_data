--------------------------------------------------------
--  DDL for Trigger SPD_U_DIRECT_COSTS
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE TRIGGER "SPIDERS_DATA"."SPD_U_DIRECT_COSTS" 
BEFORE UPDATE ON spiders_data.SPD_T_DIRECT_COSTS FOR EACH ROW

BEGIN
  :new.UPDATED_BY := nvl(v('APP_USER'),'NONE');
  :new.UPDATED_TIMESTAMP := SYSTIMESTAMP;
END;
/
ALTER TRIGGER "SPIDERS_DATA"."SPD_U_DIRECT_COSTS" ENABLE;
