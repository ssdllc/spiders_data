--------------------------------------------------------
--  DDL for Trigger SPD_I_ATTRIBUTES
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE TRIGGER "SPIDERS_DATA"."SPD_I_ATTRIBUTES" 
   BEFORE INSERT ON spiders_data.SPD_T_ATTRIBUTES FOR EACH ROW
   BEGIN
    IF :new.ATTRIBUTEID IS NULL THEN
      select spiders_data.spd_s_ATTRIBUTES.nextval INTO :new.ATTRIBUTEID FROM dual;
    END IF;
  :new.CREATED_BY := nvl(v('APP_USER'),'NONE');
  :new.CREATED_TIMESTAMP := SYSTIMESTAMP;
  END;
/
ALTER TRIGGER "SPIDERS_DATA"."SPD_I_ATTRIBUTES" ENABLE;
