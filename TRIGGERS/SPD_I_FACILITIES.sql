--------------------------------------------------------
--  DDL for Trigger SPD_I_FACILITIES
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE TRIGGER "SPIDERS_DATA"."SPD_I_FACILITIES" BEFORE INSERT ON spiders_data.spd_t_facilities
FOR EACH ROW
begin
         if :new.facid  is null
         then select spiders_data.spd_s_facilities.nextval
            into :new.facid
            from dual;
         end if;

         if :new.fileskey  is null
         then select spiders_data.spd_s_fileskey.nextval
            into :new.fileskey
            from dual;
         end if;
end;
/
ALTER TRIGGER "SPIDERS_DATA"."SPD_I_FACILITIES" ENABLE;
