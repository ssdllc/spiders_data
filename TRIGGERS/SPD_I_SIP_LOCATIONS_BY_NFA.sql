--------------------------------------------------------
--  DDL for Trigger SPD_I_SIP_LOCATIONS_BY_NFA
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE TRIGGER "SPIDERS_DATA"."SPD_I_SIP_LOCATIONS_BY_NFA" 
   BEFORE INSERT ON SPD_T_SIP_LOCATIONS_BY_NFA FOR EACH ROW
   BEGIN
    IF :new.SIPLOCATIONNFAID IS NULL THEN
      SELECT SPD_S_SIP_LOCATIONS_BY_NFA.nextval INTO :new.SIPLOCATIONNFAID FROM dual;
    END IF;
  :new.CREATED_BY := nvl(v('APP_USER'),'NONE');
  :new.CREATED_TIMESTAMP := SYSTIMESTAMP;
  END;
/
ALTER TRIGGER "SPIDERS_DATA"."SPD_I_SIP_LOCATIONS_BY_NFA" ENABLE;
