--------------------------------------------------------
--  DDL for Trigger SPD_U_MAXIMO_ASSETS
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE TRIGGER "SPIDERS_DATA"."SPD_U_MAXIMO_ASSETS" 
  BEFORE UPDATE ON SPD_T_MAXIMO_ASSETS FOR EACH ROW

  BEGIN
    :new.UPDATED_BY := nvl(v('APP_USER'),'NONE');
    :new.UPDATED_TIMESTAMP := SYSTIMESTAMP;
  END;
/
ALTER TRIGGER "SPIDERS_DATA"."SPD_U_MAXIMO_ASSETS" ENABLE;
