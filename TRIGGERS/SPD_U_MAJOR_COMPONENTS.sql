--------------------------------------------------------
--  DDL for Trigger SPD_U_MAJOR_COMPONENTS
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE TRIGGER "SPIDERS_DATA"."SPD_U_MAJOR_COMPONENTS" 
  BEFORE UPDATE ON spiders_data.SPD_T_MAJOR_COMPONENTS FOR EACH ROW

  BEGIN
    :new.UPDATED_BY := nvl(v('APP_USER'),'NONE');
    :new.UPDATED_TIMESTAMP := SYSTIMESTAMP;
  END;
/
ALTER TRIGGER "SPIDERS_DATA"."SPD_U_MAJOR_COMPONENTS" ENABLE;
