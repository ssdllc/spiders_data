--------------------------------------------------------
--  DDL for Trigger SPD_I_AI_FIND_REC
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE TRIGGER "SPIDERS_DATA"."SPD_I_AI_FIND_REC" BEFORE INSERT ON spiders_data.spd_t_ai_find_rec
FOR EACH ROW
begin
         if :new.aifrid  is null
         then select spiders_data.spd_s_ai_find_rec.nextval
            into :new.aifrid
            from dual;
         end if;
end;
/
ALTER TRIGGER "SPIDERS_DATA"."SPD_I_AI_FIND_REC" ENABLE;
