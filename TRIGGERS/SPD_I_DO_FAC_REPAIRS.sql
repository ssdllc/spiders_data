--------------------------------------------------------
--  DDL for Trigger SPD_I_DO_FAC_REPAIRS
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE TRIGGER "SPIDERS_DATA"."SPD_I_DO_FAC_REPAIRS" 
   BEFORE INSERT ON spiders_data.SPD_T_DO_FAC_REPAIRS FOR EACH ROW
   BEGIN
    IF :new.DOFACREPAIRID IS NULL THEN
      select spiders_data.spd_s_DO_FAC_REPAIRS.nextval INTO :new.DOFACREPAIRID FROM dual;
    END IF;
  :new.CREATED_BY := nvl(v('APP_USER'),'NONE');
  :new.CREATED_TIMESTAMP := SYSTIMESTAMP;
  END;
/
ALTER TRIGGER "SPIDERS_DATA"."SPD_I_DO_FAC_REPAIRS" ENABLE;
