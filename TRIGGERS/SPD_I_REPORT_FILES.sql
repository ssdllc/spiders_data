--------------------------------------------------------
--  DDL for Trigger SPD_I_REPORT_FILES
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE TRIGGER "SPIDERS_DATA"."SPD_I_REPORT_FILES" 
   before insert on "SPIDERS_DATA"."SPD_T_REPORT_FILES"
   for each row
begin
   if inserting then
      if :NEW."REPORTFILEID" is null then
         select spiders_data.spd_s_REPORT_FILES.nextval into :NEW."REPORTFILEID" from dual;
      end if;
   end if;
end;
/
ALTER TRIGGER "SPIDERS_DATA"."SPD_I_REPORT_FILES" ENABLE;
