--------------------------------------------------------
--  DDL for Trigger SPD_U_XML_DEFECTS
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE TRIGGER "SPIDERS_DATA"."SPD_U_XML_DEFECTS" 
  BEFORE UPDATE ON SPD_T_XML_DEFECTS FOR EACH ROW

  BEGIN
    :new.UPDATED_BY := nvl(v('APP_USER'),'NONE');
    :new.UPDATED_TIMESTAMP := SYSTIMESTAMP;
  END;
/
ALTER TRIGGER "SPIDERS_DATA"."SPD_U_XML_DEFECTS" ENABLE;
