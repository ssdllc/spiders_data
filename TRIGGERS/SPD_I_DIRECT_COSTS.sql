--------------------------------------------------------
--  DDL for Trigger SPD_I_DIRECT_COSTS
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE TRIGGER "SPIDERS_DATA"."SPD_I_DIRECT_COSTS" 
  BEFORE INSERT ON  spiders_data.SPD_T_DIRECT_COSTS FOR EACH ROW

  BEGIN
    IF :new.DIRECTCOSTID IS NULL THEN
      select spiders_data.spd_s_DIRECT_COSTS.nextval INTO :new.DIRECTCOSTID FROM dual;

    END IF;

      :new.CREATED_BY := nvl(v('APP_USER'),'NONE');
      :new.CREATED_TIMESTAMP := SYSTIMESTAMP;
  END;
/
ALTER TRIGGER "SPIDERS_DATA"."SPD_I_DIRECT_COSTS" ENABLE;
