--------------------------------------------------------
--  DDL for Trigger SPD_I_PLANS
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE TRIGGER "SPIDERS_DATA"."SPD_I_PLANS" 
   BEFORE INSERT ON SPD_T_PLANS FOR EACH ROW 
   BEGIN 
    IF :new.PLANID IS NULL THEN 
      SELECT SPD_S_PLANS.nextval INTO :new.PLANID FROM dual; 
    END IF; 
  :new.CREATED_BY := nvl(v('APP_USER'),'NONE'); 
  :new.CREATED_TIMESTAMP := SYSTIMESTAMP; 
  END;
/
ALTER TRIGGER "SPIDERS_DATA"."SPD_I_PLANS" ENABLE;
