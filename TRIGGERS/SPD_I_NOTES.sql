--------------------------------------------------------
--  DDL for Trigger SPD_I_NOTES
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE TRIGGER "SPIDERS_DATA"."SPD_I_NOTES" BEFORE INSERT ON spiders_data.spd_t_notes
FOR EACH ROW
begin
         if :new.noteid  is null
         then select spiders_data.spd_s_notes.nextval
            into :new.noteid
            from dual;
         end if;


         if :new.timestamp is null
         then :new.timestamp := sysdate;
         end if;
end;
/
ALTER TRIGGER "SPIDERS_DATA"."SPD_I_NOTES" ENABLE;
