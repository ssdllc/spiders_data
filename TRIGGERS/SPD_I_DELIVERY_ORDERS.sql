--------------------------------------------------------
--  DDL for Trigger SPD_I_DELIVERY_ORDERS
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE TRIGGER "SPIDERS_DATA"."SPD_I_DELIVERY_ORDERS" 
  BEFORE INSERT ON  SPIDERS_DATA.SPD_T_DELIVERY_ORDERS FOR EACH ROW

  declare

  lc_nADOID number;

  BEGIN
    IF :new.DELIVERYORDERID IS NULL THEN
      select spiders_data.spd_s_DELIVERY_ORDERS.nextval INTO :new.DELIVERYORDERID FROM dual;

    END IF;

    IF :new.ADOID IS NULL THEN
      select spiders_data.spd_s_ACTN_DOS.nextval INTO lc_nADOID FROM dual;
      :new.ADOID := lc_nADOID;
      insert into SPIDERS_DATA.spd_t_actn_dos (ADOID) values (lc_nADOID);


    END IF;


      :new.CREATED_BY := nvl(v('APP_USER'),'NONE');
      :new.CREATED_TIMESTAMP := SYSTIMESTAMP;
  END;
/
ALTER TRIGGER "SPIDERS_DATA"."SPD_I_DELIVERY_ORDERS" ENABLE;
