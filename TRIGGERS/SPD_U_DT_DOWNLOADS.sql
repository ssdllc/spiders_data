--------------------------------------------------------
--  DDL for Trigger SPD_U_DT_DOWNLOADS
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE TRIGGER "SPIDERS_DATA"."SPD_U_DT_DOWNLOADS" 
  BEFORE UPDATE ON SPD_T_DT_DOWNLOADS FOR EACH ROW

  BEGIN
    :new.UPDATED_BY := nvl(v('APP_USER'),'NONE');
    :new.UPDATED_TIMESTAMP := SYSTIMESTAMP;
  END;
/
ALTER TRIGGER "SPIDERS_DATA"."SPD_U_DT_DOWNLOADS" ENABLE;
