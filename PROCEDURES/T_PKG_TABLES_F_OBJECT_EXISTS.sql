--------------------------------------------------------
--  DDL for Procedure T_PKG_TABLES_F_OBJECT_EXISTS
--------------------------------------------------------
set define off;

  CREATE OR REPLACE EDITIONABLE PROCEDURE "SPIDERS_DATA"."T_PKG_TABLES_F_OBJECT_EXISTS" 
declare
  lc_vSql long;
  lc_Return VARCHAR2;
  lc_vAssert   long;
  lc_nAssert   number;
  lc_dAssert   date;
  lc_jAssert   json;
  lc_jlAssert  json_list;
  lc_jvAssert  json_value;
  lc_cAssert   clob;
  lc_bAssert   blob;
  lc_xAssert   xmltype;
lc_vobjectname VARCHAR2;
begin
  --setup
  lc_vAssert := '';
  lc_nAssert := -1;
  lc_dAssert :=to_date('01-JAN-0001');
  lc_jAssert := json();
  lc_jlAssert:= json_list();
  lc_jvAssert:= json_value();
  lc_cAssert := '';
  lc_bAssert := empty_blob();
  lc_xAssert := xmltype('');
lc_vobjectname:= 'SPD_I_LISTS';
lc_vSql := 'begin :x := PKG_TABLES.F_OBJECT_EXISTS(IN_VOBJECTNAME => :lc_vobjectname); end;'
execute immediate lc_vSql using out lc_Return, lc_vobjectname;
--run test
lc_vAssert :='1';
ut_common.assertEquals(lc_Return,lc_vAssert);
ut_common.pass('UT_F_OBJECT_EXISTS_1');
--teardown
exception when others then ut_common.fail('UT_F_OBJECT_EXISTS_1');
end;
 

/
