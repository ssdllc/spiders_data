--------------------------------------------------------
--  DDL for Procedure BB_P_XML_FACILITIES_MATCH
--------------------------------------------------------
set define off;

  CREATE OR REPLACE EDITIONABLE PROCEDURE "SPIDERS_DATA"."BB_P_XML_FACILITIES_MATCH" 
(
  in_nADOID number, 
  in_vFacilityName varchar2 default 'NoName', 
  in_vNFAID varchar2 default 'NONFA', 
  in_vFacilityNo varchar2 default 'NoFacNumber', 
  in_vSpidersFacid varchar2 default ''
) 
as

--JSON stuff
lc_jJson json;
lc_jMessages json;
lc_jResults json;
lc_jlMessages json_list;
lc_vReturn varchar2(1);
lc_vResult varchar2(200);
lc_jvValue json_value;

--error stuff
lc_vErrorText varchar2(2000);
lc_jError json;
json_error EXCEPTION;

--procedure specific stuff
lc_nProductlineListId number := 100085;--THIS IS HARDCODED, NEEDS TO CHANGE AS SOON AS WE IMPORT MORE TYPES OF XML
lc_nDOFID number :=0;
lc_nContinue number;
lc_nFACID number;
lc_vInput varchar2(4000) := 'in_nADOID: ' ||  in_nADOID || ', in_vFacilityName: ' || in_vFacilityName || ',in_vNFAID: ' || in_vNFAID || ',in_vFacilityNo: ' || in_vFacilityNo || ',in_vSpidersFacid: ' ||in_vSpidersFacid; 


procedure p (in_vInput varchar2, in_vMessage varchar2 default '') as

begin

insert into debug (functionname, input, message) values ('BB_P_XML_FACILITIES_MATCH', in_vInput, in_vMessage);


end;


begin

--initialize the json list
lc_jResults := json();
lc_jMessages := json();
lc_jlMessages := json_list();


--Setup the initial JSON with the parameter values
lc_jJson := json();
lc_jJson.put('ADOID',IN_nADOID);
lc_jJson.put('NFAID',in_vNFAID);
lc_jJson.put('PRODUCTLINE_LISTID',lc_nProductlineListId);
lc_jJson.put('FACILITY_NAME',in_vFacilityName);
lc_jJson.put('FACILITY_NO',in_vFacilityNo);




p('lc_jJson',json_printer.pretty_print(lc_jJson));




--Run XML ADD DO FACILITY to add the facility to the delivery order facilities list
--included in this sub program is the magic ability to add facilities to the spd_t_facilities table
--also included is the ability to add the activity associated with the facility
lc_vResult := BB_F_ADD_DO_FACILITY(lc_jJson, lc_jResults, lc_jMessages);
if lc_vResult = 'y' then
  --if the dml was good, we set the  dofid and facid that are returned
  
  p('lc_jResults',json_printer.pretty_print(lc_jResults));
  if(lc_jResults.exist('DOFID')) then
    lc_jvValue := lc_jResults.get('DOFID');
    lc_nDOFID := getVal(lc_jvValue);
    p('lc_nDOFID',lc_nDOFID);


    
      if(lc_jResults.exist('FACID')) then
        lc_jvValue := lc_jResults.get('FACID');
        lc_nFACID := getVal(lc_jvValue);
        p('lc_nFACID',lc_nFACID);
        
        --put the dofid and facid into the json
        lc_jJson.remove('FACID');
        lc_jJson.put('FACID',lc_nFACID);
        lc_jJson.put('DOFID',lc_nDOFID);
        
        --run insert fac into xml facilities, this will take care of matching as if everything matches up on NFA
        lc_vResult := BB_F_XML_INSERT_FACILITIES(lc_jJson, lc_jResults, lc_jMessages);
        p('BB_F_XML_INSERT_FACILITIES lc_jJson',json_printer.pretty_print(lc_jJson));
        p('BB_F_XML_INSERT_FACILITIES lc_vResult',lc_vResult);
        if lc_vResult = 'y' then
          lc_nContinue := 1;
        else
          --if the dml does not return 'y' then something went wrong and we need to raise an exception
          lc_vErrorText := 'XML_INSERT_FACILITIES_MATCH did not return y';
          lc_nContinue :=0;
          lc_jlMessages.append('error: ''Error XMLIFFM-0001 ' || lc_vErrorText);
        end if;
      else
        lc_vErrorText := 'FACID doesnt exist';
        lc_nContinue :=0;
        lc_jlMessages.append('error: ''Error XMLIFFM-0001 ' || lc_vErrorText);
      end if;
  else
    lc_vErrorText := 'DOFID doesnt exist';
    lc_nContinue :=0;
    lc_jlMessages.append('error: ''Error XMLIFFM-0001 ' || lc_vErrorText);
  end if;
else
  lc_vErrorText := 'XML_ADD_DO_FACILITY did not return y';
  lc_nContinue :=0;
  lc_jlMessages.append('error: ''Error XMLIFFM-0001 ' || lc_vErrorText);
end if;


lc_jMessages.put('messages',lc_jlMessages);


--if there are any messages this is not valid return false
if (lc_jlMessages.count > 0) then
    lc_vReturn := 'n';
    lc_jMessages.put('status','error');
    raise json_error;
else
    lc_vReturn := 'y';
    lc_jMessages.put('status','success');
end if;


exception     
  when json_error then
      execute immediate 'insert into debug (functionname, input, message) values (''BB_P_XML_FACILITIES_MATCH'',''' || lc_vInput || ''',''error: Custom Errors'')';
  when others then
      execute immediate 'insert into debug (functionname, input, message) values (''BB_P_XML_FACILITIES_MATCH'',''' || lc_vInput || ''',''error: ' || sqlerrm || ''')';



end BB_P_XML_FACILITIES_MATCH;
 

/
