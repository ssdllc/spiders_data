--------------------------------------------------------
--  DDL for Procedure BB_P_CE_XML_OUTPUT
--------------------------------------------------------
set define off;

  CREATE OR REPLACE EDITIONABLE PROCEDURE "SPIDERS_DATA"."BB_P_CE_XML_OUTPUT" (IN_ADOID number)

is
lc_nFacCount number :=0;
lc_nSpecInstCount number :=0;
lc_cDefaultsFacLaborCat clob :=' ';
lc_cDefaultsJobTitles clob := '';
lc_cDefaultsFacDc clob := ' ';
lc_vUICS varchar2(2000);
lc_vLocation varchar2(2000);
lc_cClob clob :='<CE>';
lc_bBlob blob;
lc_nXMLFileid number :=0;
lc_vFilename varchar2(2000);



  v_blob_offset NUMBER :=1;
  v_clob_offset NUMBER :=1;
  v_lang_context NUMBER := DBMS_LOB.DEFAULT_LANG_CTX;
  v_warning NUMBER;
  v_string_length NUMBER(10);
  v_source_locator BLOB;
  v_destination_locator BLOB;
  v_amount PLS_INTEGER;
  v_string CLOB;
begin


for rec in (
              select
                infads.activity_uic,
                max(actv.unit_title) title
              from
                spd_mv_inf_activity actv,
                spd_mv_inf_facility infads,
                spd_t_facilities fac,
                spd_t_do_facilities dofacs
              where
                dofacs.adoid = in_adoid
                and dofacs.facid = fac.facid
                and fac.infads_facility_id = infads.facility_id
                and infads.activity_uic = actv.uic
              group by
                infads.activity_uic
            )
loop
if lc_nFacCount = 0 then
  lc_vLocation := rec.title;
  lc_vUICS := rec.activity_uic;
else
  lc_vLocation := lc_vLocation || ',' || rec.title;
  lc_vUICS := lc_vUICS || ', ' || rec.activity_uic;
end if;
lc_nFacCount := 1;
end loop;

<<ACTNLOOP>>
--for actn in ce.myCe.actions.first .. ce.myCe.actions.last loop
/*!!!!!MUST MAKE THIS DYNAMIC!!!!!!*/

--get all of the activity uics from the scoped facilities:


lc_nFacCount:=0;

for rec in (
            select
              planner.productline_listid productline,
              planner.planned_year fy,
              do.contract_number,
              do.delivery_order_number,
              do.adoid,
              do.start_date,
              do.contract_listid,
              do.worktype_listid,
              do.delivery_order_name
            from
              spd_t_actn_dos do,
              spd_t_action_planner planner
            where
              do.apid = planner.apid
              and do.adoid = IN_ADOID
            )
loop
--dbms_lob.append(lc_cClob,'<CE>');
dbms_lob.append(lc_cClob, '<Action Program="' || rec.productline || '"
EIC=""
Command="NFESC"
Date="' || to_char(sysdate,'DD-MON-YYYY') || '"
blankrow=""
ContractNumber="' || rec.contract_number || '"
DeliveryOrderNumber="' || rec.delivery_order_number || '"
ACTIONSID="' || rec.adoid || '"
Activity="' || lc_vUICS || '"
Location="' || lc_vLocation || '"
FY="' || rec.fy || '"
ProjectedStartDate="' || rec.start_date || '"
WorkType="' || rec.delivery_order_name || '"
WorkTypeID="' || rec.worktype_listid || '">');

		dbms_lob.append(lc_cClob, '<baselinedirectcost>0</baselinedirectcost>');
		dbms_lob.append(lc_cClob, '<baselinelaborcost>0</baselinelaborcost>');
		dbms_lob.append(lc_cClob, '<overheadrate>0</overheadrate>');
		dbms_lob.append(lc_cClob, '<profitrate>0</profitrate>');
		dbms_lob.append(lc_cClob, '<baselinetotalcost>0</baselinetotalcost>');
		dbms_lob.append(lc_cClob, '<gcedirectcost>0</gcedirectcost>');
		dbms_lob.append(lc_cClob, '<gcelaborcost>0</gcelaborcost>');
		dbms_lob.append(lc_cClob, '<gcetotalcost>0</gcetotalcost>');
		dbms_lob.append(lc_cClob, '<factargetlaborcost>0</factargetlaborcost>');
		dbms_lob.append(lc_cClob, '<facbaselabortotal>0</facbaselabortotal>');
		dbms_lob.append(lc_cClob, '<facfieldworktotal>0</facfieldworktotal>');
		dbms_lob.append(lc_cClob, '<adjustmentrate>0</adjustmentrate>');

	<<FACLOOP>>
	--for fac in ce.myCe.facilities.first .. ce.myCe.facilities.last loop
    for fac in (
                  select
                    dofacs.dofid,
                    dofacs.facid,
                    facil.spd_fac_name,
                    facil.spd_fac_no,
                    facil.infads_facility_id
                  from
                    spd_t_do_facilities dofacs,
                    spd_t_facilities facil
                  where
                    dofacs.adoid = in_adoid and
                    dofacs.facid = facil.facid
                )
    loop
		dbms_lob.append(lc_cClob, '<Facility
				id="' || to_char(fac.dofid) || '"
				facilitiesid="' || to_char(fac.dofid) || '"
				activitiesid="0"
				GPS="0"
				priority="0"
				CI="0"
				MDI="0"
				AGE="0"
				LAST_INSPECTED="0"
				OSD="0"
				facilityName="' || to_char(fac.spd_fac_name) || '"
				facilityNo="' || to_char(fac.spd_fac_no) || '"
				nfa="' || to_char(fac.infads_facility_id) || '"
				>');


		dbms_lob.append(lc_cClob, '<FacilityDescription>');
			/*
			!!!!we need to magically get these values!!!!!!
			(
				ce_facilityextended_rec('Length','Length', rec3.length,1),
				ce_facilityextended_rec('Piles','Piles',rec3.PILECOUNT,1),
				ce_facilityextended_rec('maxwaterdepth', 'Max Water Depth',rec3.waterdepth,1),
				ce_facilityextended_rec('yearbuilt', 'Year Built',rec3.yearbuilt,1)
			);
			*/
		--<<FAC_EXT_LOOP>>
		--for facext in ce.myCe.facilities(fac).facilityextended.first .. ce.myCe.facilities(fac).facilityextended.last loop
		--dbms_lob.append(lc_cClob, '<' || to_char(ce.myCe.facilities(fac).facilityextended(facext).fieldname) || ' displayname="' || to_char(ce.myCe.facilities(fac).facilityextended(facext).displayname) || '">' || to_char(ce.myCe.facilities(fac).facilityextended(facext).displayvalue) || '</' || to_char(ce.myCe.facilities(fac).facilityextended(facext).fieldname) || '>');
		dbms_lob.append(lc_cClob, '<Length displayname="Length">0</Length>');
		dbms_lob.append(lc_cClob, '<Piles displayname="Piles">0</Piles>');
		dbms_lob.append(lc_cClob, '<maxwaterdepth displayname="Max Water Depth">0</maxwaterdepth>');
		dbms_lob.append(lc_cClob, '<yearbuilt displayname="Year Built">0</yearbuilt>');
		--end loop FAC_EXT_LOOP;
		dbms_lob.append(lc_cClob, '</FacilityDescription>');









		dbms_lob.append(lc_cClob, '<specialinstructions>');
		<<FAC_SPEC_LOOP>>

    --htp.p('facid: ' || to_char(fac.dofid));
		--for spec in ce.myCe.facilities(fac).specinstr.first .. ce.myCe.facilities(fac).specinstr.last loop
		for spec in (
                  select
                    si.*,
                    list.*,
                    listvals.displayvalue val_display
                  from
                    spd_t_do_fac_spec_inst si,
                    spd_t_lists list,
                    spd_t_lists listvals
                  where
                    dofid = fac.dofid
                    and si.spec_instr_listid = list.listid
                    and si.spec_instr_value = listvals.displayvalue(+)
                )
    loop
			dbms_lob.append(lc_cClob, '<specinstr id="' || to_char(spec.afsiid) || '"
				  displayname="' || spec.displayvalue || '"
				  adjustment="0"
				  instruction="' || spec.val_display || '"
				  />');
		end loop FAC_SPEC_LOOP;
		dbms_lob.append(lc_cClob, '</specialinstructions>');









		dbms_lob.append(lc_cClob, '<laborcategories>');
		<<FAC_LC_LOOP>>
		--for lc in ce.myCe.facilities(fac).lc.first .. ce.myCe.facilities(fac).lc.last loop
		for lc in (
					select
						*
					from
						spd_t_lists
					where
						productline_listid = rec.productline and
						listname = 'list_facility_labor_categories' and
						grouping = rec.worktype_listid
					order by
						visualorder
				)
        loop
			--we build the default based on the facility, so just make a clob and use that later in the procedure instead of relooping
			lc_cDefaultsFacLaborCat := lc_cDefaultsFacLaborCat || '<lc id="' || to_char(lc.listid) || '"
					laborcategoriesid="' || to_char(lc.listid) || '"
					productline="' || to_char(rec.productline) || '"
					lctype="' || to_char(lc.listname) || '"
					lcname="' || to_char(lc.displayvalue) || '"
					baselinetotal="0"
					gcelinetotal="0"
					/>';
			dbms_lob.append(lc_cClob, '<lc id="' || to_char(lc.listid) || '"
					laborcategoriesid="' || to_char(lc.listid) || '"
					productline="' || to_char(rec.productline) || '"
					lctype="' || to_char(lc.listname) || '"
					lcname="' || to_char(lc.displayvalue) || '"
					baselinetotal="0"
					gcelinetotal="0"
					>');



---Add the new grouping layer per contractor here:
      <<FAC_LC_CONTRACTORS>>
      for contractor in (
        select
          contractor_listid,
          (select displayvalue from spd_t_lists where listid = contractor_listid) displayvalue
        from
          spd_t_contractors_per_contract
        where
          contract_listid = rec.contract_listid



      )
      loop




			dbms_lob.append(lc_cClob,'<jobtitles id="' || contractor.contractor_listid || '" KTR="' || contractor.displayvalue || '">'); --add id and ktr attribute

			<<FAC_LC_JT_LOOP>>
			lc_cDefaultsJobTitles := '';
			for jt in (
							select
                list.*
              from
                spd_t_lists list
              where
                list.productline_listid = 100085 and
                list.listname = 'list_job_titles'
                and list.grouping = contractor.contractor_listid
              order by
                list.listid,
                list.visualorder
						)
			loop
                lc_cDefaultsJobTitles := lc_cDefaultsJobTitles || '<jobtitle id="' || to_char(jt.listid) || '"
                    jobtitlesid="' || to_char(jt.listid) || '"
                    rate="0"
                    jtname="' || to_char(jt.displayvalue)  || '"
                    productline="' || to_char(rec.productline) || '"
                    estunitsrate="0"
                    baselineunits="0"
                    levelofeffort="0"
                    baselinetotal="0"
                    gcelinetotal="0"
                    gcelineunits="0"
                    adjustmentrate="0"
                    useredited="0"
                    />';

				dbms_lob.append(lc_cClob, '<jobtitle id="' || to_char(jt.listid) || '"
						jobtitlesid="' || to_char(jt.listid) || '"
						rate="0"
						jtname="' || to_char(jt.displayvalue)  || '"
						productline="' || to_char(rec.productline) || '"
						estunitsrate="0"
						baselineunits="0"
						levelofeffort="0"
						baselinetotal="0"
						gcelinetotal="0"
						gcelineunits="0"
						adjustmentrate="0"
						useredited="0"
						/>');
			end loop FAC_LC_JT_LOOP;
			dbms_lob.append(lc_cClob, '</jobtitles>');
    end loop FAC_LC_CONTRACTORS;
			dbms_lob.append(lc_cClob, '</lc>');
		end loop FAC_LC_LOOP;
		dbms_lob.append(lc_cClob, '</laborcategories>');








		dbms_lob.append(lc_cClob, '<directcosts>');
		<<FAC_DC_LOOP>>
		lc_cDefaultsFacDc := ' ';
		--for dc in ce.myCe.facilities(fac).dc.first .. ce.myCe.facilities(fac).dc.last loop
		for dc in (
				select
				  *
				from
				  spd_t_do_facility_ce_dc dc,
				  spd_t_lists list
				where
				  dc.directcost_listid = list.listid and
				  dc.dofid = fac.dofid
				order by
				  dc.afceid
		)
		loop
			lc_cDefaultsFacDc := lc_cDefaultsFacDc || '<dc id="' || to_char(dc.afceid) || '"
					directcostsid="' || to_char(dc.listid) || '"
					rate="0"
					dcname="' || to_char(dc.displayvalue) || '"
					productline="' || to_char(rec.productline) || '"
					estunitsrate="0"
					baselineunits="0"
					baselinetotal="0"
					gcelinetotal="0"
					gcelineunits="0"
					useredited="0"
					/>';

			dbms_lob.append(lc_cClob, '<dc id="' || to_char(dc.afceid) || '"
					directcostsid="' || to_char(dc.listid) || '"
					rate="0"
					dcname="' || to_char(dc.displayvalue) || '"
					productline="' || to_char(rec.productline) || '"
					estunitsrate="0"
					baselineunits="0"
					baselinetotal="0"
					gcelinetotal="0"
					gcelineunits="0"
					useredited="0"
					/>');
		end loop FAC_DC_LOOP;
		dbms_lob.append(lc_cClob, '</directcosts>');
		dbms_lob.append(lc_cClob, '</Facility>');
	end loop FACLOOP;


--<<ACTVLOOP>>
	dbms_lob.append(lc_cClob, '<Activity name="' || lc_vLocation || '">');
	dbms_lob.append(lc_cClob, '<ActivitiesID>' || lc_vUICS || '</ActivitiesID>');
	dbms_lob.append(lc_cClob, '<GPS>0</GPS>');
	dbms_lob.append(lc_cClob, '</Activity>');

	dbms_lob.append(lc_cClob,'<defaults>');
	dbms_lob.append(lc_cClob,'<laborcategories>');
	dbms_lob.append(lc_cClob,'<facility>');

htp.p(dbms_lob.getlength(lc_cClob));

htp.p('eh>'||lc_cDefaultsFacLaborCat);
	dbms_lob.append(lc_cClob,lc_cDefaultsFacLaborCat);

	dbms_lob.append(lc_cClob,'</facility>');
	dbms_lob.append(lc_cClob,'<activity>');


	<<DEFAULTS_LC_ACTV>>
	--for lc in ce.myCe.laborcatactv.first .. ce.myCe.laborcatactv.last loop
	for lc in (
				select
				  *
				from
				  spd_t_lists
				where
				  productline_listid = rec.productline and
				  listname = 'list_delivery_order_labor_categories' and
				  grouping = rec.worktype_listid
				order by
				  visualorder
				)
	loop

		dbms_lob.append(lc_cClob, '<lc id="' || to_char(lc.listid) || '"
				laborcategoriesid="' || to_char(lc.listid) || '"
				productline="' || to_char(rec.productline) || '"
				lctype="' || to_char(lc.listname) || '"
				lcname="' || to_char(lc.displayvalue) || '"
				baselinetotal="0"
				gcelinetotal="0"
				/>');
	end loop DEFAULTS_LC_ACTV;


  dbms_lob.append(lc_cClob,'</activity>');
	dbms_lob.append(lc_cClob,'</laborcategories>');

	--dbms_lob.append(lc_cClob,lc_cDefaultsJobTitles);
	---Add the new grouping layer per contractor here:
      <<FAC_LC_CONTRACTORS>>
      for contractor in (
        select
          contractor_listid,
          (select displayvalue from spd_t_lists where listid = contractor_listid) displayvalue
        from
          spd_t_contractors_per_contract
        where
          contract_listid = rec.contract_listid



      )
      loop




			dbms_lob.append(lc_cClob,'<jobtitles id="' || contractor.contractor_listid || '" KTR="' || contractor.displayvalue || '">'); --add id and ktr attribute

			<<FAC_LC_JT_LOOP>>
			lc_cDefaultsJobTitles := '';
			for jt in (
							select
                list.*
              from
                spd_t_lists list
              where
                list.productline_listid = 100085 and
                list.listname = 'list_job_titles'
                and list.grouping = contractor.contractor_listid
              order by
                list.listid,
                list.visualorder
						)
			loop
                lc_cDefaultsJobTitles := lc_cDefaultsJobTitles || '<jobtitle id="' || to_char(jt.listid) || '"
                    jobtitlesid="' || to_char(jt.listid) || '"
                    rate="0"
                    jtname="' || to_char(jt.displayvalue)  || '"
                    productline="' || to_char(rec.productline) || '"
                    estunitsrate="0"
                    baselineunits="0"
                    levelofeffort="0"
                    baselinetotal="0"
                    gcelinetotal="0"
                    gcelineunits="0"
                    adjustmentrate="0"
                    useredited="0"
                    />';

				dbms_lob.append(lc_cClob, '<jobtitle id="' || to_char(jt.listid) || '"
						jobtitlesid="' || to_char(jt.listid) || '"
						rate="0"
						jtname="' || to_char(jt.displayvalue)  || '"
						productline="' || to_char(rec.productline) || '"
						estunitsrate="0"
						baselineunits="0"
						levelofeffort="0"
						baselinetotal="0"
						gcelinetotal="0"
						gcelineunits="0"
						adjustmentrate="0"
						useredited="0"
						/>');
			end loop FAC_LC_JT_LOOP;
			dbms_lob.append(lc_cClob, '</jobtitles>');
    end loop FAC_LC_CONTRACTORS;







	dbms_lob.append(lc_cClob,'<directcosts>');
	dbms_lob.append(lc_cClob,'<facility>');

	--<<DEFAULTS_DC_FAC>>
	dbms_lob.append(lc_cClob,lc_cDefaultsFacDC);
	--end loop DEFAULTS_DC_FAC;

	dbms_lob.append(lc_cClob,'</facility>');
	dbms_lob.append(lc_cClob,'<activity>');

	<<DEFAULTS_DC_ACTV>>
	for dc in (
				select
				  *
				from
				  spd_t_do_ce_dc dc,
				  spd_t_lists list
				where
				  dc.directcost_listid = list.listid and
				  dc.adoid = in_adoid
				order by
				  dc.docedcid
		  )
	loop
		dbms_lob.append(lc_cClob, '<dc id="' || to_char(dc.docedcid) || '"
				directcostsid="' || to_char(dc.docedcid) || '"
				rate="0"
				dcname="' || to_char(dc.displayvalue) || '"
				productline="' || to_char(rec.productline) || '"
				estunitsrate="0"
				baselineunits="0"
				baselinetotal="0"
				gcelinetotal="0"
				gcelineunits="0"
				useredited="0"
				/>');
	end loop DEFAULTS_DC_ACTV;
	dbms_lob.append(lc_cClob,'</activity>');
	dbms_lob.append(lc_cClob,'</directcosts>');
	dbms_lob.append(lc_cClob,'</defaults>');

	dbms_lob.append(lc_cClob,'<laborcategories>');
	<<ACTV_LC_LOOP>>
	for lc in (
				select
					*
				from
					spd_t_lists
				where
					productline_listid = rec.productline and
					listname = 'list_delivery_order_labor_categories' and
					grouping = rec.worktype_listid
				order by
					visualorder
			)
	loop
		dbms_lob.append(lc_cClob, '<lc id="' || to_char(lc.listid) || '"
				laborcategoriesid="' || to_char(lc.listid) || '"
				productline="' || to_char(rec.productline) || '"
				lctype="' || to_char(lc.listname) || '"
				lcname="' || to_char(lc.displayvalue) || '"
				baselinetotal="0"
				gcelinetotal="0"
				>');




---Add the new grouping layer per contractor here:
      <<FAC_LC_CONTRACTORS>>
      for contractor in (
        select
          contractor_listid,
          (select displayvalue from spd_t_lists where listid = contractor_listid) displayvalue
        from
          spd_t_contractors_per_contract
        where
          contract_listid = rec.contract_listid



      )
      loop




			dbms_lob.append(lc_cClob,'<jobtitles id="' || contractor.contractor_listid || '" KTR="' || contractor.displayvalue || '">'); --add id and ktr attribute


		<<FAC_LC_JT_LOOP>>
		lc_cDefaultsJobTitles := '';
		for jt in (
						select
              *
            from
              spd_t_lists
            where
              productline_listid = rec.productline
              and listname = 'list_job_titles'
              and grouping = contractor.contractor_listid
            order by
              grouping,
              visualorder
					)
		loop

            dbms_lob.append(lc_cClob, '<jobtitle id="' || to_char(jt.listid) || '"
                    jobtitlesid="' || to_char(jt.listid) || '"
                    rate="0"
                    jtname="' || to_char(jt.displayvalue)  || '"
                    productline="' || to_char(rec.productline) || '"
                    estunitsrate="0"
                    baselineunits="0"
                    levelofeffort="0"
                    baselinetotal="0"
                    gcelinetotal="0"
                    gcelineunits="0"
                    adjustmentrate="0"
                    useredited="0"
                    />');
            --dbms_lob.append(lc_cClob, '</jobtitle>');
        end loop FAC_LC_JT_LOOP;
        dbms_lob.append(lc_cClob, '</jobtitles>');
    end loop;
        dbms_lob.append(lc_cClob, '</lc>');
	end loop ACTV_LC_LOOP;
	dbms_lob.append(lc_cClob, '</laborcategories>');

  dbms_lob.append(lc_cClob, '<directcosts>');
	<<ACTV_DC_LOOP>>
	for dc in (
      select
			  *
			from
			  spd_t_do_ce_dc dc,
			  spd_t_lists list
			where
			  dc.directcost_listid = list.listid and
			  dc.adoid = in_adoid
			order by
			  dc.docedcid
	  )
	  loop
	  dbms_lob.append(lc_cClob, '<dc id="' || to_char(dc.docedcid) || '"
			  directcostsid="' || to_char(dc.docedcid) || '"
			  rate="0"
			  dcname="' || to_char(dc.displayvalue) || '"
			  productline="' || to_char(rec.productline) || '"
			  estunitsrate="0"
			  baselineunits="0"
			  baselinetotal="0"
			  gcelinetotal="0"
			  gcelineunits="0"
			  useredited="0"
			  />');
	  --dbms_lob.append(lc_cClob, '</dc>');
	end loop ACTV_DC_LOOP;
	dbms_lob.append(lc_cClob, '</directcosts>');


			dbms_lob.append(lc_cClob, '</Action>');
  lc_vFilename := rec.delivery_order_name || '.xml';

	end loop ACTNLOOP;

dbms_lob.append(lc_cClob,'</CE>');



lc_bBlob := CLOB2BLOB(lc_cClob);

select SPIDERS_DATA.SPD_S_XML_FILES.nextval into lc_nXMLFileid from dual;


insert into spd_t_xml_files (xmlfileid, mime_type, blob_content, apexname,  filename)
values (lc_nXMLFileid,'text/xml',lc_bBlob, lc_vFilename, lc_vFilename);
commit;

htp.p(to_char(lc_nXMLFileid));



END BB_P_CE_XML_OUTPUT;
 

/
