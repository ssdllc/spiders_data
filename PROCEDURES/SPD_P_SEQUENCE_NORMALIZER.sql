--------------------------------------------------------
--  DDL for Procedure SPD_P_SEQUENCE_NORMALIZER
--------------------------------------------------------
set define off;

  CREATE OR REPLACE EDITIONABLE PROCEDURE "SPIDERS_DATA"."SPD_P_SEQUENCE_NORMALIZER" as 
  
  lc_vValue varchar2(2000);
  
  lc_jlDML json_list;
  lc_jResults json;
  lc_jMessages json;
  lc_vDMLResult varchar2(255);
  lc_vSql long;
  lc_jReturn json;
  
/*debugging*/
lc_nDebugCount number := 0;
lc_vError long;
/*end debugging*/
/*EDITED 20170626 MR DEBUG: [SPD_BUG_FIX][03] Dry Dock Upload*/

begin

  lc_jlDML := json_list();

for rec in (
select 
   user_tab_cols.*
 from 
   user_tab_cols, 
   all_constraints cons, 
   all_cons_columns cols
 where 
   user_tab_cols.table_name = cols.table_name
   and user_tab_cols.column_name = cols.column_name
   and cons.constraint_type = 'P'
   AND cons.constraint_name = cols.constraint_name
   AND cons.owner = cols.owner
   and cols.owner = 'SPIDERS_DATA'
   and user_tab_cols.table_name not like 'SPD_MV%'
   and user_tab_cols.table_name != 'SPD_T_SPIDERS_FAC_ASSETS')
/*debugging*/
--!!COMMENT, added specific table to exclude because it has a primary key that includes alpha
/*end debugging*/


loop

  lc_vValue := 'null';
  execute immediate 'begin select max(' || rec.column_name || ') into :result from '|| rec.table_name||' ; end;' using out lc_vValue ;

  for rec2 in (
  select 
    case 
      when 
        (max_actual-sequence_last_number) > 0 
      then 
        'drop sequence ' || sequence_name
      else 
        'no change' 
      end drop_sequence ,
    case 
      when 
        (max_actual-sequence_last_number) > 0 
      then 
        'create sequence '  || sequence_name || ' start with ' || (max_actual + 1)
      else 
        'no change' 
      end new_sequence ,
    max_actual, 
    sequence_last_number, 
    max_actual-sequence_last_number difference 
  from 
    (select 
      sequence_name, 
      last_number sequence_last_number, 
      lc_vValue max_actual 
    from 
      user_sequences 
    where 
      sequence_name = replace(rec.table_name,'SPD_T','SPD_S')
    )
  )
  loop
  
    if rec2.new_sequence <> 'no change' then
      lc_jlDML.append(rec2.drop_sequence);
      lc_jlDML.append(rec2.new_sequence);
    end if;
    
  end loop;
end loop;

  lc_jResults := json();
  lc_jResults.put('results',lc_jlDML);
  lc_jMessages := json();
  lc_vDMLResult := BB_F_DML(lc_jResults, lc_jMessages);
  lc_jReturn := lc_jMessages;  
  
end;
 

/
