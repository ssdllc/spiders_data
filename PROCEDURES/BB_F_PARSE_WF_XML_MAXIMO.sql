--------------------------------------------------------
--  DDL for Procedure BB_F_PARSE_WF_XML_MAXIMO
--------------------------------------------------------
set define off;

  CREATE OR REPLACE EDITIONABLE PROCEDURE "SPIDERS_DATA"."BB_F_PARSE_WF_XML_MAXIMO" (in_nFileiD number) as
lc_xAction xmltype;
lc_xTemp xmltype;
lc_xTemp1 xmltype;
lc_xTemp2 xmltype;
lc_xTemp3 xmltype;
lc_vString varchar2(32767);
lc_nADOID number;
lc_vFileName varchar2(255);
lc_cClob clob;
lc_vPath varchar2(3000);
lc_nDOFID number :=0;
lc_nAIDEFECTID number :=0;
lc_nXMLFileID number :=0;

lc_nListid number;
lc_nListid2 number;
lc_nAIID number;
lc_nAIID_parent number;
lc_nUFIIExists number;
lc_nNewListID number;

lc_xExecutiveSummaryNode xmltype;
lc_xDrawingsNode xmltype;
lc_xSupportingFilesNode xmltype;
lc_xPhotosNode xmltype;
lc_xFacilitiesNode xmltype;
lc_xFacMetaDataNode xmltype;
lc_xFacDrawingsNode xmltype;
lc_xFacSupportingFilesNode xmltype;
lc_xAssetSummaryNode xmltype;
lc_xFindRecsNode xmltype;
lc_xAssetInventoryNode xmltype;
lc_xAIMetaDataNode xmltype;
lc_xDefectsNode xmltype;
lc_xDefectMetaDataNode xmltype;
lc_xMooringFittingsNode xmltype;
lc_xAIPhotosNode xmltype;
lc_xAIDrawingsNode xmltype;
lc_xFacilityRepairCostsNode xmltype;
lc_xRepairNode xmltype;
lc_nRow number :=0;

lc_nFolderId number :=0;
lc_vTemp varchar2(255);
lc_nThumbnailid number :=0;

lc_nCountFindReco number :=0;
lc_vUFII varchar2(50);
lc_cFinding clob;
lc_cRecommendation clob;
lc_xPhotoAssetTypesNode xmltype;
lc_nPhotoId number;
lc_nContinue number;
lc_nFacilityCount number :=0;

lc_dDate date := sysdate;
lc_nAPID number :=0;
lc_nNodeCount number:=0;
lc_nAFRID number :=0;


lc_vDataReqRevision varchar2(1000);
lc_vExitBriefDate varchar2(1000);
lc_vExitBriefTime varchar2(1000);
lc_vExitBriefLOC varchar2(1000);
lc_vDrawingID varchar2(1000);
lc_vNAVFACDrawingNumber varchar2(1000);
lc_vDrawingDescription varchar2(4000);
lc_vDrawingType varchar2(1000);
lc_vSupportingFileID varchar2(1000);
lc_vSupportingFileDescription varchar2(1000);
lc_vPhotoID varchar2(1000);
lc_vPhotoCaption varchar2(2000);
lc_vPhotoType varchar2(1000);
lc_vOnsiteStartDate varchar2(1000);
lc_vOnsiteEndDate varchar2(1000);
lc_vSubmittalDate varchar2(1000);
lc_vOverallEngAssRatingASCE varchar2(1000);
lc_nOverallCIRating number;
lc_nOveral5YearProjectedCI number;
lc_nOveral10YearProjectedCI number;
lc_vCurrentOperationalRating varchar2(1000);
lc_vOperationalRestrictions varchar2(1000);
lc_vRepairRecommendations varchar2(1000);
lc_vCurFacUsageDescription varchar2(1000);
lc_vTotalNumOfPersOnSite varchar2(1000);
lc_vTotalNumberOfDaysOnSite varchar2(1000);
lc_vMetaDisplay varchar2(1000);
lc_vMetaValue varchar2(1000);
lc_vAssetType varchar2(1000);
lc_vFacilitySection varchar2(1000);
lc_vNoOfAssetsCount varchar2(1000);
lc_vQuantityOfUnits varchar2(1000);
lc_vUnitType varchar2(1000);
lc_vCurrentCI varchar2(1000);
lc_nApproxRepairCost number;
lc_vEstimatedResultingCI varchar2(1000);
lc_vMaterial varchar2(1000);
lc_vMaterialDescription varchar2(1000);
lc_vDefectIDNo varchar2(1000);
lc_DefectLoction varchar2(1000);
lc_vDefectCenter varchar2(1000);
lc_vCriticalityFactor varchar2(1000);
lc_vDefectType varchar2(1000);
lc_vSRMClassification varchar2(1000);
lc_vFittingIDNumber varchar2(1000);
lc_vMooringType varchar2(1000);
lc_vDesignCapacity varchar2(1000);
lc_vRatingCapacity varchar2(1000);
lc_vConditionFitting varchar2(1000);
lc_vConditionBase varchar2(1000);
lc_vMXLFacilityName varchar2(1000);
lc_vXMLFacilityNFAID varchar2(1000);
lc_vReference varchar2(2000);

lc_nQuantity number;
lc_vUnits varchar2(200);
lc_vSummaryDescription varchar2 (2000);
lc_vLongDescription varchar2 (2000);
lc_nMaterialUnitCost number;
lc_nLaborUnitCost number;
lc_vExpenseCost varchar2(2000);

lc_nRowNum number;
lc_nNodeCountAssetInventory number;
lc_nRowNumAssetInventory number;
lc_nRowNumAssetSummary number;
lc_nNodeCountAssetSummary number;
lc_nNodeCountAIDefects number;
lc_nNodeCountMoorings number;
lc_nNodeCountFindRec number;
lc_nNodeCountASPhotos number;
lc_nNodeCountFacilities number;
lc_nNodeCountFacDrawings number;
lc_nNodeCountSupFiles number;
lc_nNodeCountFacRepairs number;
lc_nNodeCountRepairs number;

lc_nDML number :=0;
lc_nDML_inner number :=0;
lc_nSuccess number :=0;
lc_nSuccessDefects number :=0;


lc_cDML clob;
lc_vImportSectionForDML varchar2(2000);

  lc_jJson json;
  lc_jlResults json_list;
  lc_rec_val json_value;
  lc_rec json;
  lc_cnt number;

  lc_jResults json;
  lc_jMessages json;
  lc_jlMessages json_list;
  lc_vReturn varchar2(1);

  --error stuff
  lc_jError json;
  json_error EXCEPTION;
  lc_vError varchar2(4000);
  
  
procedure  p (in_vImportSection varchar2, in_vNotes varchar2) as

begin

dbms_output.put_line('Import section: ' || in_vImportSection || ' , ' || in_vNotes);

end;


  function  test_val (IN_FROM varchar2, in_xml xmltype) return varchar2 as
lc_vReturn varchar2(3999) := '';
lc_jError json;
lc_jlMessages json_list;
        
begin

    if in_xml is null then
      lc_vReturn := '';
    else

        p(IN_FROM,1);

      lc_vReturn := substr(in_xml.getstringval(),1,256);
      --htp.p('1 ' || lc_vReturn || '''');

      lc_vReturn := TRANSLATE (lc_vReturn, 'x'||CHR(10)||CHR(13), 'x');

      --htp.p('2 ' || lc_vReturn || '''');


      lc_vReturn := trim(lc_vReturn);

      --htp.p('3 ' || lc_vReturn || '''');
    end if;

return lc_vReturn;

  exception
    when others then
        lc_jError := json();
        lc_jlMessages := json_list();
        lc_jError.put('function','test_val');
        lc_jError.put('module','xmlImportManager');
        lc_jError.put('target','test_val');
        lc_jError.put('ref_id',1);
        lc_jError.put('input','');
        lc_jlMessages.append(json_list('[{error:''IX.000X ' || SQLERRM || '''}]'));
        lc_jError.put('messages',lc_jlMessages);
        BB_P_ERROR(lc_jError);
        return 'e';
end;

function  test_num (in_xml xmltype) return number as
lc_nReturn number := 0;
begin

    --if the xml value is not a number, we get an error, so we want to translate that to a 0. This makes it easy for us to implement a 0 as the value instead of adding difficult to test validation in validator. We eventually will do this in validator, but for now this works. I dont like changing data, but for now it will work. -MR 20100222
    begin

    if in_xml is null then
      lc_nReturn := 0;
    else
      lc_nReturn := in_xml.getnumberval();
    end if;

    exception
    when others then
      lc_nReturn := 0; --Not a number..

    end;
return lc_nReturn;
end;





begin





    lc_jMessages := json();
    lc_jlMessages := json_list();




p(in_vImportSection=>'lkjlkjlj', in_vNotes=>'asdfads');


/*-------------END DELETE EVERYTHING FOR THIS ADOID---------------*/



SELECT
  xmlfileid into lc_nXMLFileID
FROM
  spd_t_xml_files
where
  xmlfileid = in_nFileiD;





/*------------ACTION-------------------*/
lc_vImportSectionForDML :='UPDATE DELIVERY ORDER TABLE';


p(in_vImportSection=>lc_vImportSectionForDML, in_vNotes=>'');
for rec_action in
            (
              SELECT
                xml_table.column_value action
              FROM
                spd_t_xml_files source_table,
                XMLTable('/Action' passing source_table.xml_document) xml_table
              where
                source_table.xmlfileid = lc_nXMLFileID
            )
loop

    lc_xAction := rec_action.action;

p(in_vImportSection=>lc_xAction.extract('/Action/ActivityName/text()').getstringval(), in_vNotes=>'');


lc_vOnsiteStartDate := test_val('',lc_xAction.extract('/Action/OnsiteStartDate/text()'));
lc_vReference := test_val('',lc_xAction.extract('/Action/ReportNumber/text()'));




















/*------------FACILITIES OF ACTION-------------------*/

  select extract(lc_xAction, '/Action/Facilities') into lc_xFacilitiesNode from dual;
  select count(*) into lc_nNodeCountFacilities FROM XMLTable('/Facilities/Facility' passing lc_xFacilitiesNode);
lc_vImportSectionForDML :='FACILITIES OF ACTION';
p(in_vImportSection=>lc_vImportSectionForDML, in_vNotes=>'Facilities Count: ' || lc_nNodeCount);


/*------------FACILITY OF FACILITIES-------------------*/
  lc_nRowNum := 0;
  for rec_facilities in (
  SELECT
    rownum rn,
    xml_table.column_value facility 
  FROM
    XMLTable('/Facilities/Facility' passing lc_xFacilitiesNode) xml_table
  )
  loop

/*
MR:
Adding new functionality to match the xml facilities with their proper DOFID's

select dofid from
spd_t_xml_facilities_match
where

xml_facility_name = rec_facilities.facility.extract('/Facility/FacilityName/text()').getstringval() and
xml_nfaid = rec_facilities.facility.extract('/Facility/FacilityNo/text()').getstringval();
adoid = IN_nADOID


*/

  lc_nDOFID :=0;
  lc_vMXLFacilityName := test_val('',rec_facilities.facility.extract('/Facility/FacilityName/text()'));
  lc_vXMLFacilityNFAID :=  test_val('',rec_facilities.facility.extract('/Facility/NFAID/text()'));

  select count(*) into lc_nNodeCount FROM XMLTable('/Facilities/Facility' passing lc_xFacilitiesNode);
  lc_vImportSectionForDML :='FACILITY ' || rec_facilities.rn || ' OF ' || lc_nNodeCount || ' FACILITIES';
p(in_vImportSection=>lc_vImportSectionForDML, in_vNotes=>rec_facilities.rn || ' ' || lc_vMXLFacilityName);


/*
  lc_vOnsiteStartDate :=test_val('/Facility/OnsiteStartDate/',rec_facilities.facility.extract('/Facility/OnsiteStartDate/text()'));
  lc_vOnsiteEndDate := test_val('/Facility/OnsiteEndDate/text',rec_facilities.facility.extract('/Facility/OnsiteEndDate/text()'));
  lc_vSubmittalDate := test_val('/Facility/SubmittalDate',rec_facilities.facility.extract('/Facility/SubmittalDate/text()'));
  lc_vOverallEngAssRatingASCE := test_val('OverallEngAssmentRatingASCE',rec_facilities.facility.extract('/Facility/ExecutiveTable/OverallEngAssmentRatingASCE/text()'));
  lc_nOverallCIRating := test_num(rec_facilities.facility.extract('/Facility/ExecutiveTable/OverallCIRating/text()'));
  lc_nOveral5YearProjectedCI := test_num(rec_facilities.facility.extract('/Facility/ExecutiveTable/Overall5YearProjectedCI/text()'));
  lc_nOveral10YearProjectedCI := test_num(rec_facilities.facility.extract('/Facility/ExecutiveTable/Overall10YearProjectedCI/text()'));
  lc_vCurrentOperationalRating := test_val('CurrentOperationalRating',rec_facilities.facility.extract('/Facility/ExecutiveTable/CurrentOperationalRating/text()'));
  lc_vOperationalRestrictions := test_val('OperationalRestrictions',rec_facilities.facility.extract('/Facility/ExecutiveTable/OperationalRestrictions/text()'));
  lc_vRepairRecommendations := test_val('RepairRecommendations',rec_facilities.facility.extract('/Facility/ExecutiveTable/RepairRecommendations/text()'));
  lc_vCurFacUsageDescription := test_val('CurrentFacilityUsageDescription',rec_facilities.facility.extract('/Facility/ExecutiveTable/CurrentFacilityUsageDescription/text()'));
  lc_vTotalNumOfPersOnSite := test_val('OnSitePersonnel',rec_facilities.facility.extract('/Facility/ExecutiveTable/TotalNumberOfPersonnelOnSite/text()'));
  lc_vTotalNumberOfDaysOnSite := test_val('DaysOnsite',rec_facilities.facility.extract('/Facility/ExecutiveTable/TotalNumberOfDaysOnSite/text()'));

*/




/*------------FACILITY REPAIR COST OF FACILITY-------------------*/

  select extract(lc_xAction, '/Facility/FacilityRepairCosts') into lc_xFacilityRepairCostsNode from dual;
  select count(*) into lc_nNodeCountFacilities FROM XMLTable('/FacilityRepairCosts/Repair' passing lc_xFacilitiesNode);
lc_vImportSectionForDML :='FACILITY REPAIR COST OF FACILITY';
p(in_vImportSection=>lc_vImportSectionForDML, in_vNotes=>'Repairs Count: ' || lc_nNodeCountFacilities);

p(substr(lc_xAction.getStringVal(),1,200) ,'this should be the xml');

/*------------FACILITY OF FACILITIES-------------------*/
  lc_nRowNum := 0;
  for rec_FacilityRepairCosts in (
  SELECT
    rownum rn,
    xml_table.column_value fac_repair 
  FROM
    XMLTable('/FacilityRepairCosts/Repair' passing lc_xFacilityRepairCostsNode) xml_table
  )
  loop


      lc_nQuantity := test_num(rec_FacilityRepairCosts.fac_repair.extract('/Repair/Quantity/text()'));
      lc_vUnits := test_val('',rec_FacilityRepairCosts.fac_repair.extract('/Repair/Units/text()'));

      lc_vSummaryDescription := test_val('',rec_FacilityRepairCosts.fac_repair.extract('/Repair/RepairDescription/text()'));
      lc_vLongDescription := 'Magnitude of Discrepancy : ' || lc_nQuantity || ' ' ||  lc_vUnits || ' Reference : ' ||   lc_vReference;

      lc_nMaterialUnitCost := test_num(rec_FacilityRepairCosts.fac_repair.extract('/Repair/MaterialUnitCost/text()'));
      lc_nLaborUnitCost := test_num(rec_FacilityRepairCosts.fac_repair.extract('/Repair/LaborUnitCost/text()'));
      lc_vExpenseCost := (lc_nMaterialUnitCost * lc_nQuantity) + (lc_nLaborUnitCost + lc_nQuantity);
      p(in_vImportSection=>'in repair cost?', in_vNotes=>lc_vSummaryDescription);
      
/*
SITE_ID                     maximo location if we have it
SUMMARY_DESCRIPTION         lc_vSummaryDescription
LONG_DESCRIPTION            lc_vLongDescription
LOCATION                    lc_vXMLFacilityNFAID
ADDL_LOC_INFO
WORKTYPE
SUBWORKTYPE
BUSINESS_LINE
PS_DELIVERABLE
NAVFAC_WORK_CATEGORY
REPORTED_BY
CUSTOMER_ID
CUSTOMER_NAME
CUSTOMER_PHONE
CUSTOMER_EMAIL
CUSTOMER_REF_#
PRIORITY_SCORE
PRIORITY_YEAR               lc_vOnsiteStartDate
ASSET
EXPECTED_COST               lc_vExpenseCost
GL_ACCOUNT
SERVICE_PROVIDER_REF_CODE
CLASSIFICATION
INTERNAL_PRIORITY
*/
  end loop; --facilities costs 


   
  end loop;--facilities loop
p(in_vImportSection=>'END Facilities of Action', in_vNotes=>'');


end loop;--action loop
p(in_vImportSection=>'END Action', in_vNotes=>'');


  exception
    when others then
      dbms_output.put_line(sqlerrm);
end;
 

/
