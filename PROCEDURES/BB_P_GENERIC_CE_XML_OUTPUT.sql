--------------------------------------------------------
--  DDL for Procedure BB_P_GENERIC_CE_XML_OUTPUT
--------------------------------------------------------
set define off;

  CREATE OR REPLACE EDITIONABLE PROCEDURE "SPIDERS_DATA"."BB_P_GENERIC_CE_XML_OUTPUT" (
in_vFacilityNFA varchar2,
in_nTotalFacilityCount number,
in_vContractors varchar2,
in_vJobTitles varchar2,
in_vActionLaborCategories varchar2,
in_vActionDirectCosts varchar2,
in_vFacLaborCategories varchar2,
in_vFacDirectCosts varchar2,
in_nADOID number,
in_nAPID number,
in_nProductlineListID number,
in_vEIC varchar2,
in_vCommand varchar2,
in_vDate varchar2,
in_vFY varchar2,
in_vContractNumber varchar2,
in_vLocation varchar2,
in_vUIC varchar2,
in_vFundsource varchar2,
in_vProjectedStartDate varchar2,
in_vSpecInstructionTitles varchar2,
in_vSpecInstructionValues varchar2,
in_vDeliveryOrderName varchar2,
in_vWorkType varchar2)

is
lc_nFacCount number :=0;
lc_nSpecInstCount number :=0;
lc_cDefaultsFacLaborCat clob :=' ';
lc_cDefaultsJobTitles clob := ' ';
lc_cDefaultsFacDc clob := ' ';
lc_vUICS varchar2(2000);
lc_vLocation varchar2(2000);
lc_cClob clob :=' ';
lc_bBlob blob;
lc_nXMLFileid number :=0;
lc_vFilename varchar2(2000);


l_array_fac t_str_array;
l_array_spec_inst_titles t_str_array;
l_array_spec_inst_values t_str_array;
l_array_fac_lc t_str_array;
l_array_contractors t_str_array;
l_array_job_titles t_str_array;
l_array_fac_dc t_str_array;
l_array_action_dc t_str_array;
l_array_action_lc t_str_array;
l_val varchar2(4000);

  v_blob_offset NUMBER :=1;
  v_clob_offset NUMBER :=1;
  v_lang_context NUMBER := DBMS_LOB.DEFAULT_LANG_CTX;
  v_warning NUMBER;
  v_string_length NUMBER(10);
  v_source_locator BLOB;
  v_destination_locator BLOB;
  v_amount PLS_INTEGER;
  v_string CLOB;
  
  procedure p(in_vVal varchar2) is 

      begin
      
      null;--dbms_output.put_line(in_vVal);

      end;
begin





  lc_vLocation := in_vLocation;
  lc_vUICS := in_vUIC;



--for actn in ce.myCe.actions.first .. ce.myCe.actions.last loop
--get all of the activity uics from the scoped facilities:





--Open the Cost Estimate xml 
p('<CE>');
dbms_lob.append(lc_cClob,'<CE>');



--Add the action, there will be only one action per CE xml, using all of the inputs for the attributes
p('<Action>');
dbms_lob.append(lc_cClob, 
'<Action Program="' || in_nProductlineListid || '"
EIC="' || in_vEIC || '"
Command="' || in_vCommand || '"
Date="' || to_char(sysdate,'DD-MON-YYYY') || '"
blankrow=""
ContractNumber="' || in_vContractNumber || '"
DeliveryOrderNumber="' || in_nADOID || '"
ACTIONSID="' || in_nAPID || '"
Activity="' || lc_vUICS || '"
Location="' || lc_vLocation || '"
FY="' || in_vFY || '"
ProjectedStartDate="' || in_vProjectedStartDate || '"
WorkType="' || in_vWorkType || '"
WorkTypeID="">');

--Add the other nodes at the Action level
		dbms_lob.append(lc_cClob, '<baselinedirectcost>0</baselinedirectcost>');
		dbms_lob.append(lc_cClob, '<baselinelaborcost>0</baselinelaborcost>');
		dbms_lob.append(lc_cClob, '<overheadrate>0</overheadrate>');
		dbms_lob.append(lc_cClob, '<profitrate>0</profitrate>');
		dbms_lob.append(lc_cClob, '<baselinetotalcost>0</baselinetotalcost>');
		dbms_lob.append(lc_cClob, '<gcedirectcost>0</gcedirectcost>');
		dbms_lob.append(lc_cClob, '<gcelaborcost>0</gcelaborcost>');
		dbms_lob.append(lc_cClob, '<gcetotalcost>0</gcetotalcost>');
		dbms_lob.append(lc_cClob, '<factargetlaborcost>0</factargetlaborcost>');
		dbms_lob.append(lc_cClob, '<facbaselabortotal>0</facbaselabortotal>');
		dbms_lob.append(lc_cClob, '<facfieldworktotal>0</facfieldworktotal>');
		dbms_lob.append(lc_cClob, '<adjustmentrate>0</adjustmentrate>');

<<CONTRACTORSLOOP>>
  --Read the CSV of contractors, loop the array
  dbms_lob.append(lc_cClob, '<contractors>');
  l_array_contractors := csv_util_pkg.csv_to_array (in_vContractors);
  for contractor in l_array_contractors.first .. l_array_contractors.last loop
    dbms_lob.append(lc_cClob, '<contractor>' || l_array_contractors(contractor) || '</contractor>');
  end loop CONTRACTORSLOOP;
  dbms_lob.append(lc_cClob, '</contractors>');




	<<FACLOOP>> --Begin the Facility Loop
  
  --Read the CSV of facilities, loop the array
  l_array_fac := csv_util_pkg.csv_to_array (in_vFacilityNFA);
  for fac in l_array_fac.first .. l_array_fac.last loop
  
    p('<Facility id="' || trim(l_array_fac(fac)) || '">');
  
  
      dbms_lob.append(lc_cClob, 
      '<Facility
      id="0"
      facilitiesid=""
      activitiesid="0"
      GPS="0"
      priority="0"
      CI="0"
      MDI="0"
      AGE="0"
      LAST_INSPECTED="0"
      OSD="0"
      facilityName=""
      facilityNo=""
      nfa="' || trim(l_array_fac(fac)) || '"
      >');


		dbms_lob.append(lc_cClob, '<FacilityDescription>');
      dbms_lob.append(lc_cClob, '<Length displayname="Length">0</Length>');
      dbms_lob.append(lc_cClob, '<Piles displayname="Piles">0</Piles>');
      dbms_lob.append(lc_cClob, '<maxwaterdepth displayname="Max Water Depth">0</maxwaterdepth>');
      dbms_lob.append(lc_cClob, '<yearbuilt displayname="Year Built">0</yearbuilt>');
		dbms_lob.append(lc_cClob, '</FacilityDescription>');

    p('<specialinstructions>');
		dbms_lob.append(lc_cClob, '<specialinstructions>');
		<<FAC_SPEC_LOOP>>
    l_array_spec_inst_titles := csv_util_pkg.csv_to_array (in_vSpecInstructionTitles);
    l_array_spec_inst_values := csv_util_pkg.csv_to_array (in_vSpecInstructionValues);
    for spec_inst in l_array_spec_inst_values.first .. l_array_spec_inst_values.last loop
      p('<specinstr id="' || spec_inst || '" />');
			dbms_lob.append(lc_cClob, '<specinstr id="' || spec_inst || '"
				  displayname="' || l_array_spec_inst_titles(spec_inst) || '"
				  adjustment="0"
				  instruction="' || l_array_spec_inst_values(spec_inst) || '"
				  />');
		end loop FAC_SPEC_LOOP;
    p('</specialinstructions>');
		dbms_lob.append(lc_cClob, '</specialinstructions>');

    p('<laborcategories>');
		dbms_lob.append(lc_cClob, '<laborcategories>');
    
		<<FAC_LC_LOOP>>
    l_array_fac_lc := csv_util_pkg.csv_to_array (in_vFacLaborCategories);
    for fac_lc in l_array_fac_lc.first .. l_array_fac_lc.last loop
			--we build the default based on the facility, so just make a clob and use that later in the procedure instead of relooping
			p('<lc id="' || trim(l_array_fac_lc(fac_lc)) || '">');
      
      if fac=1 then
        lc_cDefaultsFacLaborCat := lc_cDefaultsFacLaborCat || '<lc id="' || fac_lc || '"
            laborcategoriesid="0"
            productline="' || in_nProductlineListid || '"
            lctype="F"
            lcname="' || trim(l_array_fac_lc(fac_lc)) || '"
            baselinetotal="0"
            gcelinetotal="0"
            />';
      end if;

      
      dbms_lob.append(lc_cClob, '<lc id="' || fac_lc || '"
					laborcategoriesid="0"
					productline="' || in_nProductlineListid || '"
					lctype="F"
					lcname="' || trim(l_array_fac_lc(fac_lc)) || '"
					baselinetotal="0"
					gcelinetotal="0"
					/>');
          

---Add the new grouping layer per contractor here:

    l_array_contractors := csv_util_pkg.csv_to_array (in_vContractors);
    for contractor in l_array_contractors.first .. l_array_contractors.last loop
      p('<jobtitles id="' || trim(l_array_contractors(contractor)) || '">');
			dbms_lob.append(lc_cClob,'<jobtitles id="' || contractor || '" KTR="' || trim(l_array_contractors(contractor)) || '">'); --add id and ktr attribute

			<<FAC_LC_JT_LOOP>>
			lc_cDefaultsJobTitles := '';

      l_array_job_titles := csv_util_pkg.csv_to_array (in_vJobTitles);
      for jt in l_array_job_titles.first .. l_array_job_titles.last loop
        p('<jobtitle id="' || trim(l_array_job_titles(jt)) || '">');
        lc_cDefaultsJobTitles := lc_cDefaultsJobTitles || '<jobtitle id="' || jt || '"
            jobtitlesid="0"
            rate="0"
            jtname="' || trim(l_array_job_titles(jt))  || '"
            productline="' || in_nProductlinelistid || '"
            estunitsrate="0"
            baselineunits="0"
            levelofeffort="0"
            baselinetotal="0"
            gcelinetotal="0"
            gcelineunits="0"
            adjustmentrate="0"
            useredited="0"
            />';

				dbms_lob.append(lc_cClob, '<jobtitle id="' || jt || '"
            jobtitlesid="0"
            rate="0"
            jtname="' || trim(l_array_job_titles(jt))  || '"
            productline="' || in_nProductlinelistid || '"
            estunitsrate="0"
            baselineunits="0"
            levelofeffort="0"
            baselinetotal="0"
            gcelinetotal="0"
            gcelineunits="0"
            adjustmentrate="0"
            useredited="0"
            />');
      p('</jobtitle>');
			end loop FAC_LC_JT_LOOP;
      p('</jobtitles>');
			dbms_lob.append(lc_cClob, '</jobtitles>');
    end loop FAC_LC_CONTRACTORS;
      --p('</lc>');
			--dbms_lob.append(lc_cClob, '</lc>');
		end loop FAC_LC_LOOP;
    p('</laborcategories>');
		dbms_lob.append(lc_cClob, '</laborcategories>');


    p('<directcosts>');
		dbms_lob.append(lc_cClob, '<directcosts>');
		<<FAC_DC_LOOP>>
		lc_cDefaultsFacDc := ' ';
    l_array_fac_dc := csv_util_pkg.csv_to_array (in_vFacDirectCosts);
    for dc in l_array_fac_dc.first .. l_array_fac_dc.last loop
      p('<dc id="' || dc || '"/>');
			lc_cDefaultsFacDc := lc_cDefaultsFacDc || '<dc id="' || dc || '"
					directcostsid="0"
					rate="0"
					dcname="' || l_array_fac_dc(dc) || '"
					productline="' || in_nProductlinelistid || '"
					estunitsrate="0"
					baselineunits="0"
					baselinetotal="0"
					gcelinetotal="0"
					gcelineunits="0"
					useredited="0"
					/>';

			dbms_lob.append(lc_cClob, '<dc id="' || dc || '"
					directcostsid="0"
					rate="0"
					dcname="' || l_array_fac_dc(dc) || '"
					productline="' || in_nProductlinelistid || '"
					estunitsrate="0"
					baselineunits="0"
					baselinetotal="0"
					gcelinetotal="0"
					gcelineunits="0"
					useredited="0"
					/>');
		end loop FAC_DC_LOOP;
    p('</directcosts>');
		dbms_lob.append(lc_cClob, '</directcosts>');
    p('</Facility>');
		dbms_lob.append(lc_cClob, '</Facility>');
	end loop FACLOOP;


--<<ACTVLOOP>>
  p('<Activity name="' || lc_vLocation || '">');
	dbms_lob.append(lc_cClob, '<Activity name="' || lc_vLocation || '">');
  
  p('<ActivitiesID>' || lc_vUICS || '</ActivitiesID>');
	dbms_lob.append(lc_cClob, '<ActivitiesID>' || lc_vUICS || '</ActivitiesID>');
	dbms_lob.append(lc_cClob, '<GPS>0</GPS>');
  
  p('</Activity>');
	dbms_lob.append(lc_cClob, '</Activity>');

  p('<defaults>');
	dbms_lob.append(lc_cClob,'<defaults>');
  

  
  
  p('<facility>');
	dbms_lob.append(lc_cClob,'<facility>');

  p('<laborcategories>');
	dbms_lob.append(lc_cClob,'<laborcategories>');
  
  p(lc_cDefaultsFacLaborCat);
  
	dbms_lob.append(lc_cClob,lc_cDefaultsFacLaborCat);
  
  p('</laborcategories>');
	dbms_lob.append(lc_cClob,'</laborcategories>');

  p('</facility>');
	dbms_lob.append(lc_cClob,'</facility>');
  
  p('<activity>');
	dbms_lob.append(lc_cClob,'<activity>');


  p('<laborcategories>');
	dbms_lob.append(lc_cClob,'<laborcategories>');
	<<DEFAULTS_LC_ACTV>>
	--for lc in ce.myCe.laborcatactv.first .. ce.myCe.laborcatactv.last loop
  l_array_action_lc := csv_util_pkg.csv_to_array (in_vActionLaborCategories);
  for lc in l_array_action_lc.first .. l_array_action_lc.last loop
    p('<lc id="' || lc || '"/>');
		dbms_lob.append(lc_cClob, '<lc id="' || lc || '"
				laborcategoriesid="0"
				productline="' || in_nProductlinelistid || '"
				lctype="S"
				lcname="' || l_array_action_lc(lc) || '"
				baselinetotal="0"
				gcelinetotal="0"
				/>');
	end loop DEFAULTS_LC_ACTV;

  p('</laborcategories>');
	dbms_lob.append(lc_cClob,'</laborcategories>');
  
  p('</activity>');
  dbms_lob.append(lc_cClob,'</activity>');


	--dbms_lob.append(lc_cClob,lc_cDefaultsJobTitles);
	---Add the new grouping layer per contractor here:
  
      <<FAC_LC_CONTRACTORS>>
      for contractor in l_array_contractors.first .. l_array_contractors.last loop
      p('<jobtitles id="' || contractor || '" KTR="' || l_array_contractors(contractor) || '">');
			dbms_lob.append(lc_cClob,'<jobtitles id="' || contractor || '" KTR="' || l_array_contractors(contractor) || '">'); --add id and ktr attribute

			<<FAC_LC_JT_LOOP>>
        l_array_job_titles := csv_util_pkg.csv_to_array (in_vJobTitles);
        for jt in l_array_job_titles.first .. l_array_job_titles.last loop
          p('<jobtitle id="' || jt || '" />');
                lc_cDefaultsJobTitles := lc_cDefaultsJobTitles || '<jobtitle id="' || jt || '"
                    jobtitlesid="0"
                    rate="0"
                    jtname="' || l_array_job_titles(jt)  || '"
                    productline="' || in_nProductlinelistid || '"
                    estunitsrate="0"
                    baselineunits="0"
                    levelofeffort="0"
                    baselinetotal="0"
                    gcelinetotal="0"
                    gcelineunits="0"
                    adjustmentrate="0"
                    useredited="0"
                    />';

				dbms_lob.append(lc_cClob, '<jobtitle id="' || jt || '"
                    jobtitlesid="0"
                    rate="0"
                    jtname="' || l_array_job_titles(jt)  || '"
                    productline="' || in_nProductlinelistid || '"
                    estunitsrate="0"
                    baselineunits="0"
                    levelofeffort="0"
                    baselinetotal="0"
                    gcelinetotal="0"
                    gcelineunits="0"
                    adjustmentrate="0"
                    useredited="0"
                    />');
			end loop FAC_LC_JT_LOOP;
      p('</jobtitles>');
			dbms_lob.append(lc_cClob, '</jobtitles>');
    end loop FAC_LC_CONTRACTORS;






  p('<directcosts>');
	dbms_lob.append(lc_cClob,'<directcosts>');
  
  p('<facility>');
	dbms_lob.append(lc_cClob,'<facility>');

	--<<DEFAULTS_DC_FAC>>
  p(lc_cDefaultsFacDC);
	dbms_lob.append(lc_cClob,lc_cDefaultsFacDC);
	--end loop DEFAULTS_DC_FAC;

  p('</facility>');
	dbms_lob.append(lc_cClob,'</facility>');
  
  p('<activity>');
	dbms_lob.append(lc_cClob,'<activity>');

	<<DEFAULTS_DC_ACTV>>
  l_array_action_dc := csv_util_pkg.csv_to_array (in_vActionDirectCosts);
  for dc in l_array_action_dc.first .. l_array_action_dc.last loop
    p('<dc id="' || dc || '" />');
		dbms_lob.append(lc_cClob, '<dc id="' || dc || '"
				directcostsid="0"
				rate="0"
				dcname="' || l_array_action_dc(dc) || '"
				productline="' || in_nProductlineListid || '"
				estunitsrate="0"
				baselineunits="0"
				baselinetotal="0"
				gcelinetotal="0"
				gcelineunits="0"
				useredited="0"
				/>');
	end loop DEFAULTS_DC_ACTV;
  p('</activity>');
	dbms_lob.append(lc_cClob,'</activity>');
  p('</directcosts>');
	dbms_lob.append(lc_cClob,'</directcosts>');
  p('</defaults>');
	dbms_lob.append(lc_cClob,'</defaults>');


  p('<laborcategories>');
	dbms_lob.append(lc_cClob,'<laborcategories>');
	<<ACTV_LC_LOOP>>
  l_array_action_lc := csv_util_pkg.csv_to_array (in_vActionLaborCategories);
  for lc in l_array_action_lc.first .. l_array_action_lc.last loop
    p('<lc id="' || lc || '" >');
		dbms_lob.append(lc_cClob, '<lc id="' || lc || '"
				laborcategoriesid="0"
				productline="' || in_nProductlineListid || '"
				lctype="S"
				lcname="' || l_array_action_lc(lc) || '"
				baselinetotal="0"
				gcelinetotal="0"
				>');




---Add the new grouping layer per contractor here:
      <<FAC_LC_CONTRACTORS>>
      for contractor in l_array_contractors.first .. l_array_contractors.last loop
      p('<jobtitles id="' || contractor || '" KTR="' || l_array_contractors(contractor) || '">');
			dbms_lob.append(lc_cClob,'<jobtitles id="' || contractor || '" KTR="' || l_array_contractors(contractor) || '">'); --add id and ktr attribute
 
 
		<<FAC_LC_JT_LOOP>>
		lc_cDefaultsJobTitles := '';
			<<FAC_LC_JT_LOOP>>
        l_array_job_titles := csv_util_pkg.csv_to_array (in_vJobTitles);
        for jt in l_array_job_titles.first .. l_array_job_titles.last loop
            p('<jobtitle id="' || jt || '" />');
            dbms_lob.append(lc_cClob, '<jobtitle id="' || jt || '"
                    jobtitlesid=""
                    rate="0"
                    jtname="' || l_array_job_titles(jt)  || '"
                    productline="' || in_nProductlinelistid || '"
                    estunitsrate="0"
                    baselineunits="0"
                    levelofeffort="0"
                    baselinetotal="0"
                    gcelinetotal="0"
                    gcelineunits="0"
                    adjustmentrate="0"
                    useredited="0"
                    />');
            --dbms_lob.append(lc_cClob, '</jobtitle>');
        end loop FAC_LC_JT_LOOP;
        p('</jobtitles>');
        dbms_lob.append(lc_cClob, '</jobtitles>');
    end loop;
        p('</lc>');
        dbms_lob.append(lc_cClob, '</lc>');
	end loop ACTV_LC_LOOP;
  p('</laborcategories>');
	dbms_lob.append(lc_cClob, '</laborcategories>');


  p('<directcosts>');
  dbms_lob.append(lc_cClob, '<directcosts>');
	<<ACTV_DC_LOOP>>
  l_array_action_dc := csv_util_pkg.csv_to_array (in_vActionDirectCosts);
  for dc in l_array_action_dc.first .. l_array_action_dc.last loop
    p('<dc id="' || dc || '" />');
	  dbms_lob.append(lc_cClob, '<dc id="' || dc || '"
			  directcostsid="0"
			  rate="0"
			  dcname="' || l_array_action_dc(dc) || '"
			  productline="' || in_nProductlinelistid || '"
			  estunitsrate="0"
			  baselineunits="0"
			  baselinetotal="0"
			  gcelinetotal="0"
			  gcelineunits="0"
			  useredited="0"
			  />');
	  --dbms_lob.append(lc_cClob, '</dc>');
	end loop ACTV_DC_LOOP;
  
  p('</directcosts>');
	dbms_lob.append(lc_cClob, '</directcosts>');


  p('</Action>');

			dbms_lob.append(lc_cClob, '</Action>');
    
  lc_vFilename := 'CE_' || in_vDeliveryOrderName || '.xml';


p('</CE>');
dbms_lob.append(lc_cClob,'</CE>');



lc_bBlob := CLOB2BLOB(lc_cClob);

select SPIDERS_DATA.SPD_S_XML_FILES.nextval into lc_nXMLFileid from dual;


insert into spd_t_xml_files (xmlfileid, mime_type, blob_content, apexname,  filename)
values (lc_nXMLFileid,'text/xml',lc_bBlob, lc_vFilename, lc_vFilename);
commit;

--htp.p(to_char(lc_nXMLFileid));

--exception
--when others then
  --execute immediate 'insert into debug (functionname, message) values (''BB_P_GENERIC_CE_XML_OUTPUT'',' || SQLERRM || ');';
  --p(sqlerrm);

END BB_P_GENERIC_CE_XML_OUTPUT;
 

/
