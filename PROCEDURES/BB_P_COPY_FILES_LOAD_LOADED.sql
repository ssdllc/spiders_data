--------------------------------------------------------
--  DDL for Procedure BB_P_COPY_FILES_LOAD_LOADED
--------------------------------------------------------
set define off;

  CREATE OR REPLACE EDITIONABLE PROCEDURE "SPIDERS_DATA"."BB_P_COPY_FILES_LOAD_LOADED" 
(
  IN_VFOLDER IN VARCHAR2 
, IN_VFOLDERID IN VARCHAR2 
, IN_NADOID IN NUMBER 
, IN_NFILEID IN NUMBER 
, IN_VPROCESSIMAGES IN VARCHAR2 
) AS 

lc_vMessage varchar2(2000);
lc_vSuccess varchar2(2000);
lc_vFolder varchar2(2000) := in_vFolder;

BEGIN
  
    insert into debug (functionname, input, message) values ('BB_P_COPY_FILES_LOAD_LOADED',lc_vFolder,'start');
    spiders_utl.utils.copy_spiders_files(lc_vFolder,lc_vFolder, lc_vMessage,lc_vSuccess);
	insert into debug (functionname, input, message) values ('BB_P_COPY_FILES_LOAD_LOADED',lc_vFolder,'lc_vMessage:' || lc_vMessage || ',lc_vSuccess:' || lc_vSuccess);
	
	/*success is returned right now, changing to a Y but not destroying the original concept*/
  if upper(lc_vSuccess) = upper('{"message":"success"}') then
    lc_vSuccess := 'Y';
  end if;
  
    if upper(lc_vSuccess) = upper('Y') then
        insert into debug (functionname, input) values ('BB_P_COPY_FILES_LOAD_LOADED',in_nADOID);
        xml_tools.import_xml(IN_nADOID=>in_nADOID, IN_vProcessImages=>IN_VPROCESSIMAGES); --dont need this i think. IN_vFILENAME=>in_nFileid, 
        insert into debug (functionname, input, message) values ('BB_P_COPY_FILES_LOAD_LOADED',lc_vFolder,'lc_vMessage:' || lc_vMessage || ',lc_vSuccess:' || lc_vSuccess);
    else
        xml_tools.import_xml(IN_nADOID=>in_nADOID, IN_vProcessImages=>IN_VPROCESSIMAGES); --dont need this i think. IN_vFILENAME=>in_nFileid, 
        insert into debug (functionname, input, message) values ('BB_P_COPY_FILES_LOAD_LOADED',lc_vFolder,'File Copy Error: ' || lc_vMessage);
    end if;

	insert into debug (functionname, input, message) values ('BB_P_COPY_FILES_LOAD_LOADED',lc_vFolder,'end');

exception when others then
        execute immediate 'insert into debug (functionname, message) values (''BB_P_COPY_FILES_LOAD_LOADED'',''' || sqlerrm || ''')';
END BB_P_COPY_FILES_LOAD_LOADED;
 

/
