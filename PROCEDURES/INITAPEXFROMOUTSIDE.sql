--------------------------------------------------------
--  DDL for Procedure INITAPEXFROMOUTSIDE
--------------------------------------------------------
set define off;

  CREATE OR REPLACE EDITIONABLE PROCEDURE "SPIDERS_DATA"."INITAPEXFROMOUTSIDE" 
      ( i_app_id     in number
      , i_page_id      in number
      , i_apex_user    in varchar2
      ) is
    v_cgivar_name  owa.vc_arr;
    v_cgivar_val   owa.vc_arr;
    v_workspace_id NUMBER;
    v_app_id       number;
      
      BEGIN
    -- set up cgi environment

   
    --htp.init;
    
    v_cgivar_name(1) := 'REQUEST_PROTOCOL';
    v_cgivar_val(1)  := 'HTTP';
    owa.init_cgi_env
      ( num_params => v_cgivar_name.count
      , param_name => v_cgivar_name
      , param_val  => v_cgivar_val
      );
    --
    -- load apex IDs by application name
    --
    
    /*
    SELECT workspace_id,
           application_id
    INTO v_workspace_id,
         v_app_id
    FROM apex_applications
    WHERE application_id=i_app_id;
    */
    --
    -- set up apex workspace
    --
    --wwv_flow_api.set_security_group_id(v_workspace_id);
    
    wwv_flow_api.set_security_group_id(1066331788022533);
    
    --
    -- set up apex session vars
    --
    
    

     apex_application.g_instance     := wwv_flow_custom_auth.get_next_session_id;

    apex_application.g_flow_id      := v_app_id;
    apex_application.g_flow_step_id := i_page_id;
    --
    -- "login"
    --
    
    apex_custom_auth.define_user_session
      ( p_user       => i_apex_user
      , p_session_id => apex_application.g_instance
      );
      
    wwv_flow_custom_auth_std.post_login
      ( p_uname      => i_apex_user
      , p_session_id => apex_application.g_instance
      , p_flow_page  => apex_application.g_flow_id
                     || ':'
                     || apex_application.g_flow_step_id
      );
      
      
      
exception 
  when others then
    htp.p('{"error":"' || sqlerrm || '"}');

      
end initApexFromOutside;
 

/
