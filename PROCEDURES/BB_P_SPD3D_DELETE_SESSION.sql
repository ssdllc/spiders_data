--------------------------------------------------------
--  DDL for Procedure BB_P_SPD3D_DELETE_SESSION
--------------------------------------------------------
set define off;

  CREATE OR REPLACE EDITIONABLE PROCEDURE "SPIDERS_DATA"."BB_P_SPD3D_DELETE_SESSION" (in_vSessionKey varchar2, in_vUsername varchar2 default '')
as

lc_vSessionKey varchar2(255) := in_vSessionKey;
lc_nSessionId number;
lc_vUsername varchar2(255);
lc_nContinue number :=1;

procedure log(in_vDescription varchar2, in_vMessage varchar2 default '') is

begin

  execute immediate 'insert into debug (functionname, input, message) values (''deleteSession.log'',''' || in_vDescription || ''',''' || in_vMessage || ''')';
  
  exception when others then
      execute immediate 'insert into debug (functionname, input, message) values (''deleteSession.log'',''none'',''error: ' || sqlerrm || ''')';
end;


function isSessionDeletable(in_nSessionId number, in_vUsername varchar2) return number
is 

lc_nCount number :=0;
lc_nReturn number :=0;

begin
  
  log('isSessionDeletable:start');
  
  log('isSessionDeletable:sql','select count(sessionid) from spd_t_telestrator_drnk_history where sessionid = ' || in_nSessionId || ' and upper(username) <> ' || in_vUsername);
  for rec in (select count(sessionid) theCount from spd_t_telestrator_drnk_history where sessionid = in_nSessionId and upper(username) <> in_vUsername)
  loop
    lc_nCount := rec.thecount;
  end loop;
  
  if lc_nCount = 0 then
    lc_nReturn := 1;
  else 
    lc_nReturn := 0;
  end if;
  log('isSessionDeletable:end',lc_nReturn);
  return lc_nReturn;

  exception when others then
      execute immediate 'insert into debug (functionname, input, message) values (''deleteSession.isSessionDeletable'',''' || in_nSessionId || ':' || in_vUsername || ''',''error: ' || sqlerrm || ''')';
      return 0;
end;

function checkDropObjects(in_nSessionId number, in_vUsername varchar2) return number
is 

lc_nCount number :=0;
lc_nReturn number :=0;

begin
  
  log('checkDropObjects:start');
  select count(sessionid) into lc_nCount from spd_t_telestrator_drop_objects where sessionid = in_nSessionId and upper(created_by) <> in_vUsername;
  if lc_nCount = 0 then
    lc_nReturn := 1;
  else 
    lc_nReturn := 0;
  end if;
  log('checkDropObjects:end');

  return lc_nReturn;


  exception when others then
      execute immediate 'insert into debug (functionname, input, message) values (''deleteSession.checkDropObjects'',''' || in_nSessionId || ':' || in_vUsername || ''',''error: ' || sqlerrm || ''')';
      return 0;
end;

function delete_(in_nSessionId number) return number 
is

begin

  log('delete_:start');
  execute immediate 'delete from spd_t_telestrator_drop_objects where sessionid =' || in_nSessionId;
  execute immediate 'delete from spd_t_telestrator_log where sessionid =' ||  in_nSessionId;
  execute immediate 'delete from spd_t_telestrator_drnk_history where sessionid =' ||  in_nSessionId;
  execute immediate 'delete from spd_t_telestrator_sessions where sessionid =' ||  in_nSessionId;
  commit;
  log('delete_:end');

  return 1;
  
  exception when others then
      execute immediate 'insert into debug (functionname, input, message) values (''deleteSession.delete'',''' || in_nSessionId || ''',''error: ' || sqlerrm || ''')';
      return 0;
end;

function setUsername(in_vUsername varchar2) return varchar2 is

lc_vReturn varchar2(255);

begin

  log('setUsername:start',in_vUsername);
  if in_vUsername = '' then
  	log('setUsername:1');
    lc_vReturn := upper(v('APP_USER'));
  else
  	log('setUsername:2');
  	if in_vUsername is null then 
  		log('setUsername:3');
  		lc_vReturn := upper(v('APP_USER'));
  	else
  		log('setUsername:4');
		lc_vReturn := upper(in_vUsername);
  	end if;
  end if;
  log('setUsername:end',lc_vReturn);
  return lc_vReturn;


  exception when others then
      execute immediate 'insert into debug (functionname, input, message) values (''deleteSession.setUsername'',''' || in_vUsername || ''',''error: ' || sqlerrm || ''')';
      return 'e';
end;

function setSessionId(in_vSessionKey varchar2) return number is 

lc_nReturn number :=0;
begin

  log('setSessionId:start');
  for rec in (select sessionid from spd_t_telestrator_sessions where sessionkey = in_vSessionKey)
  loop
    lc_nReturn := rec.sessionid;
  end loop;
  log('setSessionId:end'); 
  return lc_nReturn;


  exception when others then
      execute immediate 'insert into debug (functionname, input, message) values (''deleteSession.setUsername'',''' || in_vUsername || ''',''error: ' || sqlerrm || ''')';
      return lc_nReturn;
end;

procedure results(in_nContinue number) is

lc_jJson json;
begin

  log('results:start');
  lc_jJson := json();
  
  if  in_nContinue = 1 then
    lc_jJson := json('{"message":"success"}');
  else
    lc_jJson := json('{"message":"SessionNotDeleted"}');
  end if;
  
  lc_jJson.htp;
  log('results:end');

  exception when others then
      execute immediate 'insert into debug (functionname, input, message) values (''deleteSession.results'',' || in_nContinue || ',''error: ' || sqlerrm || ''')';

end;

begin

  log('BB_P_SPD3D_DELETE_SESSION:start');
  lc_vUsername := setUsername(in_vUsername);
  lc_nSessionId := setSessionId(lc_vSessionKey);
  
  log('lc_vUsername',lc_vUsername);
  log('lc_nSessionId',lc_nSessionId);

  lc_nContinue := isSessionDeletable(lc_nSessionId,lc_vUsername);
  if lc_nContinue = 1 then
  	log('1');
    --this session has only been accessed by the current user, delete it
    lc_nContinue := delete_(lc_nSessionId);
  else
  	log('2');
    --this session has been accessed by multiple people, but if there are no drop objects it can be deleted
    lc_nContinue := checkDropObjects(lc_nSessionId,lc_vUsername);
    if lc_nContinue = 1 then
    	log('3');
      lc_nContinue := delete_(lc_nSessionId);
    else
    	log('4');
      --this session has drop objects from another user, dont delete it, send a message to the browser
      lc_nContinue := 0;
    end if;
  end if;
  
  log('lc_nContinue',lc_nContinue);

  results(lc_nContinue);
  log('BB_P_SPD3D_DELETE_SESSION:end');
  
  exception when others then
      execute immediate 'insert into debug (functionname, input, message) values (''deleteSession'',''' || in_vSessionKey || ''',''error: ' || sqlerrm || ''')';

end;
 

/
