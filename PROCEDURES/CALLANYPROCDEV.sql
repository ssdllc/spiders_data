--------------------------------------------------------
--  DDL for Procedure CALLANYPROCDEV
--------------------------------------------------------
set define off;

  CREATE OR REPLACE EDITIONABLE PROCEDURE "SPIDERS_DATA"."CALLANYPROCDEV" (in_procName  varchar2,in_args  varchar2) AS
  lc_result varchar2(256);
begin

	--CREATE APEX SESSION FOR DEV TESTING
	spiders_data.initApexFromOutside(
		i_app_id => 123,
		i_page_id => 900,
		i_apex_user => 'MIKE@SSDLLC.BIZ' --'BRIAN.D.BEASLEY@NAVY.MIL' --  --replace this if you parse in_args as a JSON object, looking for a app_user attribute.
    ); --note: app page id is irrelevant

	lc_result := SPIDERS_DATA.PKG_CALL_ANY_PROC.f_call_any_proc(in_procName, in_args);

exception 
  when others then
    htp.p('{"errorFromCallAnyProcDev":"' || sqlerrm || '"}');

end;
 

/
