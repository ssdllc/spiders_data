--------------------------------------------------------
--  DDL for Procedure BB_P_ERROR
--------------------------------------------------------
set define off;

  CREATE OR REPLACE EDITIONABLE PROCEDURE "SPIDERS_DATA"."BB_P_ERROR" (in_jJSON json) as

  lc_jJson json;
  
  lc_vFunctionName varchar2(255);
  lc_vInput number; --varchar2(4000);
  lc_vInput_val json_value;
  lc_vType varchar2(255);
  lc_vMessage varchar2(2000);
  lc_jlMessages json_list;
  lc_vStatement varchar2(12000);
  
  lc_rec_list json_list;
  lc_rec json;
  lc_rec_val json_value;
  
  lc_jErrorsToBrowser json;
  lc_jlErrorTableMessages json_list;
  
  lc_nSequence number;
  
  json_error EXCEPTION;
  
  lc_vInput_num number;
begin


  --parse json
  lc_jJson := in_jJSON;
  lc_jErrorsToBrowser := json();
  lc_jlErrorTableMessages := json_list();

  --Test for existence of jsonp attribute for local testing debug
  if(lc_jJson.exist('function')) THEN
    
      if(lc_jJson.exist('input')) THEN
        if(lc_jJson.exist('messages')) THEN
          if (lc_jJson.exist('function')) then
            lc_vFunctionName := lc_jJson.get('function').get_string;
          end if;
          lc_vInput := lc_jJson.get('input').get_number; --.get_string;
          lc_jlMessages := json_list(in_jJson.get('messages'));
          for i in 1..lc_jlMessages.count loop
            --add record to log table

            lc_rec_list := json_list(lc_jlMessages.get(i));
            
            for x in 1..lc_rec_list.count loop
              --htp.p('lc_rec_list.count = '||lc_rec_list.count);
              lc_rec_val := lc_rec_list.get(x);
              lc_rec := json(lc_rec_val);

              for y in 1..lc_rec.count loop
                --htp.p('lc_rec.count = '||lc_rec.count);
                
                lc_vType := lc_rec.get(y).mapname; --mapname was a bitch to find in the JSON_VALUE type. 
                lc_vMessage := lc_rec.get(y).value_of;
                select spd_s_debug.nextval into lc_nSequence from dual;
                lc_jlErrorTableMessages.append(lc_nSequence);
                --htp.p('inserting with seq: '||lc_nSequence);
                lc_vStatement := 'insert into debug (debugid, functionname, input, message, messagetype, username) values (' || lc_nSequence || ',''' || lc_vFunctionName || ''',''' || lc_vInput || ''',''' || lc_vMessage || ''',''' || lc_vType || ''',''' || v('APP_USER') || ''')';
                --htp.p(lc_vStatement);
                execute immediate lc_vStatement;
                --htp.p('DONE WITH execute immediate');
                commit;
                --htp.p('COMMIT HERE');
                
              end loop;
            end loop;            
          end loop;
          
        else
          --in JSON doesnt have messages array
          null;
        end if;
      else
        --in JSON doesnt have an input attribute
        null;
      end if;
  else
    --in JSON doesnt have a function attribute
    null;
  end if;
  
  --output to browser the error
  --!!WHAT ARE WE GOING TO SEND TO THE BROWSER?
  --We send a list of id's that were generated.
  
  lc_jErrorsToBrowser.put('status','error');
  lc_jErrorsToBrowser.put('messages',lc_jlErrorTableMessages);
  lc_jErrorsToBrowser.htp;
  
  --exception, output to browser something
  
  exception
    when json_error then
      lc_jErrorsToBrowser := json();
      htp.p('ERROR json_error sqlerm');
      lc_jErrorsToBrowser.put('sqlerrm',SQLERRM);
      lc_jErrorsToBrowser.htp;
      
    when others then
      lc_jErrorsToBrowser := json();
      htp.p('ERROR OTHERS sqlerm');
      lc_jErrorsToBrowser.put('sqlerrm',SQLERRM);
      lc_jErrorsToBrowser.htp;
  
end;


/* STUBB TEST



declare

in_jJson SPIDERS_DATA.JSON;

begin

in_jJson := json('
{
"function":"userRoles",
"input" : "this is what was passed in",
"messages":[
 [{"row":"1"},{"error":"Error DML-0001 There has been an error processing the request"}]
 ,[{"row":"2"},{"error":"Error DML-0002 There has been an error processing the request"}]
 ,[{"row":"3"},{"error":"Error DML-0003 There has been an error processing the request"}]
 ,[{"row":"4"},{"error":"Error DML-0004 There has been an error processing the request"}]
 ]}');
 
 
bb_p_error(in_jJson);
 

end;

*/
 

/
