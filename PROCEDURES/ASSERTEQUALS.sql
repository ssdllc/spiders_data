--------------------------------------------------------
--  DDL for Procedure ASSERTEQUALS
--------------------------------------------------------
set define off;

  CREATE OR REPLACE EDITIONABLE PROCEDURE "SPIDERS_DATA"."ASSERTEQUALS" (o1 varchar2, o2 varchar2, functionName varchar2, dataType varchar2) IS

begin

  if(o1 = o2) then 
    htp.p('Testing '||functionName||' with '|| dataType ||': SUCCESS ');
  else
    htp.p('Testing '||functionName||' with '|| dataType ||': FAILURE ');
  end if;

end;
 

/
