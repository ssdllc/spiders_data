--------------------------------------------------------
--  DDL for Procedure BB_P_UPDATE_XML_FAC_COMPLETE
--------------------------------------------------------
set define off;

  CREATE OR REPLACE EDITIONABLE PROCEDURE "SPIDERS_DATA"."BB_P_UPDATE_XML_FAC_COMPLETE" (in_jJson json) AS

    lc_jJson json;
    lc_data json_list;
    lc_rec_val json_value;
    lc_rec json;
    lc_employeeid_val json_value; --
    lc_employeeid varchar(256);
    lc_cnt number;
    lc_nAdoid number :=0;
    lc_adoid_val json_value;
    lc_nCountXMLFac number :=0;
    lc_nCountSpidersFac number :=0;
    lc_nAPID number;

    
    lc_jlDML json_list;
    lc_jResults json;
    lc_jMessages json;
    lc_vDMLResult varchar2(255);
    lc_ret_str varchar(512);
    lc_ret json;

    thesql varchar2(2000) := '';

    lc_vWhere varchar2(2000);

    --error stuff
    lc_jError json;
    lc_jlMessages json_list;
    json_error EXCEPTION;


BEGIN
/*
stub data
{"module":"xmlImportManager","target":"itDoesntMatterNotBeingCalledFromTheBrowser","data":["ADOID":12345]}
*/

    lc_jMessages := json();
    lc_jlMessages := json_list();
    lc_jResults := json();
    lc_jlDML := json_list();
    
if(in_jJson.exist('data')) then
    if(in_jJson.get('data').is_array) then
        lc_data := json_list(in_jJson.get('data'));
        lc_rec_val := lc_data.get(1); --return null on outofbounds
        if (lc_rec_val is not null) then
            lc_rec := json(lc_rec_val);
            if (lc_rec.exist('ADOID')) then
                lc_adoid_val := lc_rec.get('ADOID');
                lc_nAdoid := lc_adoid_val.get_number;
            else
                lc_nAdoid :=0;
            end if;
        end if;
    end if;
end if;



if lc_nADOID > 0 then
    select count(*) into lc_nCountXMLFac from spd_t_xml_facilities_match xf, spd_t_do_facilities sf where xf.adoid = lc_nADOID and xf.dofid = sf.dofid;
    --The new way we are going to do it is to test if there are any dofids that are null or zero
    select count(*) into lc_nCountSpidersFac from spd_t_xml_facilities_match xf where xf.adoid = lc_nADOID;
    
    --The original way to test for complete was to check against the delivery order facilities table. then if the xmlfac.dofid count matched the do_facs count then complete.
    --select count(*) into lc_nCountSpidersFac from spd_t_do_facilities where adoid = lc_nADOID;
    --select apid into lc_nAPID from spd_t_actn_dos where adoid = lc_nADOID;
    
    delete from spd_t_import_log where adoid = lc_nADOID and import_section = 'Facility Match Complete';
           
    if lc_nCountXMLFac = lc_nCountSpidersFac then
        --this xml facility match is complete, update the table
        thesql := 'insert into
        spd_t_import_log (
            adoid,
            import_section,
            grouping,
            count_of_records,
            total_records
        )
        values (
            ' || lc_nADOID || ',
            ''Facility Match Complete'',
            ''Facility Match Complete'',
            ' || lc_nCountXMLFac || ',
            ' || lc_nCountSpidersFac || '
        )';
        

        lc_jlDML.append(thesql);
        lc_jResults.put('results',lc_jlDML);
        lc_vDMLResult := BB_F_DML(lc_jResults, lc_jMessages);
    else
        --this xml facility match is not complete, delete the record from the table
        thesql := 'delete from spd_t_import_log where adoid = ' || lc_nADOID || ' and import_section = ''Facility Match Complete''';

        lc_jlDML.append(thesql);
        lc_jResults.put('results',lc_jlDML);
        lc_vDMLResult := BB_F_DML(lc_jResults, lc_jMessages);
    end if;
else
    --throw an error eventually
    insert into debug (input) values ('error in xml facility match');
end if;



exception
when json_error then
    --htp.p('raised json_error in update func');
    lc_jError := json();
    lc_jError.put('function','BB_P_UPDATE_XML_FAC_COMPLETE');
    lc_jError.put('input',in_jJson.to_char);
    lc_jError.put('messages',lc_jlMessages);
    BB_P_ERROR(lc_jError);
    --htp.p('returning n');
when others then
    lc_jError := json();
    lc_jError.put('function','BB_P_UPDATE_XML_FAC_COMPLETE');
    lc_jError.put('input','');
    lc_jlMessages.append(json_list('[{error:''UXFMC.000X ' || SQLERRM || '''}]'));
    lc_jError.put('messages',lc_jlMessages);
    BB_P_ERROR(lc_jError);


END BB_P_UPDATE_XML_FAC_COMPLETE;
 

/
