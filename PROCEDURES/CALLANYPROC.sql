--------------------------------------------------------
--  DDL for Procedure CALLANYPROC
--------------------------------------------------------
set define off;

  CREATE OR REPLACE EDITIONABLE PROCEDURE "SPIDERS_DATA"."CALLANYPROC" (in_procName  varchar2,in_args  varchar2) AS

begin
--dbms_output.put_line(in_procName);
--dbms_output.put_line(in_args);

--apex_030200.APEX_CUSTOM_AUTH.apex_authentication.login('MIKE@SSDLLC.BIZ', 'q1');





spiders_data.initApexFromOutside(
    i_app_id => 123,
    i_page_id => 900,
    i_apex_user => 'MIKE@SSDLLC.BIZ' --'BRIAN.D.BEASLEY@NAVY.MIL' --  --replace this if you parse in_args as a JSON object, looking for a app_user attribute.
    ); --note: app page id is irrelevant

    

/*

  SPIDERS_DATA.sp_create_apex_session(
    p_app_id => '123',
    p_app_user => 'MIKE@SSDLLC.BIZ', --replace this if you parse in_args as a JSON object, looking for a app_user attribute.
    p_app_page_id => '900'); --note: app page id is irrelevant
  */
  

  if substr(in_procName,0,7)='SPIDERS' then
  --dbms_output.put_line('kjhk');
    --htp.p('BEGIN '||in_procName||' ('''||in_args||'''); END;');
		--execute immediate ('BEGIN '||in_procName||' ('''||in_args||'''); END;');
    execute immediate ('BEGIN '||in_procName||' ('||in_args||'); END;');
	end if;



exception 
  when others then
    htp.p('{"errorFromCallAnyProc":"' || sqlerrm || '"}');

end;
 

/
