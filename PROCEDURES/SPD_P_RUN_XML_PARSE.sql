--------------------------------------------------------
--  DDL for Procedure SPD_P_RUN_XML_PARSE
--------------------------------------------------------
set define off;

  CREATE OR REPLACE  PROCEDURE "SPIDERS_DATA"."SPD_P_RUN_XML_PARSE" (in_cJSON clob, in_vUser varchar2) AS 

lc_jReturn json;

BEGIN
  execute immediate 'insert into debug (functionname, input, message) values (''spd_p_run_xml_parse' || ''',''' || 'beginning spd_p_run_xml_parse' || ''',''' || 'before: pkg_xml_parse.collectXMLData(in_cJSON, in_vUser);' || ''')';
  commit;
  
  lc_jReturn := pkg_xml_parse.collectXMLData(in_cJSON, in_vUser);
  
  execute immediate 'insert into debug (functionname, input, message) values (''spd_p_run_xml_parse' || ''',''' || 'ending spd_p_run_xml_parse' || ''',''' || 'after: pkg_xml_parse.collectXMLData(in_cJSON, in_vUser);' || ''')';
  commit;
  
exception
  when others then
    execute immediate 'insert into debug (functionname, input, message) values (''spd_p_run_xml_parse' || ''',''' || 'error in spd_p_run_xml_parse' || ''',''' || sqlerrm || ''')';
  
END SPD_P_RUN_XML_PARSE;
 

/
