--------------------------------------------------------
--  DDL for Procedure DOWNLOAD_BFILE_RAW2
--------------------------------------------------------
set define off;

  CREATE OR REPLACE EDITIONABLE PROCEDURE "SPIDERS_DATA"."DOWNLOAD_BFILE_RAW2" (in_nFileId number) as 

    lc_bfBfile bfile;
    l_bBlob    blob;
    l_amt    number default 30;
    l_off   number default 1;
    l_raw   raw(4096);
    
    lc_vDirectory varchar2(255);
    lc_vFile varchar2(255);
    lc_nContinue number;
    v_length number;
begin

      
      for rec in (select '1234' folderid, the_bfile from spd_t_dt_downloads where dtdownloadid = in_nFileId and dbms_lob.fileexists(the_bfile) !=0)
        loop
          lc_vDirectory := 'SPD_' || rec.folderid;
          lc_vFile := getfilenamefrombfile(rec.the_bfile);
          lc_nContinue := 1;
        end loop;
      
      if lc_nContinue = 1 then
              
          l_bBlob := bfiletoblob(lc_vDirectory,lc_vFile);
          v_length := DBMS_LOB.getlength (l_bBlob);
      
              -- make sure to change this for your type!
          OWA_UTIL.mime_header ('application/octet', FALSE);
          HTP.p ('Content-length: ' || v_length);
          -- the filename will be used by the browser if the users does a save as
          HTP.p ('Content-Disposition:  attachment; filename="'|| lc_vFile|| '"');
          -- close the headers
          OWA_UTIL.http_header_close;
          
          
          wpg_docload.download_file(l_bBlob);
      
      end if;
      
      
      
    exception
      when others then
        htp.p(sqlerrm);
end;
 

/
