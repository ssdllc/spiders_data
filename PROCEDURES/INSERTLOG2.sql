--------------------------------------------------------
--  DDL for Procedure INSERTLOG2
--------------------------------------------------------
set define off;

  CREATE OR REPLACE EDITIONABLE PROCEDURE "SPIDERS_DATA"."INSERTLOG2" (in_nDOID number, in_vAction varchar2, in_vStatus varchar2, in_nCurrentPosition number default 0, in_nMaxPosition number default 1) AS
pragma autonomous_transaction;
lc_vImportSection varchar2(500);
lc_nExistingImportID number := 0;
lc_nCountOfRecord number:= 0;
lc_vFunctionName varchar2(10) := 'insertLog';
lc_nMaxRecords number:=1;

BEGIN
  
  lc_vImportSection := in_vAction || ' ' || in_vStatus;
  

  lc_nMaxRecords := in_nMaxPosition;
  lc_nCountOfRecord := in_nCurrentPosition;
  
  
  FOR rec in (SELECT IMPORTLOGID FROM SPD_T_IMPORT_LOG WHERE ADOID = in_nDOID AND IMPORT_SECTION = in_vAction) LOOP
    lc_nExistingImportID := rec.IMPORTLOGID;
  END LOOP;
  
  IF lc_nExistingImportID = 0 OR lc_nExistingImportID IS NULL THEN
    INSERT INTO 
      SPD_T_IMPORT_LOG (
        IMPORTLOGID, 
        ADOID, 
        IMPORT_SECTION, 
        GROUPING, 
        TOTAL_RECORDS, 
        COUNT_OF_RECORDS
      ) 
      values 
      (
        '',
        in_nDOID,
        in_vAction,
        lc_vImportSection,
        lc_nMaxRecords,
        lc_nCountOfRecord
      );
  ELSE
    UPDATE 
      SPD_T_IMPORT_LOG 
        SET 
          IMPORT_SECTION = in_vAction, 
          GROUPING = lc_vImportSection, 
          COUNT_OF_RECORDS = lc_nCountOfRecord,
          TOTAL_RECORDS = lc_nMaxRecords
        WHERE 
          IMPORTLOGID = lc_nExistingImportID;
  END IF;
  
  commit;

EXCEPTION WHEN OTHERS THEN
  htp.p('insert log 2 error ' || sqlerrm || '''');
  execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' dml error: ' || sqlerrm || ''')';
END;
 

/
