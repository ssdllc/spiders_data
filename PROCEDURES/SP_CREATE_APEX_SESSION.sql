--------------------------------------------------------
--  File created - Monday-August-06-2018   
--------------------------------------------------------
--------------------------------------------------------
--  DDL for Procedure SP_CREATE_APEX_SESSION
--------------------------------------------------------
set define off;

  CREATE OR REPLACE EDITIONABLE PROCEDURE "SPIDERS_DATA"."SP_CREATE_APEX_SESSION" (
  p_app_id IN apex_applications.application_id%TYPE,
  p_app_user IN apex_workspace_activity_log.apex_user%TYPE,
  p_app_page_id IN apex_application_pages.page_id%TYPE DEFAULT 1)
AS
  l_workspace_id apex_applications.workspace_id%TYPE;
  l_cgivar_name  owa.vc_arr;
  l_cgivar_val   owa.vc_arr;
  
  l_nextID number;
BEGIN

  htp.init;

  l_cgivar_name(1) := 'REQUEST_PROTOCOL';
  l_cgivar_val(1) := 'HTTP';

  owa.init_cgi_env(
    num_params => 1,
    param_name => l_cgivar_name,
    param_val => l_cgivar_val );
    
    
    
/*
  SELECT workspace_id
  INTO l_workspace_id
  FROM apex_030200.apex_applications
  WHERE application_id = p_app_id;
*/
  l_workspace_id := 2227422993334902; -- 2227422993334902; -- ...241 =  1066331788022533;  ...138 = 2227422993334902

  wwv_flow_api.set_security_group_id(l_workspace_id);

  apex_application.g_instance := 1;
  apex_application.g_flow_id := p_app_id;
  apex_application.g_flow_step_id := p_app_page_id;
/*
  apex_030200.apex_custom_auth.post_login(
    p_uname => p_app_user,
    p_session_id => null, -- could use APEX_CUSTOM_AUTH.GET_NEXT_SESSION_ID
    p_app_page => apex_application.g_flow_id||':'||p_app_page_id);
  */  
  --htp.p(apex_030200.APEX_CUSTOM_AUTH.GET_NEXT_SESSION_ID);
  
  -- Application Express 5.0.4.00.12
  
  --l_nextID := apex_030200.APEX_CUSTOM_AUTH.GET_NEXT_SESSION_ID; --updated by tc 20180619
  l_nextID := APEX_CUSTOM_AUTH.GET_NEXT_SESSION_ID;
  --htp.p(l_nextID);
  /*
  apex_030200.apex_custom_auth.post_login(
    p_uname => p_app_user,
    p_session_id => l_nextID, -- could use APEX_CUSTOM_AUTH.GET_NEXT_SESSION_ID
    p_app_page => '123:900');
    */
    wwv_flow_custom_auth_std.post_login
      ( p_uname      => p_app_user
      , p_session_id => l_nextID
      , p_flow_page  => '123:900'
      ); --changed 212 to 900
END;

/
