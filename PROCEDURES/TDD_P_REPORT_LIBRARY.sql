--------------------------------------------------------
--  DDL for Procedure TDD_P_REPORT_LIBRARY
--------------------------------------------------------
set define off;

  CREATE OR REPLACE EDITIONABLE PROCEDURE "SPIDERS_DATA"."TDD_P_REPORT_LIBRARY" as 

  pass_count number := 0;
  fail_count number := 0;
  total_count number := 0;
  str varchar2(200);
  lc_nId number;
  
  
  procedure p (v varchar2) as 
  begin
    dbms_output.put_line(v);
  end;
  
  procedure pass(str varchar2) as
  begin
    pass_count := pass_count + 1;
    total_count := total_count + 1;
    htp.p('OK: '||str);
  end;
  
  procedure fail(str varchar2) as
  begin
    fail_count := fail_count + 1;
    total_count := total_count + 1;
    htp.p('FAILED: '||str);
    
    execute immediate 'delete from spd_t_reports where reportid = 1';
    execute immediate 'delete from spd_t_report_files where reportid = 1';
    execute immediate 'delete from spd_t_action_planner where apid = 1';
    execute immediate 'delete from spd_t_report_files where reportfileid=1';
    execute immediate 'delete from spd_t_report_keywords where reportkeywordid = 1';
    commit;
    
  end;
  
  procedure assertTrue(b boolean) as
  begin
    if(not b) then raise_application_error(-20111, 'Test error'); end if;
  end;

  procedure assertFalse(b boolean) as
  begin
    if(b) then raise_application_error(-20111, 'Test error'); end if;
  end;

  procedure assertEquals(v varchar2, v2 varchar2) as
  begin
    if(v <> v2) then raise_application_error(-20111, 'Test error'); end if;
  end;

  procedure assertEquals(v number, v2 number) as
  begin
    if(v <> v2) then raise_application_error(-20111, 'Test error'); end if;
  end;
  
  procedure assertEqualsQuery(v number, v2 varchar2) as
    lc_nNumber number;
  begin
    execute immediate v2 into lc_nNumber;
    if(v <> lc_nNumber) then raise_application_error(-20111, 'Test error'); end if;
  end;
  
  procedure assertGreaterThan(n number, n2 number) as 
  begin
    if(n <= n2) then raise_application_error(-20111, 'Test error'); end if;
  end;
  
  procedure setId as 
  begin
    select spd_s_json_testsuite.nextval into lc_nId from dual;
  end;
  
  procedure saveResults as 
  begin
    execute immediate 'insert into spd_t_json_testsuite (pass_count, fail_count, total_count, test_name, timestamp) values (:1, :2, :3, :4, :5)' using
    pass_count,fail_count,total_count,'report library tests', sysdate;
    commit;
  exception
    when others then 
      p(sqlerrm);
  end;
  
  procedure postResults as 
    score number;
  begin
    score := round((pass_count/total_count),2)*100;
    htp.p('PASS: ' || pass_count);
    htp.p('FAIL: ' || fail_count);
    htp.p('TOTAL: ' || total_count);
    htp.p('SCORE: ' || score || '%');
  end;
begin

--lc_vSql := BB_F_C_REPORT_LIBRARY_KEYWORD(lc_jJson,lc_ojJson);
  str := 'BB_F_C_REPORT_LIBRARY_KEYWORD create ';
  declare
    lc_jJson json;
    lc_ojJson json;
    lc_jvData json_value;
    lc_nId number;
    lc_jMessages json;
    lc_vResult varchar2(2000);
  begin

    execute immediate 'insert into spd_t_reports (reportid) values (1)';
    execute immediate 'insert into spd_t_report_files (reportid, reportfileid) values (1,1)';
    
    lc_jJson := json('{"module":"reportLibrary","target":"createReportFileKeyword","data": [{"REPORTFILEID" : 1,"TEXT": "bueno2"}]}');
    lc_vResult := BB_F_C_REPORT_LIBRARY_KEYWORD(lc_jJson, lc_jMessages);
    lc_jJson := lc_jMessages;
--lc_jJson.htp;

    assertTrue(lc_jJson.exist('messages'));
    assertTrue(lc_jJson.exist('status'));
    assertTrue(lc_jJson.exist('id'));
    
    lc_jvData := lc_jJson.get('id');
    lc_nId := lc_jvData.get_number;
    assertGreaterThan(lc_nId,0);
    pass(str);

  execute immediate 'delete from spd_t_reports where reportid = 1';
  execute immediate 'delete from spd_t_report_files where reportfileid = 1';
  execute immediate 'delete from spd_t_report_keywords where reportkeywordid = ' || lc_nId;
    
  exception
    when others then 
    htp.p(sqlerrm);
    fail(str);
  end;
--lc_ojJson := BB_F_R_REPORT_LIBRARY_DD_YEARS(lc_jJson);
--lc_ojJson := BB_F_R_REPORT_LIBRARY_DD_PL(lc_jJson);
--lc_ojJson := BB_F_R_REPORT_LIBRARY_DD_STATE(lc_jJson);

--lc_ojJson := BB_F_R_REPORT_LIBRARY_FILES(lc_jJson);
--lc_ojJson := BB_F_R_REPORT_LIBRARY_ACTIONS(lc_jJson, lc_vSql);
--lc_ojJson := BB_F_R_REPORT_LIBRARY_REC(lc_jJson);
--lc_ojJson := BB_F_R_REPORT_LIBRARY_KEYWORDS(lc_jJson);

--lc_vSql := BB_F_CU_REPORT_LIBRARY_REC(lc_jJson,lc_ojJson); --create
--lc_vSql := BB_F_CU_REPORT_LIBRARY_REC(lc_jJson,lc_ojJson); --update

  setId;


  str := 'BB_F_R_REPORT_LIBRARY_DD_YEARS test';
  declare
    lc_jJson json;
    lc_ojJson json;
    lc_jlData json_list;
  begin
    lc_jJson := json('{"module":"reportLibrary","target":"reportLibraryDropDownYears","data":[]}');
    lc_ojJson := BB_F_R_REPORT_LIBRARY_DD_YEARS(lc_jJson);
   
    assertTrue(lc_ojJson.exist('names'));
    assertTrue(lc_ojJson.exist('data'));
    assertTrue(lc_ojJson.get('names').is_array);
    assertTrue(lc_ojJson.get('data').is_array);
    lc_jlData := json_list(lc_ojJson.get('data'));

    assertEquals(lc_jlData.count,30);
    pass(str);
  exception
    when others then fail(str);
  end;

  str := 'BB_F_R_REPORT_LIBRARY_DD_PL test';
  declare
    lc_jJson json;
    lc_ojJson json;
    lc_jlData json_list;
  begin
    lc_jJson := json('{"module":"reportLibrary","target":"reportLibraryDropDownProductlines","data":[]}');
    lc_ojJson := BB_F_R_REPORT_LIBRARY_DD_PL(lc_jJson);
   
    assertTrue(lc_ojJson.exist('names'));
    assertTrue(lc_ojJson.exist('data'));
    assertTrue(lc_ojJson.get('names').is_array);
    assertTrue(lc_ojJson.get('data').is_array);
    lc_jlData := json_list(lc_ojJson.get('data'));
--p(lc_jlData.count);
    assertEquals(lc_jlData.count,4);
    pass(str);
  exception
    when others then fail(str);
  end;


  str := 'BB_F_R_REPORT_LIBRARY_DD_STATE test';
  declare
    lc_jJson json;
    lc_ojJson json;
    lc_jlData json_list;
  begin
    lc_jJson := json('{"module":"reportLibrary","target":"reportLibraryDropDownStates","data":[]}');
    lc_ojJson := BB_F_R_REPORT_LIBRARY_DD_STATE(lc_jJson);
   
    assertTrue(lc_ojJson.exist('names'));
    assertTrue(lc_ojJson.exist('data'));
    assertTrue(lc_ojJson.get('names').is_array);
    assertTrue(lc_ojJson.get('data').is_array);
    lc_jlData := json_list(lc_ojJson.get('data'));
--p(lc_jlData.count);
    assertEquals(lc_jlData.count,38);
    pass(str);
  exception
    when others then fail(str);
  end;
  
  
  str := 'BB_F_R_REPORT_LIBRARY_FILES test';
  declare
    lc_jJson json;
    lc_ojJson json;
    lc_jlData json_list;
  begin

    execute immediate 'insert into spd_t_reports (reportid) values (1)'; 
    execute immediate 'insert into spd_t_report_files (reportid) values (1)';
    execute immediate 'insert into spd_t_report_files (reportid) values (1)';
    execute immediate 'insert into spd_t_report_files (reportid) values (1)';
    execute immediate 'insert into spd_t_report_files (reportid) values (1)';

    lc_jJson := json('{"module":"reportLibrary","target":"reportFiles","data":[{"REPORTID":1}]}');
    lc_ojJson := BB_F_R_REPORT_LIBRARY_FILES(lc_jJson);

    assertTrue(lc_ojJson.exist('names'));
    assertTrue(lc_ojJson.exist('data'));
    assertTrue(lc_ojJson.get('names').is_array);
    assertTrue(lc_ojJson.get('data').is_array);
    lc_jlData := json_list(lc_ojJson.get('data'));
--p(lc_jlData.count);
    assertEquals(lc_jlData.count,4);
    pass(str);
    
    execute immediate 'delete from spd_t_reports where reportid = 1';
    execute immediate 'delete from spd_t_report_files where reportid = 1';
    
  exception
    when others then fail(str);
  end;
  
  
--lc_ojJson := BB_F_R_REPORT_LIBRARY_ACTIONS(lc_jJson, lc_vSql);
  str := 'BB_F_R_REPORT_LIBRARY_ACTIONS productline = 100085';
  declare
    lc_jJson json;
    lc_ojJson json;
    lc_jlData json_list;
    lc_nCountOfRecords number :=0;
    lc_vSql varchar2(2000);
    lc_nProductlineListId number := 100085;
  begin

    select count(*) into lc_nCountOfRecords from spd_t_action_planner where productline_listid = lc_nProductlineListId;
    lc_jJson := json('{
    "module": "reportLibrary",
    "target": "reportLibraryActions",
    "aoData": [
        {
            "name": "draw",
            "value": 5
        },
        {
            "name": "columns",
            "value": [
                {
                    "data": 0,
                    "name": "ACTIVITY_NAME",
                    "searchable": true,
                    "orderable": true,
                    "search": {
                        "value": "",
                        "regex": false
                    }
                },
                {
                    "data": 1,
                    "name": "STATE",
                    "searchable": true,
                    "orderable": true,
                    "search": {
                        "value": "",
                        "regex": false
                    }
                }
            ]
        },
        {
            "name": "order",
            "value": [
                {
                    "column": 0,
                    "dir": "desc"
                }
            ]
        },
        {
            "name": "start",
            "value": 0
        },
        {
            "name": "length",
            "value": ' || lc_nCountOfRecords || '
        },
        {
            "name": "search",
            "value": {
                "value": "",
                "regex": false
            }
        }
    ],
    "data": [
        {
            "YEAR": "",
            "PRODUCTLINE": ' || lc_nProductlineListId || ',
            "STATE": ""
        }
    ]
}');
    lc_ojJson := BB_F_R_REPORT_LIBRARY_ACTIONS(lc_jJson,lc_vSql);
--htp.p(lc_vSql);
    assertTrue(lc_ojJson.exist('names'));
    assertTrue(lc_ojJson.exist('data'));
    assertTrue(lc_ojJson.get('names').is_array);
    assertTrue(lc_ojJson.get('data').is_array);
    lc_jlData := json_list(lc_ojJson.get('data'));
--p(lc_jlData.count);
    assertEquals(lc_jlData.count,lc_nCountOfRecords);
    pass(str);


  exception
    when others then fail(str);
  end;


 str := 'BB_F_R_REPORT_LIBRARY_ACTIONS productline = 100086';
  declare
    lc_jJson json;
    lc_ojJson json;
    lc_jlData json_list;
    lc_nCountOfRecords number :=0;
    lc_vSql varchar2(2000);
    lc_nProductlineListId number := 100086;
  begin

    select count(*) into lc_nCountOfRecords from spd_t_action_planner where productline_listid = lc_nProductlineListId;
    lc_jJson := json('{
    "module": "reportLibrary",
    "target": "reportLibraryActions",
    "aoData": [
        {
            "name": "draw",
            "value": 5
        },
        {
            "name": "columns",
            "value": [
                {
                    "data": 0,
                    "name": "ACTIVITY_NAME",
                    "searchable": true,
                    "orderable": true,
                    "search": {
                        "value": "",
                        "regex": false
                    }
                },
                {
                    "data": 1,
                    "name": "STATE",
                    "searchable": true,
                    "orderable": true,
                    "search": {
                        "value": "",
                        "regex": false
                    }
                }
            ]
        },
        {
            "name": "order",
            "value": [
                {
                    "column": 0,
                    "dir": "desc"
                }
            ]
        },
        {
            "name": "start",
            "value": 0
        },
        {
            "name": "length",
            "value": ' || lc_nCountOfRecords || '
        },
        {
            "name": "search",
            "value": {
                "value": "",
                "regex": false
            }
        }
    ],
    "data": [
        {
            "YEAR": "",
            "PRODUCTLINE": ' || lc_nProductlineListId || ',
            "STATE": ""
        }
    ]
}');
    lc_ojJson := BB_F_R_REPORT_LIBRARY_ACTIONS(lc_jJson,lc_vSql);
--htp.p(lc_vSql);
    assertTrue(lc_ojJson.exist('names'));
    assertTrue(lc_ojJson.exist('data'));
    assertTrue(lc_ojJson.get('names').is_array);
    assertTrue(lc_ojJson.get('data').is_array);
    lc_jlData := json_list(lc_ojJson.get('data'));
--p(lc_jlData.count);
    assertEquals(lc_jlData.count,lc_nCountOfRecords);
    pass(str);


  exception
    when others then fail(str);
  end;

 str := 'BB_F_R_REPORT_LIBRARY_ACTIONS productline = 100087';
  declare
    lc_jJson json;
    lc_ojJson json;
    lc_jlData json_list;
    lc_nCountOfRecords number :=0;
    lc_vSql varchar2(2000);
    lc_nProductlineListId number := 100087;
  begin

    select count(*) into lc_nCountOfRecords from spd_t_action_planner where productline_listid = lc_nProductlineListId;
    lc_jJson := json('{
    "module": "reportLibrary",
    "target": "reportLibraryActions",
    "aoData": [
        {
            "name": "draw",
            "value": 5
        },
        {
            "name": "columns",
            "value": [
                {
                    "data": 0,
                    "name": "ACTIVITY_NAME",
                    "searchable": true,
                    "orderable": true,
                    "search": {
                        "value": "",
                        "regex": false
                    }
                },
                {
                    "data": 1,
                    "name": "STATE",
                    "searchable": true,
                    "orderable": true,
                    "search": {
                        "value": "",
                        "regex": false
                    }
                }
            ]
        },
        {
            "name": "order",
            "value": [
                {
                    "column": 0,
                    "dir": "desc"
                }
            ]
        },
        {
            "name": "start",
            "value": 0
        },
        {
            "name": "length",
            "value": ' || lc_nCountOfRecords || '
        },
        {
            "name": "search",
            "value": {
                "value": "",
                "regex": false
            }
        }
    ],
    "data": [
        {
            "YEAR": "",
            "PRODUCTLINE": ' || lc_nProductlineListId || ',
            "STATE": ""
        }
    ]
}');
    lc_ojJson := BB_F_R_REPORT_LIBRARY_ACTIONS(lc_jJson,lc_vSql);
--htp.p(lc_vSql);
    assertTrue(lc_ojJson.exist('names'));
    assertTrue(lc_ojJson.exist('data'));
    assertTrue(lc_ojJson.get('names').is_array);
    assertTrue(lc_ojJson.get('data').is_array);
    lc_jlData := json_list(lc_ojJson.get('data'));
--p(lc_jlData.count);
    assertEquals(lc_jlData.count,lc_nCountOfRecords);
    pass(str);


  exception
    when others then fail(str);
  end; 
  
--lc_ojJson := BB_F_R_REPORT_LIBRARY_ACTIONS(lc_jJson, lc_vSql);
  str := 'BB_F_R_REPORT_LIBRARY_ACTIONS productline = 100085 and year = 2012';
  declare
    lc_jJson json;
    lc_ojJson json;
    lc_jlData json_list;
    lc_nCountOfRecords number :=0;
    lc_vSql varchar2(2000);
    lc_nProductlineListId number := 100085;
    lc_nYear number := 2012;
  begin

    select count(*) into lc_nCountOfRecords from spd_t_action_planner where productline_listid = lc_nProductlineListId and planned_year = lc_nYear;
    lc_jJson := json('{
    "module": "reportLibrary",
    "target": "reportLibraryActions",
    "aoData": [
        {
            "name": "draw",
            "value": 5
        },
        {
            "name": "columns",
            "value": [
                {
                    "data": 0,
                    "name": "ACTIVITY_NAME",
                    "searchable": true,
                    "orderable": true,
                    "search": {
                        "value": "",
                        "regex": false
                    }
                },
                {
                    "data": 1,
                    "name": "STATE",
                    "searchable": true,
                    "orderable": true,
                    "search": {
                        "value": "",
                        "regex": false
                    }
                }
            ]
        },
        {
            "name": "order",
            "value": [
                {
                    "column": 0,
                    "dir": "desc"
                }
            ]
        },
        {
            "name": "start",
            "value": 0
        },
        {
            "name": "length",
            "value": ' || lc_nCountOfRecords || '
        },
        {
            "name": "search",
            "value": {
                "value": "",
                "regex": false
            }
        }
    ],
    "data": [
        {
            "YEAR": ' || lc_nYear || ',
            "PRODUCTLINE": ' || lc_nProductlineListId || ',
            "STATE": ""
        }
    ]
}');
    lc_ojJson := BB_F_R_REPORT_LIBRARY_ACTIONS(lc_jJson,lc_vSql);
--htp.p(lc_vSql);
    assertTrue(lc_ojJson.exist('names'));
    assertTrue(lc_ojJson.exist('data'));
    assertTrue(lc_ojJson.get('names').is_array);
    assertTrue(lc_ojJson.get('data').is_array);
    lc_jlData := json_list(lc_ojJson.get('data'));
--p(lc_jlData.count);
    assertEquals(lc_jlData.count,lc_nCountOfRecords);
    pass(str);


  exception
    when others then fail(str);
  end;


  str := 'BB_F_R_REPORT_LIBRARY_ACTIONS productline = 100085 and year = 2012 and state = VIRGINIA';
  declare
    lc_jJson json;
    lc_ojJson json;
    lc_jlData json_list;
    lc_nCountOfRecords number :=0;
    lc_vSql varchar2(2000);
    lc_nProductlineListId number := 100085;
    lc_nYear number := 2012;
    lc_vState varchar2(2000) := 'VIRGINIA';
  begin

    select 
      count(*) into lc_nCountOfRecords 
    from 
      spd_t_action_planner ap,
      spd_t_activities act 
    where 
      ap.planned_activitiesid = act.activitiesid and
      ap.productline_listid = lc_nProductlineListId and 
      ap.planned_year = lc_nYear and 
      decode(act.STATE, 'NON-US',(select distinct country from spd_mv_inf_cur_activities ca where ca.uic = act.spd_uic), act.STATE) = lc_vState;
      
    lc_jJson := json('{
    "module": "reportLibrary",
    "target": "reportLibraryActions",
    "aoData": [
        {
            "name": "draw",
            "value": 5
        },
        {
            "name": "columns",
            "value": [
                {
                    "data": 0,
                    "name": "ACTIVITY_NAME",
                    "searchable": true,
                    "orderable": true,
                    "search": {
                        "value": "",
                        "regex": false
                    }
                },
                {
                    "data": 1,
                    "name": "STATE",
                    "searchable": true,
                    "orderable": true,
                    "search": {
                        "value": "",
                        "regex": false
                    }
                }
            ]
        },
        {
            "name": "order",
            "value": [
                {
                    "column": 0,
                    "dir": "desc"
                }
            ]
        },
        {
            "name": "start",
            "value": 0
        },
        {
            "name": "length",
            "value": ' || lc_nCountOfRecords || '
        },
        {
            "name": "search",
            "value": {
                "value": "",
                "regex": false
            }
        }
    ],
    "data": [
        {
            "YEAR": ' || lc_nYear || ',
            "PRODUCTLINE": ' || lc_nProductlineListId || ',
            "STATE": "' || lc_vState || '"
        }
    ]
}');
    lc_ojJson := BB_F_R_REPORT_LIBRARY_ACTIONS(lc_jJson,lc_vSql);
--htp.p(lc_vSql);
    assertTrue(lc_ojJson.exist('names'));
    assertTrue(lc_ojJson.exist('data'));
    assertTrue(lc_ojJson.get('names').is_array);
    assertTrue(lc_ojJson.get('data').is_array);
    lc_jlData := json_list(lc_ojJson.get('data'));
--p(lc_jlData.count);
    assertEquals(lc_jlData.count,lc_nCountOfRecords);
    pass(str);


  exception
    when others then fail(str);
  end;
  
  str := 'BB_F_R_REPORT_LIBRARY_ACTIONS productline = 100085 and year = 2012 and state = VIRGINIA and search = YORK';
  declare
    lc_jJson json;
    lc_ojJson json;
    lc_jlData json_list;
    lc_nCountOfRecords number :=0;
    lc_vSql varchar2(2000);
    lc_nProductlineListId number := 100085;
    lc_nYear number := 2012;
    lc_vState varchar2(2000) := 'VIRGINIA';
    lc_vSearch varchar2(2000) := 'YORK';
  begin

    select 
      count(*) into lc_nCountOfRecords 
    from 
      spd_t_action_planner ap,
      spd_t_activities act 
    where 
      ap.planned_activitiesid = act.activitiesid and
      ap.productline_listid = lc_nProductlineListId and 
      ap.planned_year = lc_nYear and 
      decode(act.STATE, 'NON-US',(select distinct country from spd_mv_inf_cur_activities ca where ca.uic = act.spd_uic), act.STATE) = lc_vState and
      upper(act.spd_activity_name || act.spd_uic) like '%' || upper(lc_vSearch) || '%';
      
    lc_jJson := json('{
    "module": "reportLibrary",
    "target": "reportLibraryActions",
    "aoData": [
        {
            "name": "draw",
            "value": 5
        },
        {
            "name": "columns",
            "value": [
                {
                    "data": 0,
                    "name": "ACTIVITY_NAME",
                    "searchable": true,
                    "orderable": true,
                    "search": {
                        "value": "",
                        "regex": false
                    }
                },
                {
                    "data": 1,
                    "name": "STATE",
                    "searchable": true,
                    "orderable": true,
                    "search": {
                        "value": "",
                        "regex": false
                    }
                }
            ]
        },
        {
            "name": "order",
            "value": [
                {
                    "column": 0,
                    "dir": "desc"
                }
            ]
        },
        {
            "name": "start",
            "value": 0
        },
        {
            "name": "length",
            "value": ' || lc_nCountOfRecords || '
        },
        {
            "name": "search",
            "value": {
                "value": "' || lc_vSearch || '",
                "regex": false
            }
        }
    ],
    "data": [
        {
            "YEAR": ' || lc_nYear || ',
            "PRODUCTLINE": ' || lc_nProductlineListId || ',
            "STATE": "' || lc_vState || '"
        }
    ]
}');
    lc_ojJson := BB_F_R_REPORT_LIBRARY_ACTIONS(lc_jJson,lc_vSql);
--htp.p(lc_vSql);
    assertTrue(lc_ojJson.exist('names'));
    assertTrue(lc_ojJson.exist('data'));
    assertTrue(lc_ojJson.get('names').is_array);
    assertTrue(lc_ojJson.get('data').is_array);
    lc_jlData := json_list(lc_ojJson.get('data'));
--p(lc_jlData.count);
    assertEquals(lc_jlData.count,lc_nCountOfRecords);
    pass(str);


  exception
    when others then fail(str);
  end;
  
  str := 'BB_F_R_REPORT_LIBRARY_ACTIONS productline = 100085 and year = 2012 and state = VIRGINIA and search = yoRK';
  declare
    lc_jJson json;
    lc_ojJson json;
    lc_jlData json_list;
    lc_nCountOfRecords number :=0;
    lc_vSql varchar2(2000);
    lc_nProductlineListId number := 100085;
    lc_nYear number := 2012;
    lc_vState varchar2(2000) := 'VIRGINIA';
    lc_vSearch varchar2(2000) := 'yoRK';
  begin

    select 
      count(*) into lc_nCountOfRecords 
    from 
      spd_t_action_planner ap,
      spd_t_activities act 
    where 
      ap.planned_activitiesid = act.activitiesid and
      ap.productline_listid = lc_nProductlineListId and 
      ap.planned_year = lc_nYear and 
      decode(act.STATE, 'NON-US',(select distinct country from spd_mv_inf_cur_activities ca where ca.uic = act.spd_uic), act.STATE) = lc_vState and
      upper(act.spd_activity_name || act.spd_uic) like '%' || upper(lc_vSearch) || '%';
      
    lc_jJson := json('{
    "module": "reportLibrary",
    "target": "reportLibraryActions",
    "aoData": [
        {
            "name": "draw",
            "value": 5
        },
        {
            "name": "columns",
            "value": [
                {
                    "data": 0,
                    "name": "ACTIVITY_NAME",
                    "searchable": true,
                    "orderable": true,
                    "search": {
                        "value": "",
                        "regex": false
                    }
                },
                {
                    "data": 1,
                    "name": "STATE",
                    "searchable": true,
                    "orderable": true,
                    "search": {
                        "value": "",
                        "regex": false
                    }
                }
            ]
        },
        {
            "name": "order",
            "value": [
                {
                    "column": 0,
                    "dir": "desc"
                }
            ]
        },
        {
            "name": "start",
            "value": 0
        },
        {
            "name": "length",
            "value": ' || lc_nCountOfRecords || '
        },
        {
            "name": "search",
            "value": {
                "value": "' || lc_vSearch || '",
                "regex": false
            }
        }
    ],
    "data": [
        {
            "YEAR": ' || lc_nYear || ',
            "PRODUCTLINE": ' || lc_nProductlineListId || ',
            "STATE": "' || lc_vState || '"
        }
    ]
}');
    lc_ojJson := BB_F_R_REPORT_LIBRARY_ACTIONS(lc_jJson,lc_vSql);
--htp.p(lc_vSql);
    assertTrue(lc_ojJson.exist('names'));
    assertTrue(lc_ojJson.exist('data'));
    assertTrue(lc_ojJson.get('names').is_array);
    assertTrue(lc_ojJson.get('data').is_array);
    lc_jlData := json_list(lc_ojJson.get('data'));
--p(lc_jlData.count);
    assertEquals(lc_jlData.count,lc_nCountOfRecords);
    pass(str);


  exception
    when others then fail(str);
  end;
  
  str := 'BB_F_R_REPORT_LIBRARY_ACTIONS productline = 100085 and state = VIRGINIA and search = yoRK';
  declare
    lc_jJson json;
    lc_ojJson json;
    lc_jlData json_list;
    lc_nCountOfRecords number :=0;
    lc_vSql varchar2(2000);
    lc_nProductlineListId number := 100085;
    lc_nYear number := 2012;
    lc_vState varchar2(2000) := 'VIRGINIA';
    lc_vSearch varchar2(2000) := 'yoRK';
  begin

    select 
      count(*) into lc_nCountOfRecords 
    from 
      spd_t_action_planner ap,
      spd_t_activities act 
    where 
      ap.planned_activitiesid = act.activitiesid and
      ap.productline_listid = lc_nProductlineListId and
      decode(act.STATE, 'NON-US',(select distinct country from spd_mv_inf_cur_activities ca where ca.uic = act.spd_uic), act.STATE) = lc_vState and
      upper(act.spd_activity_name || act.spd_uic) like '%' || upper(lc_vSearch) || '%';
      
    lc_jJson := json('{
    "module": "reportLibrary",
    "target": "reportLibraryActions",
    "aoData": [
        {
            "name": "draw",
            "value": 5
        },
        {
            "name": "columns",
            "value": [
                {
                    "data": 0,
                    "name": "ACTIVITY_NAME",
                    "searchable": true,
                    "orderable": true,
                    "search": {
                        "value": "",
                        "regex": false
                    }
                },
                {
                    "data": 1,
                    "name": "STATE",
                    "searchable": true,
                    "orderable": true,
                    "search": {
                        "value": "",
                        "regex": false
                    }
                }
            ]
        },
        {
            "name": "order",
            "value": [
                {
                    "column": 0,
                    "dir": "desc"
                }
            ]
        },
        {
            "name": "start",
            "value": 0
        },
        {
            "name": "length",
            "value": ' || lc_nCountOfRecords || '
        },
        {
            "name": "search",
            "value": {
                "value": "' || lc_vSearch || '",
                "regex": false
            }
        }
    ],
    "data": [
        {
            "YEAR": null,
            "PRODUCTLINE": ' || lc_nProductlineListId || ',
            "STATE": "' || lc_vState || '"
        }
    ]
}');
    lc_ojJson := BB_F_R_REPORT_LIBRARY_ACTIONS(lc_jJson,lc_vSql);
--htp.p(lc_vSql);
    assertTrue(lc_ojJson.exist('names'));
    assertTrue(lc_ojJson.exist('data'));
    assertTrue(lc_ojJson.get('names').is_array);
    assertTrue(lc_ojJson.get('data').is_array);
    lc_jlData := json_list(lc_ojJson.get('data'));
--p(lc_jlData.count);
    assertEquals(lc_jlData.count,lc_nCountOfRecords);
    pass(str);


  exception
    when others then fail(str);
  end;
  
  
  str := 'BB_F_R_REPORT_LIBRARY_ACTIONS productline = 100085 and search = yoRK';
  declare
    lc_jJson json;
    lc_ojJson json;
    lc_jlData json_list;
    lc_nCountOfRecords number :=0;
    lc_vSql varchar2(2000);
    lc_nProductlineListId number := 100085;
    lc_nYear number := 2012;
    lc_vState varchar2(2000) := 'VIRGINIA';
    lc_vSearch varchar2(2000) := 'yoRK';
  begin

    select 
      count(*) into lc_nCountOfRecords 
    from 
      spd_t_action_planner ap,
      spd_t_activities act 
    where 
      ap.planned_activitiesid = act.activitiesid and
      ap.productline_listid = lc_nProductlineListId and
      upper(act.spd_activity_name || act.spd_uic) like '%' || upper(lc_vSearch) || '%';
      
    lc_jJson := json('{
    "module": "reportLibrary",
    "target": "reportLibraryActions",
    "aoData": [
        {
            "name": "draw",
            "value": 5
        },
        {
            "name": "columns",
            "value": [
                {
                    "data": 0,
                    "name": "ACTIVITY_NAME",
                    "searchable": true,
                    "orderable": true,
                    "search": {
                        "value": "",
                        "regex": false
                    }
                },
                {
                    "data": 1,
                    "name": "STATE",
                    "searchable": true,
                    "orderable": true,
                    "search": {
                        "value": "",
                        "regex": false
                    }
                }
            ]
        },
        {
            "name": "order",
            "value": [
                {
                    "column": 0,
                    "dir": "desc"
                }
            ]
        },
        {
            "name": "start",
            "value": 0
        },
        {
            "name": "length",
            "value": ' || lc_nCountOfRecords || '
        },
        {
            "name": "search",
            "value": {
                "value": "' || lc_vSearch || '",
                "regex": false
            }
        }
    ],
    "data": [
        {
            "YEAR": null,
            "PRODUCTLINE": ' || lc_nProductlineListId || ',
            "STATE": ""
        }
    ]
}');
    lc_ojJson := BB_F_R_REPORT_LIBRARY_ACTIONS(lc_jJson,lc_vSql);
--htp.p(lc_vSql);
    assertTrue(lc_ojJson.exist('names'));
    assertTrue(lc_ojJson.exist('data'));
    assertTrue(lc_ojJson.get('names').is_array);
    assertTrue(lc_ojJson.get('data').is_array);
    lc_jlData := json_list(lc_ojJson.get('data'));
--p(lc_jlData.count);
    assertEquals(lc_jlData.count,lc_nCountOfRecords);
    pass(str);


  exception
    when others then fail(str);
  end;
  
  
  str := 'BB_F_R_REPORT_LIBRARY_ACTIONS search = yoRK';
  declare
    lc_jJson json;
    lc_ojJson json;
    lc_jlData json_list;
    lc_nCountOfRecords number :=0;
    lc_vSql varchar2(2000);
    lc_nProductlineListId number := 100085;
    lc_nYear number := 2012;
    lc_vState varchar2(2000) := 'VIRGINIA';
    lc_vSearch varchar2(2000) := 'yoRK';
  begin

    select 
      count(*) into lc_nCountOfRecords 
    from 
      spd_t_action_planner ap,
      spd_t_activities act 
    where 
      ap.planned_activitiesid = act.activitiesid and
      upper(act.spd_activity_name || act.spd_uic) like '%' || upper(lc_vSearch) || '%';
      
    lc_jJson := json('{
    "module": "reportLibrary",
    "target": "reportLibraryActions",
    "aoData": [
        {
            "name": "draw",
            "value": 5
        },
        {
            "name": "columns",
            "value": [
                {
                    "data": 0,
                    "name": "ACTIVITY_NAME",
                    "searchable": true,
                    "orderable": true,
                    "search": {
                        "value": "",
                        "regex": false
                    }
                },
                {
                    "data": 1,
                    "name": "STATE",
                    "searchable": true,
                    "orderable": true,
                    "search": {
                        "value": "",
                        "regex": false
                    }
                }
            ]
        },
        {
            "name": "order",
            "value": [
                {
                    "column": 0,
                    "dir": "desc"
                }
            ]
        },
        {
            "name": "start",
            "value": 0
        },
        {
            "name": "length",
            "value": ' || lc_nCountOfRecords || '
        },
        {
            "name": "search",
            "value": {
                "value": "' || lc_vSearch || '",
                "regex": false
            }
        }
    ],
    "data": [
        {
            "YEAR": null,
            "PRODUCTLINE": null,
            "STATE": ""
        }
    ]
}');
    lc_ojJson := BB_F_R_REPORT_LIBRARY_ACTIONS(lc_jJson,lc_vSql);
--htp.p(lc_vSql);
    assertTrue(lc_ojJson.exist('names'));
    assertTrue(lc_ojJson.exist('data'));
    assertTrue(lc_ojJson.get('names').is_array);
    assertTrue(lc_ojJson.get('data').is_array);
    lc_jlData := json_list(lc_ojJson.get('data'));
--p(lc_jlData.count);
    assertEquals(lc_jlData.count,lc_nCountOfRecords);
    pass(str);


  exception
    when others then fail(str);
  end;
  
  
  str := 'BB_F_R_REPORT_LIBRARY_ACTIONS year = 2012';
  declare
    lc_jJson json;
    lc_ojJson json;
    lc_jlData json_list;
    lc_nCountOfRecords number :=0;
    lc_vSql varchar2(2000);
    lc_nProductlineListId number := 100085;
    lc_nYear number := 2012;
    lc_vState varchar2(2000) := 'VIRGINIA';
    lc_vSearch varchar2(2000) := 'yoRK';
  begin

    select 
      count(*) into lc_nCountOfRecords 
    from 
      spd_t_action_planner ap,
      spd_t_activities act 
    where 
      ap.planned_activitiesid = act.activitiesid and
      ap.planned_year = lc_nYear;
      
    lc_jJson := json('{
    "module": "reportLibrary",
    "target": "reportLibraryActions",
    "aoData": [
        {
            "name": "draw",
            "value": 5
        },
        {
            "name": "columns",
            "value": [
                {
                    "data": 0,
                    "name": "ACTIVITY_NAME",
                    "searchable": true,
                    "orderable": true,
                    "search": {
                        "value": "",
                        "regex": false
                    }
                },
                {
                    "data": 1,
                    "name": "STATE",
                    "searchable": true,
                    "orderable": true,
                    "search": {
                        "value": "",
                        "regex": false
                    }
                }
            ]
        },
        {
            "name": "order",
            "value": [
                {
                    "column": 0,
                    "dir": "desc"
                }
            ]
        },
        {
            "name": "start",
            "value": 0
        },
        {
            "name": "length",
            "value": ' || lc_nCountOfRecords || '
        },
        {
            "name": "search",
            "value": {
                "value": "",
                "regex": false
            }
        }
    ],
    "data": [
        {
            "YEAR": ' || lc_nYear || ',
            "PRODUCTLINE": null,
            "STATE": ""
        }
    ]
}');
    lc_ojJson := BB_F_R_REPORT_LIBRARY_ACTIONS(lc_jJson,lc_vSql);
--htp.p(lc_vSql);
    assertTrue(lc_ojJson.exist('names'));
    assertTrue(lc_ojJson.exist('data'));
    assertTrue(lc_ojJson.get('names').is_array);
    assertTrue(lc_ojJson.get('data').is_array);
    lc_jlData := json_list(lc_ojJson.get('data'));
--p(lc_jlData.count);
    assertEquals(lc_jlData.count,lc_nCountOfRecords);
    pass(str);


  exception
    when others then fail(str);
  end;
  
 str := 'BB_F_R_REPORT_LIBRARY_ACTIONS productline = 100088';
  declare
    lc_jJson json;
    lc_ojJson json;
    lc_jlData json_list;
    lc_nCountOfRecords number :=0;
    lc_vSql varchar2(2000);
    lc_nProductlineListId number := 100088;
  begin

    select count(*) into lc_nCountOfRecords from spd_t_action_planner where productline_listid = lc_nProductlineListId;
    lc_jJson := json('{
    "module": "reportLibrary",
    "target": "reportLibraryActions",
    "aoData": [
        {
            "name": "draw",
            "value": 5
        },
        {
            "name": "columns",
            "value": [
                {
                    "data": 0,
                    "name": "ACTIVITY_NAME",
                    "searchable": true,
                    "orderable": true,
                    "search": {
                        "value": "",
                        "regex": false
                    }
                },
                {
                    "data": 1,
                    "name": "STATE",
                    "searchable": true,
                    "orderable": true,
                    "search": {
                        "value": "",
                        "regex": false
                    }
                }
            ]
        },
        {
            "name": "order",
            "value": [
                {
                    "column": 0,
                    "dir": "desc"
                }
            ]
        },
        {
            "name": "start",
            "value": 0
        },
        {
            "name": "length",
            "value": ' || lc_nCountOfRecords || '
        },
        {
            "name": "search",
            "value": {
                "value": "",
                "regex": false
            }
        }
    ],
    "data": [
        {
            "YEAR": "",
            "PRODUCTLINE": ' || lc_nProductlineListId || ',
            "STATE": ""
        }
    ]
}');
    lc_ojJson := BB_F_R_REPORT_LIBRARY_ACTIONS(lc_jJson,lc_vSql);
--htp.p(lc_vSql);
    assertTrue(lc_ojJson.exist('names'));
    assertTrue(lc_ojJson.exist('data'));
    assertTrue(lc_ojJson.get('names').is_array);
    assertTrue(lc_ojJson.get('data').is_array);
    lc_jlData := json_list(lc_ojJson.get('data'));
--p(lc_jlData.count);
    assertEqualsQuery(lc_jlData.count,'select count(*) from spd_t_action_planner where productline_listid = ' || lc_nProductlineListId);
    pass(str);


  exception
    when others then fail(str);
  end;
  


--lc_ojJson := BB_F_R_REPORT_LIBRARY_REC(lc_jJson);
  str := 'BB_F_R_REPORT_LIBRARY_REC test';
  declare
    lc_jJson json;
    lc_ojJson json;
    lc_jlData json_list;
  begin

    execute immediate 'insert into spd_t_reports (reportid) values (1)'; 

    lc_jJson := json('{"module":"reportLibrary","target":"reportLibraryRecord","data":[{"REPORTID":1}]}');
    lc_ojJson := BB_F_R_REPORT_LIBRARY_REC(lc_jJson);

    assertTrue(lc_ojJson.exist('names'));
    assertTrue(lc_ojJson.exist('data'));
    assertTrue(lc_ojJson.get('names').is_array);
    assertTrue(lc_ojJson.get('data').is_array);
    lc_jlData := json_list(lc_ojJson.get('data'));
--p(lc_jlData.count);
    assertEquals(lc_jlData.count,1);
    pass(str);
    
    execute immediate 'delete from spd_t_reports where reportid = 1';
    
  exception
    when others then fail(str);
  end;
  


--lc_ojJson := BB_F_R_REPORT_LIBRARY_KEYWORDS(lc_jJson);
  str := 'BB_F_R_REPORT_LIBRARY_KEYWORDS test';
  declare
    lc_jJson json;
    lc_ojJson json;
    lc_jlData json_list;
  begin

    execute immediate 'insert into spd_t_reports (reportid) values (1)'; 
    execute immediate 'insert into spd_t_report_files (reportfileid, reportid) values (1,1)';
    execute immediate 'insert into spd_t_report_keywords (reportfileid, reportkeywordid) values (1,1)';

    lc_jJson := json('{"module":"reportLibrary","target":"reportLibraryKeywords","data": [{"REPORTFILEID" : 1}]}');
    lc_ojJson := BB_F_R_REPORT_LIBRARY_KEYWORDS(lc_jJson);

    assertTrue(lc_ojJson.exist('names'));
    assertTrue(lc_ojJson.exist('data'));
    assertTrue(lc_ojJson.get('names').is_array);
    assertTrue(lc_ojJson.get('data').is_array);
    lc_jlData := json_list(lc_ojJson.get('data'));
--p(lc_jlData.count);
    assertEquals(lc_jlData.count,1);
    pass(str);
    
    execute immediate 'delete from spd_t_reports where reportid = 1';
    execute immediate 'delete from spd_t_report_files where reportfileid=1';
    execute immediate 'delete from spd_t_report_keywords where reportkeywordid = 1';
    
  exception
    when others then fail(str);
  end;
  
--lc_vSql := BB_F_CU_REPORT_LIBRARY_REC(lc_jJson,lc_ojJson); --create
  str := 'BB_F_CU_REPORT_LIBRARY_REC create report record';
  declare
    lc_jJson json;
    lc_ojJson json;
    lc_jvData json_value;
    lc_nId number;
    lc_jMessages json;
    lc_vResult varchar2(2000);
  begin

    

    lc_jJson := json('{"module":"reportLibrary","target":"createReportLibraryRecord","data": [
        {
            "REPORTID" : 0,
            "REPORTTITLE": "report title lkjl",
            "REPORTAUTHOR": "report author oilkj",
            "ESTSTARTDATE": 2014,
            "CONTRACTOR": "contractor lkjlk",
            "CONTRACTNUMBER": "contract number lkjlj",
            "REPORTNUMBER": "report number lkjlkj",
            "ACTIONSID": 0,
            "CITY": "city kjj",
            "STATE": "HAWAII",
            "COUNTRY": "country lkjlkj",
            "PRODUCTLINE" : 100085,
            "GOVPROJENGINEER" : "someone gpe"
        }
    ]}');
    lc_vResult := BB_F_CU_REPORT_LIBRARY_REC(lc_jJson, lc_jMessages);
    lc_jJson := lc_jMessages;
--lc_jJson.htp;

    assertTrue(lc_jJson.exist('messages'));
    assertTrue(lc_jJson.exist('status'));
    assertTrue(lc_jJson.exist('id'));
    
    lc_jvData := lc_jJson.get('id');
    lc_nId := lc_jvData.get_number;
    
    assertGreaterThan(lc_nId,0);
    pass(str);

  execute immediate 'delete from spd_t_reports where reportid = ' || lc_jvData.get_number;
    
  exception
    when others then fail(str);
  end;
  
--lc_vSql := BB_F_CU_REPORT_LIBRARY_REC(lc_jJson,lc_ojJson); --create
  str := 'BB_F_CU_REPORT_LIBRARY_REC update report record';
  declare
    lc_jJson json;
    lc_ojJson json;
    lc_jvData json_value;
    lc_nId number;
    lc_jMessages json;
    lc_vResult varchar2(2000);
  begin

    execute immediate 'insert into spd_t_reports (reportid) values (1)';

    lc_jJson := json('{"module":"reportLibrary","target":"updateReportLibraryRecord","data": [
        {
            "REPORTID" : 1,
            "REPORTTITLE": "report title lkjl",
            "REPORTAUTHOR": "report author oilkj",
            "ESTSTARTDATE": 2014,
            "CONTRACTOR": "contractor lkjlk",
            "CONTRACTNUMBER": "contract number lkjlj",
            "REPORTNUMBER": "report number lkjlkj",
            "ACTIONSID": 0,
            "CITY": "city kjj",
            "STATE": "HAWAII",
            "COUNTRY": "country lkjlkj",
            "PRODUCTLINE" : 100085,
            "GOVPROJENGINEER" : "someone gpe"
        }
    ]}');
    lc_vResult := BB_F_CU_REPORT_LIBRARY_REC(lc_jJson, lc_jMessages);
    lc_ojJson := lc_jMessages;
--lc_ojJson.htp;

    assertTrue(lc_ojJson.exist('messages'));
    assertTrue(lc_ojJson.exist('status'));
    lc_jvData := lc_ojJson.get('status');
    lc_vResult := lc_jvData.get_string;
    assertEquals(lc_vResult,'success');
    pass(str);

  execute immediate 'delete from spd_t_reports where reportid = 1';
    
  exception
    when others then fail(str);
  end;
  
--lc_vSql := BB_F_C_REPORT_LIBRARY_FILE(lc_jJson,lc_ojJson);
  str := 'BB_F_C_REPORT_LIBRARY_FILE create ';
  declare
    lc_jJson json;
    lc_ojJson json;
    lc_jvData json_value;
    lc_nId number :=0;
    lc_jMessages json;
    lc_vResult varchar2(32000);
  begin

    execute immediate 'insert into spd_t_reports (reportid) values (1)';
    execute immediate 'delete from spd_t_report_files where getfilenamefrombfile(the_bfile) = ''Preliminary_WBS_SSD_Critigen.pdf''';
    --select count(reportfileid) into lc_nId from spd_t_report_files where getfilenamefrombfile(the_bfile) = 'Preliminary_WBS_SSD_Critigen.pdf';
    --htp.p('count: ' || lc_nId);
    
    lc_jJson := json('{"module":"reportLibrary","target":"updateReportLibraryRecord","data": [{"REPORTID" : 1,"FILENAME": "Preliminary_WBS_SSD_Critigen.pdf","REPORT_NAME": "report name oilkj"}]}');
    
    lc_vResult := BB_F_C_REPORT_LIBRARY_FILE(lc_jJson, lc_jMessages);
    lc_ojJson := lc_jMessages;
--lc_ojJson.htp;

    assertTrue(lc_ojJson.exist('messages'));
    assertTrue(lc_ojJson.exist('status'));
    assertTrue(lc_ojJson.exist('id'));
    
    lc_jvData := lc_ojJson.get('id');
    lc_nId := lc_jvData.get_number;
    
    assertGreaterThan(lc_nId,0);
    pass(str);

  execute immediate 'delete from spd_t_reports where reportid = 1';
  execute immediate 'delete from spd_t_report_files where reportfileid = ' || lc_nId;

    
    
  exception
    when others then 
    htp.p(sqlerrm);
    fail(str);
  end;


--lc_vSql := BB_F_D_REPORT_LIBRARY_FILE(lc_jJson,lc_ojJson);
  str := 'BB_F_D_REPORT_LIBRARY_FILE delete ';
  declare
    lc_jJson json;
    lc_ojJson json;
    lc_jvData json_value;
    lc_nId number;
    lc_jMessages json;
    lc_vResult varchar2(2000);
  begin

    execute immediate 'insert into spd_t_reports (reportid) values (1)';
    execute immediate 'insert into spd_t_report_files (reportid, reportfileid) values (1,1)';
    
    lc_jJson := json('{"module":"reportLibrary","target":"deleteReportLibraryFile","data":[{"REPORTFILEID":1}]}');
    lc_vResult := BB_F_D_REPORT_LIBRARY_FILE(lc_jJson, lc_jMessages);
    lc_jJson := lc_jMessages;
--lc_jJson.htp;

    assertTrue(lc_jJson.exist('status'));
    
    lc_jvData := lc_jJson.get('status');
    lc_vResult := lc_jvData.get_string;
    
    assertEquals(lc_vResult,'success');
    pass(str);

    execute immediate 'delete from spd_t_reports where reportid = 1';
    
  exception
    when others then fail(str);
  end;
  
  

  
  
--lc_vSql := BB_F_D_REPORT_LIBRARY_KEYWORD(lc_jJson,lc_ojJson);
  str := 'BB_F_D_REPORT_LIBRARY_KEYWORD delete ';
  declare
    lc_jJson json;
    lc_ojJson json;
    lc_jvData json_value;
    lc_nId number;
    lc_jMessages json;
    lc_vResult varchar2(2000);
  begin

    execute immediate 'insert into spd_t_reports (reportid) values (1)';
    execute immediate 'insert into spd_t_report_files (reportid, reportfileid) values (1,1)';
    execute immediate 'insert into spd_t_report_keywords (reportfileid, reportkeywordid) values (1,1)';
    
    lc_jJson := json('{"module":"reportLibrary","target":"removeReportFileKeyword","data": [{"REPORTKEYWORDID" : 1}]}');
    
    lc_vResult := BB_F_D_REPORT_LIBRARY_KEYWORD(lc_jJson, lc_jMessages);
    lc_jJson := lc_jMessages;
--lc_jJson.htp;

    assertTrue(lc_jJson.exist('status'));
    lc_jvData := lc_jJson.get('status');
    lc_vResult := lc_jvData.get_string;
    assertEquals(lc_vResult,'success');
    pass(str);

  execute immediate 'delete from spd_t_reports where reportid = 1';
  execute immediate 'delete from spd_t_report_files where reportfileid = 1';
  execute immediate 'delete from spd_t_report_keywords where reportkeywordid = 1';
    
  exception
    when others then 
    htp.p(sqlerrm);
    fail(str);
  end;
  
  saveResults;
  postResults;
end;
 

/
