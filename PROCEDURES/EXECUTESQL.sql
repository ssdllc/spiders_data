--------------------------------------------------------
--  DDL for Procedure EXECUTESQL
--------------------------------------------------------
set define off;

  CREATE OR REPLACE EDITIONABLE PROCEDURE "SPIDERS_DATA"."EXECUTESQL" (in_jlDML json_list, in_nAutoCommit number default 1) as
      lc_jResults json;
      lc_jMessages json;
      lc_vDMLResult varchar2(1);
  begin
      
      lc_jResults := json();
      lc_jResults.put('results',in_jlDML);
      
      lc_jMessages := json();
      lc_vDMLResult := BB_F_DML(lc_jResults, lc_jMessages, in_nAutoCommit);
  end;
 

/
