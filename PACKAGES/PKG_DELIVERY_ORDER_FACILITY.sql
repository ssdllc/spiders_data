--------------------------------------------------------
--  DDL for Package PKG_DELIVERY_ORDER_FACILITY
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE PACKAGE "SPIDERS_DATA"."PKG_DELIVERY_ORDER_FACILITY" as 

$IF $$dev_env_test $THEN

function f_test_for_error (in_vReturned varchar2) return varchar2;
function getAttributeValue(in_vAttribute varchar2, in_jJSON json) return varchar2;
function getSpidersFacilityId(in_vFACILITY_ID varchar2) return number;
function getJsonList(in_vAttribute varchar2, in_jJSON json) return json_list;
function getJsonListItem(in_nRow number, in_jlJSON json_list) return json;
function makeInsertFacilitySql(in_nFacid number, in_jJSON json) return varchar2;
function getNextFacilityId return number;


$END

function f_add_delivery_order_fac(in_cJson clob) return json;

end;
 

/
