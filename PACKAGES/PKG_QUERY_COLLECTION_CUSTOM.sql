--------------------------------------------------------
--  DDL for Package PKG_QUERY_COLLECTION_CUSTOM
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE PACKAGE "SPIDERS_DATA"."PKG_QUERY_COLLECTION_CUSTOM" as 

$IF $$dev_env_test $THEN

function f_test_for_error (in_vReturned varchar2) return varchar2;
function getAttributeValue(in_vAttribute varchar2, in_jJSON json) return varchar2;
function f_parseWhere(in_jJson json, in_nFilterLength number) return varchar2;
function f_getSpecificWhere(in_vATTRIBUTE_TYPE varchar2, in_vVALUE_LIST varchar2, in_vATTRIBUTE_NAME varchar2, in_nATTRIBUTEID number, in_nRANGE_LOW number, in_nRANGE_HIGH number) return varchar2;
function f_getValueListOrRange return varchar2;
function f_perform_read_qry_collection(in_jJson json, in_vModule varchar2, in_vTarget varchar2, in_vTableName varchar2) return json;

$END

function f_read(in_cJson clob) return json;
function f_getFilterWhereClause return varchar2;

end;
 

/
