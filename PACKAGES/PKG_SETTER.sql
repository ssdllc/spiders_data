--------------------------------------------------------
--  DDL for Package PKG_SETTER
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE PACKAGE "SPIDERS_DATA"."PKG_SETTER" as

filtersetid number;

procedure set_filtersetid(in_vValue varchar2);
function get_filtersetid return number;

end;
 

/
