--------------------------------------------------------
--  DDL for Package PKG_DATA_TABLE_CUSTOM
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE PACKAGE "SPIDERS_DATA"."PKG_DATA_TABLE_CUSTOM" as

$IF $$dev_env_test $THEN

procedure p_log(in_vString varchar2);
procedure p_log_htp(in_vString varchar2);
function f_test_for_error (in_vReturned varchar2) return varchar2;
function f_parseGroupFilterData(in_jJson json, in_vTableName varchar2) return varchar2;
function f_parseAoDataSearch(in_jJson json) return varchar2;
function f_parseAoDataStart(in_jJson json) return varchar2;
function f_parseAoDataRecPerPage(in_jJson json) return varchar2;
function getAoDataOrderByColumnName (in_jJson json, in_nColumn number) return varchar2;
function f_parseAoDataOrderBy(in_jJson json) return varchar2;
function f_getDynamicWhere(in_vSearch varchar2, in_vTableName varchar2) return varchar2;
function f_buildFullWhere(in_vWhereClause varchar2, in_vDynamicWhere varchar2, in_vGroupWhere varchar2 ) return varchar2;
function f_setOrderByFunction(in_vOrderBy varchar2, in_vTarget varchar2) return varchar2;
function f_buildFullSql (in_vOrderBy varchar2, in_vStart varchar2, in_nRecordsPerPage number, in_vModule varchar2, in_vTarget varchar2) return json;
function f_getInnerForSelectRecordCount (in_vGroupBy varchar2) return varchar2;
function f_getRecordCount (in_vWhere varchar2, in_vOrderBy varchar2, in_vStart varchar2, in_nRecordsPerPage number, in_vModule varchar2, in_vTarget varchar2) return json;
function f_parseWhere(in_jJson json) return varchar2;
function getAttributeValue(in_vAttribute varchar2, in_jJSON json) return varchar2;


$END

function f_read(in_cJson clob) return json;
function f_download(in_cJson clob) return json;

end;
 

/
