--------------------------------------------------------
--  DDL for Package PKG_FM_JSON_TOOLS
--------------------------------------------------------

create or replace PACKAGE PKG_FM_JSON_TOOLS AS 
  PROCEDURE cleanupFMTables(in_FMACTIONID number, in_FMMOORINGID number);
  FUNCTION parseFleetMooringJSON(in_cJSON CLOB) RETURN JSON;
END PKG_FM_JSON_TOOLS;