--------------------------------------------------------
--  DDL for Package UT_COMMON
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE PACKAGE "SPIDERS_DATA"."UT_COMMON" AS 

  
  procedure p (v varchar2);

  procedure pass(unitTestID number, packageName varchar2, procedureName varchar2, testName varchar2);

  procedure fail(unitTestID number, packageName varchar2, procedureName varchar2, testName varchar2);

  procedure assertTrue(b boolean);

  procedure assertFalse(b boolean);

  procedure assertEquals(v varchar2, v2 varchar2);

  procedure assertEquals(v number, v2 number);

  procedure assertEquals(v json, v2 json);

  procedure assertEqualsQuery(v number, v2 varchar2);

  procedure assertGreaterThan(n number, n2 number);

  procedure saveResults;

  procedure postResults;
  
  procedure insertResults(in_vSql varchar2);
  
  procedure resetTest;

END UT_COMMON;
 

/
