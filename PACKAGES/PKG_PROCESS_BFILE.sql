--------------------------------------------------------
--  DDL for Package PKG_PROCESS_BFILE
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE PACKAGE "SPIDERS_DATA"."PKG_PROCESS_BFILE" as 

function f_process_drawings(in_cJson clob) return json;
function f_process_photos(in_cJson clob) return json;
function f_process_supfiles(in_cJson clob) return json;
function f_process_1391(in_cJson clob) return json;
function f_process_ce(in_cJson clob) return json;
function f_process_fs(in_cJson clob) return json;
/*
function f_process_supporting_files(in_cJson clob) return json;
function f_process_photos(in_cJson clob) return json;
function f_process_dd_1391(in_cJson clob) return json;
function f_process_cost_estimate(in_cJson clob) return json;
*/

end;
 

/
