create or replace PACKAGE PKG_ROWIFY as 

lg_vPackageName varchar2(255) := 'PKG_ROWIFY';

function f_XML_ACTION (in_nID number, in_nPK number, in_nADOID number, in_vCreatedBy varchar2) return XML_ACTIONS pipelined;
function f_XML_ACTION_DRAWING (in_nID number, in_nPK number, in_nADOID number, in_vCreatedBy varchar2) return XML_ACTION_DRAWINGS pipelined;
function f_XML_ACTION_PHOTO (in_nID number, in_nPK number, in_nADOID number, in_vCreatedBy varchar2) return XML_ACTION_PHOTOS pipelined;
function f_XML_ACTSUPPORT_FILE (in_nID number, in_nPK number, in_nADOID number, in_vCreatedBy varchar2)  return XML_ACTSUPPORT_FILES pipelined;
function f_XML_AI_FITTING (in_nID number, in_nPK number, in_nADOID number, in_vCreatedBy varchar2)  return XML_AI_FITTINGS pipelined;
function f_XML_FACILITY (in_nID number, in_nPK number, in_nADOID number, in_vCreatedBy varchar2)  return XML_FACILITIES pipelined;
function f_XML_EXIT_BRIEF (in_nID number, in_nPK number, in_nADOID number, in_vCreatedBy varchar2)  return XML_EXIT_BRIEFS pipelined;
function f_XML_EXEC_TABLE (in_nID number, in_nPK number, in_nADOID number, in_vCreatedBy varchar2)  return XML_EXEC_TABLES pipelined;
function f_XML_ASSET_SUMMARY (in_nID number, in_nPK number, in_nADOID number, in_vCreatedBy varchar2)  return XML_ASSET_SUMMARIES pipelined;
function f_XML_ASSET_INVENTORY (in_nID number, in_nPK number, in_nADOID number, in_vCreatedBy varchar2)  return XML_ASSET_INVENTORIES pipelined;
function f_XML_DEFECT (in_nID number, in_nPK number, in_nADOID number, in_vCreatedBy varchar2)  return XML_DEFECTS pipelined;
function f_XML_DEFECT_METADATA (in_nID number, in_nPK number, in_nADOID number, in_vCreatedBy varchar2)  return XML_DEFECT_METADATAS pipelined;
function f_XML_EXEC_SUMMARY (in_nID number, in_nPK number, in_nADOID number, in_vCreatedBy varchar2)  return XML_EXEC_SUMMARIES pipelined;
function f_XML_ES_PARAGRAPH (in_nID number, in_nPK number, in_nADOID number, in_vCreatedBy varchar2)  return XML_ES_PARAGRAPHS pipelined;
function f_XML_SENTENCE (in_nID number, in_nPK number, in_nADOID number, in_vCreatedBy varchar2)  return XML_SENTENCES pipelined;
function f_XML_AI_METADATA (in_nID number, in_nPK number, in_nADOID number, in_vCreatedBy varchar2)  return XML_AI_METADATAS pipelined;
function f_XML_FACILITY_DRAWING (in_nID number, in_nPK number, in_nADOID number, in_vCreatedBy varchar2)  return XML_FACILITY_DRAWINGS pipelined;
function f_XML_FACILITY_PHOTO (in_nID number, in_nPK number, in_nADOID number, in_vCreatedBy varchar2)  return XML_FACILITY_PHOTOS pipelined;
function f_XML_FACILITY_REPAIR (in_nID number, in_nPK number, in_nADOID number, in_vCreatedBy varchar2)  return XML_FACILITY_REPAIRS pipelined;
function f_XML_FAC_SUP_FILE (in_nID number, in_nPK number, in_nADOID number, in_vCreatedBy varchar2)  return XML_FAC_SUP_FILES pipelined;
function f_XML_FINDREC (in_nID number, in_nPK number, in_nADOID number, in_vCreatedBy varchar2)  return XML_FINDRECS pipelined;
function f_XML_REPAIR_PERCENTAGE (in_nID number, in_nPK number, in_nADOID number, in_vCreatedBy varchar2)  return XML_REPAIR_PERCENTAGES pipelined;
function f_XML_ET_METADATA (in_nID number, in_nPK number, in_nADOID number, in_vCreatedBy varchar2)  return XML_ET_METADATAS pipelined;
function f_XML_ATTENDEE (in_nID number, in_nPK number, in_nADOID number, in_vCreatedBy varchar2)  return XML_ATTENDEES pipelined;



end PKG_ROWIFY;