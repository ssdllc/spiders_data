--------------------------------------------------------
--  DDL for Package PKG_DATATABLE_SETTINGS
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE PACKAGE "SPIDERS_DATA"."PKG_DATATABLE_SETTINGS" as 

function f_test_for_error (in_vReturned varchar2) return varchar2;
function getCorrespondingJsonColumnVal(in_jJson json, in_vColumn varchar2) return varchar2;
function getUser return varchar2;
function buildDeleteStatement(in_vTableName varchar2, in_vUser varchar2) return varchar2;
function buildSelectStatement(in_vTableName varchar2, in_vUser varchar2) return varchar2;
function deleteSettings(in_cClob clob) return JSON;
function getSettings (in_cClob clob) return JSON;

end;
 

/
