--------------------------------------------------------
--  DDL for Package PKG_JOB_TITLES
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE PACKAGE "SPIDERS_DATA"."PKG_JOB_TITLES" as


$IF $$dev_env_test $THEN

function f_test_for_error (in_vReturned varchar2) return varchar2;
function getAttributeValue(in_vAttribute varchar2, in_jJSON json) return varchar2;
function getCorrespondingJsonColumnVal(in_jJson json, in_vColumn varchar2) return varchar2;


$END

function f_delete(in_cJson clob) return json;

end;
 

/
