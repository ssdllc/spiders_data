--------------------------------------------------------
--  DDL for Package PKG_COST_ESTIMATE_XML
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE PACKAGE "SPIDERS_DATA"."PKG_COST_ESTIMATE_XML" as 

$IF $$dev_env_test $THEN

function f_test_for_error (in_vReturned varchar2) return varchar2;
function getAttributeValue(in_vAttribute varchar2, in_jJSON json) return varchar2;
function getCorrespondingJsonColumnVal(in_jJson json, in_vColumn varchar2) return varchar2;
function getFacilityList(in_vDeliveryOrderId varchar2) return json;
function getContractorList(in_vDeliveryOrderId varchar2) return json;
function getFacilityDirectCosts(in_vDeliveryOrderId varchar2) return json;
function getGeneralDirectCosts(in_vDeliveryOrderId varchar2) return json;
function getGeneralLaborCatgories(in_vDeliveryOrderId varchar2) return json;
function getFacilityLaborCatgories(in_vDeliveryOrderId varchar2) return json;
function getInstructions(in_vDeliveryOrderId varchar2) return json;
function getDeliveryOrderDetails(in_vDeliveryOrderId varchar2) return json;



$END

function f_generate(in_cJson clob) return json;

end;
 

/
