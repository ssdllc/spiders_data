--------------------------------------------------------
--  DDL for Package XML_TOOLS
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE PACKAGE "SPIDERS_DATA"."XML_TOOLS" as 


/*
This software has been released under the MIT license:

  Copyright (c) 2011 Synergy Software Design, LLC

  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.
*/

function folderid(IN_vPATH varchar2, IN_nADOID number, IN_nFromCreateDO number default 0, IN_vFSES varchar2 default 'n') return number;
procedure import_xml(IN_nADOID number, IN_vFILENAME varchar2 default '',IN_vProcessImages varchar2 default 'y');
function  test_val (in_from varchar2,in_xml xmltype) return varchar2; 
function  test_num (in_xml xmltype) return number;
function  get_DOFID (in_nFACID number, in_nADOID number, in_vNFAID varchar2 default '',in_vActivityUIC varchar2, in_vFacilityName varchar2, in_vFacilityNo varchar2) return number;
procedure ce_xml_output(IN_ADOID number);
function import_xml_facility_files(IN_nADOID number, IN_nXMLFILEID number default 0) return number;
procedure  p (in_nAPID number, in_nADOID number, in_nDOFID number default 0, in_vFilename varchar2, in_vImportSection varchar2, in_vNotes varchar2, in_dRunDate date, in_nTotalRecords number default 0, in_nCountOfRecords number default 0, in_vGrouping varchar2 default '');


end;
 

/
