--------------------------------------------------------
--  DDL for Package PKG_SAVE_PWS_TEMPLATE
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE PACKAGE "SPIDERS_DATA"."PKG_SAVE_PWS_TEMPLATE" as

$IF $$dev_env_test $THEN

function f_test_for_error (in_vReturned varchar2) return varchar2;

$END

function f_save_pws_template(in_vFlowsFilesName in varchar2, o_nFileValid out number) return varchar2;

end;
 

/
