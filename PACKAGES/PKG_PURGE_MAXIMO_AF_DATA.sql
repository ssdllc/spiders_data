--------------------------------------------------------
--  DDL for Package PKG_PURGE_MAXIMO_AF_DATA
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE PACKAGE "SPIDERS_DATA"."PKG_PURGE_MAXIMO_AF_DATA" as 

$IF $$dev_env_test $THEN

function f_test_for_error (in_vReturned varchar2) return varchar2;
function getAttributeValue(in_vAttribute varchar2, in_jJSON json) return varchar2;


$END


function f_delete_af_temp_data(in_cJson clob) return json;

end;
 

/
