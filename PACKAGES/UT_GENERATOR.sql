--------------------------------------------------------
--  DDL for Package UT_GENERATOR
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE PACKAGE "SPIDERS_DATA"."UT_GENERATOR" AS
   PROCEDURE generateTests;
   PROCEDURE runTests;
   PROCEDURE makePackageFunctionPublic;
   PROCEDURE makePackageFunctionPrivate;
END UT_GENERATOR;
 

/
