--------------------------------------------------------
--  DDL for Package PKG_SAVEBLOBASFILESYSTEMFILE
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE PACKAGE "SPIDERS_DATA"."PKG_SAVEBLOBASFILESYSTEMFILE" as 

function pdf(in_vFlowsFilesName in varchar2, o_nFileValid out number) return varchar2;
function x3d(in_vFlowsFilesName in varchar2, o_nFileValid out number) return varchar2;
function zip(in_vFlowsFilesName in varchar2, o_nFileValid out number) return varchar2;
function jpg(in_vFlowsFilesName in varchar2, o_nFileValid out number) return varchar2;

end;
 

/
