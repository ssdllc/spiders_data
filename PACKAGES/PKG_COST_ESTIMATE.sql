--------------------------------------------------------
--  DDL for Package PKG_COST_ESTIMATE
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE PACKAGE "SPIDERS_DATA"."PKG_COST_ESTIMATE" as 

$IF $$dev_env_test $THEN

function f_test_for_error (in_vReturned varchar2) return varchar2;
function getAttributeValue(in_vAttribute varchar2, in_jJSON json) return varchar2;
function getCorrespondingJsonColumnVal(in_jJson json, in_vColumn varchar2) return varchar2;
function getDeliveryOrderFileName(in_vDeliveryOrderId varchar2) return varchar2;
function getSqlClob(in_vDeliveryOrderId varchar2) return clob;
function insertIntoXMLFilesTable(in_cClob clob,in_vFilename varchar2) return varchar2;



$END

function f_make_cost_estimate_xml(in_cJson clob) return json;

end;
 

/
