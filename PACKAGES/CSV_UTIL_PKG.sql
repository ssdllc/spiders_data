--------------------------------------------------------
--  DDL for Package CSV_UTIL_PKG
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE PACKAGE "SPIDERS_DATA"."CSV_UTIL_PKG" 
as
 
  /*
 
  Purpose:      Package handles comma-separated values (CSV)
 
  Remarks:      
 
  Who     Date        Description
  ------  ----------  --------------------------------
  MBR     31.03.2010  Created
  KJS     20.04.2011  Modified to allow double-quote escaping
 
  */
 
g_default_separator constant  varchar2(1) :=',';
 
function csv_to_array (p_csv_line in varchar2,
                       p_separator in varchar2:= g_default_separator) return t_str_array;

 
function array_to_csv (p_values in t_str_array,
                       p_separator in varchar2:= g_default_separator) return varchar2;


function get_array_value (p_values in t_str_array,
                          p_position in number,
                          p_column_name in varchar2 :=null) return varchar2;


function clob_to_csv (p_csv_clob in clob,
                      p_separator in varchar2:= g_default_separator,
                      p_skip_rows in number :=0) return t_csv_tab pipelined;
                      
function clob_to_csv13 (p_csv_clob in clob,
                      p_separator in varchar2:= g_default_separator,
                      p_skip_rows in number :=0) return t_csv_tab pipelined;


end csv_util_pkg;

/
