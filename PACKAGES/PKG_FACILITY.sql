--------------------------------------------------------
--  DDL for Package PKG_FACILITY
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE PACKAGE "SPIDERS_DATA"."PKG_FACILITY" as 

$IF $$dev_env_test $THEN

function f_test_for_error (in_vReturned varchar2) return varchar2;
function getCorrespondingJsonColumnVal(in_jJson json, in_vColumn varchar2) return varchar2;
function getAttributeValue(in_vAttribute varchar2, in_jJSON json) return varchar2;
function getFacilityId(in_vProductlineListID varchar2,in_vFacilityName varchar2,in_vUIC varchar2,in_vFacilityNO varchar2) return varchar2;


$END


function addByName(in_cJson clob) return json;
--function addByNFAID(in_jJson json) return varchar2;

end;
 

/
