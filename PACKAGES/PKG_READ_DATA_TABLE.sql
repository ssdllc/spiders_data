--------------------------------------------------------
--  DDL for Package PKG_READ_DATA_TABLE
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE PACKAGE "SPIDERS_DATA"."PKG_READ_DATA_TABLE" as

$IF $$dev_env_test $THEN

procedure p_log(in_vString varchar2);
procedure p_log_htp(in_vString varchar2);
function f_test_for_error (in_vReturned varchar2) return varchar2;
function f_parseGroupFilterData(in_jJson json, in_vTableName varchar2) return varchar2;
function f_parseAoDataSearch(in_jJson json) return varchar2;
function f_parseAoDataStart(in_jJson json) return varchar2;
function f_parseAoDataRecPerPage(in_jJson json) return varchar2;
function getAoDataOrderByColumnName (in_jJson json, in_nColumn number) return varchar2;
function f_wrapOrderByColumnName(in_colName varchar2) return varchar2;
function f_parseAoDataOrderBy(in_jJson json) return varchar2;
function f_getDynamicWhere(in_vSearch varchar2, in_vTableName varchar2) return varchar2;
function f_buildFullWhere(in_vWhereClause varchar2, in_vDynamicWhere varchar2, in_vGroupWhere varchar2 ) return varchar2;
function f_buildGroupFilter (in_vTableName varchar2, in_vWhere varchar2, in_vModule varchar2) return json;
function f_buildFullSql (in_vTableName varchar2, in_vWhere varchar2, in_vOrderBy varchar2, in_vStart varchar2, in_nRecordsPerPage number) return json;
function f_getRecordCount (in_vTableName varchar2, in_vWhere varchar2, in_vOrderBy varchar2, in_vStart varchar2, in_nRecordsPerPage number) return json;
function f_parseWhere(in_jJson json) return varchar2;
function f_buildGroupFilterSql (in_vTableName varchar2, in_vWhere varchar2, in_vModule varchar2) return json;
function f_makeSpreadsheetFile (in_vSql varchar2, in_vFolder varchar2, in_vDatatableName varchar2, in_vAO_DATA varchar2) return json;

$END

function f_read_data_table(in_jJson json, in_vTableName varchar2, in_vModule varchar2) return json;
function f_download(in_jJson json, in_vTableName varchar2, in_vModule varchar2) return json;

end;
 

/
