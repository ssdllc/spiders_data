--------------------------------------------------------
--  DDL for Package PKG_PROCESS_MAXIMO
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE PACKAGE "SPIDERS_DATA"."PKG_PROCESS_MAXIMO" AS 

  function f_test_for_error (in_vReturned varchar2) return varchar2;
  function getAttributeValue(in_vAttribute varchar2, in_jJSON json) return varchar2;
  function generate_fac_assets_sql (in_vDeliveryOrderId varchar2) return varchar2;
  function generate_service_request_sql (in_vDeliveryOrderId varchar2) return varchar2;
  function generate_condition_index_sql (in_vDeliveryOrderId varchar2) return varchar2;
  function process_maximo (in_cJSON clob) return json;

END PKG_PROCESS_MAXIMO;
 

/
