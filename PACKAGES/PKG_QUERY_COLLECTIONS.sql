--------------------------------------------------------
--  DDL for Package PKG_QUERY_COLLECTIONS
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE PACKAGE "SPIDERS_DATA"."PKG_QUERY_COLLECTIONS" as

$IF $$dev_env_test $THEN

function f_object_exists (in_vObjectName varchar2) return varchar2;
function f_test_for_error (in_vReturned varchar2) return varchar2;
function getAttributeValue(in_vAttribute varchar2, in_jJSON json) return varchar2;
function f_parseGroupFilterData(in_jJson json) return json;
function f_makeTableEntries (in_jJson json, in_vModule varchar2, in_vCollectionName varchar2) return varchar2;
function f_getCollectionColumns (in_jJson json) return json_list;
function f_getCollectionOrderBy (in_jJson json) return json_list;
function f_makeQueryCollectionViews (in_jlCollectionColumns json_list, in_jlCollectionOrderBy json_list, in_vQueryCollectionName varchar2) return varchar2;
function f_makeGroupFilterViews (lc_jlCollectionColumns json_list, lc_jlCollectionOrderBy json_list, lc_vQueryCollectionName varchar2) return varchar2;
function f_create_rest_table_maps (in_vModule varchar2, in_vQueryCollectionName varchar2) return varchar2;


$END

function f_make_qry_collection_objects(in_jJson json) return varchar2;

end;
 

/
