--------------------------------------------------------
--  DDL for Package PKG_REGION_ACTIVITIES
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE PACKAGE "SPIDERS_DATA"."PKG_REGION_ACTIVITIES" AS 

  /* TODO enter package declarations (types, exceptions, methods etc) here */
  function f_test_for_error (in_vReturned varchar2) return varchar2;
  function getCorrespondingJsonColumnVal(in_jJson json, in_vColumn varchar2) return varchar2;
  function f_delete(in_cJson clob) return json;

END PKG_REGION_ACTIVITIES;
 

/
