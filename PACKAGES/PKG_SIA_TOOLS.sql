--------------------------------------------------------
--  DDL for Package PKG_SIA_TOOLS
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE PACKAGE "SPIDERS_DATA"."PKG_SIA_TOOLS" AS 
   FUNCTION validateSIAValue(in_value varchar2, in_siaquestionid number)
      RETURN varchar2;
   FUNCTION is_number(p_string varchar2)
      RETURN INT;
END pkg_sia_tools;

/
