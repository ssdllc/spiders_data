--------------------------------------------------------
--  DDL for Package FILE_TOOLS
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE PACKAGE "SPIDERS_DATA"."FILE_TOOLS" as 


/*
This software has been released under the MIT license:

  Copyright (c) 2011 Synergy Software Design, LLC

  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.
*/


procedure upload_wf_xml(IN_nADOID number, IN_vFILETYPE varchar2 default 'deliverable');
procedure download_xml_file(in_nFileskey number);
procedure delete_file(in_nFileskey number, in_nListid number, in_nAdoid number default 0);
procedure upload_file (IN_vFILE_TYPE varchar2, IN_vPost_Upload_Type varchar2, IN_nPost_Upload_Id number, IN_nLISTID number);
procedure display_image(IN_NTHUMBNAILID number);
procedure display_thumbnail(IN_NTHUMBNAILID number);
PROCEDURE download_bfile (IN_nFileID IN   VARCHAR2, IN_vSource   IN   VARCHAR2 DEFAULT 'DATA_PUMP');

function get_FolderID (in_vFolder varchar2) return varchar2;
function get_FolderName (in_nPostUploadID number) return varchar2;

end;
 

/
