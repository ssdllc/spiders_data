--------------------------------------------------------
--  DDL for Package PKG_TABLES
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE PACKAGE "SPIDERS_DATA"."PKG_TABLES" as

$IF $$dev_env_test $THEN

    function f_parse_json_table_name (in_jJson json) return varchar2;
    function f_drop_sequence (in_vName varchar2) return varchar2;
    function f_drop_insert_trigger (in_vName varchar2) return varchar2;
    function f_drop_update_trigger (in_vName varchar2) return varchar2;
    function f_drop_table (in_vName varchar2) return varchar2;
    function getNextSequence return varchar2;
    function getValidObjectCount(in_vName varchar2) return varchar2;
    function f_create_sequence (in_vName varchar2) return varchar2;
    function f_create_alter_update_trigger (in_vName varchar2) return varchar2;
    function f_create_alter_insert_trigger (in_vName varchar2) return varchar2;
    function f_create_update_trigger (in_vName varchar2) return varchar2;
    function f_create_insert_trigger (in_vName varchar2, in_vColumnName varchar2) return varchar2;
    function f_create_table_comment (in_vName varchar2, in_vComment varchar2) return varchar2;
    function f_build_columns_sql_segment (in_jJson json, in_vColumnsCount varchar2) return varchar2;
    function f_build_col_names_sql_segment (in_jJson json, in_vColumnsCount varchar2) return varchar2;
    function f_build_pk_sql_segment (in_vTable varchar2, in_vColumnName varchar2) return varchar2;
    function getPrimaryKeyColumn (in_jJson json) return varchar2;
    function f_parse_json_table_column_pk (in_jJson json) return varchar2;
    function f_create_table (in_vSql varchar2) return varchar2;
    function f_build_create_table_sql (in_vTableName varchar2, in_vSqlSegmentCol varchar2, in_vSqlSegmentPk varchar2) return varchar2;
    function f_parse_json_table_columns (in_jJson json) return varchar2;
    function f_parse_json_table_comment (in_jJson json) return varchar2;
    function f_test_for_error (in_vReturned varchar2) return varchar2;
    function f_build_model_view_sql (in_vName varchar2, in_vPrimaryKey varchar2, in_vColumns varchar2) return varchar2;
    function f_create_model_view (in_jTable json) return varchar2;
    function f_build_data_table_view_sql (in_vName varchar2) return varchar2;
    function f_create_data_table_view (in_vName varchar2) return varchar2;
    function makeOrderByRecords (in_vTableName varchar2, in_jJson json) return varchar2;
    function f_parse_json_module (in_jJson json) return varchar2;
    function f_parse_json_model_name (in_jJson json) return varchar2;
    function f_create_rest_table_maps (in_jJson json, in_vTableName varchar2) return varchar2;
    

$END

    function f_object_exists (in_vObjectName varchar2) return varchar2;
    function getColumnLength (in_jJson json) return varchar2;
    function f_make_all_table_objects( in_jTable json) return varchar2;
  
end;
 

/
