--------------------------------------------------------
--  DDL for Package PKG_CALL_ANY_PROC
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE PACKAGE "SPIDERS_DATA"."PKG_CALL_ANY_PROC" as

function f_call_any_proc(in_procName_unsanitized varchar2, in_args_unsanitized varchar2) return varchar2;

end;
 

/
