--------------------------------------------------------
--  DDL for Package EXCELDOCTYPEUTILS
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE PACKAGE "SPIDERS_DATA"."EXCELDOCTYPEUTILS" AS

  
   /* These constants are associated with the creation hyperlink cells.
   
      hyperlinked data should look like: ExcelHRef:::#Sheet1!A1:::Sheet1 (<hrefIndicator>:::<target>:::<label>)
	   or ExcelHref:::http://www.google.com:::Google
   
   */

   HREF_INDICATOR CONSTANT VARCHAR2(9) := 'ExcelHRef';
   HREF_SEP_CHAR  CONSTANT VARCHAR2(3)  := ':::';
   
   HF_PAGE_NUMBER_SINGLE CONSTANT VARCHAR2(3) := 'PNS';  /* 1 */
   HF_PAGE_NUMBER_PAGES  CONSTANT VARCHAR2(3) := 'PNP';  /* 1 of 3 ... */
   HF_DATE               CONSTANT VARCHAR2(3) := 'SDT';  /* single date, no time */
   HF_DATE_TIME          CONSTANT VARCHAR2(3) := 'DTT';  /* Date and Time */
   HF_TEXT               CONSTANT VARCHAR2(3) := 'TXT';  /* Free text ... title */
   HF_FILEPATH           CONSTANT VARCHAR2(3) := 'FPT';  /* File Path to location opened from */
   
   HF_LEFT               CONSTANT VARCHAR2(3)  := 'LFT';
   HF_RIGHT              CONSTANT VARCHAR2(3)  := 'RHT';
   HF_CENTER             CONSTANT VARCHAR2(3)  := 'CTR';
   
   -- Page Orientation Indicators
   WS_ORIENT_PORTRAIT    CONSTANT VARCHAR(8)   := 'Portrait';
   WS_ORIENT_LANDSCAPE   CONSTANT VARCHAR(9)   := 'Landscape';
   
   TYPE t_refcursor IS REF CURSOR;

   pv_result_table  RESULT_TABLE := RESULT_TABLE();


   /* This type allows the user to create a title row at the top of a worksheet */

   TYPE T_SHEET_TITLE IS RECORD(
       title      VARCHAR2(1000),
       cell_span  NUMBER(12),
       style      VARCHAR2(200)
   );

   /* This type allows the user to add conditional formatting to worksheet cells. */

   TYPE T_CONDITION IS RECORD(
       qualifier    VARCHAR2(200),
       value        VARCHAR2(200),
       format_style VARCHAR2(500)
   );

   /* An Array of COND that allows the user to add multiple conditions to a worksheet. */
   TYPE CONDITIONS_TABLE IS TABLE OF T_CONDITION;


   TYPE T_CONDITIONAL_FORMATS IS RECORD(
      range             VARCHAR2(200),
      conditions        CONDITIONS_TABLE
   );
   

   TYPE CONDITIONAL_FORMATS_TABLE IS TABLE OF T_CONDITIONAL_FORMATS;
   
   /* These types allow the user to associate a print header and footer with a worksheet. */
   TYPE T_WORKSHEET_HF_DATA IS RECORD(
       hf_type       VARCHAR2(3),
	   text          VARCHAR2(200),
	   position      VARCHAR2(3),
       fontsize      VARCHAR2(3) 
   );
   
   TYPE T_WORKSHEET_HF_MARGINS IS RECORD(
        Zoom          NUMBER(4),
        PageBreakZoom NUMBER(4),
        MarginB       NUMBER(4), 
        MarginT       NUMBER(4),
        MarginL       NUMBER(4),
        MarginR       NUMBER(4)      
   );
   
   
   TYPE WORKSHEET_HF_TABLE IS TABLE OF T_WORKSHEET_HF_DATA;
   

   /* This record contains all of the components required to create an Excel Report worksheet. */
   TYPE T_WORKSHEET_DATA IS RECORD(
       query                        VARCHAR2(16000),
       title                        T_SHEET_TITLE,
       worksheet_name               VARCHAR2(50),
       worksheet_header             WORKSHEET_HF_TABLE,
       worksheet_footer             WORKSHEET_HF_TABLE, 
	   worksheet_page_margins       T_WORKSHEET_HF_MARGINS,
       worksheet_cond_formats       CONDITIONAL_FORMATS_TABLE,
       worksheet_list_delimiter     VARCHAR2(10),
       worksheet_orientation        VARCHAR2(9),
       worksheet_show_gridlines     BOOLEAN,
       col_header_freeze            BOOLEAN,
       col_header_repeat            BOOLEAN,
	   col_firstcol_freeze          BOOLEAN,
       col_count                    NUMBER(3),
       col_width_list               VARCHAR2(500),
       col_caption                  VARCHAR2(2000),
       col_header_list              VARCHAR2(2000),
       col_header_style_list        VARCHAR2(2000),
       col_datatype_list            VARCHAR2(4000),
       col_style_list               VARCHAR2(5000),
       col_formula_list             VARCHAR2(4000),
       col_formula_style_list       VARCHAR2(4000)
   );


   /* An Array of T_WORKSHEET_DATA allows us to create an excel document with multiple worksheets based on
      different queries. */
   TYPE WORKSHEET_TABLE IS TABLE OF T_WORKSHEET_DATA;


   /* This record structure matches the createStyle method of the ExcelDocumentType. */
   TYPE T_STYLE_DEF IS RECORD(
                                p_style_id         VARCHAR2(50),
                                p_font             VARCHAR2(50),
                                p_ffamily          VARCHAR2(50),
                                p_fsize            VARCHAR2(50),
                                p_bold             VARCHAR2(1),
                                p_italic           VARCHAR2(1),
                                p_underline        VARCHAR2(10),
                                p_text_color       VARCHAR2(50),
                                p_cell_color       VARCHAR2(50),
                                p_cell_pattern     VARCHAR2(50),
                                p_align_vertical   VARCHAR2(50),
                                p_align_horizontal VARCHAR2(50),
								p_align_indent     VARCHAR2(50),
                                p_wrap_text        VARCHAR2(1),
                                p_rotate_text_deg  VARCHAR2(3),
                                p_number_format    VARCHAR2(100),
                                p_custom_xml       VARCHAR2(4000) 
                             );


   /* Collection of styles that can applied to cells */
   TYPE STYLE_LIST IS TABLE OF T_STYLE_DEF;
   

   /* These four procedures are convenience procedures that make it easy to a record to it associated collection. */

   PROCEDURE addStyleType(p_style_array IN OUT NOCOPY STYLE_LIST, p_style_rec T_STYLE_DEF);

   PROCEDURE addWorksheetType(p_worksheet_data IN OUT NOCOPY WORKSHEET_TABLE, p_worksheet_rec T_WORKSHEET_DATA);

   PROCEDURE addConditionType(p_condition_data IN OUT NOCOPY CONDITIONS_TABLE, p_condition_rec T_CONDITION);

   PROCEDURE addConditionalFormatType(p_cond_format_data IN OUT NOCOPY CONDITIONAL_FORMATS_TABLE, p_cond_format_rec T_CONDITIONAL_FORMATS);
   
   PROCEDURE addHeaderFooterType(p_hf_data IN OUT NOCOPY WORKSHEET_HF_TABLE, p_hf_rec T_WORKSHEET_HF_DATA);
   
   /* This function creates an ExcelDocTypeUtils Worksheet hyperlink reference. */
   FUNCTION createWorksheetLink(p_worksheet_name VARCHAR2 := NULL,
                                p_link_text      VARCHAR2 := NULL) RETURN VARCHAR2;
                                
     /* This function creates an ExcelDocTypeUtils External hyperlink reference. */
   FUNCTION createExternalLink(p_url        VARCHAR2 := NULL,
                               p_link_text  VARCHAR2 := NULL) RETURN VARCHAR2;
                               
   /* Given select statement, this function will generate a Column Header list based on column details or column alias in the query's SELECT clause.
      The p_list_delimiter parameter needs to match the list delimiter defined for the current worksheet. */
   FUNCTION getColHeaderString(p_query          VARCHAR2 := NULL,
                               p_list_delimiter VARCHAR2 := ',') RETURN VARCHAR2;
                               
                               
               

   /* This function creates the Excel Document based on the data passed in the p_worksheet_data collection. */

   FUNCTION createExcelDocument(p_worksheet_data WORKSHEET_TABLE,
                                p_style_data     STYLE_LIST := STYLE_LIST()) RETURN ExcelDocumentType;   


END;
 
 

/
