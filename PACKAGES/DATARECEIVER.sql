--------------------------------------------------------
--  DDL for Package DATARECEIVER
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE PACKAGE "SPIDERS_DATA"."DATARECEIVER" as


/*
This software has been released under the MIT license:

  Copyright (c) 2010 Synergy Software Design, LLC

  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.
*/



procedure upd_actn_do(IN_JSON varchar2);
procedure del_do_dc_type(IN_nADOID number, IN_nLISTID number);
procedure reset_actn_do_dc(IN_APID number);
procedure add_actn_do_fac(IN_ADOID number, IN_NFA varchar2, IN_PL number, IN_nDOFID number default 0);
procedure del_actn_do_fac(IN_DOFID number);
procedure do_fac_specinstr(IN_JSON varchar2);
procedure add_do_fac_dc_type(IN_nADOID number, IN_vITEM varchar2, IN_DEFAULT varchar2 default '1', IN_PL number default 100085, IN_APPLYTOALL number default 0);
procedure del_do_fac_dc_type(IN_nADOID number, IN_nLISTID number);
procedure add_actn_do_dc_type(IN_nADOID number, IN_vITEM varchar2, IN_DEFAULT varchar2 default '1', IN_PL number default 100085);
procedure add_planned_action(IN_YEAR number default 2011, IN_FUNDSOURCELISTID number default 100032, IN_ACTIONTYPELISTID number default 100081, IN_INHOUSE number default 0, IN_KTR number default 0, IN_UIC varchar2, IN_PL number default 100085, IN_QTR varchar2 default 'Q1', IN_nAPID number default 0);
procedure del_planned_action(IN_APID number);
procedure add_base_do(IN_APID number, IN_nCONTRACTLISTID number, IN_nWORKTYPELISTID number, IN_vDONAME varchar2, IN_nADOID number default 0);
procedure del_delivery_order(IN_ADOID number);
procedure add_do_prerequisite_rows(IN_nADOID number, IN_vCONFIRM_GROUP varchar2);
procedure upd_do_confirmation(IN_nCID number, IN_nVALUE number, IN_vUSER varchar2);
procedure upd_do_confirmation_fac_si(IN_nDOFID number, IN_nVALUE number, IN_vUSER varchar2);
procedure upd_planned_action(IN_nAPID number, IN_nFundsource number, in_nInhouse number, in_nContractor number, in_nYear number, in_nQuarter number);
procedure add_set_plan(IN_vNote varchar2, IN_nPlanTypeListID number default 0, IN_vBeginYear varchar2, IN_vEndYear varchar2, IN_nFundsource_Listid number, IN_vUser varchar2);
procedure approve_set_plan(in_nSetPlanId number);
procedure del_set_plan(IN_nSetPlanId number);
procedure add_report(IN_vREPORTTITLE varchar2, IN_vREPORTAUTHOR varchar2, IN_vESTSTARTDATE varchar2, IN_vCONTRACTOR varchar2, IN_vCONTRACTNUMBER varchar2, IN_vREPORTTYPE varchar2, IN_vDIVISION varchar2, IN_vREPORTNUM varchar2);
procedure del_report_rec(IN_vREPORTID varchar2);
procedure update_report_rec(IN_vREPORTID varchar2, IN_vupd_report_title varchar2, IN_vupd_report_author varchar2, IN_vupd_report_Contractor varchar2, IN_vupd_report_Contract varchar2, IN_vupd_Report_StartDate varchar2);
procedure add_note(IN_nNOTESKEY number, IN_vNote varchar2, IN_vContact varchar2 default '', in_vToolname varchar2);
procedure del_note(IN_nNOTEID number);
procedure upd_spec_instr_val(IN_nAFSIID number, IN_vVal varchar2);
procedure upd_planned_actn_rec_funds(IN_nAPID number, IN_vReserve varchar2, in_vRecDate varchar2, in_vRecInh varchar2, in_vRecKtr varchar2);
procedure upd_oversight(IN_nPOID number, IN_nValQ1 number, IN_nValQ2 number, IN_nValQ3 number, IN_nValQ4 number);

END datareceiver;
 

/
