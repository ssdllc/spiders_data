--------------------------------------------------------
--  DDL for Package PKG_SIA_UPLOAD
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE PACKAGE "SPIDERS_DATA"."PKG_SIA_UPLOAD" AS 

function f_update_sia_dofid(in_cJson clob) return json; 
function getAttributeValue(in_vAttribute varchar2, in_jJSON json) return varchar2;
function f_test_for_error (in_vReturned varchar2) return varchar2;

END PKG_SIA_UPLOAD;
 

/
