--------------------------------------------------------
--  DDL for Package PKG_CONTRACT_CAT_CODES
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE PACKAGE "SPIDERS_DATA"."PKG_CONTRACT_CAT_CODES" AS 

  /* TODO enter package declarations (types, exceptions, methods etc) here */
  function f_test_for_error (in_vReturned varchar2) return varchar2;
  function getCorrespondingJsonColumnVal(in_jJson json, in_vColumn varchar2) return varchar2;
  function f_delete(in_cJson clob) return json;

END PKG_CONTRACT_CAT_CODES;
 

/
