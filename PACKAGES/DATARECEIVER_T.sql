--------------------------------------------------------
--  DDL for Package DATARECEIVER_T
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE PACKAGE "SPIDERS_DATA"."DATARECEIVER_T" as


/*
This software has been released under the MIT license:

  Copyright (c) 2010 Synergy Software Design, LLC

  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.
*/



procedure add_drop_object 
(
	
  
  in_vDescription varchar2, 
	in_cX3D_Node clob, --varchar2, 
	in_vObjectLabel varchar2 default '', 
	in_nObjectTypeListID number default 10085, 
	in_vSessionKey varchar2,
	in_vLength varchar2 default '',
	in_vWidth varchar2 default '',
	in_vHeight varchar2 default '',
	in_vColor varchar2 default '',
	in_vOpacity varchar2 default '1',
	in_vTranslation varchar2,
	in_vRotation varchar2  default '1 0 0 1.57',
	in_vRadius varchar2 default '',
  in_vParent_Def varchar2 default '',
  in_vNode_Def varchar2,
  in_vNodeType varchar2,
  in_vPitch varchar2  default '0 0 1 0',
  in_vRoll varchar2  default '1 0 0 0'
  

);

procedure add_session
(
  in_nSceneId number,
  in_vDescription varchar2,
  in_vEncrypted varchar2,
  in_vExpirationDate varchar2
);

procedure update_drop_object 
(
  in_nObjectid number,
	IN_vDescription varchar2, 
	in_cX3D_Node clob, 
	in_vLength varchar2,
	in_vWidth varchar2,
	in_vHeight varchar2,
	in_vColor varchar2,
	in_vOpacity varchar2,
	in_vX varchar2,
	in_vY varchar2,
	in_vZ varchar2,
	in_vRotation varchar2,
	in_vRadius varchar2,
  in_vPitch varchar2,
  in_vRoll varchar2,
  in_vYaw varchar2,
  in_vLocal_Id varchar2
);

procedure del_drop_object 
(
  in_vNode_Def varchar2,
  in_vSessionkey varchar2
);
  
procedure update_scene_config(
  in_Action varchar2,
  in_XYZ varchar2,
  in_ROTATION varchar2,
  in_FILEPATH varchar2 default null,
  in_DESCRIPTION varchar2 default null,
  in_SCENEMODELID number default null,
  in_MODELVERSIONID number default null,
  in_SOURCEMODELID number default null,
  in_MODEL_NAME varchar2 default null,
  in_SCENECONFIGID varchar2 default null
);

procedure update_source_model(
  in_Action varchar2,
  in_SOURCEMODELID number default null,
  in_MODEL_NAME varchar2 default null,
  in_MODEL_NUMBER varchar2 default null,
  in_HOMEPORT varchar2 default null  
);

procedure update_mdl_version(
  in_Action varchar2,
  in_MODELVERSIONID number default null,
  in_SOURCEMODELID number default null,
  in_DESCRIPTION varchar2 default null,
  in_FILEPATH varchar2 default null 
);

procedure update_scene(
  in_Action varchar2,
  in_SCENEID number default null,
  in_DESCRIPTION varchar2 default null,
  in_LOCATION varchar2 default null,
  in_FILEPATH varchar2 default null  
);

procedure update_t_scene_config(
  in_Action varchar2,
  in_SCENECONFIGID number default null,
  in_MODELVERSIONID number default null,
  in_SCENEMODELID number default null,
  in_XYZ varchar2 default null,
  in_ROTATION varchar2 default null 
);

procedure update_lead(
  in_Action varchar2,
  in_LEADID number default null,
  in_MODEL_TYPE varchar2 default null,
  in_LOCATION varchar2 default null,
  in_NFA varchar2 default null,
  in_DESCRIPTION varchar2 default null,
  in_REQUESTING_USER varchar2 default null
);

END datareceiver_t;
 

/
