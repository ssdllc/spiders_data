create or replace PACKAGE PKG_XML_FLATTEN IS 


  g_vMaxNode number := 0;
  
  FUNCTION convertXML (in_nXMLFileID number) return XMLTYPE;
  function XMLFlattenDOM (xmldoc in XMLType) return XMLEdgeTable pipelined;

  FUNCTION flattenXML(in_nfileId number, in_nADOID number, in_vUser varchar2) return varchar2;
  
END PKG_XML_FLATTEN;