--------------------------------------------------------
--  DDL for Package ITSTAR_XML_UTIL
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE PACKAGE "SPIDERS_DATA"."ITSTAR_XML_UTIL" is

  -- Author  : Stefan Armbruster
  -- Created : 10.11.2011 11:50:03
  -- Purpose : XML Utils

  -- Public function and procedure declarations

  function xml2json(i_xml in xmltype) return xmltype;
  function xmltype2clob(i_xml in xmltype) return clob;
  function sql2xml(i_sql_string in varchar2) return xmltype;

end itstar_xml_util;
 

/
