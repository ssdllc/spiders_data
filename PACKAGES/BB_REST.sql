--------------------------------------------------------
--  DDL for Package BB_REST
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE PACKAGE "SPIDERS_DATA"."BB_REST" AS 

/*
This software has been released under the MIT license:

  Copyright (c) 2011 Synergy Software Design, LLC

  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.
  
  EDITED 20170912 MR SPIDERS 3D: [SPD3D][45] Sync

*/

procedure create_(in_cJSON clob);
procedure update_(in_cJSON clob);
procedure delete_(in_cJSON clob);
procedure read_(in_cJSON clob);

procedure createModel(in_cJSON clob);
procedure updateModel(in_cJSON clob);
procedure deleteModel(in_cJSON clob);
procedure readModel(in_cJSON clob);
procedure readQueryCollection(in_cJSON clob);
procedure readCollection(in_cJSON clob);
procedure readDataTable(in_cJSON clob);
procedure downloadDataTable(in_cJSON clob);
procedure createModelCustom(in_cJSON clob);
procedure deleteModelCustom(in_cJSON clob);
procedure readQueryCollectionCustom(in_cJSON clob);
procedure readDataTableCustom(in_cJSON clob);
procedure downloadDataTableCustom(in_cJSON clob);


function f_getJsonDataRecordCount(in_jJSON json) return varchar2;
function f_getIndividualDataJson(in_jJSON json, in_nI number, in_vModule varchar2, in_vTarget varchar2) return json;

END BB_REST;
 

/
