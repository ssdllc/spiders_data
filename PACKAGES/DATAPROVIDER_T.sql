--------------------------------------------------------
--  DDL for Package DATAPROVIDER_T
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE PACKAGE "SPIDERS_DATA"."DATAPROVIDER_T" AS 

/*
This software has been released under the MIT license:

  Copyright (c) 2012 Synergy Software Design, LLC

  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.
*/



procedure get_geometry_types;
procedure get_colors;
procedure get_object_id;

procedure get_model_types;
procedure get_model_group_types(IN_nOBJECT_TYPE_LISTID number);
procedure get_models(IN_nOBJECT_TYPE_LISTID number, IN_vGroupValue varchar2);
procedure get_model_versions(IN_nSourceModelid number);

procedure get_scenes;
procedure get_session_scene_url(in_vSessionkey varchar2);
procedure get_session_id(in_vSessionKey varchar2);
procedure get_session_object_ids(in_nsessionid number);
procedure get_session_object(in_nObjectid number);

procedure get_session_info(in_vSessionKey varchar2);
procedure get_session_updates(in_vPreviousTimestamp varchar2, in_vCurrentTimestamp varchar2, in_vSessionkey varchar2);
procedure get_server_time;

--T BELOW----
procedure get_scene_configs(in_SCENEMODELID number default null);
procedure get_source_models;
--procedure get_model_versions(in_sourcemodelid number);
procedure get_leads;
procedure get_session_history;
--T ABOVE----
END DATAPROVIDER_T;
 
 

/
