--------------------------------------------------------
--  DDL for Package SQL_STATEMENT_FUNCTIONS
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE PACKAGE "SPIDERS_DATA"."SQL_STATEMENT_FUNCTIONS" as 


  function f_update (in_jJson json, in_vTableName varchar2) return varchar2;
  function f_insert (in_jJson json, in_vTableName varchar2, o_nSequenceId2 out number) return varchar2;
  function f_delete (in_jJson json, in_vTableName varchar2) return varchar2;


end;
 

/
