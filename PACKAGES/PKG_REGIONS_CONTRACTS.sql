--------------------------------------------------------
--  DDL for Package PKG_REGIONS_CONTRACTS
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE PACKAGE "SPIDERS_DATA"."PKG_REGIONS_CONTRACTS" AS 

  function f_test_for_error (in_vReturned varchar2) return varchar2;
  function getCorrespondingJsonColumnVal(in_jJson json, in_vColumn varchar2) return varchar2;
  function f_delete(in_cJson clob) return json; 

END PKG_REGIONS_CONTRACTS;
 

/
