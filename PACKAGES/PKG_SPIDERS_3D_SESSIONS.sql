--------------------------------------------------------
--  DDL for Package PKG_SPIDERS_3D_SESSIONS
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE PACKAGE "SPIDERS_DATA"."PKG_SPIDERS_3D_SESSIONS" AS 

  /* TODO enter package declarations (types, exceptions, methods etc) here */
  function getJsonList(in_vAttribute varchar2, in_jJSON json) return json_list;
  --function f_add_delivery_order_fac(in_cJSON clob) return json;
  function getJsonListItem(in_nRow number, in_jlJSON json_list) return json;
  function getAttributeValue(in_vAttribute varchar2, in_jJSON json) return varchar2;
  function f_test_for_error (in_vReturned varchar2) return varchar2;
  function makeInsertSessionMembersSql(lc_nSessionMemberId number) return varchar2;
  function getSessionId(lc_vSessionKey varchar2) return number;
  function getSessionMemberId(lc_nSessionId number) return number;
  function getNextSessionMemberSequence return number;
  function getSceneId(lc_nSessionId number) return number;
  function getSceneDescriptionPath(lc_nSceneId number) return json;
  function getUpdatedTimestamp(lc_nSessionId number) return varchar2;
  function getSceneInventory(lc_nSessionId number) return json;
  function getWhiteboards(lc_nSessionId number) return json;
  function getInteractiveModels return json;
  function getAllClasses return json;
  function getAllCategories return json;
  function syncSessions(in_cJSON clob) return json;
  function pullSessionUpdate(lc_nSessionId number, lc_vTimeStamp varchar2) return json;
  function pushUpdates(in_cJSON clob) return json;
  function f_append_whiteboard(in_cJSON clob) return json;
  function f_launch_session(in_cJSON clob) return json;
  function f_get_markings(in_cJSON clob) return json;
  function syncSessionsCustObj(in_cJSON clob) return json;
  function pushUpdatesCustObj(in_cJSON clob) return json;

END PKG_SPIDERS_3D_SESSIONS;

/
