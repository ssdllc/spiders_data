--------------------------------------------------------
--  DDL for Package DATAPROVIDER
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE PACKAGE "SPIDERS_DATA"."DATAPROVIDER" AS 

/*
This software has been released under the MIT license:

  Copyright (c) 2010 Synergy Software Design, LLC

  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.
*/

function get_caption (IN_nID number) return varchar2;
function test_bfile (IN_nReportId number) return number;
procedure facilities(IN_UIC varchar2 default '', IN_PL number default 100085, IN_SA varchar2 default '', IN_FACLIST varchar2 default '', IN_nDOID varchar2 default '0');
procedure facilities_search(IN_SEARCH varchar2 default '', IN_PL number default 100085);
procedure activities(IN_PL number default 100085);
procedure states(IN_PL number default 100085);
procedure special_areas(IN_UIC varchar2);
procedure deliveryOrder(IN_DOID number default 100000);
procedure LOVS(IN_PL number default 100085);
procedure facs(IN_DOID number default 100000);
procedure fac(IN_DOFID number default 100000);
procedure plannedactions(IN_PL varchar2 default '100085', IN_YEAR varchar2 default '2011');
procedure planned_action_dos(IN_APID number);
procedure planned_action_do_exec_summary(IN_ADOID number);
procedure do_deliverable_files(IN_nADOID number);
procedure do_costestimate_files(IN_nADOID number);
procedure display_ufii(in_nDOFID number, in_vLevel varchar2 default '');
procedure do_direct_costs(IN_nADOID number);
procedure do_confirmations(IN_nADOID number);
procedure get_planner_actions(IN_FILTER_JSON varchar2, IN_ALL_LOCATIONS boolean default false);
procedure plan_details(IN_APID number);
procedure fundSources(IN_PL number default 100085);
procedure productlines;
procedure get_set_plan_types;
procedure get_funding_documents(IN_nYEAR number, IN_nFUNDSOURCE number);
procedure get_gce_list;
procedure get_uploaded_gce_docs(in_nADOID number);
procedure get_deliverables;
procedure get_uploaded_deliverable_docs(in_nADOID number);
procedure funding_requests(IN_YEAR varchar2 default '2011');
procedure get_reports(IN_nPage number default 1, IN_vSearch varchar2 default '^^^');
procedure get_Report_Types;
procedure get_Report_Div_Types;
procedure get_notes(IN_nNOTESKEY number);
procedure username;
procedure get_do_fac_dc_types;
procedure get_do_dc_types(in_nAdoid number);
procedure get_spec_instr_list (IN_nPL number, in_nAdoid number);
procedure get_spec_instr_values (IN_nPL number, IN_nADOID number);
procedure get_fac_spec_instr_values (IN_nADOID number);
procedure get_fundreq_versions (IN_nSetPlanId number);
procedure get_funding_requests (IN_nYear number);
procedure get_actions_per_fund_req (IN_nSetplanid number);
procedure get_contract_regions (IN_nPL number);
procedure get_contracts_per_region (IN_nREGIONID number);
procedure get_work_types(IN_nPL number);
procedure get_report_total_pages;
procedure get_photos;
procedure get_rights;
procedure get_oversight(IN_nSETPLANID number);
procedure get_ai_supporting_files (IN_nDOFID number);
procedure get_mooring_photos(IN_nDOFID number);
procedure get_do_files(IN_nADOID number);
procedure get_do_fac_files(IN_nDOFID number);
procedure get_dof_per_fac(IN_nFACID number);
procedure get_carousel_photos(IN_nFACID number);

END DATAPROVIDER;
 

/
