--------------------------------------------------------
--  File created - Tuesday-August-21-2018   
--------------------------------------------------------
--------------------------------------------------------
--  DDL for Package PKG_XML_PARSE
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE PACKAGE "SPIDERS_DATA"."PKG_XML_PARSE" AS

$IF $$dev_env_test $THEN

  FUNCTION getCorrespondingJsonColumnVal(in_jJson json, in_vColumn varchar2) RETURN varchar2;
	FUNCTION f_test_for_error (in_vReturned varchar2) RETURN varchar2;
	FUNCTION f_test_for_error (in_vReturned number) RETURN varchar2;
	FUNCTION getMaxLevel(in_vXPath varchar2, in_vUser varchar2 default 'NONE', in_nADOID number) RETURN number;
	FUNCTION getExceptions(in_vTableName varchar2) RETURN json;
	FUNCTION getTableName(in_vFPath varchar2) RETURN varchar2;
	FUNCTION getTablePrimaryKeyColumn(in_vTableName varchar2) RETURN varchar2;
	FUNCTION setAttributes(in_nCurrentLevel number, in_vXPath varchar2, in_jExceptions json, in_jParentId json, in_vUser varchar2 default 'NONE', in_nADOID number) RETURN json;
  FUNCTION f_insert_siblings(in_vXPath varchar2, in_jParent json, in_vUser varchar2 default 'NONE', in_nADOID number) return varchar2;
  FUNCTION cleanActions(in_nADOID number) return varchar2;
  FUNCTION insertLog(in_nDOID number, in_vAction varchar2, in_vStatus varchar2) return varchar2;
  --PROCEDURE extractDataFromFlattenTable(in_nXMLLevel number, in_vXPath varchar2, in_jParentId json default null, in_vUser varchar2 default 'NONE', in_nADOID number);

$END

  FUNCTION collectXMLData(in_cJSON clob, in_vUser varchar2 default 'NONE') return json;
  FUNCTION isProcessing (in_cJSON clob) return json;
  PROCEDURE extractDataFromFlattenTable(in_nXMLLevel number, in_vXPath varchar2, in_jParentId json default null, in_vUser varchar2 default 'NONE', in_nADOID number);


END PKG_XML_PARSE;

/
