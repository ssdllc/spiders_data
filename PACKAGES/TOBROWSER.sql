--------------------------------------------------------
--  DDL for Package TOBROWSER
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE PACKAGE "SPIDERS_DATA"."TOBROWSER" AS 

procedure message(IN_vStatus varchar2, IN_vMessage varchar2);
procedure data(IN_vSql varchar2, in_vJsonPFromRemote varchar2 default null);
procedure data(in_jJson json, in_vJsonPFromRemote varchar2 default null);

END tobrowser;
 

/
