--------------------------------------------------------
--  DDL for Package PKG_DATA_TABLES
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE PACKAGE "SPIDERS_DATA"."PKG_DATA_TABLES" as

$IF $$dev_env_test $THEN

function f_object_exists (in_vObjectName varchar2) return varchar2;
function f_test_for_error (in_vReturned varchar2) return varchar2;
function getAttributeValue(in_vAttribute varchar2, in_jJSON json) return varchar2;
function f_parseGroupFilterData(in_jJson json) return json;
function f_makeTableEntries (in_jJson json, in_vModule varchar2, in_vCollectionName varchar2) return varchar2;
function f_makeGroupFilterViews (in_vModule varchar2, in_vCollectionName varchar2) return varchar2;


$END

function f_make_data_table_objects(in_jJson json) return varchar2;

end;
 

/
