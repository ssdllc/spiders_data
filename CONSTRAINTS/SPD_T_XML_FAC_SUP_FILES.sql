--------------------------------------------------------
--  Constraints for Table SPD_T_XML_FAC_SUP_FILES
--------------------------------------------------------

  ALTER TABLE "SPIDERS_DATA"."SPD_T_XML_FAC_SUP_FILES" ADD CONSTRAINT "SPD_T_XML_FAC_SUP_FILES_PK" PRIMARY KEY ("FILEID")
  USING INDEX PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  TABLESPACE "SPIDERS_DATA_TABLES"  ENABLE;
