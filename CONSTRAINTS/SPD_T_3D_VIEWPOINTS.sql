--------------------------------------------------------
--  Constraints for Table SPD_T_3D_VIEWPOINTS
--------------------------------------------------------

  ALTER TABLE "SPIDERS_DATA"."SPD_T_3D_VIEWPOINTS" ADD CONSTRAINT "SPD_T_3D_VIEWPOINTS_PK" PRIMARY KEY ("VIEWPOINTID")
  USING INDEX PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  TABLESPACE "SPIDERS_DATA_TABLES"  ENABLE;
