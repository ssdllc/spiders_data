--------------------------------------------------------
--  Constraints for Table APEX$_WS_HISTORY
--------------------------------------------------------

  ALTER TABLE "SPIDERS_DATA"."APEX$_WS_HISTORY" MODIFY ("DATA_GRID_ID" NOT NULL ENABLE);
  ALTER TABLE "SPIDERS_DATA"."APEX$_WS_HISTORY" MODIFY ("WS_APP_ID" NOT NULL ENABLE);
  ALTER TABLE "SPIDERS_DATA"."APEX$_WS_HISTORY" MODIFY ("ROW_ID" NOT NULL ENABLE);
