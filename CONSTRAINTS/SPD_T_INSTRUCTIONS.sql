--------------------------------------------------------
--  Constraints for Table SPD_T_INSTRUCTIONS
--------------------------------------------------------

  ALTER TABLE "SPIDERS_DATA"."SPD_T_INSTRUCTIONS" ADD CONSTRAINT "SPD_T_INSTRUCTIONS_U" UNIQUE ("PWSID", "GROUPING", "VISUAL_ORDER", "INSTRUCTION")
  USING INDEX PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1
  BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "SPIDERS_DATA_TABLES"  ENABLE;
  ALTER TABLE "SPIDERS_DATA"."SPD_T_INSTRUCTIONS" ADD CONSTRAINT "SPD_T_INSTRUCTIONS_PK" PRIMARY KEY ("INSTRUCTIONID")
  USING INDEX PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1
  BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "SPIDERS_DATA_TABLES"  ENABLE;
