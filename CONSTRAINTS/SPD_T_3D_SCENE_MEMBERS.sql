--------------------------------------------------------
--  Constraints for Table SPD_T_3D_SCENE_MEMBERS
--------------------------------------------------------

  ALTER TABLE "SPIDERS_DATA"."SPD_T_3D_SCENE_MEMBERS" ADD CONSTRAINT "SPD_T_3D_SCENE_MEMBERS_PK" PRIMARY KEY ("SESSIONID")
  USING INDEX PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  TABLESPACE "SPIDERS_DATA_TABLES"  ENABLE;
