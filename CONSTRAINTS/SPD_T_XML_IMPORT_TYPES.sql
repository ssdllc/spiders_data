--------------------------------------------------------
--  Constraints for Table SPD_T_XML_IMPORT_TYPES
--------------------------------------------------------

  ALTER TABLE "SPIDERS_DATA"."SPD_T_XML_IMPORT_TYPES" ADD CONSTRAINT "SPD_T_XML_IMPORT_TYPES_PK" PRIMARY KEY ("IMPORTID")
  USING INDEX PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  TABLESPACE "SPIDERS_DATA_TABLES"  ENABLE;
