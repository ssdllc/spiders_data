--------------------------------------------------------
--  Constraints for Table SPD_T_PLAN_ACTION_NOTES
--------------------------------------------------------

  ALTER TABLE "SPIDERS_DATA"."SPD_T_PLAN_ACTION_NOTES" ADD CONSTRAINT "SPD_T_PLAN_ACTION_NOTES_PK" PRIMARY KEY ("PLANACTIONNOTEID")
  USING INDEX PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  TABLESPACE "SPIDERS_DATA_TABLES"  ENABLE;
