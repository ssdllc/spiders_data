--------------------------------------------------------
--  Ref Constraints for Table SPD_T_3D_SCENE_MEMBERS
--------------------------------------------------------

  ALTER TABLE "SPIDERS_DATA"."SPD_T_3D_SCENE_MEMBERS" ADD CONSTRAINT "SCENE_MEMBERS_FK" FOREIGN KEY ("SESSIONID")
	  REFERENCES "SPIDERS_DATA"."SPD_T_3D_SESSIONS" ("SESSIONID") ENABLE;
