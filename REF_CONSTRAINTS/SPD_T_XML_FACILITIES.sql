--------------------------------------------------------
--  Ref Constraints for Table SPD_T_XML_FACILITIES
--------------------------------------------------------

  ALTER TABLE "SPIDERS_DATA"."SPD_T_XML_FACILITIES" ADD CONSTRAINT "SPD_FK_XML_FACILITIES" FOREIGN KEY ("XMLACTIONID")
	  REFERENCES "SPIDERS_DATA"."SPD_T_XML_ACTIONS" ("XMLACTIONID") ON DELETE CASCADE ENABLE;
