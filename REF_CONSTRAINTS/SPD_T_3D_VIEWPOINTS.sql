--------------------------------------------------------
--  Ref Constraints for Table SPD_T_3D_VIEWPOINTS
--------------------------------------------------------

  ALTER TABLE "SPIDERS_DATA"."SPD_T_3D_VIEWPOINTS" ADD CONSTRAINT "SESSIONID_FK" FOREIGN KEY ("SESSIONID")
	  REFERENCES "SPIDERS_DATA"."SPD_T_3D_SESSIONS" ("SESSIONID") ENABLE;
