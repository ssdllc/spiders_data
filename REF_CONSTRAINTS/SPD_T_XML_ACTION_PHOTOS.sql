--------------------------------------------------------
--  Ref Constraints for Table SPD_T_XML_ACTION_PHOTOS
--------------------------------------------------------

  ALTER TABLE "SPIDERS_DATA"."SPD_T_XML_ACTION_PHOTOS" ADD CONSTRAINT "SPD_FK_XML_ACTION_PHOTOS" FOREIGN KEY ("XMLACTIONID")
	  REFERENCES "SPIDERS_DATA"."SPD_T_XML_ACTIONS" ("XMLACTIONID") ON DELETE CASCADE ENABLE;
