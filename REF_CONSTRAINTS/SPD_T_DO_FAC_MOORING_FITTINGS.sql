--------------------------------------------------------
--  Ref Constraints for Table SPD_T_DO_FAC_MOORING_FITTINGS
--------------------------------------------------------

  ALTER TABLE "SPIDERS_DATA"."SPD_T_DO_FAC_MOORING_FITTINGS" ADD FOREIGN KEY ("AIID")
	  REFERENCES "SPIDERS_DATA"."SPD_T_DO_FAC_ASSET_INVENTORY" ("AIID") ENABLE;
