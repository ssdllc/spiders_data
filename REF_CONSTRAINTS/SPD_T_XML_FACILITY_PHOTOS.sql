--------------------------------------------------------
--  Ref Constraints for Table SPD_T_XML_FACILITY_PHOTOS
--------------------------------------------------------

  ALTER TABLE "SPIDERS_DATA"."SPD_T_XML_FACILITY_PHOTOS" ADD CONSTRAINT "SPD_FK_XML_FACILITY_PHOTOS" FOREIGN KEY ("FACILITYID")
	  REFERENCES "SPIDERS_DATA"."SPD_T_XML_FACILITIES" ("FACILITYID") ON DELETE CASCADE ENABLE;
