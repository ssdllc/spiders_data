--------------------------------------------------------
--  Ref Constraints for Table SPD_T_SIA_DATA
--------------------------------------------------------

  ALTER TABLE "SPIDERS_DATA"."SPD_T_SIA_DATA" ADD CONSTRAINT "SPD_FK_SIASTRUCTUREID" FOREIGN KEY ("SIASTRUCTUREID")
	  REFERENCES "SPIDERS_DATA"."SPD_T_SIA_STRUCTURES" ("SIASTRUCTUREID") ON DELETE CASCADE ENABLE;
