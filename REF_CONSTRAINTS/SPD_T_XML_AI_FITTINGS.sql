--------------------------------------------------------
--  Ref Constraints for Table SPD_T_XML_AI_FITTINGS
--------------------------------------------------------

  ALTER TABLE "SPIDERS_DATA"."SPD_T_XML_AI_FITTINGS" ADD CONSTRAINT "SPD_FK_XML_AI_FITTINGS" FOREIGN KEY ("INVENTORYID")
	  REFERENCES "SPIDERS_DATA"."SPD_T_XML_ASSET_INVENTORY" ("INVENTORYID") ON DELETE CASCADE ENABLE;
