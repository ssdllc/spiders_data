--------------------------------------------------------
--  Ref Constraints for Table SPD_T_XML_DEFECT_METADATA
--------------------------------------------------------

  ALTER TABLE "SPIDERS_DATA"."SPD_T_XML_DEFECT_METADATA" ADD CONSTRAINT "SPD_FK_DEFECT_METADATA" FOREIGN KEY ("DEFECTID")
	  REFERENCES "SPIDERS_DATA"."SPD_T_XML_DEFECTS" ("DEFECTID") ON DELETE CASCADE ENABLE;
