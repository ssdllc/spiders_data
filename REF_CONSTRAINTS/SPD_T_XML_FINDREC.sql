--------------------------------------------------------
--  Ref Constraints for Table SPD_T_XML_FINDREC
--------------------------------------------------------

  ALTER TABLE "SPIDERS_DATA"."SPD_T_XML_FINDREC" ADD CONSTRAINT "SPD_FK_XML_FINDREC" FOREIGN KEY ("FACILITYID")
	  REFERENCES "SPIDERS_DATA"."SPD_T_XML_FACILITIES" ("FACILITYID") ON DELETE CASCADE ENABLE;
