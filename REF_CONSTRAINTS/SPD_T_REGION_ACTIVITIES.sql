--------------------------------------------------------
--  Ref Constraints for Table SPD_T_REGION_ACTIVITIES
--------------------------------------------------------

  ALTER TABLE "SPIDERS_DATA"."SPD_T_REGION_ACTIVITIES" ADD CONSTRAINT "FK_REGION_ID" FOREIGN KEY ("REGIONID")
	  REFERENCES "SPIDERS_DATA"."SPD_T_REGIONS" ("REGIONID") ENABLE;
