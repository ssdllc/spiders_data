--------------------------------------------------------
--  Ref Constraints for Table SPD_T_XML_ATTENDEES
--------------------------------------------------------

  ALTER TABLE "SPIDERS_DATA"."SPD_T_XML_ATTENDEES" ADD CONSTRAINT "SPD_FK_XML_ATTENDEES" FOREIGN KEY ("EXITBRIEFID")
	  REFERENCES "SPIDERS_DATA"."SPD_T_XML_EXIT_BRIEFS" ("EXITBRIEFID") ON DELETE CASCADE ENABLE;
