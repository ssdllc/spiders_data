--------------------------------------------------------
--  Ref Constraints for Table SPD_T_DO_FACILITY_CE_DC
--------------------------------------------------------

  ALTER TABLE "SPIDERS_DATA"."SPD_T_DO_FACILITY_CE_DC" ADD FOREIGN KEY ("DOFID")
	  REFERENCES "SPIDERS_DATA"."SPD_T_DO_FACILITIES" ("DOFID") ENABLE;
  ALTER TABLE "SPIDERS_DATA"."SPD_T_DO_FACILITY_CE_DC" ADD FOREIGN KEY ("DIRECTCOST_LISTID")
	  REFERENCES "SPIDERS_DATA"."SPD_T_LISTS" ("LISTID") ENABLE;
