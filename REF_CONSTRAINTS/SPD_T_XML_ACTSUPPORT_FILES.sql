--------------------------------------------------------
--  Ref Constraints for Table SPD_T_XML_ACTSUPPORT_FILES
--------------------------------------------------------

  ALTER TABLE "SPIDERS_DATA"."SPD_T_XML_ACTSUPPORT_FILES" ADD CONSTRAINT "SPD_FK_XML_ACTSUPPORT_FILES" FOREIGN KEY ("XMLACTIONID")
	  REFERENCES "SPIDERS_DATA"."SPD_T_XML_ACTIONS" ("XMLACTIONID") ON DELETE CASCADE ENABLE;
