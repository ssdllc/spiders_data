--------------------------------------------------------
--  Ref Constraints for Table SPD_T_3D_SESSIONS
--------------------------------------------------------

  ALTER TABLE "SPIDERS_DATA"."SPD_T_3D_SESSIONS" ADD CONSTRAINT "FK_SESSIONS_SCENEID" FOREIGN KEY ("SCENEID")
	  REFERENCES "SPIDERS_DATA"."SPD_T_3D_SCENES" ("SCENEID") ENABLE;
