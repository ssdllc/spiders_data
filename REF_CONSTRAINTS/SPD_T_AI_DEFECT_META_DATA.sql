--------------------------------------------------------
--  Ref Constraints for Table SPD_T_AI_DEFECT_META_DATA
--------------------------------------------------------

  ALTER TABLE "SPIDERS_DATA"."SPD_T_AI_DEFECT_META_DATA" ADD FOREIGN KEY ("AIDEFECTID")
	  REFERENCES "SPIDERS_DATA"."SPD_T_AI_DEFECTS" ("AIDEFECTID") ENABLE;
