--------------------------------------------------------
--  Ref Constraints for Table SPD_T_DO_SUPPORTING_FILES
--------------------------------------------------------

  ALTER TABLE "SPIDERS_DATA"."SPD_T_DO_SUPPORTING_FILES" ADD FOREIGN KEY ("ADOID")
	  REFERENCES "SPIDERS_DATA"."SPD_T_ACTN_DOS" ("ADOID") ENABLE;
  ALTER TABLE "SPIDERS_DATA"."SPD_T_DO_SUPPORTING_FILES" ADD FOREIGN KEY ("FILE_SECTION_LISTID")
	  REFERENCES "SPIDERS_DATA"."SPD_T_LISTS" ("LISTID") ENABLE;
  ALTER TABLE "SPIDERS_DATA"."SPD_T_DO_SUPPORTING_FILES" ADD FOREIGN KEY ("FILE_TYPE_LISTID")
	  REFERENCES "SPIDERS_DATA"."SPD_T_LISTS" ("LISTID") ENABLE;
