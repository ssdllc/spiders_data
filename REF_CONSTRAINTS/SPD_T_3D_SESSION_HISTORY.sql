--------------------------------------------------------
--  Ref Constraints for Table SPD_T_3D_SESSION_HISTORY
--------------------------------------------------------

  ALTER TABLE "SPIDERS_DATA"."SPD_T_3D_SESSION_HISTORY" ADD CONSTRAINT "FK_SESSION_HISTORY" FOREIGN KEY ("SESSIONID")
	  REFERENCES "SPIDERS_DATA"."SPD_T_3D_SESSIONS" ("SESSIONID") ENABLE;
