--------------------------------------------------------
--  Ref Constraints for Table SPD_T_XML_EXIT_BRIEFS
--------------------------------------------------------

  ALTER TABLE "SPIDERS_DATA"."SPD_T_XML_EXIT_BRIEFS" ADD CONSTRAINT "SPD_FK_XML_EXIT_BRIEFS" FOREIGN KEY ("XMLACTIONID")
	  REFERENCES "SPIDERS_DATA"."SPD_T_XML_ACTIONS" ("XMLACTIONID") ON DELETE CASCADE ENABLE;
