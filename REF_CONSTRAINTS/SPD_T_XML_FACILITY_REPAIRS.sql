--------------------------------------------------------
--  Ref Constraints for Table SPD_T_XML_FACILITY_REPAIRS
--------------------------------------------------------

  ALTER TABLE "SPIDERS_DATA"."SPD_T_XML_FACILITY_REPAIRS" ADD CONSTRAINT "SPD_FK_XML_FACILITY_REPAIRS" FOREIGN KEY ("FACILITYID")
	  REFERENCES "SPIDERS_DATA"."SPD_T_XML_FACILITIES" ("FACILITYID") ON DELETE CASCADE ENABLE;
