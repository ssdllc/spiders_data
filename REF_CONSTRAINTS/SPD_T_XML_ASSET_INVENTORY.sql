--------------------------------------------------------
--  Ref Constraints for Table SPD_T_XML_ASSET_INVENTORY
--------------------------------------------------------

  ALTER TABLE "SPIDERS_DATA"."SPD_T_XML_ASSET_INVENTORY" ADD CONSTRAINT "SPD_FK_XML_ASSET_INVENTORY" FOREIGN KEY ("LINEITEMID")
	  REFERENCES "SPIDERS_DATA"."SPD_T_XML_ASSET_SUMMARY" ("LINEITEMID") ON DELETE CASCADE ENABLE;
