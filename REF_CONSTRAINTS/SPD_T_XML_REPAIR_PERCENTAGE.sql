--------------------------------------------------------
--  Ref Constraints for Table SPD_T_XML_REPAIR_PERCENTAGE
--------------------------------------------------------

  ALTER TABLE "SPIDERS_DATA"."SPD_T_XML_REPAIR_PERCENTAGE" ADD CONSTRAINT "SPD_FK_XML_REPAIR_PERCENTAGE" FOREIGN KEY ("FACILITYID")
	  REFERENCES "SPIDERS_DATA"."SPD_T_XML_FACILITIES" ("FACILITYID") ON DELETE CASCADE ENABLE;
