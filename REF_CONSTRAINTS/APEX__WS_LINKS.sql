--------------------------------------------------------
--  Ref Constraints for Table APEX$_WS_LINKS
--------------------------------------------------------

  ALTER TABLE "SPIDERS_DATA"."APEX$_WS_LINKS" ADD CONSTRAINT "APEX$_WS_LINKS_FK" FOREIGN KEY ("ROW_ID")
	  REFERENCES "SPIDERS_DATA"."APEX$_WS_ROWS" ("ID") ON DELETE CASCADE ENABLE;
