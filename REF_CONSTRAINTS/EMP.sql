--------------------------------------------------------
--  Ref Constraints for Table EMP
--------------------------------------------------------

  ALTER TABLE "SPIDERS_DATA"."EMP" ADD FOREIGN KEY ("MGR")
	  REFERENCES "SPIDERS_DATA"."EMP" ("EMPNO") ENABLE;
  ALTER TABLE "SPIDERS_DATA"."EMP" ADD FOREIGN KEY ("DEPTNO")
	  REFERENCES "SPIDERS_DATA"."DEPT" ("DEPTNO") ENABLE;
