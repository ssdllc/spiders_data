--------------------------------------------------------
--  Ref Constraints for Table SPD_T_XML_PHOTO_ASSET_TYPES
--------------------------------------------------------

  ALTER TABLE "SPIDERS_DATA"."SPD_T_XML_PHOTO_ASSET_TYPES" ADD CONSTRAINT "SPD_FK_XML_PHOTO_ASSET_TYPES" FOREIGN KEY ("PHOTOID")
	  REFERENCES "SPIDERS_DATA"."SPD_T_XML_FACILITY_PHOTOS" ("PHOTOID") ON DELETE CASCADE ENABLE;
