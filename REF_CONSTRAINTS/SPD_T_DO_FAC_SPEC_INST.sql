--------------------------------------------------------
--  Ref Constraints for Table SPD_T_DO_FAC_SPEC_INST
--------------------------------------------------------

  ALTER TABLE "SPIDERS_DATA"."SPD_T_DO_FAC_SPEC_INST" ADD FOREIGN KEY ("DOFID")
	  REFERENCES "SPIDERS_DATA"."SPD_T_DO_FACILITIES" ("DOFID") ENABLE;
  ALTER TABLE "SPIDERS_DATA"."SPD_T_DO_FAC_SPEC_INST" ADD FOREIGN KEY ("SPEC_INSTR_LISTID")
	  REFERENCES "SPIDERS_DATA"."SPD_T_LISTS" ("LISTID") ENABLE;
