--------------------------------------------------------
--  Ref Constraints for Table SPD_T_DO_CE_DC
--------------------------------------------------------

  ALTER TABLE "SPIDERS_DATA"."SPD_T_DO_CE_DC" ADD FOREIGN KEY ("ADOID")
	  REFERENCES "SPIDERS_DATA"."SPD_T_ACTN_DOS" ("ADOID") ENABLE;
  ALTER TABLE "SPIDERS_DATA"."SPD_T_DO_CE_DC" ADD FOREIGN KEY ("DIRECTCOST_LISTID")
	  REFERENCES "SPIDERS_DATA"."SPD_T_LISTS" ("LISTID") ENABLE;
