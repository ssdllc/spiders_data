--------------------------------------------------------
--  Ref Constraints for Table APEX$_WS_FILES
--------------------------------------------------------

  ALTER TABLE "SPIDERS_DATA"."APEX$_WS_FILES" ADD CONSTRAINT "APEX$_WS_FILES_FK" FOREIGN KEY ("ROW_ID")
	  REFERENCES "SPIDERS_DATA"."APEX$_WS_ROWS" ("ID") ON DELETE CASCADE ENABLE;
