--------------------------------------------------------
--  Ref Constraints for Table SPD_T_XML_EXEC_SUMMARIES
--------------------------------------------------------

  ALTER TABLE "SPIDERS_DATA"."SPD_T_XML_EXEC_SUMMARIES" ADD CONSTRAINT "SPD_FK_XML_EXEC_SUMMARIES" FOREIGN KEY ("XMLACTIONID")
	  REFERENCES "SPIDERS_DATA"."SPD_T_XML_ACTIONS" ("XMLACTIONID") ON DELETE CASCADE ENABLE;
