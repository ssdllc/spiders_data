--------------------------------------------------------
--  Ref Constraints for Table SPD_T_XML_EXEC_TABLES
--------------------------------------------------------

  ALTER TABLE "SPIDERS_DATA"."SPD_T_XML_EXEC_TABLES" ADD CONSTRAINT "SPD_FK_XML_EXEC_TABLES" FOREIGN KEY ("FACILITYID")
	  REFERENCES "SPIDERS_DATA"."SPD_T_XML_FACILITIES" ("FACILITYID") ON DELETE CASCADE ENABLE;
