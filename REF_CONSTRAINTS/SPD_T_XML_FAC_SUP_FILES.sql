--------------------------------------------------------
--  Ref Constraints for Table SPD_T_XML_FAC_SUP_FILES
--------------------------------------------------------

  ALTER TABLE "SPIDERS_DATA"."SPD_T_XML_FAC_SUP_FILES" ADD CONSTRAINT "SPD_FK_XML_FAC_SUP_FILES" FOREIGN KEY ("FACILITYID")
	  REFERENCES "SPIDERS_DATA"."SPD_T_XML_FACILITIES" ("FACILITYID") ON DELETE CASCADE ENABLE;
