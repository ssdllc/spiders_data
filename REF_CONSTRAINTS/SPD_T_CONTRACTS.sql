--------------------------------------------------------
--  Ref Constraints for Table SPD_T_CONTRACTS
--------------------------------------------------------

  ALTER TABLE "SPIDERS_DATA"."SPD_T_CONTRACTS" ADD CONSTRAINT "SPD_C_4" FOREIGN KEY ("PRODUCTLINE_LISTID")
	  REFERENCES "SPIDERS_DATA"."SPD_T_PRODUCTLINES" ("PRODUCTLINEID") DEFERRABLE INITIALLY DEFERRED ENABLE;
