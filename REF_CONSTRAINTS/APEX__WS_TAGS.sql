--------------------------------------------------------
--  Ref Constraints for Table APEX$_WS_TAGS
--------------------------------------------------------

  ALTER TABLE "SPIDERS_DATA"."APEX$_WS_TAGS" ADD CONSTRAINT "APEX$_WS_TAGS_FK" FOREIGN KEY ("ROW_ID")
	  REFERENCES "SPIDERS_DATA"."APEX$_WS_ROWS" ("ID") ON DELETE CASCADE ENABLE;
