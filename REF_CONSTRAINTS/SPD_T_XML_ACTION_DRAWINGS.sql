--------------------------------------------------------
--  Ref Constraints for Table SPD_T_XML_ACTION_DRAWINGS
--------------------------------------------------------

  ALTER TABLE "SPIDERS_DATA"."SPD_T_XML_ACTION_DRAWINGS" ADD CONSTRAINT "SPD_FK_XML_ACTION_DRAWINGS" FOREIGN KEY ("XMLACTIONID")
	  REFERENCES "SPIDERS_DATA"."SPD_T_XML_ACTIONS" ("XMLACTIONID") ON DELETE CASCADE ENABLE;
