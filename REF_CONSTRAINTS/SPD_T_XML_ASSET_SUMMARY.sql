--------------------------------------------------------
--  Ref Constraints for Table SPD_T_XML_ASSET_SUMMARY
--------------------------------------------------------

  ALTER TABLE "SPIDERS_DATA"."SPD_T_XML_ASSET_SUMMARY" ADD CONSTRAINT "SPD_FK_XML_ASSET_SUMMARY" FOREIGN KEY ("FACILITYID")
	  REFERENCES "SPIDERS_DATA"."SPD_T_XML_FACILITIES" ("FACILITYID") ON DELETE CASCADE ENABLE;
