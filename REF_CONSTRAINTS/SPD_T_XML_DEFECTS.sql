--------------------------------------------------------
--  Ref Constraints for Table SPD_T_XML_DEFECTS
--------------------------------------------------------

  ALTER TABLE "SPIDERS_DATA"."SPD_T_XML_DEFECTS" ADD CONSTRAINT "SPD_FK_XML_DEFECTS" FOREIGN KEY ("INVENTORYID")
	  REFERENCES "SPIDERS_DATA"."SPD_T_XML_ASSET_INVENTORY" ("INVENTORYID") ON DELETE CASCADE ENABLE;
