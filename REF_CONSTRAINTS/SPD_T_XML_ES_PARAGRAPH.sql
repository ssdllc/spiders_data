--------------------------------------------------------
--  Ref Constraints for Table SPD_T_XML_ES_PARAGRAPH
--------------------------------------------------------

  ALTER TABLE "SPIDERS_DATA"."SPD_T_XML_ES_PARAGRAPH" ADD CONSTRAINT "SPD_FK_XML_ES_PARAGRAPH" FOREIGN KEY ("EXECSUMMARYID")
	  REFERENCES "SPIDERS_DATA"."SPD_T_XML_EXEC_SUMMARIES" ("EXECSUMMARYID") ON DELETE CASCADE ENABLE;
