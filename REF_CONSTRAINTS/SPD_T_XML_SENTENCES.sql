--------------------------------------------------------
--  Ref Constraints for Table SPD_T_XML_SENTENCES
--------------------------------------------------------

  ALTER TABLE "SPIDERS_DATA"."SPD_T_XML_SENTENCES" ADD CONSTRAINT "SPD_FK_XML_SENTENCES" FOREIGN KEY ("PARAGRAPHID")
	  REFERENCES "SPIDERS_DATA"."SPD_T_XML_ES_PARAGRAPH" ("PARAGRAPHID") ON DELETE CASCADE ENABLE;
