--------------------------------------------------------
--  Ref Constraints for Table SPD_T_XML_FACILITY_DRAWINGS
--------------------------------------------------------

  ALTER TABLE "SPIDERS_DATA"."SPD_T_XML_FACILITY_DRAWINGS" ADD CONSTRAINT "SPD_FK_XML_FACILITY_DRAWINGS" FOREIGN KEY ("FACILITYID")
	  REFERENCES "SPIDERS_DATA"."SPD_T_XML_FACILITIES" ("FACILITYID") ON DELETE CASCADE ENABLE;
