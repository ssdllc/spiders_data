--------------------------------------------------------
--  Ref Constraints for Table DEMO_ORDERS
--------------------------------------------------------

  ALTER TABLE "SPIDERS_DATA"."DEMO_ORDERS" ADD CONSTRAINT "DEMO_ORDERS_CUSTOMER_ID_FK" FOREIGN KEY ("CUSTOMER_ID")
	  REFERENCES "SPIDERS_DATA"."DEMO_CUSTOMERS" ("CUSTOMER_ID") ON DELETE CASCADE ENABLE;
