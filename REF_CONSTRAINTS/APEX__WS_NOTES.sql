--------------------------------------------------------
--  Ref Constraints for Table APEX$_WS_NOTES
--------------------------------------------------------

  ALTER TABLE "SPIDERS_DATA"."APEX$_WS_NOTES" ADD CONSTRAINT "APEX$_WS_NOTES_FK" FOREIGN KEY ("ROW_ID")
	  REFERENCES "SPIDERS_DATA"."APEX$_WS_ROWS" ("ID") ON DELETE CASCADE ENABLE;
