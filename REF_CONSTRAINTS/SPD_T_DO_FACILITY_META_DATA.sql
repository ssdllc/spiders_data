--------------------------------------------------------
--  Ref Constraints for Table SPD_T_DO_FACILITY_META_DATA
--------------------------------------------------------

  ALTER TABLE "SPIDERS_DATA"."SPD_T_DO_FACILITY_META_DATA" ADD FOREIGN KEY ("DOFID")
	  REFERENCES "SPIDERS_DATA"."SPD_T_DO_FACILITIES" ("DOFID") ENABLE;
