--------------------------------------------------------
--  Ref Constraints for Table SPD_T_XML_DEFECT_VALIDATION
--------------------------------------------------------

  ALTER TABLE "SPIDERS_DATA"."SPD_T_XML_DEFECT_VALIDATION" ADD CONSTRAINT "SPD_FK_DEFFECT_VALIDATION" FOREIGN KEY ("XMLACTIONID")
	  REFERENCES "SPIDERS_DATA"."SPD_T_XML_ACTIONS" ("XMLACTIONID") ON DELETE CASCADE ENABLE;
