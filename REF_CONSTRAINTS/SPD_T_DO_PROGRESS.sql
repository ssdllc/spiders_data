--------------------------------------------------------
--  Ref Constraints for Table SPD_T_DO_PROGRESS
--------------------------------------------------------

  ALTER TABLE "SPIDERS_DATA"."SPD_T_DO_PROGRESS" ADD FOREIGN KEY ("ADOID")
	  REFERENCES "SPIDERS_DATA"."SPD_T_ACTN_DOS" ("ADOID") ENABLE;
  ALTER TABLE "SPIDERS_DATA"."SPD_T_DO_PROGRESS" ADD FOREIGN KEY ("MILESTONE_LISTID")
	  REFERENCES "SPIDERS_DATA"."SPD_T_LISTS" ("LISTID") ENABLE;
