--------------------------------------------------------
--  Ref Constraints for Table SPD_T_XML_ET_METADATA
--------------------------------------------------------

  ALTER TABLE "SPIDERS_DATA"."SPD_T_XML_ET_METADATA" ADD CONSTRAINT "SPD_FK_XML_ET_METADATA" FOREIGN KEY ("EXECTABLEID")
	  REFERENCES "SPIDERS_DATA"."SPD_T_XML_EXEC_TABLES" ("EXECTABLEID") ON DELETE CASCADE ENABLE;
