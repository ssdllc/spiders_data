--------------------------------------------------------
--  DDL for View SPD_V_QC_FAC_CARD
--------------------------------------------------------

  CREATE OR REPLACE FORCE EDITIONABLE VIEW "SPIDERS_DATA"."SPD_V_QC_FAC_CARD" ("FACID", "FACILITYNAME", "UIC", "FACILITYNO", "UNIT_TITLE", "CATEGORY_CODE", "PROPERTY_NO") AS 
  select
	fac.facid,
  fac.spd_fac_name FACILITYNAME,
	fac.spd_uic UIC,
	fac.spd_fac_no FACILITYNO,
	act.spd_activity_name UNIT_TITLE,
	inf.prime_use_category_code CATEGORY_CODE,
	inf.PROPERTY_NO
from SPD_T_FACILITIES fac, SPD_T_ACTIVITIES act, SPD_MV_INF_FACILITY inf
	where fac.spd_uic = act.spd_uic(+) and
		  fac.infads_facility_id = inf.FACILITY_ID(+);
