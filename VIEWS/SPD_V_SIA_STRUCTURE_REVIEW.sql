--------------------------------------------------------
--  DDL for View SPD_V_SIA_STRUCTURE_REVIEW
--------------------------------------------------------

  CREATE OR REPLACE FORCE EDITIONABLE VIEW "SPIDERS_DATA"."SPD_V_SIA_STRUCTURE_REVIEW" ("SIAID", "SIASTRUCTUREID", "SIAQUESTIONDID", "SIAQUESTION", "SIAGROUP", "SIAQUESTIONNUMBER", "VAL", "DESCRIPTION") AS 
  select 
sia_data.siaid                 siaid,
sia_data.siastructureid        siastructureid,
sia_data.siaquestionid         siaquestiondid,
sia_question.siaquestion       siaquestion,
sia_question.siagroup          siagroup,
sia_question.siaquestionnumber siaquestionnumber,
sia_data.value                 val,
des.description                description
from 
(select description from spd_t_sia_displayvalues dv, spd_t_sia_data sia_data where dv.siaquestionid = sia_data.siaquestionid and sia_data.value = dv.code) des,
spd_t_sia_data sia_data,
spd_t_sia_questions sia_question
where 
sia_question.siaquestionid = sia_data.siaquestionid
and sia_data.siastructureid = 39
order by
sia_question.siagroup,
sia_question.siaquestionnumber
 ;
