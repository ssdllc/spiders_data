--------------------------------------------------------
--  DDL for View SPD_V_M_SIA_DATA
--------------------------------------------------------

  CREATE OR REPLACE FORCE EDITIONABLE VIEW "SPIDERS_DATA"."SPD_V_M_SIA_DATA" ("SIAID", "SIASTRUCTUREID", "ADOID", "DOFID", "SIAQUESTIONID", "VALUE", "VISUALORDER") AS 
  select 
      SIAID,SIASTRUCTUREID,ADOID,DOFID,SIAQUESTIONID,VALUE,VISUALORDER
    from 
    SPD_T_SIA_DATA
 ;
