--------------------------------------------------------
--  DDL for View SPD_V_M_FILTERS
--------------------------------------------------------

  CREATE OR REPLACE FORCE EDITIONABLE VIEW "SPIDERS_DATA"."SPD_V_M_FILTERS" ("FILTERID", "FILTERSETID", "ATTRIBUTEID", "VALUE_LIST", "RANGE_LOW", "RANGE_HIGH") AS 
  select 
      FILTERID,FILTERSETID,ATTRIBUTEID,VALUE_LIST,RANGE_LOW,RANGE_HIGH
    from 
    SPD_T_FILTERS
 ;
