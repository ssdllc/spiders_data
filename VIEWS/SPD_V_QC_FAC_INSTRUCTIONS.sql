--------------------------------------------------------
--  DDL for View SPD_V_QC_FAC_INSTRUCTIONS
--------------------------------------------------------

  CREATE OR REPLACE FORCE EDITIONABLE VIEW "SPIDERS_DATA"."SPD_V_QC_FAC_INSTRUCTIONS" ("DOFACINSTRUCTIONID", "DELIVERYORDERFACILITYID", "DELIVERYORDERID", "FACID", "INSTRUCTIONID", "INSTRUCTION", "DESCRIPTION", "GROUPING", "VISUAL_ORDER", "BASELINE_ADJUSTMENT") AS 
  select 
  do_fac_instruction.dofacinstructionid       DOFACINSTRUCTIONID,
  delivery_order_fac.deliveryorderfacilityid  DELIVERYORDERFACILITYID,
  delivery_order_fac.deliveryorderid          DELIVERYORDERID,
  delivery_order_fac.FACID                    FACID,
  instruction.INSTRUCTIONID                   INSTRUCTIONID,
  instruction.INSTRUCTION                     INSTRUCTION,
  instruction.DESCRIPTION                     DESCRIPTION,
  instruction.GROUPING                        GROUPING,
  instruction.VISUAL_ORDER                    VISUAL_ORDER,
  instruction.BASELINE_ADJUSTMENT             BASELINE_ADJUSTMENT
from 
  spd_t_delivery_order_facs     delivery_order_fac,
  spd_t_do_fac_instructions     do_fac_instruction,
  spd_t_instructions            instruction
where
  delivery_order_fac.deliveryorderfacilityid = do_fac_instruction.dofacid
  and do_fac_instruction.instructionid = instruction.instructionid
ORDER BY 
  GROUPING ASC,
  VISUAL_ORDER ASC
 ;
