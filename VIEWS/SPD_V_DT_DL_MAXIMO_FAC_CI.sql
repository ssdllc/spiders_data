--------------------------------------------------------
--  DDL for View SPD_V_DT_DL_MAXIMO_FAC_CI
--------------------------------------------------------

  CREATE OR REPLACE FORCE EDITIONABLE VIEW "SPIDERS_DATA"."SPD_V_DT_DL_MAXIMO_FAC_CI" ("SPIDERSFACILITYCIID", "FACID", "FACILITY_ID", "DELIVERYORDERID", "DELIVERYORDERFACILITYID", "CI", "CIDATE", "REPORTNUM", "LOCATION", "FACILITYNAME", "FACILITYNO", "INSPECTION_TYPE", "CREATED_BY", "CREATED_TIMESTAMP", "UPDATED_BY", "UPDATED_TIMESTAMP") AS 
  select
  spiders_facility_ci.SPIDERSFACILITYCIID              SPIDERSFACILITYCIID, 
  facility.facid                                       FACID,
  spiders_facility_ci.FACILITYID                       FACILITY_ID,
  spiders_facility_ci.DELIVERYORDERID                  DELIVERYORDERID,        
  spiders_facility_ci.DELIVERYORDERFACILITYID          DELIVERYORDERFACILITYID,        
  spiders_facility_ci.CI                               CI ,
  spiders_facility_ci.CIDATE                           CIDATE ,         
  spiders_facility_ci.REPORTNUM                        REPORTNUM, 
  spiders_facility_ci.LOCATION                         LOCATION ,          
  spiders_facility_ci.FACILITYNAME                     FACILITYNAME ,
  spiders_facility_ci.FACILITYNO                       FACILITYNO ,
  spiders_facility_ci.INSPECTION_TYPE                  INSPECTION_TYPE,
  spiders_facility_ci.CREATED_BY                       CREATED_BY, 
  spiders_facility_ci.CREATED_TIMESTAMP                CREATED_TIMESTAMP,          
  spiders_facility_ci.UPDATED_BY                       UPDATED_BY ,
  spiders_facility_ci.UPDATED_TIMESTAMP                UPDATED_TIMESTAMP
from
  spd_t_spiders_facility_ci     spiders_facility_ci,
  spd_t_facilities              facility
where
  spiders_facility_ci.facilityid = facility.infads_facility_id
 ;
