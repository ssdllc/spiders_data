--------------------------------------------------------
--  DDL for View SPD_V_QC_PWSS
--------------------------------------------------------

  CREATE OR REPLACE FORCE EDITIONABLE VIEW "SPIDERS_DATA"."SPD_V_QC_PWSS" ("PWSID", "CONTRACTID", "PWS_TYPE", "PWS_DESCRIPTION", "PRODUCTLINE_LISTID") AS 
  select 
  pws.PWSID,
  pws.CONTRACTID,
  pws.PWS_TYPE,
  pws.PWS_DESCRIPTION,
  contract.PRODUCTLINE_LISTID
from 
  SPD_T_PWSS pws,
  spd_t_contracts contract
where
  pws.contractid = contract.contractid
ORDER BY 
  PWS_DESCRIPTION DESC
 ;
