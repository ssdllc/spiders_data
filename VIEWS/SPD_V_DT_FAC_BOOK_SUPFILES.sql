--------------------------------------------------------
--  DDL for View SPD_V_DT_FAC_BOOK_SUPFILES
--------------------------------------------------------

  CREATE OR REPLACE FORCE EDITIONABLE VIEW "SPIDERS_DATA"."SPD_V_DT_FAC_BOOK_SUPFILES" ("BFILEID", "FOLDERID", "FILE_EXISTS", "DELIVERYORDERFACILITYID", "FACILITYID", "DESCRIPTION", "FILEPATH", "FILENAME", "DOWNLOAD") AS 
  SELECT 
  bfile.bfileid                                           BFILEID,
  bfile.folderid                                          FOLDERID,
  bfileexists(bfile.the_bfile)                            FILE_EXISTS,
  fac.DELIVERYORDERFACILITYID                             DELIVERYORDERFACILITYID,
  sup_file.FACILITYID                                     FACILITYID,
  sup_file.DESCRIPTION                                    DESCRIPTION, 
  f_smoke_get_file_path(sup_file.filename)                FILEPATH,
  f_smoke_get_filename(sup_file.filename)                 FILENAME,
  sup_file.SUPPORTINGFILEID                               DOWNLOAD
FROM 
  SPD_T_XML_FAC_SUP_FILES sup_file,
  SPD_T_XML_FACILITIES 	  fac,
  spd_t_bfiles            bfile
WHERE
  sup_file.FACILITYID = fac.FACILITYID and
  bfile.folderpath =  f_smoke_get_file_path(sup_file.filename) and 
  bfile.filename = f_smoke_get_filename(sup_file.filename)
 ;
