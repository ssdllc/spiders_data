--------------------------------------------------------
--  DDL for View SPD_V_INF_FAC_INSTALLATIONS
--------------------------------------------------------

  CREATE OR REPLACE FORCE EDITIONABLE VIEW "SPIDERS_DATA"."SPD_V_INF_FAC_INSTALLATIONS" ("FACILITY_ID", "INSTALLATION", "INSTTYPE") AS 
  SELECT 
     fac.facility_id
    ,special_area_name installation
    ,'special area' instType
FROM spd_mv_inf_facility fac
    ,spd_mv_inf_special_area sa
WHERE fac.special_area_code=sa.special_area_code
  and fac.activity_uic=sa.uic
  and fac.special_area_code is not null
UNION
SELECT 
     fac.facility_id
    ,act.unit_title installation
    ,'activity' instType
FROM spd_mv_inf_facility fac
    ,spd_mv_inf_activity act
WHERE fac.activity_uic=act.uic
  and fac.special_area_code is null
 
 ;
