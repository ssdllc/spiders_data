--------------------------------------------------------
--  DDL for View SPD_V_QC_INSTRUCTION_GROUPING
--------------------------------------------------------

  CREATE OR REPLACE FORCE EDITIONABLE VIEW "SPIDERS_DATA"."SPD_V_QC_INSTRUCTION_GROUPING" ("INSTRUCTIONID", "GROUPING", "VISUAL_ORDER", "PWSID") AS 
  select 
  instruction.INSTRUCTIONID   INSTRUCTIONID,
  instruction.GROUPING        GROUPING,
  instruction.VISUAL_ORDER    VISUAL_ORDER,
  pws.PWSID                   PWSID
from 
  spd_t_instructions instruction,
  spd_t_pwss         pws
where
  pws.pwsid = instruction.pwsid
ORDER BY 
  GROUPING ASC
 ;
