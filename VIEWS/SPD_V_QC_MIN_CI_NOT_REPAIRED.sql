--------------------------------------------------------
--  DDL for View SPD_V_QC_MIN_CI_NOT_REPAIRED
--------------------------------------------------------

  CREATE OR REPLACE FORCE EDITIONABLE VIEW "SPIDERS_DATA"."SPD_V_QC_MIN_CI_NOT_REPAIRED" ("MINCURRENTCI", "MINFIVEYEARCIIFNOTREPAIRED", "MINTENYEARCIIFNOTREPAIRED", "UNIFORMATIICODE") AS 
  select   
  min(xml_asset_summary.CURRENTCI)                MINCURRENTCI,
  min(xml_asset_summary.FIVEYEARCIIFNOTREPAIRED)  MINFIVEYEARCIIFNOTREPAIRED,
  min(xml_asset_summary.TENYEARCIIFNOTREPAIRED)   MINTENYEARCIIFNOTREPAIRED,
  xml_asset_summary.uniformatiicode               UNIFORMATIICODE
from
  spd_t_xml_asset_summary xml_asset_summary
where
  xml_asset_summary.CURRENTCI is not null
group by
  UNIFORMATIICODE
 ;
