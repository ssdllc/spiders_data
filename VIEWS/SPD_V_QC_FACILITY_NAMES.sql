--------------------------------------------------------
--  DDL for View SPD_V_QC_FACILITY_NAMES
--------------------------------------------------------

  CREATE OR REPLACE FORCE EDITIONABLE VIEW "SPIDERS_DATA"."SPD_V_QC_FACILITY_NAMES" ("DISPLAY_VALUE") AS 
  select distinct(FACILITY_NAME) DISPLAY_VALUE from SPD_MV_DATA_ANALYSIS where rownum < 200 order by lower(FACILITY_NAME)
 ;
