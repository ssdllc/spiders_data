--------------------------------------------------------
--  DDL for View SPD_V_QC_CONTRACTOR_INFO
--------------------------------------------------------

  CREATE OR REPLACE FORCE EDITIONABLE VIEW "SPIDERS_DATA"."SPD_V_QC_CONTRACTOR_INFO" ("CONTRACTCONTRACTORID", "CONTRACTORID", "CONTRACTID", "CONTRACTOR_NAME", "CITY", "STATE", "PRIME", "OVERHEAD", "FRINGE", "GENERAL_AND_ADMINISTRATIVE") AS 
  SELECT 
    contract_contractor.contractcontractorid,
    contractor.contractorid,
    contract_contractor.contractid,
    contractor.contractor_name,
    contractor.city,
    contractor.state,
    contract_contractor.contract_role prime,
    contract_contractor.overhead,
    contract_contractor.fringe,
    contract_contractor.general_and_administrative
FROM 
    spd_t_contractors contractor,
    spd_t_contract_contractors contract_contractor
WHERE
    contractor.contractorid = contract_contractor.contractorid
 ;
