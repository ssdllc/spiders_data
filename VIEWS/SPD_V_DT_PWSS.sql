--------------------------------------------------------
--  DDL for View SPD_V_DT_PWSS
--------------------------------------------------------

  CREATE OR REPLACE FORCE EDITIONABLE VIEW "SPIDERS_DATA"."SPD_V_DT_PWSS" ("PWSID", "CONTRACTID", "PWS_TYPE", "PWS_DESCRIPTION") AS 
  select 
       PWSID
      ,CONTRACTID
      ,PWS_TYPE
      ,PWS_DESCRIPTION
    from 
      SPD_T_PWSS
 ;
