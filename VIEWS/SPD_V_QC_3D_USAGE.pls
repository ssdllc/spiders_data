CREATE OR REPLACE VIEW SPIDERS_DATA.SPD_V_QC_3D_USAGE AS
select created_by as SPIDERS_USERS
  ,TO_CHAR(max(created_timestamp),'MM/DD/YYYY') as last_session_join
  ,sum(sessions_created) as sessions_created 
  ,sum(sessions_joined) as sessions_joined
  ,sum(snapshots_created) as snapshots_created
  ,sum(models_dropped) as models_dropped
  ,sum(custom_objects_dropped) as custom_objects_dropped
from (
select null created_timestamp, created_by, count(sessionid) sessions_created, 0 sessions_joined,0 snapshots_created, 0 models_dropped, 0 custom_objects_dropped from spiders_data.spd_t_3d_sessions group by created_by
union
select max(created_timestamp), created_by, 0 sessions_created, count(sessionmemberid) sessions_joined,0 snapshots_created, 0 models_dropped, 0 custom_objects_dropped  from  spiders_data.spd_t_3d_session_members group by created_by
union
select null created_timestamp, created_by, 0 sessions_created, 0 sessions_joined,count(whiteboardid) snapshots_created, 0 models_dropped, 0 custom_objects_dropped from  spiders_data.spd_t_3d_whiteboards group by created_by
union
select null created_timestamp, created_by, 0 sessions_created, 0 sessions_joined,0 snapshots_created, count(modellogid) models_dropped, 0 custom_objects_dropped  from  spiders_data.spd_t_3d_model_logs group by created_by
union
select null created_timestamp, created_by, 0 sessions_created, 0 sessions_joined,0 snapshots_created, 0 models_dropped, count(customobjectlogid) custom_objects_dropped from  spiders_data.spd_t_3d_custom_object_logs group by created_by
)
group by created_by
order by max(created_timestamp) DESC;