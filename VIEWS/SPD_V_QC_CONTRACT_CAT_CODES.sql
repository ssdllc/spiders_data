--------------------------------------------------------
--  DDL for View SPD_V_QC_CONTRACT_CAT_CODES
--------------------------------------------------------

  CREATE OR REPLACE FORCE EDITIONABLE VIEW "SPIDERS_DATA"."SPD_V_QC_CONTRACT_CAT_CODES" ("CATEGORYCODEID", "CATEGORY_CODE", "CONTRACTCATCODEID", "CONTRACTID", "MEDIUM_DESC", "SHORT_DESC", "DESCRIPTION") AS 
  select 
  contract_cat_code.CATEGORYCODEID    CATEGORYCODEID,
  category_code.category_code         CATEGORY_CODE,
  contract_cat_code.contractcatcodeid CONTRACTCATCODEID,
  contract_cat_code.CONTRACTID        CONTRACTID,
  category_code.medium_desc           MEDIUM_DESC,
  category_code.short_desc            SHORT_DESC,
  category_code.category_code || ' - ' || category_code.short_desc DESCRIPTION
from 
  spd_t_contract_cat_codes  contract_cat_code,
  spd_t_category_codes      category_code
where 
  contract_cat_code.categorycodeid = category_code.categorycodeid
order by
  category_code.category_code asc
 ;
