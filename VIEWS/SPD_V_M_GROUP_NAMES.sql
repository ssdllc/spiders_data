--------------------------------------------------------
--  DDL for View SPD_V_M_GROUP_NAMES
--------------------------------------------------------

  CREATE OR REPLACE FORCE EDITIONABLE VIEW "SPIDERS_DATA"."SPD_V_M_GROUP_NAMES" ("GROUPNAMEID", "DISPLAY_VALUE", "RETURN_VALUE", "LIST_NAME", "VISUAL_ORDER", "NOTES") AS 
  select 
      GROUPNAMEID,DISPLAY_VALUE,RETURN_VALUE,LIST_NAME,VISUAL_ORDER,NOTES
    from 
    SPD_T_GROUP_NAMES
 ;
