--------------------------------------------------------
--  DDL for View SPD_V_M_PLAN_ACTION_NOTES
--------------------------------------------------------

  CREATE OR REPLACE FORCE EDITIONABLE VIEW "SPIDERS_DATA"."SPD_V_M_PLAN_ACTION_NOTES" ("PLANACTIONNOTEID", "PLANACTIONID", "NOTE") AS 
  select 
      PLANACTIONNOTEID,PLANACTIONID,NOTE
    from 
    SPD_T_PLAN_ACTION_NOTES;
