--------------------------------------------------------
--  DDL for View SPD_V_QC_XML_IMPORT_CE
--------------------------------------------------------

  CREATE OR REPLACE FORCE EDITIONABLE VIEW "SPIDERS_DATA"."SPD_V_QC_XML_IMPORT_CE" ("ADOID", "NFAID", "FACILITYNAME", "FACILITYNO", "UNIFORMATIICODE", "ASSETTYPE", "REPAIRDESCRIPTION", "QUANTITY", "UNITS", "MATERIALUNITCOST", "MATERIAL_COST", "LABORUNITCOST", "LABOR_COST", "TOTALUNITCOST", "TOTAL_COST") AS 
  select
 actions.adoid, facs.NFAID, facs.facilityname, facs.facilityno,repairs.uniformatiicode,repairs.assettype,repairs.repairdescription,repairs.quantity, repairs.units, repairs.materialunitcost , (to_number(repairs.quantity) * to_number(repairs.materialunitcost)) material_cost, repairs.laborunitcost,(to_number(repairs.quantity) * to_number(repairs.laborunitcost)) labor_cost, (to_number(repairs.materialunitcost) + to_number(repairs.laborunitcost) ) totalunitcost, (to_number(repairs.quantity) * (to_number(repairs.materialunitcost) + to_number(repairs.laborunitcost))) total_cost
from spd_t_xml_facility_repairs repairs
  , spd_t_xml_facilities facs
  , SPD_T_XML_ACTIONS actions
where
  repairs.facilityid = facs.facilityid
  and facs.xmlactionid = actions.xmlactionid
 ;
