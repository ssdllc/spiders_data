--------------------------------------------------------
--  DDL for View SPD_V_QC_AVAILABLE_DOS
--------------------------------------------------------

  CREATE OR REPLACE FORCE EDITIONABLE VIEW "SPIDERS_DATA"."SPD_V_QC_AVAILABLE_DOS" ("DELIVERYORDERID", "PRODUCTLINEID", "FY", "DELIVERY_ORDER_NAME", "ONSITE_START_DATE", "ONSITE_END_DATE", "CONTRACT_NAME", "FACS") AS 
  SELECT distinct
	--do.onsite_start_date,
	do.deliveryorderid,
	c.productline_listid PRODUCTLINEID,
  EXTRACT(YEAR FROM ADD_MONTHS(do.onsite_start_date,3)) FY,
	do.delivery_order_name,
	do.onsite_start_date,
	do.onsite_end_date,
	c.contract_name,
	(select count(dof.deliveryorderfacilityid)
	 from spd_t_delivery_order_facs dof
	 where dof.deliveryorderid = do.deliveryorderid) facs
FROM
	SPD_T_DELIVERY_ORDERS do,
	SPD_T_PWSS pws,
	SPD_T_CONTRACTS c,
	SPD_T_PLAN_DELIVERY_ORDERS pdo
WHERE
	do.PWSID = pws.PWSID and
	pws.CONTRACTID = c.CONTRACTID and
	do.deliveryorderid = pdo.deliveryorderid(+)
	and pdo.deliveryorderid is null;
