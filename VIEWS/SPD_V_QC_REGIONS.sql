--------------------------------------------------------
--  DDL for View SPD_V_QC_REGIONS
--------------------------------------------------------

  CREATE OR REPLACE FORCE EDITIONABLE VIEW "SPIDERS_DATA"."SPD_V_QC_REGIONS" ("REGIONID", "SHORTNAME") AS 
  SELECT 
    "REGIONID","SHORTNAME"
FROM 
    SPD_T_REGIONS
ORDER BY
    SHORTNAME ASC
 ;
