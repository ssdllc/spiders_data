--------------------------------------------------------
--  DDL for View SPD_V_M_DT_DOWNLOADS
--------------------------------------------------------

  CREATE OR REPLACE FORCE EDITIONABLE VIEW "SPIDERS_DATA"."SPD_V_M_DT_DOWNLOADS" ("DTDOWNLOADID", "DATATABLENAME", "AODATA", "CUSTOMFILTER", "THE_BFILE") AS 
  select 
      DTDOWNLOADID,DATATABLENAME,AODATA,CUSTOMFILTER,THE_BFILE
    from 
    SPD_T_DT_DOWNLOADS
 ;
