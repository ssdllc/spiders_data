--------------------------------------------------------
--  DDL for View SPD_V_QC_DO_FACILITIES
--------------------------------------------------------

  CREATE OR REPLACE FORCE EDITIONABLE VIEW "SPIDERS_DATA"."SPD_V_QC_DO_FACILITIES" ("DELIVERYORDERFACILITYID", "DELIVERYORDERID", "FACID", "NFAID", "SPIDERS_FACILITY_NAME", "SPIDERS_FACILITY_NUMBER", "FACILITY_NAME", "FACILITY_NUMBER", "ACTIVITY", "UIC", "CATEGORY_CODE", "STATE") AS 
  select 
  delivery_order_fac.DELIVERYORDERFACILITYID    DELIVERYORDERFACILITYID,
  delivery_order_fac.DELIVERYORDERID            DELIVERYORDERID,
  delivery_order_fac.FACID                      FACID,
  facility.facility_id                          NFAID,
  facility.SPIDERS_FACNAME                      SPIDERS_FACILITY_NAME,
  facility.SPIDERS_FACNO                        SPIDERS_FACILITY_NUMBER,
  facility.FACILITY_NAME                        FACILITY_NAME,
  facility.FACILITY_NO                          FACILITY_NUMBER,
  facility.UNIT_TITLE                           ACTIVITY,
  facility.ACTIVITY_UIC                         UIC,
  facility.PRIME_USE_CATEGORY_CODE              CATEGORY_CODE,
  facility.STATE_ABBR                           STATE 
from 
  spd_t_delivery_order_facs delivery_order_fac,
  spd_v_dt_facilities facility
where
  delivery_order_fac.facid = facility.facid
ORDER BY 
  FACILITY_NAME ASC
 ;
