--------------------------------------------------------
--  DDL for View SPD_V_QC_INSTRUCTION_BC
--------------------------------------------------------

  CREATE OR REPLACE FORCE EDITIONABLE VIEW "SPIDERS_DATA"."SPD_V_QC_INSTRUCTION_BC" ("INSTRUCTIONID", "PWSID", "CONTRACTID") AS 
  select 
  instruction.INSTRUCTIONID INSTRUCTIONID,
  pws.PWSID                 PWSID,
  pws.CONTRACTID            CONTRACTID 
from 
  spd_t_instructions  instruction,
  spd_t_pwss          pws
where
  instruction.pwsid = pws.pwsid
ORDER BY 
  INSTRUCTIONID ASC
 ;
