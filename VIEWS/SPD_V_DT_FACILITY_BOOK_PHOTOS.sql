--------------------------------------------------------
--  DDL for View SPD_V_DT_FACILITY_BOOK_PHOTOS
--------------------------------------------------------

  CREATE OR REPLACE FORCE EDITIONABLE VIEW "SPIDERS_DATA"."SPD_V_DT_FACILITY_BOOK_PHOTOS" ("BFILEID", "FOLDERID", "FILE_EXISTS", "DELIVERYORDERFACILITYID", "FACILITYID", "PHOTOTYPE", "CAPTION", "FILEPATH", "DOWNLOAD", "FILENAME") AS 
  SELECT 
  bfile.bfileid                                           BFILEID,
  bfile.folderid                                          FOLDERID,
  bfileexists(bfile.the_bfile)                            FILE_EXISTS,
  fac.DELIVERYORDERFACILITYID                             DELIVERYORDERFACILITYID,
  photo.FACILITYID                                        FACILITYID,
  photo.PHOTOTYPE                                         PHOTOTYPE, 
  photo.CAPTION                                           CAPTION, 
  photo.IMAGEFILENAME                                     FILEPATH, 
  photo.PHOTOID                                           DOWNLOAD,
  f_smoke_get_filename(photo.imagefilename)               FILENAME
FROM 
  SPD_T_XML_FACILITY_PHOTOS photo,
  SPD_T_XML_FACILITIES fac,
  spd_t_bfiles         bfile
WHERE
  photo.FACILITYID = fac.FACILITYID and
  bfile.folderpath =  f_smoke_get_file_path(photo.imagefilename) and 
  bfile.filename = f_smoke_get_filename(photo.imagefilename)
 ;
