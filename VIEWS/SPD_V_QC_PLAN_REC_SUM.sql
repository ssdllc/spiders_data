--------------------------------------------------------
--  DDL for View SPD_V_QC_PLAN_REC_SUM
--------------------------------------------------------

  CREATE OR REPLACE FORCE EDITIONABLE VIEW "SPIDERS_DATA"."SPD_V_QC_PLAN_REC_SUM" ("AMOUNT_TYPE", "REC_INHOUSE", "REC_KTR", "EST_INHOUSE", "EST_KTR", "PLANID") AS 
  select 'TOTAL' 				AMOUNT_TYPE,
	sum(pa.REC_INHOUSE_AMT) REC_INHOUSE,
	sum(pa.REC_KTR_AMT) 	REC_KTR,
	sum(a.INHOUSE_AMT)	 	EST_INHOUSE,
	sum(a.KTR_AMT)		 	EST_KTR,
	pa.PLANID
from SPD_T_PLAN_ACTIONS pa,
	 SPD_T_ACTIONS a
where pa.ACTIONID = a.ACTIONID
group by pa.PLANID
UNION
select 'Q'||a.QTR 			AMOUNT_TYPE,
	sum(pa.REC_INHOUSE_AMT) REC_INHOUSE,
	sum(pa.REC_KTR_AMT) 	REC_KTR,
	sum(a.INHOUSE_AMT)	 	EST_INHOUSE,
	sum(a.KTR_AMT)		 	EST_KTR,
	pa.PLANID
from SPD_T_PLAN_ACTIONS pa,
	 SPD_T_ACTIONS a
where pa.ACTIONID = a.ACTIONID
group by pa.PLANID, a.QTR;
