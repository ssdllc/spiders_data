--------------------------------------------------------
--  DDL for View SPD_V_M_REGIONS
--------------------------------------------------------

  CREATE OR REPLACE FORCE EDITIONABLE VIEW "SPIDERS_DATA"."SPD_V_M_REGIONS" ("REGIONID", "SHORTNAME", "DESCRIPTION") AS 
  select 
      REGIONID,SHORTNAME,DESCRIPTION
    from 
    SPD_T_REGIONS
 ;
