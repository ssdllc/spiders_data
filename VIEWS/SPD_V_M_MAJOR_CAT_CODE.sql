--------------------------------------------------------
--  DDL for View SPD_V_M_MAJOR_CAT_CODE
--------------------------------------------------------

  CREATE OR REPLACE FORCE EDITIONABLE VIEW "SPIDERS_DATA"."SPD_V_M_MAJOR_CAT_CODE" ("MAJORCATCODEID", "MAJOR_CAT_CODE", "INVESTMENT_CODE", "SHORT_DESC", "MEDIUM_DESC", "LONG_DESC") AS 
  select 
      MAJORCATCODEID,MAJOR_CAT_CODE,INVESTMENT_CODE,SHORT_DESC,MEDIUM_DESC,LONG_DESC
    from 
    SPD_T_MAJOR_CAT_CODE
 ;
