--------------------------------------------------------
--  DDL for View SPD_V_M_DATATABLE_COLUMNS
--------------------------------------------------------

  CREATE OR REPLACE FORCE EDITIONABLE VIEW "SPIDERS_DATA"."SPD_V_M_DATATABLE_COLUMNS" ("COLUMNID", "FRONTEND_DATA", "TITLE", "DESCRIPTION", "TARGET_NAME") AS 
  select 
      COLUMNID,FRONTEND_DATA,TITLE,DESCRIPTION,TARGET_NAME
    from 
    SPD_T_DATATABLE_COLUMNS
 ;
