--------------------------------------------------------
--  DDL for View SPD_V_QC_DATATABLE_SETTINGS
--------------------------------------------------------

  CREATE OR REPLACE FORCE EDITIONABLE VIEW "SPIDERS_DATA"."SPD_V_QC_DATATABLE_SETTINGS" ("SETTINGSID", "DATATABLENAME", "COLUMNID", "CREATED_BY", "CREATED_TIMESTAMP", "UPDATED_BY", "UPDATED_TIMESTAMP") AS 
  select 
  "SETTINGSID","DATATABLENAME","COLUMNID","CREATED_BY","CREATED_TIMESTAMP","UPDATED_BY","UPDATED_TIMESTAMP" 
from 
  SPIDERS_DATA.SPD_T_DATATABLE_SETTINGS 
WHERE 
  CREATED_BY = nvl(v('APP_USER'),'NONE')
ORDER BY 
  SETTINGSID DESC
 ;
