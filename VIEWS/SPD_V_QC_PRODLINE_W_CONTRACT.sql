--------------------------------------------------------
--  DDL for View SPD_V_QC_PRODLINE_W_CONTRACT
--------------------------------------------------------

  CREATE OR REPLACE FORCE EDITIONABLE VIEW "SPIDERS_DATA"."SPD_V_QC_PRODLINE_W_CONTRACT" ("PRODUCTLINE_DISPLAY", "PRODUCTLINEID", "CONTRACTID") AS 
  select 
    productline.productline_display,
    productline.productlineid,
    contract.contractid
from
    spd_t_productlines productline,
    spd_t_contracts contract
where
    productline.productlineid = contract.productline_listid
 ;
