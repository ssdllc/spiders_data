--------------------------------------------------------
--  DDL for View SPD_V_DT_PLAN_DOS
--------------------------------------------------------

  CREATE OR REPLACE FORCE EDITIONABLE VIEW "SPIDERS_DATA"."SPD_V_DT_PLAN_DOS" ("PLANDELIVERYORDERID", "PLANACTIONID", "ACTIONID", "DELIVERY_ORDER_NAME", "ONSITE_START_DATE", "ONSITE_END_DATE", "CONTRACT_NAME", "FACS") AS 
  SELECT distinct
	pdo.plandeliveryorderid,
	pdo.planactionid,
  pa.actionid,
	do.delivery_order_name,
	TO_CHAR(do.onsite_start_date, 'MON DD, YYYY') onsite_start_date,
  TO_CHAR(do.onsite_end_date, 'MON DD, YYYY') onsite_end_date,
	--do.onsite_start_date,
	--do.onsite_end_date,
	c.contract_name,
	(select count(dof.deliveryorderfacilityid)
	 from spd_t_delivery_order_facs dof
	 where dof.deliveryorderid = pdo.deliveryorderid) facs
FROM
	SPD_T_PLAN_DELIVERY_ORDERS pdo,
	SPD_T_DELIVERY_ORDERS do,
	SPD_T_PWSS pws,
	SPD_T_CONTRACTS c,
  SPD_T_PLAN_ACTIONS pa
WHERE
	pdo.DELIVERYORDERID = do.DELIVERYORDERID and
	do.PWSID = pws.PWSID and
	pws.CONTRACTID = c.CONTRACTID and
  pdo.PLANACTIONID = pa.PLANACTIONID;
