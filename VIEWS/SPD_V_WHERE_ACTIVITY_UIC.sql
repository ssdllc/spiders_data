--------------------------------------------------------
--  DDL for View SPD_V_WHERE_ACTIVITY_UIC
--------------------------------------------------------

  CREATE OR REPLACE FORCE EDITIONABLE VIEW "SPIDERS_DATA"."SPD_V_WHERE_ACTIVITY_UIC" ("FACILITY_ID") AS 
  select
  facility_id
from
  (select 
    facility_id,
    ACTIVITY_UIC,
    (select pkg_setter.get_filtersetid() filtersetid from dual where rownum > 0) filtersetid_val,
    (select count(*) from  spd_v_qc_filter_sets where filtersetid = (select pkg_setter.get_filtersetid() from dual where rownum > 0) and attribute_name = 'ACTIVITY_UIC') count_filter_set
  from 
    spd_mv_data_analysis data_analysis
  ) test_inner
where 
  (
    test_inner.filtersetid_val=0  
      OR 
    (
      count_filter_set = 0
        OR
      ACTIVITY_UIC in 
        (select 
            value_list 
          from 
            spd_v_qc_filter_sets filter_set,
            (select pkg_setter.get_filtersetid() filtersetid from dual where rownum > 0) vals
          where 
            filter_set.filtersetid = vals.filtersetid 
            and attribute_name = 'ACTIVITY_UIC'
        ) 
    )
  )
 ;
