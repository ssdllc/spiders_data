--------------------------------------------------------
--  DDL for View SPD_V_M_CONTRACTORS
--------------------------------------------------------

  CREATE OR REPLACE FORCE EDITIONABLE VIEW "SPIDERS_DATA"."SPD_V_M_CONTRACTORS" ("CONTRACTORID", "CONTRACTOR_NAME", "ADDRESS_1", "ADDRESS_2", "CITY", "STATE", "ZIP", "PHONE", "POC_NAME", "POC_EMAIL") AS 
  select 
      CONTRACTORID,CONTRACTOR_NAME,ADDRESS_1,ADDRESS_2,CITY,STATE,ZIP,PHONE,POC_NAME,POC_EMAIL
    from 
    SPD_T_CONTRACTORS
 ;
