--------------------------------------------------------
--  DDL for View SPD_V_M_3D_VIEWPOINTS
--------------------------------------------------------

  CREATE OR REPLACE FORCE EDITIONABLE VIEW "SPIDERS_DATA"."SPD_V_M_3D_VIEWPOINTS" ("VIEWPOINTID", "SESSIONID", "DESCRIPTION", "TRANSLATION", "ROTATION", "BASE64_IMAGE") AS 
  select 
      VIEWPOINTID,SESSIONID,DESCRIPTION,TRANSLATION,ROTATION,BASE64_IMAGE
    from 
    SPD_T_3D_VIEWPOINTS
 ;
