--------------------------------------------------------
--  DDL for View SPD_V_QC_SESSION_INFO
--------------------------------------------------------

  CREATE OR REPLACE FORCE EDITIONABLE VIEW "SPIDERS_DATA"."SPD_V_QC_SESSION_INFO" ("SESSIONID", "SESSION_KEY", "SHORT_DESCRIPTION", "OWNER_EDIT_ONLY", "LOCKED", "LOCKED_DATE", "CREATED_BY", "CREATED_TIMESTAMP", "UPDATED_BY", "UPDATED_TIMESTAMP", "DESCRIPTION", "NORTHING", "EASTING", "UTM_ZONE", "STATE", "LOCATION", "ACTIVITY_UIC", "UNIT_TITLE", "VERSION", "FILE_PATH", "SCALE", "WIDTH", "HEIGHT", "IMAGERY_SOURCE", "IMAGERY_DATE", "IMAGERY_CONFIDENCE", "TOPOGRAPHY_SOURCE", "TOPOGRAPHY_DATE", "TOPOGRAPHY_CONFIDENCE", "BUILDING_MODELS_SOURCE", "WATERLINE_SOURCE", "WATERLINE_DATE", "WATERLINE_CONFIDENCE") AS 
  SELECT 
    SESSIONS.SESSIONID,
    SESSIONS.SESSION_KEY,
    SESSIONS.SHORT_DESCRIPTION,
    SESSIONS.OWNER_EDIT_ONLY,
    SESSIONS.LOCKED,
    SESSIONS.LOCKED_DATE,
    SESSIONS.CREATED_BY,
    SESSIONS.CREATED_TIMESTAMP,
    SESSIONS.UPDATED_BY,
    SESSIONS.UPDATED_TIMESTAMP,
    SCENE.DESCRIPTION,
    SCENE.NORTHING,
    SCENE.EASTING,
    SCENE.UTM_ZONE,
    SCENE.STATE,
    SCENE.LOCATION,
    SCENE.ACTIVITY_UIC,
    SCENE.UNIT_TITLE,
    SCENE.VERSION,
    SCENE.FILE_PATH,
    SCENE.SCALE,
    SCENE.WIDTH,
    SCENE.HEIGHT,
    SCENE.IMAGERY_SOURCE,
    SCENE.IMAGERY_DATE,
    SCENE.IMAGERY_CONFIDENCE,
    SCENE.TOPOGRAPHY_SOURCE,
    SCENE.TOPOGRAPHY_DATE,
    SCENE.TOPOGRAPHY_CONFIDENCE,
    SCENE.BUILDING_MODELS_SOURCE,
    SCENE.WATERLINE_SOURCE,
    SCENE.WATERLINE_DATE,
    SCENE.WATERLINE_CONFIDENCE
FROM
    SPD_T_3D_SESSIONS SESSIONS,
    SPD_T_3D_SCENES SCENE
WHERE
    SESSIONS.SCENEID = SCENE.SCENEID
 ;
