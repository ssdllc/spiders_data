--------------------------------------------------------
--  DDL for View VIEW1
--------------------------------------------------------

  CREATE OR REPLACE FORCE EDITIONABLE VIEW "SPIDERS_DATA"."VIEW1" ("CONTRACTID", "CONTRACT_NAME", "CONTRACT_NUMBER", "BASE_START_DATE", "BASE_END_DATE", "OPTION1_START_DATE", "OPTION1_END_DATE", "OPTION2_START_DATE", "OPTION2_END_DATE", "OPTION3_START_DATE", "OPTION3_END_DATE", "OPTION4_START_DATE", "OPTION4_END_DATE") AS 
  SELECT 
    contract.contractid,
    contract.contract_name,
    contract.contract_number,
    contract.base_start_date,
    contract.base_end_date,
    contract.option1_start_date,
    contract.option1_end_date,
    contract.option2_start_date,
    contract.option2_end_date,
    contract.option3_start_date,
    contract.option3_end_date,
    contract.option4_start_date,
    contract.option4_end_date
FROM 
    spd_t_contracts contract
 ;
