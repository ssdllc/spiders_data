--------------------------------------------------------
--  DDL for View SPD_V_QC_PRODUCTLINES
--------------------------------------------------------

  CREATE OR REPLACE FORCE EDITIONABLE VIEW "SPIDERS_DATA"."SPD_V_QC_PRODUCTLINES" ("PRODUCTLINE_LISTID", "PRODUCTLINE_DISPLAY") AS 
  select PRODUCTLINEID PRODUCTLINE_LISTID, PRODUCTLINE_DISPLAY from SPD_T_PRODUCTLINES
 ;
