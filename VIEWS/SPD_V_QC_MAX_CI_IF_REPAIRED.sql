--------------------------------------------------------
--  DDL for View SPD_V_QC_MAX_CI_IF_REPAIRED
--------------------------------------------------------

  CREATE OR REPLACE FORCE EDITIONABLE VIEW "SPIDERS_DATA"."SPD_V_QC_MAX_CI_IF_REPAIRED" ("MAXCURRENTCI", "MAXFIVEYEARCIIFREPAIRED", "MAXTENYEARCIIFREPAIRED", "UNIFORMATIICODE") AS 
  select
  max(xml_asset_summary.CURRENTCI)              MAXCURRENTCI,
  max(xml_asset_summary.FIVEYEARCIIFREPAIRED)   MAXFIVEYEARCIIFREPAIRED,
  max(xml_asset_summary.TENYEARCIIFREPAIRED)    MAXTENYEARCIIFREPAIRED,
  xml_asset_summary.uniformatiicode             UNIFORMATIICODE
FROM
  spd_t_xml_asset_summary xml_asset_summary
where
  xml_asset_summary.CURRENTCI is not null
group by
  xml_asset_summary.uniformatiicode
 ;
