--------------------------------------------------------
--  DDL for View SPD_V_QC_OVR_ENG_RATINGS
--------------------------------------------------------

  CREATE OR REPLACE FORCE EDITIONABLE VIEW "SPIDERS_DATA"."SPD_V_QC_OVR_ENG_RATINGS" ("DISPLAY_VALUE") AS 
  select distinct(OVR_ENG_ASSESSMENT_RATING) DISPLAY_VALUE from SPD_MV_DATA_ANALYSIS where rownum < 200 order by lower(OVR_ENG_ASSESSMENT_RATING)
 ;
