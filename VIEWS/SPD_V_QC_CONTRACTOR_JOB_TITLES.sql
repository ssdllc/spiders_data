--------------------------------------------------------
--  DDL for View SPD_V_QC_CONTRACTOR_JOB_TITLES
--------------------------------------------------------

  CREATE OR REPLACE FORCE EDITIONABLE VIEW "SPIDERS_DATA"."SPD_V_QC_CONTRACTOR_JOB_TITLES" ("JOBTITLEID", "JOB_TITLE", "JOB_TITLE_DESCRIPTION", "VISUAL_ORDER", "BASE_RATE", "OPTION_1_RATE", "OPTION_2_RATE", "OPTION_3_RATE", "OPTION_4_RATE") AS 
  SELECT 
    job_title.jobtitleid,
    job_title.job_title,
    job_title.job_title_description,
    job_title.visual_order,
    job_title.base_rate,
    job_title.option_1_rate,
    job_title.option_2_rate,
    job_title.option_3_rate,
    job_title.option_4_rate
FROM 
    spd_t_job_titles job_title
 ;
