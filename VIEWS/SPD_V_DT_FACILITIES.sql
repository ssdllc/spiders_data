--------------------------------------------------------
--  DDL for View SPD_V_DT_FACILITIES
--------------------------------------------------------

  CREATE OR REPLACE FORCE EDITIONABLE VIEW "SPIDERS_DATA"."SPD_V_DT_FACILITIES" ("DELIVERYORDERID", "CONTRACTID", "FACID", "SPIDERS_FACNAME", "SPIDERS_FACNO", "FACILITY_ID", "FACILITY_NAME", "FACILITY_NO", "ACTIVITY_UIC", "SPECIAL_AREA_CODE", "UNIT_TITLE", "SPECIAL_AREA_NAME", "STATE_ABBR", "STATE_ABBR1", "DESCRIPTION", "PRIME_USE_CATEGORY_CODE", "MAINTENANCE_RESP_UIC", "CI", "MDI", "MDI_DATE", "ENGINEERING_EVAL_DATE") AS 
  select 
  distinct
  -- -1    deliveryorderid,
  delivery_order.deliveryorderid    deliveryorderid,
  region_per_contract.contractid    contractid,
  facility.FACID                    FACID,
  facility.SPIDERS_FACNAME          SPIDERS_FACNAME,
  facility.SPIDERS_FACNO            SPIDERS_FACNO,
  facility.FACILITY_ID              FACILITY_ID,
  facility.FACILITY_NAME            FACILITY_NAME,
  facility.FACILITY_NO              FACILITY_NO,
  facility.ACTIVITY_UIC             ACTIVITY_UIC,
  facility.SPECIAL_AREA_CODE        SPECIAL_AREA_CODE,
  facility.UNIT_TITLE               UNIT_TITLE,
  facility.SPECIAL_AREA_NAME        SPECIAL_AREA_NAME,
  facility.STATE_ABBR               STATE_ABBR,
  facility.STATE_ABBR1              STATE_ABBR1,
  facility.DESCRIPTION              DESCRIPTION,
  facility.PRIME_USE_CATEGORY_CODE  PRIME_USE_CATEGORY_CODE,
  facility.MAINTENANCE_RESP_UIC     MAINTENANCE_RESP_UIC,
  facility.CI                       CI,
  facility.MDI                      MDI,
  facility.MDI_DATE                 MDI_DATE,
  facility.ENGINEERING_EVAL_DATE    ENGINEERING_EVAL_DATE
from 
  spd_mv_dt_facilities        facility,
  spd_t_activities            activity,
  spd_t_regions               region,
  spd_t_region_activities     region_activity,
  spd_t_regions_per_contract  region_per_contract,
  spd_t_category_codes        category_code,
  spd_t_contract_cat_codes    contract_cat_codes,
  spd_t_delivery_orders       delivery_order,
  spd_t_pwss                  pws
where
  facility.activity_uic = activity.spd_uic
  and region_activity.activitiesid = activity.activitiesid
  and region.regionid = region_activity.regionid
  and region_per_contract.regionid = region.regionid
  and category_code.categorycodeid = contract_cat_codes.categorycodeid
  and contract_cat_codes.contractid = region_per_contract.contractid
  and category_code.category_code = facility.prime_use_category_code
  and delivery_order.pwsid = pws.pwsid
  and pws.contractid = region_per_contract.contractid
  --and delivery_order.deliveryorderid= 1100000000911
order by 
  facility.STATE_ABBR1,
  facility.unit_title, 
  facility.special_area_name,
  facility.facility_no;
