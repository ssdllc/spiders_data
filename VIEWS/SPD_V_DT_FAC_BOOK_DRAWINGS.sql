--------------------------------------------------------
--  DDL for View SPD_V_DT_FAC_BOOK_DRAWINGS
--------------------------------------------------------

  CREATE OR REPLACE FORCE EDITIONABLE VIEW "SPIDERS_DATA"."SPD_V_DT_FAC_BOOK_DRAWINGS" ("BFILEID", "FOLDERID", "FILE_EXISTS", "DELIVERYORDERFACILITYID", "FACILITYID", "OTHERID", "FILENAME", "FILEPATH", "NAVFACDRAWINGNUMBER", "DRAWINGTYPE", "DESCRIPTION") AS 
  select
  bfile.bfileid                                           BFILEID,
  bfile.folderid                                          FOLDERID,
  bfileexists(bfile.the_bfile)                            FILE_EXISTS,
  xml_facility_match.dofid                                DELIVERYORDERFACILITYID,
  fac_book_drawing.facilityid                             FACILITYID,
  fac_book_drawing.facdrawingid                           OTHERID,
  f_smoke_get_filename(fac_book_drawing.filename)         FILENAME,
  f_smoke_get_file_path(fac_book_drawing.filename)        FILEPATH,
  fac_book_drawing.navfacdrawingnumber                    NAVFACDRAWINGNUMBER,
  fac_book_drawing.DRAWINGTYPE                            DRAWINGTYPE,
  fac_book_drawing.DESCRIPTION                            DESCRIPTION
from 
  spd_t_xml_facility_drawings   fac_book_drawing,
  spd_t_xml_facilities          xml_facility,
  spd_t_xml_facilities_match    xml_facility_match,
  spd_t_bfiles                  bfile
where
  xml_facility.facilityid = fac_book_drawing.facilityid and
  xml_facility.nfaid = xml_facility_match.xml_nfaid and
  bfile.folderpath =  f_smoke_get_file_path(fac_book_drawing.filename) and 
  bfile.filename = f_smoke_get_filename(fac_book_drawing.filename)
 ;
