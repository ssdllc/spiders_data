--------------------------------------------------------
--  DDL for View SPD_V_DT_FM_IMPORTS
--------------------------------------------------------

  CREATE OR REPLACE FORCE EDITIONABLE VIEW "SPIDERS_DATA"."SPD_V_DT_FM_IMPORTS" ("FACID", "INFADS_FACILITY_ID", "PRODUCT_LINE_LISTID", "SPD_FAC_NAME", "SPD_FAC_NO", "FILESKEY", "DRAWINGS_FILEID", "USER_CHART_FILEID", "PROPERTY_RECORD_FILEID", "IMAGES_FILEID", "PRODUCTLINE_DISPLAY", "FMIMPORTID", "MOORING_ID", "ACTIVITY", "REPORT_NUMBER", "DESIGN_CLASS", "BUOY_TYPE", "MOORING_TYPE", "ANCHOR_TYPE", "REPORT_DATE") AS 
  select f."FACID",f."INFADS_FACILITY_ID",f."PRODUCT_LINE_LISTID",f."SPD_FAC_NAME",f."SPD_FAC_NO",f."FILESKEY",i."DRAWINGS_FILEID",
    i."USER_CHART_FILEID",i."PROPERTY_RECORD_FILEID",i."IMAGES_FILEID", p.PRODUCTLINE_DISPLAY, i.fmimportid, i.mooring_id, i.activity,
    i.report_number, i.design_class, i.buoy_type, i.mooring_type, i.anchor_type, i.report_date
from spd_t_facilities f, spd_t_productlines p, SPD_T_FM_IMPORTS i
where f.PRODUCT_LINE_LISTID = p.PRODUCTLINEID
  and p.PRODUCTLINE_DISPLAY = 'Fleet Moorings'
  and f.facid = i.facid
 ;
