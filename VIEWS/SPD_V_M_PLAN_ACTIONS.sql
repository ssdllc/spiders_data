--------------------------------------------------------
--  DDL for View SPD_V_M_PLAN_ACTIONS
--------------------------------------------------------

  CREATE OR REPLACE FORCE EDITIONABLE VIEW "SPIDERS_DATA"."SPD_V_M_PLAN_ACTIONS" ("PLANACTIONID", "PLANID", "ACTIONID", "REC_INHOUSE_AMT", "REC_KTR_AMT") AS 
  select 
      PLANACTIONID,PLANID,ACTIONID,REC_INHOUSE_AMT,REC_KTR_AMT
    from 
    SPD_T_PLAN_ACTIONS;
