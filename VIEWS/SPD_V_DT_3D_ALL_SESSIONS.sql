--------------------------------------------------------
--  DDL for View SPD_V_DT_3D_ALL_SESSIONS
--------------------------------------------------------
DROP VIEW "SPIDERS_DATA"."SPD_V_DT_ALL_SESSIONS";

  CREATE OR REPLACE FORCE EDITIONABLE VIEW "SPIDERS_DATA"."SPD_V_DT_3D_ALL_SESSIONS" ("SESSIONID", "SCENE_DESCRIPTION", "SHORT_DESCRIPTION", "DESCRIPTION", "SESSION_KEY", "CREATED_BY") AS 
  select
 sessions.sessionid,
 scenes.description scene_description,
 sessions.short_description,
 sessions.description,
 sessions.session_key,
 sessions.created_by
from
 spd_t_3d_sessions sessions,
 spd_t_3d_scenes scenes
where
 sessions.sceneid = scenes.sceneid
 ;
