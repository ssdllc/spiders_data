--------------------------------------------------------
--  DDL for View SPD_V_QC_DIRECT_COSTS
--------------------------------------------------------

  CREATE OR REPLACE FORCE EDITIONABLE VIEW "SPIDERS_DATA"."SPD_V_QC_DIRECT_COSTS" ("DIRECT_COST", "DESCRIPTION", "DIRECT_COST_TYPE", "VISUAL_ORDER", "INSTRUCTIONID", "PWSID") AS 
  select 
  direct_cost.DIRECT_COST         DIRECT_COST,
  direct_cost.DESCRIPTION         DESCRIPTION,
  direct_cost.DIRECT_COST_TYPE    DIRECT_COST_TYPE,
  direct_cost.VISUAL_ORDER        VISUAL_ORDER,
  direct_cost.DIRECTCOSTID        INSTRUCTIONID,
  pws.PWSID                       PWSID
from 
  spd_t_direct_costs direct_cost,
  spd_t_pwss         pws
where
  pws.pwsid = direct_cost.pwsid 
ORDER BY 
  DIRECT_COST_TYPE ASC,
  VISUAL_ORDER ASC
 ;
