--------------------------------------------------------
--  DDL for View SPD_V_QC_ACTIVITY_UICS
--------------------------------------------------------

  CREATE OR REPLACE FORCE EDITIONABLE VIEW "SPIDERS_DATA"."SPD_V_QC_ACTIVITY_UICS" ("DISPLAY_VALUE") AS 
  select distinct(ACTIVITY_UIC) DISPLAY_VALUE from SPIDERS_DATA.SPD_MV_DATA_ANALYSIS where rownum < 200 order by lower(ACTIVITY_UIC)
 ;
