--------------------------------------------------------
--  DDL for View SPD_V_DT_FM_FACILITIES
--------------------------------------------------------

  CREATE OR REPLACE FORCE EDITIONABLE VIEW "SPIDERS_DATA"."SPD_V_DT_FM_FACILITIES" ("FACID", "INFADS_FACILITY_ID", "PRODUCT_LINE_LISTID", "SPD_FAC_NAME", "SPD_FAC_NO", "FILESKEY", "PRODUCTLINE_DISPLAY") AS 
  select f."FACID",f."INFADS_FACILITY_ID",f."PRODUCT_LINE_LISTID",f."SPD_FAC_NAME",f."SPD_FAC_NO",f."FILESKEY", p.PRODUCTLINE_DISPLAY
from spd_t_facilities f, spd_t_productlines p
where f.PRODUCT_LINE_LISTID = p.PRODUCTLINEID
  and p.PRODUCTLINE_DISPLAY = 'Fleet Moorings'
 ;
