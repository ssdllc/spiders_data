--------------------------------------------------------
--  DDL for View SPD_V_QC_JOB_TITLES
--------------------------------------------------------

  CREATE OR REPLACE FORCE EDITIONABLE VIEW "SPIDERS_DATA"."SPD_V_QC_JOB_TITLES" ("JOB_TITLE", "JOB_TITLE_DESCRIPTION", "VISUAL_ORDER", "BASE_RATE", "OPTION_1_RATE", "OPTION_2_RATE", "OPTION_3_RATE", "OPTION_4_RATE", "JOBTITLEID", "CONTRACTCONTRACTORID") AS 
  select 
  job_title.JOB_TITLE             JOB_TITLE,
  job_title.JOB_TITLE_DESCRIPTION JOB_TITLE_DESCRIPTION,
  job_title.VISUAL_ORDER          VISUAL_ORDER,
  job_title.BASE_RATE             BASE_RATE,
  job_title.OPTION_1_RATE         OPTION_1_RATE,
  job_title.OPTION_2_RATE         OPTION_2_RATE,
  job_title.OPTION_3_RATE         OPTION_3_RATE,
  job_title.OPTION_4_RATE         OPTION_4_RATE,
  job_title.JOBTITLEID            JOBTITLEID,
  job_title.CONTRACTCONTRACTORID  CONTRACTCONTRACTORID  
from 
  spd_t_job_titles job_title  
ORDER BY 
  VISUAL_ORDER ASC
 ;
