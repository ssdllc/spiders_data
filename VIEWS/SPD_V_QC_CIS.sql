--------------------------------------------------------
--  DDL for View SPD_V_QC_CIS
--------------------------------------------------------

  CREATE OR REPLACE FORCE EDITIONABLE VIEW "SPIDERS_DATA"."SPD_V_QC_CIS" ("DISPLAY_VALUE") AS 
  select distinct(CI) DISPLAY_VALUE from SPIDERS_DATA.SPD_MV_DATA_ANALYSIS where CI is not null order by to_number(CI)
 ;
