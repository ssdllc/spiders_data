--------------------------------------------------------
--  DDL for View SPD_V_CONTRACTS_PL
--------------------------------------------------------

  CREATE OR REPLACE FORCE EDITIONABLE VIEW "SPIDERS_DATA"."SPD_V_CONTRACTS_PL" ("GROUP_FILTER_DISPLAY_NAME", "GROUP_NAME", "DISPLAY_VALUE", "RETURN_VALUE", "RECORD_COUNT", "R_N") AS 
  select
      'Product Line' group_filter_display_name,
      'PL' group_name,
      'change_display_value' display_value,
      'change_return_value' return_value,
      COUNT(*) OVER (PARTITION BY 'return_value') record_count,
      ROW_NUMBER() OVER (PARTITION BY 'return_value' ORDER BY 'return_value' NULLS LAST) r_n
    from
      dual
 ;
