--------------------------------------------------------
--  DDL for View SPD_V_M_XML_AI_FITTINGS
--------------------------------------------------------

  CREATE OR REPLACE FORCE EDITIONABLE VIEW "SPIDERS_DATA"."SPD_V_M_XML_AI_FITTINGS" ("FITTINGID", "INVENTORYID", "ASSETTYPE", "FITTINGIDNUMBER", "TYPE", "DESIGNCAPACITY", "RATINGCAPACITY", "CONDITIONFITTING", "CONDITIONBASE", "CONDITIONCONNECTIONHARDWARE", "PHOTOFILENAME") AS 
  select 
      FITTINGID,INVENTORYID,ASSETTYPE,FITTINGIDNUMBER,TYPE,DESIGNCAPACITY,RATINGCAPACITY,CONDITIONFITTING,CONDITIONBASE,CONDITIONCONNECTIONHARDWARE,PHOTOFILENAME
    from 
    SPD_T_XML_AI_FITTINGS
 ;
