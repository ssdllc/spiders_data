--------------------------------------------------------
--  DDL for View SPD_V_M_XML_PATH_TABLE_MAPS
--------------------------------------------------------

  CREATE OR REPLACE FORCE EDITIONABLE VIEW "SPIDERS_DATA"."SPD_V_M_XML_PATH_TABLE_MAPS" ("XMLNODETABLEMAPID", "NODE_NAME", "TABLE_NAME", "FPATH") AS 
  select 
      XMLNODETABLEMAPID,NODE_NAME,TABLE_NAME,FPATH
    from 
    SPD_T_XML_PATH_TABLE_MAPS
 ;
