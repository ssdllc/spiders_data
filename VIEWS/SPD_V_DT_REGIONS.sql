--------------------------------------------------------
--  DDL for View SPD_V_DT_REGIONS
--------------------------------------------------------

  CREATE OR REPLACE FORCE EDITIONABLE VIEW "SPIDERS_DATA"."SPD_V_DT_REGIONS" ("REGIONID", "SHORTNAME", "LONGNAME", "DESCRIPTION", "NUM_ACTIVITIES") AS 
  SELECT 
    "REGIONID",
    "SHORTNAME",
    "LONGNAME",
    "DESCRIPTION",
    (SELECT 
      COUNT(*)
     FROM
      SPD_T_REGION_ACTIVITIES
     WHERE
      SPD_T_REGION_ACTIVITIES.REGIONID = SPD_T_REGIONS.REGIONID
    ) num_activities
FROM 
    SPD_T_REGIONS
 ;
