--------------------------------------------------------
--  DDL for View SPD_V_QC_EST_REPAIR_CIS
--------------------------------------------------------

  CREATE OR REPLACE FORCE EDITIONABLE VIEW "SPIDERS_DATA"."SPD_V_QC_EST_REPAIR_CIS" ("EST_REPAIR_CONDITION_INDEX") AS 
  select distinct(EST_REPAIR_CONDITION_INDEX) from SPD_MV_DATA_ANALYSIS order by to_number(EST_REPAIR_CONDITION_INDEX)
 ;
