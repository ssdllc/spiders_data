--------------------------------------------------------
--  DDL for View SPD_V_QC_CONTRACT_CTRS_BC
--------------------------------------------------------

  CREATE OR REPLACE FORCE EDITIONABLE VIEW "SPIDERS_DATA"."SPD_V_QC_CONTRACT_CTRS_BC" ("CONTRACTID") AS 
  select 
  contract_contractor.CONTRACTID  CONTRACTID 
from 
  spd_t_contract_contractors contract_contractor  
ORDER BY 
  CONTRACTID ASC
 ;
