--------------------------------------------------------
--  DDL for View SPD_V_QC_SPECIAL_AREA_CODES
--------------------------------------------------------

  CREATE OR REPLACE FORCE EDITIONABLE VIEW "SPIDERS_DATA"."SPD_V_QC_SPECIAL_AREA_CODES" ("DISPLAY_VALUE") AS 
  select distinct(SPECIAL_AREA_CODE) DISPLAY_VALUE from SPIDERS_DATA.SPD_MV_DATA_ANALYSIS where rownum < 200 order by lower(SPECIAL_AREA_CODE)
 ;
