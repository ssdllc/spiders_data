--------------------------------------------------------
--  DDL for View SPD_V_DT_CONTRACT_INSTRUCTIONS
--------------------------------------------------------

  CREATE OR REPLACE FORCE EDITIONABLE VIEW "SPIDERS_DATA"."SPD_V_DT_CONTRACT_INSTRUCTIONS" ("CONTRACTINSTRUCTIONID", "INSTRUCTIONID", "CONTRACTID", "GROUPING", "VISUAL_ORDER", "INSTRUCTION", "DESCRIPTION", "BASELINE_ADJUSTMENT") AS 
  select 
    contract_instructions.contractinstructionid,
    contract_instructions.instructionid,
    contract_instructions.contractid,
    instruction.grouping,
    instruction.visual_order,
    instruction.instruction,
    instruction.description,
    instruction.baseline_adjustment
from 
    spd_t_contract_instructions contract_instructions,
    spd_t_instructions instruction
where
    contract_instructions.instructionid = instruction.instructionid
 ;
