--------------------------------------------------------
--  DDL for View SPD_V_QC_DELIVERABLE_BC
--------------------------------------------------------

  CREATE OR REPLACE FORCE EDITIONABLE VIEW "SPIDERS_DATA"."SPD_V_QC_DELIVERABLE_BC" ("DELIVERABLEID", "PWSID", "CONTRACTID") AS 
  select 
  deliverable.DELIVERABLEID   DELIVERABLEID,
  deliverable.PWSID           PWSID,
  pws.CONTRACTID              CONTRACTID 
from 
  spd_t_deliverables  deliverable,
  spd_t_pwss          pws
where
  deliverable.pwsid = pws.pwsid
ORDER BY 
  DELIVERABLEID ASC
 ;
