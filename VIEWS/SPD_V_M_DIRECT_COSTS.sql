--------------------------------------------------------
--  DDL for View SPD_V_M_DIRECT_COSTS
--------------------------------------------------------

  CREATE OR REPLACE FORCE EDITIONABLE VIEW "SPIDERS_DATA"."SPD_V_M_DIRECT_COSTS" ("DIRECTCOSTID", "PWSID", "DIRECT_COST_TYPE", "VISUAL_ORDER", "DIRECT_COST", "DESCRIPTION") AS 
  select 
      DIRECTCOSTID,PWSID,DIRECT_COST_TYPE,VISUAL_ORDER,DIRECT_COST,DESCRIPTION
    from 
    SPD_T_DIRECT_COSTS
 ;
