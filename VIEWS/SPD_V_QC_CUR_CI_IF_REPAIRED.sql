--------------------------------------------------------
--  DDL for View SPD_V_QC_CUR_CI_IF_REPAIRED
--------------------------------------------------------

  CREATE OR REPLACE FORCE EDITIONABLE VIEW "SPIDERS_DATA"."SPD_V_QC_CUR_CI_IF_REPAIRED" ("UNIFORMATIICODE", "FACILITYID", "CURRENTCI", "FIVEYEARCIIFREPAIRED", "TENYEARCIIFREPAIRED") AS 
  SELECT   
  xml_asset_summary.UNIFORMATIICODE,
  xml_asset_summary.FACILITYID,
  xml_asset_summary.CURRENTCI,
  xml_asset_summary.FIVEYEARCIIFREPAIRED,
  xml_asset_summary.TENYEARCIIFREPAIRED
FROM
  spd_t_xml_asset_summary xml_asset_summary
WHERE
  xml_asset_summary.CURRENTCI is not null
 ;
