--------------------------------------------------------
--  DDL for View SPD_V_QC_CONTRACT_CARD
--------------------------------------------------------

  CREATE OR REPLACE FORCE EDITIONABLE VIEW "SPIDERS_DATA"."SPD_V_QC_CONTRACT_CARD" ("CONTRACTID", "CONTRACT_NAME", "BASE_START_DATE", "BASE_END_DATE", "OPTION1_START_DATE", "OPTION1_END_DATE", "OPTION2_START_DATE", "OPTION2_END_DATE", "OPTION3_START_DATE", "OPTION3_END_DATE", "OPTION4_START_DATE", "OPTION4_END_DATE", "PRODUCTLINE_DISPLAY") AS 
  select c.contractid, c.contract_name, c.base_start_date, c.base_end_date, c.option1_start_date, c.option1_end_date,
c.option2_start_date, c.option2_end_date, c.option3_start_date, c.option3_end_date, c.option4_start_date, c.option4_end_date,
p.productline_display

from spd_t_contracts c, spd_t_productlines p

where p.productlineid = c.productline_listid;
