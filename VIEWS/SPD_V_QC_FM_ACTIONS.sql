--------------------------------------------------------
--  DDL for View SPD_V_QC_ACTIONS
--------------------------------------------------------

  CREATE OR REPLACE VIEW "SPIDERS_DATA"."SPD_V_QC_FM_ACTIONS" AS 
  SELECT
        NVL(fm_actions.REPORT_NUMBER, 'No Report Title') REPORT_NUMBER,
        fm_actions.REPORT_DATE,
        fm_actions.FMACTIONID,
        fm_moorings.FMMOORINGID
	FROM 
        SPD_T_FM_ACTIONS fm_actions,
        SPD_T_FM_MOORINGS fm_moorings
  WHERE fm_actions.ACTIVITIESID = fm_moorings.ACTIVITIESID
  AND fm_actions.SPECIAL_AREA_CODE = fm_moorings.SPECIAL_AREA_CODE
  AND fm_actions.FMACTIONID NOT IN (
    SELECT FMACTIONID
    FROM SPD_T_FM_ACTION_MOORINGS
    WHERE FMMOORINGID = fm_moorings.FMMOORINGID
  );
