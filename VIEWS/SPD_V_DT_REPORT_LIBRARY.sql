--------------------------------------------------------
--  DDL for View SPD_V_DT_REPORT_LIBRARY
--------------------------------------------------------

  CREATE OR REPLACE FORCE EDITIONABLE VIEW "SPIDERS_DATA"."SPD_V_DT_REPORT_LIBRARY" ("REPORTID", "REPORTTITLE", "REPORTAUTHOR", "ESTSTARTDATE", "CONTRACTOR", "CONTRACTNUMBER", "CREATEDDATE", "REPORTNUMBER", "ACTIONSID", "ACTIVITY_NAME", "ACTIVITY_UIC", "CEACTIONSID", "CITY", "STATE", "COUNTRY", "NOTESKEY", "GOVPROJENGINEER", "PRODUCTLINE_LISTID", "productline_display", "countOfFiles", "FOLDERID") AS 
  select 
  REPORTID,
  REPORTTITLE,
  REPORTAUTHOR,
  to_char(ESTSTARTDATE,'YYYY') ESTSTARTDATE,
  CONTRACTOR,
  CONTRACTNUMBER,
  CREATEDDATE,
  REPORTNUMBER,
  ACTIONSID,
  (select spd_activity_name from spd_t_activities actv, spd_t_action_planner ap where actv.activitiesid = ap.planned_activitiesid and ap.apid = rep.ACTIONSID) ACTIVITY_NAME,
  (select spd_uic from spd_t_activities actv, spd_t_action_planner ap where actv.activitiesid = ap.planned_activitiesid and ap.apid = rep.ACTIONSID) ACTIVITY_UIC,
  CEACTIONSID,
  CITY,
  STATE,
  COUNTRY,
  NOTESKEY,
  GOVPROJENGINEER,
  PRODUCTLINE_LISTID,
  (select displayvalue from spd_t_lists where listid = rep.productline_listid) productline_display,
  (select count(reportfileid) from spd_t_report_files rf where rf.reportid = rep.reportid and BFILEEXISTS(the_bfile) = 1) countOfFiles,
  FOLDERID
from 
  spd_t_reports rep
 ;
