--------------------------------------------------------
--  DDL for View SPD_V_M_PLAN_NOTES
--------------------------------------------------------

  CREATE OR REPLACE FORCE EDITIONABLE VIEW "SPIDERS_DATA"."SPD_V_M_PLAN_NOTES" ("PLANNOTEID", "PLANID", "NOTE") AS 
  select 
      PLANNOTEID,PLANID,NOTE
    from 
    SPD_T_PLAN_NOTES;
