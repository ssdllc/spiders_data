--------------------------------------------------------
--  File created - Thursday-August-09-2018   
--------------------------------------------------------
--------------------------------------------------------
--  DDL for View SPD_V_M_USERS
--------------------------------------------------------

  CREATE OR REPLACE FORCE EDITIONABLE VIEW "SPIDERS_DATA"."SPD_V_M_USERS" ("USERID", "USER_EMAIL", "USER_PERMISSION") AS 
  select
      USERID,USER_EMAIL,USER_PERMISSION
    from
    SPD_T_USERS;
