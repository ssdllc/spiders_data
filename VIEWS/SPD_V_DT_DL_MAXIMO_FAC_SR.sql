--------------------------------------------------------
--  DDL for View SPD_V_DT_DL_MAXIMO_FAC_SR
--------------------------------------------------------

  CREATE OR REPLACE FORCE EDITIONABLE VIEW "SPIDERS_DATA"."SPD_V_DT_DL_MAXIMO_FAC_SR" ("SPIDERSMAXIMOSRID", "FACID", "FACILITY_ID", "COMMENTS", "DELIVERYORDERID", "DELIVERYORDERFACILITYID", "SITEID", "SUMMARY", "LONG_DESC", "INFADS", "FACILITYNAME", "LOCATION", "ADDLLOCINFO", "WORKTYPE", "SUBWORKTYPE", "BUSINESSLINE", "PSDELIVERABLE", "WORKCATEGORY", "REPORTEDBY", "CUSTOMERID", "CUSTOMERNAME", "CUSTOMERPHONE", "CUSTOMEREMAIL", "CUSTOMERREFNUM", "PRIORITYSCORE", "PRIORITYYEAR", "SPIDERSUFII", "ASSETMAXIMO", "ASSET", "EXPECTEDCOST", "GLACCOUNT", "SVCPROVREFCODE", "CLASSIFICATION", "INTERNAL", "CREATED_BY", "CREATED_TIMESTAMP", "UPDATED_BY", "UPDATED_TIMESTAMP") AS 
  select
  spiders_maximo_ci.SPIDERSMAXIMOSRID                SPIDERSMAXIMOSRID,
  facility.facid                                     FACID,
  spiders_maximo_ci.INFADS                           FACILITY_ID,
  spiders_maximo_ci.COMMENTS                         COMMENTS ,
  spiders_maximo_ci.DELIVERYORDERID                  DELIVERYORDERID,        
  spiders_maximo_ci.DELIVERYORDERFACILITYID          DELIVERYORDERFACILITYID,        
  spiders_maximo_ci.SITEID                           SITEID ,
  spiders_maximo_ci.SUMMARY                          SUMMARY ,
  spiders_maximo_ci.LONG_DESC                        LONG_DESC, 
  spiders_maximo_ci.INFADS                           INFADS ,
  spiders_maximo_ci.FACILITYNAME                     FACILITYNAME, 
  spiders_maximo_ci.LOCATION                         LOCATION ,
  spiders_maximo_ci.ADDLLOCINFO                      ADDLLOCINFO, 
  spiders_maximo_ci.WORKTYPE                         WORKTYPE ,
  spiders_maximo_ci.SUBWORKTYPE                      SUBWORKTYPE, 
  spiders_maximo_ci.BUSINESSLINE                     BUSINESSLINE, 
  spiders_maximo_ci.PSDELIVERABLE                    PSDELIVERABLE, 
  spiders_maximo_ci.WORKCATEGORY                     WORKCATEGORY ,
  spiders_maximo_ci.REPORTEDBY                       REPORTEDBY ,
  spiders_maximo_ci.CUSTOMERID                       CUSTOMERID ,
  spiders_maximo_ci.CUSTOMERNAME                     CUSTOMERNAME, 
  spiders_maximo_ci.CUSTOMERPHONE                    CUSTOMERPHONE, 
  spiders_maximo_ci.CUSTOMEREMAIL                    CUSTOMEREMAIL ,
  spiders_maximo_ci.CUSTOMERREFNUM                   CUSTOMERREFNUM ,
  spiders_maximo_ci.PRIORITYSCORE                    PRIORITYSCORE ,
  spiders_maximo_ci.PRIORITYYEAR                     PRIORITYYEAR ,
  spiders_maximo_ci.SPIDERSUFII                      SPIDERSUFII ,
  spiders_maximo_ci.ASSETMAXIMO                      ASSETMAXIMO ,
  spiders_maximo_ci.ASSET                            ASSET ,
  spiders_maximo_ci.EXPECTEDCOST                     EXPECTEDCOST, 
  spiders_maximo_ci.GLACCOUNT                        GLACCOUNT ,
  spiders_maximo_ci.SVCPROVREFCODE                   SVCPROVREFCODE, 
  spiders_maximo_ci.CLASSIFICATION                   CLASSIFICATION ,
  spiders_maximo_ci.INTERNAL                         INTERNAL ,
  spiders_maximo_ci.CREATED_BY                       CREATED_BY, 
  spiders_maximo_ci.CREATED_TIMESTAMP                CREATED_TIMESTAMP,          
  spiders_maximo_ci.UPDATED_BY                       UPDATED_BY ,
  spiders_maximo_ci.UPDATED_TIMESTAMP                UPDATED_TIMESTAMP 
from
  spd_t_spiders_maximo_sr     spiders_maximo_ci,
  spd_t_facilities            facility
where
  spiders_maximo_ci.infads = facility.infads_facility_id
 ;
