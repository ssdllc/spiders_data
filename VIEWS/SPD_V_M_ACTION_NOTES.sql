--------------------------------------------------------
--  DDL for View SPD_V_M_ACTION_NOTES
--------------------------------------------------------

  CREATE OR REPLACE FORCE EDITIONABLE VIEW "SPIDERS_DATA"."SPD_V_M_ACTION_NOTES" ("ACTIONNOTEID", "ACTIONID", "NOTE") AS 
  select 
      ACTIONNOTEID,ACTIONID,NOTE
    from 
    SPD_T_ACTION_NOTES;
