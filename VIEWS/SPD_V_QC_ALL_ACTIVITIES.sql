--------------------------------------------------------
--  DDL for View SPD_V_QC_ALL_ACTIVITIES
--------------------------------------------------------

  CREATE OR REPLACE FORCE EDITIONABLE VIEW "SPIDERS_DATA"."SPD_V_QC_ALL_ACTIVITIES" ("UIC", "UNIT_TITLE", "STATE", "COUNTRY", "HOST_TENANT_CODE", "SPD_UIC", "ACTIVITIESID") AS 
  select 
  inf_cur_activity.uic,
  inf_cur_activity.unit_title,
  inf_cur_activity.state,
  inf_cur_activity.country,
  inf_cur_activity.host_tenant_code,
  activity.spd_uic,
  activity.activitiesid
from 
  ( select 
      uic,
      max(unit_title)       unit_title,
      max(state)            state,
      max(country)          country,
      max(host_tenant_code) host_tenant_code
    from 
      spd_mv_inf_cur_activities 
    group by 
      uic) inf_cur_activity,
  spd_t_activities activity
where  
  inf_cur_activity.uic = activity.spd_uic(+)
  and inf_cur_activity.host_tenant_code = 0
order by
  inf_cur_activity.country desc,
  inf_cur_activity.state,
  inf_cur_activity.uic
 ;
