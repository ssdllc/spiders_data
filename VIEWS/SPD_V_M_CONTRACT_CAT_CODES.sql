--------------------------------------------------------
--  DDL for View SPD_V_M_CONTRACT_CAT_CODES
--------------------------------------------------------

  CREATE OR REPLACE FORCE EDITIONABLE VIEW "SPIDERS_DATA"."SPD_V_M_CONTRACT_CAT_CODES" ("CONTRACTCATCODEID", "CONTRACTID", "CATEGORYCODEID") AS 
  select 
      contractcatcodeid,contractid,categorycodeid
    from 
    SPD_T_contract_cat_codes
 ;
