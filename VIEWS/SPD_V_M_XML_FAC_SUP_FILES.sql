--------------------------------------------------------
--  DDL for View SPD_V_M_XML_FAC_SUP_FILES
--------------------------------------------------------

  CREATE OR REPLACE FORCE EDITIONABLE VIEW "SPIDERS_DATA"."SPD_V_M_XML_FAC_SUP_FILES" ("FILEID", "FACILITYID", "SUPPORTINGFILEID", "FILENAME", "DESCRIPTION") AS 
  select 
      FILEID,FACILITYID,SUPPORTINGFILEID,FILENAME,DESCRIPTION
    from 
    SPD_T_XML_FAC_SUP_FILES
 ;
