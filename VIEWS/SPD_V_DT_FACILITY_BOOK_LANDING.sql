--------------------------------------------------------
--  DDL for View SPD_V_DT_FACILITY_BOOK_LANDING
--------------------------------------------------------

  CREATE OR REPLACE FORCE EDITIONABLE VIEW "SPIDERS_DATA"."SPD_V_DT_FACILITY_BOOK_LANDING" 
  ("UIC", 
  "ACTIVITY_UIC",
  "UNIT_TITLE",
  "CATEGORY_CODE",
  "FACILITYID",
  "FACILITYNAME",
  "FACILITYNO",
  "PRODUCTLINE") AS 
  SELECT 
  --FAC.FACILITY_NAME,
  ACT.UIC || '|' || NVL(FAC.SPECIAL_AREA_CODE, 'host') UIC,
  ACT.UIC ACTIVITY_UIC,
  ACT.UNIT_TITLE,
  --SPEC.SPECIAL_AREA_NAME,
  FAC.PRIME_USE_CATEGORY_CODE CATEGORY_CODE,
  ----FAC.PROPERTY_NO,
  ----XML_EXEC_TABLE.NFAID,
  --FAC.FACILITY_ID NFAID,
  
  XML_FACILITY.FACILITYID,
  
  XML_FACILITY.FACILITYNAME,
  TO_CHAR(XML_FACILITY.FACILITYNO) FACILITYNO,
  'Waterfront' PRODUCTLINE
  /*
  XML_FACILITY.XMLACTIONID,
  XML_FACILITY.FSFILENAME,
  XML_FACILITY.ONSITESTARTDATE,
  XML_FACILITY.ONSITEENDDATE,
  XML_FACILITY.SUBMITTALDATE,
  XML_FACILITY.FACILITYNAME,
  XML_FACILITY.FACILITYNO,
  XML_FACILITY.NFAID fac_nfaid,
  XML_FACILITY.SPIDERS_FACILITYID,
  XML_FACILITY.COSTESTIMATEFILENAME,
  XML_FACILITY.DD1391FILENAME,
  XML_FACILITY.DELIVERYORDERFACILITYID,
  XML_EXEC_TABLE.EXECTABLEID,
  XML_EXEC_TABLE.FACNAME,
  XML_EXEC_TABLE.FACID,
  
  XML_EXEC_TABLE.PRN,
  XML_EXEC_TABLE.INSPECTYPE,
  XML_EXEC_TABLE.INSPECTED,
  XML_EXEC_TABLE.ENGRATING,
  XML_EXEC_TABLE.CIRATING,
  XML_EXEC_TABLE.FIVEYEARCI,
  XML_EXEC_TABLE.TENYEARCI,
  XML_EXEC_TABLE.OPRATING,
  XML_EXEC_TABLE.RESTRICTIONS,
  XML_EXEC_TABLE.LOADING,
  XML_EXEC_TABLE.MOORING,
  XML_EXEC_TABLE.BERTHING,
  XML_EXEC_TABLE.REPAIRREC,
  XML_EXEC_TABLE.USAGEDESC
  */
FROM
  SPD_T_XML_FACILITIES  XML_FACILITY,
  (
    select  *
    from (
      select adjust_inspected.*, max(adjust_inspected.LAST_INSPECTED) over (partition by adjust_inspected.nfaid) max_inspected     
      from (
        select exectab.*, DECODE(exectab.INSPECTED,null,0,exectab.INSPECTED) as LAST_INSPECTED from SPD_T_XML_EXEC_TABLES exectab
      ) adjust_inspected
    )
    where last_inspected = max_inspected
  )                         XML_EXEC_TABLE,
  SPD_MV_INF_FACILITY       FAC,
  SPD_MV_INF_ACTIVITY       ACT
WHERE
  XML_EXEC_TABLE.FACILITYID = XML_FACILITY.FACILITYID
  AND XML_EXEC_TABLE.NFAID = fac.FACILITY_ID(+)
  AND fac.ACTIVITY_UIC = act.UIC(+)
  
UNION

select DISTINCT
  inf.activity_uic || '|' || NVL(inf.special_area_code, 'host') UIC,
  inf.activity_uic activity_uic,
  act.spd_activity_name UNIT_TITLE,
  inf.prime_use_category_code CATEGORY_CODE,
  fac.facid FACILITYID,
  fac.spd_fac_name FACILITYNAME,
  fac.spd_fac_no FACILITYNO,
  'Bridges' PRODUCTLINE
from
  --spd_t_delivery_orders delivery_order,
  --spd_t_contracts contract,
  --spd_t_productlines  productline,
  --spd_t_pwss pwss,
  spd_t_sia_structures      sia_structure,
  spd_t_sia_data            sia_data,
  spd_t_delivery_order_facs do_fac,
  spd_t_facilities fac,
  spd_t_activities act,
  spd_mv_inf_facility inf
where
  --delivery_order.pwsid = pwss.pwsid
  --and pwss.contractid = contract.contractid
  --and contract.productline_listid = productline.productlineid
  --and delivery_order.DELIVERYORDERID = do_fac.DELIVERYORDERID
  sia_structure.dofid = do_fac.DELIVERYORDERFACILITYID
  and sia_data.siastructureid = sia_structure.siastructureid

  and do_fac.facid = fac.facid
  and fac.infads_facility_id = inf.facility_id(+)
  and inf.activity_uic = act.spd_uic(+)
UNION
SELECT
  activities.SPD_UIC || '|' || moorings.SPECIAL_AREA_CODE UIC,
  activities.SPD_UIC ACTIVITY_UIC,
  activities.SPD_ACTIVITY_NAME UNIT_TITLE,
  'N/A' CATEGORY_CODE,
  moorings.FMMOORINGID FACILITYID,
  moorings.MOORING_NAME FACILITYNAME,
  'N/A' FACILITYNO,
  'Fleet Moorings' PRODUCTLINE
FROM 
  SPD_T_ACTIVITIES activities,
  SPD_T_FM_MOORINGS moorings
WHERE
  activities.ACTIVITIESID = moorings.ACTIVITIESID;
