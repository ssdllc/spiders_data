--------------------------------------------------------
--  DDL for View SPD_V_M_BFILES
--------------------------------------------------------

  CREATE OR REPLACE FORCE EDITIONABLE VIEW "SPIDERS_DATA"."SPD_V_M_BFILES" ("BFILEID", "FOLDERID", "FILENAME", "DESCRIPTION", "THE_BFILE") AS 
  select 
      BFILEID,FOLDERID,FILENAME,DESCRIPTION,THE_BFILE
    from 
    SPD_T_BFILES
 ;
