--------------------------------------------------------
--  DDL for View SPD_V_M_DELIVERY_ORDER_FACS
--------------------------------------------------------

  CREATE OR REPLACE FORCE EDITIONABLE VIEW "SPIDERS_DATA"."SPD_V_M_DELIVERY_ORDER_FACS" ("DELIVERYORDERFACILITYID", "DELIVERYORDERID", "FACID") AS 
  select 
      DELIVERYORDERFACILITYID,DELIVERYORDERID,FACID
    from 
    SPD_T_DELIVERY_ORDER_FACS
 ;
