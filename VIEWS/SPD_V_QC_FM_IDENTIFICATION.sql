CREATE OR REPLACE  VIEW "SPIDERS_DATA"."SPD_V_QC_FM_IDENTIFICATION" AS 
SELECT
  actions.FMACTIONID,
  act_with_spec_areas.SPD_UIC ACTIVITY_UIC,
  act_with_spec_areas.SPD_ACTIVITY_NAME ACTIVITY_NAME,
  act_with_spec_areas.SPECIAL_AREA_NAME SPECIAL_AREA_NAME,
  actions.REPORT_DATE REPORT_DATE,
  actions.REPORT_NUMBER REPORT_NUMBER,
  moorings.MOORING_NAME MOORING_NAME,
  action_moorings.FMACTIONMOORINGID,
  action_moorings.FMMOORINGID,
  action_moorings.DESIGN_CLASS,
  action_moorings.DESIGN_RATED_CAPACITY,
  action_moorings.BUOY_TYPE,
  action_moorings.BUOY_DIAMETER,
  action_moorings.MOORING_TYPE,
  action_moorings.QUANTITY_OF_GROUND_LEGS,
  action_moorings.ANCHOR_TYPE,
  action_moorings.ANCHOR_SIZE,
  action_moorings.RISER_SIZE,
  action_moorings.GROUND_LEG_SIZE,
  action_moorings.MOORING_CONDITION_READINESS,
  action_moorings.INSPECTION_DATE,
  action_moorings.MOORING_RATED_CONDITION,
  action_moorings.REQUIRED_REPAIRS,
  action_moorings.CURRENT_CLASS,
  action_moorings.CURRENT_RATED_CAPACITY,
  action_moorings.DIVERS,
  action_moorings.ENGINEER_IN_CHARGE,
  action_moorings.BUOY_FREEBOARD,
  action_moorings.WATER_VISIBILITY,
  action_moorings.WATER_DEPTH,
  action_moorings.BOTTOM_CONDITION,
  action_moorings.LAT_LONG_DATUM,
  action_moorings.LATITUDE,
  action_moorings.LONGITUDE,
  action_moorings.NORTHING_EASTING_DATUM,
  action_moorings.NORTHING,
  action_moorings.EASTING,
  action_moorings.ANODES_DEPLETED,
  action_moorings.ANODES_REPLACED,
  action_moorings.REPLACEMENT_ANODE_SIZE,
  action_moorings.QUANTITY_ANODES_REPLACED
FROM 
  SPD_T_FM_ACTIONS actions,
  SPD_T_FM_MOORINGS moorings,
  SPD_V_QC_ACT_WITH_SPEC_AREAS act_with_spec_areas,
  SPD_T_FM_ACTION_MOORINGS action_moorings
WHERE
  actions.ACTIVITIESID = act_with_spec_areas.ACTIVITIESID and
  actions.SPECIAL_AREA_CODE = act_with_spec_areas.SPECIAL_AREA_CODE and
  actions.FMACTIONID = action_moorings.FMACTIONID and
  actions.ACTIVITIESID = moorings.ACTIVITIESID;