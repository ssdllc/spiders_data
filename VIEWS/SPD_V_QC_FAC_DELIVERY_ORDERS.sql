--------------------------------------------------------
--  DDL for View SPD_V_QC_FAC_DELIVERY_ORDERS
--------------------------------------------------------

  CREATE OR REPLACE FORCE EDITIONABLE VIEW "SPIDERS_DATA"."SPD_V_QC_FAC_DELIVERY_ORDERS" ("DELIVERY_ORDER_NAME", "DELIVERYORDERFACILITYID", "FACILITYID") AS 
  SELECT 
  do.DELIVERY_ORDER_NAME,
  xml_fac.DELIVERYORDERFACILITYID,
  xml_fac.FACILITYID
FROM 
  SPD_T_DELIVERY_ORDERS do, 
  SPD_T_XML_FACILITIES xml_fac,
  SPD_T_XML_ACTIONS xml_act
WHERE   
  do.ADOID = xml_act.ADOID AND
  xml_act.XMLACTIONID = xml_fac.XMLACTIONID
 ;
