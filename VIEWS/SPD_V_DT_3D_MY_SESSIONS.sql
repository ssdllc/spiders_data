--------------------------------------------------------
--  DDL for View SPD_V_DT_3D_MY_SESSIONS
--------------------------------------------------------
DROP VIEW "SPIDERS_DATA"."SPD_V_DT_MY_SESSIONS";

  CREATE OR REPLACE FORCE EDITIONABLE VIEW "SPIDERS_DATA"."SPD_V_DT_3D_MY_SESSIONS" ("SESSIONID", "SCENE_DESCRIPTION", "SHORT_DESCRIPTION", "DESCRIPTION", "SESSION_KEY", "CREATED_BY") AS 
  SELECT 
    sessions.sessionid,
    scenes.description scene_description,
    sessions.short_description,
    sessions.description,
    sessions.session_key,
    sessions.created_by
FROM 
    SPD_T_3D_SESSIONS sessions,
    SPD_T_3D_SCENES scenes
WHERE
    sessions.sceneid = scenes.sceneid
    and
    sessions.created_by = v('APP_USER')
 ;
