--------------------------------------------------------
--  DDL for View SPD_V_QC_STATE_ABBRS
--------------------------------------------------------

  CREATE OR REPLACE FORCE EDITIONABLE VIEW "SPIDERS_DATA"."SPD_V_QC_STATE_ABBRS" ("DISPLAY_VALUE") AS 
  select distinct(STATE_ABBR) DISPLAY_VALUE from SPIDERS_DATA.SPD_MV_DATA_ANALYSIS order by lower(STATE_ABBR)
 ;
