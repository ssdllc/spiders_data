--------------------------------------------------------
--  DDL for View SPD_V_QC_REGION_ACTIVITIES
--------------------------------------------------------

  CREATE OR REPLACE FORCE EDITIONABLE VIEW "SPIDERS_DATA"."SPD_V_QC_REGION_ACTIVITIES" ("REGIONACTIVITYID", "REGIONID", "ACTIVITIESID", "UNIT_TITLE", "SPD_UIC", "UIC") AS 
  SELECT 
	regionActivities.REGIONACTIVITYID,
	regionActivities.REGIONID,
	regionActivities.ACTIVITIESID,
	activities.SPD_ACTIVITY_NAME unit_title,
	activities.SPD_UIC,
  activities.SPD_UIC uic

FROM 
  spd_t_region_activities regionActivities,
  spd_t_activities activities
WHERE
  regionActivities.ACTIVITIESID = activities.ACTIVITIESID;
