--------------------------------------------------------
--  DDL for View SPD_V_QC_CONTRACT_CAT_CODES_2
--------------------------------------------------------

  CREATE OR REPLACE FORCE EDITIONABLE VIEW "SPIDERS_DATA"."SPD_V_QC_CONTRACT_CAT_CODES_2" ("CONTRACTCATCODEID", "CONTRACTID", "CATEGORYCODEID", "CATEGORY_CODE", "SHORT_DESC", "MEDIUM_DESC") AS 
  select 
  contract_cat_code.contractcatcodeid   contractcatcodeid,
  contract_cat_code.contractid          contractid,
  contract_cat_code.categorycodeid      categorycodeid,
  category_code.category_code           category_code,
  category_code.short_desc              short_desc,
  category_code.medium_desc             medium_desc
from 
  spd_t_contract_cat_codes  contract_cat_code,
  spd_t_category_codes      category_code
where 
  contract_cat_code.categorycodeid = category_code.categorycodeid
 ;
