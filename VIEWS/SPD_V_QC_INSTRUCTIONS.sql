--------------------------------------------------------
--  DDL for View SPD_V_QC_INSTRUCTIONS
--------------------------------------------------------

  CREATE OR REPLACE FORCE EDITIONABLE VIEW "SPIDERS_DATA"."SPD_V_QC_INSTRUCTIONS" ("INSTRUCTIONID", "INSTRUCTION", "DESCRIPTION", "GROUPING", "VISUAL_ORDER", "BASELINE_ADJUSTMENT", "PWSID") AS 
  select 
  instruction.INSTRUCTIONID       INSTRUCTIONID,
  instruction.INSTRUCTION         INSTRUCTION,
  instruction.DESCRIPTION         DESCRIPTION,
  instruction.GROUPING            GROUPING,
  instruction.VISUAL_ORDER        VISUAL_ORDER,
  instruction.BASELINE_ADJUSTMENT BASELINE_ADJUSTMENT,
  pws.PWSID                       PWSID
from 
  spd_t_instructions instruction,
  spd_t_pwss         pws
where
  pws.pwsid = instruction.pwsid 
ORDER BY 
  GROUPING ASC,
  VISUAL_ORDER ASC
 ;
