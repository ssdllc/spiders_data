--------------------------------------------------------
--  DDL for View SPD_V_QC_FM_CARD
--------------------------------------------------------
    
CREATE OR REPLACE VIEW "SPIDERS_DATA"."SPD_V_QC_FM_CARD" AS 
/* SELECT * FROM (
    SELECT
          m.FMMOORINGID,
          m.ACTIVITIESID,
          m.mooring_name,
          a.report_date,
          act.SPD_ACTIVITY_NAME,
          act.SPECIAL_AREA_NAME,
          act.SPECIAL_AREA_CODE,
          am.BUOY_TYPE,
          am.ANCHOR_TYPE,
          am.INSPECTION_DATE,
          am.MOORING_CONDITION_READINESS,
          am.MOORING_RATED_CONDITION,
          row_number() over (partition by m.FMMOORINGID order by a.REPORT_DATE DESC) rnm
    FROM
        SPD_T_FM_MOORINGS m,
        SPD_T_FM_ACTION_MOORINGS am,
        SPD_T_FM_ACTIONS a,
        spd_v_qc_act_with_spec_areas act
    WHERE
        m.FMMOORINGID = am.FMMOORINGID
        and am.FMACTIONID = a.FMACTIONID
        and m.activitiesid = act.activitiesid
        and m.special_area_code = act.special_area_code
  ) where rnm = 1; */
  SELECT * FROM SPD_V_DT_FM_MOORINGS;
