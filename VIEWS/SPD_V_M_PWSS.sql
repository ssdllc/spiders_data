--------------------------------------------------------
--  DDL for View SPD_V_M_PWSS
--------------------------------------------------------

  CREATE OR REPLACE FORCE EDITIONABLE VIEW "SPIDERS_DATA"."SPD_V_M_PWSS" ("PWSID", "CONTRACTID", "PWS_TYPE", "PWS_DESCRIPTION") AS 
  select 
      PWSID,CONTRACTID,PWS_TYPE,PWS_DESCRIPTION
    from 
    SPD_T_PWSS
 ;
