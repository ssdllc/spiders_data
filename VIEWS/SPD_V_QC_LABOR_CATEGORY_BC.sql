--------------------------------------------------------
--  DDL for View SPD_V_QC_LABOR_CATEGORY_BC
--------------------------------------------------------

  CREATE OR REPLACE FORCE EDITIONABLE VIEW "SPIDERS_DATA"."SPD_V_QC_LABOR_CATEGORY_BC" ("LABORCATEGORYID", "PWSID", "CONTRACTID") AS 
  select 
  labor_category.LABORCATEGORYID    LABORCATEGORYID,
  labor_category.PWSID              PWSID,
  pws.CONTRACTID                    CONTRACTID 
from 
  spd_t_labor_categories  labor_category,
  spd_t_pwss              pws
where
  labor_category.pwsid = pws.pwsid
ORDER BY 
  LABORCATEGORYID ASC
 ;
