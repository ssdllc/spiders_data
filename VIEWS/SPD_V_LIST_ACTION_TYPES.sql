--------------------------------------------------------
--  DDL for View SPD_V_LIST_ACTION_TYPES
--------------------------------------------------------

  CREATE OR REPLACE FORCE EDITIONABLE VIEW "SPIDERS_DATA"."SPD_V_LIST_ACTION_TYPES" ("LISTID", "DISPLAYVALUE", "RETURNVALUE", "GROUPING", "LISTNAME", "VISUALORDER", "PRODUCTLINE_LISTID", "NOTES") AS 
  select listid, displayvalue, returnvalue, grouping, listname, visualorder, productline_listid, notes from spd_t_lists where listname = 'list_planned_action_types' order by visualorder
 
 ;
