--------------------------------------------------------
--  DDL for View SPD_V_QC_DRYDOCKS
--------------------------------------------------------

  CREATE OR REPLACE FORCE EDITIONABLE VIEW "SPIDERS_DATA"."SPD_V_QC_DRYDOCKS" ("FACID", "INFADS_FACILITY_ID", "PRODUCT_LINE_LISTID", "SPD_FAC_NAME", "SPD_FAC_NO", "FILESKEY", "COUNT_OF_CSV_FILES", "ACTIVITY_TITLE") AS 
  select
      FACID, 
      INFADS_FACILITY_ID, 
      PRODUCT_LINE_LISTID, 
      SPD_FAC_NAME, 
      SPD_FAC_NO, 
      FILESKEY,
      (select
            count(xmlfileid)
        from
            spd_t_xml_files xmlf,
            spd_t_inter_files inter
        where
            inter.FILESKEY = fac.fileskey
            and xmlf.xmlfileid = inter.filesid) count_of_csv_files,
      activity.ACTIVITY_TITLE
  from
      spd_t_facilities fac,
      spd_mv_inf_activity activity
  where
      fac.SPD_UIC = activity.UIC;
