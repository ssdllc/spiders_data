--------------------------------------------------------
--  DDL for View SPD_V_M_PLANS
--------------------------------------------------------

  CREATE OR REPLACE FORCE EDITIONABLE VIEW "SPIDERS_DATA"."SPD_V_M_PLANS" ("PLANID", "PRODUCTLINEID", "FUNDSOURCE_LISTID", "TITLE", "DESCRIPTION", "FY") AS 
  select 
      PLANID,PRODUCTLINEID,FUNDSOURCE_LISTID,TITLE,DESCRIPTION,FY
    from 
    SPD_T_PLANS;
