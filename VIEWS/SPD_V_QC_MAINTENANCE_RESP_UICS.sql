--------------------------------------------------------
--  DDL for View SPD_V_QC_MAINTENANCE_RESP_UICS
--------------------------------------------------------

  CREATE OR REPLACE FORCE EDITIONABLE VIEW "SPIDERS_DATA"."SPD_V_QC_MAINTENANCE_RESP_UICS" ("DISPLAY_VALUE") AS 
  select distinct(MAINTENANCE_RESP_UIC) DISPLAY_VALUE from SPD_MV_DATA_ANALYSIS where rownum < 200 order by lower(MAINTENANCE_RESP_UIC)
 ;
