
  CREATE OR REPLACE FORCE EDITIONABLE VIEW "SPIDERS_DATA"."SPD_V_QC_DELIVERY_ORDER_FACS" ("DELIVERYORDERFACILITYID", "DELIVERYORDERID", "FACID", "NFAID", "SPIDERS_FACILITY_NAME", "SPIDERS_FACILITY_NUMBER", "FACILITY_NAME", "FACILITY_NUMBER", "ACTIVITY", "UIC", "CATEGORY_CODE", "STATE", "INSPECT", "FACILITY_INSTR_COUNT") AS 
  select 
  delivery_order_fac.DELIVERYORDERFACILITYID    DELIVERYORDERFACILITYID,
  delivery_order_fac.DELIVERYORDERID            DELIVERYORDERID,
  delivery_order_fac.FACID                      FACID,
  facility.facility_id                          NFAID,
  DECODE(facility.SPIDERS_FACNAME,null,fac.spd_fac_name,facility.SPIDERS_FACNAME)  SPIDERS_FACILITY_NAME,
  facility.SPIDERS_FACNO                        SPIDERS_FACILITY_NUMBER,
  DECODE(facility.FACILITY_NAME,null,fac.spd_fac_name,facility.FACILITY_NAME)  FACILITY_NAME,
  facility.FACILITY_NO                          FACILITY_NUMBER,
  facility.UNIT_TITLE                           ACTIVITY,
  facility.ACTIVITY_UIC                         UIC,
  facility.PRIME_USE_CATEGORY_CODE              CATEGORY_CODE,
  facility.STATE_ABBR                           STATE,
  delivery_order_fac.INSPECT                    INSPECT, 
(select count(a.dofacinstructionid) from spd_t_do_fac_instructions a where a.dofacid = delivery_order_fac.DELIVERYORDERFACILITYID) + 
(select count(*) from spd_t_xml_facilities where deliveryorderfacilityid = delivery_order_fac.DOFID) FACILITY_INSTR_COUNT
  
from 
  spd_t_delivery_order_facs delivery_order_fac,
  spd_v_dt_facilities_source facility,
  spd_t_facilities fac
where
  delivery_order_fac.facid = fac.facid and
  delivery_order_fac.facid = facility.facid(+)
ORDER BY 
  FACILITY_NAME ASC;
