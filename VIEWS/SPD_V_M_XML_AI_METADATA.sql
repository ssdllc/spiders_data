--------------------------------------------------------
--  DDL for View SPD_V_M_XML_AI_METADATA
--------------------------------------------------------

  CREATE OR REPLACE FORCE EDITIONABLE VIEW "SPIDERS_DATA"."SPD_V_M_XML_AI_METADATA" ("METADATAID", "INVENTORYID", "DISPLAY", "VALUE", "VISUALORDER") AS 
  select 
      METADATAID,INVENTORYID,DISPLAY,VALUE,VISUALORDER
    from 
    SPD_T_XML_AI_METADATA
 ;
