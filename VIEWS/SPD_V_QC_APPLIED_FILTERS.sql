--------------------------------------------------------
--  DDL for View SPD_V_QC_APPLIED_FILTERS
--------------------------------------------------------

  CREATE OR REPLACE FORCE EDITIONABLE VIEW "SPIDERS_DATA"."SPD_V_QC_APPLIED_FILTERS" ("FILTERSETID", "ATTRIBUTE_NAME", "ATTRIBUTEID", "GROUP_NAME", "TYPE", "RANGE_LOW", "RANGE_HIGH", "SELECTED_VALUE_LISTS", "SELECTED_FILTERIDS") AS 
  select f.filtersetid, a.name ATTRIBUTE_NAME, f.attributeid, a.group_name,
  a.type,f.range_low,f.range_high,
  LISTAGG(f.value_list, ',') WITHIN GROUP (ORDER BY f.value_list) AS SELECTED_VALUE_LISTS, --, f.attributeid
  LISTAGG(f.filterid, ',') WITHIN GROUP (ORDER BY f.value_list) AS SELECTED_FILTERIDS
  from spd_t_filters f , spd_t_attributes a
  where f.attributeid=a.attributeid
  group by f.filtersetid,f.range_low,f.range_high, f.attributeid, a.name, a.group_name, a.type
 ;
