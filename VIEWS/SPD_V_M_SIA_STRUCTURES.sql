--------------------------------------------------------
--  DDL for View SPD_V_M_SIA_STRUCTURES
--------------------------------------------------------

  CREATE OR REPLACE FORCE EDITIONABLE VIEW "SPIDERS_DATA"."SPD_V_M_SIA_STRUCTURES" ("SIASTRUCTUREID", "ADOID", "DOFID") AS 
  select 
      SIASTRUCTUREID,ADOID,DOFID
    from 
    SPD_T_SIA_STRUCTURES
 ;
