--------------------------------------------------------
--  DDL for View SPD_V_QC_CI_CE_1391_FS_EXISTS
--------------------------------------------------------

  CREATE OR REPLACE FORCE EDITIONABLE VIEW "SPIDERS_DATA"."SPD_V_QC_CI_CE_1391_FS_EXISTS" ("FILE_DESCRIPTION", "DELIVERYORDERFACILITYID", "FILE_NAME", "FACILITYID", "FILE_PATH", "FILE_EXISTS", "BFILEID") AS 
  select 	 FILE_DESCRIPTION
		,DELIVERYORDERFACILITYID
		,FILE_NAME
		,FACILITYID
		,FILE_PATH
		,FILE_EXISTS
		,BFILEID
from (
	select 
	  'fs' FILE_DESCRIPTION,
	  xml_facility_match.dofid DELIVERYORDERFACILITYID, 
	  f_smoke_get_filename(FSFILENAME) FILE_NAME,
	  xml_facility.facilityid FACILITYID,
	  f_smoke_get_file_path(FSFILENAME) FILE_PATH,
	  bfileexists(bfile.the_bfile) FILE_EXISTS,
	  bfile.bfileid BFILEID
	from spd_t_xml_facilities xml_facility, 
		 spd_t_xml_facilities_match xml_facility_match, 
		 spd_t_bfiles bfile
	where xml_facility.nfaid = xml_facility_match.xml_nfaid
	  and bfile.folderpath = f_smoke_get_file_path(xml_facility.FSFILENAME)
	  and bfile.filename = f_smoke_get_filename(xml_facility.FSFILENAME)

	UNION

	select 
	  'costestimate' FILE_DESCRIPTION,
	  xml_facility_match.dofid DELIVERYORDERFACILITYID, 
	  f_smoke_get_filename(COSTESTIMATEFILENAME) FILE_NAME,
	  xml_facility.facilityid FACILITYID,
	  f_smoke_get_file_path(COSTESTIMATEFILENAME) FILE_PATH,
	  bfileexists(bfile.the_bfile) FILE_EXISTS,
	  bfile.bfileid BFILEID
	from spd_t_xml_facilities xml_facility, 
		 spd_t_xml_facilities_match xml_facility_match, 
		 spd_t_bfiles bfile
	where xml_facility.nfaid = xml_facility_match.xml_nfaid
	  and bfile.folderpath = f_smoke_get_file_path(xml_facility.COSTESTIMATEFILENAME)
	  and bfile.filename = f_smoke_get_filename(xml_facility.COSTESTIMATEFILENAME)
  
	UNION

	select 
	  'dd1391' FILE_DESCRIPTION,
	  xml_facility_match.dofid DELIVERYORDERFACILITYID, 
	  f_smoke_get_filename(DD1391FILENAME) FILE_NAME,
	  xml_facility.facilityid FACILITYID,
	  f_smoke_get_file_path(DD1391FILENAME) FILE_PATH,
	  bfileexists(bfile.the_bfile) FILE_EXISTS,
	  bfile.bfileid BFILEID
	from spd_t_xml_facilities xml_facility, 
		 spd_t_xml_facilities_match xml_facility_match, 
		 spd_t_bfiles bfile
	where xml_facility.nfaid = xml_facility_match.xml_nfaid
	  and bfile.folderpath = f_smoke_get_file_path(xml_facility.DD1391FILENAME)
	  and bfile.filename = f_smoke_get_filename(xml_facility.DD1391FILENAME)
)
 ;
