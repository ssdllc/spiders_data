--------------------------------------------------------
--  DDL for View SPD_V_M_FILTER_SETS
--------------------------------------------------------

  CREATE OR REPLACE FORCE EDITIONABLE VIEW "SPIDERS_DATA"."SPD_V_M_FILTER_SETS" ("FILTERSETID", "NAME", "DESCRIPTION", "DUE_DATE", "REQUESTOR") AS 
  select 
      FILTERSETID,NAME,DESCRIPTION,DUE_DATE,REQUESTOR
    from 
    SPD_T_FILTER_SETS
 ;
