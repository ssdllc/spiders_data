--------------------------------------------------------
--  DDL for View SPD_V_M_INSTRUCTIONS
--------------------------------------------------------

  CREATE OR REPLACE FORCE EDITIONABLE VIEW "SPIDERS_DATA"."SPD_V_M_INSTRUCTIONS" ("INSTRUCTIONID", "GROUPING", "VISUAL_ORDER", "INSTRUCTION", "DESCRIPTION", "BASELINE_ADJUSTMENT", "PWSID") AS 
  select 
      INSTRUCTIONID,GROUPING,VISUAL_ORDER,INSTRUCTION,DESCRIPTION,BASELINE_ADJUSTMENT,PWSID
    from 
    SPD_T_INSTRUCTIONS
 ;
