CREATE OR REPLACE VIEW "SPIDERS_DATA"."SPD_V_QC_APEX_USER_PERMISSION" AS
select sum(user_permission) as user_permission from 
(
select 1 as grouper, user_permission from spd_t_users where upper(user_email)=upper(v('APP_USER')) and rownum = 1
UNION
select 1 as grouper, 0 as user_permission from dual
) group by grouper