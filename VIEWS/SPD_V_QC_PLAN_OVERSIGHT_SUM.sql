--------------------------------------------------------
--  DDL for View SPD_V_QC_PLAN_OVERSIGHT_SUM
--------------------------------------------------------

  CREATE OR REPLACE FORCE EDITIONABLE VIEW "SPIDERS_DATA"."SPD_V_QC_PLAN_OVERSIGHT_SUM" ("AMOUNT_TYPE", "TYPE", "OVERSIGHT_AMOUNT", "PLANID") AS 
  select 'TOTAL' 			AMOUNT_TYPE,
  		 'ESTIMATED' TYPE,
		sum(po.AMOUNT) 	OVERSIGHT_AMOUNT,
    po.PLANID
from SPD_T_PLAN_OVERSIGHT po
where po.TYPE = 'ESTIMATED'
group by po.PLANID
UNION
select 'Q'||po.QTR  	AMOUNT_TYPE,
		'ESTIMATED' TYPE,
		sum(po.AMOUNT) 	OVERSIGHT_AMOUNT,
    po.PLANID
from SPD_T_PLAN_OVERSIGHT po
where po.TYPE = 'ESTIMATED'
group by po.PLANID, po.QTR
UNION
select 'TOTAL' 			AMOUNT_TYPE,
  		'RECEIVED' TYPE,
		sum(po.AMOUNT) 	OVERSIGHT_AMOUNT,
    po.PLANID
from SPD_T_PLAN_OVERSIGHT po
where po.TYPE = 'RECEIVED'
group by po.PLANID
UNION
select 'Q'||po.QTR  	AMOUNT_TYPE,
		'RECEIVED' TYPE,
		sum(po.AMOUNT) 	OVERSIGHT_AMOUNT,
    po.PLANID
from SPD_T_PLAN_OVERSIGHT po
where po.TYPE = 'RECEIVED'
group by po.PLANID, po.QTR;
