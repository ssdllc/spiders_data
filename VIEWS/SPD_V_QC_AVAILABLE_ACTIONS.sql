--------------------------------------------------------
--  DDL for View SPD_V_QC_AVAILABLE_ACTIONS
--------------------------------------------------------

  CREATE OR REPLACE FORCE EDITIONABLE VIEW "SPIDERS_DATA"."SPD_V_QC_AVAILABLE_ACTIONS" ("ACTIONID", "TITLE", "FY", "QTR", "ACTIVITY", "STATE", "FUND_SOURCE", "PRODUCTLINEID", "EST_INHOUSE", "EST_KTR") AS 
  select
	a.ACTIONID,
	a.TITLE,
	a.FY,
	a.QTR,
	act.SPD_ACTIVITY_NAME ACTIVITY,
	act.STATE,
	l.DISPLAYVALUE 		FUND_SOURCE,
	a.PRODUCTLINEID,
	a.INHOUSE_AMT	 	EST_INHOUSE,
	a.KTR_AMT		 	EST_KTR
from SPD_T_ACTIONS a,
	 spd_t_activities act,
	 SPD_T_PLAN_ACTIONS pa,
	 spd_t_lists l
where a.ACTIVITIESID = act.ACTIVITIESID and
	  a.ACTIONID = pa.ACTIONID(+) and
	  a.FUNDSOURCE_LISTID = l.LISTID and
	  pa.PLANACTIONID is null;
