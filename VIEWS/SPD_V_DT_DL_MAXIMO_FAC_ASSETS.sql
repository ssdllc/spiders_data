--------------------------------------------------------
--  DDL for View SPD_V_DT_DL_MAXIMO_FAC_ASSETS
--------------------------------------------------------

  CREATE OR REPLACE FORCE EDITIONABLE VIEW "SPIDERS_DATA"."SPD_V_DT_DL_MAXIMO_FAC_ASSETS" ("SPIDERSFACASSETSID", "FACILITY_ID", "FACID", "SPIDERSID", "DELIVERYORDERID", "DELIVERYORDERFACILITYID", "MAXIMOASSET", "ASSETNUM", "CHANGEDATE", "DESCRIPTION", "LONG_DESC", "MASTERSYSTEM", "SYSTEM", "SUBSYSTEM", "LOCATION", "SITEID", "WORKCENTER", "ASSETTYPE", "QUANTITY", "INVENTORY", "PURCHASEPRICE", "BUDGETCOST", "REPLACEMENTCOST", "METERGROUP", "ORGID") AS 
  select 
  spiders_fac_asset.spidersfacassetsid               SPIDERSFACASSETSID,
  sip_location_by_nfa.facilityid                     FACILITY_ID,
  facility.facid                                     FACID ,
  spiders_fac_asset.SPIDERSID                        SPIDERSID,
  spiders_fac_asset.DELIVERYORDERID                  DELIVERYORDERID ,       
  spiders_fac_asset.DELIVERYORDERFACILITYID          DELIVERYORDERFACILITYID  ,      
  spiders_fac_asset.MAXIMOASSET                      MAXIMOASSET ,
  spiders_fac_asset.ASSETNUM                         ASSETNUM,
  spiders_fac_asset.CHANGEDATE                       CHANGEDATE  ,        
  spiders_fac_asset.DESCRIPTION                      DESCRIPTION ,
  spiders_fac_asset.LONG_DESC                        LONG_DESC ,
  spiders_fac_asset.MASTERSYSTEM                     MASTERSYSTEM ,
  spiders_fac_asset.SYSTEM                           SYSTEM ,
  spiders_fac_asset.SUBSYSTEM                        SUBSYSTEM ,
  spiders_fac_asset.LOCATION                         LOCATION ,
  spiders_fac_asset.SITEID                           SITEID      ,  
  spiders_fac_asset.WORKCENTER                       WORKCENTER ,
  spiders_fac_asset.ASSETTYPE                        ASSETTYPE ,
  spiders_fac_asset.QUANTITY                         QUANTITY  ,      
  spiders_fac_asset.INVENTORY                        INVENTORY ,
  spiders_fac_asset.PURCHASEPRICE                    PURCHASEPRICE,        
  spiders_fac_asset.BUDGETCOST                       BUDGETCOST   ,     
  spiders_fac_asset.REPLACEMENTCOST                  REPLACEMENTCOST ,       
  spiders_fac_asset.METERGROUP                       METERGROUP ,
  spiders_fac_asset.ORGID                            ORGID
from
  spd_t_spiders_fac_assets    spiders_fac_asset,
  spd_t_sip_locations_by_nfa  sip_location_by_nfa,
  spd_t_facilities            facility
where
  (spiders_fac_asset.location = sip_location_by_nfa.location and
  spiders_fac_asset.siteid = sip_location_by_nfa.loc_site) and
  facility.infads_facility_id = sip_location_by_nfa.facilityid
 ;
