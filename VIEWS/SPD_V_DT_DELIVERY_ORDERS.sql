--------------------------------------------------------
--  DDL for View SPD_V_DT_DELIVERY_ORDERS
--------------------------------------------------------

  CREATE OR REPLACE FORCE EDITIONABLE VIEW "SPIDERS_DATA"."SPD_V_DT_DELIVERY_ORDERS" ("DELIVERYORDERID", "PWSID", "ONSITE_START_DATE", "ONSITE_END_DATE", "DELIVERY_ORDER_NUMBER", "KTR_SUBMITTAL_DATE", "SPIDERS_DATA_VERSION", "DELIVERY_ORDER_NAME", "PRODUCTLINE", "CONTRACT_NAME", "CONTRACT_NUMBER", "REGION", "COMMAND_NAME", "EIC", "MAIN_LOCATION", "MAIN_LOCATION_UIC", "PROJECTED_START_DATE", "FUNDSOURCE", "COUNT_OF_FACILITIES", "PWS_TYPE", "FY", "ADOID", "COUNT_OF_TEMPLATES", "CONTRACTID") AS 
  select
  delivery_order.DELIVERYORDERID        DELIVERYORDERID,
  delivery_order.PWSID                  PWSID,
  delivery_order.ONSITE_START_DATE      ONSITE_START_DATE,
  delivery_order.ONSITE_END_DATE        ONSITE_END_DATE,
  delivery_order.DELIVERY_ORDER_NUMBER  DELIVERY_ORDER_NUMBER,
  delivery_order.KTR_SUBMITTAL_DATE     KTR_SUBMITTAL_DATE,
  delivery_order.SPIDERS_DATA_VERSION   SPIDERS_DATA_VERSION,
  delivery_order.DELIVERY_ORDER_NAME    DELIVERY_ORDER_NAME,
  productline.PRODUCTLINE_DISPLAY       PRODUCTLINE,
  contract.CONTRACT_NAME                CONTRACT_NAME,
  contract.CONTRACT_NUMBER              CONTRACT_NUMBER,
  contract.REGION                       REGION,
  delivery_order.COMMAND_NAME           COMMAND_NAME,
  delivery_order.EIC                    EIC,
  delivery_order.MAIN_LOCATION          MAIN_LOCATION,
  delivery_order.MAIN_LOCATION_UIC      MAIN_LOCATION_UIC,
  delivery_order.PROJECTED_START_DATE   PROJECTED_START_DATE,
  delivery_order.FUNDSOURCE             FUNDSOURCE,
  (select count(*) from spd_t_delivery_order_facs where DELIVERYORDERID = delivery_order.DELIVERYORDERID) COUNT_OF_FACILITIES,
  -- (select count(dof.dofid) from spd_t_do_facilities dof where dof.ADOID = delivery_order.ADOID) COUNT_OF_FACILITIES,
  pwss.PWS_TYPE PWS_TYPE,
  to_char(add_months(delivery_order.ONSITE_START_DATE,3),'YYYY') FY,
  delivery_order.ADOID ADOID,
  (select count(*) from spd_t_pws_template_maps where PWSID = delivery_order.PWSID) COUNT_OF_TEMPLATES,
  contract.contractid
from
  spd_t_delivery_orders delivery_order,
  spd_t_contracts contract,
  --spd_t_lists display,
  spd_t_productlines  productline,
  spd_t_pwss pwss
where
  delivery_order.pwsid = pwss.pwsid
  and pwss.contractid = contract.contractid
  and contract.productline_listid = productline.productlineid;
