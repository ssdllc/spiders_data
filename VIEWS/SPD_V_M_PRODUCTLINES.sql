--------------------------------------------------------
--  DDL for View SPD_V_M_PRODUCTLINES
--------------------------------------------------------

  CREATE OR REPLACE FORCE EDITIONABLE VIEW "SPIDERS_DATA"."SPD_V_M_PRODUCTLINES" ("PRODUCTLINEID", "PRODUCTLINE_DISPLAY", "PRODUCTLINE_DESCRIPTION") AS 
  select 
      PRODUCTLINEID,PRODUCTLINE_DISPLAY,PRODUCTLINE_DESCRIPTION
    from 
    SPD_T_PRODUCTLINES
 ;
