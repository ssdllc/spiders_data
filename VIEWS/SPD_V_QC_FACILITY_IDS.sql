--------------------------------------------------------
--  DDL for View SPD_V_QC_FACILITY_IDS
--------------------------------------------------------

  CREATE OR REPLACE FORCE EDITIONABLE VIEW "SPIDERS_DATA"."SPD_V_QC_FACILITY_IDS" ("DISPLAY_VALUE") AS 
  select distinct(FACILITY_ID) DISPLAY_VALUE from SPIDERS_DATA.SPD_MV_DATA_ANALYSIS where rownum < 200 order by lower(FACILITY_ID)
 ;
