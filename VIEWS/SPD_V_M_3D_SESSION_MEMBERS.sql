--------------------------------------------------------
--  DDL for View SPD_V_M_3D_SESSION_MEMBERS
--------------------------------------------------------

  CREATE OR REPLACE FORCE EDITIONABLE VIEW "SPIDERS_DATA"."SPD_V_M_3D_SESSION_MEMBERS" ("SESSIONMEMBERID", "USERNAME") AS 
  select 
      SESSIONMEMBERID,USERNAME
    from 
    SPD_T_3D_SESSION_MEMBERS
 ;
