--------------------------------------------------------
--  DDL for View SPD_V_QC_FACILITY_NOS
--------------------------------------------------------

  CREATE OR REPLACE FORCE EDITIONABLE VIEW "SPIDERS_DATA"."SPD_V_QC_FACILITY_NOS" ("DISPLAY_VALUE") AS 
  select distinct(FACILITY_NO) DISPLAY_VALUE from SPD_MV_DATA_ANALYSIS order by lower(FACILITY_NO)
 ;
