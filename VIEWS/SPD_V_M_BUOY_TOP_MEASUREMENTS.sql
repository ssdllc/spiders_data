--------------------------------------------------------
--  DDL for View SPD_V_M_BUOY_TOP_MEASUREMENTS
--------------------------------------------------------

  CREATE OR REPLACE FORCE EDITIONABLE VIEW "SPIDERS_DATA"."SPD_V_M_BUOY_TOP_MEASUREMENTS" ("BUOYTOPMEASUREMENTID", "FMIMPORTID", "ITEM", "INSPECTION_POINT", "CATALOG_VALUE", "CALIPER_RESULTS", "CONDITION") AS 
  select 
      BUOYTOPMEASUREMENTID,FMIMPORTID,ITEM,INSPECTION_POINT,CATALOG_VALUE,CALIPER_RESULTS,CONDITION
    from 
    SPD_T_BUOY_TOP_MEASUREMENTS
 ;
