--------------------------------------------------------
--  DDL for View SPD_V_LIST_PRODUCT_LINES
--------------------------------------------------------

  CREATE OR REPLACE FORCE EDITIONABLE VIEW "SPIDERS_DATA"."SPD_V_LIST_PRODUCT_LINES" ("LISTID", "DISPLAYVALUE", "RETURNVALUE", "GROUPING", "LISTNAME", "VISUALORDER", "PRODUCTLINE_LISTID", "NOTES") AS 
  select listid, displayvalue, returnvalue, grouping, listname, visualorder, productline_listid, notes from spd_t_lists where listname = 'list_product_lines' order by visualorder
 
 ;
