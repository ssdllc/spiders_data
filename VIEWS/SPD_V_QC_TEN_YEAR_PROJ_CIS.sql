--------------------------------------------------------
--  DDL for View SPD_V_QC_TEN_YEAR_PROJ_CIS
--------------------------------------------------------

  CREATE OR REPLACE FORCE EDITIONABLE VIEW "SPIDERS_DATA"."SPD_V_QC_TEN_YEAR_PROJ_CIS" ("DISPLAY_VALUE") AS 
  select distinct(TEN_YEAR_PROJ_CI) DISPLAY_VALUE from SPD_MV_DATA_ANALYSIS where rownum < 200 order by to_number(TEN_YEAR_PROJ_CI)
 ;
