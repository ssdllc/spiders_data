--------------------------------------------------------
--  DDL for View SPD_V_M_MAXIMO_AF_ASSETS
--------------------------------------------------------

  CREATE OR REPLACE FORCE EDITIONABLE VIEW "SPIDERS_DATA"."SPD_V_M_MAXIMO_AF_ASSETS" ("MAXIMOAFASSETID", "NETWORK_ID", "BRANCH_NAME", "BRANCH_USE", "BRANCH_ID", "SECTION_ID", "SURFACE_TYPE", "TRUE_AREA", "LAST_INSPECTION_DATE", "PCI", "ASSET_NUMBER", "UNIFORMAT_SUBSYSTEM", "CHANGE_DESCRIPTION", "LEGACY_ASSET_NUMBER", "SITE_CODE", "FACILITY_ID", "CAT_CODE_NUMBER", "TIMESTAMP", "USERNAME") AS 
  select 
      MAXIMOAFASSETID,NETWORK_ID,BRANCH_NAME,BRANCH_USE,BRANCH_ID,SECTION_ID,SURFACE_TYPE,TRUE_AREA,LAST_INSPECTION_DATE,PCI,ASSET_NUMBER,UNIFORMAT_SUBSYSTEM,CHANGE_DESCRIPTION,LEGACY_ASSET_NUMBER,SITE_CODE,FACILITY_ID,CAT_CODE_NUMBER,TIMESTAMP,USERNAME
    from 
    SPD_T_MAXIMO_AF_ASSETS
 ;
