--------------------------------------------------------
--  DDL for View SPD_V_QC_CONTRACT_CONTRACTORS
--------------------------------------------------------

  CREATE OR REPLACE FORCE EDITIONABLE VIEW "SPIDERS_DATA"."SPD_V_QC_CONTRACT_CONTRACTORS" ("CONTRACTCONTRACTORID", "CONTRACTORID", "CONTRACTID", "CONTRACTOR_NAME", "CITY", "STATE") AS 
  SELECT 
    contract_contractor.contractcontractorid,
    contractor.contractorid,
    contract_contractor.contractid,
    contractor.contractor_name,
    contractor.city,
    contractor.state
FROM 
    spd_t_contractors contractor,
    spd_t_contract_contractors contract_contractor
WHERE
    contractor.contractorid = contract_contractor.contractorid
 ;
