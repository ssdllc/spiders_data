--------------------------------------------------------
--  DDL for View SPD_V_QC_PLAN_OVERSIGHT
--------------------------------------------------------

  CREATE OR REPLACE FORCE EDITIONABLE VIEW "SPIDERS_DATA"."SPD_V_QC_PLAN_OVERSIGHT" ("PLANOVERSIGHTID", "PLANID", "QTR", "AMOUNT", "DESCRIPTION", "TYPE") AS 
  SELECT PLANOVERSIGHTID,
       PLANID,
       QTR,
       AMOUNT,
       DESCRIPTION,
       TYPE
FROM SPD_T_PLAN_OVERSIGHT
ORDER BY QTR ASC;
