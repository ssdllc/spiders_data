--------------------------------------------------------
--  DDL for View SPD_V_ASSET_SUMMARY_INVENTORY
--------------------------------------------------------

  CREATE OR REPLACE FORCE EDITIONABLE VIEW "SPIDERS_DATA"."SPD_V_ASSET_SUMMARY_INVENTORY" ("LINEITEMID", "FACILITYID", "UNIFORMATIICODE", "DESCRIPTION", "ASSETTYPE", "FACILITYSECTION", "CRITICALITYPERCENTAGE", "NOOFASSETSCOUNT", "QUANTITYOFUNITS", "UNITTYPE", "CURRENTCI", "APPROXIMATEREPAIRCOSTS", "FIVEYEARCIIFREPAIRED", "TENYEARCIIFREPAIRED", "FIVEYEARCIIFNOTREPAIRED", "TENYEARCIIFNOTREPAIRED", "INVENTORYID", "AI_LINEITEMID", "AI_UNIFORMATIICODE", "UNIFORMATIINAME", "AI_ASSETTYPE", "ASSETTYPEDESCRIPTION", "AI_FACILITYSECTION", "CRITICALITYFACTOR", "AI_NOOFASSETSCOUNT", "AI_QUANTITYOFUNITS", "AI_UNITTYPE", "MATERIAL", "MATERAILDESCRIPTION") AS 
  select 
      asset_summary.LINEITEMID,
      asset_summary.FACILITYID,
      asset_summary.UNIFORMATIICODE,
      asset_summary.DESCRIPTION,
      asset_summary.ASSETTYPE,
      asset_summary.FACILITYSECTION,
      asset_summary.CRITICALITYPERCENTAGE,
      asset_summary.NOOFASSETSCOUNT,
      asset_summary.QUANTITYOFUNITS,
      asset_summary.UNITTYPE,
      asset_summary.CURRENTCI,
      asset_summary.APPROXIMATEREPAIRCOSTS,
      asset_summary.FIVEYEARCIIFREPAIRED,
      asset_summary.TENYEARCIIFREPAIRED,
      asset_summary.FIVEYEARCIIFNOTREPAIRED,
      asset_summary.TENYEARCIIFNOTREPAIRED,
      asset_inventory.INVENTORYID,
      asset_inventory.LINEITEMID        AI_LINEITEMID,
      asset_inventory.UNIFORMATIICODE   AI_UNIFORMATIICODE,
      asset_inventory.UNIFORMATIINAME,
      asset_inventory.ASSETTYPE         AI_ASSETTYPE,
      asset_inventory.ASSETTYPEDESCRIPTION,
      asset_inventory.FACILITYSECTION   AI_FACILITYSECTION,
      asset_inventory.CRITICALITYFACTOR,
      asset_inventory.NOOFASSETSCOUNT   AI_NOOFASSETSCOUNT,
      asset_inventory.QUANTITYOFUNITS   AI_QUANTITYOFUNITS,
      asset_inventory.UNITTYPE          AI_UNITTYPE,
      asset_inventory.MATERIAL,
      asset_inventory.MATERAILDESCRIPTION
    from 
      spd_t_xml_asset_summary   asset_summary,
      spd_t_xml_asset_inventory asset_inventory
    where
      asset_summary.lineitemid = asset_inventory.lineitemid(+)
 ;
