--------------------------------------------------------
--  DDL for View SPD_V_QC_FM_CURRENT_CLASS
--------------------------------------------------------

  CREATE OR REPLACE VIEW "SPIDERS_DATA"."SPD_V_QC_FM_CURRENT_CLASS" AS 
  SELECT DISTINCT
        CURRENT_CLASS
	FROM 
        SPD_T_FM_ACTION_MOORINGS
  WHERE
        CURRENT_CLASS IS NOT NULL
  ORDER BY
        CURRENT_CLASS;