--------------------------------------------------------
--  DDL for View SPD_V_M_PLAN_OVERSIGHT
--------------------------------------------------------

  CREATE OR REPLACE FORCE EDITIONABLE VIEW "SPIDERS_DATA"."SPD_V_M_PLAN_OVERSIGHT" ("PLANOVERSIGHTID", "PLANID", "QTR", "AMOUNT", "DESCRIPTION", "TYPE") AS 
  select
      PLANOVERSIGHTID,PLANID,QTR,AMOUNT,DESCRIPTION,TYPE
    from
    SPD_T_PLAN_OVERSIGHT;
