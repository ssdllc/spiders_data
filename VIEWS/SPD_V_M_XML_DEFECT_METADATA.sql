--------------------------------------------------------
--  DDL for View SPD_V_M_XML_DEFECT_METADATA
--------------------------------------------------------

  CREATE OR REPLACE FORCE EDITIONABLE VIEW "SPIDERS_DATA"."SPD_V_M_XML_DEFECT_METADATA" ("METADATAID", "DEFECTID", "DISPLAY", "VALUE", "VISUALORDER", "CREATED_BY", "CREATED_TIMESTAMP", "UPDATED_BY", "UPDATED_TIMESTAMP") AS 
  select 
 defMeta."METADATAID",defMeta."DEFECTID",defMeta."DISPLAY",defMeta."VALUE",defMeta."VISUALORDER",defMeta."CREATED_BY",defMeta."CREATED_TIMESTAMP",defMeta."UPDATED_BY",defMeta."UPDATED_TIMESTAMP"
from 
 SPD_T_XML_DEFECT_METADATA defMeta
 ;
