--------------------------------------------------------
--  DDL for View SPD_V_M_COMMENT_RESULTS
--------------------------------------------------------

  CREATE OR REPLACE FORCE EDITIONABLE VIEW "SPIDERS_DATA"."SPD_V_M_COMMENT_RESULTS" ("COMMENTRESULTID", "FMIMPORTID", "COMMENT_TYPE", "INSPECTION_COMMENT") AS 
  select 
      COMMENTRESULTID,FMIMPORTID,COMMENT_TYPE,INSPECTION_COMMENT
    from 
    SPD_T_COMMENT_RESULTS
 ;
