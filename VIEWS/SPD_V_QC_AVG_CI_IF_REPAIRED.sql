--------------------------------------------------------
--  DDL for View SPD_V_QC_AVG_CI_IF_REPAIRED
--------------------------------------------------------

  CREATE OR REPLACE FORCE EDITIONABLE VIEW "SPIDERS_DATA"."SPD_V_QC_AVG_CI_IF_REPAIRED" ("UNIFORMATIICODE", "AVGCURRENTCI", "AVGFIVEYEARCIIFREPAIRED", "AVGTENYEARCIIFREPAIRED") AS 
  select   
  xml_asset_summary.UNIFORMATIICODE                      UNIFORMATIICODE,
  round(avg(xml_asset_summary.CURRENTCI),0)              AVGCURRENTCI,
  round(avg(xml_asset_summary.FIVEYEARCIIFREPAIRED),0)   AVGFIVEYEARCIIFREPAIRED,
  round(avg(xml_asset_summary.TENYEARCIIFREPAIRED),0)    AVGTENYEARCIIFREPAIRED
from
  spd_t_xml_asset_summary xml_asset_summary
where
  xml_asset_summary.CURRENTCI is not null
group by
  UNIFORMATIICODE
 ;
