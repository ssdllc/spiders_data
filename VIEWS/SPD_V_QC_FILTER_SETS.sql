--------------------------------------------------------
--  DDL for View SPD_V_QC_FILTER_SETS
--------------------------------------------------------

  CREATE OR REPLACE FORCE EDITIONABLE VIEW "SPIDERS_DATA"."SPD_V_QC_FILTER_SETS" ("FILTERSETID", "FILTERID", "ATTRIBUTEID", "ATTRIBUTE_GROUP", "ATTRIBUTE_NAME", "VALUE_LIST", "RANGE_LOW", "RANGE_HIGH") AS 
  select 
  filter.FILTERSETID    FILTERSETID,
  filter.FILTERID       FILTERID,
  filter.ATTRIBUTEID    ATTRIBUTEID,
  attribute.GROUP_NAME  ATTRIBUTE_GROUP,
  attribute.NAME        ATTRIBUTE_NAME,
  filter.VALUE_LIST     VALUE_LIST,
  filter.RANGE_LOW      RANGE_LOW,
  filter.RANGE_HIGH     RANGE_HIGH 
from 
  spd_t_filters     filter,
  spd_t_attributes  attribute
where
  filter.attributeid = attribute.attributeid
ORDER BY 
  ATTRIBUTE_GROUP ASC,
  ATTRIBUTE_NAME ASC
 ;
