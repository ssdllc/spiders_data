--------------------------------------------------------
--  DDL for View SPD_V_QC_ACTIONS
--------------------------------------------------------

  CREATE OR REPLACE VIEW "SPIDERS_DATA"."SPD_V_QC_FB_FM_ACTIONS" AS 
  SELECT
        NVL(fm_actions.REPORT_NUMBER, 'No Report Title') REPORT_NUMBER,
        fm_actions.REPORT_DATE,
        fm_actions.FMACTIONID,
        fm_moorings.FMMOORINGID
	FROM 
        SPD_T_FM_ACTIONS fm_actions,
        SPD_T_FM_MOORINGS fm_moorings,
        SPD_T_FM_ACTION_MOORINGS fm_action_moorings
  WHERE fm_moorings.FMMOORINGID = fm_action_moorings.FMMOORINGID
  AND fm_actions.FMACTIONID = fm_action_moorings.FMACTIONID;
