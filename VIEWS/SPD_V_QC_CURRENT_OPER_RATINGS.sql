--------------------------------------------------------
--  DDL for View SPD_V_QC_CURRENT_OPER_RATINGS
--------------------------------------------------------

  CREATE OR REPLACE FORCE EDITIONABLE VIEW "SPIDERS_DATA"."SPD_V_QC_CURRENT_OPER_RATINGS" ("DISPLAY_VALUE") AS 
  select distinct(CURRENT_OPER_RATING) DISPLAY_VALUE from SPD_MV_DATA_ANALYSIS order by lower(CURRENT_OPER_RATING)
 ;
