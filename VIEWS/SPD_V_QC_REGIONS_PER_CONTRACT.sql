--------------------------------------------------------
--  DDL for View SPD_V_QC_REGIONS_PER_CONTRACT
--------------------------------------------------------

  CREATE OR REPLACE FORCE EDITIONABLE VIEW "SPIDERS_DATA"."SPD_V_QC_REGIONS_PER_CONTRACT" ("REGIONID", "CONTRACTID", "SHORTNAME") AS 
  SELECT 
    contract.REGIONID,
    contract.CONTRACTID,
    region.SHORTNAME
FROM 
    SPD_T_REGIONS_PER_CONTRACT contract,
    SPD_T_REGIONS region
WHERE
    contract.regionid = region.regionid
ORDER BY
    region.SHORTNAME ASC
 ;
