--------------------------------------------------------
--  DDL for View SPD_V_QC_FM_COND_READINESS
--------------------------------------------------------

  CREATE OR REPLACE VIEW "SPIDERS_DATA"."SPD_V_QC_FM_COND_READINESS" AS 
  SELECT DISTINCT
        MOORING_CONDITION_READINESS
	FROM 
        SPD_T_FM_ACTION_MOORINGS
  WHERE
        MOORING_CONDITION_READINESS IS NOT NULL
  ORDER BY
        MOORING_CONDITION_READINESS;