--------------------------------------------------------
--  DDL for View SPD_V_M_3D_MODEL_CATEGORIES
--------------------------------------------------------

  CREATE OR REPLACE FORCE EDITIONABLE VIEW "SPIDERS_DATA"."SPD_V_M_3D_MODEL_CATEGORIES" ("MODELCATEGORYID", "DESCRIPTION", "IMAGE_FILEPATH", "SHORT_DESCRIPTION") AS 
  select 
      MODELCATEGORYID,DESCRIPTION,IMAGE_FILEPATH,SHORT_DESCRIPTION
    from 
    SPD_T_3D_MODEL_CATEGORIES
 ;
