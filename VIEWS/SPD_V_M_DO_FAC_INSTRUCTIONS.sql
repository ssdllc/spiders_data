--------------------------------------------------------
--  DDL for View SPD_V_M_DO_FAC_INSTRUCTIONS
--------------------------------------------------------

  CREATE OR REPLACE FORCE EDITIONABLE VIEW "SPIDERS_DATA"."SPD_V_M_DO_FAC_INSTRUCTIONS" ("DOFACINSTRUCTIONID", "DOFACID", "INSTRUCTIONID") AS 
  select 
      DOFACINSTRUCTIONID,DOFACID,INSTRUCTIONID
    from 
    SPD_T_DO_FAC_INSTRUCTIONS
 ;
