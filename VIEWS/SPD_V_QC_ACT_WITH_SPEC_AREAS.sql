  CREATE OR REPLACE VIEW "SPIDERS_DATA"."SPD_V_QC_ACT_WITH_SPEC_AREAS" AS

  select * from (
  SELECT
    --(select distinct activitiesid from SPD_T_ACTIVITIES where SPD_UIC = sa.uic and rownum=1) as ACTIVITIESID,
    activities.activitiesid as ACTIVITIESID,
    activities.SPD_ACTIVITY_NAME as SPD_ACTIVITY_NAME,
    sa.special_area_name as SPECIAL_AREA_NAME,
    sa.special_area_code,
    sa.uic as SPD_UIC,
    '' CITY,
    state.description state,
    country.description country
  FROM spd_mv_inf_spec_area sa,
    spd_mv_inf_state_code state,
    spd_mv_inf_country_code country,
    spd_t_activities activities,
    (SELECT distinct activity_uic, special_area_code
    FROM spd_mv_inf_facility
    WHERE special_area_code is not null AND prime_use_category_code IN ('15110', '15120', '15130', '15140', '15150', '15160', '15170', '15171', '15180', '15190', '15210', '15240', '15250', '15260', '15410', '15420', '15430', '15520', '16410', '16420', '21310', '75060', '75061' )
      AND activity_uic IN (select distinct spd_uic from spd_t_activities)
  ) facs
  WHERE sa.UIC = facs.activity_uic
    AND sa.special_area_code = facs.special_area_code
    AND sa.state_code  = state.state_code
  	AND sa.country_code  = country.country_code
  	AND state.country_code = country.country_code
    AND sa.uic = activities.spd_uic
UNION
  SELECT
    act.ACTIVITIESID,
    act.SPD_ACTIVITY_NAME,
    'host' as special_area_name,
    'host' as special_area_code,
    act.SPD_UIC,
    act.CITY,
    act.STATE,
    act.COUNTRY
  FROM SPD_T_ACTIVITIES act
  ) order by state, SPD_ACTIVITY_NAME;