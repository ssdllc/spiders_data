--------------------------------------------------------
--  DDL for View SPD_V_M_XML_FILES
--------------------------------------------------------

  CREATE OR REPLACE FORCE EDITIONABLE VIEW "SPIDERS_DATA"."SPD_V_M_XML_FILES" ("XMLFILEID", "FILENAME", "MIME_TYPE") AS 
  select f.xmlfileid, f.filename, f.mime_type
from spd_t_xml_files f
 ;
