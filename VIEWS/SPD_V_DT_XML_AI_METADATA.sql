--------------------------------------------------------
--  DDL for View SPD_V_DT_XML_AI_METADATA
--------------------------------------------------------

  CREATE OR REPLACE FORCE EDITIONABLE VIEW "SPIDERS_DATA"."SPD_V_DT_XML_AI_METADATA" ("ADOID", "METADATAID", "INVENTORYID", "DISPLAY", "VALUE", "VISUALORDER", "CREATED_BY", "CREATED_TIMESTAMP", "UPDATED_BY", "UPDATED_TIMESTAMP") AS 
  select 
 act.ADOID,x."METADATAID",x."INVENTORYID",x."DISPLAY",x."VALUE",x."VISUALORDER",x."CREATED_BY",x."CREATED_TIMESTAMP",x."UPDATED_BY",x."UPDATED_TIMESTAMP"
from 
 SPD_T_XML_AI_METADATA x
left join SPD_T_XML_ASSET_INVENTORY ainv
 on x.INVENTORYID=ainv.INVENTORYID
left join SPD_T_XML_ASSET_SUMMARY asum
 on ainv.LINEITEMID=asum.LINEITEMID
left join SPD_T_XML_FACILITIES xFac
 on asum.FACILITYID=xFac.FACILITYID
left join SPD_T_XML_ACTIONS act
 on xFac.XMLACTIONID=act.XMLACTIONID
 ;
