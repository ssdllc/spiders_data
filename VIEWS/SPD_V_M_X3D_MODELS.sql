--------------------------------------------------------
--  DDL for View SPD_V_M_X3D_MODELS
--------------------------------------------------------

  CREATE OR REPLACE FORCE EDITIONABLE VIEW "SPIDERS_DATA"."SPD_V_M_X3D_MODELS" ("X3DMODELID", "CLASSNAMEID", "OBJECT_DISPLAY_TEXT", "OBJECT_DESCRIPTION", "SOURCE_INFORMATION", "CONFIDENCE_INFO", "CONFIDENCE_RATING", "MODEL_CORRECT_HEIGHT", "MODEL_CORRECT_SCALE", "MODEL_CORRECT_ORIENTATION", "MODEL_UNDER_2MB", "MODEL_CORRECT_CENTER", "MODEL_INCLUDES_DATA_HOOKS", "MODEL_INCLUDES_ANIMATIONS", "MODEL_INCLUDES_IMAGES", "MODEL_FILEID", "SOURCE_FILEID", "THUMBNAIL_FILEID", "IMAGES_FILEID", "X3D_FILENAME", "X3D_THUMBNAIL") AS 
  select
      X3DMODELID,CLASSNAMEID,OBJECT_DISPLAY_TEXT,OBJECT_DESCRIPTION,SOURCE_INFORMATION,CONFIDENCE_INFO,CONFIDENCE_RATING,MODEL_CORRECT_HEIGHT,MODEL_CORRECT_SCALE,MODEL_CORRECT_ORIENTATION,MODEL_UNDER_2MB,MODEL_CORRECT_CENTER,MODEL_INCLUDES_DATA_HOOKS,MODEL_INCLUDES_ANIMATIONS,MODEL_INCLUDES_IMAGES
     ,MODEL_FILEID, SOURCE_FILEID, THUMBNAIL_FILEID, IMAGES_FILEID, X3D_FILENAME, X3D_THUMBNAIL
from
    SPD_T_X3D_MODELS
 ;
