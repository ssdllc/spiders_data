--------------------------------------------------------
--  DDL for View SPD_V_M_ACTIVITIES
--------------------------------------------------------

  CREATE OR REPLACE FORCE EDITIONABLE VIEW "SPIDERS_DATA"."SPD_V_M_ACTIVITIES" ("ACTIVITIESID", "SPD_ACTIVITY_NAME", "SPD_UIC", "CITY", "STATE", "POCKEY", "FILESKEY", "COUNTRY") AS 
  SELECT 
    "ACTIVITIESID","SPD_ACTIVITY_NAME","SPD_UIC","CITY","STATE","POCKEY","FILESKEY","COUNTRY"
FROM 
    SPD_T_ACTIVITIES
 ;
