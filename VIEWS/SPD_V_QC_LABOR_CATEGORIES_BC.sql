--------------------------------------------------------
--  DDL for View SPD_V_QC_LABOR_CATEGORIES_BC
--------------------------------------------------------

  CREATE OR REPLACE FORCE EDITIONABLE VIEW "SPIDERS_DATA"."SPD_V_QC_LABOR_CATEGORIES_BC" ("PWSID", "CONTRACTID") AS 
  select 
  pws.PWSID       PWSID,
  pws.CONTRACTID  CONTRACTID 
from 
  spd_t_pwss pws  
ORDER BY 
  PWSID ASC
 ;
