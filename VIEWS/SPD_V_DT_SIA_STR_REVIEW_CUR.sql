--------------------------------------------------------
--  DDL for View SPD_V_DT_SIA_STR_REVIEW_CUR
--------------------------------------------------------

  CREATE OR REPLACE FORCE EDITIONABLE VIEW "SPIDERS_DATA"."SPD_V_DT_SIA_STR_REVIEW_CUR" ("FACID", "SIAID", "DOFID", "PREVIOUS_DOFID", "SIASTRUCTUREID", "SIAQUESTIONID", "SIAQUESTION", "SIAGROUP", "SIAQUESTIONNUMBER", "PREVIOUS_VALUE", "CURRENT_VALUE", "PREVIOUS_VALDESCRIPTION", "CURRENT_VALDESCRIPTION", "APPROVED") AS 
  select 
  current_values.facid                    facid,
  current_values.siaid						        siaid,
  current_values.dofid						        dofid,
  null               		                  previous_dofid,
  current_values.siastructureid				    siastructureid,
  current_values.siaquetiondid				    siaquestionid,
  current_values.siaquestion				      siaquestion,
  current_values.siagroup					        siagroup,
  current_values.siaquestionnumber			  siaquestionnumber,
  null				                            previous_value,
  current_values.current_val              current_value,
  null	                                  previous_valdescription,
  current_values.current_valdescription		current_valdescription,
  current_values.approved                 approved
from
(
	select 
    delivery_order_fac.facid,
	  sia_data.siaid                  siaid,
	  sia_structure.dofid             dofid,
	  sia_data.siastructureid         siastructureid,
	  sia_data.siaquestionid          siaquetiondid,
	  sia_question.siaquestion        siaquestion,
	  sia_question.siagroup           siagroup,
	  sia_question.siaquestionnumber  siaquestionnumber,
	  sia_data.value                  current_val,
	  (select description from spd_t_sia_displayvalues dv where dv.siaquestionid = sia_data.siaquestionid and sia_data.value = dv.code) current_valdescription,
    sia_data.approved               approved
	from 
    spd_t_sia_structures      sia_structure,
	  spd_t_sia_data            sia_data,
	  spd_t_sia_questions       sia_question,
    spd_t_delivery_order_facs delivery_order_fac
	where 
    sia_structure.siastructureid = sia_data.siastructureid
	  and sia_question.siaquestionid = sia_data.siaquestionid
    and delivery_order_fac.deliveryorderfacilityid = sia_structure.dofid
) current_values
where
  current_values.dofid is not null
order by
  current_values.siagroup,
  current_values.siaquestionnumber
 ;
