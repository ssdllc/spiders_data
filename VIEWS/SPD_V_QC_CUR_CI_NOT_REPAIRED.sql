--------------------------------------------------------
--  DDL for View SPD_V_QC_CUR_CI_NOT_REPAIRED
--------------------------------------------------------

  CREATE OR REPLACE FORCE EDITIONABLE VIEW "SPIDERS_DATA"."SPD_V_QC_CUR_CI_NOT_REPAIRED" ("MINCURRENTCI", "MINFIVEYEARCIIFREPAIRED", "MINTENYEARCIIFREPAIRED", "UNIFORMATIICODE", "FACILITYID") AS 
  SELECT
  min(xml_asset_summary.CURRENTCI)              MINCURRENTCI,
  min(xml_asset_summary.FIVEYEARCIIFREPAIRED)   MINFIVEYEARCIIFREPAIRED,
  min(xml_asset_summary.TENYEARCIIFREPAIRED)    MINTENYEARCIIFREPAIRED,
  xml_asset_summary.uniformatiicode             UNIFORMATIICODE,
  xml_asset_summary.FACILITYID                  FACILITYID
FROM
  spd_t_xml_asset_summary xml_asset_summary
WHERE
  xml_asset_summary.CURRENTCI is not null
GROUP BY
  UNIFORMATIICODE, FACILITYID
 ;
