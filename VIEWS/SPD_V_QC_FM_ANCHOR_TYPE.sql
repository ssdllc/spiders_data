--------------------------------------------------------
--  DDL for View SPD_V_QC_FM_ANCHOR_TYPE
--------------------------------------------------------

  CREATE OR REPLACE VIEW "SPIDERS_DATA"."SPD_V_QC_FM_ANCHOR_TYPE" AS 
  SELECT DISTINCT
        ANCHOR_TYPE
	FROM 
        SPD_T_FM_ACTION_MOORINGS
  WHERE
        ANCHOR_TYPE IS NOT NULL
  ORDER BY
        ANCHOR_TYPE;