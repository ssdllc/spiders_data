--------------------------------------------------------
--  DDL for View SPD_V_QC_ALL_CONTRACTORS
--------------------------------------------------------

  CREATE OR REPLACE FORCE EDITIONABLE VIEW "SPIDERS_DATA"."SPD_V_QC_ALL_CONTRACTORS" ("CONTRACTORID", "CONTRACTOR_NAME", "CITY", "STATE") AS 
  SELECT 
    contractor.contractorid,
    contractor.contractor_name,
    contractor.city,
    contractor.state
FROM 
    spd_t_contractors contractor
 ;
