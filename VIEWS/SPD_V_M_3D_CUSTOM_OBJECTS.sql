--------------------------------------------------------
--  DDL for View SPD_V_M_3D_CUSTOM_OBJECTS
--------------------------------------------------------

  CREATE OR REPLACE FORCE EDITIONABLE VIEW "SPIDERS_DATA"."SPD_V_M_3D_CUSTOM_OBJECTS" ("CUSTOMOBJECTID", "SESSIONID", "DESCRIPTION", "TYPE", "X3DNODE") AS 
  select
      CUSTOMOBJECTID,SESSIONID,DESCRIPTION,TYPE,X3DNODE
    from
    SPD_T_3D_CUSTOM_OBJECTS;
