--------------------------------------------------------
--  DDL for View SPD_V_M_DATATABLE_SETTINGS
--------------------------------------------------------

  CREATE OR REPLACE FORCE EDITIONABLE VIEW "SPIDERS_DATA"."SPD_V_M_DATATABLE_SETTINGS" ("SETTINGSID", "TARGET", "HIDDENCOLUMNS") AS 
  select 
      SETTINGSID,TARGET,HIDDENCOLUMNS
    from 
    SPD_T_DATATABLE_SETTINGS
 ;
