--------------------------------------------------------
--  DDL for View SPD_V_QC_CONTRACT_CTR_BC
--------------------------------------------------------

  CREATE OR REPLACE FORCE EDITIONABLE VIEW "SPIDERS_DATA"."SPD_V_QC_CONTRACT_CTR_BC" ("CONTRACTCONTRACTORID", "CONTRACTID") AS 
  select 
  contract_contractor.CONTRACTCONTRACTORID  CONTRACTCONTRACTORID,
  contract_contractor.CONTRACTID            CONTRACTID 
from 
  spd_t_contract_contractors contract_contractor  
ORDER BY 
  CONTRACTCONTRACTORID ASC
 ;
