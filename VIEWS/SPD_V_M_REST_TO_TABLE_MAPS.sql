--------------------------------------------------------
--  DDL for View SPD_V_M_REST_TO_TABLE_MAPS
--------------------------------------------------------

  CREATE OR REPLACE FORCE EDITIONABLE VIEW "SPIDERS_DATA"."SPD_V_M_REST_TO_TABLE_MAPS" ("RESTTOTABLEMAPID", "REST_ENDPOINT", "MODULE", "TARGET", "TABLE_NAME") AS 
  select 
      RESTTOTABLEMAPID,REST_ENDPOINT,MODULE,TARGET,TABLE_NAME
    from 
    SPD_T_REST_TO_TABLE_MAPS
 ;
