--------------------------------------------------------
--  DDL for View SPD_V_QC_ATTRIBUTES
--------------------------------------------------------

  CREATE OR REPLACE FORCE EDITIONABLE VIEW "SPIDERS_DATA"."SPD_V_QC_ATTRIBUTES" ("ATTRIBUTEID", "GROUP_NAME", "NAME", "TYPE") AS 
  select 
  attribute.ATTRIBUTEID   ATTRIBUTEID,
  attribute.GROUP_NAME    GROUP_NAME,
  attribute.NAME          NAME,
  attribute.TYPE          TYPE 
from 
  spd_t_attributes attribute  
WHERE 
  attribute.ATTRIBUTEID NOT IN (
    SELECT 
      ATTRIBUTEID 
    FROM
      spd_v_qc_filter_sets
    WHERE
      FILTERSETID = v('SPD_FILTERSETID')
  )
ORDER BY 
  GROUP_NAME ASC,
  NAME ASC
 ;
