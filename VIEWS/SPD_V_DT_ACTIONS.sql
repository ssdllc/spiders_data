--------------------------------------------------------
--  DDL for View SPD_V_DT_ACTIONS
--------------------------------------------------------

  CREATE OR REPLACE VIEW "SPIDERS_DATA"."SPD_V_DT_ACTIONS" ("PRODUCTLINEID", "ACTIVITIESID", "FUNDSOURCE_LISTID", "ACTIONID", "TITLE", "PRODUCTLINE", "STATE", "SPD_ACTIVITY_NAME", "SPECIAL_AREA_NAME", "FY", "FUND_SOURCE", "INHOUSE_AMT", "KTR_AMT", "TASK_ORDER_COUNT", "PLAN_TITLE", "PLANID") AS 
  SELECT
    actions.PRODUCTLINEID,
    actions.ACTIVITIESID,
    actions.FUNDSOURCE_LISTID,
		actions.ACTIONID,
		actions.TITLE,
		pl.PRODUCTLINE_DISPLAY PRODUCTLINE,
		activities.state,
    activities.SPD_ACTIVITY_NAME,
    activities.SPECIAL_AREA_NAME,
		--spd_f_getSpecAreaDisplay(actions.activityid, actions.special_area_code), --THIS WILL BE A NEW FUNCTION
		actions.FY,
		l.DISPLAYVALUE FUND_SOURCE,
		actions.INHOUSE_AMT,
		actions.KTR_AMT,
		(select count(pdo.PLANDELIVERYORDERID)
		 from SPD_T_PLAN_DELIVERY_ORDERS pdo,
          SPD_T_PLAN_ACTIONS pa
		 where pdo.PLANACTIONID = pa.PLANACTIONID
      and pa.ACTIONID=actions.ACTIONID) TASK_ORDER_COUNT,
		(select p.TITLE
		 from spd_t_plans p,
		 spd_t_plan_actions pa
		 where p.PLANID = pa.PLANID
		 and pa.ACTIONID = actions.ACTIONID
		 and rownum = 1) PLAN_TITLE,
		(select p.PLANID
		 from spd_t_plans p,
		 spd_t_plan_actions pa
		 where p.PLANID = pa.PLANID
		 and pa.ACTIONID = actions.ACTIONID
		 and rownum = 1) PLANID
	FROM SPD_T_ACTIONS actions,
		 --spd_t_activities activities,
     spd_v_qc_act_with_spec_areas activities,
		 spd_t_productlines pl,
		 spd_t_lists l
	WHERE actions.ACTIVITIESID = activities.ACTIVITIESID
    and actions.special_area_code = activities.special_area_code
	  and actions.PRODUCTLINEID = pl.PRODUCTLINEID
	  and actions.FUNDSOURCE_LISTID = l.LISTID;