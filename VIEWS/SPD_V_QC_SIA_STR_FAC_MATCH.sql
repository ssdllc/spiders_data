--------------------------------------------------------
--  DDL for View SPD_V_QC_SIA_STR_FAC_MATCH
--------------------------------------------------------

  CREATE OR REPLACE FORCE EDITIONABLE VIEW "SPIDERS_DATA"."SPD_V_QC_SIA_STR_FAC_MATCH" ("DELIVERYORDERID", "DOFID", "NFAID", "FACNAME", "ACTIVITY", "STRUCTURENO", "ADOID") AS 
  select 
  delivery_order_fac.deliveryorderid          deliveryorderid,
  delivery_order_fac.deliveryorderfacilityid  dofid,
  facility.infads_facility_id                 nfaid,
  infads.facility_name                        facname,
  infads.unit_title                           activity,
  
  (
  select 
    sia_data.value 
  from 
    spd_t_sia_data        sia_data,
    spd_t_sia_structures  sia_structure,
    spd_t_sia_questions   sia_question
  where 
    sia_structure.dofid = delivery_order_fac.deliveryorderfacilityid
    and sia_structure.siastructureid = sia_data.siastructureid
    and sia_data.siaquestionid = sia_question.siaquestionid
    and sia_question.siaquestion = '8_State_Str_Number'
    ) STRUCTURENO,
  
  delivery_orders.adoid ADOID
from
  spd_t_delivery_order_facs delivery_order_fac,
  spd_t_delivery_orders     delivery_orders,
  spd_t_facilities          facility,
  spd_mv_dt_facilities      infads
where
  delivery_order_fac.facid = facility.facid
  and delivery_order_fac.deliveryorderid = delivery_orders.deliveryorderid
  and facility.infads_facility_id = infads.facility_id
 ;
