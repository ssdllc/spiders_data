--------------------------------------------------------
--  DDL for View SPD_V_M_3D_MODEL_CLASSES
--------------------------------------------------------

  CREATE OR REPLACE FORCE EDITIONABLE VIEW "SPIDERS_DATA"."SPD_V_M_3D_MODEL_CLASSES" ("MODELCLASSID", "DESCRIPTION", "IMAGE_FILEPATH", "SHORT_DESCRIPTION") AS 
  select 
      MODELCLASSID,DESCRIPTION,IMAGE_FILEPATH,SHORT_DESCRIPTION
    from 
    SPD_T_3D_MODEL_CLASSES
 ;
