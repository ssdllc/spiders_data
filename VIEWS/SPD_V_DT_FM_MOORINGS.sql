
  CREATE OR REPLACE VIEW "SPIDERS_DATA"."SPD_V_DT_FM_MOORINGS" (
  "FMMOORINGID",
  "ACTIVITIESID",
  "MOORING_NAME", 
  "REPORT_DATE",
  "SPD_ACTIVITY_NAME",
  "SPECIAL_AREA_NAME",
  "SPECIAL_AREA_CODE",
  "BUOY_TYPE",
  "ANCHOR_TYPE",
  "INSPECTION_DATE",
  "FY",
  "MOORING_CONDITION_READINESS",
  "MOORING_RATED_CONDITION",
  "RNM") AS 
  SELECT "FMMOORINGID","ACTIVITIESID","MOORING_NAME","REPORT_DATE","SPD_ACTIVITY_NAME","SPECIAL_AREA_NAME","SPECIAL_AREA_CODE","BUOY_TYPE","ANCHOR_TYPE","INSPECTION_DATE","FY","MOORING_CONDITION_READINESS","MOORING_RATED_CONDITION","RNM" FROM (
    SELECT
          m.FMMOORINGID,
          m.ACTIVITIESID,
          m.mooring_name,
          a.report_date,
          act.SPD_ACTIVITY_NAME,
          act.SPECIAL_AREA_NAME,
          act.SPECIAL_AREA_CODE,
          am.BUOY_TYPE,
          am.ANCHOR_TYPE,
          am.INSPECTION_DATE,
          TO_CHAR(ADD_MONTHS( am.INSPECTION_DATE, 3 ),'YYYY') FY,
          am.MOORING_CONDITION_READINESS,
          am.MOORING_RATED_CONDITION,
          row_number() over (partition by m.FMMOORINGID order by a.REPORT_DATE DESC) rnm
    FROM
        SPD_T_FM_MOORINGS m,
        SPD_T_FM_ACTION_MOORINGS am,
        SPD_T_FM_ACTIONS a,
        spd_v_qc_act_with_spec_areas act
    WHERE
        m.FMMOORINGID = am.FMMOORINGID
        and am.FMACTIONID = a.FMACTIONID
        and m.activitiesid = act.activitiesid
        and m.special_area_code = act.special_area_code
  ) where rnm = 1
  UNION ALL
      SELECT
          m.FMMOORINGID,
          m.ACTIVITIESID,
          m.mooring_name,
          null report_date,
          act.SPD_ACTIVITY_NAME,
          act.SPECIAL_AREA_NAME,
          act.SPECIAL_AREA_CODE,
          null BUOY_TYPE,
          null ANCHOR_TYPE,
          null INSPECTION_DATE,
          null  FY,
          null MOORING_CONDITION_READINESS,
          null MOORING_RATED_CONDITION,
          0 rnm
    FROM
        SPD_T_FM_MOORINGS m,
        spd_v_qc_act_with_spec_areas act,
        SPD_T_FM_ACTION_MOORINGS am
    WHERE
        m.activitiesid = act.activitiesid
        and m.special_area_code = act.special_area_code
        and m.FMMOORINGID = am.FMMOORINGID(+)
        and am.FMMOORINGID is null;