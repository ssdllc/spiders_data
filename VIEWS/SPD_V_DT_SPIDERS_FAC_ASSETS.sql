--------------------------------------------------------
--  DDL for View SPD_V_DT_SPIDERS_FAC_ASSETS
--------------------------------------------------------

  CREATE OR REPLACE FORCE EDITIONABLE VIEW "SPIDERS_DATA"."SPD_V_DT_SPIDERS_FAC_ASSETS" ("SPIDERSID", "DELIVERYORDERID", "MAXIMOASSET", "ASSETNUM", "CHANGEDATE", "DESCRIPTION", "MASTERSYSTEM", "SYSTEM", "SUBSYSTEM", "INFADS") AS 
  select 
assets.SPIDERSID,
assets.DELIVERYORDERID,
assets.MAXIMOASSET,
assets.ASSETNUM,
assets.CHANGEDATE,
assets.DESCRIPTION,
assets.MASTERSYSTEM,
assets.SYSTEM,
assets.SUBSYSTEM,
loc.FACILITYID infads
from 
SPD_T_SPIDERS_FAC_ASSETS assets, SPD_T_MAXIMO_LOCATIONS loc
where assets.LOCATION = loc.LOCATION
 ;
