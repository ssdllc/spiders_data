--------------------------------------------------------
--  DDL for View SPD_V_M_PWS
--------------------------------------------------------

  CREATE OR REPLACE FORCE EDITIONABLE VIEW "SPIDERS_DATA"."SPD_V_M_PWS" ("PWID", "PWS_TYPE", "PWS_DESCRIPTION") AS 
  select 
      PWID,PWS_TYPE,PWS_DESCRIPTION
    from 
    SPD_T_PWS
 ;
