--------------------------------------------------------
--  DDL for View SPD_V_M_XML_COLUMN_EXCEPTION
--------------------------------------------------------

  CREATE OR REPLACE FORCE EDITIONABLE VIEW "SPIDERS_DATA"."SPD_V_M_XML_COLUMN_EXCEPTION" ("XMLCOLUMNEXCEPTIONID", "NODE_NAME", "COLUMN_NAME", "TABLE_NAME") AS 
  select 
      XMLCOLUMNEXCEPTIONID,NODE_NAME,COLUMN_NAME,TABLE_NAME
    from 
    SPD_T_XML_COLUMN_EXCEPTION
 ;
