--------------------------------------------------------
--  DDL for View SPD_V_M_XML_PHOTO_ASSET_TYPES
--------------------------------------------------------

  CREATE OR REPLACE FORCE EDITIONABLE VIEW "SPIDERS_DATA"."SPD_V_M_XML_PHOTO_ASSET_TYPES" ("ASSETTYPEID", "PHOTOID", "ASSETTYPE") AS 
  select 
      ASSETTYPEID,PHOTOID,ASSETTYPE
    from 
    SPD_T_XML_PHOTO_ASSET_TYPES
 ;
