--------------------------------------------------------
--  DDL for View SPD_V_QC_DIRECT_COST_BC
--------------------------------------------------------

  CREATE OR REPLACE FORCE EDITIONABLE VIEW "SPIDERS_DATA"."SPD_V_QC_DIRECT_COST_BC" ("DIRECTCOSTID", "PWSID", "CONTRACTID") AS 
  select 
  direct_cost.DIRECTCOSTID  DIRECTCOSTID,
  pws.PWSID                 PWSID,
  pws.CONTRACTID            CONTRACTID 
from 
  spd_t_direct_costs  direct_cost,
  spd_t_pwss          pws
where
  direct_cost.pwsid = pws.pwsid
ORDER BY 
  DIRECTCOSTID ASC
 ;
