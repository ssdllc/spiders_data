--------------------------------------------------------
--  DDL for View SPD_V_DT_FACILITY_COMPONENTS
--------------------------------------------------------

  CREATE OR REPLACE FORCE EDITIONABLE VIEW "SPIDERS_DATA"."SPD_V_DT_FACILITY_COMPONENTS" ("LINEITEMID", "FACILITYID", "UNIFORMATIICODE", "DESCRIPTION", "AS_ASSET_TYPE", "AS_FACILITY_SECTION", "CRITICALITYPERCENTAGE", "AS_NOOF", "AS_UNIT_QUANTITY", "AS_UNIT_TYPE", "CURRENTCI", "APPROXIMATEREPAIRCOSTS", "FIVEYEARCIIFREPAIRED", "TENYEARCIIFREPAIRED", "FIVEYEARCIIFNOTREPAIRED", "TENYEARCIIFNOTREPAIRED") AS 
  SELECT 
  ASSET_SUMMARY.LINEITEMID,
  ASSET_SUMMARY.FACILITYID,
  ASSET_SUMMARY.UNIFORMATIICODE,
  ASSET_SUMMARY.DESCRIPTION,
  ASSET_SUMMARY.ASSETTYPE as_asset_type,
  ASSET_SUMMARY.FACILITYSECTION as_facility_section,
  ASSET_SUMMARY.CRITICALITYPERCENTAGE,
  ASSET_SUMMARY.NOOFASSETSCOUNT as_noof,
  ASSET_SUMMARY.QUANTITYOFUNITS as_unit_quantity,
  ASSET_SUMMARY.UNITTYPE as_unit_type,
  ASSET_SUMMARY.CURRENTCI,
  ASSET_SUMMARY.APPROXIMATEREPAIRCOSTS,
  ASSET_SUMMARY.FIVEYEARCIIFREPAIRED,
  ASSET_SUMMARY.TENYEARCIIFREPAIRED,
  ASSET_SUMMARY.FIVEYEARCIIFNOTREPAIRED,
  ASSET_SUMMARY.TENYEARCIIFNOTREPAIRED
FROM
  SPD_T_XML_ASSET_SUMMARY   ASSET_SUMMARY
WHERE
  ASSET_SUMMARY.CURRENTCI is not null
 ;
