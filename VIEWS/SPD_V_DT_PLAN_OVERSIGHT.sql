--------------------------------------------------------
--  DDL for View SPD_V_DT_PLAN_OVERSIGHT
--------------------------------------------------------

  CREATE OR REPLACE FORCE EDITIONABLE VIEW "SPIDERS_DATA"."SPD_V_DT_PLAN_OVERSIGHT" ("PLANOVERSIGHTID", "PLANID", "QTR", "AMOUNT", "TYPE", "DESCRIPTION") AS 
  SELECT PLANOVERSIGHTID,
       PLANID,
       QTR,
       AMOUNT,
       TYPE,
       DESCRIPTION
FROM SPD_T_PLAN_OVERSIGHT;
