--------------------------------------------------------
--  DDL for View SPD_V_QC_MATERIAL_DESCRIPTIONS
--------------------------------------------------------

  CREATE OR REPLACE FORCE EDITIONABLE VIEW "SPIDERS_DATA"."SPD_V_QC_MATERIAL_DESCRIPTIONS" ("MATERIAL_DESCRIPTION") AS 
  select distinct(MATERIAL_DESCRIPTION) from SPD_MV_DATA_ANALYSIS order by lower(MATERIAL_DESCRIPTION)
 ;
