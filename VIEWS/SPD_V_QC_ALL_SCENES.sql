--------------------------------------------------------
--  DDL for View SPD_V_QC_ALL_SCENES
--------------------------------------------------------

  CREATE OR REPLACE FORCE EDITIONABLE VIEW "SPIDERS_DATA"."SPD_V_QC_ALL_SCENES" ("SCENEID", "DESCRIPTION") AS 
  SELECT
  sceneid, 
  description
FROM
  SPD_T_3D_SCENES
 ;
