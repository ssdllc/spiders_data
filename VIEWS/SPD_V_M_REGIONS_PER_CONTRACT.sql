--------------------------------------------------------
--  DDL for View SPD_V_M_REGIONS_PER_CONTRACT
--------------------------------------------------------

  CREATE OR REPLACE FORCE EDITIONABLE VIEW "SPIDERS_DATA"."SPD_V_M_REGIONS_PER_CONTRACT" ("REGIONPERCONTRACTID", "REGIONID", "CONTRACTID") AS 
  select 
      REGIONPERCONTRACTID,REGIONID,CONTRACTID
    from 
    SPD_T_REGIONS_PER_CONTRACT
 ;
