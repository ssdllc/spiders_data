--------------------------------------------------------
--  DDL for View SPD_V_DT_PLAN_NOTES
--------------------------------------------------------

  CREATE OR REPLACE FORCE EDITIONABLE VIEW "SPIDERS_DATA"."SPD_V_DT_PLAN_NOTES" ("PLANNOTEID", "PLANID", "NOTE", "CREATED_TIMESTAMP", "CREATED_BY") AS 
  SELECT
  pn.PLANNOTEID,
  pn.PLANID,
  pn.NOTE,
  TO_CHAR(pn.CREATED_TIMESTAMP, 'MON DD, YYYY') CREATED_TIMESTAMP,
  DECODE(
    INSTR(pn.CREATED_BY, '@'),
    0,
    pn.CREATED_BY,
    substr(pn.CREATED_BY,1,INSTR(pn.CREATED_BY, '@') - 1)
    ) CREATED_BY
FROM SPD_T_PLAN_NOTES pn;
