--------------------------------------------------------
--  DDL for View SPD_V_CONTRACTS_REGION
--------------------------------------------------------

  CREATE OR REPLACE FORCE EDITIONABLE VIEW "SPIDERS_DATA"."SPD_V_CONTRACTS_REGION" ("group_filter_display_name", "group_name", "display_value", "return_value", "record_count", "rn") AS 
  select
      'Region' group_filter_display_name,
      'REGION' group_name,
      'change_display_value' display_value,
      'change_return_value' return_value,
      COUNT(*) OVER (PARTITION BY 'return_value') record_count,
      ROW_NUMBER() OVER (PARTITION BY 'return_value' ORDER BY 'return_value' NULLS LAST) rn
    from
      dual
 ;
