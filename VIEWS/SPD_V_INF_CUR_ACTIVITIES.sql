--------------------------------------------------------
--  DDL for View SPD_V_INF_CUR_ACTIVITIES
--------------------------------------------------------

  CREATE OR REPLACE FORCE EDITIONABLE VIEW "SPIDERS_DATA"."SPD_V_INF_CUR_ACTIVITIES" ("PRODUCTLINE_LISTID", "COUNT_OF_FACS", "UIC", "ACTIVITY_TITLE", "ACTIVITY_LOCATOR_NAME", "UNIT_TITLE", "ACTIVITY_TYPE", "ACTIVITY_LOCATION", "PRIMARY_FUNCTION_DESC", "CONTRACTOR_NAME", "ROICC_SHORT_TITLE", "OPERATING_FORCES_IND", "DISESTABLISHED_IND", "PSEUDO_ACTIVITY_IND", "OICC_ROICC_IND", "NATO_IND", "POSSESSION_IND", "SNDL_NO", "INITIAL_OCCUPATION_DATE", "ENGINEERING_EVAL_DATE", "HOST_TENANT_CODE", "PLANNING_SPONSOR_CODE", "ACTIVITY_GROUPING_CODE", "BASIC_FUNDING_CODE", "ACTIVITY_STATUS_CODE", "EPA_REGION_CODE", "OPERATING_STATUS_CODE", "ACTIVITY_CODE", "EFD_PROJECT_MGR_CODE", "HOST_UIC", "OBC_UIC", "AAA_UIC", "RPI_MAILING_UIC", "PLANT_PROPERTY_UIC", "CARETAKER_UIC", "MAGIC_MAILING_UIC", "PAYROLL_UIC", "RPI_ATTENTION_LINE", "PPA_ATTENTION_LINE", "FRP_ATTENTION_LINE", "RPMA_ATTENTION_LINE", "MAILING_ADDRESS_1", "MAILING_ADDRESS_2", "MAILING_ADDRESS_3", "MAILING_ADDRESS_4", "CONGRESS_DIST_CODE", "CONTINENT_CODE", "WATER_AREA_CODE", "GEOLOCATION_CODE", "LATITUDE", "LONGITUDE", "AREA_COMPLEX_CODE", "NEAREST_CITY", "OTHER_COUNTIES", "CITY_CODE", "COUNTY_CODE", "STATE_CODE", "COUNTRY_CODE", "ZIP_CODE", "FILE_REFERENCE", "CHANGE_DATE", "SITE_CODE", "PLANNING_AREA_ID", "RS_ENTERPRISE_CODE", "CLAIMANT_CODE", "FEC_UIC", "STATE", "STATE_ABBR", "COUNTRY") AS 
  SELECT
    (select listid from spd_t_lists where listname = 'list_product_lines' and displayvalue = 'Waterfront') productline_listid, 
    (SELECT COUNT(*)
    FROM spd_mv_inf_facility
    WHERE activity_uic           = actv.UIC
    AND prime_use_category_code IN ('15110', '15120', '15130', '15140', '15150', '15160', '15170', '15171', '15180', '15190', '15210', '15240', '15250', '15260', '15410', '15420', '15430', '15520', '16410', '16420', '21310', '75060', '75061' )
    ) count_of_facs,
    actv.UIC,
    actv.ACTIVITY_TITLE,
    actv.ACTIVITY_LOCATOR_NAME,
    actv.UNIT_TITLE,
    actv.ACTIVITY_TYPE,
    actv.ACTIVITY_LOCATION,
    actv.PRIMARY_FUNCTION_DESC,
    actv.CONTRACTOR_NAME,
    actv.ROICC_SHORT_TITLE,
    actv.OPERATING_FORCES_IND,
    actv.DISESTABLISHED_IND,
    actv.PSEUDO_ACTIVITY_IND,
    actv.OICC_ROICC_IND,
    actv.NATO_IND,
    actv.POSSESSION_IND,
    actv.SNDL_NO,
    actv.INITIAL_OCCUPATION_DATE,
    actv.ENGINEERING_EVAL_DATE,
    actv.HOST_TENANT_CODE,
    actv.PLANNING_SPONSOR_CODE,
    actv.ACTIVITY_GROUPING_CODE,
    actv.BASIC_FUNDING_CODE,
    actv.ACTIVITY_STATUS_CODE,
    actv.EPA_REGION_CODE,
    actv.OPERATING_STATUS_CODE,
    actv.ACTIVITY_CODE,
    actv.EFD_PROJECT_MGR_CODE,
    actv.HOST_UIC,
    actv.OBC_UIC,
    actv.AAA_UIC,
    actv.RPI_MAILING_UIC,
    actv.PLANT_PROPERTY_UIC,
    actv.CARETAKER_UIC,
    actv.MAGIC_MAILING_UIC,
    actv.PAYROLL_UIC,
    actv.RPI_ATTENTION_LINE,
    actv.PPA_ATTENTION_LINE,
    actv.FRP_ATTENTION_LINE,
    actv.RPMA_ATTENTION_LINE,
    actv.MAILING_ADDRESS_1,
    actv.MAILING_ADDRESS_2,
    actv.MAILING_ADDRESS_3,
    actv.MAILING_ADDRESS_4,
    actv.CONGRESS_DIST_CODE,
    actv.CONTINENT_CODE,
    actv.WATER_AREA_CODE,
    actv.GEOLOCATION_CODE,
    actv.LATITUDE,
    actv.LONGITUDE,
    actv.AREA_COMPLEX_CODE,
    actv.NEAREST_CITY,
    actv.OTHER_COUNTIES,
    actv.CITY_CODE,
    actv.COUNTY_CODE,
    actv.STATE_CODE,
    actv.COUNTRY_CODE,
    actv.ZIP_CODE,
    actv.FILE_REFERENCE,
    actv.CHANGE_DATE,
    actv.SITE_CODE,
    actv.PLANNING_AREA_ID,
    actv.RS_ENTERPRISE_CODE,
    actv.CLAIMANT_CODE,
    actv.FEC_UIC,
    state.description state,
    state.state_abbr,
    country.description country
  FROM spd_mv_inf_activity actv,
    spd_mv_inf_state_code state,
    spd_mv_inf_country_code country
  WHERE actv.state_code  = state.state_code
  AND actv.country_code  = country.country_code
  AND state.country_code = country.country_code
  AND actv.uic          IN
    ( SELECT DISTINCT activity_uic FROM spd_mv_inf_facility
    )
  AND (SELECT COUNT(*)
    FROM spd_mv_inf_facility
    WHERE activity_uic           = actv.UIC
    AND prime_use_category_code IN ('15110', '15120', '15130', '15140', '15150', '15160', '15170', '15171', '15180', '15190', '15210', '15240', '15250', '15260', '15410', '15420', '15430', '15520', '16410', '16420', '21310', '75060', '75061' ) ) > 0
  
union

SELECT
    (select listid from spd_t_lists where listname = 'list_product_lines' and displayvalue = 'Bridges') productline_listid, 
    (SELECT COUNT(*)
    FROM spd_mv_inf_facility
    WHERE activity_uic           = actv.UIC
    AND prime_use_category_code IN ('85120','86030','85230' )
    ) count_of_facs,
    actv.UIC,
    actv.ACTIVITY_TITLE,
    actv.ACTIVITY_LOCATOR_NAME,
    actv.UNIT_TITLE,
    actv.ACTIVITY_TYPE,
    actv.ACTIVITY_LOCATION,
    actv.PRIMARY_FUNCTION_DESC,
    actv.CONTRACTOR_NAME,
    actv.ROICC_SHORT_TITLE,
    actv.OPERATING_FORCES_IND,
    actv.DISESTABLISHED_IND,
    actv.PSEUDO_ACTIVITY_IND,
    actv.OICC_ROICC_IND,
    actv.NATO_IND,
    actv.POSSESSION_IND,
    actv.SNDL_NO,
    actv.INITIAL_OCCUPATION_DATE,
    actv.ENGINEERING_EVAL_DATE,
    actv.HOST_TENANT_CODE,
    actv.PLANNING_SPONSOR_CODE,
    actv.ACTIVITY_GROUPING_CODE,
    actv.BASIC_FUNDING_CODE,
    actv.ACTIVITY_STATUS_CODE,
    actv.EPA_REGION_CODE,
    actv.OPERATING_STATUS_CODE,
    actv.ACTIVITY_CODE,
    actv.EFD_PROJECT_MGR_CODE,
    actv.HOST_UIC,
    actv.OBC_UIC,
    actv.AAA_UIC,
    actv.RPI_MAILING_UIC,
    actv.PLANT_PROPERTY_UIC,
    actv.CARETAKER_UIC,
    actv.MAGIC_MAILING_UIC,
    actv.PAYROLL_UIC,
    actv.RPI_ATTENTION_LINE,
    actv.PPA_ATTENTION_LINE,
    actv.FRP_ATTENTION_LINE,
    actv.RPMA_ATTENTION_LINE,
    actv.MAILING_ADDRESS_1,
    actv.MAILING_ADDRESS_2,
    actv.MAILING_ADDRESS_3,
    actv.MAILING_ADDRESS_4,
    actv.CONGRESS_DIST_CODE,
    actv.CONTINENT_CODE,
    actv.WATER_AREA_CODE,
    actv.GEOLOCATION_CODE,
    actv.LATITUDE,
    actv.LONGITUDE,
    actv.AREA_COMPLEX_CODE,
    actv.NEAREST_CITY,
    actv.OTHER_COUNTIES,
    actv.CITY_CODE,
    actv.COUNTY_CODE,
    actv.STATE_CODE,
    actv.COUNTRY_CODE,
    actv.ZIP_CODE,
    actv.FILE_REFERENCE,
    actv.CHANGE_DATE,
    actv.SITE_CODE,
    actv.PLANNING_AREA_ID,
    actv.RS_ENTERPRISE_CODE,
    actv.CLAIMANT_CODE,
    actv.FEC_UIC,
    state.description state,
    state.state_abbr,
    country.description country
  FROM spd_mv_inf_activity actv,
    spd_mv_inf_state_code state,
    spd_mv_inf_country_code country
  WHERE actv.state_code  = state.state_code
  AND actv.country_code  = country.country_code
  AND state.country_code = country.country_code
  AND actv.uic          IN
    ( SELECT DISTINCT activity_uic FROM spd_mv_inf_facility
    )
  AND (SELECT COUNT(*)
    FROM spd_mv_inf_facility
    WHERE activity_uic           = actv.UIC
    AND prime_use_category_code IN ('85120','86030','85230' ) ) > 0
  
union


 SELECT
    (select listid from spd_t_lists where listname = 'list_product_lines' and displayvalue = 'Water Tanks') productline_listid, 
    (SELECT COUNT(*)
    FROM spd_mv_inf_facility
    WHERE activity_uic           = actv.UIC
    AND (
          prime_use_category_code like '841%'
          or prime_use_category_code like '844%'
          or prime_use_category_code like '843%'
        )
    ) count_of_facs,
    actv.UIC,
    actv.ACTIVITY_TITLE,
    actv.ACTIVITY_LOCATOR_NAME,
    actv.UNIT_TITLE,
    actv.ACTIVITY_TYPE,
    actv.ACTIVITY_LOCATION,
    actv.PRIMARY_FUNCTION_DESC,
    actv.CONTRACTOR_NAME,
    actv.ROICC_SHORT_TITLE,
    actv.OPERATING_FORCES_IND,
    actv.DISESTABLISHED_IND,
    actv.PSEUDO_ACTIVITY_IND,
    actv.OICC_ROICC_IND,
    actv.NATO_IND,
    actv.POSSESSION_IND,
    actv.SNDL_NO,
    actv.INITIAL_OCCUPATION_DATE,
    actv.ENGINEERING_EVAL_DATE,
    actv.HOST_TENANT_CODE,
    actv.PLANNING_SPONSOR_CODE,
    actv.ACTIVITY_GROUPING_CODE,
    actv.BASIC_FUNDING_CODE,
    actv.ACTIVITY_STATUS_CODE,
    actv.EPA_REGION_CODE,
    actv.OPERATING_STATUS_CODE,
    actv.ACTIVITY_CODE,
    actv.EFD_PROJECT_MGR_CODE,
    actv.HOST_UIC,
    actv.OBC_UIC,
    actv.AAA_UIC,
    actv.RPI_MAILING_UIC,
    actv.PLANT_PROPERTY_UIC,
    actv.CARETAKER_UIC,
    actv.MAGIC_MAILING_UIC,
    actv.PAYROLL_UIC,
    actv.RPI_ATTENTION_LINE,
    actv.PPA_ATTENTION_LINE,
    actv.FRP_ATTENTION_LINE,
    actv.RPMA_ATTENTION_LINE,
    actv.MAILING_ADDRESS_1,
    actv.MAILING_ADDRESS_2,
    actv.MAILING_ADDRESS_3,
    actv.MAILING_ADDRESS_4,
    actv.CONGRESS_DIST_CODE,
    actv.CONTINENT_CODE,
    actv.WATER_AREA_CODE,
    actv.GEOLOCATION_CODE,
    actv.LATITUDE,
    actv.LONGITUDE,
    actv.AREA_COMPLEX_CODE,
    actv.NEAREST_CITY,
    actv.OTHER_COUNTIES,
    actv.CITY_CODE,
    actv.COUNTY_CODE,
    actv.STATE_CODE,
    actv.COUNTRY_CODE,
    actv.ZIP_CODE,
    actv.FILE_REFERENCE,
    actv.CHANGE_DATE,
    actv.SITE_CODE,
    actv.PLANNING_AREA_ID,
    actv.RS_ENTERPRISE_CODE,
    actv.CLAIMANT_CODE,
    actv.FEC_UIC,
    state.description state,
    state.state_abbr,
    country.description country
  FROM spd_mv_inf_activity actv,
    spd_mv_inf_state_code state,
    spd_mv_inf_country_code country
  WHERE actv.state_code  = state.state_code
  AND actv.country_code  = country.country_code
  AND state.country_code = country.country_code
  AND actv.uic          IN
    ( SELECT DISTINCT activity_uic FROM spd_mv_inf_facility
    )
  AND (SELECT COUNT(*)
    FROM spd_mv_inf_facility
    WHERE activity_uic           = actv.UIC
    --AND substr(prime_use_category_code,1 ,3) in ('841','844','843') ) > 0
    AND (
          prime_use_category_code like '841%'
          or prime_use_category_code like '844%'
          or prime_use_category_code like '843%'
        ) ) > 0

union

SELECT
    (select listid from spd_t_lists where listname = 'list_product_lines' and displayvalue = 'Dry Docks') productline_listid,
    (SELECT COUNT(*)
    FROM spd_mv_inf_facility
    WHERE activity_uic = actv.UIC
    AND prime_use_category_code IN ('21310','21320','21367' )
    ) count_of_facs,
    actv.UIC,
    actv.ACTIVITY_TITLE,
    actv.ACTIVITY_LOCATOR_NAME,
    actv.UNIT_TITLE,
    actv.ACTIVITY_TYPE,
    actv.ACTIVITY_LOCATION,
    actv.PRIMARY_FUNCTION_DESC,
    actv.CONTRACTOR_NAME,
    actv.ROICC_SHORT_TITLE,
    actv.OPERATING_FORCES_IND,
    actv.DISESTABLISHED_IND,
    actv.PSEUDO_ACTIVITY_IND,
    actv.OICC_ROICC_IND,
    actv.NATO_IND,
    actv.POSSESSION_IND,
    actv.SNDL_NO,
    actv.INITIAL_OCCUPATION_DATE,
    actv.ENGINEERING_EVAL_DATE,
    actv.HOST_TENANT_CODE,
    actv.PLANNING_SPONSOR_CODE,
    actv.ACTIVITY_GROUPING_CODE,
    actv.BASIC_FUNDING_CODE,
    actv.ACTIVITY_STATUS_CODE,
    actv.EPA_REGION_CODE,
    actv.OPERATING_STATUS_CODE,
    actv.ACTIVITY_CODE,
    actv.EFD_PROJECT_MGR_CODE,
    actv.HOST_UIC,
    actv.OBC_UIC,
    actv.AAA_UIC,
    actv.RPI_MAILING_UIC,
    actv.PLANT_PROPERTY_UIC,
    actv.CARETAKER_UIC,
    actv.MAGIC_MAILING_UIC,
    actv.PAYROLL_UIC,
    actv.RPI_ATTENTION_LINE,
    actv.PPA_ATTENTION_LINE,
    actv.FRP_ATTENTION_LINE,
    actv.RPMA_ATTENTION_LINE,
    actv.MAILING_ADDRESS_1,
    actv.MAILING_ADDRESS_2,
    actv.MAILING_ADDRESS_3,
    actv.MAILING_ADDRESS_4,
    actv.CONGRESS_DIST_CODE,
    actv.CONTINENT_CODE,
    actv.WATER_AREA_CODE,
    actv.GEOLOCATION_CODE,
    actv.LATITUDE,
    actv.LONGITUDE,
    actv.AREA_COMPLEX_CODE,
    actv.NEAREST_CITY,
    actv.OTHER_COUNTIES,
    actv.CITY_CODE,
    actv.COUNTY_CODE,
    actv.STATE_CODE,
    actv.COUNTRY_CODE,
    actv.ZIP_CODE,
    actv.FILE_REFERENCE,
    actv.CHANGE_DATE,
    actv.SITE_CODE,
    actv.PLANNING_AREA_ID,
    actv.RS_ENTERPRISE_CODE,
    actv.CLAIMANT_CODE,
    actv.FEC_UIC,
    state.description state,
    state.state_abbr,
    country.description country
  FROM spd_mv_inf_activity actv,
    spd_mv_inf_state_code state,
    spd_mv_inf_country_code country
  WHERE actv.state_code  = state.state_code
  AND actv.country_code  = country.country_code
  AND state.country_code = country.country_code
  AND actv.uic          IN
    ( SELECT DISTINCT activity_uic FROM spd_mv_inf_facility
    )
  AND (SELECT COUNT(*)
    FROM spd_mv_inf_facility
    WHERE activity_uic           = actv.UIC
    AND prime_use_category_code IN ('21310','21320','21367' ) ) > 0
  
        
        
  ORDER BY 
    country DESC, 
    productline_listid,
    state asc,
    unit_title
 ;
