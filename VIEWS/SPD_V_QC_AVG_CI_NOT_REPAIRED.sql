--------------------------------------------------------
--  DDL for View SPD_V_QC_AVG_CI_NOT_REPAIRED
--------------------------------------------------------

  CREATE OR REPLACE FORCE EDITIONABLE VIEW "SPIDERS_DATA"."SPD_V_QC_AVG_CI_NOT_REPAIRED" ("UNIFORMATIICODE", "AVGCURRENTCI", "AVGFIVEYEARCIIFNOTREPAIRED", "AVGTENYEARCIIFNOTREPAIRED") AS 
  select   
  xml_asset_summary.UNIFORMATIICODE                         UNIFORMATIICODE,
  round(avg(xml_asset_summary.CURRENTCI),0)                 AVGCURRENTCI,
  round(avg(xml_asset_summary.FIVEYEARCIIFNOTREPAIRED),0)   AVGFIVEYEARCIIFNOTREPAIRED,
  round(avg(xml_asset_summary.TENYEARCIIFNOTREPAIRED),0)    AVGTENYEARCIIFNOTREPAIRED
from
  spd_t_xml_asset_summary xml_asset_summary
where
  xml_asset_summary.CURRENTCI is not null
group by
  UNIFORMATIICODE
 ;
