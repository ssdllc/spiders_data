--------------------------------------------------------
--  DDL for View SPD_V_DT_PLANS
--------------------------------------------------------

  CREATE OR REPLACE FORCE EDITIONABLE VIEW "SPIDERS_DATA"."SPD_V_DT_PLANS" ("PRODUCTLINEID", "FUNDSOURCE_LISTID", "PLANID", "TITLE", "FY", "FUND_SOURCE", "PRODUCTLINE", "ACTIONS", "SUM_INHOUSE", "SUM_KTR", "SUM_REC_INHOUSE", "SUM_REC_KTR") AS 
  select
  	p.PRODUCTLINEID									PRODUCTLINEID,
  	p.FUNDSOURCE_LISTID								FUNDSOURCE_LISTID,
	planid 											PLANID,
	title											TITLE,
	FY												FY,
	l.DISPLAYVALUE 									FUND_SOURCE,
	pl.PRODUCTLINE_DISPLAY 							PRODUCTLINE,
	(select count(pa.planactionid)
	 from spd_t_plan_actions pa
	 where pa.planid = p.planid) 					ACTIONS,
	(select sum(a.INHOUSE_AMT)
	 from spd_t_plan_actions pa, spd_t_actions a
	 where pa.actionid = a.actionid and
	 	pa.planid = p.planid
	 	group by pa.planid) 						SUM_INHOUSE,
	 (select sum(a.KTR_AMT)
	 from spd_t_plan_actions pa, spd_t_actions a
	 where pa.actionid = a.actionid and
	 	pa.planid = p.planid
	 	group by pa.planid) 						SUM_KTR,
	(select
	 sum(pa.REC_INHOUSE_AMT)
	 from spd_t_plan_actions pa
	 where pa.planid = p.planid
	 group by pa.planid) 							SUM_REC_INHOUSE,
	(select
	 sum(pa.REC_KTR_AMT)
	 from spd_t_plan_actions pa
	 where pa.planid = p.planid
	 group by pa.planid) 							SUM_REC_KTR
from
	spd_t_plans p,
	spd_t_productlines pl,
	spd_t_lists l
where p.PRODUCTLINEID = pl.PRODUCTLINEID
	  and p.FUNDSOURCE_LISTID = l.LISTID;
