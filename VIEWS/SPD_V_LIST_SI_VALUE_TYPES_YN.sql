--------------------------------------------------------
--  DDL for View SPD_V_LIST_SI_VALUE_TYPES_YN
--------------------------------------------------------

  CREATE OR REPLACE FORCE EDITIONABLE VIEW "SPIDERS_DATA"."SPD_V_LIST_SI_VALUE_TYPES_YN" ("LISTID", "DISPLAYVALUE", "RETURNVALUE", "GROUPING", "LISTNAME", "VISUALORDER", "PRODUCTLINE_LISTID", "NOTES") AS 
  select listid, displayvalue, returnvalue, grouping, listname, visualorder, productline_listid, notes from spd_t_lists where listname = 'list_yes_no' order by visualorder
 
 ;
