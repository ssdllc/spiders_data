--------------------------------------------------------
--  DDL for View SPD_V_M_XML_ASSET_INVENTORY
--------------------------------------------------------

  CREATE OR REPLACE FORCE EDITIONABLE VIEW "SPIDERS_DATA"."SPD_V_M_XML_ASSET_INVENTORY" ("INVENTORYID", "LINEITEMID", "UNIFORMATIICODE", "UNIFORMATIINAME", "ASSETTYPE", "ASSETTYPEDESCRIPTION", "FACILITYSECTION", "CRITICALITYFACTOR", "NOOFASSETSCOUNT", "QUANTITYOFUNITS", "UNITTYPE", "MATERIAL", "MATERAILDESCRIPTION", "CREATED_BY", "CREATED_TIMESTAMP", "UPDATED_BY", "UPDATED_TIMESTAMP") AS 
  select x."INVENTORYID",x."LINEITEMID",x."UNIFORMATIICODE",x."UNIFORMATIINAME",x."ASSETTYPE",x."ASSETTYPEDESCRIPTION",x."FACILITYSECTION",x."CRITICALITYFACTOR",x."NOOFASSETSCOUNT",x."QUANTITYOFUNITS",x."UNITTYPE",x."MATERIAL",x."MATERAILDESCRIPTION",x."CREATED_BY",x."CREATED_TIMESTAMP",x."UPDATED_BY",x."UPDATED_TIMESTAMP"
from 
    SPD_T_XML_ASSET_INVENTORY x
 ;
