--------------------------------------------------------
--  DDL for View SPD_V_QC_MAX_CI_NOT_REPAIRED
--------------------------------------------------------

  CREATE OR REPLACE FORCE EDITIONABLE VIEW "SPIDERS_DATA"."SPD_V_QC_MAX_CI_NOT_REPAIRED" ("MAXCURRENTCI", "MAXFIVEYEARCIIFNOTREPAIRED", "MAXTENYEARCIIFNOTREPAIRED", "UNIFORMATIICODE") AS 
  select   
  max(xml_asset_summary.CURRENTCI)                MAXCURRENTCI,
  max(xml_asset_summary.FIVEYEARCIIFNOTREPAIRED)  MAXFIVEYEARCIIFNOTREPAIRED,
  max(xml_asset_summary.TENYEARCIIFNOTREPAIRED)   MAXTENYEARCIIFNOTREPAIRED,
  xml_asset_summary.uniformatiicode               UNIFORMATIICODE
from
  spd_t_xml_asset_summary xml_asset_summary
where
  xml_asset_summary.CURRENTCI is not null
group by
  UNIFORMATIICODE
 ;
