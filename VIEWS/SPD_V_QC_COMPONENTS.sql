--------------------------------------------------------
--  DDL for View SPD_V_QC_COMPONENTS
--------------------------------------------------------

  CREATE OR REPLACE FORCE EDITIONABLE VIEW "SPIDERS_DATA"."SPD_V_QC_COMPONENTS" ("RETURNVALUE") AS 
  select distinct(RETURNVALUE) from SPD_MV_DATA_ANALYSIS order by lower(RETURNVALUE)
 ;
