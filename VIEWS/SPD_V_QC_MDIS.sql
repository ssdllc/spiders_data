--------------------------------------------------------
--  DDL for View SPD_V_QC_MDIS
--------------------------------------------------------

  CREATE OR REPLACE FORCE EDITIONABLE VIEW "SPIDERS_DATA"."SPD_V_QC_MDIS" ("DISPLAY_VALUE") AS 
  select distinct(MDI) DISPLAY_VALUE from SPIDERS_DATA.SPD_MV_DATA_ANALYSIS where rownum < 200 order by to_number(MDI)
 ;
