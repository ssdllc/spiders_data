--------------------------------------------------------
--  DDL for View SPD_V_QC_REPAIR_COST_ESTIMATES
--------------------------------------------------------

  CREATE OR REPLACE FORCE EDITIONABLE VIEW "SPIDERS_DATA"."SPD_V_QC_REPAIR_COST_ESTIMATES" ("REPAIR_COST_ESTIMATE") AS 
  select distinct(REPAIR_COST_ESTIMATE) from SPD_MV_DATA_ANALYSIS order by to_number(REPAIR_COST_ESTIMATE)
 ;
