
  CREATE OR REPLACE FORCE EDITIONABLE VIEW "SPIDERS_DATA"."SPD_V_DT_FACILITIES_SOURCE" ("FACID", "SPIDERS_FACNAME", "SPIDERS_FACNO", "FACILITY_ID", "FACILITY_NAME", "FACILITY_NO", "ACTIVITY_UIC", "SPECIAL_AREA_CODE", "UNIT_TITLE", "SPECIAL_AREA_NAME", "STATE_ABBR", "STATE_ABBR1", "DESCRIPTION", "PRIME_USE_CATEGORY_CODE", "MAINTENANCE_RESP_UIC", "CI", "MDI", "MDI_DATE", "ENGINEERING_EVAL_DATE") AS 
  select "FACID","SPIDERS_FACNAME","SPIDERS_FACNO","FACILITY_ID","FACILITY_NAME","FACILITY_NO","ACTIVITY_UIC","SPECIAL_AREA_CODE","UNIT_TITLE","SPECIAL_AREA_NAME","STATE_ABBR","STATE_ABBR1","DESCRIPTION","PRIME_USE_CATEGORY_CODE","MAINTENANCE_RESP_UIC","CI","MDI","MDI_DATE","ENGINEERING_EVAL_DATE" from (
  select * from (
  select * from (
    select 
        sfac.facid                    facid,
        sfac.spd_fac_name             spiders_facname,
        sfac.spd_fac_no               spiders_facno,
        fac.facility_id               facility_id,
        trim(fac.facility_name)             facility_name,
        trim(fac.facility_no)               facility_no,
        fac.activity_uic              activity_uic,
        fac.special_area_code         special_area_code,
        actv.unit_title               unit_title,
        sa.special_area_name          special_area_name, 
        decode(state.state_abbr,null,country.description,state.state_abbr) state_abbr,
        state.state_abbr              state_abbr1,
        country.description           description,
        fac.prime_use_category_code   prime_use_category_code,
        fac.maintenance_resp_uic      maintenance_resp_uic,
        fac.condition_rating          ci,
        fac.mission_dependency_index  mdi,
        fac.mdi_date                  mdi_date,
        fac.engineering_eval_date     engineering_eval_date
      from 
        spd_mv_inf_facility           fac,
        spd_mv_inf_activity           actv,
        spd_mv_inf_cur_actv_spec_area sa,
        spd_t_facilities              sfac,
        spd_t_fac_geom                loc,
        spd_mv_inf_state_code         state,
        spd_mv_inf_country_code       country
      where 
        actv.state_code = state.state_code
        and state.country_code = country.country_code
        and actv.country_code = country.country_code
        and loc.facid(+) = sfac.facid
        and sfac.infads_facility_id(+) = fac.facility_id
        and sa.uic = actv.uic
        and (
              decode(fac.special_area_code,null,-1) = decode(sa.special_area_code,null,-1)
              or
              fac.special_area_code = sa.special_area_code
            )
              
        and fac.activity_uic = sa.uic
        
      --order by 
        --actv.activity_title, 
        --fac.facility_name
    )
union all
  select * from (
    select
      sfac.facid          facid,
      sfac.spd_fac_name   spiders_facname,
      sfac.spd_fac_no     spiders_facno,
      ''                  facility_id,
      trim(sfac.spd_fac_name)                  facility_name,
      trim(sfac.spd_fac_no)                  facility_no,
      ''                  activity_uic,
      ''                  special_area_code,
      ''                  unit_title,
      ''                  special_area_name, 
      ''                  state_abbr,
      ''                  state_abbr1,
      ''                  description,
      ''                  prime_use_category_code,
      ''                  maintenance_resp_uic,
      0                   ci,
      0                   mdi,
      sysdate             mdi_date,
      sysdate             engineering_eval_date
    from 
      spd_t_facilities sfac
    where
      sfac.infads_facility_id is null
    )
))
 ;
