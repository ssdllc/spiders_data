--------------------------------------------------------
--  DDL for View SPD_V_DT_CONTRACTOR_JOB_TITLES
--------------------------------------------------------

  CREATE OR REPLACE FORCE EDITIONABLE VIEW "SPIDERS_DATA"."SPD_V_DT_CONTRACTOR_JOB_TITLES" ("JOBTITLEID", "CONTRACTCONTRACTORID", "CONTRACTORID", "CONTRACTID", "CONTRACTOR_NAME", "CITY", "STATE", "PRIME", "OVERHEAD", "FRINGE", "GENERAL_AND_ADMINISTRATIVE", "JOB_TITLE", "JOB_TITLE_DESCRIPTION", "VISUAL_ORDER", "BASE_RATE", "OPTION_1_RATE", "OPTION_2_RATE", "OPTION_3_RATE", "OPTION_4_RATE") AS 
  SELECT 
    job_title.jobtitleid,
    contract_contractor.contractcontractorid,
    contractor.contractorid,
    contract_contractor.contractid,
    contractor.contractor_name,
    contractor.city,
    contractor.state,
    contract_contractor.contract_role prime,
    contract_contractor.overhead,
    contract_contractor.fringe,
    contract_contractor.general_and_administrative,
    job_title.job_title,
    job_title.job_title_description,
    job_title.visual_order,
    job_title.base_rate,
    job_title.option_1_rate,
    job_title.option_2_rate,
    job_title.option_3_rate,
    job_title.option_4_rate
FROM 
    spd_t_contractors contractor,
    spd_t_contract_contractors contract_contractor,
    spd_t_job_titles job_title
WHERE
    contractor.contractorid = contract_contractor.contractorid
 ;
