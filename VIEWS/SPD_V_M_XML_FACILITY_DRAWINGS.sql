--------------------------------------------------------
--  DDL for View SPD_V_M_XML_FACILITY_DRAWINGS
--------------------------------------------------------

  CREATE OR REPLACE FORCE EDITIONABLE VIEW "SPIDERS_DATA"."SPD_V_M_XML_FACILITY_DRAWINGS" ("FACDRAWINGID", "FACILITYID", "DRAWINGID", "NAVFACDRAWINGNUMBER", "DESCRIPTION", "DRAWINGTYPE", "FILENAME") AS 
  select 
      FACDRAWINGID,FACILITYID,DRAWINGID,NAVFACDRAWINGNUMBER,DESCRIPTION,DRAWINGTYPE,FILENAME
    from 
    SPD_T_XML_FACILITY_DRAWINGS
 ;
