--------------------------------------------------------
--  DDL for View SPD_V_QC_CONTRACTORS
--------------------------------------------------------

  CREATE OR REPLACE FORCE EDITIONABLE VIEW "SPIDERS_DATA"."SPD_V_QC_CONTRACTORS" ("CONTRACTORID", "CONTRACTOR_NAME") AS 
  select CONTRACTORID,CONTRACTOR_NAME 
  from SPD_T_CONTRACTORS  ORDER BY CONTRACTOR_NAME DESC
 ;
