--------------------------------------------------------
--  DDL for View SPD_V_M_ATTRIBUTES
--------------------------------------------------------

  CREATE OR REPLACE FORCE EDITIONABLE VIEW "SPIDERS_DATA"."SPD_V_M_ATTRIBUTES" ("ATTRIBUTEID", "GROUP_NAME", "NAME", "TYPE") AS 
  select 
      ATTRIBUTEID,GROUP_NAME,NAME,TYPE
    from 
    SPD_T_ATTRIBUTES
 ;
