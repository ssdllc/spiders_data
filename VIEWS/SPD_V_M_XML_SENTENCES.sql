--------------------------------------------------------
--  DDL for View SPD_V_M_XML_SENTENCES
--------------------------------------------------------

  CREATE OR REPLACE FORCE EDITIONABLE VIEW "SPIDERS_DATA"."SPD_V_M_XML_SENTENCES" ("SENTENCEID", "PARAGRAPHID", "SENTENCE") AS 
  select 
      SENTENCEID,PARAGRAPHID,SENTENCE
    from 
    SPD_T_XML_SENTENCES
 ;
