--------------------------------------------------------
--  DDL for View SPD_V_QC_FINDINGS_AND_RECO
--------------------------------------------------------

  CREATE OR REPLACE FORCE EDITIONABLE VIEW "SPIDERS_DATA"."SPD_V_QC_FINDINGS_AND_RECO" ("RECID", "FACILITYID", "UNIFORMATIICODE", "DESCRIPTION", "FINDINGS", "RECOMMENDATIONS") AS 
  SELECT
  XML_FINDREC.RECID,
  XML_FINDREC.FACILITYID,
  XML_FINDREC.UNIFORMATIICODE,
  XML_FINDREC.DESCRIPTION,
  XML_FINDREC.FINDINGS,
  XML_FINDREC.RECOMMENDATIONS
FROM
  SPD_T_XML_FINDREC         XML_FINDREC,
  SPD_T_XML_ASSET_INVENTORY XML_ASSET_INVENTORY
WHERE
  XML_FINDREC.UNIFORMATIICODE = XML_ASSET_INVENTORY.UNIFORMATIICODE
 ;
