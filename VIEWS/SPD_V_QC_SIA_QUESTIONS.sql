--------------------------------------------------------
--  DDL for View SPD_V_QC_SIA_QUESTIONS
--------------------------------------------------------

  CREATE OR REPLACE FORCE EDITIONABLE VIEW "SPIDERS_DATA"."SPD_V_QC_SIA_QUESTIONS" ("SIAQUESTIONID", "START_INDEX", "STOP_INDEX", "SIA_LENGTH") AS 
  SELECT 
  SIAQUESTIONID,START_INDEX,STOP_INDEX, SIA_LENGTH   
FROM 
  SPD_T_SIA_QUESTIONS
 ;
