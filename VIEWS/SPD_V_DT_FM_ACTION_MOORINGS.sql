--------------------------------------------------------
--  File created - Wednesday-May-02-2018   
--------------------------------------------------------
--------------------------------------------------------
--  DDL for View SPD_V_DT_FM_ACTION_MOORINGS
--------------------------------------------------------

  CREATE OR REPLACE VIEW "SPIDERS_DATA"."SPD_V_DT_FM_ACTION_MOORINGS" AS 
  SELECT
    mooringAction.FMACTIONMOORINGID,
    mooringAction.FMMOORINGID,
    mooringAction.INSPECTION_DATE,
    actions.REPORT_NUMBER,
    mooringAction.MOORING_CONDITION_READINESS,
    mooringAction.MOORING_RATED_CONDITION,
    mooringAction.DESIGN_CLASS,
    mooringAction.CURRENT_CLASS,
    mooringAction.BUOY_TYPE,
    mooringAction.ANCHOR_TYPE
  FROM
    SPD_T_FM_ACTION_MOORINGS mooringAction,
    SPD_T_FM_ACTIONS actions
  WHERE mooringAction.FMACTIONID = actions.FMACTIONID;
