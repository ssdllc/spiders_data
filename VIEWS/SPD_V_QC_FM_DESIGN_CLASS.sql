--------------------------------------------------------
--  DDL for View SPD_V_QC_DESIGN_CLASS
--------------------------------------------------------

  CREATE OR REPLACE VIEW "SPIDERS_DATA"."SPD_V_QC_FM_DESIGN_CLASS" AS 
  SELECT DISTINCT
        DESIGN_CLASS
	FROM 
        SPD_T_FM_ACTION_MOORINGS
  WHERE
        DESIGN_CLASS IS NOT NULL
  ORDER BY
        DESIGN_CLASS;