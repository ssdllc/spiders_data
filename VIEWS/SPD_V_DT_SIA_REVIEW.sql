--------------------------------------------------------
--  DDL for View SPD_V_DT_SIA_REVIEW
--------------------------------------------------------

  CREATE OR REPLACE FORCE EDITIONABLE VIEW "SPIDERS_DATA"."SPD_V_DT_SIA_REVIEW" ("DOFID", "ADOID", "APPROVED", "SIASTRUCTUREID", "STRUCTURENO", "FACILITYCARRIED", "STATENAME") AS 
  select 
  sia_structure.dofid dofid,
  sia_structure.adoid adoid,
  decode(
    (select count(sia_data.approved) from spd_t_sia_data sia_data where sia_data.siastructureid = sia_structure.siastructureid and sia_data.approved = 1),119,1,0) approved,
  sia_structure.siastructureid siastructureid,
  (
    select 
      value
    from 
      spd_t_sia_data sia_data 
    where 
      sia_structure.siastructureid = sia_data.siastructureid and 
      sia_data.siaquestionid = (select siaquestionid from spd_t_sia_questions where siarawquestion = 'Item8')) STRUCTURENO ,
  (
    select 
      value
    from 
      spd_t_sia_data sia_data 
    where 
      sia_structure.siastructureid = sia_data.siastructureid and 
      sia_data.siaquestionid = (select siaquestionid from spd_t_sia_questions where siarawquestion = 'Item7')) facilitycarried ,
  (
    select 
      sia_displayvalue.description
    from 
      spd_t_sia_data          sia_data,
      spd_t_sia_displayvalues sia_displayvalue,
      spd_t_sia_questions     sia_question
    where 
      sia_structure.siastructureid = sia_data.siastructureid and 
      sia_displayvalue.siaquestionid = sia_data.siaquestionid and
      sia_displayvalue.siaquestionid = sia_question.siaquestionid and
      sia_question.siarawquestion = 'Item1' and
      sia_displayvalue.code = sia_data.value) statename
from 
  spd_t_sia_structures  sia_structure;
