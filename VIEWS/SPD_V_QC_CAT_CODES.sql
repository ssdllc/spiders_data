--------------------------------------------------------
--  DDL for View SPD_V_QC_CAT_CODES
--------------------------------------------------------

  CREATE OR REPLACE FORCE EDITIONABLE VIEW "SPIDERS_DATA"."SPD_V_QC_CAT_CODES" ("CATEGORYCODEID", "DESCRIPTION", "CATEGORY_CODE", "CAT_SHORT_DESCRIPTION", "CAT_MEDIUM_DESCRIPTION", "CAT_LONG_DESCRIPTION", "MAJ_SHORT_DESCRIPTION", "MAJ_MEDUM_DESCRIPTION", "MAJ_LONG_DESCRIPTION", "FULL_SEARCH_TEXT") AS 
  select 
  category_code.categorycodeid,
  category_code.category_code || ' - ' || category_code.short_desc AS DESCRIPTION,
	category_code.category_code,
	category_code.short_desc			cat_short_description,
	category_code.medium_desc			cat_medium_description,
	category_code.long_desc				cat_long_description,
	major_category_code.short_desc		maj_short_description,
	major_category_code.medium_desc		maj_medum_description,
	major_category_code.long_desc		maj_long_description,
	category_code.category_code || 
	category_code.short_desc ||
	category_code.medium_desc ||	
	category_code.long_desc	||
	major_category_code.short_desc ||
	major_category_code.medium_desc ||
	major_category_code.long_desc 		full_search_text
from
	spd_t_category_codes 		category_code,
	spd_t_major_cat_code 	major_category_code
where
	category_code.major_cat_code = major_category_code.major_cat_code
order by 
  category_code.category_code
 ;
