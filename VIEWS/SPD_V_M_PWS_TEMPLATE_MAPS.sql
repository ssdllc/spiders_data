--------------------------------------------------------
--  DDL for View SPD_V_M_PWS_TEMPLATE_MAPS
--------------------------------------------------------

  CREATE OR REPLACE FORCE EDITIONABLE VIEW "SPIDERS_DATA"."SPD_V_M_PWS_TEMPLATE_MAPS" ("PWSTEMPLATEMAPID", "PWSID", "PWSTEMPLATEID") AS 
  select 
      PWSTEMPLATEMAPID,PWSID,PWSTEMPLATEID
    from 
    SPD_T_PWS_TEMPLATE_MAPS
 ;
