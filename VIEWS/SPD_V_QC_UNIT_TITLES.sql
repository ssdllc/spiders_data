--------------------------------------------------------
--  DDL for View SPD_V_QC_UNIT_TITLES
--------------------------------------------------------

  CREATE OR REPLACE FORCE EDITIONABLE VIEW "SPIDERS_DATA"."SPD_V_QC_UNIT_TITLES" ("DISPLAY_VALUE") AS 
  select distinct(UNIT_TITLE) DISPLAY_VALUE from SPIDERS_DATA.SPD_MV_DATA_ANALYSIS where rownum < 200 order by lower(UNIT_TITLE)
 ;
