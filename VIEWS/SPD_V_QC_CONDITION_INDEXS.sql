--------------------------------------------------------
--  DDL for View SPD_V_QC_CONDITION_INDEXS
--------------------------------------------------------

  CREATE OR REPLACE FORCE EDITIONABLE VIEW "SPIDERS_DATA"."SPD_V_QC_CONDITION_INDEXS" ("DISPLAY_VALUE") AS 
  select distinct(CONDITION_INDEX) DISPLAY_VALUE from SPD_MV_DATA_ANALYSIS order by to_number(CONDITION_INDEX)
 ;
