--------------------------------------------------------
--  DDL for View SPD_V_QC_PLAN_ACTIONS
--------------------------------------------------------

  CREATE OR REPLACE FORCE EDITIONABLE VIEW "SPIDERS_DATA"."SPD_V_QC_PLAN_ACTIONS" ("PLANACTIONID", "PLANID", "ACTIONID", "PRODUCTLINEID", "PRODUCTLINE", "ACTION_TITLE", "ACTIVITY", "STATE", "QTR", "FY", "FUND_SOURCE", "REC_INHOUSE", "REC_KTR", "EST_INHOUSE", "EST_KTR", "TASK_ORDER_COUNT") AS 
  select
  pa.PLANACTIONID,
  pa.PLANID,
  pa.ACTIONID,
  a.PRODUCTLINEID,
  pl.PRODUCTLINE_DISPLAY 	PRODUCTLINE,
	a.TITLE				ACTION_TITLE,
	act.SPD_ACTIVITY_NAME		ACTIVITY,
	act.STATE,
	a.QTR,
	a.FY,
	l.DISPLAYVALUE 		FUND_SOURCE,
	pa.REC_INHOUSE_AMT 	REC_INHOUSE,
	pa.REC_KTR_AMT 		REC_KTR,
	a.INHOUSE_AMT	 	EST_INHOUSE,
	a.KTR_AMT		 	EST_KTR,
	(select count(pdo.PLANDELIVERYORDERID)
		 from
		   SPD_T_PLAN_DELIVERY_ORDERS pdo, SPD_T_PLAN_ACTIONS pdoa
		 where
		   pdo.planactionid = pdoa.planactionid and pdoa.ACTIONID = a.ACTIONID) TASK_ORDER_COUNT
from SPD_T_PLAN_ACTIONS pa,
	 SPD_T_ACTIONS a,
	 spd_t_activities act,
	 spd_t_lists l,
   spd_t_productlines pl
where a.PRODUCTLINEID = pl.PRODUCTLINEID and
    pa.ACTIONID = a.ACTIONID and
	  a.ACTIVITIESID = act.ACTIVITIESID and
	  a.FUNDSOURCE_LISTID = l.LISTID;
