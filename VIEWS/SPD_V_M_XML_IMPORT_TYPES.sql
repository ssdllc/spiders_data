--------------------------------------------------------
--  DDL for View SPD_V_M_XML_IMPORT_TYPES
--------------------------------------------------------

  CREATE OR REPLACE FORCE EDITIONABLE VIEW "SPIDERS_DATA"."SPD_V_M_XML_IMPORT_TYPES" ("IMPORTID", "DESCRIPTION", "OPENINGTAG") AS 
  select 
      IMPORTID,DESCRIPTION,OPENINGTAG
    from 
    SPD_T_XML_IMPORT_TYPES
 ;
