--------------------------------------------------------
--  DDL for View SPD_V_QC_BRIDGE_FACILITY_DO
--------------------------------------------------------

  CREATE OR REPLACE FORCE EDITIONABLE VIEW "SPIDERS_DATA"."SPD_V_QC_BRIDGE_FACILITY_DO" ("DELIVERYORDERID", "DELIVERY_ORDER_NAME", "PRODUCTLINE", "FY", "ADOID", "FACILITYID", "DOFID") AS 
  select DISTINCT
  delivery_order.DELIVERYORDERID        DELIVERYORDERID,
  delivery_order.DELIVERY_ORDER_NAME    DELIVERY_ORDER_NAME,
  'Bridges'       PRODUCTLINE,
  to_char(add_months(delivery_order.ONSITE_START_DATE,3),'YYYY') FY,
  delivery_order.ADOID					ADOID,
  fac.facid FACILITYID,
  do_fac.deliveryorderfacilityid dofid --WE CALL deliveryorderfacilityid dofid in the sia_structure tables
from
  spd_t_delivery_orders delivery_order,
  --spd_t_contracts contract,
  --spd_t_lists display,
  --spd_t_productlines  productline,
  --spd_t_pwss pwss,
  spd_t_delivery_order_facs do_fac,
  spd_t_sia_structures      sia_structure,
  spd_t_sia_data            sia_data,
  spd_t_facilities fac
where
  --delivery_order.pwsid = pwss.pwsid
  --and pwss.contractid = contract.contractid
  --and contract.productline_listid = productline.productlineid
  delivery_order.DELIVERYORDERID = do_fac.DELIVERYORDERID
  and do_fac.facid = fac.facid
  and sia_structure.dofid = do_fac.DELIVERYORDERFACILITYID
  and sia_data.siastructureid = sia_structure.siastructureid;
