--------------------------------------------------------
--  DDL for View SPD_V_QC_SIA_SEARCH_PREVIOUS
--------------------------------------------------------

  CREATE OR REPLACE FORCE EDITIONABLE VIEW "SPIDERS_DATA"."SPD_V_QC_SIA_SEARCH_PREVIOUS" ("PREVIOUS_DOFID", "DOFID") AS 
  select 
  distinct(previous_dofid)  previous_dofid,
  dofid                     dofid
from
  (
    select 
      sia_structure.dofid                       dofid,
      f_getmostrecentdofid(sia_structure.dofid) previous_dofid 
    from 
      spd_t_sia_data            sia_data,
      spd_t_sia_structures      sia_structure
    where 
      sia_data.siastructureid = sia_structure.siastructureid
  )
 ;
