--------------------------------------------------------
--  DDL for View SPD_V_QC_PWS_TEMPLATES
--------------------------------------------------------

  CREATE OR REPLACE FORCE EDITIONABLE VIEW "SPIDERS_DATA"."SPD_V_QC_PWS_TEMPLATES" ("PWSTEMPLATEID", "PRODUCTLINE_LISTID", "TEMPLATE_TITLE") AS 
  select distinct
  pws_template.PWSTEMPLATEID			PWSTEMPLATEID,
	pws_template.PRODUCTLINE_LISTID		PRODUCTLINE_LISTID,
	pws_template.TEMPLATE_TITLE			TEMPLATE_TITLE

from
  spd_t_pws_templates pws_template
ORDER BY
  TEMPLATE_TITLE ASC
 ;
