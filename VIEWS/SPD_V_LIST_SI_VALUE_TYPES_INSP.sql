--------------------------------------------------------
--  DDL for View SPD_V_LIST_SI_VALUE_TYPES_INSP
--------------------------------------------------------

  CREATE OR REPLACE FORCE EDITIONABLE VIEW "SPIDERS_DATA"."SPD_V_LIST_SI_VALUE_TYPES_INSP" ("LISTID", "DISPLAYVALUE", "RETURNVALUE", "GROUPING", "LISTNAME", "VISUALORDER", "PRODUCTLINE_LISTID", "NOTES") AS 
  select listid, displayvalue, returnvalue, grouping, listname, visualorder, productline_listid, notes from spd_t_lists where listname = 'list_special_instructions_inspection_type' order by visualorder
 
 ;
