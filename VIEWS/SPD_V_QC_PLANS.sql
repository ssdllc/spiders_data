--------------------------------------------------------
--  DDL for View SPD_V_QC_PLANS
--------------------------------------------------------

  CREATE OR REPLACE FORCE EDITIONABLE VIEW "SPIDERS_DATA"."SPD_V_QC_PLANS" ("PRODUCTLINEID", "FUNDSOURCE_LISTID", "PLANID", "TITLE", "FY", "FUND_SOURCE", "PRODUCTLINE", "DESCRIPTION") AS 
  select
  	p.PRODUCTLINEID									PRODUCTLINEID,
  	p.FUNDSOURCE_LISTID								FUNDSOURCE_LISTID,
	planid 											PLANID,
	title											TITLE,
	FY												FY,
	l.DISPLAYVALUE 									FUND_SOURCE,
	pl.PRODUCTLINE_DISPLAY 							PRODUCTLINE,
	p.DESCRIPTION									DESCRIPTION
from
	spd_t_plans p,
	spd_t_productlines pl,
	spd_t_lists l
where p.PRODUCTLINEID = pl.PRODUCTLINEID
	  and p.FUNDSOURCE_LISTID = l.LISTID;
