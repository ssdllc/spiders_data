--------------------------------------------------------
--  DDL for View SPD_V_LIST_LABOR_CATEGORIES
--------------------------------------------------------

  CREATE OR REPLACE FORCE EDITIONABLE VIEW "SPIDERS_DATA"."SPD_V_LIST_LABOR_CATEGORIES" ("LISTID", "DISPLAYVALUE", "RETURNVALUE", "GROUPING", "LISTNAME", "VISUALORDER", "PRODUCTLINE_LISTID", "NOTES") AS 
  select listid, displayvalue, returnvalue, grouping, listname, visualorder, productline_listid, notes from spd_t_lists where listname = 'list_facility_labor_categories' order by visualorder
 
 ;
