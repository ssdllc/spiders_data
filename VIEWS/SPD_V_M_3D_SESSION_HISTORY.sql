--------------------------------------------------------
--  DDL for View SPD_V_M_3D_SESSION_HISTORY
--------------------------------------------------------

  CREATE OR REPLACE FORCE EDITIONABLE VIEW "SPIDERS_DATA"."SPD_V_M_3D_SESSION_HISTORY" ("SESSIONHISTORYID", "SESSIONID", "USERNAME", "JOIN_TIMESTAMP") AS 
  select 
      SESSIONHISTORYID,SESSIONID,USERNAME,JOIN_TIMESTAMP
    from 
    SPD_T_3D_SESSION_HISTORY
 ;
