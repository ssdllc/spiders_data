--------------------------------------------------------
--  DDL for View SPD_V_M_3D_WHITEBOARDS
--------------------------------------------------------

  CREATE OR REPLACE FORCE EDITIONABLE VIEW "SPIDERS_DATA"."SPD_V_M_3D_WHITEBOARDS" ("WHITEBOARDID", "SESSIONID", "DESCRIPTION", "TRANSLATION", "ROTATION", "BASE64_IMAGE") AS 
  select 
      WHITEBOARDID,SESSIONID,DESCRIPTION,TRANSLATION,ROTATION,BASE64_IMAGE
    from 
    SPD_T_3D_WHITEBOARDS
 ;
