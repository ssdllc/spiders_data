--------------------------------------------------------
--  DDL for View SPD_V_DT_XML_SENTENCES
--------------------------------------------------------

  CREATE OR REPLACE FORCE EDITIONABLE VIEW "SPIDERS_DATA"."SPD_V_DT_XML_SENTENCES" ("EXECSUMMARYID", "PARAGRAPHID", "SENTENCE") AS 
  select 
 execSum.EXECSUMMARYID,
 para.PARAGRAPHID,
 sent.SENTENCE
from 
 SPD_T_XML_SENTENCES sent
left join SPD_T_XML_ES_PARAGRAPH para
 on sent.PARAGRAPHID=para.PARAGRAPHID
left join SPD_T_XML_EXEC_SUMMARIES execSum
 on para.EXECSUMMARYID=execSum.EXECSUMMARYID
 ;
