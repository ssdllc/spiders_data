--------------------------------------------------------
--  DDL for View SPD_V_QC_FUNDSOURCES
--------------------------------------------------------

  CREATE OR REPLACE FORCE EDITIONABLE VIEW "SPIDERS_DATA"."SPD_V_QC_FUNDSOURCES" ("LISTID", "DISPLAYVALUE", "RETURNVALUE", "VISUALORDER") AS 
  select 
  list.listid       LISTID,
  list.DISPLAYVALUE DISPLAYVALUE,
  list.RETURNVALUE  RETURNVALUE,
  list.VISUALORDER  VISUALORDER 
from
  spd_t_lists list
where
  listname = 'list_fund_sources'
ORDER BY 
  VISUALORDER ASC
 ;
