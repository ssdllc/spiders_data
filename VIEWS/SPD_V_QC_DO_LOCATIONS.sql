--------------------------------------------------------
--  DDL for View SPD_V_QC_DO_LOCATIONS
--------------------------------------------------------

  CREATE OR REPLACE FORCE EDITIONABLE VIEW "SPIDERS_DATA"."SPD_V_QC_DO_LOCATIONS" ("LOCATION_NAME", "LOCATION_UIC", "FACILITY_COUNT", "DELIVERYORDERID") AS 
  select 
  max(do_facility.ACTIVITY)     LOCATION_NAME,
  do_facility.UIC               LOCATION_UIC,
  count(*)                      FACILITY_COUNT,
  do_facility.deliveryorderid   DELIVERYORDERID
from 
  spd_v_qc_do_facilities        do_facility
group by
  do_facility.deliveryorderid,
  do_facility.UIC
ORDER BY 
  FACILITY_COUNT DESC
 ;
