--------------------------------------------------------
--  DDL for View SPD_V_QC_ONSITE_DAYS
--------------------------------------------------------

  CREATE OR REPLACE FORCE EDITIONABLE VIEW "SPIDERS_DATA"."SPD_V_QC_ONSITE_DAYS" ("DISPLAY_VALUE") AS 
  select distinct(ONSITE_PERSONNEL) DISPLAY_VALUE from SPD_MV_DATA_ANALYSIS where rownum < 200 order by to_number(ONSITE_PERSONNEL)
 ;
