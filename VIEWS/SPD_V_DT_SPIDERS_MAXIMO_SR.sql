--------------------------------------------------------
--  DDL for View SPD_V_DT_SPIDERS_MAXIMO_SR
--------------------------------------------------------

  CREATE OR REPLACE FORCE EDITIONABLE VIEW "SPIDERS_DATA"."SPD_V_DT_SPIDERS_MAXIMO_SR" ("SITEID", "DELIVERYORDERID", "FACILITYNAME", "LOCATION", "WORKTYPE", "SUBWORKTYPE", "BUSINESSLINE", "WORKCATEGORY", "SPIDERSUFII", "ROUNDEXPECTEDCOST", "SUBSYSTEM") AS 
  select distinct sr.SITEID, sr.DELIVERYORDERID, sr.FACILITYNAME,
sr.LOCATION,
sr.WORKTYPE,
sr.SUBWORKTYPE,
sr.BUSINESSLINE,
sr.WORKCATEGORY,
sr.SPIDERSUFII,
ROUND(sr.EXPECTEDCOST, 2) ROUNDEXPECTEDCOST,
(select assets.subsystem 
 from SPD_T_SPIDERS_FAC_ASSETS assets
 where sr.asset = assets.assetnum 
   and sr.location=assets.location 
   and subsystem is not null
   and rownum = 1) subsystem
from 
SPD_T_SPIDERS_MAXIMO_SR sr
 ;
