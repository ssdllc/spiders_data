--------------------------------------------------------
--  DDL for View SPD_V_QC_ONSITE_PERSONNEL
--------------------------------------------------------

  CREATE OR REPLACE FORCE EDITIONABLE VIEW "SPIDERS_DATA"."SPD_V_QC_ONSITE_PERSONNEL" ("DISPLAY_VALUE") AS 
  select distinct(ONSITE_PERSONNEL) DISPLAY_VALUE from SPD_MV_DATA_ANALYSIS order by to_number(ONSITE_PERSONNEL)
 ;
