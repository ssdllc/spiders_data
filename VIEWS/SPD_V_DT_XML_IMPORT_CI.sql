--------------------------------------------------------
--  DDL for View SPD_V_DT_XML_IMPORT_CI
--------------------------------------------------------

  CREATE OR REPLACE FORCE EDITIONABLE VIEW "SPIDERS_DATA"."SPD_V_DT_XML_IMPORT_CI" ("ADOID", "NFAID", "FACILITYNAME", "FACILITYNO", "INSPECTION_TYPE", "CI") AS 
  select
  actions.adoid,  facs.NFAID, facs.facilityname, facs.facilityno, exec_table.inspectype INSPECTION_TYPE, exec_table.cirating CI
from spd_t_xml_exec_tables exec_table
  , spd_t_xml_facilities facs
  , SPD_T_XML_ACTIONS actions
where
  exec_table.facilityid = facs.facilityid
  and facs.xmlactionid = actions.xmlactionid
 ;
