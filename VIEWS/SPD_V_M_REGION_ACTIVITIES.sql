--------------------------------------------------------
--  DDL for View SPD_V_M_REGION_ACTIVITIES
--------------------------------------------------------

  CREATE OR REPLACE FORCE EDITIONABLE VIEW "SPIDERS_DATA"."SPD_V_M_REGION_ACTIVITIES" ("REGIONACTIVITYID", "REGIONID", "ACTIVITIESID") AS 
  select 
      REGIONACTIVITYID,REGIONID,ACTIVITIESID
    from 
    SPD_T_REGION_ACTIVITIES
 ;
