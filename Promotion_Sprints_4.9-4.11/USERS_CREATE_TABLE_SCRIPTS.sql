declare
	lc_jJson json;
begin
	lc_jJson := json('{
	"collectionName":"USERS",
	"modelName":"USER",
	"module":"users",
	"path":[],
	"comment":"This table is used to store all users and manage their permissions.",
	"columns":[
	{"name":"USERID","type":"NUMBER","primaryKey":"yes"},
  {"name":"USER_EMAIL","type":"VARCHAR2(64)","primaryKey":""},
	{"name":"USER_PERMISSION","type":"NUMBER(5,2)","primaryKey":""}
	],
	"order":[["USER_EMAIL","DESC"]],
	"dataTable":{}
	}');

	htp.p(pkg_tables.f_make_all_table_objects(lc_jJson));
end;
/