set define off;

insert into SPIDERS_DATA.SPD_T_USERS (USER_EMAIL, USER_PERMISSION) VALUES ('melissa.filsinger@navy.mil',2);
insert into SPIDERS_DATA.SPD_T_USERS (USER_EMAIL, USER_PERMISSION) VALUES ('michael.t.clarke@navy.mil',2);
insert into SPIDERS_DATA.SPD_T_USERS (USER_EMAIL, USER_PERMISSION) VALUES ('thomas.f.cowan.ctr@navy.mil',2);
insert into SPIDERS_DATA.SPD_T_USERS (USER_EMAIL, USER_PERMISSION) VALUES ('gregor.s.bo1@navy.mil',1);
insert into SPIDERS_DATA.SPD_T_USERS (USER_EMAIL, USER_PERMISSION) VALUES ('katherine.e.unger2.ctr@navy.mil',2);
insert into SPIDERS_DATA.SPD_T_USERS (USER_EMAIL, USER_PERMISSION) VALUES ('adam.b.barkley.ctr@navy.mil',2);
insert into SPIDERS_DATA.SPD_T_USERS (USER_EMAIL, USER_PERMISSION) VALUES ('michael.e.russalesi.ctr@navy.mil',2);
	
commit;