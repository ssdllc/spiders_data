
UPDATE SPD_T_REPORTS
SET COUNTRY = STATE
WHERE STATE NOT IN (
                  'ALASKA',
                  'ALABAMA',
                  'ARKANSAS',
                  'AMERICAN SAMOA',
                  'ARIZONA',
                  'CALIFORNIA',
                  'COLORADO',
                  'CONNECTICUT',
                  'DISTRICT OF COLUMBIA',
                  'DELAWARE',
                  'FLORIDA',
                  'GEORGIA',
                  'GUAM',
                  'HAWAII',
                  'IOWA',
                  'IDAHO',
                  'ILLINOIS',
                  'INDIANA',
                  'KANSAS',
                  'KENTUCKY',
                  'LOUISIANA',
                  'MASSACHUSETTS',
                  'MARYLAND',
                  'MAINE',
                  'MICHIGAN',
                  'MINNESOTA',
                  'MISSOURI',
                  'MISSISSIPPI',
                  'MONTANA',
                  'NORTH CAROLINA',
                  ' NORTH DAKOTA',
                  'NEBRASKA',
                  'NEW HAMPSHIRE',
                  'NEW JERSEY',
                  'NEW MEXICO',
                  'NEVADA',
                  'NEW YORK',
                  'OHIO',
                  'OKLAHOMA',
                  'OREGON',
                  'PENNSYLVANIA',
                  'PUERTO RICO',
                  'RHODE ISLAND',
                  'SOUTH CAROLINA',
                  'SOUTH DAKOTA',
                  'TENNESSEE',
                  'TEXAS',
                  'UTAH',
                  'VIRGINIA',
                  'VIRGIN ISLANDS',
                  'VERMONT',
                  'WASHINGTON',
                  'WISCONSIN',
                  'WEST VIRGINIA',
                  'WYOMING');
                  
UPDATE SPD_T_REPORTS
SET COUNTRY = 'UNITED STATES'
WHERE STATE IN (
                  'ALASKA',
                  'ALABAMA',
                  'ARKANSAS',
                  'AMERICAN SAMOA',
                  'ARIZONA',
                  'CALIFORNIA',
                  'COLORADO',
                  'CONNECTICUT',
                  'DISTRICT OF COLUMBIA',
                  'DELAWARE',
                  'FLORIDA',
                  'GEORGIA',
                  'GUAM',
                  'HAWAII',
                  'IOWA',
                  'IDAHO',
                  'ILLINOIS',
                  'INDIANA',
                  'KANSAS',
                  'KENTUCKY',
                  'LOUISIANA',
                  'MASSACHUSETTS',
                  'MARYLAND',
                  'MAINE',
                  'MICHIGAN',
                  'MINNESOTA',
                  'MISSOURI',
                  'MISSISSIPPI',
                  'MONTANA',
                  'NORTH CAROLINA',
                  ' NORTH DAKOTA',
                  'NEBRASKA',
                  'NEW HAMPSHIRE',
                  'NEW JERSEY',
                  'NEW MEXICO',
                  'NEVADA',
                  'NEW YORK',
                  'OHIO',
                  'OKLAHOMA',
                  'OREGON',
                  'PENNSYLVANIA',
                  'PUERTO RICO',
                  'RHODE ISLAND',
                  'SOUTH CAROLINA',
                  'SOUTH DAKOTA',
                  'TENNESSEE',
                  'TEXAS',
                  'UTAH',
                  'VIRGINIA',
                  'VIRGIN ISLANDS',
                  'VERMONT',
                  'WASHINGTON',
                  'WISCONSIN',
                  'WEST VIRGINIA',
                  'WYOMING');