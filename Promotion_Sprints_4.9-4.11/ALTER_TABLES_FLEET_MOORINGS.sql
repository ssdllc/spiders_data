ALTER TABLE SPIDERS_DATA.SPD_T_FM_BUOY_TOP_JEWELRY
DROP CONSTRAINT FK_FM_BTJ_ACTIONMOORINGID;
/

ALTER TABLE SPIDERS_DATA.SPD_T_FM_BUOY_TOPSIDE
DROP CONSTRAINT FK_FM_BT_ACTIONMOORINGID;
/

ALTER TABLE SPIDERS_DATA.SPD_T_FM_INSP_COMMENTS
DROP CONSTRAINT FK_FM_IC_ACTIONMOORINGID;
/

ALTER TABLE SPIDERS_DATA.SPD_T_FM_INSP_MEASUREMENTS
DROP CONSTRAINT FK_FM_IM_ACTIONMOORINGID;
/

ALTER TABLE SPIDERS_DATA.SPD_T_FM_BUOY_TOP_JEWELRY
ADD CONSTRAINT FK_FM_BTJ_ACTIONMOORINGID
  FOREIGN KEY (FMACTIONMOORINGID)
  REFERENCES SPIDERS_DATA.SPD_T_FM_ACTION_MOORINGS(FMACTIONMOORINGID)
  ON DELETE CASCADE;
/
  
ALTER TABLE SPIDERS_DATA.SPD_T_FM_BUOY_TOPSIDE
ADD CONSTRAINT FK_FM_BT_ACTIONMOORINGID
  FOREIGN KEY (FMACTIONMOORINGID)
  REFERENCES SPIDERS_DATA.SPD_T_FM_ACTION_MOORINGS(FMACTIONMOORINGID)
  ON DELETE CASCADE;
/
  
ALTER TABLE SPIDERS_DATA.SPD_T_FM_INSP_COMMENTS
ADD CONSTRAINT FK_FM_IC_ACTIONMOORINGID
  FOREIGN KEY (FMACTIONMOORINGID)
  REFERENCES SPIDERS_DATA.SPD_T_FM_ACTION_MOORINGS(FMACTIONMOORINGID)
  ON DELETE CASCADE;
/
  
ALTER TABLE SPIDERS_DATA.SPD_T_FM_INSP_MEASUREMENTS
ADD CONSTRAINT FK_FM_IM_ACTIONMOORINGID
  FOREIGN KEY (FMACTIONMOORINGID)
  REFERENCES SPIDERS_DATA.SPD_T_FM_ACTION_MOORINGS(FMACTIONMOORINGID)
  ON DELETE CASCADE;
/