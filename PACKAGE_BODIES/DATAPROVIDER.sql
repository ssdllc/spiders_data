--------------------------------------------------------
--  DDL for Package Body DATAPROVIDER
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE PACKAGE BODY "SPIDERS_DATA"."DATAPROVIDER" AS

/*
This software has been released under the MIT license:

  Copyright (c) 2010 Synergy Software Design, LLC

  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.
*/


function get_caption (IN_nID number) return varchar2

as 
lc_vReturn varchar2(255) := 'No Caption';
begin

for rec in (select caption from spd_t_do_supporting_files a  where a.dosfid = IN_nID)
loop
  lc_vReturn := rec.caption;

end loop;

return lc_vReturn;

end;



function test_bfile (IN_nReportId number) return number

as 

lc_nReturn number := 0;

begin

for rec in (select the_bfile from spd_t_reports where reportid = in_nReportid and the_bfile is not null)
loop
  lc_nReturn := dbms_lob.fileexists(rec.the_bfile);
end loop;

return lc_nReturn;

end;


procedure facilities(IN_UIC varchar2 default '', IN_PL number default 100085, IN_SA varchar2 default '', IN_FACLIST varchar2 default '', IN_nDOID varchar2 default '0')

is 
  thesql varchar2(3000);
  incodes varchar2(4000);
  ret JSON;
  extraWhere varchar2(2000) := '';
  duh varchar2(20);
  inStatement varchar2(2000);
begin

  duh := '^' || IN_UIC || '^';
  
  if IN_PL = 100085 then
      incodes :='''15110'',''15120'',''15130'',''15140'',''15150'',''15160'',''15170'',''15171'',''15180'',''15190'',''15210'',''15220'',''15240'',''15250'',''15260'',''15410'',''15420'',''15430'',''15520'',''16410'',''16420'',''21310'',''75060'',''75061''';
      inStatement := 'fac.prime_use_category_code in ( ' || incodes || ')';
  end if;
 
  if IN_PL = 100086 then
      inStatement := '(
          prime_use_category_code like ''841%''
          or prime_use_category_code like ''844%''
          or prime_use_category_code like ''843%''
        )';
  end if;
  
  if IN_PL = 100087 then
      incodes :='''85120'',''86030'',''85230''';
      inStatement := 'fac.prime_use_category_code in ( ' || incodes || ')';
  end if;
  
  if length(IN_FACLIST) > 0 then
      --maybe put some validation on this ...
      extraWhere := 'and fac.facility_id in (' || IN_FACLIST || ')';
  end if;
  
  if length(IN_SA) > 0 then
    extraWhere := extraWhere || ' and fac.special_area_code = ' || IN_SA;
  end if;
  
  if  length(IN_UIC) > 0 then
    extraWhere := extraWhere || ' and fac.activity_uic = ' || replace(duh,'^','''');  
  end if;  
  
  thesql := 
  'select 
      sfac.facid,
      sfac.spd_fac_name spiders_facname,
      sfac.spd_fac_no spiders_facno,
      fac.facility_id,
      fac.facility_name,
      fac.facility_no,
      fac.activity_uic,
      fac.special_area_code,
      actv.unit_title,
      sa.special_area_name, 
      decode(state.state_abbr,null,country.description,state.state_abbr) state_abbr,
      state.state_abbr,
      country.description,
      loc.latitude,
      loc.longitude,
      fac.prime_use_category_code,
      fac.maintenance_resp_uic,
      fac.condition_rating ci,
      fac.mission_dependency_index mdi,
      fac.mdi_date,
      fac.engineering_eval_date,
      ( select 
          dofid 
        from 
          spd_t_do_facilities dofac
        where 
          dofac.facid = sfac.facid
          and adoid  = ' || IN_nDOID || ') dofac
    from 
      spd_mv_inf_facility fac,
      spd_mv_inf_cur_activities actv,
      spd_mv_inf_cur_actv_spec_area sa,
      spd_t_facilities sfac,
      spd_t_fac_geom loc,
      spd_mv_inf_state_code state,
      spd_mv_inf_country_code country
    where 
      actv.state_code = state.state_code
      and state.country_code = country.country_code
      and actv.country_code = country.country_code
      and loc.facid(+) = sfac.facid
      and sfac.infads_facility_id(+) = fac.facility_id
      and sa.uic = actv.uic
      and (
            decode(fac.special_area_code,null,-1) = decode(sa.special_area_code,null,-1)
            or
            fac.special_area_code = sa.special_area_code
          )
            
      and fac.activity_uic = sa.uic
      --and fac.activity_uic = actv.uic
      and ' || inStatement || '  
      '
      || extraWhere || 
      '
      and actv.productline_listid = ' || IN_PL || '
    order by 
      actv.activity_title, 
      fac.facility_name';
--htp.p(thesql);
ret := json_dyn.executeObject(thesql);
ret.htp;
end facilities;

procedure facilities_search(IN_SEARCH varchar2 default '', IN_PL number default 100085)

is 
  thesql varchar2(3000);
  incodes varchar2(4000);
  ret JSON;
  extraWhere varchar2(2000) := '';
  duh varchar2(20);
  inStatement varchar2(2000);
begin

  
  
  if IN_PL = 100085 then
      incodes :='''15110'',''15120'',''15130'',''15140'',''15150'',''15160'',''15170'',''15171'',''15180'',''15190'',''15210'',''15220'',''15240'',''15250'',''15260'',''15410'',''15420'',''15430'',''15520'',''16410'',''16420'',''21310'',''75060'',''75061''';
      inStatement := 'fac.prime_use_category_code in ( ' || incodes || ')';
  end if;
 
  if IN_PL = 100086 then
      inStatement := '(
          prime_use_category_code like ''841%''
          or prime_use_category_code like ''844%''
          or prime_use_category_code like ''843%''
        )';
  end if;
  
  if IN_PL = 100087 then
      incodes :='''85120'',''86030'',''85230''';
      inStatement := 'fac.prime_use_category_code in ( ' || incodes || ')';
  end if;
  
  
  
  thesql := 
  'select 
      sfac.facid,
      sfac.spd_fac_name spiders_facname,
      sfac.spd_fac_no spiders_facno,
      fac.facility_id,
      fac.facility_name,
      fac.facility_no,
      fac.activity_uic,
      fac.special_area_code,
      actv.unit_title,
      sa.special_area_name, 
      decode(state.state_abbr,null,country.description,state.state_abbr) state_abbr,
      state.state_abbr,
      country.description,
      loc.latitude,
      loc.longitude,
      fac.prime_use_category_code,
      fac.maintenance_resp_uic,
      fac.condition_rating ci,
      fac.mission_dependency_index mdi,
      fac.mdi_date,
      fac.engineering_eval_date      
    from 
      spd_mv_inf_facility fac,
      spd_mv_inf_activity actv,
      spd_mv_inf_cur_actv_spec_area sa,
      spd_t_facilities sfac,
      spd_t_fac_geom loc,
      spd_mv_inf_state_code state,
      spd_mv_inf_country_code country
    where 
      actv.state_code = state.state_code
      and state.country_code = country.country_code
      and actv.country_code = country.country_code
      and loc.facid(+) = sfac.facid
      and sfac.infads_facility_id(+) = fac.facility_id
      and sa.uic = actv.uic
      and (
            decode(fac.special_area_code,null,-1) = decode(sa.special_area_code,null,-1)
            or
            fac.special_area_code = sa.special_area_code
          )
            
      and fac.activity_uic = sa.uic
      --and fac.activity_uic = actv.uic
      and fac.facility_id in (''' || IN_SEARCH || ''')
      
      
    order by 
      actv.activity_title, 
      fac.facility_name';
      
--htp.p(thesql);
ret := json_dyn.executeObject(thesql);
ret.htp;
end facilities_search;

procedure activities (IN_PL number default 100085)  

is 
    lc_vSql varchar2(2000);
  
begin

  
  lc_vSql := 
'select 
  unit_title, 
  uic, 
  decode(state_abbr,null,country,state_abbr) state_abbr, 
  country,
  count_of_facs
from 
  spd_mv_inf_cur_activities cur_actv
where 
  productline_listid = ' || IN_PL || ' order by unit_title';
  
  
    tobrowser.data(lc_vSql);
    
    exception
        when others then
        tobrowser.message('error', 'error retrieving activity facilities: ' || SQLERRM
        );
        
end;



        
        


procedure states(IN_PL number default 100085) is 
  ret JSON;
  theSql varchar2(4000);
begin

thesql := 
'select * from (
                  select
                    decode(state_abbr,null,country,state_abbr) state_abbr,
                    state,
                    decode(country,''UNITED STATES'',0,1) country,
                    row_number() over (partition by country, state_abbr order by  country, state_abbr) rn
                  from 
                    spd_mv_inf_cur_activities 
                  where 
                    productline_listid = ' || IN_PL || '
                  )
                  where 
                    rn = 1
                  order by
                    country, 
                    state_abbr
                ';

ret := json_dyn.executeObject(thesql);
ret.htp;

end;


procedure special_areas(IN_UIC varchar2) is 
  ret JSON;
  theSql varchar2(4000);
  duh varchar2(20);
begin

duh := '^' || IN_UIC || '^';
thesql := 'select * from spd_mv_inf_cur_actv_spec_area where uic = ' ||  replace(duh,'^','''');  

ret := json_dyn.executeObject(thesql);
ret.htp;

end;







procedure deliveryOrder(IN_DOID number default 100000) 

is 
  messages json;
  LOVS json;
  SpecialInstructions json_list;
  DirectCostsDO json;
  DirectCostsFac json;
  FundSources json;
  YesNo json;
  DrawingOptions json;
  AssetInventoryOptions json;
  InspectionTypes json;
  Milestones json;
  FileTypes json;
  ActionPlannerTypes json;
  ProductLines json;
  lovDeliverables json;
  Action json;
  DeliveryOrder json;
  Progress json;
  Deliverables json;
  do_facs json;
  do_fac json;
  facdc json;
  specinstr json;
  DO_DC json;
  i number;
  
  --in_apid number := 10000;

begin
  messages := json();
  LOVS := json();
  SpecialInstructions := json_list();
  DirectCostsDO := json();
  DirectCostsFac := json();
  FundSources := json();
  YesNo := json();
  DrawingOptions := json();
  AssetInventoryOptions := json();
  InspectionTypes := json();
  Milestones := json();
  FileTypes := json();
  ActionPlannerTypes := json();
  ProductLines := json();
  lovDeliverables := json();
  Action := json();
  DeliveryOrder := json();
  Progress := json();
  Deliverables := json();
  do_facs := json();
  do_fac := json();
  facdc := json();
  specinstr := json();
  DO_DC := json();
  
  SpecialInstructions := json_dyn.executeList('select * from spd_v_list_si');
  DirectCostsDO := json_dyn.executeObject('select * from spd_v_list_direct_costs');
  DirectCostsFac := json_dyn.executeObject('select * from spd_v_list_direct_costs');
  FundSources := json_dyn.executeObject('select * from spd_v_list_fund_sources');
  YesNo := json_dyn.executeObject('select * from spd_v_list_si_value_types_yn');
  DrawingOptions := json_dyn.executeObject('select * from spd_v_list_si_value_types_dwg');
  AssetInventoryOptions := json_dyn.executeObject('select * from spd_v_list_si_value_types_ai');
  InspectionTypes := json_dyn.executeObject('select * from spd_v_list_si_value_types_insp');
  Milestones := json_dyn.executeObject('select * from spd_v_list_do_milestones');
  FileTypes := json_dyn.executeObject('select * from spd_v_list_do_file_types');
  ActionPlannerTypes := json_dyn.executeObject('select * from spd_v_list_action_types');
  ProductLines := json_dyn.executeObject('select * from spd_v_list_product_lines');
  lovDeliverables := json_dyn.executeObject('select * from spd_v_list_do_deliverables');
  
  
  
  Action := json_dyn.executeObject('select * from spd_t_action_planner where apid in (select apid from spd_t_actn_dos where adoid = ' || in_doid || ')');
    DeliveryOrder := json_dyn.executeObject('select ADOID, APID, FUND_SOURCE_LISTID, REPORTID, to_char(start_date,''YYYY-MM-DD'') START_DATE, to_char(end_date,''YYYY-MM-DD'') END_DATE, CONTRACT_NUMBER, DELIVERY_ORDER_NUMBER, KTR_SUBMITTAL_DATE, SPIDERS_DATA_VERSION, CONTRACTOR, WORKORDERID, DELIVERY_ORDER_NAME from spd_t_actn_dos where adoid = ' || in_doid);
    for rec_delivery_order in (select * from spd_t_actn_dos where adoid = in_doid) loop
      DO_DC := json_dyn.executeObject('select * from spd_t_lists where listid in (select directcost_listid from spd_t_do_ce_dc where adoid = ' || in_doid  || ') order by visualorder');
      Progress  :=  json_dyn.executeObject('select * from spd_t_do_progress where adoid = ' || rec_delivery_order.adoid);
      Deliverables := json_dyn.executeObject('select * from spd_t_do_deliverables where adoid = ' || rec_delivery_order.adoid);    
     /*
      i := 1;
      do_fac := json_dyn.executeObject('select * from spd_t_do_facilities where adoid = ' || rec_delivery_order.adoid);

      for rec_do_facs in (select * from spd_t_do_facilities where adoid = rec_delivery_order.adoid) loop
        facdc := json_dyn.executeObject('select spd_t_do_facility_ce_dc.* from spd_t_do_facility_ce_dc where dofid = ' || rec_do_facs.dofid);
        specinstr := json_dyn.executeObject('select * from spd_t_do_fac_spec_inst where dofid = ' || rec_do_facs.dofid);
        json_ext.put(do_fac, 'data[' || i || '][15].facdc', facdc);
        json_ext.put(do_fac, 'data[' || i || '][15].specinstr', specinstr);
        i := i + 1;
        
        
        --do_fac.put('facdc',facdc);
        --do_fac.put('specinstr',specinstr);
      end loop;
      
      do_facs.put('do_fac',do_fac);
    */
    end loop;
  
  DeliveryOrder.put('do_facs',do_facs);
  DeliveryOrder.put('Deliverables',Deliverables);
  DeliveryOrder.put('Progress',Progress);
  DeliveryOrder.put('DirectCosts',DO_DC);
  Action.put('DeliveryOrder',DeliveryOrder);
  

  messages.put('Action',Action);
  messages.htp;

exception
        when others then
        htp.p('error: ' || SQLERRM
        );
end;




procedure LOVS(IN_PL number default 100085)

is
  messages json;
  LOVS json;
  SpecialInstructions json_list;
  DirectCostsDO json;
  DirectCostsFac json;
  FundSources json;
  YesNo json;
  DrawingOptions json;
  AssetInventoryOptions json;
  InspectionTypes json;
  Milestones json;
  FileTypes json;
  ActionPlannerTypes json;
  ProductLines json;
  lovDeliverables json;
  Action json;
  DeliveryOrder json;
  Progress json;
  Deliverables json;
  do_facs json;
  do_fac json;
  facdc json;
  specinstr json;
  DO_DC json;
  i number;
  
  --in_apid number := 10000;

begin
  LOVS := json();
  SpecialInstructions := json_list();
  DirectCostsDO := json();
  DirectCostsFac := json();
  FundSources := json();
  YesNo := json();
  DrawingOptions := json();
  AssetInventoryOptions := json();
  InspectionTypes := json();
  Milestones := json();
  FileTypes := json();
  ActionPlannerTypes := json();
  ProductLines := json();
  lovDeliverables := json();
  
  SpecialInstructions := json_dyn.executeList('select * from spd_v_list_si');
  DirectCostsDO := json_dyn.executeObject('select * from spd_v_list_direct_costs');
  DirectCostsFac := json_dyn.executeObject('select * from spd_v_list_direct_costs');
  FundSources := json_dyn.executeObject('select * from spd_v_list_fund_sources');
  YesNo := json_dyn.executeObject('select * from spd_v_list_si_value_types_yn');
  DrawingOptions := json_dyn.executeObject('select * from spd_v_list_si_value_types_dwg');
  AssetInventoryOptions := json_dyn.executeObject('select * from spd_v_list_si_value_types_ai');
  InspectionTypes := json_dyn.executeObject('select * from spd_v_list_si_value_types_insp');
  Milestones := json_dyn.executeObject('select * from spd_v_list_do_milestones');
  FileTypes := json_dyn.executeObject('select * from spd_v_list_do_file_types');
  ActionPlannerTypes := json_dyn.executeObject('select * from spd_v_list_action_types');
  ProductLines := json_dyn.executeObject('select * from spd_v_list_product_lines');
  lovDeliverables := json_dyn.executeObject('select * from spd_v_list_do_deliverables');
  
  
  
  
  LOVS.put('SpecialInstructions',SpecialInstructions);
  LOVS.put('DirectCostsDO',DirectCostsDO);
  LOVS.put('DirectCostsFac',DirectCostsFac);
  LOVS.put('FundSources',FundSources);
  LOVS.put('YesNo',YesNo);
  LOVS.put('DrawingOptions',DrawingOptions);
  LOVS.put('AssetInventoryOptions',AssetInventoryOptions);
  LOVS.put('InspectionTypes',InspectionTypes);
  LOVS.put('Milestones',Milestones);
  LOVS.put('FileTypes',FileTypes);
  LOVS.put('ActionPlannerTypes',ActionPlannerTypes);
  LOVS.put('ProductLines',ProductLines);
  LOVS.put('lovDeliverables',lovDeliverables);
  
  LOVS.htp;

exception
        when others then
        htp.p('error: ' || SQLERRM
        );
end;


procedure facs(IN_DOID number default 100000)

is
  lc_vSql varchar2(4000);
begin


  lc_vSql := 
'select 
  sfac.facid,
  sfac.spd_fac_name spiders_facname,
  sfac.spd_fac_no spiders_facno,
  fac.facility_id,
  fac.facility_name,
  fac.facility_no,
  fac.activity_uic,
  fac.special_area_code, 
  dofac.dofid
from 
  spd_mv_inf_facility fac,
  spd_t_facilities sfac,
  spd_t_do_facilities dofac
where 
  sfac.infads_facility_id(+) = fac.facility_id
  and dofac.facid = sfac.facid
  and dofac.adoid = ' || IN_DOID || '
order by 
  fac.facility_name';

tobrowser.data(lc_vSql);
      
  
  exception
        when others then
        tobrowser.message('error', 'error retrieving delivery order facilities: ' || to_char(in_doid) || ':' || SQLERRM
        );
end;


procedure fac(IN_DOFID number default 100000) 


is 

  do_fac json;
  facdc json;
  specinstr json;
  infads json;
  i number :=1;
  lc_nColCount number;
begin
  --get the column count of the main data query to dynamically append specinstr and facdc
  select count(*) into lc_nColCount from user_tab_cols  where table_name = 'SPD_T_DO_FACILITIES';
  lc_nColCount := lc_nColCount + 1;--This accounts for adding the row number into the query below...testing.
  do_fac := json();
  facdc  := json();
  specinstr := json();
 
  
  do_fac := json_dyn.executeObject('
  select  fac.*, rownum 
  from
  (select dofac.* from 
    spd_t_do_facilities dofac,
    spd_t_facilities sfac,
    spd_mv_inf_facility ifac,
    spd_mv_inf_activity actv
  where 
    dofac.facid = sfac.facid
    and sfac.infads_facility_id = ifac.facility_id (+)
    and ifac.activity_uic = actv.uic
    and dofac.dofid= ' || IN_DOFID || '
  order by 
    actv.unit_title,
    sfac.spd_fac_name) fac');

  for rec_do_facs in 
  (
  select  fac.*, rownum 
  from
  (select actv.unit_title, dofac.* from 
    spd_t_do_facilities dofac,
    spd_t_facilities sfac,
    spd_mv_inf_facility ifac,
    spd_mv_inf_activity actv
  where 
    dofac.facid = sfac.facid
    and sfac.infads_facility_id = ifac.facility_id (+)
    and ifac.activity_uic = actv.uic
    and dofac.dofid = IN_DOFID
  order by 
    actv.unit_title,
    sfac.spd_fac_name) fac
  ) loop
  
    
    facdc := json_dyn.executeObject('select dofacdc.*, lists.displayvalue from spd_t_do_facility_ce_dc dofacdc, spd_t_lists lists where dofacdc.directcost_listid = lists.listid and dofid = ' || rec_do_facs.dofid || ' order by lists.visualorder');
    specinstr := json_dyn.executeObject('select facsi.*, lists.notes, lists.displayvalue from spd_t_do_fac_spec_inst facsi, spd_t_lists lists where facsi.spec_instr_listid = lists.listid and dofid= ' || rec_do_facs.dofid || ' order by lists.visualorder');
    infads := json_dyn.executeObject(
    'select 
      sfac.facid,
      sfac.spd_fac_name spiders_facname,
      sfac.spd_fac_no spiders_facno,
      fac.facility_id,
      fac.facility_name,
      fac.facility_no,
      fac.activity_uic,
      fac.special_area_code,
      actv.unit_title,
      sa.special_area_name, 
      decode(actv.state_abbr,null,actv.country,actv.state_abbr) state_abbr,
      actv.state,
      actv.country,
      loc.latitude,
      loc.longitude
    from 
      spd_mv_inf_facility fac,
      spd_mv_inf_cur_activities actv,
      spd_mv_inf_cur_actv_spec_area sa,
      spd_t_facilities sfac,
      spd_t_fac_geom loc
    where 
      loc.facid(+) = sfac.facid
      and sfac.infads_facility_id(+) = fac.facility_id
      and sa.uic = actv.uic
      and (
            decode(fac.special_area_code,null,-1) = decode(sa.special_area_code,null,-1)
            or
            fac.special_area_code = sa.special_area_code
          )
            
      and fac.activity_uic = sa.uic
      and sfac.facid = ' || rec_do_facs.facid);
      
    json_ext.put(do_fac, 'data[' || i || '][' || to_char(lc_nColCount) || '].facdc', facdc);
    json_ext.put(do_fac, 'data[' || i || '][' || to_char(lc_nColCount) || '].specinstr', specinstr);
    json_ext.put(do_fac, 'data[' || i || '][' || to_char(lc_nColCount) || '].infads', infads);
    i := i + 1;
    
    
    --do_fac.put('facdc',facdc);
    --do_fac.put('specinstr',specinstr);
  end loop; 
  do_fac.htp;
  
  
  exception
        when others then
        htp.p('error: ' || SQLERRM
        );
end;



procedure plannedactions(IN_PL varchar2 default '100085', IN_YEAR varchar2 default '2011')

is 
    lc_vSql varchar2(2000);
begin
  lc_vSql := 
  'select 
  ap.apid,
  ap.setplanid,
  ap.planned_year,
  ap.planned_fund_source_listid,
  ap.planned_fund_source_list,
  ap.planned_action_type_listid,
  ap.planned_action_type_list,
  ap.planned_inhouse_amt,
  ap.planned_ktr_amt,
  ap.planned_activitiesid,
  ap.productline_listid,
  (select decode(money_approved,null,'''',money_approved) from spd_t_list_set_plans sp where sp.setplanid = (select setplanid from spd_t_inter_set_plans isp where isp.apid = ap.apid)) funding_request_flag,
  ap.planned_qtr,
  (select displayvalue from spd_t_lists where listid = ap.planned_fund_source_listid) FUNDSOURCE,
  (select displayvalue from spd_t_lists where listid = ap.productline_listid) PL,
  (select count(*) from spd_t_actn_dos where apid = ap.apid) DeliveryOrders,
  (select spd_activity_name from spd_t_activities where activitiesid = ap.planned_activitiesid) Activity,
  (select state from spd_t_activities where activitiesid = ap.planned_activitiesid) State,
  (select country from spd_t_activities where activitiesid = ap.planned_activitiesid) Country,
  (select plantitle from spd_t_list_set_plans sp where sp.setplanid = (select setplanid from spd_t_inter_set_plans isp where isp.apid = ap.apid)) plantitle
from
  spd_t_action_planner ap
where 
  ap.productline_listid in ( ' || IN_PL || ') and ap.planned_year in ( ' || IN_YEAR || ')
order by
  (select state from spd_t_activities where activitiesid = ap.planned_activitiesid),
  (select spd_activity_name from spd_t_activities where activitiesid = ap.planned_activitiesid),
  ap.apid';
  
  
 
  
    tobrowser.data(lc_vSql);
    
    exception
        when others then
        tobrowser.message('error', 'error retrieving planned actions per productline: ' || IN_PL || ':' || SQLERRM
        );
end;


procedure planned_action_dos(IN_APID number)

is 

thesql varchar2(2000);
begin



thesql := 
'select
  ADOID,
  APID,
  CONTRACT_LISTID,
  REPORTID,
  START_DATE,
  END_DATE,
  (select displayvalue from spd_t_lists where listid = contract_listid) CONTRACT_NUMBER,
  DELIVERY_ORDER_NUMBER,
  KTR_SUBMITTAL_DATE,
  SPIDERS_DATA_VERSION,
  CONTRACTOR,
  WORKORDERID,
  DELIVERY_ORDER_NAME,
  (select count(*) from spd_t_do_facilities where adoid = do.adoid) Facilities,
  (select 0 from spd_t_do_progress where adoid = do.adoid) Progress,
  noteskey,
  worktype_listid,
  (select displayvalue from spd_t_lists where listid = worktype_listid) WORK_TYPE
from
  spd_t_actn_dos do
  where apid = ' || IN_APID ;

tobrowser.data(thesql);

    exception
        when others then
        tobrowser.message('error', 'error retrieving planned action delivery orders: ' || to_char(IN_APID) || ':' || SQLERRM
        );
end;

procedure planned_action_do_exec_summary(IN_ADOID number)

is 

lc_jPlannedActionsDOS json;
thesql varchar2(2000);
begin



thesql := 
'select
  executive_summary
from
  spd_t_actn_dos do
  where adoid = ' || IN_ADOID ;

lc_jPlannedActionsDOS := json_dyn.executeObject(thesql);
lc_jPlannedActionsDOS.htp;
end;



procedure do_deliverable_files(IN_nADOID number)

is

lc_jDOFiles json;
thesql varchar2(2000);
begin



thesql := 
'select
  XMLFILEID,
  FILENAME,
  CONTENT_TYPE,
  FILE_TYPE
from
  spd_t_xml_files files
  where adoid = ' || IN_nADOID || 
  ' and file_type=''deliverable''';

lc_jDOFiles := json_dyn.executeObject(thesql);
lc_jDOFiles.htp;
end;

procedure do_costestimate_files(IN_nADOID number)

is

lc_jDOFiles json;
thesql varchar2(2000);
begin



thesql := 
'select
  XMLFILEID,
  FILENAME,
  CONTENT_TYPE,
  FILE_TYPE
from
  spd_t_xml_files files
  where adoid = ' || IN_nADOID || 
  ' and file_type=''costestimate''';

lc_jDOFiles := json_dyn.executeObject(thesql);
lc_jDOFiles.htp;
end;


procedure display_ufii(in_nDOFID number, in_vLevel varchar2 default '')

is
lc_jUFII json;
thesql varchar2(2000);
begin



thesql := 
'select 
  codes.displayvalue,
  codes.returnvalue,
  ai.asset_type,ai.condition_index,
  ai.aiid,
  ai.repair_cost_estimate,
  ai.est_repair_condition_index,
  ai.quantity_of_units,
  ai.unit_type,
  ai.count_of_assets,
  ai.criticality_factor,
  codes.listid,
  codes.grouping,
  ai.parent_aiid  
from 
  spd_t_do_fac_asset_inventory ai,
  spd_v_list_codes codes
where 
  dofid = ' || in_nDOFID || ' and
  ai.code_listid = codes.listid
order by
  codes.visualorder';

lc_jUFII := json_dyn.executeObject(thesql);
lc_jUFII.htp;
end;


procedure do_direct_costs(IN_nADOID number)

is
lc_jDODC json;
thesql varchar2(4000);
begin

thesql := 
'select
  ce_dc.docedcid,
  ce_dc.adoid,
  ce_dc.directcost_listid,
  list.displayvalue,
  list.returnvalue,
  list.visualorder,
  list.grouping
from
  spd_t_do_ce_dc ce_dc,
  spd_t_lists list
where 
  ce_dc.directcost_listid = list.listid
  and ce_dc.adoid = ' || IN_nADOID;

lc_jDODC := json_dyn.executeObject(thesql);
lc_jDODC.htp;

end;


procedure do_confirmations(IN_nADOID number)

is
lc_jDODC json;
thesql varchar2(4000);
begin

thesql := 
'select
  confirm.cid,
  confirm.adoid,
  confirm.confirmation_name_listid,
  confirm.confirm,
  confirm.timestamp,
  list.displayvalue,
  list.returnvalue,
  list.visualorder,
  list.grouping,
  list.listname,
  confirm.created_by,
  list.notes
from
  spd_t_confirmation confirm,
  spd_t_lists list
where 
  confirm.confirmation_name_listid = list.listid
  and confirm.adoid = ' || IN_nADOID;

lc_jDODC := json_dyn.executeObject(thesql);
lc_jDODC.htp;

end;

procedure get_set_plan_types

is
lc_jSetPlanTypes json;
thesql varchar2(4000);
begin

thesql := 
'select
  *
  from 
    spd_t_lists
  where
    listname = ''set_plan_types''
  order by
    visualorder';

lc_jSetPlanTypes := json_dyn.executeObject(thesql);
lc_jSetPlanTypes.htp;

end;

procedure get_funding_documents(IN_nYEAR number, IN_nFUNDSOURCE number)

is 
    lc_vSql varchar2(2000);
begin
  lc_vSql := 
  'select
  sp.*,
  (select count(*) from spd_t_inter_set_plans where setplanid = sp.setplanid) count_of_actions
from 
  spd_t_list_set_plans sp
where
  sp.basefy = ' || IN_nYEAR || '
  and sp.fundsource_listid = ' || IN_nFUNDSOURCE || '
  and (select count(*) from spd_t_inter_set_plans where setplanid = sp.setplanid) > 0
order by
  basefy,
  fundsource_listid,
  plantitle
  ';

 
  
    tobrowser.data(lc_vSql);
    
    exception
        when others then
        tobrowser.message('error', 'error retrieving funding requests per year: IN_nYEAR:' || to_char(IN_nYEAR) || ',IN_nFUNDSOURCE:' || to_char(IN_nFUNDSOURCE) || ':' || SQLERRM
        );

end;


procedure get_GCE_list

is

lc_jGCEList json;
thesql varchar2(4000);
begin

thesql := 
'select
  *
from 
  spd_t_lists l,
  spd_t_do_ce_files c
where
  listname = ''GCE_list''
  and l.listid = c.ce_listid (+)
order by
  visualorder
  ';


lc_jGCEList := json_dyn.executeObject(thesql);
lc_jGCEList.htp;


end;

procedure get_uploaded_gce_docs(in_nADOID number)

is 
lc_jGCEList json;
thesql varchar2(4000);
begin

thesql := 
'select
  l.*,
  (select fileskey from spd_t_do_ce_files c where  c.adoid = ' || in_nADOID || ' and c.ce_listid = l.listid) fileskey
from 
  spd_t_lists l
where
  listname = ''GCE_list''
order by
  visualorder
  ';


lc_jGCEList := json_dyn.executeObject(thesql);
lc_jGCEList.htp;
end;

procedure get_deliverables

is 

lc_jDODeliverables json;
thesql varchar2(4000);
begin

thesql := 
'select
  *
from 
  spd_t_lists l,
  spd_t_do_deliverables d
where
  listname = ''Deliverables_list''
  and l.listid = d.deliverables_listid (+)
order by
  visualorder
  ';

lc_jDODeliverables := json_dyn.executeObject(thesql);
lc_jDODeliverables.htp;


end;

procedure get_uploaded_deliverable_docs(in_nADOID number)

is 
lc_jGCEList json;
thesql varchar2(4000);
begin

thesql := 
'select
  l.*,
  (select fileskey from spd_t_do_ce_files c where  c.adoid = ' || in_nADOID || ' and c.ce_listid = l.listid) fileskey
from 
  spd_t_lists l
where
  listname = ''Deliverables_list''
order by
  visualorder
  ';


lc_jGCEList := json_dyn.executeObject(thesql);
lc_jGCEList.htp;
end;


procedure funding_requests(IN_YEAR varchar2 default '2011')

is 

lc_jFundingRequests json;
lc_jFRVersions json;
lc_jFRNotes json;
thesql varchar2(2000);
lc_nFundingRequest number :=1;
begin

----------------------


  thesql := 
  'select
    SETPLANID,
    SET_PLAN_TYPE_LISTID,
    EXPORTFILE,
    BASEFY,
    CREATED_ON,
    CREATED_BY,
    PLANTITLE,
    MONEY_APPROVED,
    MONEY_APPROVED_DATE,
    MONEY_APPROVED_AMT_INH,
    MONEY_APPROVED_AMT_KTR,
    NOTESKEY,
    SET_PLAN_AMT_INH,
    SET_PLAN_AMT_KTR,
    ENDFY,
    FUNDSOURCE_LISTID

  from
    spd_t_list_set_plans
  where
    setplanid in (
                    select 
                      sp.setplanid 
                    from 
                      spd_t_list_set_plans sp, 
                      spd_t_inter_set_plans inter 
                    where 
                      basefy = ' || IN_YEAR || ' and 
                      sp.setplanid = inter.setplanid 
                    group by 
                      sp.setplanid
                  )';
  
  
  lc_jFundingRequests := json_dyn.executeObject(thesql);
  
for rec in (
            select
              SETPLANID,
              SET_PLAN_TYPE_LISTID,
              EXPORTFILE,
              BASEFY,
              CREATED_ON,
              CREATED_BY,
              PLANTITLE,
              MONEY_APPROVED,
              MONEY_APPROVED_DATE,
              MONEY_APPROVED_AMT_INH,
              MONEY_APPROVED_AMT_KTR,
              NOTESKEY,
              SET_PLAN_AMT_INH,
              SET_PLAN_AMT_KTR,
              ENDFY,
              FUNDSOURCE_LISTID
            from
              spd_t_list_set_plans
            where
              setplanid in (
                              select 
                                sp.setplanid 
                              from 
                                spd_t_list_set_plans sp, 
                                spd_t_inter_set_plans inter 
                              where 
                                basefy = IN_YEAR and 
                                sp.setplanid = inter.setplanid 
                              group by 
                                sp.setplanid
                            )
          )
loop

  theSql := '  select 
                *
              from 
                spd_t_list_set_plans sp
              where 
                basefy = ''' || IN_YEAR || ''' and 
                sp.fundsource_listid = ' || rec.fundsource_listid;
                
  
  lc_jFRVersions := json_dyn.executeObject(thesql);
  
  json_ext.put(lc_jFundingRequests, 'data[' || lc_nFundingRequest || '][17].versions', lc_jFRVersions);
  
  
  theSql := '  select 
                * 
              from 
                spd_t_notes 
              where 
                noteskey in (
                              select 
                                noteskey 
                              from 
                                spd_t_list_set_plans 
                              where 
                                basefy = ' || IN_YEAR || ' and 
                                fundsource_listid = ' || rec.fundsource_listid || ')';
  
  lc_jFRNotes := json_dyn.executeObject(thesql);
  
  json_ext.put(lc_jFundingRequests, 'data[' || lc_nFundingRequest || '][17].notes', lc_jFRNotes);
  
  lc_nFundingRequest := lc_nFundingRequest + 1;
end loop;


lc_jFundingRequests.htp;
end;



/***T CODE BELOW**********************************/
--procedure six_year_actions(IN_PL number default 100085, IN_LATEST_YEAR number default to_number(to_char(add_months(SYSDATE, 3),'YYYY')), IN_ALL_LOCATIONS boolean default false) is
procedure get_planner_actions(IN_FILTER_JSON varchar2, IN_ALL_LOCATIONS boolean default false) is
  lc_jHistoricalActions json;
  thesql varchar2(8000);
  IN_LATEST_YEAR number:=2011;
  --IN_ALL_LOCATIONS boolean:=false;
  messages json;
  filterList json_list;
  filter_jsonArray json_list;
  filter_json      json;
  
  filter_jsonVal json_value;
  
  jsonObj json;
  
  FilterName varchar2(256);
  FilterValue varchar2(4000);
  
  filterCount number;
  
  lc_beginningYear number;
  lc_numberOfYears number;
  
  lc_productLineWhereClause varchar2(4000):=' ';
  
  procedure applyYearFilter is
  begin
    if lc_numberOfYears < 1 then lc_numberOfYears:=1; end if;
    thesql:=thesql||'
    and (actionplanner.APID is null or (';
    for i in 0 .. lc_numberOfYears-1 loop
      if i > 0 then
        thesql:=thesql||'
                                        or ';
      end if;
      thesql:=thesql||'decode(deliveryorders.start_date,null,actionplanner.planned_year,to_number(to_char(add_months(deliveryorders.start_date, 3),''YYYY''))) = '||(lc_beginningYear-i)||' ';
      --  thesql:=thesql||'actionplanner.planned_year = '||(IN_LATEST_YEAR-i)||' ';
    end loop;
    thesql:=thesql||'
        ))';
  end applyYearFilter;
  
  procedure applyNumericFilter(in_wherePrefix varchar2, in_makePLwhereClause boolean := false) is
    l_list json_list;
  begin
    l_list:=json_list(FilterValue);
    if l_list.count() > 0 then
      if in_makePLwhereClause then lc_productLineWhereClause:=' where '; end if;
      thesql:=thesql||' and ('||in_wherePrefix||' is null or (';
      for lc in 1 .. l_list.count() loop
        if lc>1 then 
          thesql:=thesql||' or '; 
          if in_makePLwhereClause then lc_productLineWhereClause:=lc_productLineWhereClause||' or '; end if;
        end if;
        thesql:=thesql||in_wherePrefix||' = '||l_list.get(lc).to_char();
        if in_makePLwhereClause then lc_productLineWhereClause:=lc_productLineWhereClause||'productline_listid = '||l_list.get(lc).to_char(); end if;
      end loop;
      thesql:=thesql||'     ))';
    end if;
  end applyNumericFilter;
  
begin

  --filter_jsonArray := json_list('[{"nam":"beginningyear","val":"2011"},{"nam":"actiontypes","val":"PLANNED"},{"nam":"showlocations","val":"ONLYACTIONS"},{"nam":"numberofyears","val":"6"},{"nam":"productline","val":["100085","100086","100087","100088"]},{"nam":"fundsource","val":["100031","100032","100033","100034","100035","100036","100037","100038","100039"]}]');
  --filter_jsonArray := json_list('[{"nam":"beginningyear","val":"2011"},{"nam":"actiontypes","val":"EXECUTION"},{"nam":"showlocations","val":"ALL"},{"nam":"numberofyears","val":"6"},{"nam":"productline","val":["100085","100086","100087","100088"]},{"nam":"fundsource","val":["100031","100032","100033","100034","100035","100036","100037","100038","100039"]}]');
  filter_jsonArray := json_list(IN_FILTER_JSON);
  filterCount:=filter_jsonArray.count();

  thesql:=
    'select   locations.state location
            ,decode(deliveryorders.start_date,null,actionplanner.planned_year,to_number(to_char(add_months(deliveryorders.start_date, 3),''YYYY''))) FY
            ,decode(deliveryorders.start_date,null,actionPlanner.PLANNED_QTR, CASE WHEN (to_number(to_char(to_date(deliveryorders.start_date),''MM'')) BETWEEN 1 AND 3) THEN 2
                                                                                   WHEN (to_number(to_char(to_date(deliveryorders.start_date),''MM'')) BETWEEN 4 AND 6) THEN 3
                                                                                   WHEN (to_number(to_char(to_date(deliveryorders.start_date),''MM'')) BETWEEN 7 AND 9) THEN 4
                                                                              ELSE 1
                                                                              END ) QTR
            ,actionPlanner.PRODUCTLINE_LISTID
            ,lst_productLine.displayvalue PRODUCT_LINE
            ,decode(deliveryorders.adoid,null,''No Delivery Orders'',deliveryorders.delivery_order_name) action_title
            ,actionPlanner.activity
            ,actionPlanner.APID
            ,deliveryorders.adoid
            ,actionplanner.planned_uic uic
            ,lst_actiontype.displayvalue action_type
            ,lst_fundSource.displayvalue fund_source
            ,actionplanner.planned_inhouse_amt
            ,actionplanner.planned_ktr_amt
            ,to_char(deliveryorders.start_date,''MM/DD/YY'') start_date
    from   (select distinct decode(state_abbr,null,country,state_abbr) state, country 
            from spd_mv_inf_cur_activities 
            --productLineWhereClause--
           ) locations
          ,(select 
              distinct 
                b.spd_uic planned_uic,
                b.state,
                b.spd_activity_name activity, 
                a.*  
            from 
              SPIDERS_DATA.spd_t_action_planner a,
              SPIDERS_DATA.spd_t_activities b
            where 
              a.planned_activitiesid=b.activitiesid) actionPlanner
          ,SPIDERS_DATA.spd_t_actn_dos deliveryOrders
          ,SPIDERS_DATA.spd_t_lists lst_fundSource
          ,SPIDERS_DATA.spd_t_lists lst_productLine
          ,SPIDERS_DATA.spd_t_lists lst_actionType
    where locations.state=actionPlanner.state--(+)--';
      thesql:=thesql||'
      and actionPlanner.PRODUCTLINE_LISTID = lst_productLine.listid(+)
      and actionPlanner.planned_fund_source_listid = lst_fundSource.listid(+)
      and actionplanner.planned_action_type_listid = lst_actiontype.listid(+)
      and actionPlanner.APID = deliveryorders.apid(+)
      and actionplanner.setplanid is null ';
      
      for fc in 1 .. filterCount loop
        jsonObj := json(filter_jsonArray.get(fc));
        FilterName:=replace(jsonObj.get(1).to_char(),'"','');
        FilterValue:=replace(jsonObj.get(2).to_char(),'"','');  
        
        CASE FilterName
          WHEN 'showlocations' THEN
            if FilterValue='ALL' then
              thesql:=replace(thesql,'--(+)--','(+)');
            end if;
          WHEN 'actiontypes' THEN
            if  FilterValue='PLANNED' then
              thesql:=thesql||'
              and deliveryorders.adoid is null ';
            elsif  FilterValue='EXECUTION' then
              thesql:=thesql||'
              and (actionPlanner.APID is null or deliveryorders.adoid is not null) ';
            end if;
          WHEN 'beginningyear' THEN 
            lc_beginningYear:=FilterValue;
            if lc_numberofyears is not null then applyYearFilter(); end if;
          WHEN 'numberofyears' THEN 
            lc_numberofyears:=FilterValue;
            if lc_beginningYear is not null then applyYearFilter(); end if;
          WHEN 'productline' THEN
            applyNumericFilter(in_wherePrefix=>'actionPlanner.productline_listid',in_makePLwhereClause=>true);
            thesql:=replace(thesql,'--productLineWhereClause--',lc_productLineWhereClause);
          WHEN 'fundsource' THEN
            applyNumericFilter(in_wherePrefix=>'actionPlanner.planned_fund_source_listid');
          ELSE null;
        END CASE;
      end loop;  
          
      thesql:=thesql||'
    order by locations.country desc, locations.state 
            ,actionplanner.planned_year desc               
            ,actionPlanner.activity';

  --htp.p(thesql);
  lc_jHistoricalActions := json_dyn.executeObject(thesql);
  lc_jHistoricalActions.htp;
end;


procedure plan_details(IN_APID number) is
  lc_jHistoricalActions json;
  thesql varchar2(3000);
  lc_pl_id number;
begin
  
  select a.productline_listid into lc_pl_id
  from spd_t_action_planner a
  where a.apid = IN_APID;

  /*
  thesql:=
    'select decode(activities.activitiesid,null,''Unknown'',activities.state) location
            ,decode(deliveryorders.start_date,null,actionplanner.planned_year,to_number(to_char(add_months(deliveryorders.start_date, 3),''YYYY''))) FY
            ,decode(deliveryorders.delivery_order_name,null,''No Delivery Orders'',deliveryorders.delivery_order_name) action_title
            ,actionPlanner.APID
            ,deliveryorders.adoid
            ,actionplanner.planned_activitiesid
            ,decode(activities.spd_activity_name,null,''Unknown-''||actionplanner.planned_activitiesid,activities.spd_activity_name) activity
            ,lst_actiontype.displayvalue action_type
            ,lst_fundSource.displayvalue fund_source
            ,actionplanner.planned_inhouse_amt
            ,actionplanner.planned_ktr_amt
            ,to_char(deliveryorders.start_date,''MM/DD/YY'') start_date
      from   SPIDERS_DATA.spd_t_action_planner actionPlanner
            ,SPIDERS_DATA.spd_t_actn_dos deliveryOrders
            ,SPIDERS_DATA.spd_t_activities activities
            ,SPIDERS_DATA.spd_t_lists lst_fundSource
            ,SPIDERS_DATA.spd_t_lists lst_actionType
      where actionPlanner.planned_fund_source_listid = lst_fundSource.listid 
        and actionplanner.planned_action_type_listid = lst_actiontype.listid
        and actionplanner.planned_activitiesid = activities.activitiesid(+)
        and actionPlanner.APID = deliveryorders.apid(+)
        and actionplanner.setplanid is null
        and actionPlanner.APID = '||IN_APID||'
      order by --FIRST: location (each row) - SECOND: year (each column) - THRID: activity (each accordian in a cell)
               decode(activities.activitiesid,null,''Unknown'',activities.state) 
              ,actionplanner.planned_year desc                                
              ,decode(activities.spd_activity_name,null,''Unknown-''||actionplanner.planned_activitiesid,activities.spd_activity_name)';
              
              select 
  to_number(to_char(to_date('01-APR-2010'),'MM')) theMonth,
  CASE WHEN (to_number(to_char(to_date('01-APR-2010'),'MM')) BETWEEN 1 AND 3) THEN 2
       WHEN (to_number(to_char(to_date('01-APR-2010'),'MM')) BETWEEN 4 AND 6) THEN 3
       WHEN (to_number(to_char(to_date('01-APR-2010'),'MM')) BETWEEN 7 AND 9) THEN 4
  ELSE 1
  END AS theMonth
from dual

  */            

  thesql:=
    'select decode(activities.uic,null,''Unknown'',activities.state) location
            ,decode(deliveryorders.start_date,null,actionplanner.planned_year,to_number(to_char(add_months(deliveryorders.start_date, 3),''YYYY''))) FY
            ,decode(deliveryorders.start_date,null,actionPlanner.PLANNED_QTR, CASE WHEN (to_number(to_char(to_date(deliveryorders.start_date),''MM'')) BETWEEN 1 AND 3) THEN 2
                                                                                   WHEN (to_number(to_char(to_date(deliveryorders.start_date),''MM'')) BETWEEN 4 AND 6) THEN 3
                                                                                   WHEN (to_number(to_char(to_date(deliveryorders.start_date),''MM'')) BETWEEN 7 AND 9) THEN 4
                                                                              ELSE 1
                                                                              END ) QTR
            ,decode(deliveryorders.adoid,null,''No Delivery Orders'',deliveryorders.delivery_order_name) action_title
            ,decode(activities.uic,null,''Unknown'',activities.unit_title) activity
            ,actionPlanner.APID
            ,deliveryorders.adoid
            --,actionplanner.planned_activitiesid
            ,activities.uic
            ,decode(activities.uic,null,''Unknown-''||actionplanner.planned_uic,activities.unit_title) activities
            ,lst_actiontype.displayvalue action_type
            ,lst_fundSource.displayvalue fund_source
            ,lst_fundsource.listid fund_source_listid
            ,actionplanner.planned_inhouse_amt
            ,actionplanner.planned_ktr_amt
            ,to_char(deliveryorders.start_date,''MM/DD/YY'') start_date
      from   SPIDERS_DATA.spd_t_action_planner actionPlanner
            ,SPIDERS_DATA.spd_t_actn_dos deliveryOrders
            ,(select unit_title, uic, decode(state_abbr,null,country,state_abbr) state, country from spd_mv_inf_cur_activities where productline_listid ='||lc_pl_id||') activities
            ,SPIDERS_DATA.spd_t_lists lst_fundSource
            ,SPIDERS_DATA.spd_t_lists lst_actionType
      where actionPlanner.planned_fund_source_listid = lst_fundSource.listid 
        and actionplanner.planned_action_type_listid = lst_actiontype.listid
        --and actionplanner.planned_activitiesid = activities.activitiesid(+)
        and actionplanner.planned_uic = activities.uic(+)
        and actionPlanner.APID = deliveryorders.apid(+)
        and actionplanner.setplanid is null
        and actionPlanner.APID = '||IN_APID||'
      order by --FIRST: location (each row) - SECOND: year (each column) - THRID: activity (each accordian in a cell)
               decode(activities.uic,null,''Unknown'',activities.state) 
              ,actionplanner.planned_year desc                                
              ,decode(activities.uic,null,''Unknown-''||actionplanner.planned_uic,activities.unit_title) ';
  --htp.p(thesql);
  
  lc_jHistoricalActions := json_dyn.executeObject(thesql);
  lc_jHistoricalActions.htp;
end;
/***T CODE ABOVE**********************************/


procedure fundSources(IN_PL number default 100085)


is
  thesql varchar2(4000);
begin

  thesql := 'select listid,displayvalue from spd_v_list_fund_sources where productline_listid ='||IN_PL;
  tobrowser.data(thesql);

exception
        when others then
        tobrowser.message('error','error fetching fundsources: ' || SQLERRM
        );
end;

procedure productlines

is
  thesql varchar2(4000);
begin

  thesql := 'select * from spd_v_list_product_lines where displayvalue <> ''All''';
  tobrowser.data(thesql);

exception
        when others then
        tobrowser.message('error','error fetching productlines: ' || SQLERRM
        );
end;

procedure get_reports(IN_nPage number default 1, IN_vSearch varchar2 default '^^^')

is 
    lc_vSql varchar2(2000);
  lc_vWhere varchar2(2000);
  lc_nRecordsPerPage number :=25;
  lc_nMinus number;
begin

if IN_nPage = 1 then
  lc_nMinus := 0;
else
  lc_nMinus := 1;
end if;

  lc_vSql := 
  'select 
      * 
    from (
        select 
          REPORTID,
          REPORTTITLE,
          REPORTAUTHOR,
          ESTSTARTDATE,
          CONTRACTOR,
          CONTRACTNUMBER,
          CREATEDDATE,
          REPORTNUMBER,
          ACTIONSID,
          CEACTIONSID,
          CITY,
          STATE,
          COUNTRY,
          NOTESKEY,
          row_number() over (order by createddate desc) r,
          FOLDERID,
          dataprovider.test_bfile(reportid) exist
        from 
          spd_t_reports
        #where#
      )
    where r between ' || to_char((IN_nPage*lc_nRecordsPerPage)-lc_nRecordsPerPage) || ' and ' || to_char((IN_nPage*lc_nRecordsPerPage)-lc_nMinus);



if IN_vSearch = '^^^' then
  lc_vSql := replace(lc_vSql,'#where#','');
else
  lc_vWhere := 'where 
  upper(REPORTID || 
  REPORTTITLE     || 
  REPORTAUTHOR ||     
  ESTSTARTDATE ||     
  CONTRACTOR ||     
  CONTRACTNUMBER ||     
  CREATEDDATE ||     
  REPORTNUMBER)
  like ''%' || upper(in_vSearch) || '%''';
  
  lc_vSql := replace(lc_vSql,'#where#',lc_vWhere);
    
end if;


    tobrowser.data(lc_vSql);
    
    exception
        when others then
        tobrowser.message('error', 'error retrieving reports: ' || to_char(IN_nPage) || ':' || SQLERRM
        );

end;


procedure get_Report_Types

is 

thesql varchar2(4000);
begin

thesql := 
'select
  *
from 
  spd_t_lists
where
  listname = ''Report_Number_Option_Values''
order by
  visualorder
  ';

tobrowser.data(thesql);


end;

procedure get_Report_Div_Types

is 

thesql varchar2(4000);
begin

thesql := 
'select
  *
from 
  spd_t_lists
where
  listname = ''Report_Number_Division_Values''
order by
  visualorder
  ';

tobrowser.data(thesql);


end;


procedure get_notes(IN_nNOTESKEY number)

is 

thesql varchar2(4000);
begin

thesql := 
'select
  *
from 
  spd_t_notes
where
  noteskey = ' || IN_nNOTESKEY || '
order by
  timestamp desc
  ';

tobrowser.data(thesql);

end;

procedure username

is

lc_vUsername varchar2(200);
begin

lc_vUsername := APEX_UTIL.GET_SESSION_STATE('APP_USER');

tobrowser.message('no action', lc_vUsername);

end;


procedure get_do_fac_dc_types

is 

thesql varchar2(4000);
begin

thesql := 'select * from spd_v_list_direct_costs';

tobrowser.data(thesql);
  
end;

procedure get_do_dc_types(in_nAdoid number)

is 

thesql varchar2(4000);
begin

thesql := 'select * from spd_t_lists where listid in (select directcost_listid from spd_t_do_ce_dc where adoid = ' || in_nAdoid  || ') order by visualorder';

tobrowser.data(thesql);
  
end;


procedure get_spec_instr_list (in_nPL number, in_nAdoid number)

is 
    lc_vSql varchar2(2000);
begin
    
    lc_vSql := '
    select 
        *
    from
        spd_t_lists
    where
        listname = ''list_facility_special_instructions''
    and productline_listid = ' || IN_nPL || '
        and grouping = (select displayvalue from spd_t_lists where listid = (select worktype_listid from spd_t_actn_dos where adoid = ' || in_nAdoid || '))
    order by
        visualorder asc';
--and notes <> ''not used''

    tobrowser.data(lc_vSql);
    
    exception
        when others then
        tobrowser.message('error', 'error retrieving special instructions: ' || to_char(in_nAdoid) || ':' || SQLERRM
        );
end;


procedure get_spec_instr_values (IN_nPL number, IN_nADOID number)

is 
    lc_vSql varchar2(2000);
begin
    
  
    lc_vSql := '
select 
        *
from
  spd_t_lists
where
  listname = ''list_special_instruction_values''
order by
  productline_listid,
  grouping,
  visualorder asc';


/*
    lc_vSql := '
    select 
        *
  from
    spd_t_lists
  where
    listid in 
    (
      select 
        listid 
      from 
        spd_t_lists
      where 
        productline_listid = ' || IN_nPL || '
        and listname = ''list_special_instruction_values''
        and notes <> ''not used''
        and grouping = 
        (
          select 
            displayvalue 
          from 
            spd_t_lists 
          where 
            listid = 
            (
              select 
                WORKTYPE_LISTID 
              from 
                spd_t_actn_dos 
              where 
                adoid = ' || IN_nADOID || '
            )
        )
      )
  order by
    grouping,
    visualorder asc';
*/
        
    tobrowser.data(lc_vSql);
    
    exception
        when others then
        tobrowser.message('error', 'error retrieving special instructions values for dropdowns: ' || to_char(IN_nPL) || ':' || SQLERRM
        );
end;


procedure get_fac_spec_instr_values (IN_nADOID number)

is 
    lc_vSql varchar2(2000);
begin
    
    lc_vSql := '
    select 
        list.listid,
    si.afsiid,
    si.spec_instr_listid,
    si.spec_instr_value,
    dofac.dofid,
    fac.spd_fac_name,
    fac.spd_fac_no
    from
        spd_t_lists list,
        spd_t_do_fac_spec_inst si,
        spd_t_do_facilities dofac,
        spd_t_facilities fac
    where
        list.listid = si.spec_instr_listid
        and si.dofid = dofac.dofid
        and dofac.facid = fac.facid
        and dofac.adoid = ' || IN_nADOID || '
       order by
           fac.spd_fac_name,
           list.listid';
    
    tobrowser.data(lc_vSql);
    
    exception
        when others then
        tobrowser.message('error', 'error retrieving special instructions values per facility: ' || to_char(IN_nADOID) || ':' || SQLERRM
        );
end;



procedure get_fundreq_versions (IN_nSetPlanId number)

is 
    lc_vSql varchar2(2000);
begin
  lc_vSql := '  select 
                *
              from 
                spd_t_list_set_plans sp
              where 
                setplanid = ' || IN_nSetPlanId;
                
    tobrowser.data(lc_vSql);
    
    exception
        when others then
        tobrowser.message('error', 'error retrieving versions of funding request per fundsource: ' || to_char(IN_nSetPlanId) || ':' || SQLERRM
        );
end;



procedure get_funding_requests (IN_nYear number)

is 
    lc_vSql varchar2(4000);
begin

  lc_vSql := 
  'select
    SETPLANID,
    SET_PLAN_TYPE_LISTID,
    EXPORTFILE,
    BASEFY,
    CREATED_ON,
    CREATED_BY,
    PLANTITLE,
    MONEY_APPROVED,
    MONEY_APPROVED_DATE,
    MONEY_APPROVED_AMT_INH,
    MONEY_APPROVED_AMT_KTR,
    NOTESKEY,
    SET_PLAN_AMT_INH,
    SET_PLAN_AMT_KTR,
    ENDFY,
    FUNDSOURCE_LISTID,
    (select sum(rec_inhouse_amt) from spd_t_action_planner where apid in (select apid from spd_t_inter_set_plans inter_sp where inter_sp.setplanid = spd_t_list_set_plans.setplanid)) REC_INHOUSE,
    (select sum(rec_ktr_amt) from spd_t_action_planner where apid in (select apid from spd_t_inter_set_plans inter_sp where inter_sp.setplanid = spd_t_list_set_plans.setplanid)) REC_KTR,
    (select sum(planned_inhouse_amt) from spd_t_action_planner where apid in (select apid from spd_t_inter_set_plans inter_sp where inter_sp.setplanid = spd_t_list_set_plans.setplanid)) PLAN_INHOUSE,
    (select sum(planned_ktr_amt) from spd_t_action_planner where apid in (select apid from spd_t_inter_set_plans inter_sp where inter_sp.setplanid = spd_t_list_set_plans.setplanid)) PLAN_KTR,
    (select count(*) from spd_t_inter_set_plans inter_sp where inter_sp.setplanid = spd_t_list_set_plans.setplanid) PA_COUNT,
    (select decode(sum(program_oversight_amt_q1),null,0,sum(program_oversight_amt_q1)) program_oversight from spd_t_program_oversight po where po.setplanid = spd_t_list_set_plans.setplanid) PROGRAM_OVERSIGHT_Q1,
    (select decode(sum(program_oversight_amt_q2),null,0,sum(program_oversight_amt_q2)) program_oversight from spd_t_program_oversight po where po.setplanid = spd_t_list_set_plans.setplanid) PROGRAM_OVERSIGHT_Q2,
    (select decode(sum(program_oversight_amt_q3),null,0,sum(program_oversight_amt_q3)) program_oversight from spd_t_program_oversight po where po.setplanid = spd_t_list_set_plans.setplanid) PROGRAM_OVERSIGHT_Q3,
    (select decode(sum(program_oversight_amt_q4),null,0,sum(program_oversight_amt_q4)) program_oversight from spd_t_program_oversight po where po.setplanid = spd_t_list_set_plans.setplanid) PROGRAM_OVERSIGHT_Q4
  from
    spd_t_list_set_plans
  where
    setplanid in (
                    select 
                      sp.setplanid 
                    from 
                      spd_t_list_set_plans sp, 
                      spd_t_inter_set_plans inter 
                    where 
                      basefy = ' || IN_nYear || ' and 
                      sp.setplanid = inter.setplanid 
                    group by 
                      sp.setplanid
                  )';
  
  
    tobrowser.data(lc_vSql);
    
    exception
        when others then
        tobrowser.message('error', 'error retrieving funding requests per fundsource: ' || to_char(IN_nYear) || ':' || SQLERRM
        );
end;

procedure get_actions_per_fund_req (IN_nSetplanid number)

is 
    lc_vSql varchar2(2000);
begin
  lc_vSql := 
  'select 
    pa.* ,
    actv.spd_activity_name,
    actv.spd_uic,
    actv.state,
    pl_list.displayvalue,
    pl_list.returnvalue
  from 
    spd_t_action_planner pa,
    spd_t_activities actv,
    spd_t_lists pl_list
  where 
    pa.apid in (select apid from spd_t_inter_set_plans where setplanid = ' || IN_nSetplanid || ')
    and actv.activitiesid = pa.planned_activitiesid
    and pl_list.listid = pa.productline_listid
  order by
    pl_list.displayvalue,
    actv.state,
    actv.spd_activity_name';
  
  
 
  
    tobrowser.data(lc_vSql);
    
    exception
        when others then
        tobrowser.message('error', 'error retrieving funding requests per fundsource: ' || to_char(IN_nSetplanid) || ':' || SQLERRM
        );
end;

procedure get_contract_regions (IN_nPL number)

is 
    lc_vSql varchar2(2000);
begin
  lc_vSql := 
  'select 
    *
  from 
    spd_t_lists
  where 
    listname = ''list_contract_regions''
    and productline_listid = ' || IN_nPL || '
  order by
    visualorder';
  
  
    tobrowser.data(lc_vSql);
    
    exception
        when others then
        tobrowser.message('error', 'error retrieving productline contract regions: ' || to_char(IN_nPL) || ':' || SQLERRM
        );
end;


procedure get_contracts_per_region (IN_nREGIONID number)

is 
    lc_vSql varchar2(2000);
begin
  lc_vSql := 
  'select 
    *
  from 
    spd_t_lists
  where 
    listid in (select contract_listid from spd_t_contracts_per_region where region_listid = ' || IN_nREGIONID || ') 
  order by
    visualorder';
  
  
 
  
    tobrowser.data(lc_vSql);
    
    exception
        when others then
        tobrowser.message('error', 'error retrieving contracts per region: ' || to_char(IN_nREGIONID) || ':' || SQLERRM
        );
end;

procedure get_work_types (IN_nPL number)

is 
    lc_vSql varchar2(2000);
begin
  lc_vSql := 
  'select 
    *
  from 
    spd_t_lists
  where 
    listname = ''list_sow_types''
    and productline_listid = ' || IN_nPL || '
  order by
    visualorder';
  
  
    tobrowser.data(lc_vSql);
    
    exception
        when others then
        tobrowser.message('error', 'error retrieving productline statement of work types: ' || to_char(IN_nPL) || ':' || SQLERRM
        );
end;


procedure get_report_total_pages

is 
    lc_vSql varchar2(2000);
begin
  lc_vSql := 
  'select 
    round(count(*) / 50) pages
  from 
    spd_t_reports';
  
  
    tobrowser.data(lc_vSql);
    
    exception
        when others then
        tobrowser.message('error', 'error retrieving report total pages: ' || SQLERRM
        );
end;

procedure get_photos

is 
    lc_vSql varchar2(2000);
begin
  lc_vSql := 
  'select 
    xmlfileid
  from 
    spd_t_xml_files
  where 
    xmlfileid > 430000840';
  
  
    tobrowser.data(lc_vSql);
    
    exception
        when others then
        tobrowser.message('error', 'error retrieving photos: ' || SQLERRM
        );
end;


procedure get_rights

is 
  lc_vUsername varchar2(200) := APEX_UTIL.GET_SESSION_STATE('APP_USER');
    lc_vSql varchar2(2000);
begin
  lc_vSql := 
  'select
    ''FacilityBook View''
  from dual
  
  union
  
  select
    ''Report Library View''
  from dual
  
  union
  
  select 
    g.group_name
  from 
    ssd_security_groups g,
    ssd_security_users u
  where 
    g.securitygroupid = u.securitygroupid and
    u.employeeid = ''' || lc_vUsername || '''';
  
  
    tobrowser.data(lc_vSql);
    
    exception
        when others then
        tobrowser.message('error', 'error retrieving user rights: ' || SQLERRM
        );
end;


procedure get_oversight(IN_nSETPLANID number)

is 
    lc_vSql varchar2(2000);
begin
  lc_vSql := 
  'select
    po.*,
    list.displayvalue
  from
    spd_t_program_oversight po,
    spd_t_lists list
  where
    po.productline_listid = list.listid and
    setplanid = ' || in_nSetplanid;
  
  
    tobrowser.data(lc_vSql);
    
    exception
        when others then
        tobrowser.message('error', 'error retrieving report total pages: ' || SQLERRM
        );
end;






procedure get_ai_supporting_files(IN_nDOFID number)

is 
    lc_vSql varchar2(2000);
begin
  lc_vSql := 
'select 
  ufii.displayvalue,
  ai.asset_type,
  aisf.local_id localid,
  aisf.aipid,
  aisf.aiid,
  aisf.local_id,
  aisf.caption,
  aisf.file_type_string,
  aisf.folderid,
  DBMS_LOB.FILEEXISTS(dofsf.the_bfile) exist,
  decode
  (
    dofsf.thumbnailid,null,(select thumbnailid from spd_t_thumbnails where filename = ''file_not_available.png'' and rownum = 1),
    0,(select thumbnailid from spd_t_thumbnails where filename = ''file_not_available.png'' and rownum = 1),
    dofsf.thumbnailid
  ) thumbnailid
from
  spd_t_lists ufii,
  spd_t_do_fac_asset_inventory ai,
  spd_t_ai_supporting_files aisf,
  spd_t_do_fac_supporting_files dofsf
where
  dofsf.dofsfid = aisf.dofsfid
  and ufii.listid = ai.code_listid
  and ai.aiid = aisf.aiid
  and dofsf.dofid = ' || in_nDOFID || '
order by
  to_number(substr(aisf.local_id,instr(aisf.local_id,''-'',1,1)+1))';


    tobrowser.data(lc_vSql);
    
    exception
        when others then
        tobrowser.message('error', 'error retrieving ai supporting photos: ' || SQLERRM
        );
end;


procedure get_mooring_photos(IN_nDOFID number)

is 
    lc_vSql varchar2(2000);
begin
  lc_vSql := 
'select
  dofacmfid,
  aiid,
  fitting_id_number,
  asset_type_name,
  fitting_type,
  Design_capacity,
  rating_capacity,
  condition_fitting,
  condition_base,
  thumbnailid
from 
  spd_t_do_fac_mooring_fittings 
where 
  aiid in (select aiid from spd_t_do_fac_asset_inventory where dofid = ' || in_nDOFID || ')';
  
  
    tobrowser.data(lc_vSql);
    
    exception
        when others then
        tobrowser.message('error', 'error retrieving mooring photos: ' || SQLERRM
        );
end;

procedure get_do_files(IN_nADOID number)

is 
    lc_vSql varchar2(2000);
begin
  lc_vSql := 
'select * from (  
select
  dosfid,
  local_id,
  file_name,
  dataprovider.get_caption(dosfid) caption,
  file_type_string,
  navfac_drawing_number,
  thumbnailid,
  FOLDERID,
  DBMS_LOB.FILEEXISTS(the_bfile) file_exists
from 
  spd_t_do_supporting_files dosf
where 
  adoid = ' || IN_nADOID || ')';
  
  --
    tobrowser.data(lc_vSql);
    
    exception
        when others then
        tobrowser.message('error', 'error retrieving do supporting files: ' || SQLERRM
        );
end;

procedure get_do_fac_files(IN_nDOFID number)

is 
    lc_vSql varchar2(2000);
begin
  lc_vSql := 
'select
  dofsfid,
  local_id,
  file_name,
  (select caption from spd_t_do_fac_supporting_files a  where a.dofsfid = dofsf.dofsfid) thecaption,
  file_type_string,
  navfac_drawing_number,
  thumbnailid,
  FOLDERID,
  DBMS_LOB.FILEEXISTS(the_bfile) file_exists
from 
  spd_t_do_fac_supporting_files dofsf
where 
  file_section_listid not in (select listid from spd_t_lists where listname = ''list_delivery_order_file_sections'' and displayvalue = ''Photo Files'')
  and dofid = ' || IN_nDOFID;
  
  
    tobrowser.data(lc_vSql);
    
    exception
        when others then
        tobrowser.message('error', 'error retrieving do fac supporting files: ' || SQLERRM
        );
end;


procedure get_dof_per_fac(IN_nFACID number)

is
  lc_vSql varchar2(4000);
begin


  lc_vSql := 
'select 
  sfac.facid,
  sfac.spd_fac_name spiders_facname,
  sfac.spd_fac_no spiders_facno,
  fac.facility_id,
  fac.facility_name,
  fac.facility_no,
  fac.activity_uic,
  fac.special_area_code, 
  dofac.dofid
from 
  spd_mv_inf_facility fac,
  spd_t_facilities sfac,
  spd_t_do_facilities dofac
where 
  sfac.infads_facility_id(+) = fac.facility_id
  and dofac.facid = sfac.facid
  and sfac.facid = ' || IN_nFACID || '
order by 
  fac.facility_name';

tobrowser.data(lc_vSql);
      
  
  exception
        when others then
        tobrowser.message('error', 'error retrieving delivery order facilities: ' || to_char(IN_nFACID) || ':' || SQLERRM
        );
end;




procedure get_carousel_photos(IN_nFACID number)

is
  lc_vSql varchar2(4000);
begin


  lc_vSql := 
'select
  dofsfid,
  local_id,
  file_name,
  (select caption from spd_t_do_fac_supporting_files a  where a.dofsfid = dofsf.dofsfid) thecaption,
  file_type_string,
  navfac_drawing_number,
  thumbnailid
from 
  spd_t_do_fac_supporting_files dofsf
where 
  DBMS_LOB.FILEEXISTS(the_bfile) <>0
  and file_type_string = ''Overall''
  and dofid in (select dofid from spd_t_do_facilities where facid = ' || IN_nFACID || ')';

tobrowser.data(lc_vSql);
      
  
  exception
        when others then
        tobrowser.message('error', 'error retrieving delivery order facilities: ' || to_char(IN_nFACID) || ':' || SQLERRM
        );
end;



END DATAPROVIDER;

/
