--------------------------------------------------------
--  DDL for Package Body PKG_QUERY_COLLECTIONS
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE PACKAGE BODY "SPIDERS_DATA"."PKG_QUERY_COLLECTIONS" as

function f_object_exists (in_vObjectName varchar2) return varchar2 as 
  /* template variables */
  lc_vFunctionName varchar2(255) := 'f_object_exists';
  lc_vReturn varchar2(32000) := '{"error":"default"}'; --defaul the return to e for error
  lc_nCount number :=0;
  
  /* function specific variables */
begin

  
  select count(object_name) into lc_nCount from user_objects where object_name = in_vObjectName;
  return lc_nCount;

exception     
  when others then
    execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' dml error: ' || sqlerrm || ''')';
    return '{"error":"' || sqlerrm || '"}';
end;


function f_test_for_error (in_vReturned varchar2) return varchar2 as 
  /* template variables */
  lc_vFunctionName varchar2(255) := 'f_test_for_error';
  lc_vReturn varchar2(32000) := '{"error":"default"}'; --defaul the return to e for error
  lc_nContinue number :=0; --default to 0 to dissalow continuing
  
  /* function specific variables */
  lc_vTest varchar2(32000) := in_vReturned;
begin

  if substr(lc_vTest, 1, 9) = '{"error":' then
    lc_vReturn := '0';
  else
    lc_vReturn := '1';
  end if;
  
  return lc_vReturn;

exception     
  when others then
      execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' dml error: ' || sqlerrm || ''')';
      return '{"error":"' || sqlerrm || '"}';
end; 

function getAttributeValue(in_vAttribute varchar2, in_jJSON json) return varchar2 as 
  /* template variables */
  json_error EXCEPTION;
  spiders_error EXCEPTION;
  lc_vErrorDescription varchar2(255) := 'default';
  lc_vFunctionName varchar2(255) := 'getAttributeValue';
  lc_vReturn varchar2(32000) := '{"error":"default"}'; --defaul the return to e for error
  lc_nContinue number :=0; --default to 0 to dissalow continuing
  
  /* function specific variables */
  lc_jvValue json_value;
begin
  
  if(in_jJSON.exist(in_vAttribute)) then       
    lc_jvValue := in_jJSON.get(in_vAttribute);
    lc_vReturn := getVal(lc_jvValue);
  else
    lc_vErrorDescription := in_vAttribute || ' attribute not found in json';
    raise spiders_error;
  end if;
  
  /* get name of object */
  return lc_vReturn;

exception     
  when spiders_error then
      execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' SPIDERS error ' || lc_vErrorDescription || ''')';
      return '{"error":"' || lc_vErrorDescription || '"}';
  when json_error then
      execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' JSON error'')';
      return '{"error":"json"}';
  when others then
      execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' dml error: ' || sqlerrm || ''')';
      return '{"error":"' || sqlerrm || '"}';
end; 

function f_parseGroupFilterData(in_jJson json) return json as
  /* template variables */
  lc_vFunctionName varchar2(255) := 'f_parseGroupFilterData';

  json_error EXCEPTION;
  spiders_error EXCEPTION;
  lc_vErrorDescription varchar2(255) := 'default';

  lc_jReturn json; --defaul the return to e for error
  lc_nContinue number :=0; --default to 0 to dissalow continuing

  
  /* function specific variables */
  lc_jlGroupFilters json_list;
  lc_jvRec json_value;
  lc_jValue json;
  lc_vObjectName varchar2(255);
  lc_vDisplayName varchar2(255);
  lc_vOrder varchar2(255);
  
  lc_vInClause long;
  lc_jvValue json_value;
  lc_jlDataValue json_list;
  lc_vValue varchar2(255);
  lc_vGroupWhere varchar(255);
  lc_nGroupCount number := 1;
  
  lc_jJson json;
  lc_jlGFoutput json_list;
  lc_jlGFSingle json_list;
begin

  lc_jlGFoutput := json_list();
  lc_jJson := json();
  
  if(in_jJson.exist('groupFilters')) then
      if(in_jJson.get('groupFilters').is_array) then
          lc_jlGroupFilters := json_list(in_jJson.get('groupFilters'));
          if(lc_jlGroupFilters.count>0) then
            for rec in 1..lc_jlGroupFilters.count loop
              lc_jvRec := lc_jlGroupFilters.get(rec);
              lc_jValue := json(lc_jvRec);
              
              
              if (lc_jValue.exist('objectName')) then
                  lc_jvValue := lc_jValue.get('objectName');
                  lc_vObjectName := getVal(lc_jvValue); 
              else
                lc_vErrorDescription := 'objectName does not exist';
              end if;
              
              
              if (lc_jValue.exist('displayName')) then
                  lc_jvValue := lc_jValue.get('displayName');
                  lc_vDisplayName := getVal(lc_jvValue); 
              else
                lc_vErrorDescription := 'displayName does not exist';
              end if;              
              
              
              if (lc_jValue.exist('order')) then
                  lc_jvValue := lc_jValue.get('order');
                  lc_vOrder := getVal(lc_jvValue); 
              else
                lc_vErrorDescription := 'Order does not exist';
              end if;  
              
                lc_jlGFSingle := json_list();
                lc_jlGFSingle.append(lc_vObjectName);
                lc_jlGFSingle.append(lc_vDisplayName);
                lc_jlGFSingle.append(lc_vOrder);
                lc_jlGFoutput.append(lc_jlGFSingle);
            end loop;
          else
            lc_vGroupWhere := '';
          end if;
      else
        lc_vErrorDescription := 'groupFilterData does not an array';
      end if;
  else
    lc_vErrorDescription := 'groupFilterData does not exist';
    raise spiders_error;
  end if;
  
  
  lc_jJson.put('filters',lc_jlGFoutput);
  
  lc_jReturn := lc_jJson;
  return lc_jReturn;

exception   
  when spiders_error then
      execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' SPIDERS error ' || lc_vErrorDescription || ''')';
      return json('{"error":"' || lc_vErrorDescription || '"}');
  when json_error then
      execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' JSON error'')';
      return json('{"error":"json"}');
  when others then
      execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' dml error: ' || sqlerrm || ''')';
      return json('{"error":"' || sqlerrm || '"}');
end; 

function f_makeTableEntries (in_jJson json, in_vModule varchar2, in_vCollectionName varchar2) return varchar2 as 
  /* template variables */
  json_error EXCEPTION;
  spiders_error EXCEPTION;
  lc_vErrorDescription varchar2(255) := 'default';
  lc_vFunctionName varchar2(255) := 'f_makeTableEntries';
  lc_vReturn varchar2(32000) := '{"error":"default"}'; --defaul the return to e for error
  lc_nContinue number :=0; --default to 0 to dissalow continuing

  
  /* function specific variables */
  lc_jvValue json_value;
  lc_vSql long;
  lc_vModule varchar2(255);
  lc_vModelName varchar2(255);
  lc_jResults json;
  lc_jMessages json;
  lc_vDMLResult varchar2(255);
  lc_jlDML json_list;
  lc_vGroupFilterExists varchar2(255);
  
  lc_jlGroupFilters json_list;
  lc_jlFilter json_list;
  lc_vGroupFilter varchar2(255);
  lc_vGroupFilterDisplay varchar2(255);
  lc_vVisualOrder varchar2(10);
begin

  lc_jlDML := json_list();
  lc_vSql := 'DELETE FROM SPD_T_COLLECTION_GRP_FLTRS WHERE module =''' || in_vModule || ''' and collection_name=''' || in_vCollectionName || '''';
  lc_jlDML.append(lc_vSql);
  
  if(in_jJson.exist('filters')) then
      if(in_jJson.get('filters').is_array) then
          lc_jlGroupFilters := json_list(in_jJson.get('filters'));
          if(lc_jlGroupFilters.count>0) then
            for rec in 1..lc_jlGroupFilters.count loop
              lc_jlFilter := json_list(lc_jlGroupFilters.get(rec));
              if lc_jlFilter.count = 3 then
                lc_vGroupFilter := getVal(lc_jlFilter.get(1));
                lc_vGroupFilterDisplay := getVal(lc_jlFilter.get(2));
                lc_vVisualOrder := getVal(lc_jlFilter.get(3));

                lc_vSql := 'INSERT INTO SPD_T_COLLECTION_GRP_FLTRS (module, collection_name, group_filter, group_filter_display, visual_order) VALUES (''' || in_vModule || ''',''' || in_vCollectionName || ''',''' || lc_vGroupFilter || ''',''' || lc_vGroupFilterDisplay || ''',' || lc_vVisualOrder || ')';
                lc_jlDML.append(lc_vSql);
              else
                lc_vErrorDescription := 'filter count is not 3';
                --raise spiders_error;
              end if; 
            end loop;
          end if;
      end if;
  end if;
  

  lc_jResults := json();
  lc_jResults.put('results',lc_jlDML);
  lc_jMessages := json();
  lc_vDMLResult := BB_F_DML(lc_jResults, lc_jMessages);
  lc_vReturn := lc_vDMLResult;


  return lc_vReturn;

exception   
  when spiders_error then
      execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' SPIDERS error ' || lc_vErrorDescription || ''')';
      return '{"error":"' || lc_vErrorDescription || '"}';
  when json_error then
      execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' JSON error'')';
      return '{"error":"json"}';
  when others then
      execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' dml error: ' || sqlerrm || ''')';
      return '{"error":"' || sqlerrm || '"}';
end; 

function f_getCollectionColumns (in_jJson json) return json_list as 
  /* template variables */
  json_error EXCEPTION;
  spiders_error EXCEPTION;
  lc_vErrorDescription varchar2(255) := 'default';
  lc_vFunctionName varchar2(255) := 'f_getCollectionColumns';
  lc_jlReturn json_list;-- := '{"error":"default"}'; --defaul the return to e for error
  lc_nContinue number :=0; --default to 0 to dissalow continuing

  
  /* function specific variables */
  lc_jlColumns json_list;
begin

  lc_jlReturn := json_list();
  
  if(in_jJson.exist('columns')) then
    if(in_jJson.get('columns').is_array) then
      lc_jlColumns := json_list(in_jJson.get('columns'));
    end if;
  end if;
  
  lc_jlReturn := lc_jlColumns;
  return lc_jlReturn;

exception   
  when spiders_error then
      execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' SPIDERS error ' || lc_vErrorDescription || ''')';
      return json_list('[{"error":"' || lc_vErrorDescription || '"}]');
  when json_error then
      execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' JSON error'')';
      return json_list('[{"error":"json"}]');
  when others then
      execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' dml error: ' || sqlerrm || ''')';
      return json_list('[{"error":"' || sqlerrm || '"}]');
end; 

function f_getCollectionOrderBy (in_jJson json) return json_list as 
  /* template variables */
  json_error EXCEPTION;
  spiders_error EXCEPTION;
  lc_vErrorDescription varchar2(255) := 'default';
  lc_vFunctionName varchar2(255) := 'f_getCollectionOrderBy';
  lc_jlReturn json_list;-- := '{"error":"default"}'; --defaul the return to e for error
  lc_nContinue number :=0; --default to 0 to dissalow continuing

  
  /* function specific variables */
  lc_jlOrderBy json_list;
begin

  lc_jlReturn := json_list();
  
  if(in_jJson.exist('orderBy')) then
    if(in_jJson.get('orderBy').is_array) then
      lc_jlOrderBy := json_list(in_jJson.get('orderBy'));
    end if;
  end if;
  
  lc_jlReturn := lc_jlOrderBy;
  return lc_jlReturn;

exception   
  when spiders_error then
      execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' SPIDERS error ' || lc_vErrorDescription || ''')';
      return json_list('[{"error":"' || lc_vErrorDescription || '"}]');
  when json_error then
      execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' JSON error'')';
      return json_list('[{"error":"json"}]');
  when others then
      execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' dml error: ' || sqlerrm || ''')';
      return json_list('[{"error":"' || sqlerrm || '"}]');
end; 
    
function f_makeQueryCollectionViews (in_jlCollectionColumns json_list, in_jlCollectionOrderBy json_list, in_vQueryCollectionName varchar2) return varchar2 as 
  /* template variables */
  json_error EXCEPTION;
  spiders_error EXCEPTION;
  lc_vErrorDescription varchar2(255) := 'default';
  lc_vFunctionName varchar2(255) := 'f_makeQueryCollectionViews';
  lc_vReturn varchar2(32000) := '{"error":"default"}'; --defaul the return to e for error
  lc_nContinue number :=0; --default to 0 to dissalow continuing

  
  /* function specific variables */
  lc_vPrefix varchar2(25) := 'SPD_V_QC_';
  lc_jlDML json_list;
  lc_vSql long;
  lc_jResults json;
  lc_jMessages json;
  lc_vDMLResult long;
  lc_jvColumnSegment json_value;
  lc_vColumnSegment long;
  lc_vColumnSegmentValue long;
  
  lc_jlOrderItem json_list;
  lc_vColumnName varchar2(255);
  lc_vSortDirection varchar2(255);
  
  lc_jvOrderBySegment json_value;
  lc_vOrderBySegment long;
  lc_jvOrderBySegmentColumn varchar2(255);
  lc_jvOrderBySegmentDir varchar2(255);
  
begin


  for rec in 1..in_jlCollectionColumns.count loop
    lc_jvColumnSegment := in_jlCollectionColumns.get(rec);
    lc_vColumnSegmentValue := getVal(lc_jvColumnSegment);
    lc_vColumnSegmentValue := '''' ||  lc_vColumnSegmentValue || ''' ' || lc_vColumnSegmentValue;
    
    if rec = 1 then
      lc_vColumnSegment := lc_vColumnSegmentValue;
    else
      lc_vColumnSegment := lc_vColumnSegment || ',' || lc_vColumnSegmentValue;
    end if;
  end loop;
  
  lc_vOrderBySegment := '';
  
  for rec in 1..in_jlCollectionOrderBy.count loop
  
    lc_jlOrderItem := json_list(in_jlCollectionOrderBy.get(rec));
    lc_vColumnName := getVal(lc_jlOrderItem.get(1));
    lc_vSortDirection := getVal(lc_jlOrderItem.get(2));
      
    if rec = 1 then
      lc_vOrderBySegment := ' ORDER BY ' || lc_vColumnName || ' ' || lc_vSortDirection ;
    else
      lc_vOrderBySegment := lc_vOrderBySegment || ',' || lc_vColumnName || ' ' || lc_vSortDirection;
    end if;
  end loop;  

  
  lc_jlDML := json_list();
  lc_vSql := 'CREATE OR REPLACE FORCE VIEW ' || lc_vPrefix || in_vQueryCollectionName || ' AS select ' || lc_vColumnSegment ||  ' from dual ' || lc_vOrderBySegment;
    
  lc_jlDML.append(lc_vSql);

  lc_jResults := json();
  lc_jResults.put('results',lc_jlDML);
  lc_jMessages := json();
  lc_vDMLResult := BB_F_DML(lc_jResults, lc_jMessages);
  lc_vReturn := lc_vDMLResult;
  
  return lc_vReturn;

exception   
  when spiders_error then
      execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' SPIDERS error ' || lc_vErrorDescription || ''')';
      return '{"error":"' || lc_vErrorDescription || '"}';
  when json_error then
      execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' JSON error'')';
      return '{"error":"json"}';
  when others then
      execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' dml error: ' || sqlerrm || ''')';
      return '{"error":"' || sqlerrm || '"}';
end; 

function f_makeGroupFilterViews (lc_jlCollectionColumns json_list, lc_jlCollectionOrderBy json_list, lc_vQueryCollectionName varchar2) return varchar2 as 
  /* template variables */
  json_error EXCEPTION;
  spiders_error EXCEPTION;
  lc_vErrorDescription varchar2(255) := 'default';
  lc_vFunctionName varchar2(255) := 'f_makeGroupFilterViews';
  lc_vReturn varchar2(32000) := '{"error":"default"}'; --defaul the return to e for error
  lc_nContinue number :=0; --default to 0 to dissalow continuing

  
  /* function specific variables */
  lc_vPrefix varchar2(25) := 'SPD_V_';
  lc_jlDML json_list;
  lc_vSql long;
  lc_jResults json;
  lc_jMessages json;
  lc_vDMLResult varchar2(255);
begin

/*
  lc_jlDML := json_list();
  for rec in (select * from SPD_T_COLLECTION_GRP_FLTRS WHERE module = in_vModule and collection_name= in_vCollectionName) loop
    lc_vSql := 'CREATE OR REPLACE FORCE VIEW 
    ' || lc_vPrefix || in_vCollectionName || '_' ||  rec.group_filter  || '
        (
         "group_filter_display_name", "group_name", "display_value", "return_value", "record_count", "rn"
        ) 
    AS 
      select
      ''' || rec.GROUP_FILTER_DISPLAY || ''' group_filter_display_name,
      ''' || rec.GROUP_FILTER || ''' group_name,
      ''change_display_value'' display_value,
      ''change_return_value'' return_value,
      COUNT(*) OVER (PARTITION BY ''return_value'') record_count,
      ROW_NUMBER() OVER (PARTITION BY ''return_value'' ORDER BY ''return_value'' NULLS LAST) rn
    from
      dual';
    
    lc_jlDML.append(lc_vSql);
  end loop;
  

  lc_jResults := json();
  lc_jResults.put('results',lc_jlDML);
  lc_jMessages := json();
  lc_vDMLResult := BB_F_DML(lc_jResults, lc_jMessages);
  lc_vReturn := lc_vDMLResult;
*/
 
  return lc_vReturn;

exception   
  when spiders_error then
      execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' SPIDERS error ' || lc_vErrorDescription || ''')';
      return '{"error":"' || lc_vErrorDescription || '"}';
  when json_error then
      execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' JSON error'')';
      return '{"error":"json"}';
  when others then
      execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' dml error: ' || sqlerrm || ''')';
      return '{"error":"' || sqlerrm || '"}';
end; 

function f_create_rest_table_maps (in_vModule varchar2, in_vQueryCollectionName varchar2) return varchar2 as 
  /* template variables */
  json_error EXCEPTION;
  spiders_error EXCEPTION;
  lc_vErrorDescription varchar2(255) := 'default';
  lc_vFunctionName varchar2(255) := 'f_create_rest_table_maps';
  lc_vReturn varchar2(32000) := '{"error":"default"}'; --defaul the return to e for error
  lc_nContinue number :=0; --default to 0 to dissalow continuing

  
  /* function specific variables */
  lc_jvValue json_value;
  lc_vSql long;
  lc_vModule varchar2(255);
  lc_vModelName varchar2(255);
  lc_jResults json;
  lc_jMessages json;
  lc_vDMLResult varchar2(255);
  lc_jlDML json_list;
begin

  lc_jlDML := json_list();

  lc_vSql := 'DELETE FROM SPD_T_REST_TO_TABLE_MAPS WHERE upper(MODULE) = ''' || upper(in_vModule) || ''' and upper(target) ='''|| upper(in_vQueryCollectionName) || ''' and rest_endpoint=''READQUERYCOLLECTION''';
  lc_jlDML.append(lc_vSql);

  lc_vSql := 'INSERT INTO SPD_T_REST_TO_TABLE_MAPS (REST_ENDPOINT, MODULE, TARGET, TABLE_NAME) VALUES (''READQUERYCOLLECTION'',''' || in_vModule || ''',''' || in_vQueryCollectionName || ''',''' || in_vQueryCollectionName || ''')';
  lc_jlDML.append(lc_vSql);

  

  lc_jResults := json();
  lc_jResults.put('results',lc_jlDML);
  lc_jMessages := json();
  lc_vDMLResult := BB_F_DML(lc_jResults, lc_jMessages);
  lc_vReturn := lc_vDMLResult;
  
  return lc_vReturn;

exception   
  when spiders_error then
      execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' SPIDERS error ' || lc_vErrorDescription || ''')';
      return '{"error":"' || lc_vErrorDescription || '"}';
  when json_error then
      execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' JSON error'')';
      return '{"error":"json"}';
  when others then
      execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' dml error: ' || sqlerrm || ''')';
      return '{"error":"' || sqlerrm || '"}';
end; 

function f_make_qry_collection_objects(in_jJson json) return varchar2 as
  /* template variables */
  json_error EXCEPTION;
  spiders_error EXCEPTION;
  lc_vErrorDescription varchar2(255) := 'default';
  lc_vFunctionName varchar2(255) := 'f_make_data_table_objects';
  lc_vReturn varchar2(32000) := '{"error":"default"}'; --defaul the return to e for error
  lc_nContinue number :=0; --default to 0 to dissalow continuing

  
  /* function specific variables */
  lc_vSql long;
  lc_jJson json;
  lc_vModule varchar2(255);
  lc_vQueryCollectionName varchar2(255);
  
  lc_jlCollectionColumns json_list;
  lc_jlCollectionOrderBy json_list;
  lc_vTemp varchar2(255);
begin
  lc_vModule := getAttributeValue('module', in_jJson);
  lc_nContinue := f_test_for_error(lc_vModule);
  
  if lc_nContinue = 1 then
    lc_vQueryCollectionName := getAttributeValue('queryCollectionName', in_jJson);
    lc_nContinue := f_test_for_error(lc_vQueryCollectionName);
  else
    lc_vErrorDescription := 'Module attribute not found';
    raise spiders_error;
  end if;
  

  
  if lc_nContinue = 1 then
    lc_jlCollectionColumns := f_getCollectionColumns(in_jJson);
    --lc_nContinue := f_test_for_error(lc_vTemp);
  else
    lc_vErrorDescription := 'collectionName attribute not found';
    raise spiders_error;
  end if;
  

  if lc_nContinue = 1 then
    lc_jlCollectionOrderBy := f_getCollectionOrderBy(in_jJson);
    --lc_nContinue := f_test_for_error(lc_vTemp);
  else
    lc_vErrorDescription := 'error getting columns';
    raise spiders_error;
  end if;
  
  if lc_nContinue = 1 then
    lc_vTemp := f_makeQueryCollectionViews(lc_jlCollectionColumns, lc_jlCollectionOrderBy, lc_vQueryCollectionName);
    --lc_nContinue := f_test_for_error(lc_vTemp);
  else
    lc_vErrorDescription := 'error getting order by';
    raise spiders_error;
  end if;
  

  if lc_nContinue = 1 then
    lc_vTemp := f_create_rest_table_maps(lc_vModule, lc_vQueryCollectionName);
    lc_nContinue := f_test_for_error(lc_vTemp);
  else
    lc_vErrorDescription := 'error during make query collection views';
    raise spiders_error;
  end if;

  
  return lc_vReturn;

exception   
  when spiders_error then
      execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' SPIDERS error ' || lc_vErrorDescription || ''')';
      return '{"error":"' || lc_vErrorDescription || '"}';
  when json_error then
      execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' JSON error'')';
      return '{"error":"json"}';
  when others then
      execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' dml error: ' || sqlerrm || ''')';
      return '{"error":"' || sqlerrm || '"}';
end; 



end;

/
