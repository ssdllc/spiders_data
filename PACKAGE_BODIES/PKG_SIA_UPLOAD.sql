--------------------------------------------------------
--  DDL for Package Body PKG_SIA_UPLOAD
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE PACKAGE BODY "SPIDERS_DATA"."PKG_SIA_UPLOAD" AS

  function f_update_sia_dofid(in_cJson clob) return json AS
  json_error EXCEPTION;
  spiders_error EXCEPTION;
  
  lc_nStrId number;
  lc_nDofid number;
  
  lc_jParameterJson json;
  lc_jReturn json;
  
  lc_nContinue number :=0;
  
  lc_vErrorDescription varchar2(255) := 'default';
  lc_vFunctionName varchar2(255) := 'f_update_sia_dofid';
  BEGIN
  execute immediate 'insert into debug (message) values ('' inside f_update_sia_dofid '')';
    
  lc_jReturn := json();
  lc_jReturn.put('error','nofileid');
  
  lc_jParameterJson := json(in_cJson);
  lc_nStrId := getAttributeValue('siastructureid', lc_jParameterJson);
  lc_nContinue := f_test_for_error(lc_nStrId); 
  
  if lc_nContinue = 1 then
    lc_nDofid := getAttributeValue('dofid', lc_jParameterJson);
    lc_nContinue := f_test_for_error(lc_nDofid);
  else
    lc_vErrorDescription := 'StructureId attribute not found';
    raise spiders_error;
  end if;
  
  if lc_nContinue = 1 then
    execute immediate 'update spd_t_sia_data set dofid=''' || lc_nDofid || '''where siastructureid= ''' || lc_nStrId || '''';
    lc_jReturn := json();
    lc_jReturn.put('success',1);
  else
    lc_vErrorDescription := 'DOFID attricute not found';
  end if;
  
  return lc_jReturn;

  exception     
    when spiders_error then
        execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' SPIDERS error ' || lc_vErrorDescription || ''')';
        return json('{"error":"' || lc_vFunctionName || ' SPIDERS error ' || lc_vErrorDescription || '"}');
    when json_error then
        execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' JSON error'')';
        return json('{"error":"' || lc_vFunctionName || ' JSON error ' || lc_vErrorDescription || '"}');
    when others then
        execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' dml error: ' || sqlerrm || ''')';
       return json('{"error":"' || lc_vFunctionName || ' dml error ' || sqlerrm || '"}');
  
  END f_update_sia_dofid;
  
  function getAttributeValue(in_vAttribute varchar2, in_jJSON json) return varchar2 as 
  /* template variables */
  json_error EXCEPTION;
  spiders_error EXCEPTION;
  lc_vErrorDescription varchar2(255) := 'default';
  lc_vFunctionName varchar2(255) := 'getAttributeValue';
  lc_vReturn varchar2(32000) := '{"error":"default"}'; --defaul the return to e for error
  lc_nContinue number :=0; --default to 0 to dissalow continuing
  
  /* function specific variables */
  lc_jvValue json_value;
begin
  
  if(in_jJSON.exist(in_vAttribute)) then       
    lc_jvValue := in_jJSON.get(in_vAttribute);
    lc_vReturn := getVal(lc_jvValue);
  else
    lc_vErrorDescription := in_vAttribute || ' attribute not found in json';
    raise spiders_error;
  end if;
  
  /* get name of object */
  return lc_vReturn;

exception     
  when spiders_error then
      execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' SPIDERS error ' || lc_vErrorDescription || ''')';
      return '{"error":"' || lc_vErrorDescription || '"}';
  when json_error then
      execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' JSON error'')';
      return '{"error":"json"}';
  when others then
      execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' dml error: ' || sqlerrm || ''')';
      return '{"error":"' || sqlerrm || '"}';
end; 
 
 
function f_test_for_error (in_vReturned varchar2) return varchar2 as 
  /* template variables */
  lc_vFunctionName varchar2(255) := 'f_test_for_error';
  lc_vReturn varchar2(32000) := '{"error":"default"}'; --defaul the return to e for error
  lc_nContinue number :=0; --default to 0 to dissalow continuing
  
  /* function specific variables */
  lc_vTest varchar2(32000) := in_vReturned;
begin

  if substr(lc_vTest, 1, 9) = '{"error":' then
    lc_vReturn := '0';
  else
    lc_vReturn := '1';
  end if;
  
  return lc_vReturn;

exception     
  when others then
      execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' dml error: ' || sqlerrm || ''')';
      return '{"error":"' || sqlerrm || '"}';
end;

END PKG_SIA_UPLOAD;

/
