--------------------------------------------------------
--  DDL for Package Body PKG_COST_ESTIMATE_XML
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE PACKAGE BODY "SPIDERS_DATA"."PKG_COST_ESTIMATE_XML" as 

function f_test_for_error (in_vReturned varchar2) return varchar2 as 
  /* template variables */
  lc_vFunctionName varchar2(255) := 'f_test_for_error';
  lc_vReturn varchar2(32000) := '{"error":"default"}'; --defaul the return to e for error
  lc_nContinue number :=0; --default to 0 to dissalow continuing
  
  /* function specific variables */
  lc_vTest varchar2(32000) := in_vReturned;
begin

  if substr(lc_vTest, 1, 9) = '{"error":' then
    lc_vReturn := '0';
  else
    lc_vReturn := '1';
  end if;
  
  return lc_vReturn;

exception     
  when others then
      execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' dml error: ' || sqlerrm || ''')';
      return '{"error":"' || sqlerrm || '"}';
end; 


function getAttributeValue(in_vAttribute varchar2, in_jJSON json) return varchar2 as 
  /* template variables */
  json_error EXCEPTION;
  spiders_error EXCEPTION;
  lc_vErrorDescription varchar2(255) := 'default';
  lc_vFunctionName varchar2(255) := 'getAttributeValue';
  lc_vReturn varchar2(32000) := '{"error":"default"}'; --defaul the return to e for error
  lc_nContinue number :=0; --default to 0 to dissalow continuing
  
  /* function specific variables */
  lc_jvValue json_value;
begin
  
  if(in_jJSON.exist(in_vAttribute)) then       
    lc_jvValue := in_jJSON.get(in_vAttribute);
    lc_vReturn := getVal(lc_jvValue);
  else
    lc_vErrorDescription := in_vAttribute || ' attribute not found in json';
    raise spiders_error;
  end if;
  
  /* get name of object */
  return lc_vReturn;

exception     
  when spiders_error then
      execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' SPIDERS error ' || lc_vErrorDescription || ''')';
      return '{"error":"' || lc_vErrorDescription || '"}';
  when json_error then
      execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' JSON error'')';
      return '{"error":"json"}';
  when others then
      execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' dml error: ' || sqlerrm || ''')';
      return '{"error":"' || sqlerrm || '"}';
end; 
 

function getCorrespondingJsonColumnVal(in_jJson json, in_vColumn varchar2) return varchar2 as 
  /* template variables */
  json_error EXCEPTION;
  spiders_error EXCEPTION;
  lc_vErrorDescription varchar2(255) := 'default';
  lc_vFunctionName varchar2(255) := 'getCorrespondingJsonColumnVal';
  lc_vReturn varchar2(32000) := '{"error":"default"}'; --defaul the return to e for error
  lc_nContinue number :=0; --default to 0 to dissalow continuing
  
  /* function specific variables */
  lc_vName varchar2(2000);
  lc_vTemp varchar2(2000);
  lc_vComment varchar2(2000);
  lc_jvValue json_value;
  lc_jlData json_list;
  lc_jvRecVal json_value;
  lc_jValue json;
begin

  /* i could not convince myself that we needed multiple functions here, since we still need to check if attributes exist in the sub functions*/
  if(in_jJson.exist('data')) then
      if(in_jJson.get('data').is_array) then
          lc_jlData := json_list(in_jJson.get('data'));        
          lc_jvRecVal := lc_jlData.get(1); --return null on outofbounds
          if (lc_jvRecVal is not null) then
            lc_jValue := json(lc_jvRecVal);            
            if(lc_jValue.exist(in_vColumn)) then       
              lc_jvValue := lc_jValue.get(in_vColumn);
              lc_vReturn := getVal(lc_jvValue);
            else
              lc_vErrorDescription := in_vColumn || ' attribute not found in json';
              raise spiders_error;
            end if;
          else 
            lc_vErrorDescription := 'Rec val is null';
            raise spiders_error;
          end if;
      else
        lc_vErrorDescription := 'Data is not an array';
        raise spiders_error;
      end if;
  else
    lc_vErrorDescription := 'Data attribute not found in json';
    raise spiders_error;
  end if;
            
  return lc_vReturn;

exception     
  when spiders_error then
      execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' SPIDERS error ' || lc_vErrorDescription || ''')';
      return '{"error":"' || lc_vErrorDescription || '"}';
  when json_error then
      execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' JSON error'')';
      return '{"error":"json"}';
  when others then
      execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' dml error: ' || sqlerrm || ''')';
      return '{"error":"' || sqlerrm || '"}';
end; 

function getFacilityList(in_vDeliveryOrderId varchar2) return json as 
  /* template variables */
  json_error EXCEPTION;
  spiders_error EXCEPTION;
  lc_vErrorDescription varchar2(255) := 'default';
  lc_vFunctionName varchar2(255) := 'getFacilityList';
  lc_jReturn json; --defaul the return to e for error
  lc_nContinue number :=0; --default to 0 to dissalow continuing
  
  /* function specific variables */
  lc_vSql long;
  lc_jJson json;
begin

  lc_vSql := '
  select
    delivery_order_fac.DELIVERYORDERFACILITYID  DELIVERYORDERFACILITYID,
    delivery_order_fac.FACID                    FACID,
    facility.SPD_FAC_NAME                       SPD_FAC_NAME,
    facility.SPD_FAC_NO                         SPD_FAC_NO,
    facility.INFADS_FACILITY_ID                 INFADS_FACILITY_ID,
    delivery_order.DELIVERYORDERID              DELIVERYORDERID
  from
    spd_t_delivery_order_facs   delivery_order_fac,
    spd_t_facilities            facility,
    spd_t_delivery_orders       delivery_order
  where
    delivery_order_fac.deliveryorderid = delivery_order.deliveryorderid
    and delivery_order_fac.facid = facility.facid
    and delivery_order.deliveryorderid = ' || in_vDeliveryOrderId || '
  order by 
    facility.SPD_FAC_NO';
  
  lc_jJson := json_dyn.executeObject(lc_vSql);
    
  lc_jReturn := lc_jJson;
  return lc_jReturn;

exception     
  when spiders_error then
      execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' SPIDERS error ' || lc_vErrorDescription || ''')';
      return json('{"error":"' || lc_vErrorDescription || '"}');
  when json_error then
      execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' JSON error'')';
      return json('{"error":"json"}');
  when others then
      execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' dml error: ' || sqlerrm || ''')';
      return json('{"error":"' || sqlerrm || '"}');
end; 

function getContractorList(in_vDeliveryOrderId varchar2) return json as 
  /* template variables */
  json_error EXCEPTION;
  spiders_error EXCEPTION;
  lc_vErrorDescription varchar2(255) := 'default';
  lc_vFunctionName varchar2(255) := 'getContractorList';
  lc_jReturn json; --defaul the return to e for error
  lc_nContinue number :=0; --default to 0 to dissalow continuing
  
  /* function specific variables */
  lc_vSql long;
  lc_jJson json;
begin

  lc_vSql := '
  select
    contractor.CONTRACTOR_NAME      CONTRACTOR_NAME,
    job_title.job_title             JOB_TITLE,
    job_title.visual_order          VISUAL_ORDER,
    job_title.BASE_RATE             BASE_RATE,
    job_title.OPTION_1_RATE         OPTION_1_RATE,
    job_title.OPTION_2_RATE         OPTION_2_RATE,
    job_title.OPTION_3_RATE         OPTION_3_RATE,
    job_title.OPTION_4_RATE         OPTION_4_RATE,
    delivery_order.DELIVERYORDERID  DELIVERYORDERID
  from
    spd_t_contractors           contractor,
    spd_t_contract_contractors  contract_contractor,
    spd_t_job_titles            job_title,
    spd_t_delivery_orders       delivery_order,
    spd_t_pwss                  pws
  where
    contractor.contractorid = contract_contractor.contractorid
    and contract_contractor.contractcontractorid = job_title.contractcontractorid
    and delivery_order.pwsid = pws.pwsid
    and pws.contractid = contract_contractor.contractid
    and delivery_order.deliveryorderid = ' || in_vDeliveryOrderId || '
  order by 
    job_title.visual_order';
  
  lc_jJson := json_dyn.executeObject(lc_vSql);
    
  lc_jReturn := lc_jJson;
  return lc_jReturn;

exception     
  when spiders_error then
      execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' SPIDERS error ' || lc_vErrorDescription || ''')';
      return json('{"error":"' || lc_vErrorDescription || '"}');
  when json_error then
      execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' JSON error'')';
      return json('{"error":"json"}');
  when others then
      execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' dml error: ' || sqlerrm || ''')';
      return json('{"error":"' || sqlerrm || '"}');
end; 

function getFacilityDirectCosts(in_vDeliveryOrderId varchar2) return json as 
  /* template variables */
  json_error EXCEPTION;
  spiders_error EXCEPTION;
  lc_vErrorDescription varchar2(255) := 'default';
  lc_vFunctionName varchar2(255) := 'getFacilityDirectCosts';
  lc_jReturn json; --defaul the return to e for error
  lc_nContinue number :=0; --default to 0 to dissalow continuing
  
  /* function specific variables */
  lc_vSql long;
  lc_jJson json;
begin

  lc_vSql := '
  select
    direct_cost.DIRECTCOSTID      DIRECTCOSTID,
    direct_cost.DIRECT_COST       DIRECT_COST,
    direct_cost.VISUAL_ORDER      VISUAL_ORDER,
    direct_cost.DESCRIPTION       DESCRIPTION,
    direct_cost.DIRECT_COST_TYPE  DIRECT_COST_TYPE,
    delivery_order.DELIVERYORDERID  DELIVERYORDERID
  from
    spd_t_delivery_orders       delivery_order,
    spd_t_direct_costs          direct_cost
  where
    direct_cost.pwsid = delivery_order.pwsid
    and upper(direct_cost.direct_cost_type) = ''FACILITY''
    and delivery_order.deliveryorderid = ' || in_vDeliveryOrderId || '
  order by 
    direct_cost.visual_order';
  
  lc_jJson := json_dyn.executeObject(lc_vSql);
    
  lc_jReturn := lc_jJson;
  return lc_jReturn;

exception     
  when spiders_error then
      execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' SPIDERS error ' || lc_vErrorDescription || ''')';
      return json('{"error":"' || lc_vErrorDescription || '"}');
  when json_error then
      execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' JSON error'')';
      return json('{"error":"json"}');
  when others then
      execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' dml error: ' || sqlerrm || ''')';
      return json('{"error":"' || sqlerrm || '"}');
end; 

function getGeneralDirectCosts(in_vDeliveryOrderId varchar2) return json as 
  /* template variables */
  json_error EXCEPTION;
  spiders_error EXCEPTION;
  lc_vErrorDescription varchar2(255) := 'default';
  lc_vFunctionName varchar2(255) := 'getGeneralDirectCosts';
  lc_jReturn json; --defaul the return to e for error
  lc_nContinue number :=0; --default to 0 to dissalow continuing
  
  /* function specific variables */
  lc_vSql long;
  lc_jJson json;
begin

  lc_vSql := '
  select
    direct_cost.DIRECTCOSTID      DIRECTCOSTID,
    direct_cost.DIRECT_COST       DIRECT_COST,
    direct_cost.VISUAL_ORDER      VISUAL_ORDER,
    direct_cost.DESCRIPTION       DESCRIPTION,
    direct_cost.DIRECT_COST_TYPE  DIRECT_COST_TYPE,
    delivery_order.DELIVERYORDERID  DELIVERYORDERID
  from
    spd_t_delivery_orders       delivery_order,
    spd_t_direct_costs          direct_cost
  where
    direct_cost.pwsid = delivery_order.pwsid
    and upper(direct_cost.direct_cost_type) = ''GENERAL''
    and delivery_order.deliveryorderid = ' || in_vDeliveryOrderId || '
  order by 
    direct_cost.visual_order';
  
  lc_jJson := json_dyn.executeObject(lc_vSql);
    
  lc_jReturn := lc_jJson;
  return lc_jReturn;

exception     
  when spiders_error then
      execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' SPIDERS error ' || lc_vErrorDescription || ''')';
      return json('{"error":"' || lc_vErrorDescription || '"}');
  when json_error then
      execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' JSON error'')';
      return json('{"error":"json"}');
  when others then
      execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' dml error: ' || sqlerrm || ''')';
      return json('{"error":"' || sqlerrm || '"}');
end; 

function getGeneralLaborCatgories(in_vDeliveryOrderId varchar2) return json as 
  /* template variables */
  json_error EXCEPTION;
  spiders_error EXCEPTION;
  lc_vErrorDescription varchar2(255) := 'default';
  lc_vFunctionName varchar2(255) := 'getGeneralLaborCatgories';
  lc_jReturn json; --defaul the return to e for error
  lc_nContinue number :=0; --default to 0 to dissalow continuing
  
  /* function specific variables */
  lc_vSql long;
  lc_jJson json;
begin

  lc_vSql := '
  select
    labor_category.LABORCATEGORYID              LABORCATEGORYID,
    labor_category.LABOR_CATEGORY               LABOR_CATEGORY,
    labor_category.LABOR_CATEGORY_TYPE          LABOR_CATEGORY_TYPE,
    labor_category.LABOR_CATEGORY_DESCRIPTION   LABOR_CATEGORY_DESCRIPTION,
    delivery_order.DELIVERYORDERID              DELIVERYORDERID
  from
    spd_t_delivery_orders       delivery_order,
    spd_t_labor_categories      labor_category
  where
    labor_category.pwsid = delivery_order.pwsid
    and upper(labor_category.LABOR_CATEGORY_TYPE) = ''GENERAL''
    and delivery_order.deliveryorderid = ' || in_vDeliveryOrderId || '
  order by 
    labor_category.labor_category';
  
  lc_jJson := json_dyn.executeObject(lc_vSql);
    
  lc_jReturn := lc_jJson;
  return lc_jReturn;

exception     
  when spiders_error then
      execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' SPIDERS error ' || lc_vErrorDescription || ''')';
      return json('{"error":"' || lc_vErrorDescription || '"}');
  when json_error then
      execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' JSON error'')';
      return json('{"error":"json"}');
  when others then
      execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' dml error: ' || sqlerrm || ''')';
      return json('{"error":"' || sqlerrm || '"}');
end; 

function getFacilityLaborCatgories(in_vDeliveryOrderId varchar2) return json as 
  /* template variables */
  json_error EXCEPTION;
  spiders_error EXCEPTION;
  lc_vErrorDescription varchar2(255) := 'default';
  lc_vFunctionName varchar2(255) := 'getFacilityLaborCatgories';
  lc_jReturn json; --defaul the return to e for error
  lc_nContinue number :=0; --default to 0 to dissalow continuing
  
  /* function specific variables */
  lc_vSql long;
  lc_jJson json;
begin

  lc_vSql := '
  select
    labor_category.LABORCATEGORYID              LABORCATEGORYID,
    labor_category.LABOR_CATEGORY               LABOR_CATEGORY,
    labor_category.LABOR_CATEGORY_TYPE          LABOR_CATEGORY_TYPE,
    labor_category.LABOR_CATEGORY_DESCRIPTION   LABOR_CATEGORY_DESCRIPTION,
    delivery_order.DELIVERYORDERID              DELIVERYORDERID
  from
    spd_t_delivery_orders       delivery_order,
    spd_t_labor_categories      labor_category
  where
    labor_category.pwsid = delivery_order.pwsid
    and upper(labor_category.LABOR_CATEGORY_TYPE) = ''FACILITY''
    and delivery_order.deliveryorderid = ' || in_vDeliveryOrderId || '
  order by 
    labor_category.labor_category';
  
  lc_jJson := json_dyn.executeObject(lc_vSql);
    
  lc_jReturn := lc_jJson;
  return lc_jReturn;

exception     
  when spiders_error then
      execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' SPIDERS error ' || lc_vErrorDescription || ''')';
      return json('{"error":"' || lc_vErrorDescription || '"}');
  when json_error then
      execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' JSON error'')';
      return json('{"error":"json"}');
  when others then
      execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' dml error: ' || sqlerrm || ''')';
      return json('{"error":"' || sqlerrm || '"}');
end; 

function getInstructions(in_vDeliveryOrderId varchar2) return json as 
  /* template variables */
  json_error EXCEPTION;
  spiders_error EXCEPTION;
  lc_vErrorDescription varchar2(255) := 'default';
  lc_vFunctionName varchar2(255) := 'getInstructions';
  lc_jReturn json; --defaul the return to e for error
  lc_nContinue number :=0; --default to 0 to dissalow continuing
  
  /* function specific variables */
  lc_vSql long;
  lc_jJson json;
begin

  lc_vSql := '
  select
    instruction.INSTRUCTIONID                     INSTRUCTIONID,
    instruction.GROUPING                          GROUPING,
    instruction.INSTRUCTION                       INSTRUCTION,
    instruction.VISUAL_ORDER                      VISUAL_ORDER,
    delivery_order.DELIVERYORDERID                DELIVERYORDERID,
    delivery_order_fac.DELIVERYORDERFACILITYID    DELIVERYORDERFACILITYID,
    do_fac_instruction.DOFACINSTRUCTIONID         DOFACINSTRUCTIONID
  from
    spd_t_delivery_orders       delivery_order,
    spd_t_instructions          instruction,
    spd_t_delivery_order_facs   delivery_order_fac,
    spd_t_do_fac_instructions   do_fac_instruction
  where
    delivery_order_fac.deliveryorderid = delivery_order.deliveryorderid
    and do_fac_instruction.instructionid = instruction.instructionid
    and instruction.pwsid = delivery_order.pwsid
    and delivery_order.deliveryorderid = ' || in_vDeliveryOrderId || '
  order by 
    instruction.GROUPING asc, 
    instruction.VISUAL_ORDER asc';
  
  lc_jJson := json_dyn.executeObject(lc_vSql);
    
  lc_jReturn := lc_jJson;
  return lc_jReturn;

exception     
  when spiders_error then
      execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' SPIDERS error ' || lc_vErrorDescription || ''')';
      return json('{"error":"' || lc_vErrorDescription || '"}');
  when json_error then
      execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' JSON error'')';
      return json('{"error":"json"}');
  when others then
      execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' dml error: ' || sqlerrm || ''')';
      return json('{"error":"' || sqlerrm || '"}');
end; 

function getDeliveryOrderDetails(in_vDeliveryOrderId varchar2) return json as 
  /* template variables */
  json_error EXCEPTION;
  spiders_error EXCEPTION;
  lc_vErrorDescription varchar2(255) := 'default';
  lc_vFunctionName varchar2(255) := 'getDeliveryOrderDetails';
  lc_jReturn json; --defaul the return to e for error
  lc_nContinue number :=0; --default to 0 to dissalow continuing
  
  /* function specific variables */
  lc_vSql long;
  lc_jJson json;
begin

  lc_vSql := '
  select
    *
  from
    spd_t_delivery_orders   delivery_order
  where
    delivery_order.deliveryorderid = ' || in_vDeliveryOrderId;
  
  lc_jJson := json_dyn.executeObject(lc_vSql);
    
  lc_jReturn := lc_jJson;
  return lc_jReturn;

exception     
  when spiders_error then
      execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' SPIDERS error ' || lc_vErrorDescription || ''')';
      return json('{"error":"' || lc_vErrorDescription || '"}');
  when json_error then
      execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' JSON error'')';
      return json('{"error":"json"}');
  when others then
      execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' dml error: ' || sqlerrm || ''')';
      return json('{"error":"' || sqlerrm || '"}');
end; 

function f_generate(in_cJSON clob) return json as 
  /* template variables */
  json_error EXCEPTION;
  spiders_error EXCEPTION;
  lc_vErrorDescription varchar2(255) := 'default';
  lc_vFunctionName varchar2(255) := 'f_generate';
  lc_vRest varchar2(255) := 'CREATEMODELCUSTOM';
  lc_jReturn json; 
  lc_nContinue number :=0; --default to 0 to dissalow continuing
  
  /* function specific variables */
  lc_vModule varchar2(255);
  lc_jParameterJson json;
  lc_jlDML json_list;
  lc_jResults json;
  lc_jMessages json;
  lc_vDMLResult varchar2(255);
  lc_vSql long;
  lc_dNow date;
  
  lc_vFacilityNFA long;
  lc_jContractors json;
  lc_vJobTitles long;
  lc_vActionLaborCategories long;
  lc_vActionDirectCosts long;
  lc_vFacLaborCategories long;
  lc_vFacDirectCosts long;
  lc_vSpecInstructionTitles long;
  lc_vSpecInstructionValues long;
  lc_nTotalFacilityCount long;
  lc_vDELIVERYORDERID long; --maps to in_nADOID
  lc_nAPID number;
  lc_nProductlineListID number;
  lc_vEIC long;
  lc_vCommand long;
  lc_vDate long;
  lc_vFY long;
  lc_vContractNumber long;
  lc_vLocation long;
  lc_vUIC long;
  lc_vFundsource long;
  lc_vProjectedStartDate long;
  lc_vWorkType long;
  
  
  lc_vTarget varchar2(255);
  lc_vIsAuthorized varchar2(255);
  lc_vTargetFunctionName long;
  lc_jFacilities json;
  lc_jFacilityDirectCosts json;
  lc_jGeneralDirectCosts json;
  lc_jGeneralLaborCategories json;
  lc_jFacilityLaborCategories json;
  lc_jInstructions json;
  lc_jDeliveryOrderDetails json;
  lc_jTemp json;
  
begin

  lc_jTemp := json();
  
  lc_jParameterJson := json(in_cJson);
  lc_vModule := getAttributeValue('module', lc_jParameterJson);
  lc_nContinue := f_test_for_error(lc_vModule);
  
  if lc_nContinue = 1 then
    lc_vTarget := getAttributeValue('target', lc_jParameterJson);
    lc_nContinue := f_test_for_error(lc_vTarget);
  else
    lc_vErrorDescription := 'Module attribute not found';
    raise spiders_error;
  end if;

/*
  if lc_nContinue = 1 then
    lc_vIsAuthorized := BB_F_AUTHORIZATION('BB_REST.create_',lc_vModule);
  else
    lc_vErrorDescription := 'target attribute not found';
    raise spiders_error; 
  end if; 
*/

  lc_vIsAuthorized := 'y'; --!!REMOVE AFTER TESTING
  
  if lc_vIsAuthorized = 'y' then
    lc_vTargetFunctionName := getTargetFunctionName(lc_vRest,lc_vModule,lc_vTarget);
    lc_nContinue := f_test_for_error(lc_vTargetFunctionName);
  else
    lc_vErrorDescription := 'NOT AUTHORIZED';
    raise spiders_error; 
  end if;
  
  if lc_nContinue = 1 then
    lc_vDELIVERYORDERID := getCorrespondingJsonColumnVal(lc_jParameterJson, 'DELIVERYORDERID');
    lc_nContinue := f_test_for_error(lc_vDELIVERYORDERID);
  else
    lc_vErrorDescription := 'problem getting target function name';
    raise spiders_error;
  end if;
  
  if lc_nContinue = 1 then
    lc_jFacilities := getFacilityList(lc_vDELIVERYORDERID);
    lc_nContinue := f_test_for_error(lc_vTarget);
  else
    lc_vErrorDescription := 'problem getting delivery order id';
    raise spiders_error;
  end if;
  
  if lc_nContinue = 1 then
    lc_jContractors := getContractorList(lc_vDELIVERYORDERID);
    lc_nContinue := f_test_for_error(lc_vTarget);
  else
    lc_vErrorDescription := 'problem getting facility list';
    raise spiders_error;
  end if;
  
  if lc_nContinue = 1 then
    lc_jFacilityDirectCosts := getFacilityDirectCosts(lc_vDELIVERYORDERID); 
    lc_nContinue := f_test_for_error(lc_vTarget);
  else
    lc_vErrorDescription := 'problem getting contractors list';
    raise spiders_error;
  end if;
  
  if lc_nContinue = 1 then
    lc_jGeneralDirectCosts := getGeneralDirectCosts(lc_vDELIVERYORDERID); 
    lc_nContinue := f_test_for_error(lc_vTarget);
  else
    lc_vErrorDescription := 'problem getting facility direct costs list';
    raise spiders_error;
  end if;
  
  if lc_nContinue = 1 then
    lc_jGeneralLaborCategories := getGeneralLaborCatgories(lc_vDELIVERYORDERID); 
    lc_nContinue := f_test_for_error(lc_vTarget);
  else
    lc_vErrorDescription := 'problem getting general direct costs list';
    raise spiders_error;
  end if;
  
  if lc_nContinue = 1 then
    lc_jFacilityLaborCategories := getFacilityLaborCatgories(lc_vDELIVERYORDERID); 
    lc_nContinue := f_test_for_error(lc_vTarget);
  else
    lc_vErrorDescription := 'problem getting general labor categories';
    raise spiders_error;
  end if;
  
  if lc_nContinue = 1 then
    lc_jInstructions := getInstructions(lc_vDELIVERYORDERID); 
    lc_nContinue := f_test_for_error(lc_vTarget);
  else
    lc_vErrorDescription := 'problem getting facility labor categories';
    raise spiders_error;
  end if;
  
  if lc_nContinue = 1 then
    lc_jDeliveryOrderDetails := getDeliveryOrderDetails(lc_vDELIVERYORDERID); 
    lc_nContinue := f_test_for_error(lc_vTarget);
  else
    lc_vErrorDescription := 'problem getting instructions';
    raise spiders_error;
  end if;
  
  lc_jTemp.put('deliveryorderid',lc_vDELIVERYORDERID);
  lc_jTemp.put('contractors',lc_jContractors);
  lc_jTemp.put('facilityDirectCosts',lc_jFacilityDirectCosts);
  lc_jTemp.put('generalDirectCosts',lc_jGeneralDirectCosts);
  lc_jTemp.put('generalLaborCategories',lc_jGeneralLaborCategories);
  lc_jTemp.put('facilityLaborCategories',lc_jFacilityLaborCategories);
  lc_jTemp.put('facilities',lc_jFacilities);
  lc_jTemp.put('instructions',lc_jInstructions);
  lc_jTemp.put('deliveryOrder',lc_jDeliveryOrderDetails);
  lc_jTemp.htp;
/*
list aggregate queries needed

√ in_vContractors,
√ in_vJobTitles,
√ in_vActionLaborCategories,
√ in_vActionDirectCosts,
√ in_vFacLaborCategories,
√ in_vFacDirectCosts,
√ in_vSpecInstructionTitles,
√ in_vSpecInstructionValues,

√ in_nTotalFacilityCount, from facilities.data.count
√ in_nADOID,
X in_nAPID, deprecated
in_nProductlineListID,
in_vEIC,
in_vCommand,
in_vDate,
in_vContractNumber,
in_vLocation,
in_vUIC,
in_vProjectedStartDate,
in_vFundsource,
in_vWorkType




in_vFY,


  lc_jlDML.append(lc_vSql);

  lc_jResults := json();
  lc_jResults.put('results',lc_jlDML);
  lc_jMessages := json();
  lc_vDMLResult := BB_F_DML(lc_jResults, lc_jMessages);
  lc_jReturn := lc_jMessages;
  return lc_jReturn;
  */
  
  return json();

exception     
  when spiders_error then
      execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' SPIDERS error ' || lc_vErrorDescription || ''')';
      return json('{"error":"' || lc_vFunctionName || ' SPIDERS error ' || lc_vErrorDescription || '"}');
  when json_error then
      execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' JSON error'')';
      return json('{"error":"' || lc_vFunctionName || ' JSON error ' || lc_vErrorDescription || '"}');
  when others then
      execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' dml error: ' || sqlerrm || ''')';
     return json('{"error":"' || lc_vFunctionName || ' dml error ' || sqlerrm || '"}');
end; 

end;

/
