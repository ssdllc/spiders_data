set define off;

create or replace PACKAGE BODY                "PKG_SPIDERS_3D_SESSIONS" AS

  function f_test_for_error (in_vReturned varchar2) return varchar2 as 
    /* template variables */
    lc_vFunctionName varchar2(255) := 'f_test_for_error';
    lc_vReturn varchar2(32000) := '{"error":"default"}'; --defaul the return to e for error
    lc_nContinue number :=0; --default to 0 to dissalow continuing
    
    /* function specific variables */
    lc_vTest varchar2(32000) := in_vReturned;
  begin
  
    if substr(lc_vTest, 1, 9) = '{"error":' then
      lc_vReturn := '0';
    else
      lc_vReturn := '1';
    end if;
    
    return lc_vReturn;
  
  exception     
    when others then
        execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' dml error: ' || sqlerrm || ''')';
        return '{"error":"' || sqlerrm || '"}';
  end; 

  function getJsonList(in_vAttribute varchar2, in_jJSON json) return json_list as 
    /* template variables */
    lc_vFunctionName varchar2(255) := 'getJsonList';
    
    json_error EXCEPTION;
    spiders_error EXCEPTION;
    lc_vErrorDescription varchar2(255) := 'default';
    lc_jlReturn json_list; --defaul the return to e for error
    lc_nContinue number :=0; --default to 0 to dissalow continuing
    
    /* function specific variables */
    lc_jlData json_list;
  begin
    
    if(in_jJSON.exist(in_vAttribute)) then 
      if(in_jJson.get(in_vAttribute).is_array) then
        lc_jlData := json_list(in_jJson.get(in_vAttribute));
      else
        lc_jlData := json_list();
      end if;
    else
      lc_vErrorDescription := in_vAttribute || ' attribute not found in json';
      raise spiders_error;
    end if;
    
    lc_jlReturn := lc_jlData;
    return lc_jlReturn;
  
  exception     
    when spiders_error then
        execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' SPIDERS error ' || lc_vErrorDescription || ''')';
        return json_list('{"error":"' || lc_vErrorDescription || '"}');
    when json_error then
        execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' JSON error'')';
        return json_list('{"error":"json"}');
    when others then
        execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' dml error: ' || sqlerrm || ''')';
        return json_list('{"error":"' || sqlerrm || '"}');
  end; 
  
  function getJsonListItem(in_nRow number, in_jlJSON json_list) return json as 
    /* template variables */
    lc_vFunctionName varchar2(255) := 'getJsonList';
    
    json_error EXCEPTION;
    spiders_error EXCEPTION;
    lc_vErrorDescription varchar2(255) := 'default';
    lc_jReturn json; --defaul the return to e for error
    lc_nContinue number :=0; --default to 0 to dissalow continuing
    
    /* function specific variables */
    lc_jData json;
  begin
    
    lc_jData := json(in_jlJSON.get(in_nRow));
    
    lc_jReturn := lc_jData;
    return lc_jReturn;
  
  exception     
    when spiders_error then
        execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' SPIDERS error ' || lc_vErrorDescription || ''')';
        return json('{"error":"' || lc_vErrorDescription || '"}');
    when json_error then
        execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' JSON error'')';
        return json('{"error":"json"}');
    when others then
        execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' dml error: ' || sqlerrm || ''')';
        return json('{"error":"' || sqlerrm || '"}');
  end; 
  
  function makeInsertSessionMembersSql(lc_nSessionMemberId number) return varchar2 as 
    /* template variables */
    lc_vFunctionName varchar2(255) := 'makeInsertFacilitySql';
    json_error EXCEPTION;
    spiders_error EXCEPTION;
    lc_vErrorDescription varchar2(255) := 'default';
    lc_vReturn long := '{"error":"default"}';
    lc_nContinue number :=0; --default to 0 to dissalow continuing
    
    /* function specific variables */
    lc_jJson json;
    lc_vSql long;
  begin
    
     
    lc_vSql :=  'insert into SPD_T_3D_SESSION_MEMBERS (username, SESSIONMEMBERID) values ( v(''APP_USER''),' || lc_nSessionMemberId || ')';

    
    lc_vReturn := lc_vSql;
    /* get name of object */
    return lc_vReturn;
  
  exception     
    when spiders_error then
        execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' SPIDERS error ' || lc_vErrorDescription || ''')';
        return '{"error":"' || lc_vErrorDescription || '"}';
    when json_error then
        execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' JSON error'')';
        return '{"error":"json"}';
    when others then
        execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' dml error: ' || sqlerrm || ''')';
        return '{"error":"' || sqlerrm || '"}';
  end; 
  
  function getSessionId(lc_vSessionKey varchar2) return number as
   /* template variables */
    lc_vFunctionName varchar2(255) := 'getSessionId';
  
    json_error EXCEPTION;
    spiders_error EXCEPTION;
    lc_vErrorDescription varchar2(255) := 'default';
  
    lc_nReturn number;
    lc_nContinue number :=0; --default to 0 to dissalow continuing

    /* function specific variables */
  begin
    lc_nReturn :=0;
  
    --THERE WILL BE ISSUES WITH THIS IF THERE IS MORE THAN RECORD FOR THE SAME NFAID IN SPIDERS TABLE
    for rec in (select sessionid from spd_t_3d_sessions where upper(session_key) = upper(lc_vSessionKey)) loop
      lc_nReturn := rec.sessionid;
    end loop;
  
  
    lc_nReturn := lc_nReturn;
    return lc_nReturn;
  
  exception   
    when spiders_error then
        execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' SPIDERS error ' || lc_vErrorDescription || ''')';
        return -1;
    when json_error then
        execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' JSON error'')';
        return -2;
    when others then
        execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' dml error: ' || sqlerrm || ''')';
        return -3;
  end; 
  
  function getSceneId(lc_nSessionId number) return number as
   /* template variables */
    lc_vFunctionName varchar2(255) := 'getSessionId';
  
    json_error EXCEPTION;
    spiders_error EXCEPTION;
    lc_vErrorDescription varchar2(255) := 'default';
  
    lc_nReturn number;
    lc_nContinue number :=0; --default to 0 to dissalow continuing

    /* function specific variables */
  begin
    lc_nReturn :=0;
  
    --THERE WILL BE ISSUES WITH THIS IF THERE IS MORE THAN RECORD FOR THE SAME NFAID IN SPIDERS TABLE
    for rec in (select sceneid from spd_t_3d_sessions where sessionid = lc_nSessionId) loop
      lc_nReturn := rec.sceneid;
    end loop;
  
  
    lc_nReturn := lc_nReturn;
    return lc_nReturn;
  
  exception   
    when spiders_error then
        execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' SPIDERS error ' || lc_vErrorDescription || ''')';
        return -1;
    when json_error then
        execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' JSON error'')';
        return -2;
    when others then
        execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' dml error: ' || sqlerrm || ''')';
        return -3;
  end; 
  
  function getSessionInfo(lc_nSessionId number) return json as
   /* template variables */
    lc_vFunctionName varchar2(255) := 'getSessionInfo';
  
    json_error EXCEPTION;
    spiders_error EXCEPTION;
    lc_vErrorDescription varchar2(255) := 'default';
  
    lc_jReturn json;
    lc_nContinue number :=0; --default to 0 to dissalow continuing

    /* function specific variables */
  begin
    lc_jReturn := json();
  
    --THERE WILL BE ISSUES WITH THIS IF THERE IS MORE THAN RECORD FOR THE SAME NFAID IN SPIDERS TABLE
    for rec in (select short_description from spd_t_3d_sessions where sessionid = lc_nSessionId) loop
      lc_jReturn.put('title', rec.short_description);
    end loop;
  
  
    --lc_nReturn := lc_nReturn;
    return lc_jReturn;
  
  exception   
    when spiders_error then
        execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' SPIDERS error ' || lc_vErrorDescription || ''')';
        --return -1;
    when json_error then
        execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' JSON error'')';
        --return -2;
    when others then
        execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' dml error: ' || sqlerrm || ''')';
        --return -3;
  end; 
  
  function getSceneDescriptionPath(lc_nSceneId number) return json as
   /* template variables */
    lc_vFunctionName varchar2(255) := 'getSceneDescriptionPath';
  
    json_error EXCEPTION;
    spiders_error EXCEPTION;
    lc_vErrorDescription varchar2(255) := 'default';
  
    lc_jReturn json;
    lc_nContinue number :=0; --default to 0 to dissalow continuing

    /* function specific variables */
  begin
    lc_jReturn := json();
  
    --THERE WILL BE ISSUES WITH THIS IF THERE IS MORE THAN RECORD FOR THE SAME NFAID IN SPIDERS TABLE
    for rec in (select description, file_path from spd_t_3d_scenes where sceneid = lc_nSceneId) loop
      lc_jReturn.put('description', rec.description);
      lc_jReturn.put('file_path', rec.file_path);
    end loop;
  
  
    --lc_nReturn := lc_nReturn;
    return lc_jReturn;
  
  exception   
    when spiders_error then
        execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' SPIDERS error ' || lc_vErrorDescription || ''')';
        --return -1;
    when json_error then
        execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' JSON error'')';
        --return -2;
    when others then
        execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' dml error: ' || sqlerrm || ''')';
        --return -3;
  end; 
  
  function getUpdatedTimestamp(lc_nSessionId number) return varchar2 as
   /* template variables */
    lc_vFunctionName varchar2(255) := 'getUpdatedTimestamp';
  
    json_error EXCEPTION;
    spiders_error EXCEPTION;
    lc_vErrorDescription varchar2(255) := 'default';
  
    lc_vReturn varchar2(225);
    lc_nContinue number :=0; --default to 0 to dissalow continuing

    /* function specific variables */
  begin
  
    --THERE WILL BE ISSUES WITH THIS IF THERE IS MORE THAN RECORD FOR THE SAME NFAID IN SPIDERS TABLE
    for rec in (select TO_CHAR(max(updated_timestamp), 'DD-MM-YY HH:MI:SS AM') updated_timestamp from  spd_t_3d_model_logs where sessionid = lc_nSessionId) loop
      lc_vReturn := rec.updated_timestamp;
    end loop;

    return lc_vReturn;
  
  exception   
    when spiders_error then
        execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' SPIDERS error ' || lc_vErrorDescription || ''')';
        
    when json_error then
        execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' JSON error'')';
        
    when others then
        execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' dml error: ' || sqlerrm || ''')';
        
  end; 
  
  function getSceneInventory(lc_nSessionId number) return json as
   /* template variables */
    lc_vFunctionName varchar2(225) := 'getSceneInventory';
  
    json_error EXCEPTION;
    spiders_error EXCEPTION;
    lc_vErrorDescription varchar2(225) := 'default';
    
    --type inventory_array IS VARRAY(225) OF VARCHAR2(225); 
    --inv_arr inventory_array := inventory_array();
    --type grades IS VARRAY(5) OF INTEGER; 
  
    lc_jReturn json;
    lc_nContinue number :=0; --default to 0 to dissalow continuing

    /* function specific variables */
  begin
  
    lc_jReturn := json();
    
    lc_jReturn := json_dyn.executeObject('
    select 
      models.model_name model_name, 
      models.model_number model_number, 
      models.filepath filepath, 
      models.description description,
      logs.color color, 
      logs.localid localid, 
      logs.modellogid modellogid, 
      logs.opacity opacity, 
      logs.translation translation, 
      logs.rotation rotation, 
      logs.pitch pitch, 
      logs.roll roll,
      models.interactivemodelid
    from  
      spd_t_3d_model_logs logs, 
      spd_t_3d_interactive_models models
    where 
      logs.interactivemodelid = models.interactivemodelid AND
      logs.sessionid = ' || lc_nSessionId || ' AND
      logs.actiontype <> ''DELETE'' ');
    
    return lc_jReturn;
  
  exception   
    when spiders_error then
        execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' SPIDERS error ' || lc_vErrorDescription || ''')';
        
    when json_error then
        execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' JSON error'')';
        
    when others then
        execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' dml error: ' || sqlerrm || ''')';
        
  end; 
  
  function getWhiteboards(lc_nSessionId number) return json as
   /* template variables */
    lc_vFunctionName varchar2(255) := 'getWhiteboards';
  
    json_error EXCEPTION;
    spiders_error EXCEPTION;
    lc_vErrorDescription varchar2(255) := 'default';
  
    lc_jReturn json;
    lc_nContinue number :=0; --default to 0 to dissalow continuing

    /* function specific variables */
  begin
  
    --lc_jReturn := json();
    
    lc_jReturn := json_dyn.executeObject('select  updated_timestamp, created_by, whiteboardid, description, position, orientation, center_of_rotation, base64_image, markings from  spd_t_3d_whiteboards where sessionid = ' || lc_nSessionId ||' order by created_timestamp desc');
    
    return lc_jReturn;
  
  exception   
    when spiders_error then
        execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' SPIDERS error ' || lc_vErrorDescription || ''')';
        
    when json_error then
        execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' JSON error'')';
        
    when others then
        execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' dml error: ' || sqlerrm || ''')';
        
  end; 
  
   function pullSessionCustObjUpdate(lc_nSessionId number, lc_vTimeStamp varchar2) return json as
   /* template variables */
    lc_vFunctionName varchar2(255) := 'pullSessionUpdate';
  
    json_error EXCEPTION;
    spiders_error EXCEPTION;
    lc_vErrorDescription varchar2(255) := 'default';
  
    lc_jReturn json;
    lc_nContinue number :=0; --default to 0 to dissalow continuing

    /* function specific variables */
  begin
  
    --lc_jReturn := json();
    
    lc_jReturn := json_dyn.executeObject('
      select 
        col.customobjectlogid,
        col.customobjectid,
        col.localid,
        col.translation,
        col.rotation,
        col.pitch,
        col.roll,
        col.actiontype,
        co.x3dnode,
        col.updated_by,
        TO_CHAR(col.updated_timestamp, ''DD-MM-YY HH:MI:SS AM'') updated_timestamp
        --DECODE(col.updated_timestamp, TO_CHAR(col.UPDATED_TIMESTAMP, ''DD-MM-YY HH:MI:SS AM''), TO_CHAR(col.created_timestamp, ''DD-MM-YY HH:MI:SS AM'')) updated_timestamp
      from 
        spd_t_3d_custom_object_logs col, spd_t_3d_custom_objects co
      where 
        co.customobjectid = col.customobjectid AND
        co.sessionid = ' || lc_nSessionId || ' AND
        col.updated_timestamp > TO_DATE(''' || lc_vTimeStamp || ''', ''DD-MM-YY HH:MI:SS AM'')');
        --DECODE(col.updated_timestamp, col.updated_timestamp, col.created_timestamp) > TO_DATE(''' || lc_vTimeStamp || ''', ''DD-MM-YY HH:MI:SS AM'')');

    return lc_jReturn;
  
  exception   
    when spiders_error then
        execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' SPIDERS error ' || lc_vErrorDescription || ''')';
        
    when json_error then
        execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' JSON error'')';
        
    when others then
        execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' dml error: ' || sqlerrm || ''')';
        
  end;
  
  function pullSessionUpdate(lc_nSessionId number, lc_vTimeStamp varchar2) return json as
   /* template variables */
    lc_vFunctionName varchar2(255) := 'pullSessionUpdate';
  
    json_error EXCEPTION;
    spiders_error EXCEPTION;
    lc_vErrorDescription varchar2(255) := 'default';
  
    lc_jReturn json;
    lc_nContinue number :=0; --default to 0 to dissalow continuing

    /* function specific variables */
  begin
  
    --lc_jReturn := json();
    
    lc_jReturn := json_dyn.executeObject('
      select 
        logs.modellogid,
        models.interactivemodelid, 
        models.filepath, 
        logs.actiontype, 
        logs.localid, 
        logs.color, 
        logs.opacity, 
        logs.translation, 
        logs.rotation, 
        logs.pitch, 
        logs.roll, 
        logs.sessionid, 
        logs.created_by,
        logs.updated_by,
        TO_CHAR(logs.UPDATED_TIMESTAMP, ''DD-MM-YY HH:MI:SS AM'') updated_timestamp
        --DECODE(logs.updated_timestamp, TO_CHAR(logs.UPDATED_TIMESTAMP, ''DD-MM-YY HH:MI:SS AM''), TO_CHAR(logs.created_timestamp, ''DD-MM-YY HH:MI:SS AM'')) updated_timestamp
      from 
        spd_t_3d_interactive_models models, 
        spd_t_3d_model_logs         logs
      where 
        logs.interactivemodelid = models.interactivemodelid AND
        logs.sessionid = ' || lc_nSessionId || ' AND
        logs.updated_timestamp > TO_DATE(''' || lc_vTimeStamp || ''', ''DD-MM-YY HH:MI:SS AM'')');
        --DECODE(logs.updated_timestamp, logs.updated_timestamp, logs.created_timestamp) > TO_DATE(''' || lc_vTimeStamp || ''', ''DD-MM-YY HH:MI:SS AM'')');
    
    
    
    return lc_jReturn;
  
  exception   
    when spiders_error then
        execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' SPIDERS error ' || lc_vErrorDescription || ''')';
        
    when json_error then
        execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' JSON error'')';
        
    when others then
        execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' dml error: ' || sqlerrm || ''')';
        
  end; 
  
  function getCustomObjectInstances(lc_nSessionId number) return json as
     /* template variables */
    lc_vFunctionName varchar2(255) := 'getCustomObjectInstances';
  
    json_error EXCEPTION;
    spiders_error EXCEPTION;
    lc_vErrorDescription varchar2(255) := 'default';
  
    lc_jReturn json;
    lc_nContinue number :=0; --default to 0 to dissalow continuing

    /* function specific variables */
  begin
  
    lc_jReturn := json();
                                          
    lc_jReturn := json_dyn.executeObject('select  col.customobjectlogid, col.customobjectid, col.localid, col.actiontype, col.translation, col.rotation, col.pitch, col.roll, co.x3dnode 
                                          from spd_t_3d_custom_objects co, spd_t_3d_custom_object_logs col 
                                          where col.actiontype <> ''DELETE'' AND co.customobjectid = col.customobjectid and co.sessionid = '|| lc_nSessionId);
/*
      select 
      models.model_name model_name, 
      models.model_number model_number, 
      models.filepath filepath, 
      models.description description,
      logs.color color, 
      logs.localid localid, 
      logs.modellogid modellogid, 
      logs.opacity opacity, 
      logs.translation translation, 
      logs.rotation rotation, 
      logs.pitch pitch, 
      logs.roll roll,
      models.interactivemodelid
    from  
      spd_t_3d_model_logs logs, 
      spd_t_3d_interactive_models models
    where 
      logs.interactivemodelid = models.interactivemodelid AND
      logs.sessionid = ' || lc_nSessionId || ' AND
      logs.actiontype <> ''DELETE'' ');
  */    
    return lc_jReturn;
  
  exception   
    when spiders_error then
        execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' SPIDERS error ' || lc_vErrorDescription || ''')';
        
    when json_error then
        execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' JSON error'')';
        
    when others then
        execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' dml error: ' || sqlerrm || ''')';
        
  end; 
  
  function getCustomObjectTypes(lc_nSessionId number) return json as
     /* template variables */
    lc_vFunctionName varchar2(255) := 'getCustomObjectTypes';
  
    json_error EXCEPTION;
    spiders_error EXCEPTION;
    lc_vErrorDescription varchar2(255) := 'default';
  
    lc_jReturn json;
    lc_nContinue number :=0; --default to 0 to dissalow continuing

    /* function specific variables */
  begin
  
    lc_jReturn := json();
    
    --htp.p('select  customobjectid, sessionid, description, type, x3dnode from spd_t_3d_custom_objects where sessionid = '|| lc_nSessionId);
                                          
    lc_jReturn := json_dyn.executeObject('select  customobjectid, sessionid, description, type, x3dnode from spd_t_3d_custom_objects where sessionid = '|| lc_nSessionId);
                                          

    return lc_jReturn;
  
  exception   
    when spiders_error then
        execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' SPIDERS error ' || lc_vErrorDescription || ''')';
        
    when json_error then
        execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' JSON error'')';
        
    when others then
        execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' dml error: ' || sqlerrm || ''')';
        
  end; 
  
  function getInteractiveModels return json as
   /* template variables */
    lc_vFunctionName varchar2(255) := 'getInteractiveModels';
  
    json_error EXCEPTION;
    spiders_error EXCEPTION;
    lc_vErrorDescription varchar2(255) := 'default';
  
    lc_jReturn json;
    lc_nContinue number :=0; --default to 0 to dissalow continuing

    /* function specific variables */
  begin
  
    lc_jReturn := json();
    
    lc_jReturn := json_dyn.executeObject('select  models.interactivemodelid, models.model_name model_name, models.model_number model_number, models.description description,
                    models.version version, models.MODEL_SOURCE_DETAILS MODEL_SOURCE_DETAILS, models.THUMBNAIL_FILE_PATH THUMBNAIL_FILE_PATH, models.confidence_rating confidence_rating,
                    models.length length, models.width width, models.filepath filepath,
                    categories.short_description category, classes.short_description class_description,
                    models.modelcategoryid, models.modelclassid
                from  spd_t_3d_interactive_models models, spd_t_3d_model_categories categories, spd_t_3d_model_classes classes
                where models.modelcategoryid = categories.modelcategoryid AND
                      models.modelclassid = classes.modelclassid' );

    return lc_jReturn;
  
  exception   
    when spiders_error then
        execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' SPIDERS error ' || lc_vErrorDescription || ''')';
        
    when json_error then
        execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' JSON error'')';
        
    when others then
        execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' dml error: ' || sqlerrm || ''')';
        
  end; 
  
  function getAllClasses return json as
  /* template variables */
    lc_vFunctionName varchar2(255) := 'getAllClasses';
  
    json_error EXCEPTION;
    spiders_error EXCEPTION;
    lc_vErrorDescription varchar2(255) := 'default';
  
    lc_jReturn json;
    lc_jClasses json;
    lc_nContinue number :=0; --default to 0 to dissalow continuing

    /* function specific variables */
  begin
  
    lc_jReturn := json();
    
    lc_jReturn := json_dyn.executeObject('select modelclassid as value, modelclassid, description, image_filepath, short_description as display, short_description
                from  spd_t_3d_model_classes' );
      

    return lc_jReturn;
  
  exception   
    when spiders_error then
        execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' SPIDERS error ' || lc_vErrorDescription || ''')';
        
    when json_error then
        execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' JSON error'')';
        
    when others then
        execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' dml error: ' || sqlerrm || ''')';
        
  end; 
  
  function getAllCategories return json as
  /* template variables */
    lc_vFunctionName varchar2(255) := 'getAllCategories';
  
    json_error EXCEPTION;
    spiders_error EXCEPTION;
    lc_vErrorDescription varchar2(255) := 'default';
  
    lc_jReturn json;
    lc_nContinue number :=0; --default to 0 to dissalow continuing

    /* function specific variables */
  begin
  
    lc_jReturn := json();
    
    lc_jReturn := json_dyn.executeObject('select  modelcategoryid as value, modelcategoryid, description, image_filepath, short_description as display, short_description
                from  spd_t_3d_model_categories' );

    return lc_jReturn;
  
  exception   
    when spiders_error then
        execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' SPIDERS error ' || lc_vErrorDescription || ''')';
        
    when json_error then
        execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' JSON error'')';
        
    when others then
        execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' dml error: ' || sqlerrm || ''')';
        
  end;
  
  
  function getSessionMemberId(lc_nSessionId number) return number as
   /* template variables */
    lc_vFunctionName varchar2(255) := 'getSessionMemberId';
  
    json_error EXCEPTION;
    spiders_error EXCEPTION;
    lc_vErrorDescription varchar2(255) := 'default';
  
    lc_nReturn number;
    lc_nContinue number :=0; --default to 0 to dissalow continuing

    /* function specific variables */
  begin
    lc_nReturn :=0;
  
    --THERE WILL BE ISSUES WITH THIS IF THERE IS MORE THAN RECORD FOR THE SAME NFAID IN SPIDERS TABLE
    for rec in (select sessionmemberid from spd_t_3d_session_members where upper(username) = upper(v('APP_USER')) and upper(sessionid) = upper(lc_nSessionId)) loop
      lc_nReturn := rec.sessionmemberid;
    end loop;
  
    lc_nReturn := lc_nReturn;
    return lc_nReturn;
  
  exception   
    when spiders_error then
        execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' SPIDERS error ' || lc_vErrorDescription || ''')';
        return -1;
    when json_error then
        execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' JSON error'')';
        return -2;
    when others then
        execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' dml error: ' || sqlerrm || ''')';
        return -3;
  end; 
  
  function getAttributeValue(in_vAttribute varchar2, in_jJSON json) return varchar2 as 
    /* template variables */
    json_error EXCEPTION;
    spiders_error EXCEPTION;
    lc_vErrorDescription varchar2(255) := 'default';
    lc_vFunctionName varchar2(255) := 'getAttributeValue';
    lc_vReturn varchar2(32000) := '{"error":"default"}'; --defaul the return to e for error
    lc_nContinue number :=0; --default to 0 to dissalow continuing
    
    /* function specific variables */
    lc_jvValue json_value;
  begin
   
    if(in_jJSON.exist(in_vAttribute)) then    
      lc_jvValue := in_jJSON.get(in_vAttribute);
      lc_vReturn := getVal(lc_jvValue);
    else
      lc_vErrorDescription := in_vAttribute || ' attribute not found in json';
      raise spiders_error;
    end if;
    
    /* get name of object */
    return lc_vReturn;
  
  exception     
    when spiders_error then
        execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' SPIDERS error ' || lc_vErrorDescription || ''')';
        return '{"error":"' || lc_vErrorDescription || '"}';
    when json_error then
        execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' JSON error'')';
        return '{"error":"json"}';
    when others then
        execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' dml error: ' || sqlerrm || ''')';
        return '{"error":"' || sqlerrm || '"}';
  end;

  function getNextSessionMemberSequence return number as 
    /* template variables */
    lc_vFunctionName varchar2(255) := 'getNextFacilitiesId';
    json_error EXCEPTION;
    spiders_error EXCEPTION;
    lc_vErrorDescription varchar2(255) := 'default';
    lc_nReturn number;
    lc_nContinue number :=0; --default to 0 to dissalow continuing
    
    /* function specific variables */
  begin
  
    select SPD_S_3D_SESSION_MEMBERS.nextval into lc_nReturn from dual;
  
    return lc_nReturn;
  
  exception     
    when spiders_error then
        execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' SPIDERS error ' || lc_vErrorDescription || ''')';
        return -1;
    when json_error then
        execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' JSON error'')';
        return -2;
    when others then
        execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' dml error: ' || sqlerrm || ''')';
        return -3;
  end;
  
  function f_append_whiteboard(in_cJSON clob) return json AS
  
    lc_vFunctionName varchar2(255) := 'f_append_whiteboard';
    json_error EXCEPTION;
    spiders_error EXCEPTION;
    
    lc_vSessionKey varchar2(255);
    lc_vBase64_image varchar2(32000);
    lc_nWhiteboardId number;
    lc_nSessionId number;
    lc_nSessionMemberId number;
    lc_nSceneId number;
    lc_jResults json;
    lc_jMessages json;
    lc_jReturn json;
    lc_jParameterJson json;
    lc_jlData json_list;
    lc_jData json;
    lc_nContinue number;
    lc_vModule varchar(255);
    lc_vDMLResult long;
    lc_vErrorDescription varchar2(255) := 'default';
    lc_jlDML json_list;
    lc_vSql long;
    lc_jDescriptionPath json;
    lc_vUpdatedTimestamp varchar2(225);
    lc_jSceneInventory json;
    lc_jWhiteboards json;
    
    lc_vMarkings varchar2(32000);
    
    lc_jInteractiveModels json;
    lc_jClasses json;
    lc_jCategories json;
    
    lc_vStatus varchar2(64);
    
    lc_c1 clob;
    lc_c2 clob;
  
    
  BEGIN
   --htp.p(in_cJSON);
   --insert into debug (functionname, misc) values ('TESTING WHITEBOARDS4', in_cJson);
    lc_jlDML := json_list();
    
    lc_jParameterJson := json(in_cJSON);
    lc_vModule := getAttributeValue('module', lc_jParameterJson);
    --execute immediate 'insert into debug (message) values (' || lc_vModule || ')';
    lc_nContinue := f_test_for_error(lc_vModule);
    
  
    --if lc_nContinue = 1 then
    lc_jlData := getJsonList('data', lc_jParameterJson);
      
    --else
    --  lc_vErrorDescription := 'Module attribute not found';
    --  raise spiders_error;
    --end if;
    
    --insert into debug (functionname) values ('TESTING1 WHITEBOARDS4');
    
    if lc_nContinue = 1 then
      lc_jData := getJsonListItem('1', lc_jlData);
      --htp.p(lc_nContinue);
    else
      lc_vErrorDescription := 'Module attribute not found';
      raise spiders_error;
    end if;
     --insert into debug (functionname) values ('TESTING2 WHITEBOARDS4');
    
    if lc_nContinue = 1 then
      lc_nWhiteboardId := getAttributeValue('WHITEBOARDID', lc_jData);
      --htp.p(lc_vSessionKey);
      lc_nContinue := f_test_for_error(lc_nWhiteboardId);
    else
      lc_vErrorDescription := 'WHITEBOARDID not found';
      raise spiders_error;
    end if;
     --insert into debug (functionname) values ('TESTING3 WHITEBOARDS4');
    
    if lc_nContinue = 1 then
      lc_vBase64_image := getAttributeValue('BASE64_IMAGE', lc_jData);
      --htp.p(lc_vBase64_image);
      lc_nContinue := f_test_for_error(lc_vBase64_image);
    else
      lc_vErrorDescription := 'BASE64_IMAGE not found';
      raise spiders_error;
    end if;
     --insert into debug (functionname) values ('TESTING4 WHITEBOARDS4');
    
    if lc_nContinue = 1 then
      lc_vMarkings := getAttributeValue('MARKINGS', lc_jData);
      --htp.p(lc_vBase64_image);
      lc_nContinue := f_test_for_error(lc_vMarkings);
    else
      lc_vErrorDescription := 'MARKINGS not found';
      raise spiders_error;
    end if;
     --insert into debug (functionname) values ('TESTING5 WHITEBOARDS4');
    
    if lc_nContinue = 1 then
      lc_vStatus := getAttributeValue('STATUS', lc_jData);
      --htp.p(lc_vBase64_image);
      lc_nContinue := f_test_for_error(lc_vStatus);
    else
      lc_vErrorDescription := 'STATUS not found';
      raise spiders_error;
    end if;
     --insert into debug (functionname) values ('TESTING6 WHITEBOARDS4');
    
    
    --
    --execute immediate 'insert into debug (message) values ('' step one '')';
    
    
    if lc_nContinue = 1 then
      --FIRST SELECT THE CURRENT WHITEBOARD & MARKINGS - LOCK THE ROW FOR THESE ATTRIBUTES
      SELECT BASE64_IMAGE, MARKINGS into lc_c1, lc_c2 FROM SPD_T_3D_WHITEBOARDS  WHERE WHITEBOARDID=lc_nWhiteboardId FOR UPDATE OF BASE64_IMAGE, MARKINGS;
      
      --SECOND APPEND THE NEXT CHUNK
      if (lc_vBase64_image is not null) then
        if (lc_c1 is null) then
          lc_c1:=to_clob(lc_vBase64_image);
        else
          dbms_lob.append(lc_c1,to_clob(lc_vBase64_image));
        end if;
      end if;
      
      if (lc_vMarkings is not null) then
        if (lc_c2 is null) then
          lc_c2:=to_clob(lc_vMarkings);
        else
          dbms_lob.append(lc_c2,to_clob(lc_vMarkings));
        end if;
      end if;
      --THIRD - UPDATE THE MARKINGS AND WHITEBOARD
      UPDATE SPD_T_3D_WHITEBOARDS SET STATUS = lc_vStatus, UPDATED_TIMESTAMP=SYSDATE, MARKINGS=lc_c2, BASE64_IMAGE=lc_c1  WHERE WHITEBOARDID=lc_nWhiteboardId; 
      --htp.p(lc_vSql);
      --lc_jlDML.append(lc_vSql);
    else
      lc_vErrorDescription := 'Soemthing went wrong during the UPDATE - appending to BASE64_IMAGE';
      raise spiders_error;
    end if;
    
    --insert into debug (functionname) values ('TESTING7 WHITEBOARDS4');
    /*
    if lc_nContinue = 1 then
      lc_jResults := json();
      lc_jResults.put('results',lc_jlDML);
      lc_jMessages := json();
      lc_vDMLResult := BB_F_DML(lc_jResults, lc_jMessages);
    else
      lc_vErrorDescription := 'something went wrong with the sql statement creation';
      raise spiders_error;
    end if;
    */
    
    lc_jReturn := json();
    lc_jReturn.put('results', 'success');

    
    RETURN lc_jReturn;
    
  exception     
    when spiders_error then
        execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' SPIDERS error ' || lc_vErrorDescription || ''')';
        return json('{"error":"' || lc_vFunctionName || ' SPIDERS error ' || lc_vErrorDescription || '"}');
    when json_error then
        execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' JSON error'')';
        return json('{"error":"' || lc_vFunctionName || ' JSON error ' || lc_vErrorDescription || '"}');
    when others then
        execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' dml error: ' || sqlerrm || ''')';
       return json('{"error":"' || lc_vFunctionName || ' dml error ' || sqlerrm || '"}');
  END f_append_whiteboard;
  
  
  --function f_add_delivery_order_fac(in_cJSON clob) return json AS
  function f_launch_session(in_cJSON clob) return json AS
    lc_vFunctionName varchar2(255) := 'f_launch_session'; -- 'f_add_delivery_order_fac';
    json_error EXCEPTION;
    spiders_error EXCEPTION;
    
    lc_vSessionKey varchar2(255);
    lc_nSessionId number;
    lc_nSessionMemberId number;
    lc_nSceneId number;
    lc_jResults json;
    lc_jMessages json;
    lc_jReturn json;
    lc_jParameterJson json;
    lc_jlData json_list;
    lc_jData json;
    lc_nContinue number;
    lc_vModule varchar(255);
    lc_vDMLResult long;
    lc_vErrorDescription varchar2(255) := 'default';
    lc_jlDML json_list;
    lc_vSql long;
    lc_jSessionInfo json;
    lc_jDescriptionPath json;
    lc_vUpdatedTimestamp varchar2(225);
    lc_jSceneInventory json;
    lc_jWhiteboards json;
    lc_jInteractiveModels json;
    lc_jCustomObjectTypes json;
    lc_jCustomObjectInstances json;
    lc_jClasses json;
    lc_jCategories json;
    --lc_vTimestamp varchar2(255);
    lc_vTimestampFull varchar2(255);
    
  BEGIN
  
    lc_jlDML := json_list();
    
    lc_jParameterJson := json(in_cJSON);
    lc_vModule := getAttributeValue('module', lc_jParameterJson);
    --execute immediate 'insert into debug (message) values (' || lc_vModule || ')';
    lc_nContinue := f_test_for_error(lc_vModule);
    
  
    --if lc_nContinue = 1 then
    lc_jlData := getJsonList('data', lc_jParameterJson);
      
    --else
    --  lc_vErrorDescription := 'Module attribute not found';
    --  raise spiders_error;
    --end if;
    
    if lc_nContinue = 1 then
      lc_jData := getJsonListItem('1', lc_jlData);
      --htp.p(lc_nContinue);
    else
      lc_vErrorDescription := 'Module attribute not found';
      raise spiders_error;
    end if;
    
    if lc_nContinue = 1 then
      lc_vSessionKey := getAttributeValue('SESSION_KEY', lc_jData);
      --htp.p(lc_vSessionKey);
      lc_nContinue := f_test_for_error(lc_vSessionKey);
    else
      lc_vErrorDescription := 'session key not found';
      raise spiders_error;
    end if;
    
    --execute immediate 'insert into debug (message) values ('' step one '')';
    
    if lc_nContinue = 1 then
      lc_nSessionId := getSessionId(lc_vSessionKey);
      lc_nSceneId := getSceneId(lc_nSessionId);
      lc_nContinue := f_test_for_error(lc_nSessionId);
    else
      lc_vErrorDescription := 'unable to select session id';
      raise spiders_error;
    end if;
    
    if lc_nContinue = 1 then 
      lc_nSessionMemberId := getSessionMemberId(lc_nSessionId);
      lc_nContinue := f_test_for_error(lc_nSessionMemberId);
    else
      lc_vErrorDescription := 'unable to select session member id';
      raise spiders_error;
    end if;
    
    if lc_nContinue = 1 then
      if lc_nSessionMemberId = 0 then
        lc_nSessionMemberId := getNextSessionMemberSequence;
        lc_vSql := makeInsertSessionMembersSql(lc_nSessionMemberId);  
        lc_jlDML.append(lc_vSql);
      else
        lc_nContinue := 1;
      end if;
    else
      lc_vErrorDescription := 'something went wrong with lc_nSessionMemberId';
      raise spiders_error;
    end if;
    
    if lc_nContinue = 1 then
      lc_vSql := 'INSERT INTO SPD_T_3D_SESSION_HISTORY (SESSIONMEMBERID,SESSIONID) values (' || lc_nSessionMemberId || ',' || lc_nSessionId || ')';
      lc_jlDML.append(lc_vSql);
    else
      lc_vErrorDescription := 'Soemthing went wrong fetching session member id';
      raise spiders_error;
    end if;
    
    if lc_nContinue = 1 then
      lc_jResults := json();
      lc_jResults.put('results',lc_jlDML);
      lc_jMessages := json();
      lc_vDMLResult := BB_F_DML(lc_jResults, lc_jMessages);
    else
      lc_vErrorDescription := 'something went wrong with the sql statement creation';
      raise spiders_error;
    end if;
    
    -- AFTER ALL INSERTS COME THE FETCHES --
    --lc_jSessionInfo json;
    
    if lc_nContinue = 1 then
      lc_jSessionInfo := getSessionInfo(lc_nSessionId);
    else
      lc_vErrorDescription := 'something went wrong with BB_F_DML';
      raise spiders_error;
    end if;
    
    if lc_nContinue = 1 then
      lc_jDescriptionPath := getSceneDescriptionPath(lc_nSceneId);
    else
      lc_vErrorDescription := 'something went wrong with BB_F_DML';
      raise spiders_error;
    end if;
    
    if lc_nContinue = 1 then
      lc_vUpdatedTimestamp := getUpdatedTimestamp(lc_nSessionId);
    else
      lc_vErrorDescription := 'something went wrong with getSceneDescriptionPath';
      raise spiders_error;
    end if;
    
    if lc_nContinue = 1 then
      lc_jSceneInventory := getSceneInventory(lc_nSessionId);
      --lc_jSceneInventory := json();
    else
      lc_vErrorDescription := 'something went wrong with getUpdatedTimestamp';
      raise spiders_error;
    end if;
    /**
    if lc_nContinue = 1 then
      lc_jWhiteboards := getWhiteboards(lc_nSessionId);
    else
      lc_vErrorDescription := 'something went wrong with getSceneInventory';
      raise spiders_error;
    end if;
    */
    if lc_nContinue = 1 then
      lc_jInteractiveModels := getInteractiveModels;
    else
      lc_vErrorDescription := 'something went wrong with getWhiteboards';
      raise spiders_error;
    end if;
    
    if lc_nContinue = 1 then
      lc_jCustomObjectTypes := getCustomObjectTypes(lc_nSessionId);
    else
      lc_vErrorDescription := 'something went wrong with getCustomObjectTypes';
      raise spiders_error;
    end if;
    
    if lc_nContinue = 1 then
      lc_jCustomObjectInstances := getCustomObjectInstances(lc_nSessionId);
    else
      lc_vErrorDescription := 'something went wrong with getCustomObjectInstances';
      raise spiders_error;
    end if;
    
    if lc_nContinue = 1 then
      lc_jClasses := getAllClasses;
    else
      lc_vErrorDescription := 'something went wrong with getInteractiveModels';
      raise spiders_error;
    end if;
    
    if lc_nContinue = 1 then
      lc_jCategories := getAllCategories;
    else
      lc_vErrorDescription := 'something went wrong with getAllClasses';
      raise spiders_error;
    end if;
    
    select to_char(SYSDATE, 'dd-mm-yyyy HH:MI:SS AM') into lc_vTimestampFull from dual;
    
    lc_jReturn := json();
    lc_jReturn.put('sessionId', lc_nSessionId);
    lc_jReturn.put('sessionInfo', lc_jSessionInfo);
    lc_jReturn.put('descriptionFilepath', lc_jDescriptionPath);
    --lc_jReturn.put('updated_timestamp', lc_vUpdatedTimestamp);
    lc_jReturn.put('sceneInventory', lc_jSceneInventory);
    --lc_jReturn.put('whiteboards', lc_jWhiteboards);
    lc_jReturn.put('interactiveModels', lc_jInteractiveModels);
    lc_jReturn.put('customObjectTypes', lc_jCustomObjectTypes);
    lc_jReturn.put('customObjectInstances', lc_jCustomObjectInstances);
    lc_jReturn.put('classes', lc_jClasses);
    lc_jReturn.put('categories', lc_jCategories);
    lc_jReturn.put('timestampFull', lc_vTimestampFull);
    lc_jReturn.put('timestamp', SYSDATE);
    RETURN lc_jReturn;
    
  exception     
    when spiders_error then
        execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' SPIDERS error ' || lc_vErrorDescription || ''')';
        return json('{"error":"' || lc_vFunctionName || ' SPIDERS error ' || lc_vErrorDescription || '"}');
    when json_error then
        execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' JSON error'')';
        return json('{"error":"' || lc_vFunctionName || ' JSON error ' || lc_vErrorDescription || '"}');
    when others then
        execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' dml error: ' || sqlerrm || ''')';
       return json('{"error":"' || lc_vFunctionName || ' dml error ' || sqlerrm || '"}');
  END f_launch_session;--f_add_delivery_order_fac;
  
  
  function f_get_markings(in_cJSON clob) return json AS
    lc_vFunctionName varchar2(255) := 'syncSessions';
    json_error EXCEPTION;
    spiders_error EXCEPTION;
    lc_vErrorDescription varchar2(255) := 'default';
    
    lc_jReturn json;
    lc_jlDML json_list;
    lc_JParameterJson json;
    lc_jlData json_list;
    lc_jData json;
    lc_vWhiteboardId varchar2(225);
    lc_nWhiteboardId number;
    lc_vMarkings clob;
    
    lc_count number;
    lc_return clob;
  begin
    --lc_jReturn := json();
    
    lc_jlDML := json_list();
    
    lc_jParameterJson := json(in_cJSON);
    
    lc_jlData := getJsonList('data', lc_jParameterJson);
    
    lc_jData := getJsonListItem('1', lc_jlData);
    
    lc_vWhiteboardId := getAttributeValue('WHITEBOARDID', lc_jData);
    
    lc_nWhiteboardId := TO_NUMBER(lc_vWhiteboardId);
    
    
    --select MARKINGS into lc_vMarkings from spd_t_3d_whiteboards where whiteboardid = lc_nWhiteboardId; 
    
    lc_return := '{markings:''none''}';

    select count(whiteboardid) into lc_count from spd_t_3d_whiteboards where whiteboardid = lc_nWhiteboardId; --81000000000541; 
    
    if (lc_count > 0) then
      select MARKINGS into lc_vMarkings from spd_t_3d_whiteboards where whiteboardid = lc_nWhiteboardId; --81000000000541; 
      if (lc_vMarkings is not null) then
        lc_return := '{markings:'''|| lc_vMarkings ||'''}';
      end if;
    end if;
    
    lc_jReturn := json(lc_return);
    return lc_jReturn;
    --htp.p(lc_return);
  end f_get_markings;
  
  
  function syncSessionsCustObj(in_cJSON clob) return json AS
    lc_vFunctionName varchar2(255) := 'syncSessionsCustObj';
    json_error EXCEPTION;
    spiders_error EXCEPTION;
    lc_vErrorDescription varchar2(255) := 'default';
    
    lc_jReturn json;
    lc_jlDML json_list;
    lc_JParameterJson json;
    lc_jlData json_list;
    lc_jData json;
    lc_vSessionId varchar2(225);
    lc_nSessionId number;
    lc_vTimeStamp varchar2(225);
    lc_jPullSessionCustObjUpdate json;
  BEGIN
    lc_jReturn := json();
    
    lc_jlDML := json_list();
    
    lc_jParameterJson := json(in_cJSON);
    
    lc_jlData := getJsonList('data', lc_jParameterJson);
    
    lc_jData := getJsonListItem('1', lc_jlData);
    
    lc_vSessionId := getAttributeValue('SESSIONID', lc_jData);
    
    lc_nSessionId := TO_NUMBER(lc_vSessionId);
    
    lc_vTimeStamp := getAttributeValue('CLIENT_PROVIDED_TIMESTAMP', lc_jData);
    
    lc_jPullSessionCustObjUpdate := pullSessionCustObjUpdate(lc_nSessionId, lc_vTimeStamp);
    
    lc_jReturn.put('update', lc_jPullSessionCustObjUpdate);
    
    return lc_jReturn;
    
  exception     
    when spiders_error then
        execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' SPIDERS error ' || lc_vErrorDescription || ''')';
        return json('{"error":"' || lc_vFunctionName || ' SPIDERS error ' || lc_vErrorDescription || '"}');
    when json_error then
        execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' JSON error'')';
        return json('{"error":"' || lc_vFunctionName || ' JSON error ' || lc_vErrorDescription || '"}');
    when others then
        execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' dml error: ' || sqlerrm || ''')';
       return json('{"error":"' || lc_vFunctionName || ' dml error ' || sqlerrm || '"}');
    
  END;
  
  
  function syncSessions(in_cJSON clob) return json AS
    lc_vFunctionName varchar2(255) := 'syncSessions';
    json_error EXCEPTION;
    spiders_error EXCEPTION;
    lc_vErrorDescription varchar2(255) := 'default';
    
    lc_jReturn json;
    lc_jlDML json_list;
    lc_JParameterJson json;
    lc_jlData json_list;
    lc_jData json;
    lc_vSessionId varchar2(225);
    lc_nSessionId number;
    lc_vTimeStamp varchar2(225);
    lc_jPullSessionUpdate json;
  BEGIN
    lc_jReturn := json();
    
    lc_jlDML := json_list();
    
    lc_jParameterJson := json(in_cJSON);
    
    lc_jlData := getJsonList('data', lc_jParameterJson);
    
    lc_jData := getJsonListItem('1', lc_jlData);
    
    lc_vSessionId := getAttributeValue('SESSIONID', lc_jData);
    
    lc_nSessionId := TO_NUMBER(lc_vSessionId);
    
    lc_vTimeStamp := getAttributeValue('CLIENT_PROVIDED_TIMESTAMP', lc_jData);
    
    lc_jPullSessionUpdate := pullSessionUpdate(lc_nSessionId, lc_vTimeStamp);
    
    lc_jReturn.put('update', lc_jPullSessionUpdate);
    
    return lc_jReturn;
    
  exception     
    when spiders_error then
        execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' SPIDERS error ' || lc_vErrorDescription || ''')';
        return json('{"error":"' || lc_vFunctionName || ' SPIDERS error ' || lc_vErrorDescription || '"}');
    when json_error then
        execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' JSON error'')';
        return json('{"error":"' || lc_vFunctionName || ' JSON error ' || lc_vErrorDescription || '"}');
    when others then
        execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' dml error: ' || sqlerrm || ''')';
       return json('{"error":"' || lc_vFunctionName || ' dml error ' || sqlerrm || '"}');
    
  END;
  
function pushUpdates(in_cJSON clob) return json AS
    lc_vFunctionName varchar2(255) := 'pushUpdates';
    lc_vErrorDescription varchar2(255) := 'default';
    
    lc_jReturn json;
    lc_jlDML json_list;
    lc_JParameterJson json;
    lc_jlData json_list;
    lc_jData json;
    lc_vSessionId varchar2(225);
    lc_nSessionId number;
    lc_vTimeStamp varchar2(225);
    lc_jPullSessionUpdate json;
    
    /* template variables */
  json_error EXCEPTION;
  spiders_error EXCEPTION;
  lc_vRest varchar2(255) := 'CREATEMODELCUSTOM';
  lc_vReturn varchar2(32000) := '{"error":"default"}'; --defaul the return to e for error
  lc_nContinue number :=0; --default to 0 to dissalow continuing
  
  /* function specific variables */
  lc_vTarget varchar2(255);
  lc_vModule varchar2(255);
  lc_vTableName varchar2(255);
  lc_jJson json;
  lc_jResults json;
  lc_jMessages json;
  lc_jlMessages json_list;
  lc_vDMLResult varchar2(1);
  lc_vIsAuthorized varchar2(1);
  lc_vDebug varchar2(2000);
  lc_vSql long;
  lc_jIndividual  json;
  lc_nRecordCount number;
  lc_vLocalID varchar2(2000);
  lc_nLocalIDCount number :=0;
  lc_nNewID number;
  lc_jvData json_value;
  lc_jNewID json;
  lc_jlNewID json_list;

  /*EDITED 20170913 MR ADDED: [SPD3D][45] Sync*/
  /*EDITED 20170914 TC Updated for multiple DML: [SPD3D][45] Sync*/
  /*EDITED 20170915 MR Updated for returning ID + localid: [SPD3D][45] Sync*/

begin

  /*!! I LEFT OFF just at the end of the createModel function, have not tested*/
  lc_jParameterJson := json(in_cJson);
  
  lc_vModule := getAttributeValue('module', lc_jParameterJson);

  lc_nContinue := f_test_for_error(lc_vModule);
 
  if lc_nContinue = 1 then
    lc_vTarget := getAttributeValue('target', lc_jParameterJson);
    lc_nContinue := f_test_for_error(lc_vTarget);
  else
    lc_vErrorDescription := 'Module attribute not found';
    raise spiders_error; 
  end if;

/*
  if lc_nContinue = 1 then
    lc_vIsAuthorized := BB_F_AUTHORIZATION('BB_REST.create_',lc_vModule);
  else
    lc_vErrorDescription := 'target attribute not found';
    raise spiders_error; 
  end if; 
*/


 
  lc_vIsAuthorized := 'y'; --!!REMOVE AFTER TESTING
  
  if lc_vIsAuthorized = 'y' then
    lc_vTableName := getTargetTableName(lc_vRest,lc_vModule,lc_vTarget);
    lc_nContinue := f_test_for_error(lc_vTableName);
  else
    lc_vErrorDescription := 'NOT AUTHORIZED';
    raise spiders_error; 
  end if;  

  if lc_nContinue = 1 then
    lc_nRecordCount := bb_rest.f_getJsonDataRecordCount(lc_jParameterJson);
    lc_nContinue := f_test_for_error(lc_vSql);
  else
    lc_vErrorDescription := 'Table map record not found';
    raise spiders_error; 
  end if; 
  
  lc_jlDML := json_list();
  lc_jlNewID := json_list();
  
  for i in 1..lc_nRecordCount loop
  
    lc_jNewID := json();

    if lc_nContinue = 1 then
      lc_jIndividual := bb_rest.f_getIndividualDataJson(lc_jParameterJson,i,lc_vModule,lc_vTarget);
      lc_nContinue := 1;
    else
      lc_vErrorDescription := 'Table map record not found';
      raise spiders_error; 
    end if; 
    
    lc_jvData := lc_jIndividual.get('data');
    lc_jlData := json_list(lc_jvData);
    lc_jData := json(lc_jlData.get(1));
    
    if lc_nContinue = 1 then
      lc_vLocalID := getAttributeValue('LOCALID', lc_jData);
      lc_jNewID.put('LOCALID',lc_vLocalID);
      lc_nContinue := f_test_for_error(lc_vTarget);
    else
      lc_vErrorDescription := 'Module attribute not found';
      raise spiders_error; 
    end if;
    
    select count(*) into lc_nLocalIDCount from spd_t_3d_model_logs where localid = lc_vLocalID; 
    
    --Put this inline, need to make a separate function for this at some point...
    if lc_nLocalIDCount > 0 then
      lc_vSql := sql_statement_functions.f_update(lc_jIndividual, lc_vTableName);
      lc_nContinue := f_test_for_error(lc_vSql);
    else
      lc_vSql := sql_statement_functions.f_insert(lc_jIndividual, lc_vTableName, lc_nNewID);
      lc_jNewID.put('NEWID',lc_nNewID);
      lc_jlNewID.append(lc_jNewID.to_json_value);
      lc_nContinue := f_test_for_error(lc_vSql);
    end if; 
    
    if lc_nContinue = 1 then
      lc_jlDML.append(lc_vSql);
    else
      lc_vErrorDescription := 'making id and local id json failed';
      raise spiders_error; 
    end if;
  
  end loop;
   

  if lc_nContinue = 1 then    
    lc_jResults := json();
    lc_jResults.put('results',lc_jlDML);
    
    lc_jMessages := json();
    lc_vDMLResult := BB_F_DML(lc_jResults, lc_jMessages);
    lc_jMessages.put('id',lc_jlNewID);
    --lc_jMessages.htp;
    lc_vReturn := 'SUCCESS';
  else
    lc_vErrorDescription := 'SQL Statement failed';
    raise spiders_error;
  end if; 
    
    lc_jReturn := lc_jMessages;
    return lc_jReturn;
    
  exception     
    when spiders_error then
        execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' SPIDERS error ' || lc_vErrorDescription || ''')';
        return json('{"error":"' || lc_vFunctionName || ' SPIDERS error ' || lc_vErrorDescription || '"}');
    when json_error then
        execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' JSON error'')';
        return json('{"error":"' || lc_vFunctionName || ' JSON error ' || lc_vErrorDescription || '"}');
    when others then
        execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' dml error: ' || sqlerrm || ''')';
       return json('{"error":"' || lc_vFunctionName || ' dml error ' || sqlerrm || '"}');
    
  END;
  
function pushUpdatesCustObj(in_cJSON clob) return json AS
    lc_vFunctionName varchar2(255) := 'pushUpdatesCustObj';
    lc_vErrorDescription varchar2(255) := 'default';
    
    lc_jReturn json;
    lc_jlDML json_list;
    lc_JParameterJson json;
    lc_jlData json_list;
    lc_jData json;
    lc_vSessionId varchar2(225);
    lc_nSessionId number;
    lc_vTimeStamp varchar2(225);
    lc_jPullSessionUpdate json;
    
    /* template variables */
  json_error EXCEPTION;
  spiders_error EXCEPTION;
  lc_vRest varchar2(255) := 'CREATEMODELCUSTOM';
  lc_vReturn varchar2(32000) := '{"error":"default"}'; --defaul the return to e for error
  lc_nContinue number :=0; --default to 0 to dissalow continuing
  
  /* function specific variables */
  lc_vTarget varchar2(255);
  lc_vModule varchar2(255);
  lc_vTableName varchar2(255);
  lc_jJson json;
  lc_jResults json;
  lc_jMessages json;
  lc_jlMessages json_list;
  lc_vDMLResult varchar2(1);
  lc_vIsAuthorized varchar2(1);
  lc_vDebug varchar2(2000);
  lc_vSql long;
  lc_jIndividual  json;
  lc_nRecordCount number;
  lc_vLocalID varchar2(2000);
  lc_nLocalIDCount number :=0;
  lc_nNewID number;
  lc_jvData json_value;
  lc_jNewID json;
  lc_jlNewID json_list;

  /*EDITED 20170913 MR ADDED: [SPD3D][45] Sync*/
  /*EDITED 20170914 TC Updated for multiple DML: [SPD3D][45] Sync*/
  /*EDITED 20170915 MR Updated for returning ID + localid: [SPD3D][45] Sync*/

begin

  /*!! I LEFT OFF just at the end of the createModel function, have not tested*/
  lc_jParameterJson := json(in_cJson);
  
  lc_vModule := getAttributeValue('module', lc_jParameterJson);

  lc_nContinue := f_test_for_error(lc_vModule);
 
  if lc_nContinue = 1 then
    lc_vTarget := getAttributeValue('target', lc_jParameterJson);
    lc_nContinue := f_test_for_error(lc_vTarget);
  else
    lc_vErrorDescription := 'Module attribute not found';
    raise spiders_error; 
  end if;

/*
  if lc_nContinue = 1 then
    lc_vIsAuthorized := BB_F_AUTHORIZATION('BB_REST.create_',lc_vModule);
  else
    lc_vErrorDescription := 'target attribute not found';
    raise spiders_error; 
  end if; 
*/


 
  lc_vIsAuthorized := 'y'; --!!REMOVE AFTER TESTING
  
  if lc_vIsAuthorized = 'y' then
    lc_vTableName := getTargetTableName(lc_vRest,lc_vModule,lc_vTarget);
    lc_nContinue := f_test_for_error(lc_vTableName);
  else
    lc_vErrorDescription := 'NOT AUTHORIZED';
    raise spiders_error; 
  end if;  

  if lc_nContinue = 1 then
    lc_nRecordCount := bb_rest.f_getJsonDataRecordCount(lc_jParameterJson);
    lc_nContinue := f_test_for_error(lc_vSql);
  else
    lc_vErrorDescription := 'Table map record not found';
    raise spiders_error; 
  end if; 
  
  lc_jlDML := json_list();
  lc_jlNewID := json_list();
  
  for i in 1..lc_nRecordCount loop
  
    lc_jNewID := json();

    if lc_nContinue = 1 then
      lc_jIndividual := bb_rest.f_getIndividualDataJson(lc_jParameterJson,i,lc_vModule,lc_vTarget);
      lc_nContinue := 1;
    else
      lc_vErrorDescription := 'Table map record not found';
      raise spiders_error; 
    end if; 
    
    lc_jvData := lc_jIndividual.get('data');
    lc_jlData := json_list(lc_jvData);
    lc_jData := json(lc_jlData.get(1));
    
    if lc_nContinue = 1 then
      lc_vLocalID := getAttributeValue('LOCALID', lc_jData);
      lc_jNewID.put('LOCALID',lc_vLocalID);
      lc_nContinue := f_test_for_error(lc_vTarget);
    else
      lc_vErrorDescription := 'Module attribute not found';
      raise spiders_error; 
    end if;
    
    select count(*) into lc_nLocalIDCount from spd_t_3d_custom_object_logs where localid = lc_vLocalID; 
    
    --Put this inline, need to make a separate function for this at some point...
    if lc_nLocalIDCount > 0 then
      lc_vSql := sql_statement_functions.f_update(lc_jIndividual, lc_vTableName);
      lc_nContinue := f_test_for_error(lc_vSql);
    else
      lc_vSql := sql_statement_functions.f_insert(lc_jIndividual, lc_vTableName, lc_nNewID);
      lc_jNewID.put('NEWID',lc_nNewID);
      lc_jlNewID.append(lc_jNewID.to_json_value);
      lc_nContinue := f_test_for_error(lc_vSql);
    end if; 
    
    if lc_nContinue = 1 then
      lc_jlDML.append(lc_vSql);
    else
      lc_vErrorDescription := 'making id and local id json failed';
      raise spiders_error; 
    end if;
  
  end loop;
   

  if lc_nContinue = 1 then    
    lc_jResults := json();
    lc_jResults.put('results',lc_jlDML);
    
    lc_jMessages := json();
    lc_vDMLResult := BB_F_DML(lc_jResults, lc_jMessages);
    lc_jMessages.put('id',lc_jlNewID);
    --lc_jMessages.htp;
    lc_vReturn := 'SUCCESS';
  else
    lc_vErrorDescription := 'SQL Statement failed';
    raise spiders_error;
  end if; 
    
    lc_jReturn := lc_jMessages;
    return lc_jReturn;
    
  exception     
    when spiders_error then
        execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' SPIDERS error ' || lc_vErrorDescription || ''')';
        return json('{"error":"' || lc_vFunctionName || ' SPIDERS error ' || lc_vErrorDescription || '"}');
    when json_error then
        execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' JSON error'')';
        return json('{"error":"' || lc_vFunctionName || ' JSON error ' || lc_vErrorDescription || '"}');
    when others then
        execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' dml error: ' || sqlerrm || ''')';
       return json('{"error":"' || lc_vFunctionName || ' dml error ' || sqlerrm || '"}');
    
  END;
END PKG_SPIDERS_3D_SESSIONS;