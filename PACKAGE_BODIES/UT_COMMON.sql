--------------------------------------------------------
--  DDL for Package Body UT_COMMON
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE PACKAGE BODY "SPIDERS_DATA"."UT_COMMON" AS

  --set serveroutput on size unlimited;

--create or replace procedure TDD_REPORT_LIBRARY as
  pass_count number := 0;
  fail_count number := 0;
  total_count number := 0;
  str varchar2(2000);


  procedure p (v varchar2) as
  begin
    dbms_output.put_line(v);
    htp.p('output: ' || v);
  end;
  
  procedure insertResults(in_vSql varchar2) as
      lc_jlDML json_list;
      lc_jResults json;
      lc_jMessages json;
      lc_vDMLResult varchar2(1);
  begin
      lc_jlDML := json_list();
      lc_jlDML.append(in_vSql);
      
      lc_jResults := json();
      lc_jResults.put('results',lc_jlDML);
      
      lc_jMessages := json();
      lc_vDMLResult := BB_F_DML(lc_jResults, lc_jMessages);
  end;

  procedure pass(unitTestID number, packageName varchar2, procedureName varchar2, testName varchar2) as
  lc_vSql varchar2(500);
  begin
    pass_count := pass_count + 1;
    total_count := total_count + 1;
    htp.p('OK: '|| packageName || ' - ' || testName);
    --lc_vSql := 'INSERT INTO SPD_T_UNIT_TEST_RESULTS (UNITTESTRESULTID, UNITTESTID, PACKAGE_NAME, PACKAGE_PROCEDURE, TEST_NAME, PASS, FAIL) values (' || '''' || '''' || ',' || unitTestID || ',''' || packageName || ''',''' || procedureName || ''',''' || testName || ''',' || '1' ||  ',' || '0' || ');';
    --insertResults(lc_vSql);
    --htp.p(lc_vsql);
    --execute immediate lc_vsql;
    INSERT INTO SPD_T_UNIT_TEST_RESULTS  (UNITTESTRESULTID, UNITTESTID, PACKAGE_NAME, PACKAGE_PROCEDURE, TEST_NAME, PASS, FAIL) values ('', unitTestID, packageName, procedureName, testName, 1, 0);
  end;

  procedure fail(unitTestID number, packageName varchar2, procedureName varchar2, testName varchar2) as
    lc_vUsername varchar2(2000) := 'UT_USER@UNITTEST.COM';
    lc_vSql varchar(500);
  begin
    fail_count := fail_count + 1;
    total_count := total_count + 1;
    htp.p('FAILED: '||testName);
    htp.p(sqlerrm);
    --lc_vsql := 'INSERT INTO SPD_T_UNIT_TEST_RESULTS (UNITTESTRESULTID, UNITTESTID, PACKAGE_NAME, PACKAGE_PROCEDURE, TEST_NAME, PASS, FAIL) values (' || '''' || '''' || ',' || unitTestID || ',''' || packageName || ''',''' || procedureName || ''',''' || testName || ''',' || '1' ||  ',' || '0' || ');';
    --insertResults(lc_vSql);
    INSERT INTO SPD_T_UNIT_TEST_RESULTS  (UNITTESTRESULTID, UNITTESTID, PACKAGE_NAME, PACKAGE_PROCEDURE, TEST_NAME, PASS, FAIL) values ('', unitTestID, packageName, procedureName, testName, 0, 1);
  end;

  procedure assertTrue(b boolean) as
  begin
    if(not b) then raise_application_error(-20111, 'Test error'); end if;
  end;

  procedure assertFalse(b boolean) as
  begin
    if(b) then raise_application_error(-20111, 'Test error'); end if;
  end;

  procedure assertEquals(v varchar2, v2 varchar2) as
  begin
    if(v <> v2) then raise_application_error(-20111, 'Test error'); end if;
  end;

  procedure assertEquals(v number, v2 number) as
  begin
    if(v <> v2) then raise_application_error(-20111, 'Test error'); end if;
  end;

  procedure assertEquals(v json, v2 json) as
  begin
    if(v.to_char <> v2.to_char) then raise_application_error(-20111, 'Test error'); end if;
  end;

  procedure assertEqualsQuery(v number, v2 varchar2) as
    lc_nNumber number;
  begin
    execute immediate v2 into lc_nNumber;
    if(v <> lc_nNumber) then raise_application_error(-20111, 'Test error'); end if;
  end;

  procedure assertGreaterThan(n number, n2 number) as
  begin
    if(n <= n2) then raise_application_error(-20111, 'Test error'); end if;
  end;

  procedure saveResults as
  begin
    execute immediate 'insert into spiders_data.spd_t_json_testsuite (pass_count, fail_count, total_count, test_name, timestamp) values (:1, :2, :3, :4, :5)' using
    pass_count,fail_count,total_count,'report library tests', sysdate;
    commit;
  exception
    when others then
      p(sqlerrm);
  end;

  procedure postResults as
    score number;
  begin
    score := round((pass_count/total_count),2)*100;
    htp.p('PASS: ' || pass_count);
    htp.p('FAIL: ' || fail_count);
    htp.p('TOTAL: ' || total_count);
    htp.p('SCORE: ' || score || '%');
  end;
  
  procedure resetTest as
  begin
      pass_count := 0;
      fail_count := 0;
      total_count := 0;
  end;

END UT_COMMON;

/
