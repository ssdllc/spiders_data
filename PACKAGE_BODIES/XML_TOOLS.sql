--------------------------------------------------------
--  DDL for Package Body XML_TOOLS
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE PACKAGE BODY "SPIDERS_DATA"."XML_TOOLS" as


/*
This software has been released under the MIT license:

  Copyright (c) 2011 Synergy Software Design, LLC

  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.
*/




/*
function  test_date (in_dDate date) return number;

funtion test_date (in_dDate date) return number
  lc_nReturn number :=0;
as

begin
  return lc_nReturn;

  exception
      when others then
      lc_nReturn := 1;
      return lc_nReturn;

end;
*/

procedure BB_P_ERROR (in_jJSON json);
function import_xml_DML(in_cDML clob, in_nAdoid number, in_vFilename varchar2, in_vImportSection varchar2, in_cClob clob default '') return number;


procedure  p (in_nAPID number, in_nADOID number, in_nDOFID number default 0, in_vFilename varchar2, in_vImportSection varchar2, in_vNotes varchar2, in_dRunDate date, in_nTotalRecords number default 0, in_nCountOfRecords number default 0, in_vGrouping varchar2 default '') as

begin

insert into spd_t_import_log (apid, adoid, dofid, filename, import_section, notes, run_date, total_records, count_of_records, grouping) values (in_nAPID, in_nADOID, in_nDOFID, in_vFilename, in_vImportSection, in_vNotes, in_dRunDate,in_nTotalRecords,in_nCountOfRecords,in_vGrouping);
commit;

end;


function folderid(IN_vPATH varchar2, IN_nADOID number, IN_nFromCreateDO number default 0, IN_vFSES varchar2 default 'n') return number

as

lc_vFolderPath varchar2(2000) :='';
lc_vRaw varchar2(2000) :='2011_WFI_Routine_NBV_CA_MNB\FACS\2011_WFI_Wharf 3_NFA100001678829\facility photos\P8110117.jpg';
lc_nCount number :=0;
lc_nCounter number :=1;
lc_nFolderId number :=0;
lc_vDOName varchar2(255);
lc_cDML clob;
lc_nDML number;

  lc_jJson json;
  lc_jlResults json_list;
  lc_rec_val json_value;
  lc_rec json;
  lc_cnt number;

  lc_jResults json;
  lc_jMessages json;
  lc_jlMessages json_list;
  lc_vReturn varchar2(1);

  --error stuff
  lc_jError json;
  json_error EXCEPTION;

--lc_vPathPrefix varchar2(2000) := '/oranas01/SDRD/SPIDERS_FILES/LOADED/'; --NITC TEST
--lc_vPathPrefix varchar2(2000) := '/oranas01/SDRA/SPIDERS_FILES/LOADED/'; --NITC ACCEPTANCE
--lc_vPathPrefix varchar2(2000) := '/oranas01/SDRP/SPIDERS_FILES/LOADED/'; --NITC PRODUCTION
lc_vPathPrefix varchar2(2000) := '/u02/SPIDERS_FILES/LOADED/'; --FIDO


lc_nStartFolderPosition number :=1;

begin

select
  count(*) into lc_nCount
from
(
  select regexp_substr
    (
      IN_vPATH,
      '[^\/]+',
      1,
      level
    ) folder
  from
    dual
  connect by
    regexp_substr
    (
      IN_vPATH,
      '[^\/]+',
      1,
      level
    ) is not null
);

if IN_vFSES = 'FS' then
  lc_nStartFolderPosition := lc_nCount - 3;

end if;

if IN_vFSES = 'ES' then
  lc_nStartFolderPosition := lc_nCount-1;
  --htp.p(IN_vPATH);
end if;



for rec in (
              select
                regexp_substr
                (
                  IN_vPATH,
                  '[^\/]+',
                  1,
                  level
                ) folder
              from
                dual
              connect by
                regexp_substr
                (
                  IN_vPATH,
                  '[^\/]+',
                  1,
                  level
                ) is not null
          )
loop


--test each folder path to see if it has been created before. If it has, skip it, if it hasnt create the directory right here.
  --do it;



    if lc_nCounter >= lc_nStartFolderPosition then
      if lc_nCounter < lc_nCount then
        if lc_nCounter = lc_nStartFolderPosition then

          lc_vFolderPath := rec.folder;
        else
          lc_vFolderPath := lc_vFolderPath || '/' || rec.folder;
          --lc_vFolderPath := replace(lc_vFolderPath,'//','/');
        end if;
      end if;
    end if;


  lc_nCounter := lc_nCounter + 1;
end loop;



for rec in (select * from spd_t_folders where upper(folder_name) = upper(lc_vFolderPath))
loop

  lc_nFolderId := rec.folderid;
end loop;



if lc_nFolderId = 0 then
  select spd_s_folders.nextval into lc_nFolderId from dual;
  select delivery_order_name into lc_vDOName from spd_t_actn_dos where adoid = in_nAdoid;
--if this is coming from create delivery order, we need to handle the create a bit different
  if IN_nFromCreateDO = 1 then
    lc_cDML := 'insert into spd_t_folders (folderid, folder_name, full_path) values (' || lc_nFolderId|| ',''' || UPPER(IN_vPATH) || ''',''' || UPPER(IN_vPATH) || ''')';
    lc_nDML := import_xml_DML(lc_cDML, IN_nADOID, UPPER(IN_vPATH), 'INSERT INTO SPD_T_FOLDERS');

    lc_cDML := 'create or replace directory SPD_' || lc_nFolderId || ' AS ''' || lc_vPathPrefix || UPPER(IN_vPATH) || '''';
    lc_nDML := import_xml_DML(lc_cDML, IN_nADOID, UPPER(IN_vPATH), 'THUMBNAIL IN FROM CREATE DO TRUE');

    lc_cDML := 'GRANT READ ON DIRECTORY SPD_' || lc_nFolderId || ' TO APEX_PUBLIC_USER';
    lc_nDML := import_xml_DML(lc_cDML, IN_nADOID, UPPER(IN_vPATH), 'THUMBNAIL IN FROM CREATE DO TRUE');
    p(in_nAPID=>0, in_nADOID=>IN_nADOID, in_nDOFID=>0, in_vFilename=>lc_nFolderId, in_vImportSection=>'Create folder IN_nFromCreateDO = 1', in_vNotes=> lc_vPathPrefix || lc_vDOName || '/' || UPPER(lc_vFolderPath), in_dRunDate=>sysdate);

  else
    lc_cDML := 'insert into spd_t_folders (folderid, folder_name, full_path) values (' || lc_nFolderId|| ',''' || UPPER(lc_vFolderPath) || ''',''' || UPPER(lc_vFolderPath) || ''')';
    lc_nDML := import_xml_DML(lc_cDML, IN_nADOID, UPPER(IN_vPATH), 'INSERT INTO SPD_T_FOLDERS');

    --insert into spd_t_folders (folderid, folder_name, full_path) values (lc_nFolderId, UPPER(lc_vFolderPath), lc_vDOName || UPPER(lc_vFolderPath));

    lc_cDML := 'create or replace directory SPD_' || lc_nFolderId || ' AS ''' || lc_vPathPrefix || lc_vDOName || '/' || UPPER(lc_vFolderPath) || '''';
    lc_nDML := import_xml_DML(lc_cDML, IN_nADOID, UPPER(IN_vPATH), 'THUMBNAIL IN FROM CREATE DO FALSE');

    --execute immediate 'create or replace directory SPD_' || lc_nFolderId || ' AS ''' || lc_vPathPrefix || lc_vDOName || '/' || UPPER(lc_vFolderPath) || '''';


    lc_cDML := 'GRANT READ ON DIRECTORY SPD_' || lc_nFolderId || ' TO APEX_PUBLIC_USER';
    lc_nDML := import_xml_DML(lc_cDML, IN_nADOID, UPPER(IN_vPATH), 'THUMBNAIL IN FROM CREATE DO FALSE');
    --execute immediate 'GRANT READ ON DIRECTORY SPD_' || lc_nFolderId || ' TO APEX_PUBLIC_USER';
    p(in_nAPID=>0, in_nADOID=>IN_nADOID, in_nDOFID=>0, in_vFilename=>lc_nFolderId, in_vImportSection=>'Create folder IN_nFromCreateDO = 0', in_vNotes=>lc_vPathPrefix || lc_vDOName || '/' || UPPER(lc_vFolderPath), in_dRunDate=>sysdate);
  end if;
end if;


return lc_nFolderId;

  exception
    when others then
        lc_jError := json();
        lc_jlMessages := json_list();
        lc_jError.put('function','import_xml');
        lc_jError.put('module','xmlImportManager');
        lc_jError.put('target','Create folder IN_nFromCreateDO');
        lc_jError.put('ref_id',in_nAdoid);
        lc_jError.put('input','');
        lc_jlMessages.append(json_list('[{error:''CF.000X ' || SQLERRM || '''}]'));
        lc_jError.put('messages',lc_jlMessages);
        BB_P_ERROR(lc_jError);
        return 0;
end;


function getthumbnailid(IN_vFOLDERID varchar2, IN_vFILENAME varchar2, in_vImages varchar2 default 'y', in_nADOID number) return number;
function getthumbnailid(IN_vFOLDERID varchar2, IN_vFILENAME varchar2, in_vImages varchar2 default 'y', in_nADOID number) return number

as

lc_nThumbnailID number;
lc_nCount number :=0;
lc_bfBFILE bfile := BFILENAME('SPD_' || IN_vFOLDERID,IN_vFILENAME);
lc_nAmount number;
lc_bBlob blob;
dest_offset   integer := 1;
src_offset    integer := 1;

big_image blob;
small_image blob;
idm number;


  lc_jJson json;
  lc_jlResults json_list;
  lc_rec_val json_value;
  lc_rec json;
  lc_cnt number;

  lc_jResults json;
  lc_jMessages json;
  lc_jlMessages json_list;
  lc_vReturn varchar2(1);

  --error stuff
  lc_jError json;
  json_error EXCEPTION;
  lc_vError varchar2(4000);

begin

  lc_jMessages := json();
  lc_jlMessages := json_list();

select count(*) into lc_nCount from spd_t_thumbnails where filename = in_vFilename and folderid = in_VFolderid;



if lc_nCount = 0 then
--this image has not had thumbnails created
--so mak'em dummy


  --if DBMS_LOB.FILEEXISTS(lc_bfBFILE) != 0 then

    select spd_s_thumbnails.nextval into lc_nthumbnailid from dual;

    if upper(IN_vIMAGES) = 'Y' then

      dbms_lob.createtemporary (lc_bBlob, true);
      dbms_lob.open (lc_bfBFILE, dbms_lob.lob_readonly);
      dbms_lob.loadblobfromfile (lc_bBlob, lc_bfBFILE, dbms_lob.getlength (lc_bfBFILE), dest_offset, src_offset);
      dbms_lob.close (lc_bfBFILE);

      big_image :=EMPTY_BLOB();
      dbms_lob.createtemporary(small_image, true, DBMS_LOB.CALL);
      SELECT lc_bBlob INTO big_image FROM dual;


      ORDSYS.ORDImage.processCopy(big_image,'maxscale=620 620', small_image);
      ORDSYS.ORDImage.process(small_image,'cut=0 108 620 250');



    end if;
    insert into spd_t_thumbnails (thumbnailid, filename, folderid, carousel, thumbnail) values (lc_nthumbnailid, in_vFilename, in_vFolderid, small_image, big_image);



    --dbms_lob.freetemporary (lc_bBlob);
    commit;
 -- end if;

else
	for rec in (select thumbnailid from spd_t_thumbnails where filename = in_vFilename and folderid = in_VFolderid)
	loop
		lc_nThumbnailID := rec.thumbnailid;
	end loop;
end if;


return lc_nThumbnailID;

  exception
    when others then
        lc_vError := replace(SQLERRM,CHR(10),' ');
        lc_jError := json();
        lc_jError.put('function','get thumbnail id');
        lc_jError.put('module','xmlImportManager');
        lc_jError.put('target','Processing Thumbnail ' || in_VFolderid || ':' || in_vFilename);
        lc_jError.put('ref_id',in_nAdoid);
        lc_jError.put('input',in_vFilename);
        lc_jlMessages.append(json_list('[{error:''IXT.000X ' || lc_vError || '''}]'));
        lc_jError.put('messages',lc_jlMessages);
        BB_P_ERROR(lc_jError);
        lc_nThumbnailID :=0;
        return lc_nThumbnailid;

end;


function filename(IN_vPATH varchar2) return varchar2

as

lc_vFilename varchar2(255);

begin



for rec in (
              select
                regexp_substr
                (
                  IN_vPATH,
                  '[^\/]+',
                  1,
                  level
                ) folder
              from
                dual
              connect by
                regexp_substr
                (
                  IN_vPATH,
                  '[^\/]+',
                  1,
                  level
                ) is not null
          )
loop


      lc_vFilename := trim(rec.folder);

end loop;

--htp.p('[' || lc_vFilename || ']');
return lc_vFilename;

end;

--      --p(DBMS_LOB.SUBSTR(lc_xTemp1.getClobVal() , 100, 1));
procedure add_actn_do_fac(in_nADOID number, io_vNFAID IN OUT varchar2, IN_PL number, o_nDOFID OUT number, o_nFACID OUT number, in_vActivityUIC varchar2, in_vFacilityName varchar2, in_vFacilityNo varchar2);




function  test_val (IN_FROM varchar2, in_xml xmltype) return varchar2 as
lc_vReturn varchar2(3999) := '';
lc_jError json;
lc_jlMessages json_list;

begin

    if in_xml is null then
      lc_vReturn := '';
    else

        --p(IN_FROM,1);

      lc_vReturn := substr(in_xml.getstringval(),1,256);
      --htp.p('1 ' || lc_vReturn || '''');

      lc_vReturn := TRANSLATE (lc_vReturn, 'x'||CHR(10)||CHR(13), 'x');

      --htp.p('2 ' || lc_vReturn || '''');


      lc_vReturn := trim(lc_vReturn);

      --htp.p('3 ' || lc_vReturn || '''');
    end if;

return lc_vReturn;

  exception
    when others then
        lc_jError := json();
        lc_jlMessages := json_list();
        lc_jError.put('function','test_val');
        lc_jError.put('module','xmlImportManager');
        lc_jError.put('target','test_val');
        lc_jError.put('ref_id',1);
        lc_jError.put('input','');
        lc_jlMessages.append(json_list('[{error:''IX.000X ' || SQLERRM || '''}]'));
        lc_jError.put('messages',lc_jlMessages);
        BB_P_ERROR(lc_jError);
        return 'e';
end;

function  test_num (in_xml xmltype) return number as
lc_nReturn number := 0;
begin

    --if the xml value is not a number, we get an error, so we want to translate that to a 0. This makes it easy for us to implement a 0 as the value instead of adding difficult to test validation in validator. We eventually will do this in validator, but for now this works. I dont like changing data, but for now it will work. -MR 20100222
    begin

    if in_xml is null then
      lc_nReturn := 0;
    else
      lc_nReturn := in_xml.getnumberval();
    end if;

    exception
    when others then
      lc_nReturn := 0; --Not a number..

    end;
return lc_nReturn;
end;

procedure BB_P_ERROR (in_jJSON json) as

  lc_jJson json;


  lc_vModule varchar2(255) := '';
  lc_vTarget varchar2(255) := '';
  lc_nRef_ID number := 0;
  lc_vFunctionName varchar2(255) := '';
  lc_vInput varchar2(32000); --varchar2(4000);
  lc_vInput_val json_value;
  lc_vType varchar2(255);
  lc_vMessage varchar2(2000);
  lc_jlMessages json_list;
  lc_vStatement varchar2(12000);

  lc_rec_list json_list;
  lc_rec json;
  lc_rec_val json_value;

  lc_jErrorsToBrowser json;
  lc_jlErrorTableMessages json_list;

  lc_nSequence number;

  json_error EXCEPTION;

  lc_vInput_num number;
begin
  --parse json
  lc_jJson := in_jJSON;
  lc_jErrorsToBrowser := json();
  lc_jlErrorTableMessages := json_list();

  --Test for existence of jsonp attribute for local testing debug
  if(lc_jJson.exist('function')) THEN

      if(lc_jJson.exist('input')) THEN
        if(lc_jJson.exist('messages')) THEN
          if (lc_jJson.exist('function')) then
            lc_vFunctionName := lc_jJson.get('function').get_string;
          end if;

          if (lc_jJson.exist('module')) then
            lc_vModule := lc_jJson.get('module').get_string;
          end if;

          if (lc_jJson.exist('target')) then
            lc_vTarget := lc_jJson.get('target').get_string;
          end if;

          if (lc_jJson.exist('ref_id')) then
            lc_nRef_ID := lc_jJson.get('ref_id').get_number;
          end if;

lc_vInput := 'blah';
          /*
          if (lc_jJson.exist('input')) then
            lc_vInput := lc_jJson.get('input').get_string;
          end if;
          */

          lc_jlMessages := json_list(in_jJson.get('messages'));
          for i in 1..lc_jlMessages.count loop
            --add record to log table

            lc_rec_list := json_list(lc_jlMessages.get(i));

            for x in 1..lc_rec_list.count loop
              --htp.p('lc_rec_list.count = '||lc_rec_list.count);
              lc_rec_val := lc_rec_list.get(x);
              lc_rec := json(lc_rec_val);

              for y in 1..lc_rec.count loop
                --htp.p('lc_rec.count = '||lc_rec.count);

                lc_vType := lc_rec.get(y).mapname; --mapname was a bitch to find in the JSON_VALUE type.
                lc_vMessage := lc_rec.get(y).value_of;

                select spd_s_debug.nextval into lc_nSequence from dual;
                lc_jlErrorTableMessages.append(lc_nSequence);
                --htp.p('inserting with seq: '||lc_nSequence);
                lc_vStatement := 'insert into debug (debugid, functionname, input, message, messagetype, username, module, target, ref_id) values (' || lc_nSequence || ',''' || lc_vFunctionName || ''',''' || lc_vInput || ''',''' || lc_vMessage || ''',''' || lc_vType || ''',''' || v('APP_USER') || ''',''' || lc_vModule || ''',''' || lc_vTarget || ''',' || lc_nRef_ID || ')';
                --htp.p(lc_vStatement);
                execute immediate lc_vStatement;
                --htp.p('DONE WITH execute immediate');
                commit;
                --htp.p('COMMIT HERE');

              end loop;
            end loop;
          end loop;

        else
          --in JSON doesnt have messages array
          null;
        end if;
      else
        --in JSON doesnt have an input attribute
        null;
      end if;
  else
    --in JSON doesnt have a function attribute
    null;
  end if;


  --exception, output to browser something

  exception
    when others then
      dbms_output.put_line(SQLERRM);

end;


function import_xml_DML(in_cDML clob, in_nAdoid number, in_vFilename varchar2, in_vImportSection varchar2, in_cClob clob default '') return number as

  lc_jJson json;
  lc_jlResults json_list;
  lc_rec_val json_value;
  lc_rec json;
  lc_cnt number;

  lc_jResults json;
  lc_jMessages json;
  lc_jlMessages json_list;
  lc_vReturn varchar2(1);
  lc_vDML varchar2(32000);
  lc_cClob clob;
  --error stuff
  lc_jError json;
  json_error EXCEPTION;

begin
  lc_jMessages := json();
  lc_jlMessages := json_list();
  --lc_vDML := SUBSTR(in_cDML, 0, 32000);
  lc_cClob := in_cClob;

  if dbms_lob.getLength(in_cClob) > 0 then
    execute immediate in_cDML using lc_cClob;
  else
    execute immediate in_cDML;
  end if;

  return 1; --if succesful return 1, if exception return 0. number will be used as source for numerator

  exception
    when others then
        htp.p('something2: ' || dbms_lob.getLength(in_cDML));
        lc_jError := json();
        lc_jError.put('function','import_xml');
        lc_jError.put('module','xmlImportManager');
        lc_jError.put('target',in_vImportSection);
        lc_jError.put('ref_id',in_nAdoid);
        lc_jError.put('input','');
        lc_jlMessages.append(json_list('[{error:''IX.000X ' || SQLERRM || '''}]'));
        lc_jError.put('messages',lc_jlMessages);
        BB_P_ERROR(lc_jError);
        return 0;

end;

procedure import_xml(IN_nADOID number, IN_vFILENAME varchar2 default '', IN_vProcessImages varchar2 default 'y') as
lc_xAction xmltype;
lc_xTemp xmltype;
lc_xTemp1 xmltype;
lc_xTemp2 xmltype;
lc_xTemp3 xmltype;
lc_vString varchar2(32767);
lc_nADOID number := IN_nADOID;--100181;--100160,100181
lc_vFileName varchar2(255):=IN_vFILENAME;--'2';
lc_cClob clob;
lc_vPath varchar2(3000);
lc_nDOFID number :=0;
lc_nAIDEFECTID number :=0;
lc_nXMLFileID number :=0;

lc_nListid number;
lc_nListid2 number;
lc_nAIID number;
lc_nAIID_parent number;
lc_nUFIIExists number;
lc_nNewListID number;

lc_xExecutiveSummaryNode xmltype;
lc_xDrawingsNode xmltype;
lc_xSupportingFilesNode xmltype;
lc_xPhotosNode xmltype;
lc_xFacilitiesNode xmltype;
lc_xFacMetaDataNode xmltype;
lc_xFacDrawingsNode xmltype;
lc_xFacSupportingFilesNode xmltype;
lc_xAssetSummaryNode xmltype;
lc_xFindRecsNode xmltype;
lc_xAssetInventoryNode xmltype;
lc_xAIMetaDataNode xmltype;
lc_xDefectsNode xmltype;
lc_xDefectMetaDataNode xmltype;
lc_xMooringFittingsNode xmltype;
lc_xAIPhotosNode xmltype;
lc_xAIDrawingsNode xmltype;
lc_xFacilityRepairsNode xmltype;
lc_nRow number :=0;

lc_nFolderId number :=0;
lc_vTemp varchar2(255);
lc_nThumbnailid number :=0;

lc_nCountFindReco number :=0;
lc_vUFII varchar2(50);
lc_cFinding clob;
lc_cRecommendation clob;
lc_xPhotoAssetTypesNode xmltype;
lc_nPhotoId number;
lc_nContinue number;
lc_nFacilityCount number :=0;

lc_dDate date := sysdate;
lc_nAPID number :=0;
lc_nNodeCount number:=0;
lc_nAFRID number :=0;


lc_vDataReqRevision varchar2(1000);
lc_vExitBriefDate varchar2(1000);
lc_vExitBriefTime varchar2(1000);
lc_vExitBriefLOC varchar2(1000);
lc_vDrawingID varchar2(1000);
lc_vNAVFACDrawingNumber varchar2(1000);
lc_vDrawingDescription varchar2(4000);
lc_vDrawingType varchar2(1000);
lc_vSupportingFileID varchar2(1000);
lc_vSupportingFileDescription varchar2(1000);
lc_vPhotoID varchar2(1000);
lc_vPhotoCaption varchar2(2000);
lc_vPhotoType varchar2(1000);
lc_vOnsiteStartDate varchar2(1000);
lc_vOnsiteEndDate varchar2(1000);
lc_vSubmittalDate varchar2(1000);
lc_vOverallEngAssRatingASCE varchar2(1000);
lc_nOverallCIRating number;
lc_nOveral5YearProjectedCI number;
lc_nOveral10YearProjectedCI number;
lc_vCurrentOperationalRating varchar2(1000);
lc_vOperationalRestrictions varchar2(1000);
lc_vRepairRecommendations varchar2(1000);
lc_vCurFacUsageDescription varchar2(1000);
lc_vTotalNumOfPersOnSite varchar2(1000);
lc_vTotalNumberOfDaysOnSite varchar2(1000);
lc_vMetaDisplay varchar2(1000);
lc_vMetaValue varchar2(1000);
lc_vAssetType varchar2(1000);
lc_vFacilitySection varchar2(1000);
lc_vNoOfAssetsCount varchar2(1000);
lc_vQuantityOfUnits varchar2(1000);
lc_vUnitType varchar2(1000);
lc_vCurrentCI varchar2(1000);
lc_nApproxRepairCost number;
lc_vEstimatedResultingCI varchar2(1000);
lc_vMaterial varchar2(1000);
lc_vMaterialDescription varchar2(1000);
lc_vDefectIDNo varchar2(1000);
lc_DefectLoction varchar2(1000);
lc_vDefectCenter varchar2(1000);
lc_vCriticalityFactor varchar2(1000);
lc_vDefectType varchar2(1000);
lc_vSRMClassification varchar2(1000);
lc_vFittingIDNumber varchar2(1000);
lc_vMooringType varchar2(1000);
lc_vDesignCapacity varchar2(1000);
lc_vRatingCapacity varchar2(1000);
lc_vConditionFitting varchar2(1000);
lc_vConditionBase varchar2(1000);
lc_vMXLFacilityName varchar2(1000);
lc_vXMLFacilityNFAID varchar2(1000);

lc_xDoFacRepairsNode varchar2(255);
lc_vUniFormatIICode varchar2(255);
lc_vRepairDescription varchar2(255);
lc_nQuantity NUMBER;
lc_vUnits varchar2(255);
lc_nMaterialUnitCost NUMBER;
lc_nLaborUnitCost NUMBER;



lc_nRowNum number;
lc_nNodeCountAssetInventory number;
lc_nRowNumAssetInventory number;
lc_nRowNumAssetSummary number;
lc_nNodeCountAssetSummary number;
lc_nNodeCountAIDefects number;
lc_nNodeCountMoorings number;
lc_nNodeCountFindRec number;
lc_nNodeCountASPhotos number;
lc_nNodeCountFacilities number;
lc_nNodeCountFacDrawings number;
lc_nNodeCountSupFiles number;

lc_nDML number :=0;
lc_nDML_inner number :=0;
lc_nSuccess number :=0;
lc_nSuccessDefects number :=0;


lc_cDML clob;
lc_vImportSectionForDML varchar2(2000);

  lc_jJson json;
  lc_jlResults json_list;
  lc_rec_val json_value;
  lc_rec json;
  lc_cnt number;

  lc_jResults json;
  lc_jMessages json;
  lc_jlMessages json_list;
  lc_vReturn varchar2(1);

  --error stuff
  lc_jError json;
  json_error EXCEPTION;
  lc_vError varchar2(4000);
begin

    lc_jMessages := json();
    lc_jlMessages := json_list();

for rec in (select apid from spd_t_actn_dos where adoid = IN_nADOID)
loop
  lc_nAPID := rec.apid;
end loop;

/*------------DELETE EVERYTHING FOR THIS ADOID-------------------*/
lc_vImportSectionForDML :='DELETE EVERYTHING FOR THIS ADOID';
p(in_nAPID=>lc_nAPID, in_nADOID=>IN_nADOID, in_nDOFID=>0, in_vFilename=>IN_vFILENAME, in_vImportSection=>lc_vImportSectionForDML, in_vNotes=>'', in_dRunDate=>lc_dDate);


lc_cDML := 'delete from spd_t_thumbnails where thumbnailid in
(
  select thumbnailid from spd_t_do_fac_mooring_fittings where aiid in (select aiid from spd_t_do_fac_asset_inventory where dofid in (select dofid from spd_t_do_facilities where adoid = ' || lc_nADOID || ')) and thumbnailid is not null
  union
  select thumbnailid from spd_t_do_supporting_files where adoid = ' || lc_nADOID || ' and thumbnailid is not null
  union
  select thumbnailid from spd_t_ai_supporting_files where aiid in (select aiid from spd_t_do_fac_asset_inventory where dofid in (select dofid from spd_t_do_facilities where adoid = ' || lc_nADOID || ')) and thumbnailid is not null
  union
  select thumbnailid from spd_t_do_fac_supporting_files where dofid in (select dofid from spd_t_do_facilities where adoid = ' || lc_nADOID || ') and thumbnailid is not null
)';
lc_nDML := import_xml_DML(lc_cDML,lc_nADOID,IN_vFILENAME,lc_vImportSectionForDML);

lc_cDML := 'delete from spd_t_do_supporting_files where adoid = ' || lc_nADOID ;
lc_nDML := import_xml_DML(lc_cDML,lc_nADOID,IN_vFILENAME,lc_vImportSectionForDML);

lc_cDML := 'delete from spd_t_do_fac_mooring_fittings where aiid in (select aiid from spd_t_do_fac_asset_inventory where dofid in (select dofid from spd_t_do_facilities where adoid = ' || lc_nADOID || '))';
lc_nDML := import_xml_DML(lc_cDML,lc_nADOID,IN_vFILENAME,lc_vImportSectionForDML);

lc_cDML := 'delete from spd_t_ai_defect_meta_data where aidefectid in (select aidefectid from spd_t_ai_defects where aiid in (select aiid from spd_t_do_fac_asset_inventory where dofid  in (select dofid from spd_t_do_facilities where adoid = ' || lc_nADOID || ')))';
lc_nDML := import_xml_DML(lc_cDML,lc_nADOID,IN_vFILENAME,lc_vImportSectionForDML);

lc_cDML := 'delete from spd_t_ai_defects where aiid in (select aiid from spd_t_do_fac_asset_inventory where dofid in (select dofid from spd_t_do_facilities where adoid = ' || lc_nADOID || '))';
lc_nDML := import_xml_DML(lc_cDML,lc_nADOID,IN_vFILENAME,lc_vImportSectionForDML);

lc_cDML := 'delete from spd_t_ai_find_rec where aiid in (select aiid from spd_t_do_fac_asset_inventory where dofid in (select dofid from spd_t_do_facilities where adoid = ' || lc_nADOID || '))';
lc_nDML := import_xml_DML(lc_cDML,lc_nADOID,IN_vFILENAME,lc_vImportSectionForDML);

lc_cDML := 'delete from spd_t_ai_meta_data where aiid in (select aiid from spd_t_do_fac_asset_inventory where dofid in (select dofid from spd_t_do_facilities where adoid = ' || lc_nADOID || '))';
lc_nDML := import_xml_DML(lc_cDML,lc_nADOID,IN_vFILENAME,lc_vImportSectionForDML);

lc_cDML := 'delete from spd_t_ai_supporting_files where aiid in (select aiid from spd_t_do_fac_asset_inventory where dofid in (select dofid from spd_t_do_facilities where adoid = ' || lc_nADOID || '))';
lc_nDML := import_xml_DML(lc_cDML,lc_nADOID,IN_vFILENAME,lc_vImportSectionForDML);

lc_cDML := 'delete from spd_t_do_fac_asset_inventory where aiid in (select aiid from spd_t_do_fac_asset_inventory where dofid in (select dofid from spd_t_do_facilities where adoid = ' || lc_nADOID || '))';
lc_nDML := import_xml_DML(lc_cDML,lc_nADOID,IN_vFILENAME,lc_vImportSectionForDML);

lc_cDML := 'delete from spd_t_do_facility_meta_data where dofid in (select dofid from spd_t_do_facilities where adoid = ' || lc_nADOID || ')';
lc_nDML := import_xml_DML(lc_cDML,lc_nADOID,IN_vFILENAME,lc_vImportSectionForDML);

lc_cDML := 'delete from spd_t_do_fac_supporting_files where dofid in (select dofid from spd_t_do_facilities where adoid = ' || lc_nADOID || ')';
lc_nDML := import_xml_DML(lc_cDML,lc_nADOID,IN_vFILENAME,lc_vImportSectionForDML);


--delete from spd_t_lists_for_review; --not deleteing this list for now,
--commit;
/*-------------END DELETE EVERYTHING FOR THIS ADOID---------------*/




SELECT
  xmlfileid into lc_nXMLFileID
FROM
  spd_t_xml_files
where
  adoid = IN_nADOID;





/*------------ACTION-------------------*/
lc_vImportSectionForDML :='UPDATE DELIVERY ORDER TABLE';
p(in_nAPID=>lc_nAPID, in_nADOID=>IN_nADOID, in_nDOFID=>0, in_vFilename=>IN_vFILENAME, in_vImportSection=>lc_vImportSectionForDML, in_vNotes=>'', in_dRunDate=>lc_dDate);
for rec_action in
            (
              SELECT
                xml_table.column_value action
              FROM
                spd_t_xml_files source_table,
                XMLTable('/Action' passing source_table.xml_document) xml_table
              where
                source_table.xmlfileid = lc_nXMLFileID
            )
loop

    lc_xAction := rec_action.action;

    --p(lc_xAction.extract('/Action/ActivityName/text()').getstringval(),3);



lc_vSubmittalDate := test_val('/Action/SubmittalDate/',lc_xAction.extract('/Action/SubmittalDate/text()'));
lc_vDataReqRevision := test_val('/Action/DataReqRevision/',lc_xAction.extract('/Action/DataReqRevision/text()'));
lc_vExitBriefDate := test_val('/Action/ExitBrief/ExitBriefDate',lc_xAction.extract('/Action/ExitBrief/ExitBriefDate/text()'));
lc_vExitBriefTime := test_val('/Action/ExitBrief/ExitBriefTime/',lc_xAction.extract('/Action/ExitBrief/ExitBriefTime/text()'));
lc_vExitBriefLOC := test_val('/Action/ExitBrief/ExitBriefLOC',lc_xAction.extract('/Action/ExitBrief/ExitBriefLOC/text()'));



/*------------UPDATE DELIVERY ORDER TABLE-------------------*/
--p('hey there' || lc_xAction.extract('/Action/SubmittalDate/text()').getstringval(),99);
lc_cDML := '  update
    spd_t_actn_dos
  set
    spd_t_actn_dos.ktr_submittal_date = to_date(''' || lc_vSubmittalDate || ''',''MM/DD/YYYY''),
    spd_t_actn_dos.spiders_data_version =''' ||  lc_vDataReqRevision || ''',
    spd_t_actn_dos.exit_brief_date = ''' || lc_vExitBriefDate || ''',
    spd_t_actn_dos.exit_brief_time = ''' || lc_vExitBriefTime || ''',
    spd_t_actn_dos.exit_brief_loc = ''' || lc_vExitBriefLOC || '''
  where
    adoid = ' || lc_nADOID;

lc_nDML := import_xml_DML(lc_cDML,lc_nADOID,IN_vFILENAME,lc_vImportSectionForDML);
    --(add field)
/*------------END UPDATE DELIVERY ORDER TABLE-------------------*/






/*------------EXECUTIVE SUMMARY OF ACTION-------------------*/
  select extract(lc_xAction, '/Action/ExecutiveSummary') into lc_xExecutiveSummaryNode from dual;



lc_vImportSectionForDML :='EXECUTIVE SUMMARY OF ACTION';


  lc_cClob := '';

/*------------PARAGRAPH OF EXECUTIVE SUMMARY-------------------*/
  for rec_ex_sum_paragraph in (
  SELECT
    xml_table.column_value paragraph
  FROM
    XMLTable('/ExecutiveSummary/Paragraph' passing lc_xExecutiveSummaryNode) xml_table
  )
  loop

  lc_cClob := lc_cClob || '<p>';

/*------------SENTENCE OF PARAGRAPH-------------------*/
        for rec_ex_sum_paragraph_sentence in (
        SELECT
          xml_table.column_value sentence
        FROM
          XMLTable('/Paragraph/Sentence' passing rec_ex_sum_paragraph.paragraph) xml_table
        )
        loop
          lc_cClob := lc_cClob || ' ' || rec_ex_sum_paragraph_sentence.sentence.extract('/Sentence/text()').getstringval();
        end loop;

  lc_cClob := lc_cClob || '</p>';
/*------------END SENTENCE OF PARAGRAPH-------------------*/

  end loop;
/*------------END PARAGRAPH OF EXECUTIVE SUMMARY-------------------*/


/*------------UPDATE EXECUTIVE SUMMARY-------------------*/
lc_cDML := '  update
    spd_t_actn_dos
  set
    spd_t_actn_dos.executive_summary = :1
  where
    adoid = ' || lc_nADOID;


/*
MR NOTE: This is sending a clob as a parameter in the execute immediate. If sent as an inline "string" oracle barfs and wont update the table.
The :1 in the lc_cDML is a bind variable, in these situations you have to send a separate clob parameter to the dml function, the clob parameter sent will be used
as a binding variable to the execute immediate statement, allowing clob fields of unusual size to be sent in the manner we want.
*/

lc_nDML := import_xml_DML(lc_cDML,lc_nADOID,IN_vFILENAME,lc_vImportSectionForDML, lc_cClob);
p(in_nAPID=>lc_nAPID, in_nADOID=>IN_nADOID, in_nDOFID=>0, in_vFilename=>IN_vFILENAME, in_vImportSection=>'EXECUTIVE SUMMARY', in_vNotes=>'', in_dRunDate=>lc_dDate, in_nTotalRecords => 1, in_nCountOfRecords => lc_nDML, in_vGrouping => 'EXECUTIVE SUMMARY');

/*------------END UPDATE EXECUTIVE SUMMARY-------------------*/
/*------------END EXECUTIVE SUMMARY OF ACTION-------------------*/















/*------------DRAWINGS OF ACTION-------------------*/

  select extract(lc_xAction, '/Action/Drawings') into lc_xDrawingsNode from dual;
  select count(*) into lc_nNodeCount FROM XMLTable('/Drawings/Drawing' passing lc_xDrawingsNode);
lc_vImportSectionForDML :='DRAWINGS OF ACTION';

/*------------DRAWING OF DRAWINGS-------------------*/
  lc_nSuccess := 0;
  for rec_drawings in (
  SELECT
    rownum rn,
    xml_table.column_value drawing
  FROM
    XMLTable('/Drawings/Drawing' passing lc_xDrawingsNode) xml_table
  )
  loop

  lc_vTemp := test_val('/Drawing/FileName/text()',rec_drawings.drawing.extract('/Drawing/FileName/text()'));
  lc_nFolderId := folderid(lc_vTemp, lc_nADOID );
  lc_vFileName := filename(lc_vTemp);

lc_vImportSectionForDML :='DRAWING OF ACTION';
p(in_nAPID=>lc_nAPID, in_nADOID=>IN_nADOID, in_nDOFID=>0, in_vFilename=>IN_vFILENAME, in_vImportSection=>lc_vImportSectionForDML, in_vNotes=>'SPD_' || lc_nFolderId || ',' || lc_vFileName, in_dRunDate=>lc_dDate);



/*------------CREATE DRAWING RECORDS (spd_t_do_supporting_files)-------------------*/


    lc_vDrawingID := test_val('/Drawings/Drawing/DrawingID/text()',rec_drawings.drawing.extract('/Drawings/Drawing/DrawingID/text()'));
    lc_vNAVFACDrawingNumber := test_val('/Drawing/NAVFACDrawingnumber/text()',rec_drawings.drawing.extract('/Drawing/NAVFACDrawingnumber/text()'));
    lc_vDrawingDescription := test_val('/Drawing/Description/text()',rec_drawings.drawing.extract('/Drawing/Description/text()'));
    lc_vDrawingType := test_val('/Drawing/DrawingType/text()',rec_drawings.drawing.extract('/Drawing/DrawingType/text()'));


lc_cDML := 'insert into
      spd_t_do_supporting_files
    (
      adoid,
      local_id,
      NAVFAC_drawing_number,
      caption,
      file_type_string,
      file_type_listid,
      file_name,
      file_section_listid,
      folderid,
      the_bfile
    )
    values
    (
      ' || lc_nADOID || ',
      ''' || lc_vDrawingID || ''',
      ''' || lc_vNAVFACDrawingNumber || ''',
      ''' || lc_vDrawingDescription || ''',
      ''' || lc_vDrawingType || ''',
      (select listid from spd_t_lists where listname = ''list_delivery_order_file_types'' and displayvalue = ''General''),
      ''' || lc_vFileName || ''',
      (select listid from spd_t_lists where listname = ''list_delivery_order_file_sections'' and displayvalue = ''Drawing Files''),
      ''' || lc_nFolderId || ''',
      bfilename(''SPD_' || lc_nFolderId || ''',''' || lc_vFileName || ''')
    )';

lc_nDML := import_xml_DML(lc_cDML,lc_nADOID,IN_vFILENAME,lc_vImportSectionForDML);
/*------------END CREATE DRAWING RECORDS (spd_t_do_supporting_files)-------------------*/
lc_nSuccess := lc_nSuccess + lc_nDML;
  end loop;
p(in_nAPID=>lc_nAPID, in_nADOID=>IN_nADOID, in_nDOFID=>0, in_vFilename=>IN_vFILENAME, in_vImportSection=>lc_vImportSectionForDML, in_vNotes=>'Drawings Count: ' || lc_nNodeCount, in_dRunDate=>lc_dDate, in_nTotalRecords => lc_nNodeCount, in_nCountOfRecords => lc_nSuccess, in_vGrouping => lc_vImportSectionForDML);

/*------------END DRAWING OF DRAWINGS-------------------*/
/*------------END DRAWINGS OF ACTION-------------------*/




/*------------SUPPORTING FILES OF ACTION-------------------*/

  select extract(lc_xAction, '/Action/SupportingFiles') into lc_xSupportingFilesNode from dual;
lc_vImportSectionForDML :='SUPPORTING FILES OF ACTION';

/*------------CUSTOM SUPPORTING FILES FOR FACILITY-------------------*/
--ES File
lc_vTemp := test_val('FileName',lc_xAction.extract('/Action/ESFile/text()'));
lc_nFolderId := folderid(lc_vTemp, lc_nADOID,0, 'ES' );
lc_vFileName := filename(lc_vTemp);



lc_cDML := 'insert into
    spd_t_do_supporting_files
    (
      adoid,
      local_id,
      caption,
      file_type_string,
      file_type_listid,
      file_name,
      file_section_listid,
      folderid,
      the_bfile
    )
    values
    (
      ' || lc_nADOID || ',
      null,
      ''Executive Summary File'',
      ''General'',
      (select listid from spd_t_lists where listname = ''list_delivery_order_file_types'' and displayvalue = ''General''),
      ''' || lc_vFileName || ''',
      (select listid from spd_t_lists where listname = ''list_delivery_order_file_sections'' and displayvalue = ''Supporting Files''),
      ''' || lc_nFolderId || ''',
      bfilename(''SPD_' || lc_nFolderId || ''',''' || lc_vFileName || ''')
    )';

lc_nDML := import_xml_DML(lc_cDML,lc_nADOID,IN_vFILENAME,lc_vImportSectionForDML);
p(in_nAPID=>lc_nAPID, in_nADOID=>IN_nADOID, in_nDOFID=>0, in_vFilename=>IN_vFILENAME, in_vImportSection=>lc_vImportSectionForDML, in_vNotes=>'ESFile', in_dRunDate=>lc_dDate, in_nTotalRecords => 1, in_nCountOfRecords => lc_nDML, in_vGrouping => 'Executive Summary File');

/*------------END CUSTOM SUPPORTING FILES FOR FILES-------------------*/

  select count(*) into lc_nNodeCount FROM XMLTable('/SupportingFiles/SupportingFile' passing lc_xSupportingFilesNode);
lc_vImportSectionForDML :='SUPPORTING FILES OF ACTION';
p(in_nAPID=>lc_nAPID, in_nADOID=>IN_nADOID, in_nDOFID=>0, in_vFilename=>IN_vFILENAME, in_vImportSection=>lc_vImportSectionForDML, in_vNotes=>'SupportingFiles Count: ' || lc_nNodeCount, in_dRunDate=>lc_dDate);

/*------------SUPPORTING FILE OF SUPPORTING FILES-------------------*/
  lc_nSuccess := 0;
  for rec_supporting_files in (
  SELECT
    rownum rn,
    xml_table.column_value supportingfile
  FROM
    XMLTable('/SupportingFiles/SupportingFile' passing lc_xSupportingFilesNode) xml_table
  )
  loop

  lc_vTemp := test_val('FileName',rec_supporting_files.supportingfile.extract('/SupportingFile/FileName/text()'));
  lc_nFolderId := folderid(lc_vTemp, lc_nADOID );
  lc_vFileName := filename(lc_vTemp);

  lc_vSupportingFileID := test_val('SupportingFileID',rec_supporting_files.supportingfile.extract('/SupportingFile/SupportingFileID/text()'));
  lc_vSupportingFileDescription := test_val('SupportingFile/Description',rec_supporting_files.supportingfile.extract('/SupportingFile/Description/text()'));

/*------------CREATE SUPPORTING FILE RECORD (spd_t_do_supporting_files)-------------------*/
lc_cDML := 'insert into
    spd_t_do_supporting_files
    (
      adoid,
      local_id,
      caption,
      file_type_string,
      file_type_listid,
      file_name,
      file_section_listid,
      folderid,
      the_bfile
    )
    values
    (
      ' || lc_nADOID || ',
      ''' || lc_vSupportingFileID || ''',
      ''' || lc_vSupportingFileDescription || ''',
      ''General'',
      (select listid from spd_t_lists where listname = ''list_delivery_order_file_types'' and displayvalue = ''General''),
      ''' || lc_vFileName || ''',
      (select listid from spd_t_lists where listname = ''list_delivery_order_file_sections'' and displayvalue = ''Supporting Files''),
      ''' || lc_nFolderId || ''',
      bfilename(''SPD_' || lc_nFolderId || ''',''' || lc_vFileName || ''')
    )';

lc_nDML := import_xml_DML(lc_cDML,lc_nADOID,IN_vFILENAME,lc_vImportSectionForDML);
/*------------END CREATE SUPPORTING FILE RECORD (spd_t_do_supporting_files)-------------------*/
lc_nSuccess := lc_nSuccess + lc_nDML;
  end loop;
p(in_nAPID=>lc_nAPID, in_nADOID=>IN_nADOID, in_nDOFID=>0, in_vFilename=>IN_vFILENAME, in_vImportSection=>lc_vImportSectionForDML, in_vNotes=>'', in_dRunDate=>lc_dDate, in_nTotalRecords => lc_nNodeCount, in_nCountOfRecords => lc_nSuccess, in_vGrouping => 'Action Supporting Files');

/*------------END SUPPORTING FILE OF SUPPORTING FILES-------------------*/
/*------------END SUPPORTING FILES OF ACTION-------------------*/










/*------------PHOTOS OF ACTION-------------------*/



  select extract(lc_xAction, '/Action/Photos') into lc_xPhotosNode from dual;
  select count(*) into lc_nNodeCount FROM XMLTable('/Photos/Photo' passing lc_xPhotosNode);
lc_vImportSectionForDML :='PHOTOS OF ACTION';

/*------------PHOTO OF PHOTOS-------------------*/
  lc_nSuccess :=0;
  for rec_photos in (
  SELECT
    rownum rn,
    xml_table.column_value photo
  FROM
    XMLTable('/Photos/Photo' passing lc_xPhotosNode) xml_table
  )
  loop

  lc_vTemp := test_val('FileName',rec_photos.photo.extract('/Photo/FileName/text()'));
  lc_nFolderId := folderid(lc_vTemp, lc_nADOID );
  lc_vFileName := filename(lc_vTemp);
  lc_nThumbnailid := getthumbnailid(lc_nFolderId, lc_vFileName,IN_vProcessImages,IN_nADOID);
  --p(in_nAPID=>lc_nAPID, in_nADOID=>IN_nADOID, in_nDOFID=>0, in_vFilename=>IN_vFILENAME, in_vImportSection=>'PHOTO '|| rec_photos.rn || ' OF ' || lc_nNodeCount || ' PHOTOS (ACTION)', in_vNotes=>'action thumbnail process:' || lc_nFolderId || ', ' || lc_vFileName, in_dRunDate=>lc_dDate);


  lc_vPhotoID := test_val('PhotoID',rec_photos.photo.extract('/Photo/PhotoID/text()'));
  lc_vPhotoCaption := test_val('Caption',rec_photos.photo.extract('/Photo/Caption/text()'));
  lc_vPhotoType := test_val('something',rec_photos.photo.extract('/Photo/PhotoType/text()'));

/*------------CREATE PHOTO RECORD (spd_t_do_supporting_files)-------------------*/
lc_cDML := 'insert into
      spd_t_do_supporting_files
    (
      adoid,
      local_id,
      caption,
      file_type_string,
      file_type_listid,
      file_name,
      file_section_listid,
      folderid,
      the_bfile,
      thumbnailid
    )
    values
    (
      ' || lc_nADOID || ',
      ''' || lc_vPhotoID || ''',
      ''' || lc_vPhotoCaption || ''',
      ''' || lc_vPhotoType || ''',
      (select listid from spd_t_lists where listname = ''list_delivery_order_file_types'' and displayvalue = ''' || lc_vPhotoType || ''',
      ''' || lc_vFileName || ''',
      (select listid from spd_t_lists where listname = ''list_delivery_order_file_sections'' and displayvalue = ''Photo Files''),
      ''' || lc_nFolderId || ''',
      bfilename(''SPD_' || lc_nFolderId || ''',''' || lc_vFileName || ''')
      ' || lc_nThumbnailid || '
    )';

lc_nDML := import_xml_DML(lc_cDML,lc_nADOID,IN_vFILENAME,lc_vImportSectionForDML);
/*------------END CREATE PHOTO RECORD (spd_t_do_supporting_files)-------------------*/
lc_nSuccess := lc_nSuccess + lc_nDML;
  end loop;
p(in_nAPID=>lc_nAPID, in_nADOID=>IN_nADOID, in_nDOFID=>0, in_vFilename=>IN_vFILENAME, in_vImportSection=>lc_vImportSectionForDML, in_vNotes=>'', in_dRunDate=>lc_dDate, in_nTotalRecords => lc_nNodeCount, in_nCountOfRecords => lc_nSuccess, in_vGrouping => 'Executive Summary File');
/*------------END PHOTO OF PHOTOS-------------------*/
/*------------END PHOTOS OF ACTION-------------------*/









/*------------FACILITIES OF ACTION-------------------*/

  select extract(lc_xAction, '/Action/Facilities') into lc_xFacilitiesNode from dual;
  select count(*) into lc_nNodeCountFacilities FROM XMLTable('/Facilities/Facility' passing lc_xFacilitiesNode);
lc_vImportSectionForDML :='FACILITIES OF ACTION';
p(in_nAPID=>lc_nAPID, in_nADOID=>IN_nADOID, in_nDOFID=>0, in_vFilename=>IN_vFILENAME, in_vImportSection=>lc_vImportSectionForDML, in_vNotes=>'Facilities Count: ' || lc_nNodeCount, in_dRunDate=>lc_dDate);


/*------------FACILITY OF FACILITIES-------------------*/
  lc_nRowNum := 0;
  for rec_facilities in (
  SELECT
    rownum rn,
    xml_table.column_value facility
  FROM
    XMLTable('/Facilities/Facility' passing lc_xFacilitiesNode) xml_table
  )
  loop

/*
MR:
Adding new functionality to match the xml facilities with their proper DOFID's

select dofid from
spd_t_xml_facilities_match
where

xml_facility_name = rec_facilities.facility.extract('/Facility/FacilityName/text()').getstringval() and
xml_nfaid = rec_facilities.facility.extract('/Facility/FacilityNo/text()').getstringval();
adoid = IN_nADOID


*/

  lc_nDOFID :=0;
  lc_vMXLFacilityName := test_val('/Facility/FacilityName/text()',rec_facilities.facility.extract('/Facility/FacilityName/text()'));
  lc_vXMLFacilityNFAID :=  test_val('/Facility/FacilityNo/text()',rec_facilities.facility.extract('/Facility/NFAID/text()'));
  select
    dofid into lc_nDOFID
  from
    spd_t_xml_facilities_match
  where
    xml_facility_name = lc_vMXLFacilityName and
    xml_nfaid = lc_vXMLFacilityNFAID and
    adoid = IN_nADOID;

  if lc_nDOFID > 0 then
    --lc_nDOFID := rec_facilities.facility.extract('/Facility/SPIDERS_FACILITYID/text()').getstringval();
    select count(dofid) into lc_nFacilityCount from spd_t_do_facilities where dofid = lc_nDOFID;
    if lc_nFacilityCount = 1 then
  /*
    get_DOFID(
      rec_facilities.facility.extract('/Facility/ExecutiveTable/FACID/text()').getstringval(),
      in_nADOID,
      rec_facilities.facility.extract('/Facility/ExecutiveTable/NFAID/text()').getstringval(),
      'uhhh',--lc_xAction.extract('/Action/ActivityUIC/text()').getstringval(),
      rec_facilities.facility.extract('/Facility/FacilityName/text()').getstringval(),
      rec_facilities.facility.extract('/Facility/FacilityNo/text()').getstringval()
    );
  */

    --p(rec_facilities.facility.extract('/Facility/ExecutiveTable/FACID/text()').getstringval(),3);
    --using the FACID value in the xml, get the DOFID for this facility. If the DOFID does not exist, we need to do something about that eventually
    --select dofid into lc_nDOFID from spd_t_do_facilities where facid =  and adoid = lc_nADOID;






    /*
    update spd_t_xml_files set xml_document =
    rec_facilities.facility.extract('/Facility/ExecutiveTable/FACID/text()').getstringval()
    where adoid = in_nADOID;
    */

  /*------------UPDATE FACILITY RECORD-------------------*/
  select count(*) into lc_nNodeCount FROM XMLTable('/Facilities/Facility' passing lc_xFacilitiesNode);
  lc_vImportSectionForDML :='FACILITY ' || rec_facilities.rn || ' OF ' || lc_nNodeCount || ' FACILITIES';
  p(in_nAPID=>lc_nAPID, in_nADOID=>IN_nADOID, in_nDOFID=>lc_nDOFID, in_vFilename=>IN_vFILENAME, in_vImportSection=>lc_vImportSectionForDML, in_vNotes=>rec_facilities.rn || ' ' || lc_vMXLFacilityName, in_dRunDate=>lc_dDate);


  lc_vOnsiteStartDate :=test_val('/Facility/OnsiteStartDate/',rec_facilities.facility.extract('/Facility/OnsiteStartDate/text()'));
  lc_vOnsiteEndDate := test_val('/Facility/OnsiteEndDate/text',rec_facilities.facility.extract('/Facility/OnsiteEndDate/text()'));
  lc_vSubmittalDate := test_val('/Facility/SubmittalDate',rec_facilities.facility.extract('/Facility/SubmittalDate/text()'));
  lc_vOverallEngAssRatingASCE := test_val('OverallEngAssmentRatingASCE',rec_facilities.facility.extract('/Facility/ExecutiveTable/OverallEngAssmentRatingASCE/text()'));
  lc_nOverallCIRating := test_num(rec_facilities.facility.extract('/Facility/ExecutiveTable/OverallCIRating/text()'));
  lc_nOveral5YearProjectedCI := test_num(rec_facilities.facility.extract('/Facility/ExecutiveTable/Overall5YearProjectedCI/text()'));
  lc_nOveral10YearProjectedCI := test_num(rec_facilities.facility.extract('/Facility/ExecutiveTable/Overall10YearProjectedCI/text()'));
  lc_vCurrentOperationalRating := test_val('CurrentOperationalRating',rec_facilities.facility.extract('/Facility/ExecutiveTable/CurrentOperationalRating/text()'));
  lc_vOperationalRestrictions := test_val('OperationalRestrictions',rec_facilities.facility.extract('/Facility/ExecutiveTable/OperationalRestrictions/text()'));
  lc_vRepairRecommendations := test_val('RepairRecommendations',rec_facilities.facility.extract('/Facility/ExecutiveTable/RepairRecommendations/text()'));
  lc_vCurFacUsageDescription := test_val('CurrentFacilityUsageDescription',rec_facilities.facility.extract('/Facility/ExecutiveTable/CurrentFacilityUsageDescription/text()'));
  lc_vTotalNumOfPersOnSite := test_val('OnSitePersonnel',rec_facilities.facility.extract('/Facility/ExecutiveTable/TotalNumberOfPersonnelOnSite/text()'));
  lc_vTotalNumberOfDaysOnSite := test_val('DaysOnsite',rec_facilities.facility.extract('/Facility/ExecutiveTable/TotalNumberOfDaysOnSite/text()'));




  lc_cDML := 'update
      spd_t_do_facilities
    set
      spd_t_do_facilities.facility_onsite_start_date = to_date(''' || lc_vOnsiteStartDate || ''',''MM/DD/YYYY''),
      spd_t_do_facilities.facility_onsite_end_date = to_date(''' || lc_vOnsiteEndDate || ''',''MM/DD/YYYY''),
      spd_t_do_facilities.facility_submittal_date = ''' || lc_vSubmittalDate || ''',
      spd_t_do_facilities.ovr_eng_assessment_rating = ''' || lc_vOverallEngAssRatingASCE || ''',
      spd_t_do_facilities.condition_index = ' || lc_nOverallCIRating || ' ,
      spd_t_do_facilities.five_year_proj_ci =  ' || lc_nOveral5YearProjectedCI || ',
      spd_t_do_facilities.ten_year_proj_ci =  ' || lc_nOveral10YearProjectedCI || ',
      spd_t_do_facilities.current_oper_rating = ''' || lc_vCurrentOperationalRating || ''',
      spd_t_do_facilities.operational_restrictions =  ''' || lc_vOperationalRestrictions || ''',
      spd_t_do_facilities.repair_recommendations =  ''' || lc_vRepairRecommendations || ''',
      spd_t_do_facilities.current_fac_usage_description = ''' || lc_vCurFacUsageDescription || ''' ,
      spd_t_do_facilities.onsite_personnel =  ''' || lc_vTotalNumOfPersOnSite || ''',
      spd_t_do_facilities.onsite_days =  ''' || lc_vTotalNumberOfDaysOnSite || '''

    where
      dofid = ' || lc_nDOFID;

  lc_nDML := import_xml_DML(lc_cDML,lc_nADOID,IN_vFILENAME,lc_vImportSectionForDML);
  /*------------END UPDATE FACILITY RECORD-------------------*/

      --p(rec_facilities.facility.extract('/Facility/ExecutiveTable/FACID/text()').getstringval(),3);

  /*------------METADATA OF FACILITY-------------------*/


    select extract(rec_facilities.facility, '/Facility/ExecutiveTable/metadata') into lc_xFacMetaDataNode from dual;
    select count(*) into lc_nNodeCount FROM XMLTable('/metadata/LineItem' passing lc_xFacMetaDataNode);
  lc_vImportSectionForDML :='METADATA OF FACILITY';
  p(in_nAPID=>lc_nAPID, in_nADOID=>IN_nADOID, in_nDOFID=>lc_nDOFID, in_vFilename=>IN_vFILENAME, in_vImportSection=>lc_vImportSectionForDML, in_vNotes=>'MetaData Count: ' || lc_nNodeCount, in_dRunDate=>lc_dDate);


  /*------------LINEITEM OF METADATA-------------------*/
    for rec_facility_metadata in (
    SELECT
      rownum rn,
      xml_table.column_value lineitem
    FROM
      XMLTable('/metadata/LineItem' passing lc_xFacMetaDataNode) xml_table
    )
    loop


    lc_vMetaDisplay := test_val('/LineItem/display/',rec_facility_metadata.lineitem.extract('/LineItem/display/text()'));
    lc_vMetaValue := test_val('/LineItem/value/',rec_facility_metadata.lineitem.extract('/LineItem/value/text()'));

  /*------------CREATE LINEITEM RECORD (spd_t_do_facility_meta_data)-------------------*/
  lc_cDML := 'insert into
        spd_t_do_facility_meta_data
      (
        dofid,
        displayvalue,
        value
      )
      values
      (
        ' || lc_nDOFID || ',
        ''' || lc_vMetaDisplay || ''',
        ''' || lc_vMetaValue || '''
      )';

  lc_nDML := import_xml_DML(lc_cDML,lc_nADOID,IN_vFILENAME,lc_vImportSectionForDML);
  /*------------END CREATE LINEITEM RECORD (spd_t_do_facility_meta_data)-------------------*/

    end loop;
  /*------------END LINEITEM OF METADATA-------------------*/
  /*------------END METADATA OF FACILITY-------------------*/




  /*------------DRAWINGS OF FACILITY-------------------*/

  select extract(rec_facilities.facility, '/Facility/FacilityDrawings') into lc_xFacDrawingsNode from dual;
  select count(*) into lc_nNodeCountFacDrawings FROM XMLTable('/FacilityDrawings/FacilityDrawing' passing lc_xFacDrawingsNode);
  lc_vImportSectionForDML :='DRAWINGS OF FACILITY';
  p(in_nAPID=>lc_nAPID, in_nADOID=>IN_nADOID, in_nDOFID=>lc_nDOFID, in_vFilename=>IN_vFILENAME, in_vImportSection=>lc_vImportSectionForDML, in_vNotes=>'Drawings: ' || lc_nNodeCount, in_dRunDate=>lc_dDate);


  /*------------DRAWING OF DRAWINGS-------------------*/
      lc_nSuccess := 0;
      for rec_fac_drawings in (
          SELECT
            rownum rn,
            xml_table.column_value drawing
          FROM
            XMLTable('/FacilityDrawings/FacilityDrawing' passing lc_xFacDrawingsNode) xml_table
      )
      loop


        lc_vTemp := rec_fac_drawings.drawing.extract('/FacilityDrawing/FileName/text()').getstringval();
        lc_nFolderId := folderid(lc_vTemp, lc_nADOID );
        lc_vFileName := filename(lc_vTemp);


        lc_vDrawingID := test_val('/FacilityDrawing/DrawingID/text()',rec_fac_drawings.drawing.extract('/FacilityDrawing/DrawingID/text()'));
        lc_vNAVFACDrawingNumber := test_val('/FacilityDrawing/NAVFACDrawingnumber/text()',rec_fac_drawings.drawing.extract('/FacilityDrawing/NAVFACDrawingnumber/text()'));
        lc_vDrawingDescription := test_val('/FacilityDrawing/Description/text()',rec_fac_drawings.drawing.extract('/FacilityDrawing/Description/text()'));
        lc_vDrawingType := test_val('/FacilityDrawing/DrawingType/text()',rec_fac_drawings.drawing.extract('/FacilityDrawing/DrawingType/text()'));

  /*------------CREATE DRAWING RECORDS (spd_t_do_fac_supporting_files)-------------------*/
  lc_cDML := 'insert into
          spd_t_do_fac_supporting_files
        (
          dofid,
          local_id,
          NAVFAC_drawing_number,
          caption,
          file_type_string,
          file_type_listid,
          file_name,
          file_section_listid,
          folderid,
          the_bfile
        )
        values
        (
        ' || lc_nDOFID || ',
        ''' || lc_vDrawingID || ''',
        ''' || lc_vNAVFACDrawingNumber || ''',
        ''' || lc_vDrawingDescription || ''',
        ''' || lc_vDrawingType || ''',
        (select listid from spd_t_lists where listname = ''list_delivery_order_file_types'' and displayvalue = ''General''),
        ''' || lc_vFileName || ''',
        (select listid from spd_t_lists where listname = ''list_delivery_order_file_sections'' and displayvalue = ''Drawing Files''),
        ''' || lc_nFolderId || ''',
        bfilename(''SPD_' || lc_nFolderId || ''',''' || lc_vFileName || ''')
        )';

  lc_nDML := import_xml_DML(lc_cDML,lc_nADOID,IN_vFILENAME,lc_vImportSectionForDML);
  /*------------END CREATE DRAWING RECORDS (spd_t_do_fac_supporting_files)-------------------*/
  lc_nSuccess := lc_nSuccess + lc_nDML;
      end loop; --fac drawings
p(in_nAPID=>lc_nAPID, in_nADOID=>IN_nADOID, in_nDOFID=>lc_nDofid, in_vFilename=>IN_vFILENAME, in_vImportSection=>'DRAWINGS OF FACILITY', in_vNotes=>'', in_dRunDate=>lc_dDate, in_nTotalRecords => lc_nNodeCountFacDrawings, in_nCountOfRecords => lc_nSuccess, in_vGrouping => 'DRAWINGS OF FACILITY');
  /*------------END DRAWING OF DRAWINGS-------------------*/
  /*------------END DRAWINGS OF FACILITY-------------------*/



  /*------------SUPPORTING FILES OF FACILITY-------------------*/

    select extract(rec_facilities.facility, '/Facility/FacilitySupportingFiles') into lc_xFacSupportingFilesNode from dual;
    select count(*) into lc_nNodeCountSupFiles FROM XMLTable('/FacilitySupportingFiles/FacilitySupportingFile' passing lc_xFacSupportingFilesNode);
  lc_vImportSectionForDML :='SUPPORTING FILES OF FACILITY';
  p(in_nAPID=>lc_nAPID, in_nADOID=>IN_nADOID, in_nDOFID=>lc_nDOFID, in_vFilename=>IN_vFILENAME, in_vImportSection=>lc_vImportSectionForDML, in_vNotes=>'Supporting Files: ' || lc_nNodeCount, in_dRunDate=>lc_dDate);
  p(in_nAPID=>lc_nAPID, in_nADOID=>IN_nADOID, in_nDOFID=>lc_nDOFID, in_vFilename=>IN_vFILENAME, in_vImportSection=>lc_vImportSectionForDML, in_vNotes=>'Supporting Files: Cost Estimate', in_dRunDate=>lc_dDate);

  /*------------CUSTOM SUPPORTING FILES FOR FACILITY-------------------*/
  --Cost Estimate
    lc_vTemp := test_val('FileName',rec_facilities.facility.extract('/Facility/CostEstimateFilename/text()'));
    lc_nFolderId := folderid(lc_vTemp, lc_nADOID );
    lc_vFileName := filename(lc_vTemp);

  lc_cDML := 'insert into
        spd_t_do_fac_supporting_files
      (
        dofid,
        local_id,
        caption,
        file_type_string,
        file_type_listid,
        file_name,
        file_section_listid,
        folderid,
        the_bfile
      )
      values
      (
        ' || lc_nDOFID || ',
        null,
        ''Repair Cost Estimate'',
        ''General'',
        (select listid from spd_t_lists where listname = ''list_delivery_order_file_types'' and displayvalue = ''General''),
        ''' || lc_vFileName || ''',
        (select listid from spd_t_lists where listname = ''list_delivery_order_file_sections'' and displayvalue = ''Supporting Files''),
        ''' || lc_nFolderId || ''',
        bfilename(''SPD_' || lc_nFolderId || ''',''' || lc_vFileName || ''')
      )';

  lc_nDML := import_xml_DML(lc_cDML,lc_nADOID,IN_vFILENAME,lc_vImportSectionForDML);
p(in_nAPID=>lc_nAPID, in_nADOID=>IN_nADOID, in_nDOFID=>lc_nDOFID, in_vFilename=>IN_vFILENAME, in_vImportSection=>lc_vImportSectionForDML, in_vNotes=>'', in_dRunDate=>lc_dDate, in_nTotalRecords => 1, in_nCountOfRecords => lc_nDML, in_vGrouping => 'Repair Cost Estimate File');


  --DD1391
  p(in_nAPID=>lc_nAPID, in_nADOID=>IN_nADOID, in_nDOFID=>lc_nDOFID, in_vFilename=>IN_vFILENAME, in_vImportSection=>lc_vImportSectionForDML, in_vNotes=>'Supporting Files: 1391', in_dRunDate=>lc_dDate);

  lc_vTemp := test_val('FileName',rec_facilities.facility.extract('/Facility/DD1391FileName/text()'));
  lc_nFolderId := folderid(lc_vTemp, lc_nADOID );
  lc_vFileName := filename(lc_vTemp);


  lc_cDML := 'insert into
        spd_t_do_fac_supporting_files
      (
        dofid,
        local_id,
        caption,
        file_type_string,
        file_type_listid,
        file_name,
        file_section_listid,
        folderid,
        the_bfile
      )
      values
      (
        ' || lc_nDOFID || ',
        null,
        ''DD1391'',
        ''General'',
        (select listid from spd_t_lists where listname = ''list_delivery_order_file_types'' and displayvalue = ''General''),
        ''' || lc_vFileName || ''',
        (select listid from spd_t_lists where listname = ''list_delivery_order_file_sections'' and displayvalue = ''Supporting Files''),
        ''' || lc_nFolderId || ''',
        bfilename(''SPD_' || lc_nFolderId || ''',''' || lc_vFileName || ''')
      )';

  lc_nDML := import_xml_DML(lc_cDML,lc_nADOID,IN_vFILENAME,lc_vImportSectionForDML);
p(in_nAPID=>lc_nAPID, in_nADOID=>IN_nADOID, in_nDOFID=>lc_nDOFID, in_vFilename=>IN_vFILENAME, in_vImportSection=>lc_vImportSectionForDML, in_vNotes=>'', in_dRunDate=>lc_dDate, in_nTotalRecords => 1, in_nCountOfRecords => lc_nDML, in_vGrouping => 'DD1391 File');

  --FS File

  lc_vTemp := test_val('FileName',rec_facilities.facility.extract('/Facility/FSFileName/text()'));
  lc_nFolderId := folderid(lc_vTemp, lc_nADOID,0, 'FS' );
  lc_vFileName := filename(lc_vTemp);

  lc_cDML := 'insert into
        spd_t_do_fac_supporting_files
      (
        dofid,
        local_id,
        caption,
        file_type_string,
        file_type_listid,
        file_name,
        file_section_listid,
        folderid,
        the_bfile
      )
      values
      (
        ' || lc_nDOFID || ',
        null,
        ''Facility Summary'',
        ''General'',
        (select listid from spd_t_lists where listname = ''list_delivery_order_file_types'' and displayvalue = ''General''),
        ''' || lc_vFileName || ''',
        (select listid from spd_t_lists where listname = ''list_delivery_order_file_sections'' and displayvalue = ''Supporting Files''),
        ''' || lc_nFolderId || ''',
        bfilename(''SPD_' || lc_nFolderId || ''',''' || lc_vFileName || ''')
      )';

  lc_nDML := import_xml_DML(lc_cDML,lc_nADOID,IN_vFILENAME,lc_vImportSectionForDML);
p(in_nAPID=>lc_nAPID, in_nADOID=>IN_nADOID, in_nDOFID=>lc_nDOFID, in_vFilename=>IN_vFILENAME, in_vImportSection=>lc_vImportSectionForDML, in_vNotes=>'', in_dRunDate=>lc_dDate, in_nTotalRecords => 1, in_nCountOfRecords => lc_nDML, in_vGrouping => 'Facility Summary File: ' || lc_vMXLFacilityName);

  /*------------END CUSTOM SUPPORTING FILES FOR FILES-------------------*/


  /*------------SUPPORTING FILE OF SUPPORTING FILES-------------------*/
    lc_nSuccess := 0;
    for rec_supporting_files in (
    SELECT
      rownum rn,
      xml_table.column_value supportingfile
    FROM
      XMLTable('/FacilitySupportingFiles/FacilitySupportingFile' passing lc_xFacSupportingFilesNode) xml_table
    )
    loop

    lc_vTemp := test_val('FileName',rec_supporting_files.supportingfile.extract('/FacilitySupportingFile/FileName/text()'));
    lc_nFolderId := folderid(lc_vTemp, lc_nADOID );
    lc_vFileName := filename(lc_vTemp);
    --p(in_nAPID=>lc_nAPID, in_nADOID=>IN_nADOID, in_nDOFID=>lc_nDOFID, in_vFilename=>IN_vFILENAME, in_vImportSection=>'SUPPORTING FILE ' || rec_supporting_files.rn ||' OF ' || xLength(lc_xFacSupportingFilesNode) || ' SUPPORTING FILES', in_vNotes=>lc_vTemp, in_dRunDate=>lc_dDate);


    lc_vSupportingFileID := test_val('SupportingFileID',rec_supporting_files.supportingfile.extract('/SupportingFile/SupportingFileID/text()'));
    lc_vSupportingFileDescription := test_val('SupportingFile/Description',rec_supporting_files.supportingfile.extract('/SupportingFile/Description/text()'));


  /*------------CREATE SUPPORTING FILE RECORD (spd_t_do_fac_supporting_files)-------------------*/
  lc_cDML := 'insert into
      spd_t_do_fac_supporting_files
    (
      dofid,
      local_id,
      caption,
      file_type_string,
      file_type_listid,
      file_name,
      file_section_listid,
      folderid,
      the_bfile
    )
    values
    (
      ' || lc_nDOFID || ',
      ''' || lc_vSupportingFileID || ''',
      ''' || lc_vSupportingFileDescription || ''',
      ''General'',
      (select listid from spd_t_lists where listname = ''list_delivery_order_file_types'' and displayvalue = ''General''),
      ''' || lc_vFileName || ''',
      (select listid from spd_t_lists where listname = ''list_delivery_order_file_sections'' and displayvalue = ''Supporting Files''),
      ''' || lc_nFolderId || ''',
      bfilename(''SPD_' || lc_nFolderId || ''',''' || lc_vFileName || ''')
    )';

  lc_nDML := import_xml_DML(lc_cDML,lc_nADOID,IN_vFILENAME,lc_vImportSectionForDML);

  /*------------END CREATE SUPPORTING FILE RECORD (spd_t_do_supporting_files)-------------------*/
  lc_nSuccess := lc_nSuccess + lc_nDML;
    end loop;
p(in_nAPID=>lc_nAPID, in_nADOID=>IN_nADOID, in_nDOFID=>lc_nDOFID, in_vFilename=>IN_vFILENAME, in_vImportSection=>lc_vImportSectionForDML, in_vNotes=>'this total count may need to be reviewed, as we have custom files and regulars in a single node', in_dRunDate=>lc_dDate, in_nTotalRecords => lc_nNodeCountSupFiles, in_nCountOfRecords => lc_nSuccess, in_vGrouping => 'Facility Supporting Files');

  /*------------END SUPPORTING FILE OF SUPPORTING FILES-------------------*/
  /*------------END SUPPORTING FILES OF FACILITY-------------------*/


























  /*------------ASSET SUMMARY OF FACILITY-------------------*/

    select extract(rec_facilities.facility, '/Facility/AssetSummary') into lc_xAssetSummaryNode from dual;
    select count(*) into lc_nNodeCountAssetSummary FROM XMLTable('/AssetSummary/AssetSummaryLineItem' passing lc_xAssetSummaryNode);
  lc_vImportSectionForDML :='ASSET SUMMARY';
  p(in_nAPID=>lc_nAPID, in_nADOID=>IN_nADOID, in_nDOFID=>lc_nDOFID, in_vFilename=>IN_vFILENAME, in_vImportSection=>lc_vImportSectionForDML, in_vNotes=>'Asset Summary count: ' || lc_nNodeCountAssetSummary, in_dRunDate=>lc_dDate);



  /*------------ASSET SUMMARY LINE ITEM OF ASSET SUMMARY-------------------*/
    lc_nSuccess := 0;
    for rec_facility_asset_summary in (
    SELECT
      rownum rn,
      xml_table.column_value as_lineitem
    FROM
      XMLTable('/AssetSummary/AssetSummaryLineItem' passing lc_xAssetSummaryNode) xml_table
    )
    loop

  p(in_nAPID=>lc_nAPID, in_nADOID=>IN_nADOID, in_nDOFID=>lc_nDOFID, in_vFilename=>IN_vFILENAME, in_vImportSection=>'Asset Summary: ' || rec_facility_asset_summary.rn, in_vNotes=>'BEGIN', in_dRunDate=>lc_dDate);

     --if there is no Uniformat node, we dont want to process this at all
    if rec_facility_asset_summary.as_lineitem.existsNode('/AssetSummaryLineItem/UniFormatIICode') > 0 then

      lc_vString := replace(rec_facility_asset_summary.as_lineitem.extract('/AssetSummaryLineItem/UniFormatIICode/text()').getstringval(),'.');

      select spd_s_do_fac_asset_inventory.nextval into lc_nAIID from dual;

      lc_nUFIIExists := 0;



  /*------------UNIFORMAT II-------------------*/
      --this looks for an exact match of uniformat codes, hardcoded WF Productline listid
      for rec_ufii in (

          select listid from spd_t_lists list_inner where list_inner.listname = 'list_uniformat_II_codes' and list_inner.productline_listid = 100085 and list_inner.displayvalue = lc_vString
          --union
          --select listid from spd_t_lists_for_review list_inner where list_inner.listname = 'list_uniformat_II_codes' and list_inner.productline_listid = 100085 and list_inner.displayvalue = lc_vString

      )
      loop

        lc_nListid := rec_ufii.listid;
        lc_nUFIIExists := 1;
      end loop;



      if lc_nUFIIExists = 0 then

        --we need to add this to the for_review table
        --old way, mr: select spd_s_lists_for_review.nextval into lc_nNewListID from dual;

        --old way, mr: insert into spd_t_lists_for_review (listid, displayvalue, returnvalue, grouping, listname, visualorder, productline_listid, notes) values (lc_nNewListID,lc_vString,rec_facility_asset_summary.as_lineitem.extract('/AssetSummaryLineItem/Description/text()').getstringval(),'','list_uniformat_II_codes',1,100085,'User Defined');

        --add user defined codes directly to the spd_t_lists, this is only for the historical reports, as the new ones are all validated
        select spd_s_lists.nextval into lc_nNewListID from dual;
        insert into spd_t_lists (listid, displayvalue, returnvalue, grouping, listname, visualorder, productline_listid, notes) values (lc_nNewListID,lc_vString,rec_facility_asset_summary.as_lineitem.extract('/AssetSummaryLineItem/Description/text()').getstringval(),'','list_uniformat_II_codes',1,100085,'User Defined');

        commit;
        --p('added user defined ' || lc_vString || ':' || rec_facility_asset_summary.as_lineitem.extract('/AssetSummaryLineItem/Description/text()').getstringval(),1);
      end if;

      lc_nListID2 := 0;

      --this looks for the parent record of ufii
      for rec in (

        select listid from spd_t_lists list_inner where list_inner.listname = 'list_uniformat_II_codes' and list_inner.productline_listid = 100085 and list_inner.displayvalue = substr(lc_vString, 1, length(lc_vString)-2)
        --union
        --select listid from spd_t_lists_for_review list_inner where list_inner.listname = 'list_uniformat_II_codes' and list_inner.productline_listid = 100085 and list_inner.displayvalue = substr(lc_vString, 1, length(lc_vString)-2)

      )
      loop
        lc_nListID2 := rec.listid;
      end loop;




      if lc_nListID2 = 0 then
        --we need to add this to the for_review table
        --old way, mr: select spd_s_lists_for_review.nextval into lc_nNewListID from dual;
        --old way, mr: insert into spd_t_lists_for_review (displayvalue, returnvalue, grouping, listname, visualorder, productline_listid, notes) values (substr(lc_vString, 1, length(lc_vString)-2), rec_facility_asset_summary.as_lineitem.extract('/AssetSummaryLineItem/Description/text()').getstringval(),'','list_uniformat_II_codes',1,100085,'User Defined (Parent)');
        --add user defined codes directly to the spd_t_lists, this is only for the historical reports, as the new ones are all validated
        select spd_s_lists.nextval into lc_nNewListID from dual;
        insert into spd_t_lists (displayvalue, returnvalue, grouping, listname, visualorder, productline_listid, notes) values (substr(lc_vString, 1, length(lc_vString)-2), rec_facility_asset_summary.as_lineitem.extract('/AssetSummaryLineItem/Description/text()').getstringval(),'','list_uniformat_II_codes',1,100085,'User Defined (Parent)');


        commit;
        --p('added user defined parent ' || lc_vString || ':' || rec_facility_asset_summary.as_lineitem.extract('/AssetSummaryLineItem/Description/text()').getstringval(),1);
      end if;

      lc_nAIID_parent := lc_nAIID;
      for rec_parent in (
      select aiid from spd_t_do_fac_asset_inventory where dofid = lc_nDOFID and code_listid = lc_nListID2
      )
      loop
        lc_nAIID_parent := rec_parent.aiid;
      end loop;
  /*------------END UNIFORMAT II-------------------*/






  /*------------CREATE ASSET INVENTORY TABLE RECORDS WITH ASSET SUMMARY INFO (spd_t_do_fac_asset_inventory)-------------------*/
      --p(in_nAPID=>lc_nAPID, in_nADOID=>IN_nADOID, in_nDOFID=>lc_nDOFID, in_vFilename=>IN_vFILENAME, in_vImportSection=>'INSERT ASSET INVENTORY RECORD (ASSET SUMMARY VERSION)', in_vNotes=>rec_facility_asset_summary.rn || ': UFII_ASSET_TYPE' || test_val('asset_type v',rec_facility_asset_summary.as_lineitem.extract('/AssetSummaryLineItem/AssetType/text()')), in_dRunDate=>lc_dDate);

      lc_vAssetType := test_val('asset_type v',rec_facility_asset_summary.as_lineitem.extract('/AssetSummaryLineItem/AssetType/text()'));
      lc_vFacilitySection := test_val('facility_section v',rec_facility_asset_summary.as_lineitem.extract('/AssetSummaryLineItem/FacilitySection/text()'));
      lc_vCriticalityFactor := test_val('criticality_factor v',rec_facility_asset_summary.as_lineitem.extract('/AssetSummaryLineItem/CriticalityFactor/text()'));
      lc_vNoOfAssetsCount := test_val('count_of_assets v',rec_facility_asset_summary.as_lineitem.extract('/AssetSummaryLineItem/NoofAssetsCount/text()'));
      lc_vQuantityOfUnits := test_val('quantity_of_units v',rec_facility_asset_summary.as_lineitem.extract('/AssetSummaryLineItem/QuantityofUnits/text()'));
      lc_vUnitType := test_val('unit_type v',rec_facility_asset_summary.as_lineitem.extract('/AssetSummaryLineItem/UnitType/text()'));
      lc_vCurrentCI := test_val('condition_index v',rec_facility_asset_summary.as_lineitem.extract('/AssetSummaryLineItem/CurrentCI/text()'));
      lc_nApproxRepairCost := test_num(rec_facility_asset_summary.as_lineitem.extract('/AssetSummaryLineItem/ApproximateRepairCosts/text()'));
      lc_vEstimatedResultingCI := test_val('est_repair_condition_index',rec_facility_asset_summary.as_lineitem.extract('/AssetSummaryLineItem/EstimatedResultingCI/text()'));
      lc_vMaterial := test_val('material v',rec_facility_asset_summary.as_lineitem.extract('/AssetSummaryLineItem/Material/text()'));
      lc_vMaterialDescription := test_val('material_description',rec_facility_asset_summary.as_lineitem.extract('/AssetSummaryLineItem/MaterialDescription/text()'));



  lc_cDML := 'insert into
        spd_t_do_fac_asset_inventory
      (
        aiid,
        dofid,
        parent_aiid,
        asset_type,
        facility_section,
        criticality_factor,
        count_of_assets,
        quantity_of_units,
        unit_type,
        condition_index,
        repair_cost_estimate,
        est_repair_condition_index,
        code_type_listid,
        code_listid,
        material,
        material_description
      )
      values
      (
        ' || lc_nAIID || ',
        ' || lc_nDOFID || ',
        ' || lc_nAIID_parent || ',
        ''' || lc_vAssetType || ''',
        ''' || lc_vFacilitySection || ''',
        ''' || lc_vCriticalityFactor || ''',
        ''' || lc_vNoOfAssetsCount || ''',
        ''' || lc_vQuantityOfUnits || ''',
        ''' || lc_vUnitType || ''',
        ''' || lc_vCurrentCI || ''',
        ' || lc_nApproxRepairCost || ',
        ''' || lc_vEstimatedResultingCI || ''',
        100001,
        ' || lc_nListid || ',
        ''' || lc_vMaterial || ''',
        ''' || lc_vMaterialDescription || '''
      )';

  lc_nDML := import_xml_DML(lc_cDML,lc_nADOID,IN_vFILENAME,lc_vImportSectionForDML);
  /*------------END CREATE ASSET INVENTORY TABLE RECORDS WITH ASSET SUMMARY INFO (spd_t_do_fac_asset_inventory)-------------------*/








  /*
  Facility/AssetSummary/AssetSummaryLineItem/UniFormatIICode


  Facility/FindingsRecommendations/FindingRecommendation/UniFormatIICode

  */









  /*------------Asset Inventory-------------------*/
  select extract(rec_facility_asset_summary.as_lineitem, '/AssetSummaryLineItem/AssetInventory') into lc_xAssetInventoryNode from dual;
  select count(*) into lc_nNodeCountAssetInventory FROM XMLTable('/AssetInventory/AssetInventoryLineItem' passing lc_xAssetInventoryNode);
  lc_vImportSectionForDML :='ASSET INVENTORY';


  /*------------Asset Inventory Line Items-------------------*/
      for rec_asset_inventory in (
          SELECT
            rownum rn,
            xml_table.column_value asset_inventory_line_item
          FROM
            XMLTable('/AssetInventory/AssetInventoryLineItem' passing lc_xAssetInventoryNode) xml_table
      )
      loop

  lc_nAIID_Parent := lc_nAIID;

  select spd_s_do_fac_asset_inventory.nextval into lc_nAIID from dual;

  /*------------CREATE ASSET INVENTORY TABLE RECORDS WITH ASSET INVENTORY INFO (spd_t_do_fac_asset_inventory)-------------------*/
  --p(in_nAPID=>lc_nAPID, in_nADOID=>IN_nADOID, in_nDOFID=>lc_nDOFID, in_vFilename=>IN_vFILENAME, in_vImportSection=>'INSERT ASSET INVENTORY RECORD PROPER', in_vNotes=>rec_asset_inventory.rn || ':' || test_val('asset_type v',rec_asset_inventory.asset_inventory_line_item.extract('/AssetInventoryLineItem/AssetType/text()')), in_dRunDate=>lc_dDate);


      lc_vAssetType := test_val('asset_type v',rec_asset_inventory.asset_inventory_line_item.extract('/AssetInventoryLineItem/AssetType/text()'));
      lc_vFacilitySection := test_val('facility_section v',rec_asset_inventory.asset_inventory_line_item.extract('/AssetInventoryLineItem/FacilitySection/text()'));
      lc_vCriticalityFactor := test_val('criticality_factor v',rec_asset_inventory.asset_inventory_line_item.extract('/AssetInventoryLineItem/CriticalityFactor/text()'));
      lc_vNoOfAssetsCount := test_val('count_of_assets v',rec_asset_inventory.asset_inventory_line_item.extract('/AssetInventoryLineItem/NoofAssetsCount/text()'));
      lc_vQuantityOfUnits := test_val('quantity_of_units v',rec_asset_inventory.asset_inventory_line_item.extract('/AssetInventoryLineItem/QuantityofUnits/text()'));
      lc_vUnitType := test_val('unit_type v',rec_asset_inventory.asset_inventory_line_item.extract('/AssetInventoryLineItem/UnitType/text()'));
      lc_vCurrentCI := test_val('condition_index v',rec_asset_inventory.asset_inventory_line_item.extract('/AssetInventoryLineItem/CurrentCI/text()'));
      lc_nApproxRepairCost := test_num(rec_asset_inventory.asset_inventory_line_item.extract('/AssetInventoryLineItem/ApproximateRepairCosts/text()'));
      lc_vEstimatedResultingCI := test_val('est_repair_condition_index',rec_asset_inventory.asset_inventory_line_item.extract('/AssetInventoryLineItem/EstimatedResultingCI/text()'));
      lc_vMaterial := test_val('material v',rec_asset_inventory.asset_inventory_line_item.extract('/AssetInventoryLineItem/Material/text()'));
      lc_vMaterialDescription := test_val('material_description',rec_asset_inventory.asset_inventory_line_item.extract('/AssetInventoryLineItem/MaterialDescription/text()'));


  lc_cDML := 'insert into
        spd_t_do_fac_asset_inventory
      (
        aiid,
        dofid,
        parent_aiid,
        asset_type,
        facility_section,
        criticality_factor,
        count_of_assets,
        quantity_of_units,
        unit_type,
        condition_index,
        repair_cost_estimate,
        est_repair_condition_index,
        code_type_listid,
        code_listid,
        material,
        material_description
      )
      values
      (
        ' || lc_nAIID || ',
        ' || lc_nDOFID || ',
        ' || lc_nAIID_parent || ',
        ''' || lc_vAssetType || ''',
        ''' || lc_vFacilitySection || ''',
        ''' || lc_vCriticalityFactor || ''',
        ''' || lc_vNoOfAssetsCount || ''',
        ''' || lc_vQuantityOfUnits || ''',
        ''' || lc_vUnitType || ''',
        ''' || lc_vCurrentCI || ''',
        ' || lc_nApproxRepairCost || ',
        ''' || lc_vEstimatedResultingCI || ''',
        100001,
        ' || lc_nListid || ',
        ''' || lc_vMaterial || ''',
        ''' || lc_vMaterialDescription || '''
      )';

  lc_nDML := import_xml_DML(lc_cDML,lc_nADOID,IN_vFILENAME,lc_vImportSectionForDML);
  /*------------END CREATE ASSET INVENTORY TABLE RECORDS WITH ASSET SUMMARY INFO (spd_t_do_fac_asset_inventory)-------------------*/
  p(in_nAPID=>lc_nAPID, in_nADOID=>IN_nADOID, in_nDOFID=>lc_nDOFID, in_vFilename=>IN_vFILENAME, in_vImportSection=>'Asset Type: ' || lc_vAssetType, in_vNotes=>'Processed ' || rec_asset_inventory.rn || ' of ' || lc_nNodeCountAssetInventory || ' Asset Types', in_dRunDate=>lc_dDate);



  /*------------METADATA OF ASSET INVENTORY-------------------*/
    select extract(rec_asset_inventory.asset_inventory_line_item, '/AssetInventoryLineItem/Metadata') into lc_xAIMetaDataNode from dual;
    --p(in_nAPID=>lc_nAPID, in_nADOID=>IN_nADOID, in_nDOFID=>lc_nDOFID, in_vFilename=>IN_vFILENAME, in_vImportSection=>'ASSET INVENTORY METADATA', in_vNotes=>xLength(lc_xAIMetaDataNode), in_dRunDate=>lc_dDate);


  /*------------LINEITEM OF METADATA-------------------*/
    for rec_ai_metadata in (
    SELECT
      rownum rn,
      xml_table.column_value lineitem
    FROM
      XMLTable('/Metadata/LineItem' passing lc_xAIMetaDataNode) xml_table
    )
    loop

    lc_vMetaDisplay := test_val('/LineItem/display/',rec_ai_metadata.lineitem.extract('/LineItem/display/text()'));
    lc_vMetaValue := test_val('/LineItem/value/',rec_ai_metadata.lineitem.extract('/LineItem/value/text()'));

  /*------------CREATE LINEITEM RECORD (spd_t_do_facility_meta_data)-------------------*/
  lc_cDML := 'insert into
        spd_t_ai_meta_data
      (
        aiid,
        displayvalue,
        value
      )
      values
      (
        ' || lc_nAIID || ',
        ''' || lc_vMetaDisplay || ''',
        ''' || lc_vMetaValue || '''
      )';

  lc_nDML := import_xml_DML(lc_cDML,lc_nADOID,IN_vFILENAME,lc_vImportSectionForDML);

  /*------------END CREATE LINEITEM RECORD (spd_t_do_facility_meta_data)-------------------*/

    end loop;
  /*------------END LINEITEM OF METADATA-------------------*/
  /*------------END METADATA OF FACILITY-------------------*/




  /*------------DEFECTS NODE OF ASSET INVENTORY LINE ITEM-------------------*/


      select extract(rec_asset_inventory.asset_inventory_line_item, '/AssetInventoryLineItem/Defects') into lc_xDefectsNode from dual;
      select count(*) into lc_nNodeCountAIDefects FROM XMLTable('/Defects/Defect' passing lc_xDefectsNode);
  lc_vImportSectionForDML :='DEFECTS OF ASSET INVENTORY';
  p(in_nAPID=>lc_nAPID, in_nADOID=>IN_nADOID, in_nDOFID=>lc_nDOFID, in_vFilename=>IN_vFILENAME, in_vImportSection=>lc_vImportSectionForDML, in_vNotes=>lc_vAssetType || ' Defects count: ' || lc_nNodeCountAIDefects, in_dRunDate=>lc_dDate);





  /*------------DEFECT NODE OF DEFECTS-------------------*/
      lc_nSuccessDefects := 0;
      for rec_defects in (
      SELECT
        xml_table.column_value defect,
        rownum rn
      FROM
        XMLTable('/Defects/Defect' passing lc_xDefectsNode) xml_table
      )
      loop




  /*------------CREATE DEFECT RECORDS (spd_t_ai_defects)-------------------*/
          select spd_s_ai_defects.nextval into lc_nAIDEFECTID from dual;

          lc_vDefectIDNo := test_val('/Defect/DefectIDNo/text()',rec_defects.defect.extract('/Defect/DefectIDNo/text()'));
          lc_DefectLoction := test_val('/Defect/DefectLocation/text()',rec_defects.defect.extract('/Defect/DefectLocation/text()'));
          lc_vDefectCenter := test_val('/Defect/DefectCenter/text()',rec_defects.defect.extract('/Defect/DefectCenter/text()'));
          lc_vCriticalityFactor := test_val('/Defect/CriticalityFactor/text()',rec_defects.defect.extract('/Defect/CriticalityFactor/text()'));
          lc_vDefectType := test_val('/Defect/DefectType/text()',rec_defects.defect.extract('/Defect/DefectType/text()'));
          lc_vSRMClassification := test_val('/Defect/SRMClassification/text()',rec_defects.defect.extract('/Defect/SRMClassification/text()'));

        --p(rec_ai_find_recs.ai_find_rec.extract('/Defect/Findings/text()').getstringval());
  lc_cDML := 'insert into
          spd_t_ai_defects
        (
          aidefectid,
          aiid,
          local_id,
          defect_location,
          defect_center,
          criticality_factor,
          defect_type_string,
          SRM_Classification
        )
        values
        (
          ' || lc_nAIDEFECTID || ',
          ' ||lc_nAIID || ',
          ''' || lc_vDefectIDNo || ''',
          ''' || lc_DefectLoction || ''',
          ''' || lc_vDefectCenter || ''',
          ''' || lc_vCriticalityFactor || ''',
          ''' || lc_vDefectType || ''',
          ''' || lc_vSRMClassification || '''
        )';

  lc_nDML := import_xml_DML(lc_cDML,lc_nADOID,IN_vFILENAME,lc_vImportSectionForDML || '_ai_defects_' || rec_defects.rn);

  /*------------END CREATE DEFECT RECORDS (spd_t_ai_defects)-------------------*/






  /*------------DEFECT METADATA OF DEFECT-------------------*/
        /*add the defect metadata records*/
        select extract(rec_defects.defect, '/Defect/metadata') into lc_xDefectMetaDataNode from dual;


        for rec_defect_metadata in (
        SELECT
          rownum rn,
          xml_table.column_value lineitem
        FROM
          XMLTable('/metadata/LineItem' passing lc_xDefectMetaDataNode) xml_table
        )
        loop


  /*------------CREATE DEFECT META DATA RECORDS (spd_t_ai_defect_meta_data)-------------------*/
  lc_cDML := 'insert into
        spd_t_ai_defect_meta_data
      (
        aidefectid,
        displayvalue,
        value
      )
      values
      (
        ' || lc_nAIDEFECTID || ',
        ''' || lc_vMetaDisplay || ''',
        ''' || lc_vMetaValue || '''
      )';

  lc_nDML_inner := import_xml_DML(lc_cDML,lc_nADOID,IN_vFILENAME,lc_vImportSectionForDML || '_defect_metadata_' || rec_defect_metadata.rn);
  /*------------END CREATE DEFECT META DATA RECORDS (spd_t_ai_defect_meta_data)-------------------*/





        end loop;
  /*------------END DEFECT METADATA OF DEFECT-------------------*/
  p(in_nAPID=>lc_nAPID, in_nADOID=>IN_nADOID, in_nDOFID=>lc_nDOFID, in_vFilename=>IN_vFILENAME, in_vImportSection=>'DEFECT ' || lc_vDefectType || ' of ' || lc_vAssetType, in_vNotes=>'Processed ' || rec_defects.rn || ' of ' || lc_nNodeCountAIDefects, in_dRunDate=>lc_dDate);


/* this is a cheesy way of picking the numerator, there should be something more robust that checks each record of the metadata, but thats for tomorrow */
  if lc_nDML + lc_nDML_inner > 1 then
    lc_nSuccessDefects := lc_nSuccessDefects + 1;
  else
    lc_nSuccessDefects := lc_nSuccessDefects;
  end if;


end loop;
p(in_nAPID=>lc_nAPID, in_nADOID=>IN_nADOID, in_nDOFID=>lc_nDOFID, in_vFilename=>IN_vFILENAME, in_vImportSection=>'Asset Inventory Defects', in_vNotes=>lc_vAssetType, in_dRunDate=>lc_dDate, in_nTotalRecords => lc_nNodeCountAIDefects, in_nCountOfRecords => lc_nSuccessDefects, in_vGrouping => 'Asset Inventory Defects (' || lc_vAssetType || ')');


  /*------------END DEFECT NODE OF DEFECTS-------------------*/
  /*------------END DEFECTS NODE OF ASSET INVENTORY LINE ITEM-------------------*/













  /*------------MOORING FITTINGS NODE OF ASSET INVENTORY LINE ITEM-------------------*/
      --mooring fittings is an option node, test if it exists before continuing
      if rec_asset_inventory.asset_inventory_line_item.existsNode('/AssetInventoryLineItem/MooringFittings') > 0 then

      select extract(rec_asset_inventory.asset_inventory_line_item, '/AssetInventoryLineItem/MooringFittings') into lc_xMooringFittingsNode from dual;

      select count(*) into lc_nNodeCountMoorings FROM XMLTable('/MooringFittings/MooringFitting' passing lc_xMooringFittingsNode);
  lc_vImportSectionForDML :='MOORING FITTINGS';
  p(in_nAPID=>lc_nAPID, in_nADOID=>IN_nADOID, in_nDOFID=>lc_nDOFID, in_vFilename=>IN_vFILENAME, in_vImportSection=>lc_vImportSectionForDML || ' of ' || lc_vAssetType, in_vNotes=>'Moorings count: ' || lc_nNodeCountMoorings, in_dRunDate=>lc_dDate);


--dbms_output.put_line('MOORING FITTINGS ' || lc_nNodeCountMoorings);

  /*------------MOORING FITTING NODE OF MOORING FITTINGS-------------------*/
      lc_nSuccess := 0;


      for rec_mooring_fittings in (
      SELECT
        rownum rn,
        xml_table.column_value mooring_fitting
      FROM
        XMLTable('/MooringFittings/MooringFitting' passing lc_xMooringFittingsNode) xml_table
      )
      loop

dbms_output.put_line('before before MOORING FITTINGS ' || rec_mooring_fittings.rn || ' : ' || lc_vAssetType || ' : ' || lc_vFileName);

            --p(DBMS_LOB.SUBSTR(rec_mooring_fittings.lineitem.getClobVal() , 100, 1),99);
            --need to figure out what to do with the filepath for the image
            --should have the aiid from the loop



            lc_vTemp := test_val('temp',rec_mooring_fittings.mooring_fitting.extract('/MooringFitting/PhotoFileName/text()'));
            dbms_output.put_line('temp ' || rec_mooring_fittings.rn || ' : ' || lc_vAssetType || ' : ' || lc_vTemp);

            if lc_vTemp is not null then
              dbms_output.put_line('if lc_vTemp is not null ' || rec_mooring_fittings.rn || ' : ' || lc_vAssetType || ' : ' || lc_vTemp);

              lc_vTemp := rec_mooring_fittings.mooring_fitting.extract('/MooringFitting/PhotoFileName/text()').getstringval();
            end if;
            dbms_output.put_line('after lc_vTemp is not null ' || rec_mooring_fittings.rn || ' : ' || lc_vAssetType || ' : ' || lc_vTemp);

            lc_nFolderId := folderid(lc_vTemp, lc_nADOID );
            lc_vFileName := filename(lc_vTemp);
            lc_nThumbnailid := getthumbnailid(lc_nFolderId, lc_vFileName,IN_vProcessImages,IN_nADOID);
            dbms_output.put_line('temp ' || rec_mooring_fittings.rn || ' : ' || lc_vAssetType || ' : ' || lc_vTemp);



            lc_vAssetType := test_val('/MooringFitting/AssetType/',rec_mooring_fittings.mooring_fitting.extract('/MooringFitting/AssetType/text()'));
            dbms_output.put_line('lc_vAssetType ' || rec_mooring_fittings.rn || ' : ' || lc_vAssetType || ' : ' || lc_vAssetType);

            lc_vFittingIDNumber := test_val('/MooringFitting/FittingIDNumber/',rec_mooring_fittings.mooring_fitting.extract('/MooringFitting/FittingIDNumber/text()'));
            dbms_output.put_line('lc_vFittingIDNumber ' || rec_mooring_fittings.rn || ' : ' || lc_vAssetType || ' : ' || lc_vFittingIDNumber);

            lc_vMooringType := test_val('/MooringFitting/Type/',rec_mooring_fittings.mooring_fitting.extract('/MooringFitting/Type/text()'));
            dbms_output.put_line('lc_vMooringType ' || rec_mooring_fittings.rn || ' : ' || lc_vAssetType || ' : ' || lc_vMooringType);

            lc_vDesignCapacity := test_val('/MooringFitting/DesignCapacity/',rec_mooring_fittings.mooring_fitting.extract('/MooringFitting/DesignCapacity/text()'));
            dbms_output.put_line('lc_vDesignCapacity ' || rec_mooring_fittings.rn || ' : ' || lc_vAssetType || ' : ' || lc_vDesignCapacity);

            lc_vRatingCapacity := test_val('/MooringFitting/RatingCapacity/',rec_mooring_fittings.mooring_fitting.extract('/MooringFitting/RatingCapacity/text()'));
            dbms_output.put_line('lc_vRatingCapacity ' || rec_mooring_fittings.rn || ' : ' || lc_vAssetType || ' : ' || lc_vRatingCapacity);

            lc_vConditionFitting := test_val('/MooringFitting/ConditionFitting/',rec_mooring_fittings.mooring_fitting.extract('/MooringFitting/ConditionFitting/text()'));
            dbms_output.put_line('lc_vConditionFitting ' || rec_mooring_fittings.rn || ' : ' || lc_vAssetType || ' : ' || lc_vConditionFitting);

            lc_vConditionBase := test_val('/MooringFitting/ConditionBase/',rec_mooring_fittings.mooring_fitting.extract('/MooringFitting/ConditionBase/text()'));
            dbms_output.put_line('lc_vConditionBase ' || rec_mooring_fittings.rn || ' : ' || lc_vAssetType || ' : ' || lc_vConditionBase);



  /*------------CREATE MOORING FITTING RECORDS (spd_t_do_fac_mooring_fittings)-------------------*/

  lc_cDML := 'insert into
              spd_t_do_fac_mooring_fittings
            (
              aiid,
              asset_type_name,
              fitting_id_number,
              fitting_type,
              design_capacity,
              rating_capacity,
              condition_fitting,
              condition_base,
              photo_filename,
              folderid,

              thumbnailid
            )
            values
            (
              ' || lc_nAIID || ',
              ''' || lc_vAssetType || ''',
              ''' || lc_vFittingIDNumber || ''',
              ''' || lc_vMooringType || ''',
              ''' || lc_vDesignCapacity || ''',
              ''' || lc_vRatingCapacity || ''',
              ''' || lc_vConditionFitting || ''',
              ''' || lc_vConditionBase || ''',
              ''' || lc_vTemp || ''',
              ' || lc_nFolderId || ',

              ' || lc_nThumbnailid || '
            )';



            --the_bfile,
            --bfilename(''SPD_' || lc_nFolderId || ''',''' || lc_vFileName || '''),

  --MR: Remove for testing mooring fitting error.
  lc_nDML := import_xml_DML(lc_cDML,lc_nADOID,IN_vFILENAME,lc_vImportSectionForDML || '_mooring_fitting_' || rec_mooring_fittings.rn);

  /*------------END CREATE MOORING FITTING RECORDS (spd_t_do_fac_mooring_fittings)-------------------*/

--p(in_nAPID=>lc_nAPID, in_nADOID=>IN_nADOID, in_nDOFID=>lc_nDOFID, in_vFilename=>IN_vFILENAME, in_vImportSection=>lc_vImportSectionForDML || ' of ' || lc_vAssetType, in_vNotes=>'Processed ' || rec_mooring_fittings.rn || ' of ' || lc_nNodeCountMoorings, in_dRunDate=>lc_dDate);
  --lc_nSuccess := lc_nSuccess + lc_nDML;
--dbms_output.put_line('After MOORING FITTINGS ' || rec_mooring_fittings.rn || ' : ' || lc_vAssetType || ' : ' || lc_vFileName);
      end loop;
p(in_nAPID=>lc_nAPID, in_nADOID=>IN_nADOID, in_nDOFID=>lc_nDOFID, in_vFilename=>IN_vFILENAME, in_vImportSection=>'Asset Inventory Mooring Fittings (' || lc_vAssetType || ')', in_vNotes=>lc_vAssetType, in_dRunDate=>lc_dDate, in_nTotalRecords => lc_nNodeCountMoorings, in_nCountOfRecords => lc_nSuccess, in_vGrouping => 'Asset Inventory Mooring Fittings (' || lc_vAssetType || ')');
--dbms_output.put_line(' INSIDE IF AFTER AFTER MOORING FITTINGS');


  /*------------END MOORING FITTING NODE OF MOORING FITTINGS-------------------*/

      end if;
--dbms_output.put_line('OUTSIDE IF AFTER AFTER MOORING FITTINGS');

  /*------------END MOORING FITTINGS NODE OF ASSET INVENTORY LINE ITEM-------------------*/
      end loop;
  /*------------END Asset Inventory Line Items-------------------*/
  /*------------END Asset Inventory-------------------*/

--dbms_output.put_line('AFTER AFTER MOORING FITTINGS');










  /*------------DRAWINGS OF ASSET SUMMARY LINE ITEM-------------------*/
  select extract(rec_facility_asset_summary.as_lineitem, '/AssetSummaryLineItem/Drawings') into lc_xAIDrawingsNode from dual;
  --p(in_nAPID=>lc_nAPID, in_nADOID=>IN_nADOID, in_nDOFID=>lc_nDOFID, in_vFilename=>IN_vFILENAME, in_vImportSection=>'DRAWINGS OF ASSET SUMMARY LINE ITEM', in_vNotes=>xLength(lc_xAIDrawingsNode), in_dRunDate=>lc_dDate);


  /*------------DRAWING OF DRAWINGS-------------------*/
      lc_nSuccess := 0;
      for rec_ai_drawings in (
          SELECT
            xml_table.column_value drawing
          FROM
            XMLTable('/Drawings/Drawing' passing lc_xAIDrawingsNode) xml_table
      )
      loop


dbms_output.put_line('DRAWINGS OF ASSET SUMMARY LINE ITEM');


              lc_vTemp := rec_ai_drawings.drawing.extract('/Drawing/FileName/text()').getstringval();
              lc_nFolderId := folderid(lc_vTemp, lc_nADOID );
              lc_vFileName := filename(lc_vTemp);





        lc_vDrawingID := test_val('/FacilityDrawing/DrawingID/text()',rec_ai_drawings.drawing.extract('/Drawings/Drawing/DrawingID/text()'));
        lc_vNAVFACDrawingNumber := test_val('/FacilityDrawing/NAVFACDrawingnumber/text()',rec_ai_drawings.drawing.extract('/Drawing/NAVFACDrawingnumber/text()'));
        lc_vDrawingDescription := test_val('/FacilityDrawing/Description/text()',rec_ai_drawings.drawing.extract('/Drawing/Description/text()'));
        lc_vDrawingType := test_val('/FacilityDrawing/DrawingType/text()',rec_ai_drawings.drawing.extract('/Drawing/DrawingType/text()'));

  /*------------CREATE DRAWING RECORDS (spd_t_ai_supporting_files)-------------------*/
  lc_cDML := 'insert into
          spd_t_ai_supporting_files
        (
          aiid,
          local_id,
          NAVFAC_drawing_number,
          caption,
          file_type_string,
          file_type_listid,
          file_name,
          file_section_listid,
          folderid,
          the_bfile
        )
        values
        (
        ' || lc_nAIID || ',
        ''' || lc_vDrawingID || ''',
        ''' || lc_vNAVFACDrawingNumber || ''',
        ''' || lc_vDrawingDescription || ''',
        ''' || lc_vDrawingType || ''',
        (select listid from spd_t_lists where listname = ''list_delivery_order_file_types'' and displayvalue = ''General''),
        ''' || lc_vFileName || ''',
        (select listid from spd_t_lists where listname = ''list_delivery_order_file_sections'' and displayvalue = ''Drawing Files''),
        ''' || lc_nFolderId || ''',
        bfilename(''SPD_' || lc_nFolderId || ''',''' || lc_vFileName || ''')
        )';

  lc_nDML := import_xml_DML(lc_cDML,lc_nADOID,IN_vFILENAME,lc_vImportSectionForDML);
  /*------------END CREATE DRAWING RECORDS (spd_t_ai_supporting_files)-------------------*/
  lc_nSuccess := lc_nSuccess + 1;
      end loop; --ai drawings
p(in_nAPID=>lc_nAPID, in_nADOID=>IN_nADOID, in_nDOFID=>lc_nDOFID, in_vFilename=>IN_vFILENAME, in_vImportSection=>'Asset Inventory Drawings', in_vNotes=>'', in_dRunDate=>lc_dDate, in_nTotalRecords => lc_nNodeCountSupFiles, in_nCountOfRecords => lc_nSuccess, in_vGrouping => 'Asset Inventory Drawings');
  /*------------END DRAWING OF DRAWINGS-------------------*/
  /*------------END DRAWINGS OF ASSET SUMMARY LINE ITEM-------------------*/




    end if; --if there is no Uniformat node, we dont want to process this at all

    lc_nRowNumAssetSummary := rec_facility_asset_summary.rn;
  end loop;
p(in_nAPID=>lc_nAPID, in_nADOID=>IN_nADOID, in_nDOFID=>lc_nDOFID, in_vFilename=>IN_vFILENAME, in_vImportSection=>'Asset Summary', in_vNotes=>'', in_dRunDate=>lc_dDate, in_nTotalRecords => lc_nNodeCountAssetSummary, in_nCountOfRecords => lc_nRowNumAssetSummary, in_vGrouping => 'Asset Summary');
  /*------------END ASSET SUMMARY-------------------*/
  /*------------END ASSET SUMMARIES-------------------*/





  /*------------FINDINGS AND RECOMMENDATIONS OF ASSETSUMMARYLINEITEM-------------------*/
    --findings and recommendations
      select extract(rec_facilities.facility,'/Facility/FindingsRecommendations') into lc_xFindRecsNode from dual;
      select count(*) into lc_nNodeCountFindRec FROM XMLTable('/FindingsRecommendations/FindingRecommendation' passing lc_xFindRecsNode);
  lc_vImportSectionForDML :='FINDINGS AND RECOMMENDATIONS';
  p(in_nAPID=>lc_nAPID, in_nADOID=>IN_nADOID, in_nDOFID=>lc_nDOFID, in_vFilename=>IN_vFILENAME, in_vImportSection=>lc_vImportSectionForDML, in_vNotes=>'Findings and Recommendations count: ' || lc_nNodeCountFindRec, in_dRunDate=>lc_dDate);



  /*------------FINDING AND RECOMMENDATION OF FINDINGS AND RECOMMENDATIONS-------------------*/
      for rec_ai_find_recs in (
      SELECT
        xml_table.column_value ai_find_rec, rownum rn
      FROM
        XMLTable('/FindingsRecommendations/FindingRecommendation' passing lc_xFindRecsNode) xml_table
      )
      loop


  /*------------CREATE FINDINGS AND RECOMMENDATIONS RECORDS (spd_t_ai_find_rec)-------------------*/
  --lc_vString

          lc_vUFII := rec_ai_find_recs.ai_find_rec.extract('/FindingRecommendation/UniFormatIICode/text()').getStringVal();
          lc_cFinding := test_val('finding',rec_ai_find_recs.ai_find_rec.extract('/FindingRecommendation/Findings/text()'));

          if lc_cFinding is not null then
            lc_cFinding := rec_ai_find_recs.ai_find_rec.extract('/FindingRecommendation/Findings/text()').getClobVal();
          end if;

          lc_cRecommendation := test_val('finding',rec_ai_find_recs.ai_find_rec.extract('/FindingRecommendation/Recommendations/text()'));
          if lc_cRecommendation is not null then
            lc_cRecommendation := rec_ai_find_recs.ai_find_rec.extract('/FindingRecommendation/Recommendations/text()').getClobVal();
          end if;

          select spd_s_ai_find_rec.nextval into lc_nAFRID from dual;


  lc_cDML := 'insert into
            spd_t_ai_find_rec
          (
            aifrid,
            aiid,
            finding,
            recommendation
          )
          values
          (
            ' || lc_nAFRID || ',
            ' || lc_nAFRID || ',
            ''' || lc_cFinding || ''',
            ''' || lc_cRecommendation || '''
          )';

  lc_nDML := import_xml_DML(lc_cDML,lc_nADOID,IN_vFILENAME,lc_vImportSectionForDML);

          for rec_aiids_assoc in (select aiid from spd_t_do_fac_asset_inventory where code_listid = (select listid from spd_t_lists where displayvalue = lc_vString and listname = 'list_uniformat_II_codes'))
          loop
            --ADD RECORD TO SPD_T_AI_FIND_RECO_INTER
            lc_cDML := 'insert into spd_t_ai_find_reco_inter (aifrid, aiid) values ( ' || lc_nAFRID || ',' ||  rec_aiids_assoc.aiid || ')';

            lc_nDML := import_xml_DML(lc_cDML,lc_nADOID,IN_vFILENAME,lc_vImportSectionForDML);

          end loop;

  /*------------END CREATE FINDINGS AND RECOMMENDATIONS RECORDS (spd_t_ai_find_rec)-------------------*/
  p(in_nAPID=>lc_nAPID, in_nADOID=>IN_nADOID, in_nDOFID=>lc_nDOFID, in_vFilename=>IN_vFILENAME, in_vImportSection=>lc_vImportSectionForDML || ' ' || lc_vUFII, in_vNotes=>'Processed ' || rec_ai_find_recs.rn || ' of ' || lc_nNodeCountFindRec, in_dRunDate=>lc_dDate);

  lc_nSuccess := lc_nSuccess + lc_nDML;
      end loop; --find rec
p(in_nAPID=>lc_nAPID, in_nADOID=>IN_nADOID, in_nDOFID=>lc_nDOFID, in_vFilename=>IN_vFILENAME, in_vImportSection=>'Asset Summary Findings and Recommendations', in_vNotes=>'', in_dRunDate=>lc_dDate, in_nTotalRecords => lc_nNodeCountFindRec, in_nCountOfRecords => lc_nSuccess, in_vGrouping => 'Asset Summary Findings and Recommendations');

  /*------------END FINDING AND RECOMMENDATION OF FINDINGS AND RECOMMENDATIONS-------------------*/
  /*------------END FINDINGS AND RECOMMENDATIONS OF ASSETSUMMARYLINEITEM-------------------*/






  /*------------PHOTOS OF ASSET SUMMARY LINE ITEM-------------------*/
      select extract(rec_facilities.facility,'/Facility/Photos') into lc_xAIPhotosNode from dual;
      select count(*) into lc_nNodeCountASPhotos FROM XMLTable('/Photos/Photo' passing lc_xAIPhotosNode);
  lc_vImportSectionForDML :='PHOTOS OF ASSET SUMMARY';
  p(in_nAPID=>lc_nAPID, in_nADOID=>IN_nADOID, in_nDOFID=>lc_nDOFID, in_vFilename=>IN_vFILENAME, in_vImportSection=>lc_vImportSectionForDML, in_vNotes=>'PHOTOS OF ASSET SUMMARY LINE ITEM count: ' || lc_nNodeCount, in_dRunDate=>lc_dDate);

  /*------------PHOTO OF PHOTOS-------------------*/
      lc_nSuccess := 0;
      for rec_ai_photos in (
          SELECT
            xml_table.column_value photo
          FROM
            XMLTable('/Photos/Photo' passing lc_xAIPhotosNode) xml_table
      )
      loop


          lc_vTemp := test_val('photo filename',rec_ai_photos.photo.extract('/Photo/ImageFileName/text()'));

          if lc_vTemp is not null then
            lc_vTemp := rec_ai_photos.photo.extract('/Photo/ImageFileName/text()').getstringval();
          end if;

          lc_vPhotoType := test_val('process',rec_ai_photos.photo.extract('/Photo/PhotoType/text()'));
          if lc_vPhotoType is not null then
            lc_vPhotoType := rec_ai_photos.photo.extract('/Photo/PhotoType/text()').getstringval();
            if lc_vPhotoType = 'Asset' or instr(upper(lc_vPhotoType),'TYPICAL') >0 then
              lc_vPhotoType := 'Typical';
            end if;
          else
            lc_vPhotoType := 'Typical';
          end if;

          lc_nFolderId := folderid(lc_vTemp, lc_nADOID );
          lc_vFileName := filename(lc_vTemp);
          lc_nThumbnailid := getthumbnailid(lc_nFolderId, lc_vFileName,IN_vProcessImages,IN_nADOID);

          select spd_s_do_fac_supporting_files.nextval into lc_nPhotoId from dual;

          lc_vPhotoID := test_val('/Photo/PhotoID/text()',rec_ai_photos.photo.extract('/Photo/PhotoID/text()'));
          lc_vPhotoCaption := test_val('/Photo/Caption/text()',rec_ai_photos.photo.extract('/Photo/Caption/text()'));

  /*------------CREATE PHOTO RECORDS (spd_t_ai_supporting_files)-------------------*/
  lc_cDML := 'insert into
          spd_t_do_fac_supporting_files
          (
            dofsfid,
            dofid,
            local_id,
            caption,
            file_type_string,
            file_type_listid,
            file_name,
            file_section_listid,
            folderid,
            the_bfile,
            thumbnailid
          )
          values
          (
            ' || lc_nPhotoId || ',
            ' || lc_nDOFID || ',
            ''' || lc_vPhotoID || ''',
            ''' || lc_vPhotoCaption || ''',
            ''' || lc_vPhotoType || ''',
            (select listid from spd_t_lists where rownum = 1 and listname = ''list_delivery_order_file_types'' and displayvalue = ''' || lc_vPhotoType || '''),
            ''' || lc_vFileName || ''',
            (select listid from spd_t_lists where listname = ''list_delivery_order_file_sections'' and displayvalue = ''Photo Files''),
            ' || lc_nFolderId || ',
            bfilename(''SPD_' || lc_nFolderId || ''',''' || lc_vFileName || '''),
            ' || lc_nThumbnailid || '
          )';



  lc_nDML := import_xml_DML(lc_cDML,lc_nADOID,IN_vFILENAME,lc_vImportSectionForDML || '_1');


  /*
  Photo/PhotoID
  Photo/UniFormat-IICode
  Photo/AssetTypes/AssetType
  Photo/ImageFileName
  */
          lc_vUFII := test_val('ufii',rec_ai_photos.photo.extract('/Photo/UniFormat-IICode/text()'));

          if lc_vUFII is not null then
            lc_vUFII := rec_ai_photos.photo.extract('/Photo/UniFormat-IICode/text()').getstringval();
          end if;

  /*------------CREATE PHOTO RECORDS ASSOCIATION WITH ASSET TYPE (spd_t_ai_photo_inter)-------------------*/
        select extract(rec_ai_photos.photo,'/Photo/AssetTypes') into lc_xPhotoAssetTypesNode from dual;

        lc_nContinue :=0;
        --if lc_vUFII = lc_vString then --lc_vString = uniformat II code from line 1065
          for rec_asset_type_photos in (
              SELECT
                xml_table.column_value AssetType
              FROM
                XMLTable('/AssetTypes/AssetType' passing lc_xPhotoAssetTypesNode) xml_table
          )
          loop

            lc_vAssetType := trim(rec_asset_type_photos.AssetType.extract('/AssetType/text()').getstringval());


            lc_vPhotoType := test_val('process',rec_ai_photos.photo.extract('/Photo/PhotoType/text()'));
            if lc_vPhotoType is not null then
              lc_vPhotoType := rec_ai_photos.photo.extract('/Photo/PhotoType/text()').getstringval();
              if lc_vPhotoType = 'Asset' or instr(upper(lc_vPhotoType),'TYPICAL') >0 then
                lc_vPhotoType := 'Typical';
              end if;
            else
              lc_vPhotoType := 'Typical';
            end if;

            --htp.p('select * from spd_t_do_fac_asset_inventory where dofid = ' || lc_nDOFID || ' and asset_type = ' || lc_vAssetType );
    --for each asset type in asset types, check against the asset inventory table for this dofid to see if this picture is relevant. Get the aiid and apply the aiid and dosfid to the table spd_t_ai_photo_inter



            lc_nContinue := 0;

            for rec in (select * from spd_t_do_fac_asset_inventory where dofid = lc_nDOFID and asset_type = lc_vAssetType and code_type_listid = 100000)
            loop
              --this asset type and dofid matches an asset inventory record. add this to the spd_t_ai_photo_inter table

              lc_vPhotoID := test_val('/Photo/PhotoID/text()',rec_ai_photos.photo.extract('/Photo/PhotoID/text()'));
              lc_vPhotoCaption := test_val('/Photo/Caption/text()',rec_ai_photos.photo.extract('/Photo/Caption/text()'));

              --MR: REMOVING THIS TO ADDRESS ISSUE #T1023 XML IMPORT photos for asset inventory
              --select spd_s_do_fac_supporting_files.nextval into lc_nPhotoId from dual;


  lc_cDML := 'insert into
              spd_t_ai_supporting_files
              (
                dofsfid,
                aiid,
                local_id,
                caption,
                file_type_string,
                file_type_listid,
                file_name,
                file_section_listid,
                folderid,
                the_bfile,
                thumbnailid
              )
              values
              (
                ' || lc_nPhotoId || ',
                ' || rec.aiid || ',
                ''' || lc_vPhotoID || ''',
                ''' || lc_vPhotoCaption || ''',
                ''' || lc_vPhotoType || ''',
                (select listid from spd_t_lists where rownum = 1 and listname = ''list_delivery_order_file_types'' and displayvalue = ''' || lc_vPhotoType || '''),
                ''' || lc_vFileName || ''',
                (select listid from spd_t_lists where listname = ''list_delivery_order_file_sections'' and displayvalue = ''Photo Files''),
                ' || lc_nFolderId || ',
                bfilename(''SPD_' || lc_nFolderId || ''',''' || lc_vFileName || '''),
                ' || lc_nThumbnailid || '
              )';


  lc_nDML := import_xml_DML(lc_cDML,lc_nADOID,IN_vFILENAME,lc_vImportSectionForDML|| '_2');
              lc_nContinue := 1;
            end loop;



          end loop;


          if lc_nContinue = 0 then
          --this is an asset summary record, check ufii value and asset summary type of 100001
          for rec in (select * from spd_t_do_fac_asset_inventory where dofid = lc_nDOFID and code_listid = (select listid from spd_t_lists where listname = 'list_uniformat_II_codes' and displayvalue = lc_vUFII) and code_type_listid = 100001)
            loop
              --this asset type and dofid matches an asset inventory record. add this to the spd_t_ai_photo_inter table
              lc_vPhotoID := test_val('/Photo/PhotoID/text()',rec_ai_photos.photo.extract('/Photo/PhotoID/text()'));
              lc_vPhotoCaption := test_val('/Photo/Caption/text()',rec_ai_photos.photo.extract('/Photo/Caption/text()'));
              --MR: REMOVING THIS TO ADDRESS ISSUE #T1023 XML IMPORT photos for asset inventory
              --select spd_s_do_fac_supporting_files.nextval into lc_nPhotoId from dual;

  lc_cDML := 'insert into
              spd_t_ai_supporting_files
              (
                dofsfid,
                aiid,
                local_id,
                caption,
                file_type_string,
                file_type_listid,
                file_name,
                file_section_listid,
                folderid,
                the_bfile,
                thumbnailid
              )
              values
              (
                ' || lc_nPhotoId || ',
                ' || rec.aiid || ',
                ''' || lc_vPhotoID || ''',
                ''' || lc_vPhotoCaption || ''',
                ''' || lc_vPhotoType || ''',
                (select listid from spd_t_lists where rownum = 1 and listname = ''list_delivery_order_file_types'' and displayvalue = ''' || lc_vPhotoType || '''),
                ''' || lc_vFileName || ''',
                (select listid from spd_t_lists where listname = ''list_delivery_order_file_sections'' and displayvalue = ''Photo Files''),
                ' || lc_nFolderId || ',
                bfilename(''SPD_' || lc_nFolderId || ''',''' || lc_vFileName || '''),
                ' || lc_nThumbnailid || '
              )';


  lc_nDML := import_xml_DML(lc_cDML,lc_nADOID,IN_vFILENAME,lc_vImportSectionForDML|| '_3');


              lc_nContinue := 1;
            end loop;
          end if;

  /*------------CREATE PHOTO RECORDS (spd_t_ai_supporting_files)-------------------*/
/* there are alot of things going on here, need to decipher the real deal, but for now we will just add one and call it a day */
  lc_nSuccess := lc_nSuccess + 1;
      end loop; --find rec
p(in_nAPID=>lc_nAPID, in_nADOID=>IN_nADOID, in_nDOFID=>lc_nDOFID, in_vFilename=>IN_vFILENAME, in_vImportSection=>'Asset Summary Photos', in_vNotes=>'', in_dRunDate=>lc_dDate, in_nTotalRecords => lc_nNodeCountASPhotos, in_nCountOfRecords => lc_nSuccess, in_vGrouping => 'Asset Summary Photos');


  /*------------END PHOTO OF PHOTOS-------------------*/
  /*------------END PHOTOS OF ASSET SUMMARY LINE ITEM-------------------*/

    end if;
    lc_nRowNum := lc_nRowNum + 1;
  else
    /* not throwing a json error yet, but will log the issue */
    lc_jError := json();
    lc_jError.put('function','import_xml');
    lc_jError.put('module','xmlImportManager');
    lc_jError.put('target',lc_vImportSectionForDML);
    lc_jError.put('ref_id',in_nAdoid);
    lc_jError.put('input',in_vFilename);
    lc_jlMessages.append(json_list('[{error:''IXML.0001 xml facility not matched with dofid ''}]'));
    lc_jError.put('messages',lc_jlMessages);
    BB_P_ERROR(lc_jError);
    lc_nRowNum := lc_nRowNum + 0;
  end if;




 /*------------ FACILITY REPAIR COSTS -------------------*/
   /*------------CREATE PHOTO RECORDS ASSOCIATION WITH ASSET TYPE (spd_t_ai_photo_inter)-------------------*/
    select extract(rec_facilities.facility, '/Facility/FacilityRepairCosts') into lc_xFacilityRepairsNode from dual;

    lc_vImportSectionForDML := 'FACILITY REPAIR COST';

	for rec_do_fac_repairs in (
              SELECT
                xml_table.column_value doFacRepair
              FROM
                XMLTable('/FacilityRepairCosts/Repair' passing lc_xFacilityRepairsNode) xml_table
      )
    loop



      lc_vUniFormatIICode := XML_TOOLS.test_val('/Repair/UniFormatIICode',rec_do_fac_repairs.doFacRepair.extract('/Repair/UniFormatIICode/text()'));
      lc_vAssetType := XML_TOOLS.test_val('/Repair/AssetType',rec_do_fac_repairs.doFacRepair.extract('/Repair/AssetType/text()'));
      lc_vRepairDescription := XML_TOOLS.test_val('/Repair/RepairDescription',rec_do_fac_repairs.doFacRepair.extract('/Repair/RepairDescription/text()'));
      lc_nQuantity := XML_TOOLS.test_val('/Repair/Quantity',rec_do_fac_repairs.doFacRepair.extract('/Repair/Quantity/text()'));
      lc_vUnits := XML_TOOLS.test_val('/Repair/Units',rec_do_fac_repairs.doFacRepair.extract('/Repair/Units/text()'));
      lc_nMaterialUnitCost := XML_TOOLS.test_val('/Repair/MaterialUnitCost',rec_do_fac_repairs.doFacRepair.extract('/Repair/MaterialUnitCost/text()'));
      lc_nLaborUnitCost := XML_TOOLS.test_val('/Repair/LaborUnitCost',rec_do_fac_repairs.doFacRepair.extract('/Repair/LaborUnitCost/text()'));



      /*------------CREATE REPAIR RECORD (spd_t_repairs)-------------------*/
-- we insert the repair in the table
lc_cDML := 'insert into
    spd_t_do_fac_repairs
    (
      DOFACID,
      UNIFORMATIICODE,
      ASSET_TYPE,
      REPAIR_DESCRIPTION,
      QUANTITY,
      UNITS,
      MATERIAL_UNIT_COST,
      LABOR_UNIT_COST
    )
    values
    (
      ''' || lc_nDOFID || ''',
      ''' || lc_vUniFormatIICode || ''',
      ''' || lc_vAssetType || ''',
      ''' || lc_vRepairDescription || ''',
      ''' || lc_nQuantity || ''',
      ''' || lc_vUnits || ''',
      ''' || lc_nMaterialUnitCost || ''',
      ''' || lc_nLaborUnitCost || '''
    )';


lc_nDML := import_xml_DML(lc_cDML,lc_nADOID,IN_vFILENAME,lc_vImportSectionForDML);
/*------------END CREATE REPAIR RECORD (spd_t_repairs)-------------------*/
lc_nSuccess := lc_nSuccess + lc_nDML;

  end loop;
p(in_nAPID=>lc_nAPID, in_nADOID=>IN_nADOID, in_nDOFID=>0, in_vFilename=>IN_vFILENAME, in_vImportSection=>lc_vImportSectionForDML, in_vNotes=>'', in_dRunDate=>lc_dDate, in_nTotalRecords => lc_nNodeCount, in_nCountOfRecords => lc_nSuccess, in_vGrouping => 'Action Supporting Files');


 /*------------ END FACILITY REPAIR COSTS -------------------*/





  end loop;--facilities loop
p(in_nAPID=>lc_nAPID, in_nADOID=>IN_nADOID, in_nDOFID=>0, in_vFilename=>IN_vFILENAME, in_vImportSection=>'Facilities of Action', in_vNotes=>'', in_dRunDate=>lc_dDate, in_nTotalRecords => lc_nNodeCountFacilities, in_nCountOfRecords => lc_nRowNum, in_vGrouping => 'Facilities of Action');


end loop;--action loop
p(in_nAPID=>lc_nAPID, in_nADOID=>IN_nADOID, in_nDOFID=>lc_nDOFID, in_vFilename=>IN_vFILENAME, in_vImportSection=>'IMPORT COMPLETE', in_vNotes=>IN_vFILENAME, in_dRunDate=>lc_dDate);
lc_vImportSectionForDML :='IMPORT COMPLETE';
p(in_nAPID=>lc_nAPID, in_nADOID=>IN_nADOID, in_nDOFID=>lc_nDOFID, in_vFilename=>IN_vFILENAME, in_vImportSection=>lc_vImportSectionForDML, in_vNotes=>'COMPLETE', in_dRunDate=>lc_dDate,in_nTotalRecords=>1,in_nCountOfRecords=>1,in_vGrouping=>'IMPORT COMPLETE');


  exception
    when others then
        lc_vError := replace(SQLERRM,CHR(10),' ');
        lc_jError := json();
        lc_jlMessages := json_list();
        lc_jError.put('function','import_xml');
        lc_jError.put('module','xmlImportManager');
        lc_jError.put('target',lc_vImportSectionForDML);
        lc_jError.put('ref_id',in_nAdoid);
        lc_jError.put('input',in_vFilename);
        lc_jlMessages.append(json_list('[{error:' || lc_vError || '''}]'));
        lc_jError.put('messages',lc_jlMessages);
        BB_P_ERROR(lc_jError);

end;


function  get_DOFID (in_nFACID number, in_nADOID number, in_vNFAID varchar2 default '',in_vActivityUIC varchar2, in_vFacilityName varchar2, in_vFacilityNo varchar2) return number as
lc_nReturn number := 0;
lc_nExists number := 0;
lc_nFACID number :=0;
lc_vNFAID varchar2(25) := in_vNFAID;

begin
lc_vNFAID := in_vNFAID;

/**Does this facility exist in the DO_FACILITIES table*/
for rec in
(
  select dofid from spd_t_do_facilities where facid = in_nFACID and adoid = in_nADOID
)
loop
  lc_nReturn := rec.dofid;
  lc_nExists := 1;
end loop;

if lc_nExists = 0 then

  /**Check the mapped table first*/
  for rec_mapped_ids in (select old_id, new_id from spd_t_mapped_ids where table_name = 'spd_t_facilities' and old_id = in_nFACID)
  loop
    lc_nFACID := rec_mapped_ids.new_id;
    lc_nExists := 1;
  end loop;

  if lc_nExists =0 then
      /**Add the facility to the delivery order, get back the DOFID in lc_nReturn and the new FACID if applicable in lc_nFACID*/
      add_actn_do_fac(in_nADOID, lc_vNFAID, 100085, lc_nReturn, lc_nFACID, in_vActivityUIC, in_vFacilityName, in_vFacilityNo);
      insert into spd_t_mapped_ids(old_id, new_id, table_name) values (in_nFACID, lc_nReturn, 'SPD_T_DO_FACILITIES');
      commit;
      lc_nExists := 1;
  end if;

end if;

return lc_nReturn;

end;

/**Add the facility to spd_t_do_facilities*/
procedure add_actn_do_fac(in_nADOID number, io_vNFAID IN OUT varchar2, IN_PL number, o_nDOFID OUT number, o_nFACID OUT number, in_vActivityUIC varchar2, in_vFacilityName varchar2, in_vFacilityNo varchar2)


is

i number;
lc_nDOFID number;
lc_nCount number :=0;
lc_nColCount number;
lc_nNFACount number :=0;
lc_nInfadsTemp number :=0;
lc_nFacid number :=0;
lc_nDeliveryOrderID number :=0;
begin

/*
When adding a facility to a scope, we need to check if its in the spd_t_facilities
if it is, then get the spiders facid
else add it and then get the spiders facid
*/
for rec in (select * from spd_t_facilities where infads_facility_id = io_vNFAID) loop
  lc_nCount := 1;
  lc_nFacid := rec.facid;
end loop;

if lc_nCount = 0 then


  /**Check to see if this facility exists in infads facility table. if it does not then add it to the spd_t_infads_temp...*/
  for rec_infads in (select facility_id from spd_mv_inf_facility where facility_id = io_vNFAID)

  loop
    lc_nNFACount := 1;
  end loop;

  if lc_nNFACount = 1 then
    /**facility was found in infads, add to spd_t_facilities*/
    select spd_s_facilities.nextval into lc_nFacid from dual;
    insert into spd_t_facilities (facid, infads_facility_id, product_line_listid, spd_fac_name, spd_fac_no) select lc_nFacid, io_vNFAID, IN_PL, facility_name, facility_no from spd_mv_inf_facility where facility_id = io_vNFAID;
    commit;
    lc_nCount := 0;

    for rec_activities in (select * from spd_t_activities where spd_uic in (select inf.activity_uic from spd_mv_inf_facility inf where inf.facility_id = io_vNFAID)) loop
      lc_nCount := 1;
    end loop;

    if lc_nCount = 0 then
      /*
        add the activity record to spd_t_activities table
      */
      insert into spd_t_activities (spd_activity_name, spd_uic, state) select unit_title, uic, state from spd_mv_inf_cur_activities where uic in (select activity_uic from spd_mv_inf_facility where facility_id = io_vNFAID and productline_listid = IN_PL);
      commit;

    end if;
  else
    /**facility was not found in infads, add to spd_t_infads_temp and then to spd_t_facilities*/
    select spd_s_infads_temp.nextval into lc_nInfadsTemp from dual;
    insert into spd_t_infads_temp (infadstempid, facility_id, incorrect_facility_id, activity_uic) values (lc_nInfadsTemp, 'SPDNFA' || lc_nInfadsTemp, io_vNFAID, in_vActivityUIC);
    select spd_s_facilities.nextval into lc_nFacid from dual;
    insert into spd_t_facilities (facid, infads_facility_id, product_line_listid, spd_fac_name, spd_fac_no) values (lc_nFacid, 'SPDNFA' || lc_nInfadsTemp, IN_PL, in_vFacilityName, in_vFacilityNo);
    commit;
  end if;

end if;


/*
get the new dofid to be used when adding special instructions and direct cost lists
*/
select spd_s_do_facilities.nextval into lc_nDOFID from dual;
o_nDOFID := lc_nDOFID;

/*!!super sloppy, but we need to get the DELIVERYORDERID from the ADOID. The trigger then figures out the ADOID from the Deliveryorderid*/
for rec in (select deliveryorderid from spd_t_delivery_orders where adoid = in_nADOID) loop
  lc_nDeliveryOrderID := rec.deliveryorderid;
end loop;

/*What happens if this adoid doesnt have a new delivrey order associated?*/
if lc_nDeliveryOrderID = 0 then
  null; --for now;
end if;

/*!!NEW VERSION, This one makes a record in SPD_T_DELIVERY_ORDER_FACS which has a trigger to make the legacy SPD_T_DO_FACILITIES record*/
insert into
  spd_t_delivery_order_facs
    (
      dofid,
      deliveryorderid,
      facid
    )
values
  (
    lc_nDOFID,
    lc_nDeliveryOrderID,
    lc_nFacid
  );
commit;





/*
!!LEGACY VERSION
insert into
  spd_t_do_facilities
    (
      dofid,
      adoid,
      facid,
      ACTION_TYPE_LISTID
    )
values
  (
    lc_nDOFID,
    in_nADOID,
    lc_nFacid,
    (select listid from spd_t_lists where listname = 'list_planned_action_types' and returnvalue = 'Routine Inspection' and  productline_listid = IN_PL)
  );
commit;
*/


/*
!!REMOVING UNUSED LEGACY CODE
insert into spd_t_do_fac_spec_inst (dofid, spec_instr_listid) select lc_nDOFID, listid from spd_t_lists where listname = 'list_facility_special_instructions' and productline_listid = IN_PL;
commit;

insert into spd_t_do_facility_ce_dc (dofid, directcost_listid) select lc_nDOFID, listid from spd_t_lists where listname = 'list_facility_direct_cost_types' and productline_listid = IN_PL;
commit;
*/


  exception
        when others then
        htp.p('error inserting delivery order facility, special instruction or facility direct cost types: ' ||
        'IN_APID = ' || to_char(IN_nADOID) || ',
        IN_NFA = ' || io_vNFAID || ',
        IN_PL = ' || to_char(IN_PL) || ': ' ||
        SQLERRM
        );



end;

/**not going to use this for now
procedure update_xml_facility_id(IN_nADOID number)

as

begin

for rec in (select old_id, new_id, table_name from spd_t_mapped_ids where table_name = 'spd_t_facilities')
loop
    UPDATE spd_t_xml_files
    SET xml_document =  UPDATEXML(xml_document,
       '/Action/DataReqRevision/text()', '1.3.2.22222')
    where xmlfileid = 430000000;
end loop;

end;
*/




/*
ok, so we want to import a data requirements xml document.

what are the steps.

1. what is the adoid
    a. this should be easy, as we will be selecting it from the planner page
    var lc_nADOID
    var lc_nDOFID
    var lc_nAIID
    var lc_nParentAIID
    var lc_nAIID_2
    var lc_nCount_Facilities
    var lc_nCount_Chunk
    var lc_nCount_Files

2. with the adoid we can:
    a. update this information
        1. spd_t_actn_dos.end_date = /Action/OnSiteEndDate
        2. spd_t_actn_dos.ktr_submittal_date = /Action/SubmittalDate
        3. spd_t_actn_dos.spiders_data_version = NOT IN XML (possible)
        4. spd_t_actn_dos.exit_brief_date = /Action/ExitBrief/ExitBriefDate (add field)
        5. spd_t_actn_dos.exit_brief_time = /Action/ExitBrief/ExitBriefTime (add field)
        6. spd_t_actn_dos.exit_brief_loc = /Action/ExitBrief/ExitBriefLOC (add field)
        7. spd_t_actn_dos.executive_summary
            for each Chunk in ExecutiveSummary
                1. spd_t_actn_dos.executive_summary = /Action/ExecutiveSummary/Chunk(n) (chunked xml)
        for each drawing in /Action/Drawings
            a. insert into spd_t_do_supporting files (adoid, columns below) values (lc_nADOID, values below);
            8. spd_t_do_supporting_files.local_id = /Action/Drawings/Drawing/DrawingID
            9. spd_t_do_supporting_files.NAVFAC_drawing_number = /Action/Drawings/Drawing/NAVFACDrawingnumber (add field)
           10. spd_t_do_supporting_files.caption = /Action/Drawings/Drawing/Description
           11. spd_t_do_supporting_files.file_type_string = /Action/Drawings/Drawing/DrawingType (add field)
                a. spd_t_do_supporting_files.file_type_listid (this has to be discovered using the file_type_string. If none is found, use general document (*))
           12. spd_t_do_supporting_files.file_name = /Action/Drawings/Drawing/FileName
           13. spd_t_do_supporting_files.file_section_listid (based on the location of the node /Action/Drawings)
        for each Photo in /Action/Photos
            a. insert into spd_t_do_supporting files (adoid, columns below) values (lc_nADOID, values below);
           14. spd_t_do_supporting_files.local_id = /Action/Photos/Photo/PhotoID
           15. spd_t_do_supporting_files.caption = /Action/Photos/Photo/Caption
           16. spd_t_do_supporting_files.file_type_string = /Action/Photos/Photo/PhotoType (add field)
                a. spd_t_do_supporting_files.file_type_listid (this has to be discovered using the file_type_string. If none is found, use general document (*))
           17. spd_t_do_supporting_files.file_name = /Action/Photos/Photo/ImageFileName
           18. spd_t_do_supporting_files.file_section_listid (based on the location of the node /Action/Photos)
        for each SupportingFile in /Action/SupportingFiles
        /* Need to have an example to fill this section out completely
            a. insert into spd_t_do_supporting_files (adoid, columns below) values (lc_nADOID, values below);
           19. spd_t_do_supporting_files.local_id
           20. spd_t_do_supporting_files.caption
           21. spd_t_do_supporting_files.file_type_string (add field)
                a. spd_t_do_supporting_files.file_type_listid (this has to be discovered using the file_type_string. If none is found, use general document (*))
           22. spd_t_do_supporting_files.file_name
           23. spd_t_do_supporting_files.file_section_listid (based on the location of the node /Action/Photos)
        */
        /*
        for each Facility in /Action/Facility --!!Should we add a Facilities node? /Action/Facilities/Facility i did in the sample datafile
            a. we can get the dofid, select dofid into lc_nDOFID from spd_t_do_facilities where facid = /Action/Facilities/Facility/Facid and adoid = lc_nADOID;
            24. spd_t_do_facilities.facility_onsite_start_date = /Action/Facilities/Facility/OnsiteStartDate
            25. spd_t_do_facilities.facility_onsite_end_date = /Action/Facilities/Facility/OnsiteEndDate
            26. spd_t_do_facilities.facility_submittal_date = /Action/Facilities/Facility/SubmittalDate
            27. spd_t_do_facility_meta_data
                a. spd_t_do_facility_meta_data.displayvalue = 'Latitude' .value= /Action/Facilities/Facility/Latitude, dofid = lc_nDOFID
                b. spd_t_do_facility_meta_data.displayvalue = 'Longitude' .value= /Action/Facilities/Facility/Longitude, dofid = lc_nDOFID
            28. spd_t_do_fac_supporting_files (add table)
                a. .file_name = /Action/Facilities/Facility/CostEstimateFileName
                b. .file_type_listid = get the list id for facility cost estimates
                c. .dofid = lc_nDOFID
            29. spd_t_do_fac_supporting_files (add table)
                a. .file_name = /Action/Facilities/Facility/DD1391
                b. .file_type_listid = get the list id for facility 1391s
                c. .dofid = lc_nDOFID
            30. spd_t_do_facility_meta_data
                a. spd_t_do_faciity_meta_data.displayvalue = 'Year Built' .value= /Action/Facilities/Facility/ExecutiveTable/YearBuilt, dofid = lc_nDOFID
                b. spd_t_do_faciity_meta_data.displayvalue = 'Year Improved' .value= /Action/Facilities/Facility/ExecutiveTable/YearBuilt, dofid = lc_nDOFID
                c. spd_t_do_faciity_meta_data.displayvalue = 'Number of Piles' .value= /Action/Facilities/Facility/ExecutiveTable/YearBuilt, dofid = lc_nDOFID
                d. spd_t_do_faciity_meta_data.displayvalue = 'Linear Feet' .value= /Action/Facilities/Facility/ExecutiveTable/YearBuilt, dofid = lc_nDOFID
                e. spd_t_do_faciity_meta_data.displayvalue = 'Length' .value= /Action/Facilities/Facility/ExecutiveTable/YearBuilt, dofid = lc_nDOFID
                f. spd_t_do_faciity_meta_data.displayvalue = 'Width' .value= /Action/Facilities/Facility/ExecutiveTable/YearBuilt, dofid = lc_nDOFID
                g. spd_t_do_faciity_meta_data.displayvalue = 'Area' .value= /Action/Facilities/Facility/ExecutiveTable/YearBuilt, dofid = lc_nDOFID
                h. spd_t_do_faciity_meta_data.displayvalue = 'Structural Material' .value= /Action/Facilities/Facility/ExecutiveTable/YearBuilt, dofid = lc_nDOFID
            31. spd_t_do_facilities.ovr_eng_assessment_rating = /Action/Facilities/Facility/ExecutiveTable/OverallEngineeringRatingASCE
            32. spd_t_do_facilities.contition_index = /Action/Facilities/Facility/ExecutiveTable/OverallCIRating
            33. spd_t_do_facilities.five_year_proj_ci = /Action/Facilities/Facility/ExecutiveTable/Overall5YearProjectedCI
            34. spd_t_do_facilities.ten_year_proj_ci = /Action/Facilities/Facility/ExecutiveTable/Overall10YearProjectedCI
            35. spd_t_do_facilities.current_oper_rating = /Action/Facilities/Facility/ExecutiveTable/CurrentOperationRating
            36. spd_t_do_facilities.operational_restrictions = /Action/Facilities/Facility/ExecutiveTable/OperationalRestrictions
            37. spd_t_do_facilities.repair_recommendations = /Action/Facilities/Facility/ExecutiveTable/RepairRecommendations
            38. spd_t_do_facilities.cur_fac_usage_description = /Action/Facilities/Facility/ExecutiveTable/CurrentFacilityUsageDescription
            39. spd_t_do_facility_meta_data
                a. spd_t_do_faciity_meta_data.displayvalue = 'Max Current' .value= /Action/Facilities/Facility/ExecutiveTable/MaxCurrent, dofid = lc_nDOFID
                b. spd_t_do_faciity_meta_data.displayvalue = 'Water Clarity' .value= /Action/Facilities/Facility/ExecutiveTable/WaterClarity, dofid = lc_nDOFID
                c. spd_t_do_faciity_meta_data.displayvalue = 'Tide Variation' .value= /Action/Facilities/Facility/ExecutiveTable/TideVariation, dofid = lc_nDOFID
                d. spd_t_do_faciity_meta_data.displayvalue = 'Max Water Depth' .value= /Action/Facilities/Facility/ExecutiveTable/MaxWaterDepth, dofid = lc_nDOFID
                e. spd_t_do_faciity_meta_data.displayvalue = 'Seasonal Water Temperature' .value= /Action/Facilities/Facility/ExecutiveTable/SeasonalWaterTemp, dofid = lc_nDOFID
                f. spd_t_do_faciity_meta_data.displayvalue = 'Seasonal Ambient Temperature' .value= /Action/Facilities/Facility/ExecutiveTable/SeasonalAmbientTemp, dofid = lc_nDOFID
            for each AssetSummaryLineItem in /Action/Facilities/Facility/AssetSummary
                a. select spd_s_aiid.nextval into lc_nAIID from dual
                b. get the listid for the uniformat code /Action/Facilities/Facility/AssetSummary/AssetSummaryLineItem/UniFormatIICode
                c. get the parent AIID for this record based on the parent listid for the uniformat (lc_nParentAIID)
                40. spd_t_do_fac_asset_inventory.asset_type = /Action/Facilities/Facility/AssetSummary/AssetSummaryLineItem/AssetType
                41. spd_t_do_fac_asset_inventory.facility_section = /Action/Facilities/Facility/AssetSummary/AssetSummaryLineItem/FacilitySection
                42. spd_t_do_fac_asset_inventory.criticality_factor = /Action/Facilities/Facility/AssetSummary/AssetSummaryLineItem/CriticalityFactor
                43. spd_t_do_fac_asset_inventory.count_of_assets = /Action/Facilities/Facility/AssetSummary/AssetSummaryLineItem/NoOfAssetsCount (from No.ofAssetsCount) (add count_of_assets)
                44. spd_t_do_fac_asset_inventory.quantity_of_units = /Action/Facilities/Facility/AssetSummary/AssetSummaryLineItem/QuantityofUnits (add quantity_of_units)
                45. spd_t_do_fac_asset_inventory.unit_type = /Action/Facilities/Facility/AssetSummary/AssetSummaryLineItem/UnitType (add unit_type)
                46. spd_t_do_fac_asset_inventory.condition_index = /Action/Facilities/Facility/AssetSummary/AssetSummaryLineItem/CurrentCI
                47. spd_t_do_fac_asset_inventory.repair_cost_estimate = /Action/Facilities/Facility/AssetSummary/AssetSummaryLineItem/ApproximateRepairCosts
                48. spd_t_do_fac_asset_inventory.est_repair_condition_index = /Action/Facilities/Facility/AssetSummary/AssetSummaryLineItem/EstimatedResultingCI
                for each FindingRecommendation in /Action/Facilities/Facility/AssetSummary/AssetSummaryLineItem/FindingsRecommendations
                    49. spd_t_ai_find_rec.finding = /Action/Facilities/Facility/AssetSummary/AssetSummaryLineItem/FindingsRecommendations/FindingRecommendation/Findings
                    50. spd_t_ai_find_rec.recommendation = /Action/Facilities/Facility/AssetSummary/AssetSummaryLineItem/FindingsRecommendations/FindingRecommendation/Recommendations
                for each Photo in /Action/Facilities/Facility/AssetSummary/AssetSummaryLineItem/Photos
                    51. spd_t_ai_supporting_files.local_id = /Action/Facilities/Facility/AssetSummary/AssetSummaryLineItem/Photos/Photo/PhotoID
                    52. spd_t_ai_supporting_files.file_name = /Action/Facilities/Facility/AssetSummary/AssetSummaryLineItem/Photos/Photo/ImageFileName
                    53. spd_t_ai_supporting_files.caption = /Action/Facilities/Facility/AssetSummary/AssetSummaryLineItem/Photos/Photo/Caption
                    54. spd_t_ai_supporting_files.file_type_string = /Action/Facilities/Facility/AssetSummary/AssetSummaryLineItem/Photos/Photo/PhotoType
                        a. spd_t_ai_supporting_files.file_type_id = get file type id from file_type_string
                for each drawing in /Action/Facilities/Facility/AssetSummary/AssetSummaryLineItem/Drawings
                    ...
                for each AssetInventoryLineItem in /Action/Facilities/Facility/AssetSummary/AssetSummaryLineItem/AssetInventory
                    a. select spd_s_aiid.nextval into lc_nAIID_2 from dual
                    b. get the listid for the uniformat code /Action/Facilities/Facility/AssetSummary/AssetSummaryLineItem/AssetInventory/AssetInventoryLineItem/UniFormatIICode
                    c. get the parent AIID for this record based on the parent listid for the uniformat (lc_nParentAIID_2)
                    55. spd_t_do_fac_asset_inventory.asset_type = /Action/Facilities/Facility/AssetSummary/AssetSummaryLineItem/AssetInventory/AssetInventoryLineItem/AssetType
                    56. spd_t_do_fac_asset_inventory.facility_section = /Action/Facilities/Facility/AssetSummary/AssetSummaryLineItem/AssetInventory/AssetInventoryLineItem/FacilitySection
                    57. spd_t_do_fac_asset_inventory.criticality_factor = /Action/Facilities/Facility/AssetSummary/AssetSummaryLineItem/AssetInventory/AssetInventoryLineItem/CriticalityFactor
                    58. spd_t_do_fac_asset_inventory.count_of_assets = /Action/Facilities/Facility/AssetSummary/AssetSummaryLineItem/AssetInventory/AssetInventoryLineItem/NoOfAssetsCount (from No.ofAssetsCount) (add count_of_assets)
                    59. spd_t_do_fac_asset_inventory.quantity_of_units = /Action/Facilities/Facility/AssetSummary/AssetSummaryLineItem/AssetInventory/AssetInventoryLineItem/QuantityofUnits (add quantity_of_units)
                    60. spd_t_do_fac_asset_inventory.unit_type = /Action/Facilities/Facility/AssetSummary/AssetSummaryLineItem/AssetInventory/AssetInventoryLineItem/UnitType (add unit_type)
                    61. spd_t_do_fac_asset_inventory.condition_index = /Action/Facilities/Facility/AssetSummary/AssetSummaryLineItem/AssetInventory/AssetInventoryLineItem/CurrentCI
                    62. spd_t_do_fac_asset_inventory.repair_cost_estimate = /Action/Facilities/Facility/AssetSummary/AssetSummaryLineItem/AssetInventory/AssetInventoryLineItem/ApproximateRepairCosts
                    63. spd_t_do_fac_asset_inventory.est_repair_condition_index = /Action/Facilities/Facility/AssetSummary/AssetSummaryLineItem/AssetInventory/AssetInventoryLineItem/EstimatedResultingCI
                    64. spd_t_do_ai_meta_data
                        a. spd_t_do_faciity_meta_data.displayvalue = 'Material' .value=/Action/Facilities/Facility/AssetSummary/AssetSummaryLineItem/AssetInventory/AssetInventoryLineItem/Material, aiid = lc_nAIID_2
                        b. spd_t_do_faciity_meta_data.displayvalue = 'Material Description' .value=/Action/Facilities/Facility/AssetSummary/AssetSummaryLineItem/AssetInventory/AssetInventoryLineItem/MaterialDescription, aiid = lc_nAIID_2
                        c. spd_t_do_faciity_meta_data.displayvalue = 'Age' .value=/Action/Facilities/Facility/AssetSummary/AssetSummaryLineItem/AssetInventory/AssetInventoryLineItem/Material, aiid = lc_nAIID_2
                        d. spd_t_do_faciity_meta_data.displayvalue = 'Pile Fittings Size (in)' .value=/Action/Facilities/Facility/AssetSummary/AssetSummaryLineItem/AssetInventory/AssetInventoryLineItem/PilesFittingsSizein, aiid = lc_nAIID_2 (from PilesFittingsSizein.)
                        e. spd_t_do_faciity_meta_data.displayvalue = 'Pile Fittings Shape' .value=/Action/Facilities/Facility/AssetSummary/AssetSummaryLineItem/AssetInventory/AssetInventoryLineItem/PilesFittingsShape, aiid = lc_nAIID_2
                        f. spd_t_do_faciity_meta_data.displayvalue = 'Pile Fittings Shape Description' .value=/Action/Facilities/Facility/AssetSummary/AssetSummaryLineItem/AssetInventory/AssetInventoryLineItem/PilesFittingsShapeDescription, aiid = lc_nAIID_2
                        g. spd_t_do_faciity_meta_data.displayvalue = 'Caps/Beams/Girders Depth (in)' .value=/Action/Facilities/Facility/AssetSummary/AssetSummaryLineItem/AssetInventory/AssetInventoryLineItem/CapsBeamsGirdersDepthin, aiid = lc_nAIID_2 (from CapsBeamsGirdersDepthin.)
                        h. spd_t_do_faciity_meta_data.displayvalue = 'Caps/Beams/Girders Width (in)' .value=/Action/Facilities/Facility/AssetSummary/AssetSummaryLineItem/AssetInventory/AssetInventoryLineItem/CapsBeamsGirdersWidthin, aiid = lc_nAIID_2 (from CapsBeamsGirdersWidthin.)
                        i. spd_t_do_faciity_meta_data.displayvalue = 'Caps/Beams/Girders Length (ft)' .value=/Action/Facilities/Facility/AssetSummary/AssetSummaryLineItem/AssetInventory/AssetInventoryLineItem/CapsBeamsGirdersLengthft, aiid = lc_nAIID_2
                        j. spd_t_do_faciity_meta_data.displayvalue = 'Deck Depth (in)' .value=/Action/Facilities/Facility/AssetSummary/AssetSummaryLineItem/AssetInventory/AssetInventoryLineItem/DeckDepthin, aiid = lc_nAIID_2 (from DeckDepthin.)
                        K. spd_t_do_faciity_meta_data.displayvalue = 'Deck Area (sq ft)' .value=/Action/Facilities/Facility/AssetSummary/AssetSummaryLineItem/AssetInventory/AssetInventoryLineItem/DeckAreaSF, aiid = lc_nAIID_2
                    for each Defect in /Action/Facilities/Facility/AssetSummary/AssetSummaryLineItem/AssetInventory/AssetInventoryLineItem/Defects
                        a. select spd_s_defects.nextval into lc_nDefectID from dual;
                        65. spd_t_ai_defects.aiid = lc_nAIID_2
                        66. spd_t_ai_defects.local_id = /Action/Facilities/Facility/AssetSummary/AssetSummaryLineItem/AssetInventory/AssetInventoryLineItem/Defects/Defect/DefectIDNo (from DefctIDNo.)
                        67. spd_t_ai_defects.defect_location = /Action/Facilities/Facility/AssetSummary/AssetSummaryLineItem/AssetInventory/AssetInventoryLineItem/Defects/Defect/DefectLocation
                        68. spd_t_ai_defects.defect_center = /Action/Facilities/Facility/AssetSummary/AssetSummaryLineItem/AssetInventory/AssetInventoryLineItem/Defects/Defect/DefectCenter
                        69. spd_t_ai_defects.criticality_factor = /Action/Facilities/Facility/AssetSummary/AssetSummaryLineItem/AssetInventory/AssetInventoryLineItem/Defects/Defect/DefectCenter
                        70. spd_t_ai_defect_meta_data
                            a. spd_t_ai_defect_meta_data.displayvalue = 'Defect Type' .value=/Action/Facilities/Facility/AssetSummary/AssetSummaryLineItem/AssetInventory/AssetInventoryLineItem/Defects/Defect/DefectType, aidefectid=lc_nDefectID
                            b. spd_t_ai_defect_meta_data.displayvalue = 'Defect TypeS tring' .value=/Action/Facilities/Facility/AssetSummary/AssetSummaryLineItem/AssetInventory/AssetInventoryLineItem/Defects/DefectTypeString, aidefectid=lc_nDefectID
                            c. spd_t_ai_defect_meta_data.displayvalue = 'Check if Repaired .value=/Action/Facilities/Facility/AssetSummary/AssetSummaryLineItem/AssetInventory/AssetInventoryLineItem/Defects/CheckifRepaired, aidefectid=lc_nDefectID
                            d. spd_t_ai_defect_meta_data.displayvalue = 'SRM Classification' .value=/Action/Facilities/Facility/AssetSummary/AssetSummaryLineItem/AssetInventory/AssetInventoryLineItem/Defects/SRMClassification, aidefectid=lc_nDefectID
                            e. spd_t_ai_defect_meta_data.displayvalue = 'Pile Sides' .value=/Action/Facilities/Facility/AssetSummary/AssetSummaryLineItem/AssetInventory/AssetInventoryLineItem/Defects/PileSides, aidefectid=lc_nDefectID
                            f. spd_t_ai_defect_meta_data.displayvalue = 'Pile P (ft)' .value=/Action/Facilities/Facility/AssetSummary/AssetSummaryLineItem/AssetInventory/AssetInventoryLineItem/Defects/PilePft, aidefectid=lc_nDefectID
                            g. spd_t_ai_defect_meta_data.displayvalue = 'Beam H (in)' .value=/Action/Facilities/Facility/AssetSummary/AssetSummaryLineItem/AssetInventory/AssetInventoryLineItem/Defects/BeamHin (from BeamHin.), aidefectid=lc_nDefectID
                            h. spd_t_ai_defect_meta_data.displayvalue = 'Beam V (in)' .value=/Action/Facilities/Facility/AssetSummary/AssetSummaryLineItem/AssetInventory/AssetInventoryLineItem/Defects/BeamVin (from BeamVin.), aidefectid=lc_nDefectID
                            i. spd_t_ai_defect_meta_data.displayvalue = 'Length L (ft)' .value=/Action/Facilities/Facility/AssetSummary/AssetSummaryLineItem/AssetInventory/AssetInventoryLineItem/Defects/LengthLft, aidefectid=lc_nDefectID
                            j. spd_t_ai_defect_meta_data.displayvalue = 'Deck X (ft)' .value=/Action/Facilities/Facility/AssetSummary/AssetSummaryLineItem/AssetInventory/AssetInventoryLineItem/Defects/DeckXft, aidefectid=lc_nDefectID
                            k. spd_t_ai_defect_meta_data.displayvalue = 'Deck Y (ft)' .value=/Action/Facilities/Facility/AssetSummary/AssetSummaryLineItem/AssetInventory/AssetInventoryLineItem/Defects/DeckYft, aidefectid=lc_nDefectID
                            l. spd_t_ai_defect_meta_data.displayvalue = 'Area SFO r' .value=/Action/Facilities/Facility/AssetSummary/AssetSummaryLineItem/AssetInventory/AssetInventoryLineItem/Defects/AreaSFOr, aidefectid=lc_nDefectID
                            m. spd_t_ai_defect_meta_data.displayvalue = 'Crack W (in)' .value=/Action/Facilities/Facility/AssetSummary/AssetSummaryLineItem/AssetInventory/AssetInventoryLineItem/Defects/CrackWin (from CrackWin.), aidefectid=lc_nDefectID
                            n. spd_t_ai_defect_meta_data.displayvalue = 'Depth D (in)' .value=/Action/Facilities/Facility/AssetSummary/AssetSummaryLineItem/AssetInventory/AssetInventoryLineItem/Defects/DepthDin (from DepthDin.), aidefectid=lc_nDefectID
                            o. spd_t_ai_defect_meta_data.displayvalue = 'Exp Bars (N)' .value=/Action/Facilities/Facility/AssetSummary/AssetSummaryLineItem/AssetInventory/AssetInventoryLineItem/Defects/ExpBarsN, aidefectid=lc_nDefectID




*/




procedure ce_xml_output(IN_ADOID number)

is
lc_nFacCount number :=0;
lc_nSpecInstCount number :=0;
lc_cDefaultsFacLaborCat clob :=' ';
lc_cDefaultsJobTitles clob := ' ';
lc_cDefaultsFacDc clob := ' ';
lc_vUICS varchar2(2000);
lc_vLocation varchar2(2000);
lc_cClob clob :='<CE>';
lc_bBlob blob;
lc_nXMLFileid number :=0;
lc_vFilename varchar2(2000);



  v_blob_offset NUMBER :=1;
  v_clob_offset NUMBER :=1;
  v_lang_context NUMBER := DBMS_LOB.DEFAULT_LANG_CTX;
  v_warning NUMBER;
  v_string_length NUMBER(10);
  v_source_locator BLOB;
  v_destination_locator BLOB;
  v_amount PLS_INTEGER;
  v_string CLOB;
begin


for rec in (
              select
                infads.activity_uic,
                max(actv.unit_title) title
              from
                spd_mv_inf_activity actv,
                spd_mv_inf_facility infads,
                spd_t_facilities fac,
                spd_t_do_facilities dofacs
              where
                dofacs.adoid = in_adoid
                and dofacs.facid = fac.facid
                and fac.infads_facility_id = infads.facility_id
                and infads.activity_uic = actv.uic
              group by
                infads.activity_uic
            )
loop
if lc_nFacCount = 0 then
  lc_vLocation := rec.title;
  lc_vUICS := rec.activity_uic;
else
  lc_vLocation := lc_vLocation || ',' || rec.title;
  lc_vUICS := lc_vUICS || ', ' || rec.activity_uic;
end if;
lc_nFacCount := 1;
end loop;

<<ACTNLOOP>>
--for actn in ce.myCe.actions.first .. ce.myCe.actions.last loop
/*!!!!!MUST MAKE THIS DYNAMIC!!!!!!*/

--get all of the activity uics from the scoped facilities:


lc_nFacCount:=0;

for rec in (
            select
              planner.productline_listid productline,
              planner.planned_year fy,
              do.contract_number,
              do.delivery_order_number,
              do.adoid,
              do.start_date,
              do.contract_listid,
              do.worktype_listid,
              do.delivery_order_name
            from
              spd_t_actn_dos do,
              spd_t_action_planner planner
            where
              do.apid = planner.apid
              and do.adoid = IN_ADOID
            )
loop
--dbms_lob.append(lc_cClob,'<CE>');
dbms_lob.append(lc_cClob, '<Action Program="' || rec.productline || '"
EIC=""
Command="NFESC"
Date="' || to_char(sysdate,'DD-MON-YYYY') || '"
blankrow=""
ContractNumber="' || rec.contract_number || '"
DeliveryOrderNumber="' || rec.delivery_order_number || '"
ACTIONSID="' || rec.adoid || '"
Activity="' || lc_vUICS || '"
Location="' || lc_vLocation || '"
FY="' || rec.fy || '"
ProjectedStartDate="' || rec.start_date || '"
WorkType="' || rec.delivery_order_name || '"
WorkTypeID="' || rec.worktype_listid || '">');

		dbms_lob.append(lc_cClob, '<baselinedirectcost>0</baselinedirectcost>');
		dbms_lob.append(lc_cClob, '<baselinelaborcost>0</baselinelaborcost>');
		dbms_lob.append(lc_cClob, '<overheadrate>0</overheadrate>');
		dbms_lob.append(lc_cClob, '<profitrate>0</profitrate>');
		dbms_lob.append(lc_cClob, '<baselinetotalcost>0</baselinetotalcost>');
		dbms_lob.append(lc_cClob, '<gcedirectcost>0</gcedirectcost>');
		dbms_lob.append(lc_cClob, '<gcelaborcost>0</gcelaborcost>');
		dbms_lob.append(lc_cClob, '<gcetotalcost>0</gcetotalcost>');
		dbms_lob.append(lc_cClob, '<factargetlaborcost>0</factargetlaborcost>');
		dbms_lob.append(lc_cClob, '<facbaselabortotal>0</facbaselabortotal>');
		dbms_lob.append(lc_cClob, '<facfieldworktotal>0</facfieldworktotal>');
		dbms_lob.append(lc_cClob, '<adjustmentrate>0</adjustmentrate>');

	<<FACLOOP>>
	--for fac in ce.myCe.facilities.first .. ce.myCe.facilities.last loop
    for fac in (
                  select
                    dofacs.dofid,
                    dofacs.facid,
                    facil.spd_fac_name,
                    facil.spd_fac_no,
                    facil.infads_facility_id
                  from
                    spd_t_do_facilities dofacs,
                    spd_t_facilities facil
                  where
                    dofacs.adoid = in_adoid and
                    dofacs.facid = facil.facid
                )
    loop
		dbms_lob.append(lc_cClob, '<Facility
				id="' || to_char(fac.dofid) || '"
				facilitiesid="' || to_char(fac.dofid) || '"
				activitiesid="0"
				GPS="0"
				priority="0"
				CI="0"
				MDI="0"
				AGE="0"
				LAST_INSPECTED="0"
				OSD="0"
				facilityName="' || to_char(fac.spd_fac_name) || '"
				facilityNo="' || to_char(fac.spd_fac_no) || '"
				nfa="' || to_char(fac.infads_facility_id) || '"
				>');


		dbms_lob.append(lc_cClob, '<FacilityDescription>');
			/*
			!!!!we need to magically get these values!!!!!!
			(
				ce_facilityextended_rec('Length','Length', rec3.length,1),
				ce_facilityextended_rec('Piles','Piles',rec3.PILECOUNT,1),
				ce_facilityextended_rec('maxwaterdepth', 'Max Water Depth',rec3.waterdepth,1),
				ce_facilityextended_rec('yearbuilt', 'Year Built',rec3.yearbuilt,1)
			);
			*/
		--<<FAC_EXT_LOOP>>
		--for facext in ce.myCe.facilities(fac).facilityextended.first .. ce.myCe.facilities(fac).facilityextended.last loop
		--dbms_lob.append(lc_cClob, '<' || to_char(ce.myCe.facilities(fac).facilityextended(facext).fieldname) || ' displayname="' || to_char(ce.myCe.facilities(fac).facilityextended(facext).displayname) || '">' || to_char(ce.myCe.facilities(fac).facilityextended(facext).displayvalue) || '</' || to_char(ce.myCe.facilities(fac).facilityextended(facext).fieldname) || '>');
		dbms_lob.append(lc_cClob, '<Length displayname="Length">0</Length>');
		dbms_lob.append(lc_cClob, '<Piles displayname="Piles">0</Piles>');
		dbms_lob.append(lc_cClob, '<maxwaterdepth displayname="Max Water Depth">0</maxwaterdepth>');
		dbms_lob.append(lc_cClob, '<yearbuilt displayname="Year Built">0</yearbuilt>');
		--end loop FAC_EXT_LOOP;
		dbms_lob.append(lc_cClob, '</FacilityDescription>');









		dbms_lob.append(lc_cClob, '<specialinstructions>');
		<<FAC_SPEC_LOOP>>

    --htp.p('facid: ' || to_char(fac.dofid));
		--for spec in ce.myCe.facilities(fac).specinstr.first .. ce.myCe.facilities(fac).specinstr.last loop
		for spec in (
                  select
                    si.*,
                    list.*,
                    listvals.displayvalue val_display
                  from
                    spd_t_do_fac_spec_inst si,
                    spd_t_lists list,
                    spd_t_lists listvals
                  where
                    dofid = fac.dofid
                    and si.spec_instr_listid = list.listid
                    and si.spec_instr_value = listvals.displayvalue(+)
                )
    loop
			dbms_lob.append(lc_cClob, '<specinstr id="' || to_char(spec.afsiid) || '"
				  displayname="' || spec.displayvalue || '"
				  adjustment="0"
				  instruction="' || spec.val_display || '"
				  />');
		end loop FAC_SPEC_LOOP;
		dbms_lob.append(lc_cClob, '</specialinstructions>');









		dbms_lob.append(lc_cClob, '<laborcategories>');
		<<FAC_LC_LOOP>>
		--for lc in ce.myCe.facilities(fac).lc.first .. ce.myCe.facilities(fac).lc.last loop
		for lc in (
					select
						*
					from
						spd_t_lists
					where
						productline_listid = rec.productline and
						listname = 'list_facility_labor_categories' and
						grouping = rec.worktype_listid
					order by
						visualorder
				)
        loop
			--we build the default based on the facility, so just make a clob and use that later in the procedure instead of relooping
			lc_cDefaultsFacLaborCat := lc_cDefaultsFacLaborCat || '<lc id="' || to_char(lc.listid) || '"
					laborcategoriesid="' || to_char(lc.listid) || '"
					productline="' || to_char(rec.productline) || '"
					lctype="' || to_char(lc.listname) || '"
					lcname="' || to_char(lc.displayvalue) || '"
					baselinetotal="0"
					gcelinetotal="0"
					/>';
			dbms_lob.append(lc_cClob, '<lc id="' || to_char(lc.listid) || '"
					laborcategoriesid="' || to_char(lc.listid) || '"
					productline="' || to_char(rec.productline) || '"
					lctype="' || to_char(lc.listname) || '"
					lcname="' || to_char(lc.displayvalue) || '"
					baselinetotal="0"
					gcelinetotal="0"
					>');



---Add the new grouping layer per contractor here:
      <<FAC_LC_CONTRACTORS>>
      for contractor in (
        select
          contractor_listid,
          (select displayvalue from spd_t_lists where listid = contractor_listid) displayvalue
        from
          spd_t_contractors_per_contract
        where
          contract_listid = rec.contract_listid



      )
      loop




			dbms_lob.append(lc_cClob,'<jobtitles id="' || contractor.contractor_listid || '" KTR="' || contractor.displayvalue || '">'); --add id and ktr attribute

			<<FAC_LC_JT_LOOP>>
			lc_cDefaultsJobTitles := '';
			for jt in (
							select
                list.*
              from
                spd_t_lists list
              where
                list.productline_listid = 100085 and
                list.listname = 'list_job_titles'
                and list.grouping = contractor.contractor_listid
              order by
                list.listid,
                list.visualorder
						)
			loop
                lc_cDefaultsJobTitles := lc_cDefaultsJobTitles || '<jobtitle id="' || to_char(jt.listid) || '"
                    jobtitlesid="' || to_char(jt.listid) || '"
                    rate="0"
                    jtname="' || to_char(jt.displayvalue)  || '"
                    productline="' || to_char(rec.productline) || '"
                    estunitsrate="0"
                    baselineunits="0"
                    levelofeffort="0"
                    baselinetotal="0"
                    gcelinetotal="0"
                    gcelineunits="0"
                    adjustmentrate="0"
                    useredited="0"
                    />';

				dbms_lob.append(lc_cClob, '<jobtitle id="' || to_char(jt.listid) || '"
						jobtitlesid="' || to_char(jt.listid) || '"
						rate="0"
						jtname="' || to_char(jt.displayvalue)  || '"
						productline="' || to_char(rec.productline) || '"
						estunitsrate="0"
						baselineunits="0"
						levelofeffort="0"
						baselinetotal="0"
						gcelinetotal="0"
						gcelineunits="0"
						adjustmentrate="0"
						useredited="0"
						/>');
			end loop FAC_LC_JT_LOOP;
			dbms_lob.append(lc_cClob, '</jobtitles>');
    end loop FAC_LC_CONTRACTORS;
			dbms_lob.append(lc_cClob, '</lc>');
		end loop FAC_LC_LOOP;
		dbms_lob.append(lc_cClob, '</laborcategories>');








		dbms_lob.append(lc_cClob, '<directcosts>');
		<<FAC_DC_LOOP>>
		lc_cDefaultsFacDc := ' ';
		--for dc in ce.myCe.facilities(fac).dc.first .. ce.myCe.facilities(fac).dc.last loop
		for dc in (
				select
				  *
				from
				  spd_t_do_facility_ce_dc dc,
				  spd_t_lists list
				where
				  dc.directcost_listid = list.listid and
				  dc.dofid = fac.dofid
				order by
				  dc.afceid
		)
		loop
			lc_cDefaultsFacDc := lc_cDefaultsFacDc || '<dc id="' || to_char(dc.afceid) || '"
					directcostsid="' || to_char(dc.listid) || '"
					rate="0"
					dcname="' || to_char(dc.displayvalue) || '"
					productline="' || to_char(rec.productline) || '"
					estunitsrate="0"
					baselineunits="0"
					baselinetotal="0"
					gcelinetotal="0"
					gcelineunits="0"
					useredited="0"
					/>';

			dbms_lob.append(lc_cClob, '<dc id="' || to_char(dc.afceid) || '"
					directcostsid="' || to_char(dc.listid) || '"
					rate="0"
					dcname="' || to_char(dc.displayvalue) || '"
					productline="' || to_char(rec.productline) || '"
					estunitsrate="0"
					baselineunits="0"
					baselinetotal="0"
					gcelinetotal="0"
					gcelineunits="0"
					useredited="0"
					/>');
		end loop FAC_DC_LOOP;
		dbms_lob.append(lc_cClob, '</directcosts>');
		dbms_lob.append(lc_cClob, '</Facility>');
	end loop FACLOOP;


--<<ACTVLOOP>>
	dbms_lob.append(lc_cClob, '<Activity name="' || lc_vLocation || '">');
	dbms_lob.append(lc_cClob, '<ActivitiesID>' || lc_vUICS || '</ActivitiesID>');
	dbms_lob.append(lc_cClob, '<GPS>0</GPS>');
	dbms_lob.append(lc_cClob, '</Activity>');

	dbms_lob.append(lc_cClob,'<defaults>');
	dbms_lob.append(lc_cClob,'<laborcategories>');
	dbms_lob.append(lc_cClob,'<facility>');

	dbms_lob.append(lc_cClob,lc_cDefaultsFacLaborCat);

	dbms_lob.append(lc_cClob,'</facility>');
	dbms_lob.append(lc_cClob,'<activity>');


	<<DEFAULTS_LC_ACTV>>
	--for lc in ce.myCe.laborcatactv.first .. ce.myCe.laborcatactv.last loop
	for lc in (
				select
				  *
				from
				  spd_t_lists
				where
				  productline_listid = rec.productline and
				  listname = 'list_delivery_order_labor_categories' and
				  grouping = rec.worktype_listid
				order by
				  visualorder
				)
	loop

		dbms_lob.append(lc_cClob, '<lc id="' || to_char(lc.listid) || '"
				laborcategoriesid="' || to_char(lc.listid) || '"
				productline="' || to_char(rec.productline) || '"
				lctype="' || to_char(lc.listname) || '"
				lcname="' || to_char(lc.displayvalue) || '"
				baselinetotal="0"
				gcelinetotal="0"
				/>');
	end loop DEFAULTS_LC_ACTV;


  dbms_lob.append(lc_cClob,'</activity>');
	dbms_lob.append(lc_cClob,'</laborcategories>');

	--dbms_lob.append(lc_cClob,lc_cDefaultsJobTitles);
	---Add the new grouping layer per contractor here:
      <<FAC_LC_CONTRACTORS>>
      for contractor in (
        select
          contractor_listid,
          (select displayvalue from spd_t_lists where listid = contractor_listid) displayvalue
        from
          spd_t_contractors_per_contract
        where
          contract_listid = rec.contract_listid



      )
      loop




			dbms_lob.append(lc_cClob,'<jobtitles id="' || contractor.contractor_listid || '" KTR="' || contractor.displayvalue || '">'); --add id and ktr attribute

			<<FAC_LC_JT_LOOP>>
			lc_cDefaultsJobTitles := '';
			for jt in (
							select
                list.*
              from
                spd_t_lists list
              where
                list.productline_listid = 100085 and
                list.listname = 'list_job_titles'
                and list.grouping = contractor.contractor_listid
              order by
                list.listid,
                list.visualorder
						)
			loop
                lc_cDefaultsJobTitles := lc_cDefaultsJobTitles || '<jobtitle id="' || to_char(jt.listid) || '"
                    jobtitlesid="' || to_char(jt.listid) || '"
                    rate="0"
                    jtname="' || to_char(jt.displayvalue)  || '"
                    productline="' || to_char(rec.productline) || '"
                    estunitsrate="0"
                    baselineunits="0"
                    levelofeffort="0"
                    baselinetotal="0"
                    gcelinetotal="0"
                    gcelineunits="0"
                    adjustmentrate="0"
                    useredited="0"
                    />';

				dbms_lob.append(lc_cClob, '<jobtitle id="' || to_char(jt.listid) || '"
						jobtitlesid="' || to_char(jt.listid) || '"
						rate="0"
						jtname="' || to_char(jt.displayvalue)  || '"
						productline="' || to_char(rec.productline) || '"
						estunitsrate="0"
						baselineunits="0"
						levelofeffort="0"
						baselinetotal="0"
						gcelinetotal="0"
						gcelineunits="0"
						adjustmentrate="0"
						useredited="0"
						/>');
			end loop FAC_LC_JT_LOOP;
			dbms_lob.append(lc_cClob, '</jobtitles>');
    end loop FAC_LC_CONTRACTORS;







	dbms_lob.append(lc_cClob,'<directcosts>');
	dbms_lob.append(lc_cClob,'<facility>');

	--<<DEFAULTS_DC_FAC>>
	dbms_lob.append(lc_cClob,lc_cDefaultsFacDC);
	--end loop DEFAULTS_DC_FAC;

	dbms_lob.append(lc_cClob,'</facility>');
	dbms_lob.append(lc_cClob,'<activity>');

	<<DEFAULTS_DC_ACTV>>
	for dc in (
				select
				  *
				from
				  spd_t_do_ce_dc dc,
				  spd_t_lists list
				where
				  dc.directcost_listid = list.listid and
				  dc.adoid = in_adoid
				order by
				  dc.docedcid
		  )
	loop
		dbms_lob.append(lc_cClob, '<dc id="' || to_char(dc.docedcid) || '"
				directcostsid="' || to_char(dc.docedcid) || '"
				rate="0"
				dcname="' || to_char(dc.displayvalue) || '"
				productline="' || to_char(rec.productline) || '"
				estunitsrate="0"
				baselineunits="0"
				baselinetotal="0"
				gcelinetotal="0"
				gcelineunits="0"
				useredited="0"
				/>');
	end loop DEFAULTS_DC_ACTV;
	dbms_lob.append(lc_cClob,'</activity>');
	dbms_lob.append(lc_cClob,'</directcosts>');
	dbms_lob.append(lc_cClob,'</defaults>');

	dbms_lob.append(lc_cClob,'<laborcategories>');
	<<ACTV_LC_LOOP>>
	for lc in (
				select
					*
				from
					spd_t_lists
				where
					productline_listid = rec.productline and
					listname = 'list_delivery_order_labor_categories' and
					grouping = rec.worktype_listid
				order by
					visualorder
			)
	loop
		dbms_lob.append(lc_cClob, '<lc id="' || to_char(lc.listid) || '"
				laborcategoriesid="' || to_char(lc.listid) || '"
				productline="' || to_char(rec.productline) || '"
				lctype="' || to_char(lc.listname) || '"
				lcname="' || to_char(lc.displayvalue) || '"
				baselinetotal="0"
				gcelinetotal="0"
				>');




---Add the new grouping layer per contractor here:
      <<FAC_LC_CONTRACTORS>>
      for contractor in (
        select
          contractor_listid,
          (select displayvalue from spd_t_lists where listid = contractor_listid) displayvalue
        from
          spd_t_contractors_per_contract
        where
          contract_listid = rec.contract_listid



      )
      loop




			dbms_lob.append(lc_cClob,'<jobtitles id="' || contractor.contractor_listid || '" KTR="' || contractor.displayvalue || '">'); --add id and ktr attribute


		<<FAC_LC_JT_LOOP>>
		lc_cDefaultsJobTitles := '';
		for jt in (
						select
              *
            from
              spd_t_lists
            where
              productline_listid = rec.productline
              and listname = 'list_job_titles'
              and grouping = contractor.contractor_listid
            order by
              grouping,
              visualorder
					)
		loop

            dbms_lob.append(lc_cClob, '<jobtitle id="' || to_char(jt.listid) || '"
                    jobtitlesid="' || to_char(jt.listid) || '"
                    rate="0"
                    jtname="' || to_char(jt.displayvalue)  || '"
                    productline="' || to_char(rec.productline) || '"
                    estunitsrate="0"
                    baselineunits="0"
                    levelofeffort="0"
                    baselinetotal="0"
                    gcelinetotal="0"
                    gcelineunits="0"
                    adjustmentrate="0"
                    useredited="0"
                    />');
            --dbms_lob.append(lc_cClob, '</jobtitle>');
        end loop FAC_LC_JT_LOOP;
        dbms_lob.append(lc_cClob, '</jobtitles>');
    end loop;
        dbms_lob.append(lc_cClob, '</lc>');
	end loop ACTV_LC_LOOP;
	dbms_lob.append(lc_cClob, '</laborcategories>');

  dbms_lob.append(lc_cClob, '<directcosts>');
	<<ACTV_DC_LOOP>>
	for dc in (
      select
			  *
			from
			  spd_t_do_ce_dc dc,
			  spd_t_lists list
			where
			  dc.directcost_listid = list.listid and
			  dc.adoid = in_adoid
			order by
			  dc.docedcid
	  )
	  loop
	  dbms_lob.append(lc_cClob, '<dc id="' || to_char(dc.docedcid) || '"
			  directcostsid="' || to_char(dc.docedcid) || '"
			  rate="0"
			  dcname="' || to_char(dc.displayvalue) || '"
			  productline="' || to_char(rec.productline) || '"
			  estunitsrate="0"
			  baselineunits="0"
			  baselinetotal="0"
			  gcelinetotal="0"
			  gcelineunits="0"
			  useredited="0"
			  />');
	  --dbms_lob.append(lc_cClob, '</dc>');
	end loop ACTV_DC_LOOP;
	dbms_lob.append(lc_cClob, '</directcosts>');


			dbms_lob.append(lc_cClob, '</Action>');
  lc_vFilename := rec.delivery_order_name || '.xml';

	end loop ACTNLOOP;

dbms_lob.append(lc_cClob,'</CE>');



lc_bBlob := CLOB2BLOB(lc_cClob);

select SPIDERS_DATA.SPD_S_XML_FILES.nextval into lc_nXMLFileid from dual;


insert into spd_t_xml_files (xmlfileid, mime_type, blob_content, apexname,  filename)
values (lc_nXMLFileid,'text/xml',lc_bBlob, lc_vFilename, lc_vFilename);
commit;

htp.p(to_char(lc_nXMLFileid));



END ce_xml_output;



function import_xml_facility_files(IN_nADOID number, IN_nXMLFILEID number default 0) return number as

lc_xAction xmltype;
lc_nADOID number := IN_nADOID;--100181;--100160,100181
lc_xFacilitiesNode xmltype;
lc_jJson json;

begin
    for rec_action in
        (
          SELECT
            xml_table.column_value action
          FROM
            spd_t_xml_files source_table,
            XMLTable('/Action' passing source_table.xml_document) xml_table
          where
            source_table.xmlfileid = IN_nXMLFILEID
        )
    loop
        lc_xAction := rec_action.action;

    /*------------FACILITIES OF ACTION-------------------*/

        select extract(lc_xAction, '/Action/Facilities') into lc_xFacilitiesNode from dual;

    /*------------FACILITY OF FACILITIES-----------------*/

        for rec_facilities in (
            SELECT
                xml_table.column_value facility, rownum rn
            FROM
                XMLTable('/Facilities/Facility' passing lc_xFacilitiesNode) xml_table
            )
        loop

/*
we want to update this function to
1. Add NFAID values to the Delivery Order Facility Table, if no NFA matches
2. We want to then add the same data to the spd_t_xml_facilities_match
3. Then we want to match the data automatically
4. DO WE WANT TO MATCH BASED ON NFAID Automatically? (i'm going yes for now)
*/


            BB_P_XML_FACILITIES_MATCH(
              IN_nADOID,
              rec_facilities.facility.extract('/Facility/FacilityName/text()').getstringval(),
              rec_facilities.facility.extract('/Facility/NFAID/text()').getstringval(),
              rec_facilities.facility.extract('/Facility/FacilityNo/text()').getstringval(),
              rec_facilities.facility.extract('/Facility/SPIDERS_FACILITYID/text()').getstringval()
            );


        end loop; --facilities loop

        lc_jJson := json('{"data":[{"ADOID":' || IN_nADOID || '}]}');


        execute immediate 'insert into debug (functionname, input, message) values (''XML_TOOLS.import_xml_facility_files'',''' || json_printer.pretty_print(lc_jJson) || ''',''info: '')';


        BB_P_UPDATE_XML_FAC_COMPLETE(lc_jJson);


    end loop;--action loop

    return 1;

    exception
    when others then
      execute immediate 'insert into debug (functionname, input, message) values (''XML_TOOLS.import_xml_facility_files'',''' || json_printer.pretty_print(lc_jJson) || ''',''error: ' || sqlerrm || ''')';
      return 0;

end;

end;
--END Update XML_TOOLS package body

/
