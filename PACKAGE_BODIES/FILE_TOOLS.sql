create or replace PACKAGE BODY                "FILE_TOOLS" as


/*
This software has been released under the MIT license:

  Copyright (c) 2011 Synergy Software Design, LLC

  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.
*/


FUNCTION blob_to_clob (blob_in IN BLOB) return clob;

procedure upload_wf_xml(IN_nADOID number, IN_vFILETYPE varchar2 default 'deliverable') as
lc_cClob clob;
lc_nXML number := 0;
lc_nAPID number :=0;


  lc_jJson json;
  lc_data json_list;
  lc_rec_val json_value;
  lc_rec json;
  lc_DOFID_val json_value; --
  lc_nDOFID number;
  lc_xmlFacilityId_val json_value;
  lc_nXmlFacilityId number;

  lc_ret_str varchar(512);
  lc_ret json;

  lc_jResults json;
  lc_jlDML json_list;

  lc_jMessages json;
  lc_jlMessages json_list;
  lc_bReturn varchar2(1);

  --error stuff
  lc_jError json;
  json_error EXCEPTION;

begin
   --
   -- When an image is uploaded it is stored in the
   -- FLOW_FILES.WWV_FLOW_FILE_OBJECTS$ table.
   -- The wwv_flow_file_objects$ table is identified in the dads.conf
   -- file in the PlsqlDocumentTablename  parameter.
   -- WWV_FLOW_FILES is a view to this table.
   --
   -- We want to keep the image in our own table.
   -- Copy the image from WWV_FLOW_FILES to our own table_with_xml_column table.
   --


select apid into lc_nApid from spd_t_actn_dos where adoid = IN_nADOID;

xml_tools.p(in_nAPID =>lc_nApid, in_nADOID => in_nADOID, in_nDOFID =>0, in_vFilename => v('P3_FILE_UPLOAD'), in_vImportSection => 'XML File Upload', in_vNotes =>'BEGIN', in_dRunDate => sysdate);



  for rec in
  (
    select
      BLOB_CONTENT,
      mime_type
    from
      wwv_flow_files
    where
      name = v('P3_FILE_UPLOAD')
  )
  loop
    if rec.mime_type = 'text/xml' then
      lc_nXML := 1;
    end if;
    insert into debug(column1) values ('did this get here inside loop before blob to clob');
    lc_cClob := blob_to_clob(rec.blob_content);
  end loop;



  /*if this is an xml document, then process it as such, otherwise just copy the file to the blob content*...will need to make this more robust later*/
if lc_nXML = 1 then

    insert into debug(column1) values ('starting insert into t_xml ' || lc_nXML);
  insert into spd_t_xml_files
  (
    ADOID,
    APEXNAME,
    FILENAME,
    TITLE,
    MIME_TYPE,
    DOC_SIZE,
    DAD_CHARSET,
    CREATED_BY,
    CREATED_ON,
    UPDATED_BY,
    UPDATED_ON,
    LAST_UPDATED,
    CONTENT_TYPE,
    BLOB_CONTENT,
    LANGUAGE,
    DESCRIPTION,
    FILE_TYPE,
    FILE_CHARSET,
    xml_document
  )
  select
    IN_nADOID,
    wwv_flow_files.NAME,
    wwv_flow_files.FILENAME,
    wwv_flow_files.TITLE,
    wwv_flow_files.MIME_TYPE,
    wwv_flow_files.DOC_SIZE,
    wwv_flow_files.DAD_CHARSET,
    wwv_flow_files.CREATED_BY,
    wwv_flow_files.CREATED_ON,
    wwv_flow_files.UPDATED_BY,
    wwv_flow_files.UPDATED_ON,
    wwv_flow_files.LAST_UPDATED,
    wwv_flow_files.CONTENT_TYPE,
    wwv_flow_files.BLOB_CONTENT,
    wwv_flow_files.LANGUAGE,
    wwv_flow_files.DESCRIPTION,
    IN_vFILETYPE,
    wwv_flow_files.FILE_CHARSET,
    XMLType(lc_cClob)
  from
    wwv_flow_files
  where
    name = v('P3_FILE_UPLOAD');
else
    insert into debug(column1) values ('starting insert into t_xml ' || lc_nXML);
  insert into spd_t_xml_files
  (
    ADOID,
    APEXNAME,
    FILENAME,
    TITLE,
    MIME_TYPE,
    DOC_SIZE,
    DAD_CHARSET,
    CREATED_BY,
    CREATED_ON,
    UPDATED_BY,
    UPDATED_ON,
    LAST_UPDATED,
    CONTENT_TYPE,
    BLOB_CONTENT,
    LANGUAGE,
    DESCRIPTION,
    FILE_TYPE,
    FILE_CHARSET
  )
  select
    IN_nADOID,
    wwv_flow_files.NAME,
    wwv_flow_files.FILENAME,
    wwv_flow_files.TITLE,
    wwv_flow_files.MIME_TYPE,
    wwv_flow_files.DOC_SIZE,
    wwv_flow_files.DAD_CHARSET,
    wwv_flow_files.CREATED_BY,
    wwv_flow_files.CREATED_ON,
    wwv_flow_files.UPDATED_BY,
    wwv_flow_files.UPDATED_ON,
    wwv_flow_files.LAST_UPDATED,
    wwv_flow_files.CONTENT_TYPE,
    wwv_flow_files.BLOB_CONTENT,
    wwv_flow_files.LANGUAGE,
    wwv_flow_files.DESCRIPTION,
    IN_vFILETYPE,
    wwv_flow_files.FILE_CHARSET
  from
    wwv_flow_files
  where
    name = v('P3_FILE_UPLOAD');
end if;
    --
    -- Now that we have copied the image to our own EASY_IMAGE table
   -- delete it from the WWV_FLOW_FILES table.  That way we can keep
   -- the files from growing too much in the WWV_FLOW_FILES and make
   -- it easier for use to make backups of our data.
   -- Deleting the record is done by referencing the NAME column.
   -- When the file is uploaded the P14_NAME page item will be set to
   -- the value of NAME that was put into the WWV_FLOW_FILES table
   -- and we can use it for reference.
   --

xml_tools.p(in_nAPID =>lc_nApid, in_nADOID => in_nADOID, in_nDOFID =>0, in_vFilename => v('P3_FILE_UPLOAD'), in_vImportSection => 'XML File Upload', in_vNotes =>'COMPLETE', in_dRunDate => sysdate);


delete from wwv_flow_files where name = v('P3_FILE_UPLOAD');


  exception
    when others then
        lc_jError := json();
        lc_jError.put('function','XML File Upload');
        lc_jError.put('input',v('P3_FILE_UPLOAD'));
        lc_jlMessages := json_list();
        lc_jlMessages.append(json_list('[{error:''FU-0001 ' || SQLERRM || '''}]'));
        lc_jError.put('messages',lc_jlMessages);
        BB_P_ERROR(lc_jError);
end;


FUNCTION blob_to_clob (blob_in IN BLOB)
RETURN CLOB
AS
	v_clob    CLOB;
	v_varchar VARCHAR2(32767);
	v_start	 PLS_INTEGER := 1;
	v_buffer  PLS_INTEGER := 32767;
BEGIN
	DBMS_LOB.CREATETEMPORARY(v_clob, TRUE);

	FOR i IN 1..CEIL(DBMS_LOB.GETLENGTH(blob_in) / v_buffer)
	LOOP

	   v_varchar := UTL_RAW.CAST_TO_VARCHAR2(DBMS_LOB.SUBSTR(blob_in, v_buffer, v_start));

           DBMS_LOB.WRITEAPPEND(v_clob, LENGTH(v_varchar), v_varchar);

		v_start := v_start + v_buffer;
	END LOOP;

   RETURN v_clob;

  exception
        when others then
        htp.p('error blob to clob: ' ||
        SQLERRM
        );

END blob_to_clob;



PROCEDURE download_xml_file(in_nFileskey in number) AS
        v_mime  VARCHAR2(48);
        v_length  NUMBER;
        v_file_name VARCHAR2(2000);
        Lob_loc  BLOB;
        lc_nXMLFileid number :=in_nFileskey;
BEGIN
/*
for rec in (select filesid from spd_t_inter_files where fileskey = in_nFileskey)
loop
  --limiting factor is that if there are more than one files for this fileskey we dont have an ability to deal with that without more parameters
  lc_nXMLFileid := rec.filesid;

end loop;
*/

insert into debug (functionname, input) values ('download_xml_file',in_nFileskey);

        SELECT MIME_TYPE, blob_content, apexname name,DBMS_LOB.GETLENGTH(blob_content)
                INTO v_mime,lob_loc,v_file_name,v_length
                FROM spd_t_xml_files
                WHERE xmlfileid = lc_nXMLFileid;
              --
              -- set up HTTP header
              --
                    -- use an NVL around the mime type and
                    -- if it is a null set it to application/octect
                    -- application/octect may launch a download window from windows
                    owa_util.mime_header( nvl(v_mime,'application/octet'), FALSE );

                -- set the size so the browser knows how much to download
                htp.p('Content-length: ' || v_length);
                -- the filename will be used by the browser if the users does a save as
                htp.p('Content-Disposition:  attachment; filename="'||replace(replace(substr(v_file_name,instr(v_file_name,'/')+1),chr(10),null),chr(13),null)|| '"');
                -- close the headers
                owa_util.http_header_close;
                -- download the BLOB
                wpg_docload.download_file( Lob_loc );
end download_xml_file;


procedure delete_file(in_nFileskey number, in_nListid number, in_nAdoid number default 0)

is
lc_nFileid number :=0;

begin

for rec in (select filesid from spd_t_inter_files where fileskey = in_nFileskey)
loop
  --limiting factor is that if there are more than one files for this fileskey we dont have an ability to deal with that without more parameters
  lc_nFileid := rec.filesid;

end loop;

  delete from spd_t_xml_files where xmlfileid = lc_nFileid;
  delete from spd_t_inter_files where filesid = lc_nFileid and fileskey = in_nFileskey;
  delete from spd_t_do_deliverables where deliverables_listid = in_nListid and adoid = in_nAdoid;
  delete from spd_t_do_ce_files where ce_listid = in_nListid and adoid = in_nAdoid;

commit;

tobrowser.message('success','This file has been deleted');



  exception
        when others then
        tobrowser.message('error', 'deleting file: ' ||
        'in_nFileskey: ' || to_char(in_nFileskey) || ',
        in_nListid: ' || to_char(in_nListid) || ',
        in_nAdoid: ' || to_char(in_nAdoid) || ' : ' ||
        SQLERRM
        );

end;




PROCEDURE display_image(IN_NTHUMBNAILID number)
AS

 l_mime        VARCHAR2 (255);
   l_length      NUMBER;
   l_file_name   VARCHAR2 (2000);
   lob_loc       BLOB;
BEGIN

         SELECT 'image/jpeg', thumbnail, DBMS_LOB.getlength (thumbnail)
         INTO l_mime, lob_loc, l_length
         FROM spd_t_thumbnails
         WHERE thumbnailid = IN_NTHUMBNAILID;


   OWA_UTIL.mime_header (NVL (l_mime, 'application/octet'), FALSE);
   HTP.p ('Content-length: ' || l_length);
   OWA_UTIL.http_header_close;
   WPG_DOCLOAD.download_file (lob_loc);

END;


PROCEDURE display_thumbnail(IN_NTHUMBNAILID number)
AS

  l_mime        VARCHAR2 (255);
  l_length      NUMBER;
  l_file_name   VARCHAR2 (2000);
  lob_loc       BLOB;
BEGIN

         SELECT 'image/jpeg', carousel, DBMS_LOB.getlength (carousel)
         INTO l_mime, lob_loc, l_length
         FROM spd_t_thumbnails
         WHERE thumbnailid = IN_NTHUMBNAILID;


   OWA_UTIL.mime_header (NVL (l_mime, 'application/octet'), FALSE);
   HTP.p ('Content-length: ' || l_length);
   OWA_UTIL.http_header_close;
   WPG_DOCLOAD.download_file (lob_loc);

END;

procedure process_wf_files (in_nADOID number)


as


begin
--Load the xml data
  --when a file is referenced, check the endpoint path. if it exists in spd_t_folders then use the folderid, otherwise make a new record in the table, add this record to the database

--return the result using tobrowser

--copy the spiders files to the loaded directory

--post-process the files

  --check if file exists
    --if exists make a record in the database or update the database record to say file exists
      --if image file, crop and make thumbnail, add to thumbnail blob or thumbnail table

    --end if

null;


end;


PROCEDURE download_bfile (
      IN_nFileID        IN   VARCHAR2,
      IN_vSource   IN   VARCHAR2 DEFAULT 'DATA_PUMP'
   )
   AS
      lob_loc    BFILE;
      v_mime     VARCHAR2 (48) DEFAULT 'application/octet';
      v_length   NUMBER;
      lc_vFile varchar2(255);
      lc_vDirectory varchar2(255);
      lc_nContinue number :=0;
   BEGIN
      if in_vSource = 'do' then
        for rec in (select folderid, file_name from spd_t_do_supporting_files where dosfid = in_nFileid and dbms_lob.fileexists(the_bfile) !=0)
        loop
          lc_vFile := rec.file_name;
          lc_vDirectory := 'SPD_' || rec.folderid;
          lc_nContinue := 1;
        end loop;
      end if;



      if in_vSource = 'dof' then
        for rec in (select folderid, file_name from spd_t_do_fac_supporting_files where dofsfid = in_nFileid and dbms_lob.fileexists(the_bfile) !=0)
        loop
          lc_vFile := rec.file_name;
          lc_vDirectory := 'SPD_' || rec.folderid;
          lc_nContinue := 1;
        end loop;
      end if;


      if in_vSource = 'report' then
       for rec in (select folderid, the_bfile from spd_t_report_files where reportfileid = in_nFileid and dbms_lob.fileexists(the_bfile) !=0)
        loop
          lc_vDirectory := 'SPD_' || rec.folderid;
          lc_vFile := getfilenamefrombfile(rec.the_bfile);
          lc_nContinue := 1;
          
          --htp.p(lc_vFile || ':' || lc_vDirectory);

        end loop;
        --htp.p(IN_nFileID || ':' || in_vSource || ':' || lc_vDirectory || ':' || lc_vFile || ':' || lc_nContinue);

      end if;


      if lc_nContinue = 1 then
      

        lob_loc := BFILENAME (UPPER (lc_vDirectory), lc_vFile);
        v_length := DBMS_LOB.getlength (lob_loc);

        --htp.p(DBMS_LOB.getlength (lob_loc));

        --
        -- set up HTTP header
        --
        --htp.p('v_length:' || v_length);
              -- use an NVL around the mime type and
              -- if it is a null set it to application/octect
              -- application/octect may launch a download window from windows
        --OWA_UTIL.mime_header (NVL (v_mime, 'application/octet'), FALSE);
        OWA_UTIL.mime_header ('application/octet', FALSE);
        


        -- set the size so the browser knows how much to download
        HTP.p ('Content-length: ' || v_length);
        -- the filename will be used by the browser if the users does a save as
        HTP.p ('Content-Disposition:  attachment; filename="'|| lc_vFile|| '"');
              

      
        -- close the headers
        OWA_UTIL.http_header_close;
        -- download the BLOB
        WPG_DOCLOAD.download_file (lob_loc);

      else
        htp.p('File does not exist');
      end if;

exception
        when others then
        tobrowser.message('error','error downloading bfile: ' || SQLERRM
        );

   END download_bfile;






function convertBlobToXML (in_nADOID number) return number as
   --lc_cClob clob;
   
    lc_bBlob blob;
    lc_nReturn number :=0;
    lc_xXML xmltype;
    lc_vDataReqRevision varchar2(1000);
    lc_nFileID number;

    lc_jJson json;
    lc_jlResults json_list;
    lc_rec_val json_value;
    lc_rec json;
    lc_cnt number;

    lc_jResults json;
    lc_jMessages json;
    lc_jlMessages json_list;
    lc_vReturn varchar2(1);
    lc_vDML varchar2(32000);
    lc_cClob clob;
    --error stuff
    lc_jError json;
    json_error EXCEPTION;


begin
    lc_jMessages := json();
    lc_jlMessages := json_list();
    
    select xmlfileid into lc_nFileID from spd_t_xml_files where adoid = in_nADOID;

    select blob_content into lc_bBlob from spd_t_xml_files where xmlfileid = lc_nFileID;
    lc_cClob := blob_to_clob(lc_bBlob);
    lc_cClob := REPLACE(lc_cClob,chr(9), ' ');
    lc_cClob := REPLACE(lc_cClob,chr(10),' ');
    lc_cClob := REPLACE(lc_cClob,chr(13),' ');
    lc_xXML  := XMLType(lc_cClob);
    --lc_xXML := XMLType(blob_to_clob(lc_bBlob));
    update spd_t_xml_files set xml_document = lc_xXML  where xmlfileid = lc_nFileID;



    lc_vDataReqRevision := 'Error';
    for rec_action in
    (
        SELECT
            xml_table.column_value action
        FROM
            spd_t_xml_files source_table,
            XMLTable('/Action' passing source_table.xml_document) xml_table
        where
            source_table.xmlfileid = lc_nFileID
    )
    loop

       lc_vDataReqRevision:= rec_action.action.extract('/Action/DataReqRevision/text()').getstringval();

    end loop;

 
    if lc_vDataReqRevision <> 'Error' then
        insert into spd_t_import_log (adoid, import_section, grouping,total_records,count_of_records) values (in_nADOID, 'FILE UPLOADED','FILE UPLOADED',1,1);
    end if;

    lc_nReturn := 1;

    return lc_nReturn;

    exception
        when others then
            lc_jError := json();
            lc_jError.put('function','convertBlobToXML');
            lc_jError.put('module','file_upload');
            lc_jError.put('target','');
            lc_jError.put('ref_id','');
            lc_jError.put('input','');
            lc_jlMessages.append(json_list('[{error:''FU.000X ' || SQLERRM || '''}]'));
            lc_jError.put('messages',lc_jlMessages);
            BB_P_ERROR(lc_jError);
            return lc_nReturn;



end;



function get_MimeType (in_nFileid number) return varchar2 as

    lc_vReturn varchar2(1000);

    lc_jJson json;
    lc_jlResults json_list;
    lc_rec_val json_value;
    lc_rec json;
    lc_cnt number;

    lc_jResults json;
    lc_jMessages json;
    lc_jlMessages json_list;
    lc_vDML varchar2(32000);
    lc_cClob clob;
    --error stuff
    lc_jError json;
    json_error EXCEPTION;

begin
    lc_jMessages := json();
    lc_jlMessages := json_list();

    select mime_type into lc_vReturn from spd_t_xml_files where xmlfileid = in_nFileid;

    return lc_vReturn;

    exception
        when others then
            lc_jError := json();
            lc_jError.put('function','get_MimeType');
            lc_jError.put('module','file_upload');
            lc_jError.put('target','');
            lc_jError.put('ref_id','');
            lc_jError.put('input','');
            lc_jlMessages.append(json_list('[{error:''FU.000X ' || SQLERRM || '''}]'));
            lc_jError.put('messages',lc_jlMessages);
            BB_P_ERROR(lc_jError);
            return 'Error';



end;







function get_LoadID (in_vFolderid varchar2) return number as

    lc_nReturn number := 0;

    lc_jJson json;
    lc_jlResults json_list;
    lc_rec_val json_value;
    lc_rec json;
    lc_cnt number;

    lc_jResults json;
    lc_jMessages json;
    lc_jlMessages json_list;
    lc_vReturn varchar2(1);
    lc_vDML varchar2(32000);
    lc_cClob clob;
    --error stuff
    lc_jError json;
    json_error EXCEPTION;

begin
    lc_jMessages := json();
    lc_jlMessages := json_list();

    select
        loadid into lc_nReturn
    from
        spd_t_load_progress
    where
        foldername = in_vFolderid;


    return lc_nReturn;

    exception
        when others then
            lc_jError := json();
            lc_jError.put('function','get_LoadID');
            lc_jError.put('module','file_upload');
            lc_jError.put('target','');
            lc_jError.put('ref_id','');
            lc_jError.put('input','');
            lc_jlMessages.append(json_list('[{error:''FU.000X ' || SQLERRM || '''}]'));
            lc_jError.put('messages',lc_jlMessages);
            BB_P_ERROR(lc_jError);
            return 0;



end;

function get_FolderID (in_vFolder varchar2) return varchar2 as

    lc_vReturn varchar2(1000) := '';
    lc_vFolderid varchar2(1000) :='';
    
    lc_jJson json;
    lc_jlResults json_list;
    lc_rec_val json_value;
    lc_rec json;
    lc_cnt number;

    lc_jResults json;
    lc_jMessages json;
    lc_jlMessages json_list;
    lc_vDML varchar2(32000);
    lc_cClob clob;
    --error stuff
    lc_jError json;
    json_error EXCEPTION;

begin
    lc_jMessages := json();
    lc_jlMessages := json_list();

    
    for rec in (
        select
            folderid 
        from
            spd_t_folders
        where
            folder_name = in_vFolder
    )
    loop
      lc_vFolderid := 'SPD_' || rec.folderid;
    end loop;


    return lc_vReturn;

    exception
        when others then
            lc_jError := json();
            lc_jError.put('function','get_FolderID');
            lc_jError.put('module','file_upload');
            lc_jError.put('target','');
            lc_jError.put('ref_id','');
            lc_jError.put('input','');
            lc_jlMessages.append(json_list('[{error:''FU.000X ' || SQLERRM || '''}]'));
            lc_jError.put('messages',lc_jlMessages);
            BB_P_ERROR(lc_jError);
            return 'Error';



end;


function get_FolderName (in_nPostUploadID number) return varchar2 as

    lc_vReturn varchar2(1000) := '';

    lc_jJson json;
    lc_jlResults json_list;
    lc_rec_val json_value;
    lc_rec json;
    lc_cnt number;

    lc_jResults json;
    lc_jMessages json;
    lc_jlMessages json_list;
    lc_vDML varchar2(32000);
    lc_cClob clob;
    --error stuff
    lc_jError json;
    json_error EXCEPTION;

begin
    lc_jMessages := json();
    lc_jlMessages := json_list();
    
    for rec in (
        select
            delivery_order_name
        from
            spd_t_actn_dos
        where
            adoid = in_nPostUploadID
    )
    loop    
      lc_vReturn := rec.delivery_order_name;
    end loop;

    return lc_vReturn;

    exception
        when others then
            lc_jError := json();
            lc_jError.put('function','get_FolderName');
            lc_jError.put('module','file_upload');
            lc_jError.put('target','');
            lc_jError.put('ref_id','');
            lc_jError.put('input','');
            lc_jlMessages.append(json_list('[{error:''FU.000X ' || SQLERRM || '''}]'));
            lc_jError.put('messages',lc_jlMessages);
            BB_P_ERROR(lc_jError);
            return 'Error';



end;

function get_FileName (in_vP3FileUpload varchar2) return varchar2 as

    lc_vReturn varchar2(1000) := '';

    lc_jJson json;
    lc_jlResults json_list;
    lc_rec_val json_value;
    lc_rec json;
    lc_cnt number;

    lc_jResults json;
    lc_jMessages json;
    lc_jlMessages json_list;
    lc_vDML varchar2(32000);
    lc_cClob clob;
    --error stuff
    lc_jError json;
    json_error EXCEPTION;

begin
    lc_jMessages := json();
    lc_jlMessages := json_list();


    select
        wwv_flow_files.FILENAME into lc_vReturn
    from
        wwv_flow_files
    where
        name = in_vP3FileUpload;

    return lc_vReturn;

    exception
        when others then
            lc_jError := json();
            lc_jError.put('function','get_FileName');
            lc_jError.put('module','file_upload');
            lc_jError.put('target','');
            lc_jError.put('ref_id','');
            lc_jError.put('input','');
            lc_jlMessages.append(json_list('[{error:''FU.000X ' || SQLERRM || '''}]'));
            lc_jError.put('messages',lc_jlMessages);
            BB_P_ERROR(lc_jError);
            return 'Error';



end;


function file_upload_dml(in_cDML clob, in_nAdoid number, in_vFilename varchar2, in_vImportSection varchar2, in_cClob clob default '') return number as

    lc_jJson json;
    lc_jlResults json_list;
    lc_rec_val json_value;
    lc_rec json;
    lc_cnt number;

    lc_jResults json;
    lc_jMessages json;
    lc_jlMessages json_list;
    lc_vReturn varchar2(1);
    lc_vDML varchar2(32000);
    lc_cClob clob;
    --error stuff
    lc_jError json;
    json_error EXCEPTION;

begin
    lc_jMessages := json();
    lc_jlMessages := json_list();

    execute immediate in_cDML;

    return 1;

    exception
        when others then
            lc_jError := json();
            lc_jError.put('function','import_xml');
            lc_jError.put('module','xmlImportManager');
            lc_jError.put('target',in_vImportSection);
            lc_jError.put('ref_id',in_nAdoid);
            lc_jError.put('input','');
            lc_jlMessages.append(json_list('[{error:''IX.000X ' || SQLERRM || '''}]'));
            lc_jError.put('messages',lc_jlMessages);
            BB_P_ERROR(lc_jError);
            return 0;

end;



procedure upload_file (IN_vFILE_TYPE varchar2, IN_vPost_Upload_Type varchar2, IN_nPost_Upload_Id number, IN_nLISTID number)


is
    lc_nDocumentID number :=0;
    lc_nContinue number :=0;
    lc_nFileid number :=0;
    lc_nFileskey number :=0;
    lc_vSuccess varchar2(255);
    lc_vMessage varchar2(255);
    lc_vFolder varchar2(255);
    lc_vFolderid varchar2(255);
    lc_nLoadid number;
    lc_vMimeType varchar2(255);
    lc_vFilename varchar2(1000);
    lc_xXML xmltype;
    lc_bBlob blob;
    lc_nXML number :=0;
    lc_nXMLFacilitiesImport number :=0;
    
    lc_cDML clob;
    lc_nDML number;

    lc_jJson json;
    lc_jlResults json_list;
    lc_rec_val json_value;
    lc_rec json;
    lc_cnt number;

    lc_jResults json;
    lc_jMessages json;
    lc_jlMessages json_list;
    lc_vReturn varchar2(1);
    lc_vDML varchar2(32000);
    lc_cClob clob;
    --error stuff
    lc_jError json;
    json_error EXCEPTION;


begin

    lc_jResults := json('{}');
    lc_jMessages := json('{}');
    lc_jlMessages := json_list('[]');



    select SPIDERS_DATA.SPD_S_XML_FILES.nextval into lc_nFileid from dual;
    select SPIDERS_DATA.SPD_S_FILESKEY.nextval into lc_nFileskey from dual;
  
    lc_cDML := 'delete from spd_t_import_log where adoid = ' || IN_nPost_Upload_Id;
    lc_nDML := file_upload_dml(lc_cDML, IN_nPost_Upload_Id,lc_vFilename,'DELETE LOG UPLOAD');
 
 
    lc_cDML := 'delete from spd_t_xml_files where adoid = ' || IN_nPost_Upload_Id;
    lc_nDML := file_upload_dml(lc_cDML, IN_nPost_Upload_Id,lc_vFilename,'DELETE FILE UPLOAD');
    
    lc_cDML := 'delete from spd_t_xml_facilities_match where adoid = ' || IN_nPost_Upload_Id;
    lc_nDML := file_upload_dml(lc_cDML, IN_nPost_Upload_Id,lc_vFilename,'DELETE XML FACILITY MATCH');
    
    lc_cDML := 'delete from debug where ref_id = ' || IN_nPost_Upload_Id;
    lc_nDML := file_upload_dml(lc_cDML, IN_nPost_Upload_Id,lc_vFilename,'DELETE ERRORS');
    
    
    lc_cDML := 'insert into spd_t_xml_files
    (
        XMLFILEID,
        ADOID,
        APEXNAME,
        FILENAME,
        TITLE,
        MIME_TYPE,
        DOC_SIZE,
        DAD_CHARSET,
        CREATED_BY,
        CREATED_ON,
        UPDATED_BY,
        UPDATED_ON,
        LAST_UPDATED,
        CONTENT_TYPE,
        BLOB_CONTENT,
        LANGUAGE,
        DESCRIPTION,
        FILE_TYPE,
        FILE_CHARSET
    )
        select
        ' || lc_nFileid || ',
        ' || IN_nPost_Upload_Id || ',
        wwv_flow_files.NAME,
        wwv_flow_files.FILENAME,
        wwv_flow_files.TITLE,
        wwv_flow_files.MIME_TYPE,
        wwv_flow_files.DOC_SIZE,
        wwv_flow_files.DAD_CHARSET,
        wwv_flow_files.CREATED_BY,
        wwv_flow_files.CREATED_ON,
        wwv_flow_files.UPDATED_BY,
        wwv_flow_files.UPDATED_ON,
        wwv_flow_files.LAST_UPDATED,
        wwv_flow_files.CONTENT_TYPE,
        wwv_flow_files.BLOB_CONTENT,
        wwv_flow_files.LANGUAGE,
        wwv_flow_files.DESCRIPTION,
        ''' || IN_vFILE_TYPE || ''',
        wwv_flow_files.FILE_CHARSET
    from
        wwv_flow_files
    where
        name = ''' ||  v('P3_FILE_UPLOAD') || '''';



    
    lc_vFilename := get_FileName(v('P3_FILE_UPLOAD'));
    lc_nDML := file_upload_dml(lc_cDML, IN_nPost_Upload_Id,lc_vFilename,'FILE UPLOAD');



    if lc_nDML = 1 then --100085_XML formerly

        --we need to get the base directory name, which is the do_name. use the IN_nPost_Upload_Id to get it;
        lc_vFolder := get_FolderName(IN_nPost_Upload_Id);
        --get the folderid
        --lc_vFolderID := get_FolderID(lc_vFolder);
        --get the loadid from the folderid
        --lc_nLoadID := get_LoadID(lc_vFolderid);

        
        --test to see if it is an xml file
        lc_vMimeType := get_MimeType(lc_nFileId);
        
        if lc_vMimeType = 'text/xml' then
            lc_nXML := convertBlobToXML(IN_nPost_Upload_Id);

            if lc_nXML = 1 then
                --import xml facilities here
                lc_nXMLFacilitiesImport := xml_tools.import_xml_facility_files(IN_nPost_Upload_Id, lc_nFileid);
            else
                --ERROR, something went wrong in the xml>parsing
                lc_jlMessages.append(json_list('[{error:''Error FU-0001 There has been an error processing the request''}]'));
            end if;
        else
            --ERROR NOT AN XML FILE
            lc_jlMessages.append(json_list('[{error:''Error FU-0002 There has been an error processing the request''}]'));
        end if;
    else
        --IN_VFILE_TYPE not 100085_WF
        lc_jlMessages.append(json_list('[{error:''Error FU-0003 There has been an error processing the request''}]'));
    end if;
    
    
    --update import_log
    if lc_nXMLFacilitiesImport = 1 then
        insert into spd_t_import_log (adoid, import_section, grouping,total_records,count_of_records) values (IN_nPost_Upload_Id,'XML FACILITIES IMPORTED','XML FACILITIES IMPORTED',1,1);
    else
        --an error occured and the xml facilities were not imported
        null;
    end if;

    exception
        when others then
          lc_jError := json();
          lc_jError.put('function','file_upload');
          lc_jError.put('module','xmlImportManager');
          lc_jError.put('target','');
          lc_jError.put('ref_id',IN_nPost_Upload_Id);
          lc_jError.put('input','');
          lc_jlMessages.append(json_list('[{error:''FU.000X ' || SQLERRM || '''}]'));
          lc_jError.put('messages',lc_jlMessages);
          BB_P_ERROR(lc_jError);
end;


end;