--------------------------------------------------------
--  DDL for Package Body DATARECEIVER_T
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE PACKAGE BODY "SPIDERS_DATA"."DATARECEIVER_T" AS



procedure add_drop_object 
(
	
  
  in_vDescription varchar2, 
	in_cX3D_Node clob, --varchar2, 
	in_vObjectLabel varchar2 default '', 
	in_nObjectTypeListID number default 10085, 
	in_vSessionKey varchar2,
	in_vLength varchar2 default '',
	in_vWidth varchar2 default '',
	in_vHeight varchar2 default '',
	in_vColor varchar2 default '',
	in_vOpacity varchar2 default '1',
	in_vTranslation varchar2,
	in_vRotation varchar2  default '1 0 0 1.57',
	in_vRadius varchar2 default '',
  in_vParent_Def varchar2 default '',
  in_vNode_Def varchar2,
  in_vNodeType varchar2,
  in_vPitch varchar2  default '0 0 1 0',
  in_vRoll varchar2  default '1 0 0 0'
  

)
as

lc_nObjectId number :=0;
lc_nSessionID number :=0;
lc_nCount number :=0;
lc_vActionType varchar2(255) :='';
lc_cX3D_Node clob;
lc_cSql clob;

begin

--htp.p('in_cX3D_node = '||in_cX3D_node);

insert into debug (functionname,input,message,username) values ('datareceiver_t.add_drop_object',in_cX3D_Node,'start',v('APP_USER'));
insert into debug (functionname,input,message,username) values ('datareceiver_t.add_drop_object initial node test',v('P202_X3D_NODE'),'start',v('APP_USER'));
lc_cX3D_Node := v('P202_X3D_NODE') || v('P202_X3D_NODE2') || v('P202_X3D_NODE3');
--lc_cX3D_Node := utl_url.unescape(lc_cX3D_Node); !!TRY THIS LATER MAYBE
--set the sessionid from the sessionkey
for rec in (select sessionid from spd_t_telestrator_sessions where sessionkey = in_vSessionKey) loop
  lc_nSessionID := rec.sessionid;
end loop;


--REMOVE OLD TELESTRATOR AND IT'S DRAW OBJECTS
if in_vNodeType='TELESTRATOR' then
  select count(logid) into lc_nCount from spd_t_telestrator_log where sessionid = lc_nSessionID and localid = in_vNode_Def;
  if lc_nCount > 0 then
    delete from spd_t_telestrator_log where sessionid = lc_nSessionID and localid = in_vNode_Def;
    delete from spd_t_telestrator_log where sessionid = lc_nSessionID and local_parent_id = replace(in_vNode_Def,'_TID','_DOTID');
    
    delete from spd_t_telestrator_drop_objects where sessionid = lc_nSessionID and local_id = in_vNode_Def;
    delete from spd_t_telestrator_drop_objects where sessionid = lc_nSessionID and local_parent_id = replace(in_vNode_Def,'_TID','_DOTID');
    
    commit;
  end if;
end if;

--REMOVE OLD JOIN ME's 
if in_vNodeType='JOINME' then
  select count(logid) into lc_nCount from spd_t_telestrator_log where sessionid = lc_nSessionID and node_type = in_vNodeType;
  if lc_nCount > 0 then
    delete from spd_t_telestrator_log where sessionid = lc_nSessionID and node_type = in_vNodeType;
    
    delete from spd_t_telestrator_drop_objects where sessionid = lc_nSessionID and node_type = in_vNodeType;
    
    commit;
  end if;
end if;

if in_vNodeType='DELETE_JOINME' then
  select count(logid) into lc_nCount from spd_t_telestrator_log where sessionid = lc_nSessionID and node_type = 'JOINME' and object_label = in_vObjectLabel;
  if lc_nCount > 0 then
    delete from spd_t_telestrator_log where sessionid = lc_nSessionID and node_type = 'JOINME' and object_label = in_vObjectLabel;
    
    delete from spd_t_telestrator_drop_objects where sessionid = lc_nSessionID and node_type = 'JOINME' and object_label = in_vObjectLabel;
    
    commit;
  end if;
  lc_vActionType:='DELETE JOINME';
  GOTO end_proc;
end if;

/*
Determine action to be performed:

ADD (to t_telestrator_drop_objects)
UPDATE (t_telestrator_drop_obejcts)

Add = in_vNode_Def does not exist in t_telestrator_drop_objects
Update = in_vNode_Def does exist in t_telestrator_drop_objects

*/

insert into debug (functionname,input,message,username) values ('datareceiver_t.add_drop_object','','determine add or update',v('APP_USER'));
for rec in (select count(objectid) the_count , objectid from spd_t_telestrator_drop_objects where local_id = in_vNode_Def and sessionid= lc_nSessionid group by objectid) loop
  lc_nCount := rec.the_count;
  lc_nObjectid := rec.objectid;
end loop;

if lc_nCount = 1 and in_vNodeType!='TELESTRATOR' and in_vNodeType!='JOINME' then
--htp.p('RUNNING UPDATE, lc_nCount='||lc_nCount);
--This node def exists, this is an update
lc_vActionType := 'UPDATE';

lc_cSql := 'update 
		SPD_T_TELESTRATOR_DROP_OBJECTS 
	set
		X3D_NODE = ' || lc_cX3D_Node || ' ,
		X = ' || in_vTranslation || ',
		ROTATION = ' || in_vRotation || ',
    PITCH = ' || in_vPitch || ',
    ROLL = ' || in_vRoll || '
  where
    objectid = ' || lc_nObjectid;
  insert into debug (functionname,misc) values ('datareceiver_t: insert into log',lc_cSql);
  
  
	update 
		SPD_T_TELESTRATOR_DROP_OBJECTS 
	set
		X3D_NODE = lc_cX3D_Node,
		X = in_vTranslation,
		ROTATION = in_vRotation,
    PITCH = in_vPitch,
    ROLL = in_vRoll
  where
    objectid = lc_nObjectid;
  commit;
  
else
--htp.p('RUNNING INSERT');
--this node def does not exist, this is an add
lc_vActionType := 'ADD';

--get the next objectid
select spd_s_telestrator_drop_objects.nextval into lc_nObjectId from dual;

/*  --TESTING - t did this
	insert into 
		SPD_T_TELESTRATOR_DROP_OBJECTS 
	columns
	(
    OBJECTID,
		DESCRIPTION,
		--X3D_NODE,
		PARENT_OBJECTID
	)
	values
	(
    lc_nObjectId,
		in_vDescription, 
		--in_cX3D_Node, 
    lc_nObjectid  --for now there is no parent, its self referencing, when we get to telestrators we will change this i think.

	);
*/
  
  
  insert into debug (functionname,input,message,username) values ('datareceiver_t.add_drop_object','','insert into drop objects',v('APP_USER'));
	insert into 
		SPD_T_TELESTRATOR_DROP_OBJECTS 
	columns
	(
    OBJECTID,
		DESCRIPTION,
		X3D_NODE,
		PARENT_OBJECTID,
    OBJECT_LABEL,
		CREATED_BY,
		OBJECT_TYPE_LISTID,
		SESSIONID,
		OBJECT_LENGTH,
		OBJECT_WIDTH,
		OBJECT_HEIGHT,
		OBJECT_COLOR,
		OBJECT_OPACITY,
		X,
		ROTATION,
    OBJECT_RADIUS,
    LOCAL_ID,
    NODE_TYPE,
    LOCAL_PARENT_ID,
    PITCH,
    ROLL
	)
	values
	(
    lc_nObjectId,
		in_vDescription, 
		lc_cX3D_Node, --DECODE(in_cX3D_Node,null,' ',in_cX3D_Node), 
    lc_nObjectid , --for now there is no parent, its self referencing, when we get to telestrators we will change this i think.
		in_vObjectLabel,
    APEX_UTIL.GET_SESSION_STATE('APP_USER'), 
		in_nObjectTypeListID , 
		lc_nSessionID ,
		in_vLength ,
		in_vWidth ,
		in_vHeight ,
		in_vColor ,
		in_vOpacity ,
		in_vTranslation ,
		in_vRotation,
    in_vRadius,
    in_vNode_Def,
    in_vNodeType,
    in_vParent_Def,
    in_vPitch,
    in_vRoll
	);


end if;
  
/*
Add a record to the log table
*/

  insert into debug (functionname,input,message,username) values ('datareceiver_t.add_drop_object','','insert into telestrator log',v('APP_USER'));
  lc_cSql := '  insert into 
		SPD_T_TELESTRATOR_LOG 
	columns
	(
    LOCALID,
    LOCAL_PARENT_ID,
    ACTIONTYPE,
    COLOR,
    OPACITY,
    TRANSLATION,
    ROTATION,
    X3D_NODE,
    CREATED_BY,
    SESSIONID,
    NODE_TYPE,
    OBJECT_LABEL,
    PITCH,
    ROLL
  )
  values
  (
    ' || in_vNode_Def || ',
    ' || in_vParent_Def || ',
    ' || lc_vActionType || ',
    ' || in_vColor || ',
    ' || in_vOpacity || ',
    ' || in_vTranslation || ',
    ' || in_vRotation || ',
    ' || lc_cX3D_Node || ', 
    ' || APEX_UTIL.GET_SESSION_STATE('APP_USER') || ',
    ' || lc_nSessionID || ',
    ' || in_vNodeType || ',
    ' || in_vObjectLabel || ',
    ' || in_vPitch || ',
    ' || in_vRoll || '
  )';
  
  insert into debug (functionname,misc,username) values ('datareceiver_t: testing x3d node',lc_cX3D_Node,v('APP_USER'));
  insert into debug (functionname,misc,username) values ('datareceiver_t: insert into log',lc_cSql,v('APP_USER'));
  
  insert into 
		SPD_T_TELESTRATOR_LOG 
	columns
	(
    LOCALID,
    LOCAL_PARENT_ID,
    ACTIONTYPE,
    COLOR,
    OPACITY,
    TRANSLATION,
    ROTATION,
    X3D_NODE,
    CREATED_BY,
    SESSIONID,
    NODE_TYPE,
    OBJECT_LABEL,
    PITCH,
    ROLL
  )
  values
  (
    in_vNode_Def,
    in_vParent_Def,
    lc_vActionType,
    in_vColor,
    in_vOpacity,
    in_vTranslation,
    in_vRotation,
    lc_cX3D_Node, --DECODE(in_cX3D_Node,null,' ',in_cX3D_Node),
    APEX_UTIL.GET_SESSION_STATE('APP_USER'),
    lc_nSessionID,
    in_vNodeType,
    in_vObjectLabel,
    in_vPitch,
    in_vRoll
  );



commit;

<<end_proc>>
insert into debug (functionname,input,message,username) values ('datareceiver_t.add_drop_object','','end',v('APP_USER'));
tobrowser.message('success',lc_vActionType || ' : ' || to_char(lc_nObjectID));
    

exception
        when others then
        execute immediate 'insert into debug (functionname, message,username) values (''datareceiver_t.add_drop_object'',''' || sqlerrm || ''',''' || v('APP_USER') || ''')'; 
        /*tobrowser.message('error', lc_vActionType || ': object in_nObjectid:' || to_char(in_vNode_Def) || ',IN_vDescription:' || IN_vDescription|| ',in_vObjectLabel:' || in_vObjectLabel|| ',in_nSession:' || to_char(lc_nSessionID) || ':' ||
        SQLERRM
        );*/
      
end;



procedure update_drop_object 
(
  in_nObjectid number,
	IN_vDescription varchar2, 
	in_cX3D_Node clob, 
	in_vLength varchar2,
	in_vWidth varchar2,
	in_vHeight varchar2,
	in_vColor varchar2,
	in_vOpacity varchar2,
	in_vX varchar2,
	in_vY varchar2,
	in_vZ varchar2,
	in_vRotation varchar2,
	in_vRadius varchar2,
  in_vPitch varchar2,
  in_vRoll varchar2,
  in_vYaw varchar2,
  in_vLocal_Id varchar2
)
as


	lc_vLength varchar2(20) := in_vLength;
	lc_vWidth varchar2(20) := in_vWidth;
	lc_vHeight varchar2(20) := in_vHeight;
  
begin

	if lc_vLength ='null' then 
    lc_vLength := '';
  end if;
  
	if lc_vWidth ='null' then 
    lc_vWidth := '';
  end if;
  
  if lc_vHeight ='null' then 
    lc_vHeight := '';
  end if;

	update 
		SPD_T_TELESTRATOR_DROP_OBJECTS 
	set
		DESCRIPTION = IN_vDescription,
		X3D_NODE = in_cX3D_Node,
		OBJECT_LENGTH = lc_vLength,
		OBJECT_WIDTH = lc_vWidth,
		OBJECT_HEIGHT = lc_vHeight,
		OBJECT_COLOR = in_vColor,
		OBJECT_OPACITY =  in_vOpacity,
		X = in_vX, 
		Y = in_vY,
		Z = in_vZ,
		ROTATION = in_vRotation,
    OBJECT_RADIUS = in_vRadius,
    PITCH = in_vPitch,
    ROLL = in_vRoll,
    YAW = in_vYaw
  where
    objectid = in_nObjectid;
    
    
/*
Add a record to the log table
*/
  insert into 
		SPD_T_TELESTRATOR_LOG 
	columns
	(
    LOCALID,
    ACTIONTYPE,
    COLOR,
    OPACITY,
    TRANSLATION,
    ROTATION,
    X3D_NODE,
    CREATED_BY,
    SESSIONID,
    PITCH,
    ROLL
  )
  values
  (
    in_vLocal_Id,
    'UPDATE',
    in_vColor,
    in_vOpacity,
    in_vX || ' ' || in_vY || ' ' || in_vZ,
    in_vRotation,
    in_cX3D_Node,
    APEX_UTIL.GET_SESSION_STATE('APP_USER'),
    1, -- WILL HAVE TO GET THE SESSION ID SOMEHOW in_nSession
    in_vPITCH,
    in_vROLL
  );


commit;

tobrowser.message('success','Updated object');
    

exception
        when others then
        tobrowser.message('error', 'updating drop object in_nObjectid:' || to_char(in_nObjectid) || ':' ||
        SQLERRM
        );
        
end;


procedure add_session
(
  in_nSceneId number,
  in_vDescription varchar2,
  in_vEncrypted varchar2,
  in_vExpirationDate varchar2
)

is 

lc_vSessionkey varchar2(11);
lc_vSessionID number;
lc_logid number;
lc_localid varchar2(256);

begin
	select dbms_random.string('X', 3) || '-' || dbms_random.string('X', 3) || '-' || dbms_random.string('X', 3) into lc_vSessionkey from dual;
	SELECT spd_s_telestrator_sessions.nextval INTO lc_vSessionID FROM dual;
  
  insert into 
		SPD_T_TELESTRATOR_SESSIONS 
	columns
	(
    SESSIONID,
    SCENEMODELID, 
    SCENEID, --MR EDIT, SCENEID CANT BE NULL
		DESCRIPTION,
		ENCRYPTED,
		EXPIRATION_DATE,
    sessionkey
	)
	values
	(
    lc_vSessionID,
    in_nSceneId,
    in_nSceneId, --MR EDIT, SCENEID CANT BE NULL
		IN_vDescription, 
		in_vEncrypted , 
    in_vExpirationDate,
    lc_vSessionkey
	);

  commit;

  for rec in (SELECT a.XYZ, a.ROTATION, b.FILEPATH, b.DESCRIPTION
              FROM SPD_T_TELESTRATOR_SCENE_CONFIG a, SPD_T_TELESTRATOR_MDL_VERSIONS b,SPD_T_TELESTRATOR_SRC_MODELS c
              WHERE a.MODELVERSIONID = b.MODELVERSIONID AND b.SOURCEMODELID = c.SOURCEMODELID
                AND a.SCENEMODELID = in_nSceneId) loop
    
    SELECT SPD_S_TELESTRATOR_LOG.nextval INTO lc_logid FROM dual;
    lc_localid:='INLINE_TEST_'||lc_logid||'_OBJECT';
    
    insert into SPD_T_TELESTRATOR_LOG
      columns (LOGID,LOCALID,OPACITY,ACTIONTYPE,TRANSLATION,ROTATION,CREATED_BY,SESSIONID,NODE_TYPE,X3D_NODE)
      values  (lc_logid,lc_localid,1,'ADD',rec.XYZ,rec.ROTATION,'scene_config',lc_vSessionID,rec.DESCRIPTION,rec.FILEPATH);
      
    insert into SPD_T_TELESTRATOR_DROP_OBJECTS
      columns (local_id,OBJECT_OPACITY,X,ROTATION,CREATED_BY,SESSIONID,NODE_TYPE,X3D_NODE)
      values  (lc_localid,1,rec.XYZ,rec.ROTATION,'scene_config',lc_vSessionID,rec.DESCRIPTION,rec.FILEPATH);
    
  end loop;

  insert into spd_t_telestrator_drnk_history
    (USERNAME,SESSIONID)
  values
    (APEX_UTIL.GET_SESSION_STATE('APP_USER'),lc_vSessionID);
  
tobrowser.message('success',lc_vSessionkey);
    

exception
        when others then
        tobrowser.message('error', 'adding scene in_nSceneId:' || to_char(in_nSceneId) || ',IN_vDescription:' || IN_vDescription|| ',in_vEncrypted:' || in_vEncrypted|| ',in_vExpirationDate' || in_vExpirationDate || ':' ||
        SQLERRM
        );
end;


procedure del_drop_object 
(
  in_vNode_Def varchar2,
  in_vSessionkey varchar2
)

is 

lc_nObjectid number :=0;
lc_nSessionID number :=0;
lc_nCount number :=0;

begin

for rec in (select sessionid from spd_t_telestrator_sessions where sessionkey = in_vSessionKey) loop
  lc_nSessionID := rec.sessionid;
end loop;

for rec in (select count(objectid) thecount, objectid from spd_t_telestrator_drop_objects where local_id = in_vNode_Def and sessionid= lc_nSessionid group by objectid) loop
  lc_nObjectid := rec.objectid;
  lc_nCount := rec.thecount;
end loop;

htp.p(in_vNode_Def || ':' || lc_nCount || ':' || lc_nsessionid);

if lc_nCount > 0 then
  delete from spd_t_telestrator_drop_objects where objectid = lc_nObjectid and sessionid = lc_nSessionid;
  delete from spd_t_telestrator_log where localid = in_vNode_Def and sessionid = lc_nSessionid;
  commit;
  
  /*
  Add a record to the log table
  */
    insert into 
      SPD_T_TELESTRATOR_LOG 
    columns
    (
      LOCALID,
      ACTIONTYPE,
      CREATED_BY,
      SESSIONID
    )
    values
    (
      in_vNode_Def,
      'DELETE',
      APEX_UTIL.GET_SESSION_STATE('APP_USER'),
      lc_nSessionID
    ); 
  
  tobrowser.message('success','deleting object');
else
  tobrowser.message('success','no action');
end if;

exception
        when others then
        tobrowser.message('error', 'deleting object from scene in_nObjectId:' || to_char(lc_nObjectid) || ':' ||
        SQLERRM
        );
end;


--in_Action=>'Add',in_SCENEMODELID=>680000032,in_MODELVERSIONID=>'580001080',in_XYZ=>'zzz',in_ROTATION=>'xxx'
--in_Action=>'Add',in_SCENEMODELID=>680000020,in_MODELVERSIONID=>580001080,in_XYZ=>'1 1 1',in_ROTATION=>'2 2 2 2'
--in_Action=>'Add',in_SCENEMODELID=>680000020,in_MODELVERSIONID=>580001080,in_XYZ=>'3 3 3',in_ROTATION=>'4 4 4 4'
procedure update_scene_config(
  in_Action varchar2,
  in_XYZ varchar2,
  in_ROTATION varchar2,
  in_FILEPATH varchar2 default null,
  in_DESCRIPTION varchar2 default null,
  in_SCENEMODELID number default null,
  in_MODELVERSIONID number default null,
  in_SOURCEMODELID number default null,
  in_MODEL_NAME varchar2 default null,
  in_SCENECONFIGID varchar2 default null
)
is

begin
  if in_Action='Add' then
    INSERT INTO "SPIDERS_DATA"."SPD_V_SCENE_CONFIG" 
    COLUMNS (XYZ,ROTATION,FILEPATH,DESCRIPTION,MODEL_NAME,SCENEMODELID)
    VALUES (in_XYZ,in_ROTATION,in_FILEPATH,in_DESCRIPTION,in_MODEL_NAME,in_SCENEMODELID);
  elsif in_Action='Edit' then
    if in_SCENECONFIGID is not null then
      update SPD_V_SCENE_CONFIG 
      set  XYZ = in_XYZ
          ,ROTATION = in_ROTATION
          ,FILEPATH = in_FILEPATH
          ,DESCRIPTION = in_DESCRIPTION
          ,SCENEMODELID = in_SCENEMODELID
          ,SOURCEMODELID = in_SOURCEMODELID
          ,MODEL_NAME = in_MODEL_NAME
      where sceneconfigid = in_SCENECONFIGID;
    else
      update SPD_V_SCENE_CONFIG 
      set  XYZ = in_XYZ
          ,ROTATION = in_ROTATION
          ,FILEPATH = in_FILEPATH
          ,DESCRIPTION = in_DESCRIPTION
          ,SCENEMODELID = in_SCENEMODELID
          ,SOURCEMODELID = in_SOURCEMODELID
          ,MODEL_NAME = in_MODEL_NAME
      where SCENEMODELID = in_SCENEMODELID and modelversionid = in_MODELVERSIONID;
    end if;
  elsif in_Action='Delete' then
    if in_SCENECONFIGID is not null then
      delete from SPD_V_SCENE_CONFIG where sceneconfigid = in_SCENECONFIGID;
    else
      delete from SPD_V_SCENE_CONFIG where scenemodelid = in_SCENEMODELID and modelversionid = in_MODELVERSIONID;
    end if;
  end if;
  htp.p('success');
end;

procedure update_source_model(
  in_Action varchar2,
  in_SOURCEMODELID number default null,
  in_MODEL_NAME varchar2 default null,
  in_MODEL_NUMBER varchar2 default null,
  in_HOMEPORT varchar2 default null  
)
is
  lc_Count number := 1;
begin
  if in_Action='Add' then
    INSERT INTO "SPIDERS_DATA"."SPD_T_TELESTRATOR_SRC_MODELS" 
    COLUMNS (MODEL_NAME,MODEL_NUMBER,HOMEPORT)
    VALUES (in_MODEL_NAME,in_MODEL_NUMBER,in_HOMEPORT);
    htp.p('success');
  elsif in_Action='Edit' then
    update SPD_T_TELESTRATOR_SRC_MODELS 
    set  MODEL_NAME = in_MODEL_NAME
        ,MODEL_NUMBER = in_MODEL_NUMBER
        ,HOMEPORT = in_HOMEPORT
    where SOURCEMODELID = in_SOURCEMODELID;
    htp.p('success');
  elsif in_Action='Delete' then
    select count(modelversionid) into lc_Count from spd_t_telestrator_mdl_versions where sourcemodelid = in_SOURCEMODELID;
    if lc_Count = 0 then
      delete from SPD_T_TELESTRATOR_SRC_MODELS where SOURCEMODELID = in_SOURCEMODELID;
      htp.p('success');
    else
      htp.p('You must remove all model versions before the model can be deleted.');
    end if;
  end if;
end;

procedure update_mdl_version(
  in_Action varchar2,
  in_MODELVERSIONID number default null,
  in_SOURCEMODELID number default null,
  in_DESCRIPTION varchar2 default null,
  in_FILEPATH varchar2 default null 
)
is

begin
  if in_Action='Add' then
    INSERT INTO SPD_T_TELESTRATOR_MDL_VERSIONS
    COLUMNS (SOURCEMODELID,DESCRIPTION,FILEPATH)
    VALUES (in_SOURCEMODELID,in_DESCRIPTION,in_FILEPATH);
  elsif in_Action='Edit' then
    update SPD_T_TELESTRATOR_MDL_VERSIONS 
    set  SOURCEMODELID = in_SOURCEMODELID
        ,DESCRIPTION = in_DESCRIPTION
        ,FILEPATH = in_FILEPATH
    where MODELVERSIONID = in_MODELVERSIONID;
  elsif in_Action='Delete' then
    delete from SPD_T_TELESTRATOR_MDL_VERSIONS where MODELVERSIONID = in_MODELVERSIONID;
  end if;
  htp.p('success');
end;

procedure update_scene(
  in_Action varchar2,
  in_SCENEID number default null,
  in_DESCRIPTION varchar2 default null,
  in_LOCATION varchar2 default null,
  in_FILEPATH varchar2 default null  
)
is
  lc_Count number;
begin
  if in_Action='Add' then
    INSERT INTO SPD_T_TELESTRATOR_SCENE_MODEL
    COLUMNS (DESCRIPTION,LOCATION,FILEPATH)
    VALUES (in_DESCRIPTION,in_LOCATION,in_FILEPATH);
    htp.p('success');
  elsif in_Action='Edit' then
    update SPD_T_TELESTRATOR_SCENE_MODEL 
    set  DESCRIPTION = in_DESCRIPTION
        ,LOCATION = in_LOCATION
        ,FILEPATH = in_FILEPATH
    where SCENEMODELID = in_SCENEID;
    htp.p('success');
  elsif in_Action='Delete' then
    select count(sceneconfigid) into lc_Count from spd_t_telestrator_scene_config where scenemodelid = in_SCENEID;
    if lc_Count = 0 then
      delete from SPD_T_TELESTRATOR_SCENE_MODEL where SCENEMODELID = in_SCENEID;
      htp.p('success');
    else
      htp.p('You must remove all scene configurations before the scene can be deleted.');
    end if;
  end if;
end;

procedure update_t_scene_config(
  in_Action varchar2,
  in_SCENECONFIGID number default null,
  in_MODELVERSIONID number default null,
  in_SCENEMODELID number default null,
  in_XYZ varchar2 default null,
  in_ROTATION varchar2 default null 
)
is

begin
  if in_Action='Add' then
    INSERT INTO SPD_T_TELESTRATOR_SCENE_CONFIG
    COLUMNS (MODELVERSIONID,SCENEMODELID,XYZ,ROTATION)
    VALUES (in_MODELVERSIONID,in_SCENEMODELID,in_XYZ,in_ROTATION);
  elsif in_Action='Edit' then
    update SPD_T_TELESTRATOR_SCENE_CONFIG 
    set  SCENEMODELID = DECODE(in_SCENEMODELID,null,SCENEMODELID,in_SCENEMODELID)
        ,MODELVERSIONID = DECODE(in_MODELVERSIONID,null,MODELVERSIONID,in_MODELVERSIONID)
        ,XYZ = DECODE(in_XYZ,null,XYZ,in_XYZ)
        ,ROTATION = DECODE(in_ROTATION,null,ROTATION,in_ROTATION)
    where SCENECONFIGID = in_SCENECONFIGID;
  elsif in_Action='Delete' then
    delete from SPD_T_TELESTRATOR_SCENE_CONFIG where SCENECONFIGID = in_SCENECONFIGID;
  end if;
  htp.p('success');
end;

procedure update_lead(
  in_Action varchar2,
  in_LEADID number default null,
  in_MODEL_TYPE varchar2 default null,
  in_LOCATION varchar2 default null,
  in_NFA varchar2 default null,
  in_DESCRIPTION varchar2 default null,
  in_REQUESTING_USER varchar2 default null
)
is
  lc_Count number;
begin
  if in_Action='Add' then
    INSERT INTO SPD_T_TELESTRATOR_LEADS
    COLUMNS (MODEL_TYPE,LOCATION,NFA,DESCRIPTION,REQUESTING_USER)
    VALUES (in_MODEL_TYPE,in_LOCATION,in_NFA,in_DESCRIPTION,in_REQUESTING_USER);
    htp.p('success');
  elsif in_Action='Edit' then
    update SPD_T_TELESTRATOR_LEADS 
    set  MODEL_TYPE = in_MODEL_TYPE
        ,LOCATION = in_LOCATION
        ,NFA = in_NFA
        ,DESCRIPTION = in_DESCRIPTION
    where LEADID = in_LEADID;
    htp.p('success');
  elsif in_Action='Delete' then
    null;
    /*
    select count(sceneconfigid) into lc_Count from spd_t_telestrator_scene_config where scenemodelid = in_SCENEID;
    if lc_Count = 0 then
      delete from SPD_T_TELESTRATOR_SCENE_MODEL where SCENEMODELID = in_SCENEID;
      htp.p('success');
    else
      htp.p('You must remove all scene configurations before the scene can be deleted.');
    end if;
    */
  end if;
end;

END datareceiver_t;

/
