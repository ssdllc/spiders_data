--------------------------------------------------------
--  DDL for Package Body DATAPROVIDER_T
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE PACKAGE BODY "SPIDERS_DATA"."DATAPROVIDER_T" AS

/*
This software has been released under the MIT license:

  Copyright (c) 2012 Synergy Software Design, LLC

  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.
*/


procedure get_geometry_types

is 
	lc_vSql varchar2(2000);
begin
  lc_vSql := 
  'select
  *
  from 
    spd_t_lists
  where
    listname = ''list_geom_types''
  order by
    visualorder';
  
  
	tobrowser.data(lc_vSql);
	
	exception
        when others then
        tobrowser.message('error', 'error retrieving geometry types: ' || SQLERRM
        );
end;


procedure get_colors

is 
	lc_vSql varchar2(2000);
begin
  lc_vSql := 
  'select
  *
  from 
    spd_t_lists
  where
    listname = ''list_colors''
  order by
    visualorder';
  
  
	tobrowser.data(lc_vSql);
	
	exception
        when others then
        tobrowser.message('error', 'error retrieving list of colors: ' || SQLERRM
        );
end;

procedure get_object_id

is 
	lc_vSql varchar2(2000);
begin
  lc_vSql := 
  'select
  spd_s_telestrator_drop_objects.nextval
  from 
    dual';
  
  
	tobrowser.data(lc_vSql);
	
	exception
        when others then
        tobrowser.message('error', 'error retrieving list of colors: ' || SQLERRM
        );
end;




procedure get_model_types


is 
	lc_vSql varchar2(2000);
begin
  lc_vSql := 
  'select
  *
  from 
    spd_t_lists
  where
    listname = ''list_model_types''
  order by
    visualorder';
  
  
	tobrowser.data(lc_vSql);
	
	exception
        when others then
        tobrowser.message('error', 'error retrieving list of model types: ' || SQLERRM
        );
end;


procedure get_model_group_types(IN_nOBJECT_TYPE_LISTID number)


is 
	lc_vSql varchar2(2000);
begin
  lc_vSql := 
  'select 
    ship_type 
  from 
    spd_t_telestrator_src_models 
  where 
    object_type_listid = ' || IN_nOBJECT_TYPE_LISTID || ' 
  group by 
    ship_type
  order by
    ship_type';
  
  
	tobrowser.data(lc_vSql);
	
	exception
        when others then
        tobrowser.message('error', 'error retrieving list of model group types: ' || SQLERRM
        );
end;

procedure get_models(IN_nOBJECT_TYPE_LISTID number, IN_vGroupValue varchar2)


is 
	lc_vSql varchar2(2000);
begin
  lc_vSql := 
  'select 
    *
  from 
    spd_t_telestrator_src_models 
  where 
    object_type_listid = ' || IN_nOBJECT_TYPE_LISTID || ' 
    and ship_type = ''' || IN_vGroupValue || '''';
  
  
	tobrowser.data(lc_vSql);
	
	exception
        when others then
        tobrowser.message('error', 'error retrieving list of models: ' || SQLERRM
        );
end;

procedure get_model_versions(IN_nSourceModelid number)

is 
	lc_vSql varchar2(2000);
begin
  lc_vSql := 
  'select 
    modelversionid,sourcemodelid,description,filepath
  from 
    spd_t_telestrator_mdl_versions 
  where 
    sourcemodelid = ' || IN_nSourceModelid;
  
  
	tobrowser.data(lc_vSql);
	
	exception
        when others then
        tobrowser.message('error', 'error retrieving list of model versions: ' || SQLERRM
        );
  
end;





procedure get_scenes


is 
	lc_vSql varchar2(2000);
begin
  lc_vSql := 
  'select 
    scenemodelid,
    upper(state || '' '' || replace(location,''_'','' '') ||  ''   ('' || uic || '')'') description,
    NORTHING,
    EASTING,
    UTM_ZONE,
    STATE,
    LOCATION,
    SPECIAL_AREA_CODE,
    UIC,
    FILEPATH,
    VERSION,
    CREATED_DATE,
    SCENEID,
    SCALE,
    WIDTH,
    HEIGHT
  from 
    spd_t_telestrator_scene_model
  order by
    STATE, location';
  
  
	tobrowser.data(lc_vSql);
	
	exception
        when others then
        tobrowser.message('error', 'error retrieving list of scenes: ' || SQLERRM
        );
end;

procedure get_session_scene_url(in_vSessionkey varchar2)

is 
	lc_vSql varchar2(2000);
begin
  lc_vSql := 
  'select 
    * 
  from 
    spd_t_telestrator_scenes
  where
    sceneid in (select sceneid from spd_t_telestrator_sessions where sessionkey = ''' || in_vSessionkey || ''')';
  
  
	tobrowser.data(lc_vSql);
	
	exception
        when others then
        tobrowser.message('error', 'error retrieving scene: ' || SQLERRM
        );
end;



procedure get_session_id(in_vSessionKey varchar2)

is 
	lc_vSql varchar2(2000);
begin
  lc_vSql := 
  'select 
    * 
  from 
    spd_t_telestrator_sessions
  where
    sessionkey = ''' || in_vSessionkey || '''';
  
  
	tobrowser.data(lc_vSql);
	
	exception
        when others then
        tobrowser.message('error', 'error retrieving list of scenes: ' || SQLERRM
        );
end;


procedure get_session_object_ids(in_nsessionid number)

is 
	lc_vSql varchar2(4000) := '';
begin

  lc_vSql := '
  Select
    * 
  from 
    spd_t_telestrator_drop_objects
  where
    sessionid = ' || in_nsessionid;
  
  
	tobrowser.data(lc_vSql);
	
	exception
        when others then
        tobrowser.message('error', 'error retrieving list of scenes objects: ' || SQLERRM
        );
end;



procedure get_session_object(in_nObjectId number)

is 
	lc_vSql varchar2(32000) := '';
begin

for rec in (
  Select
    * 
  from 
    spd_t_telestrator_drop_objects
  where
    objectid = in_nObjectId)
loop
  lc_vSql := rec.x3d_node;

end loop;
    
    
	tobrowser.message('success', lc_vSql);
	
	exception
        when others then
        tobrowser.message('error', 'error retrieving scene object: ' || SQLERRM
        );
end;


procedure get_session_info(in_vSessionKey varchar2)

is 
	lc_vSql varchar2(32000) := '';
begin

lc_vSql := '
select
   ses.sessionkey
  ,sm.filepath
  ,sm.scale
  ,sm.width
  ,sm.height
  ,ses.description session_description

  ,ses.created_by
  ,ses.created_date
  ,sm.northing
  ,sm.easting
  ,sm.utm_zone
  ,sm.state
  ,sm.location
  ,sm.special_area_code
  ,sm.uic
  ,sm.version
  ,sm.created_date scene_model_created_date
  ,DECODE(APEX_UTIL.GET_SESSION_STATE(''APP_USER''),null,''hey'',APEX_UTIL.GET_SESSION_STATE(''APP_USER'')) current_user
from 
  spd_t_telestrator_sessions ses,
  spd_t_telestrator_scene_model sm
where
  ses.scenemodelid = sm.scenemodelid and
  upper(ses.sessionkey) = ''' || upper(in_vSessionKey) || '''';

	tobrowser.data(lc_vSql);
	
	exception
        when others then
        tobrowser.message('error', 'error retrieving session info: ' || SQLERRM
        );
end;


procedure get_session_updates(in_vPreviousTimestamp varchar2, in_vCurrentTimestamp varchar2, in_vSessionkey varchar2)

is 
	lc_vSql varchar2(4000) := '';
  lc_Count number;
  lc_sessionid number;
begin


  lc_vSql := '
  select
    logid,
    localid,
    timestamp,
    actiontype,
    color,
    opacity,
    translation,
    rotation,
    x3d_node,
    created_by,
    sessionid,
    node_type,
    object_label,
    local_parent_id,
    DECODE(node_type,''JOINME'',2,1) sortVal_1,
    DECODE(local_parent_id,''0'',replace(localid,''_T'',''_DOT''),local_parent_id) sortVal_2,
    PITCH,
    ROLL
  from
    spd_t_telestrator_log
  where
    sessionid = (select sessionid from spd_t_telestrator_sessions where sessionkey = ''' || in_vSessionkey || ''')
    and timestamp between to_date(''' || in_vPreviousTimestamp || ''',''YYYYMMDDHH24MISS'') and to_date(''' || in_vCurrentTimestamp || ''',''YYYYMMDDHH24MISS'')
    and created_by <> ''' || APEX_UTIL.GET_SESSION_STATE('APP_USER') || ''' order by sortVal_1,sortVal_2, logid asc'; --  order by localid asc'; --logid
    
if in_vPreviousTimestamp = '20120101000000' then
--if this is not the initial call, add the filter by created_by

  lc_vSql := '
  select
    logid,
    localid,
    to_char(timestamp) timestamp,
    to_char(actiontype) actiontype,
    to_char(color) color,
    to_char(opacity) opacity,
    translation,
    rotation,
    x3d_node,
    created_by,
    sessionid,
    node_type,
    object_label,
    local_parent_id,
    DECODE(node_type,''JOINME'',2,1) sortVal_1,
    DECODE(local_parent_id,''0'',replace(localid,''_T'',''_DOT''),local_parent_id) sortVal_2,
    PITCH,
    ROLL
  from
    spd_t_telestrator_log
  where
    sessionid = (select sessionid from spd_t_telestrator_sessions where sessionkey = ''' || in_vSessionkey || ''')
    and timestamp between to_date(''' || in_vPreviousTimestamp || ''',''YYYYMMDDHH24MISS'') and to_date(''' || in_vCurrentTimestamp || ''',''YYYYMMDDHH24MISS'')
    and created_by <> ''' || APEX_UTIL.GET_SESSION_STATE('APP_USER') || '''
  
union all

    select
    objectid logid,
    local_id localid,
    to_char(objectid) timestamp,
    ''ADD'' actiontype,
    to_char(objectid) color,
    to_char(objectid) opacity,
    X,
    rotation,
    x3d_node,
    created_by,
    sessionid,
    node_type,
    object_label,
    local_parent_id,
    DECODE(node_type,''JOINME'',2,1) sortVal_1,
    DECODE(local_parent_id,''0'',replace(local_id,''_T'',''_DOT''),local_parent_id) sortVal_2,
    PITCH,
    ROLL
  from
    spd_t_telestrator_drop_objects
  where
    sessionid = (select sessionid from spd_t_telestrator_sessions where sessionkey = ''' || in_vSessionkey || ''')
    and created_by = ''' || APEX_UTIL.GET_SESSION_STATE('APP_USER') || '''
    order by  logid asc'; -- order by localid asc --logid sortVal_1,sortVal_2,
    
  --UPDATE SESSION HISTORY BELOW----------------------  
  --GET SESSIONID
  select a.sessionid into lc_sessionid
    from spd_t_telestrator_sessions a
    where a.sessionkey = in_vSessionkey;
  --CHECK IF SESSION IS ALREADY IN THE USERS HISTORY
  select count(a.drunkhistoryid) into lc_Count
  from spd_t_telestrator_drnk_history a
  where a.sessionid = lc_sessionid and a.username = APEX_UTIL.GET_SESSION_STATE('APP_USER');
  if lc_Count > 0 then
    --UPDATE TIMESTAMP FOR THIS SESSION IN THIS USERS SESSION HISTORY
    update spd_t_telestrator_drnk_history set join_timestamp = sysdate where sessionid = lc_sessionid and username = APEX_UTIL.GET_SESSION_STATE('APP_USER');
  else
    --FIRST TIME LOGIN FOR THIS USER - ADD IT TO THEIR SESSION HISTORY
    insert into spd_t_telestrator_drnk_history
      (USERNAME,SESSIONID)
    values
      (APEX_UTIL.GET_SESSION_STATE('APP_USER'),lc_sessionid);
  end if;
  --UPDATE SESSION HISTORY ABOVE----------------------
end if;


/*

DECODE(local_parent_id,''0'',replace(localid,''_T'',''_DOT''),local_parent_id) sortVal_2
DECODE(local_parent_id,''0'',replace(local_id,''_T'',''_DOT''),local_parent_id) sortVal_2

and created_by <> ''' || APEX_UTIL.GET_SESSION_STATE('APP_USER') || ''' 

and created_by = ''' || APEX_UTIL.GET_SESSION_STATE('APP_USER') || '''



,
    DECODE(local_parent_id,''0'',replace(localid,''Q_T'',''Q_DOT''),local_parent_id) sortVal
    
--tc HAD TO CHANGE node_type to nodetype 1/22
  lc_vSql := '
  select
    logid,
    localid,
    timestamp,
    actiontype,
    color,
    opacity,
    translation,
    rotation,
    x3d_node,
    created_by,
    sessionid,
    node_type,
    object_label,
    local_parent_id
  from
    spd_t_telestrator_log
  where
    sessionid = (select sessionid from spd_t_telestrator_sessions where sessionkey = ''' || in_vSessionkey || ''') ';
  
  if in_vPreviousTimestamp = '20120101000000' then
    lc_vSql := lc_vSql || ' 
    and created_by = ''' || APEX_UTIL.GET_SESSION_STATE('APP_USER') || '''';
  else
    lc_vSql := lc_vSql || ' 
    and timestamp between to_date(''' || in_vPreviousTimestamp || ''',''YYYYMMDDHH24MISS'') and to_date(''' || in_vCurrentTimestamp || ''',''YYYYMMDDHH24MISS'')
    and created_by <> ''' || APEX_UTIL.GET_SESSION_STATE('APP_USER') || '''';
  end if;
*/  

  --htp.p(lc_vSql);
	
  tobrowser.data(lc_vSql);
	
	exception
        when others then
        tobrowser.message('error', 'error retrieving list of spiders3d log: ' || SQLERRM
        );
  
end;


procedure get_server_time

is 
	lc_vSql varchar2(4000) := '';
begin

  lc_vSql := '
  Select
    to_char(sysdate,''YYYYMMDDHH24MISS'') TIME
  from 
    dual';
  
	tobrowser.data(lc_vSql);
	
	exception
        when others then
        tobrowser.message('error', 'error retrieving time: ' || SQLERRM
        );
end;

--T BELOW----

procedure get_scene_configs(in_SCENEMODELID number default null)
is
  lc_vSql varchar2(4000) := '';
begin
  if in_SCENEMODELID is null then
    lc_vSql := 'Select * from SPD_V_SCENE_CONFIG';
  else
    lc_vSql := 'Select * from SPD_V_SCENE_CONFIG where scenemodelid = '||in_SCENEMODELID;
  end if;
  tobrowser.data(lc_vSql);
	
	exception
        when others then
        tobrowser.message('error', 'error retrieving time: ' || SQLERRM
        );

end;

procedure get_source_models
is
  lc_vSql varchar2(4000) := '';
begin
  lc_vSql := 'Select sourcemodelid,model_name,model_number,homeport from spd_t_telestrator_src_models order by sourcemodelid desc';
  
  tobrowser.data(lc_vSql);
	
	exception
        when others then
        tobrowser.message('error', 'error retrieving time: ' || SQLERRM
        );

end;

procedure get_leads
is
  lc_vSql varchar2(4000) := '';
begin
  lc_vSql := 'select LEADID,MODEL_TYPE,LOCATION,NFA,DESCRIPTION,REQUESTING_USER from spd_t_telestrator_leads order by LEADID desc';
  
  tobrowser.data(lc_vSql);
	
	exception
        when others then
        tobrowser.message('error', 'error retrieving time: ' || SQLERRM
        );

end;

procedure get_session_history


is 
	lc_vSql varchar2(2000);
begin
/*
  lc_vSql := 
  'SELECT * FROM (
  select 
    sessionkey,
    to_char(JOIN_TIMESTAMP,''DD-MON-YY'')||'' ''||description displayname
  from 
    spd_t_telestrator_drnk_history a, spd_t_telestrator_sessions b
    where a.sessionid = b.sessionid
      and username = ''' || APEX_UTIL.GET_SESSION_STATE('APP_USER') || '''
  order by
    a.join_timestamp desc
    )
    '; -- WHERE ROWNUM < 11
 */ 
  
  
  lc_vSql := 
  'SELECT * FROM (
  select 
    sessionkey,
    to_char(JOIN_TIMESTAMP,''DD-MON-YY'')||'' ''|| c.LOCATION ||'' ''|| DECODE(b.description, null, '' '', ''- ''||b.description) displayname
  from 
    spd_t_telestrator_drnk_history a, spd_t_telestrator_sessions b, spd_t_telestrator_scene_model c
    where a.sessionid = b.sessionid and b.scenemodelid = c.scenemodelid
      and username = ''' || APEX_UTIL.GET_SESSION_STATE('APP_USER') || '''
  order by
    a.join_timestamp desc
    )
    ';
  
	tobrowser.data(lc_vSql);
	
	exception
        when others then
        tobrowser.message('error', 'error retrieving list of scenes: ' || SQLERRM
        );
end;

/*
procedure get_model_versions(in_sourcemodelid number)
is
  lc_vSql varchar2(4000) := '';
begin
  lc_vSql := 'Select modelversionid,sourcemodelid,description,xyz,rotation,filepath from spd_t_telestrator_mdl_versions where sourcemodelid = '+in_sourcemodelid+' order by sourcemodelid desc';
  tobrowser.data(lc_vSql);
	
	exception
        when others then
        tobrowser.message('error', 'error retrieving time: ' || SQLERRM
        );
end;
*/
--T ABOVE
----
END DATAPROVIDER_T;

/
