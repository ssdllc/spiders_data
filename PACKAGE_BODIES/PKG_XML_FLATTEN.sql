create or replace PACKAGE BODY                "PKG_XML_FLATTEN" AS

FUNCTION convertXML (in_nXMLFileID number) return XMLTYPE AS
  lc_vFunctionName varchar2(255):='convertXML';
  
  v_doc dbms_xmldom.DOMDocument;
  v_transformer dbms_xmldom.DOMDocument;
  v_XSLprocessor dbms_xslprocessor.Processor;
  v_stylesheet dbms_xslprocessor.Stylesheet;
  v_clob clob;
  lc_xSourceXML xmltype;
  v_begin_op_time   NUMBER;
  v_end_op_time   NUMBER;
  V_TIME_TAKEN NUMBER;
  transformer varchar2(2000);
  lc_xReturn xmlType;
  
-----
vAuthor   varchar2(200) := 'mrussalesi';
vCreated  varchar2(200) := '20180526';
vUpdate   varchar2(200) := '20180526';
------

BEGIN

  transformer := q'~<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
			  <xsl:output method="xml"/>
			  <xsl:template match="/">
				<ROWSET>
				  <xsl:apply-templates select="*">
					<xsl:with-param name="lvl" select="0"/>
				  </xsl:apply-templates>
			  </ROWSET>
			</xsl:template>
			<xsl:template match="*">
			  <xsl:param name="pid"/>
			  <xsl:param name="pxp"/>
			  <xsl:param name="lvl"/>
			  <xsl:variable name="id" select="substring(generate-id(.),2)"/>
			  <xsl:variable name="name" select="name(.)"/>
			  <xsl:variable name="xp" select="concat($pxp,'/',$name,'[',count(preceding-sibling::*[name()=$name])+1,']')"/>
			  <ROW id="{$id}" pid="{$pid}" name="{$name}" xpath="{$xp}" level="{$lvl+1}">
				<xsl:value-of select="text()"/>
			  </ROW>
			  <xsl:apply-templates select="*|@*">
				<xsl:with-param name="pid" select="$id"/>
				<xsl:with-param name="pxp" select="$xp"/>
				<xsl:with-param name="lvl" select="$lvl+1"/>
			  </xsl:apply-templates>
			</xsl:template>
			<xsl:template match="@*">
			  <xsl:param name="pid"/>
			  <xsl:param name="pxp"/>
			  <xsl:param name="lvl"/>
			  <ROW id="{substring(generate-id(.),2)}" pid="{$pid}" name="{name(.)}" xpath="{concat($pxp,'/@',name(.))}" level="{$lvl}">
				<xsl:value-of select="."/>
			  </ROW>
			</xsl:template>
		  </xsl:stylesheet>~';
      
  v_transformer := dbms_xmldom.newDOMDocument(transformer);
  v_XSLprocessor := Dbms_Xslprocessor.newProcessor;
  v_stylesheet := dbms_xslprocessor.newStylesheet(v_transformer, ''); 
      
  select XML_DOCUMENT into lc_xSourceXML from spd_t_xml_files where xmlfileid = in_nXMLFileID; --430002048 --430002800
  v_doc := dbms_xmldom.newDOMDocument(lc_xSourceXML);
    
--reset the CLOB                    
  v_clob := ' ';

--Apply XSL Transform
  dbms_xslprocessor.processXSL(p => v_XSLprocessor, ss => v_stylesheet, xmldoc => v_Doc, cl => v_clob);
  --v_doc := dbms_xmldom.newDOMDocument(XMLType(v_clob));
  lc_xReturn := XMLType(v_clob);
  
  DBMS_XMLDOM.freeDocument(v_Doc);
  DBMS_LOB.freetemporary(lob_loc => v_clob);
    
  return lc_xReturn;
      
EXCEPTION WHEN OTHERS THEN
  execute immediate 'insert into debug (FUNCTIONNAME, INPUT, MESSAGE) values (''' || 'PKG_XML_PARSE.' || lc_vFunctionName || ''','''|| 'An error occured while cleaning the actions' ||''',''' || sqlerrm || ''')';
  --!!!With no return in the exception this might be an issue...--
  
END;


FUNCTION deleteExistingFlattenData (in_nADOID number, in_vUser varchar2) return number AS
  lc_vFunctionName varchar2(255):='deleteExistingFlattenData';
  
  lc_nReturn number := 1;
  
-----
vAuthor   varchar2(200) := 'mrussalesi';
vCreated  varchar2(200) := '20180526';
vUpdate   varchar2(200) := '20180526';
-----
begin

  delete from SPD_T_XML_FLATTEN WHERE CREATED_BY = in_vUser AND ADOID = in_nADOID;
  
  return lc_nReturn;

EXCEPTION WHEN OTHERS THEN
  execute immediate 'insert into debug (FUNCTIONNAME, INPUT, MESSAGE) values (''' || 'PKG_XML_PARSE.' || lc_vFunctionName || ''','''|| 'An error occured: ' ||''',''' || sqlerrm || ''')';
  return -1;
  
END;


function XMLFlattenDOM (xmldoc in XMLType) return XMLEdgeTable pipelined is
  lc_vFunctionName varchar2(255):='XMLFlattenDOM';
  
  lc_xDOM      dbms_xmldom.DOMDocument;
  node_list    dbms_xmldom.DOMNodeList;
  node_name     varchar2(2000);
  node          dbms_xmldom.DOMNode;
  attr             dbms_xmldom.DOMNode;
  attrList         dbms_xmldom.DOMNamedNodeMap;
  
  
  lc_xSourceXML xmltype;
  i             number := 0;
  len           number := 0;
  
  
  lc_nNode_id       number;
  lc_nParentid      number;
  lc_vNode_Name     varchar2(200);
  lc_vXpath         varchar2(4000);
  lc_vFpath         varchar2(4000);
  lc_nLevel         number;
  lc_cNodeValue     clob;
  lc_vLocalName     varchar2(200);
  
  edge             XMLEdgeTableRow := new XMLEdgeTableRow(null, null, null, null, null, null, null, null);

-----
vAuthor   varchar2(200) := 'mrussalesi';
vCreated  varchar2(200) := '20180526';
vUpdate   varchar2(200) := '20180526';
----- 
begin
  -- create the dom document from our example xmltype
  lc_xDOM := dbms_xmldom.newDOMDocument(xmldoc);
  
  -- find all text nodes in the dom document and return them into a node list
  node_list := DBMS_XMLDOM.getElementsByTagName(lc_xDOM, '*');
  len := DBMS_XMLDOM.getLength(node_list);

  FOR i IN 0 .. len - 1 LOOP
      node := DBMS_XMLDOM.item(node_list, i);
      lc_cNodeValue := SUBSTR(DBMS_XMLDOM.getNodeValue(DBMS_XMLDOM.getFirstChild(node)),1,3000);
      
      attrList := dbms_xmldom.getAttributes(node);
      if not dbms_xmldom.isNull(attrList) then

      for i2 in 0 .. dbms_xmldom.getLength(attrList)-1 loop
      
        --determine where the attribute value should go
        attr := dbms_xmldom.item(attrList, i2);
        dbms_xmldom.getLocalName(attr, lc_vLocalName);
        
        if lc_vLocalName = 'id' then
          lc_nNode_id := dbms_xmldom.getNodeValue(attr);
        end if;
        
        if lc_vLocalName = 'pid' then
          lc_nParentid := dbms_xmldom.getNodeValue(attr);
        end if;

        if lc_vLocalName = 'name' then
          lc_vNode_Name := dbms_xmldom.getNodeValue(attr);
        end if;
        
        if lc_vLocalName = 'level' then
          lc_nLevel := dbms_xmldom.getNodeValue(attr);
        end if;
        
        if lc_vLocalName = 'xpath' then
          lc_vXpath := dbms_xmldom.getNodeValue(attr);
        end if;
        
        if lc_vLocalName = 'fpath' then
          lc_vFpath := dbms_xmldom.getNodeValue(attr);
        end if;
        
        if lc_vLocalName is null then
          lc_cNodeValue := dbms_xmldom.getNodeValue(attr);
        end if;
        
      end loop;
      
        edge.NODE_POSITION := lc_nNode_id;
        edge.PARENTID := lc_nParentid;
        edge.NODE_NAME := lc_vNode_Name;
        edge.XPATH := lc_vXpath;
        edge.XMLLEVEL := lc_nLevel;
        edge.NODE_VALUE := lc_cNodeValue;
        edge.FPATH := REGEXP_REPLACE (lc_vXpath, '\[.*?\]', '');
        
        pipe row(edge);
      
      end if;
  
  END LOOP;
  
  dbms_xmldom.freeDocument(lc_xDOM); 
  
  return;

EXCEPTION WHEN OTHERS THEN
  execute immediate 'insert into debug (FUNCTIONNAME, INPUT, MESSAGE) values (''' || 'PKG_XML_PARSE.' || lc_vFunctionName || ''','''|| 'An error occured: ' ||''',''' || sqlerrm || ''')';


end XMLFlattenDOM;

--------------------------------------------------------------------------------------------------------------------------------
 
 

FUNCTION flattenXML(in_nfileId number, in_nADOID number, in_vUser varchar2) RETURN varchar2 as 

  lc_vFunctionName varchar2(255) := 'flattenXML';
  lc_nContinue number := 0;
	

-----
vAuthor   varchar2(200) := 'mrussalesi';
vCreated  varchar2(200) := '20180526';
vUpdate   varchar2(200) := '20180526';
----- 	
BEGIN
  
  -- Clean the xml flatten table
  lc_nContinue := deleteExistingFlattenData(in_nADOID,in_vUser);
  
  execute immediate 'insert into spd_t_xml_flatten
    (
      node_name,
      node_value,
      xmllevel,
      xpath,
      node_position,
      node_id,
      fpath,
      parentid,
      created_by,
      adoid
    )
  select 
    node_name,
    node_value,
    xmllevel,
    xpath,
    node_position,
    node_id,
    fpath,
    parentid,
    ''' || in_vUser || '''  created_by,
    ''' || in_nADOID || ''' adoid
  from 
    (
      select 
        x.*
      from 
        table(pkg_xml_flatten.xmlflattendom((pkg_xml_flatten.convertXML(' || in_nfileId || ')))
      ) x 
    )';


  return lc_nContinue; --this is not the final return, original was the count of the xml length...need to see what used the return.
      
EXCEPTION WHEN OTHERS THEN
  execute immediate 'insert into debug (FUNCTIONNAME,INPUT,MESSAGE) values ('''|| 'flattenXML' ||''','''|| 'An error occured while flattening  the file with the XMLFileID : '||in_nfileId || ''', ''' || lc_vFunctionName || ' dml error: ' || sqlerrm || ''')';
  return '{"error":"error while flattening a file"}';


end flattenXML;

END PKG_XML_FLATTEN;