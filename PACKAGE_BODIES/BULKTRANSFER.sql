--------------------------------------------------------
--  DDL for Package Body BULKTRANSFER
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE PACKAGE BODY "SPIDERS_DATA"."BULKTRANSFER" AS 

/*
This software has been released under the MIT license:

  Copyright (c) 2012 Synergy Software Design, LLC

  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.
*/


procedure updateProcessed(IN_nID number)
/*
Local procedure to be run at the end of processing xml data files
*/

is 

begin

update spd_t_load_progress set processedxml = '1' , processedxml_date = sysdate where loadid = IN_nID;
commit;



end;



procedure needsToBeMoved
/*
Call this procedure from the grid control. It will return a formatted string of data using dbms_output.put_line. The string will look like this:
1,FOLDERNAME1^2,FOLDERNAME2^,3,FOLDERNAME3^
The first number is the loadid. The second value is the foldername that should be on the ftp server.
*/
is 
lc_vReturn varchar2(2000) :='';

begin

for rec in (select * from spd_t_load_progress where uploadtoftp_date is not null and movedtoserver_date is null)
loop
  lc_vReturn := lc_vReturn || rec.loadid || ',' || rec.foldername || '^';
end loop;

dbms_output.put_line(lc_vReturn);

end;


procedure updateMoved(IN_nID number)
/*
Call this procedure from the grid control after the grid control procedure for SCP'ng the files is complete and successful. Use the LOADID returned from "needsToBeMoved" procedure.
*/
is 

begin

update spd_t_load_progress set uploadtoftp = '1' , uploadtoftp_date = sysdate where loadid = IN_nID;
commit;


end;


procedure needsToBeProcessed
/*
Call this procedure from the grid control (on a separate schedule than the "needsToBeMoved"). It will return a formatted string of data using dbms_output.put_line. The string will look like this:
1,2,3,4,5,
The number is the loadid.
*/
is 
lc_vReturn varchar2(2000) :='';

begin

for rec in (select * from spd_t_load_progress where movedtoserver_date is not null and processedxml_date is null)
loop
  lc_vReturn := lc_vReturn || rec.loadid || ',';
end loop;

dbms_output.put_line(lc_vReturn);

end;




END BULKTRANSFER;

/
