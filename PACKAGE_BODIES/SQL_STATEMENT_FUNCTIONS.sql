--------------------------------------------------------
--  DDL for Package Body SQL_STATEMENT_FUNCTIONS
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE PACKAGE BODY "SPIDERS_DATA"."SQL_STATEMENT_FUNCTIONS" as 

/*EDITED 20170913 MR COMMENT DEBUG: [SPD_BUG_FIX][03] Dry Dock Upload*/


function getRawTableName (in_vTableName varchar2) return varchar2 as 
  /* template variables */
  lc_vFunctionName varchar2(255) := 'getRawTableName';
  lc_vReturn varchar2(32000) := '{"error":"default"}'; --defaul the return to e for error
  lc_nContinue number :=0; --default to 0 to dissalow continuing
  
  /* function specific variables */
  lc_vTest varchar2(32000);
begin

  lc_vTest := substr(in_vTableName, 7, length(in_vTableName)-6);
  lc_vReturn := lc_vTest;
  return lc_vReturn;

exception     
  when others then
      execute immediate 'insert into debug (FUNCTIONNAME, INPUT, MESSAGE) values (''' || 'SQL_STATEMENT_FUNCTIONS.' || lc_vFunctionName || ''',''' || 'An error occured in getting the Raw Table Name' ||''','''||  sqlerrm || ''')';
      return '{"error":"' || sqlerrm || '"}';
end; 


function f_test_for_error (in_vReturned varchar2) return varchar2 as 
  /* template variables */
  lc_vFunctionName varchar2(255) := 'SQL_STATEMENT_FUNCTIONS.f_test_for_error';
  lc_vReturn varchar2(32000) := '{"error":"default"}'; --defaul the return to e for error
  lc_nContinue number :=0; --default to 0 to dissalow continuing
  
  /* function specific variables */
  lc_vTest varchar2(32000) := in_vReturned;
begin

  if substr(lc_vTest, 1, 9) = '{"error":' then
    lc_vReturn := '0';
  else
    lc_vReturn := '1';
  end if;
  
  return lc_vReturn;

exception     
  when others then
      execute immediate 'insert into debug (FUNCTIONNAME, INPUT, MESSAGE) values (''' || 'SQL_STATEMENT_FUNCTIONS.' || lc_vFunctionName || ''',''' || 'An error occured in f_test_for_error' ||''','''||  sqlerrm || ''')';
      return '{"error":"' || sqlerrm || '"}';
end; 

function getCorrespondingJsonColumnVal(in_jJson json, in_vColumn varchar2) return varchar2 as 
  /* template variables */
  json_error EXCEPTION;
  spiders_error EXCEPTION;
  lc_vErrorDescription varchar2(255) := 'default';
  lc_vFunctionName varchar2(255) := 'getCorrespondingJsonColumnVal';
  lc_vReturn varchar2(32000) := '{"error":"default"}'; --defaul the return to e for error
  lc_nContinue number :=0; --default to 0 to dissalow continuing
  
  /* function specific variables */
  lc_vName varchar2(32000);
  lc_vTemp varchar2(32000);
  lc_vComment varchar2(32000);
  lc_jvValue json_value;
  lc_jlData json_list;
  lc_jvRecVal json_value;
  lc_jValue json;
  
  /*EDITED 20170915 MR TURNED OFF SPIDERS ERROR DEBUG WHEN NO CORRESPONDING COLUMN FOUND: [SPD3D][45] Sync*/
  
begin
  --insert into debug (input) values ('begin column name - '||in_vColumn);
  /* i could not convince myself that we needed multiple functions here, since we still need to check if attributes exist in the sub functions*/
  if in_vColumn not in ('UPDATED_TIMESTAMP','UPDATED_BY','CREATED_TIMESTAMP','CREATED_BY') then
    if(in_jJson.exist('data')) then
        if(in_jJson.get('data').is_array) then
            --insert into debug (input) values ('begin2 column name - '||in_vColumn);
            lc_jlData := json_list(in_jJson.get('data'));        
            lc_jvRecVal := lc_jlData.get(1); --return null on outofbounds
            if (lc_jvRecVal is not null) then
              --insert into debug (input) values ('begin3 column name - '||in_vColumn);
              lc_jValue := json(lc_jvRecVal);            
              if(lc_jValue.exist(in_vColumn)) then    
                --insert into debug (input) values ('begin4 column name - '||in_vColumn);
                lc_jvValue := lc_jValue.get(in_vColumn);
                --insert into debug (input) values ('begin4.5 column name - '||in_vColumn);
                lc_vReturn := getVal2(lc_jvValue);
                --insert into debug (input) values ('begin5 column name - '||in_vColumn);
              else
                lc_vErrorDescription := in_vColumn || ' attribute not found in json';
                --raise spiders_error;
              end if;
            end if;
        end if;
    end if;
   end if;        
   --insert into debug (message) values ('made it to the end of getCorrespondingJsonColumnValue');
   --insert into debug (input) values ('end column name - '||in_vColumn); -- (lc_vReturn);
  return lc_vReturn;

exception     
  when spiders_error then
      execute immediate 'insert into debug (FUNCTIONNAME, INPUT, MESSAGE) values (''' || 'SQL_STATEMENT_FUNCTIONS.' || lc_vFunctionName || ''',''' || ' SPIDERS ERROR : the attribute '||in_vColumn|| ' was not found in the JSON' ||''','''||  sqlerrm || ''')';
      return '{"error":"' || lc_vErrorDescription || '"}';
  when json_error then
      execute immediate 'insert into debug (FUNCTIONNAME, INPUT, MESSAGE) values (''' || 'SQL_STATEMENT_FUNCTIONS.' || lc_vFunctionName || ''',''' || 'An JSON error occured in getCorrespondingJsonColumnVal' ||''','''||  sqlerrm || ''')';
      return '{"error":"json"}';
  when others then
      execute immediate 'insert into debug (FUNCTIONNAME, INPUT, MESSAGE) values (''' || 'SQL_STATEMENT_FUNCTIONS.' || lc_vFunctionName || ''',''' || 'An error occured in getCorrespondingJsonColumnVal' ||''','''||  sqlerrm || ''')';
      return '{"error":"' || sqlerrm || '"}';
end; 

function isDate(in_vValue varchar2) return DATE as 
  /* template variables */
  lc_vFunctionName varchar2(255) := 'formatDate';
  lc_vReturn varchar2(32000) := '{"error":"default"}'; --defaul the return to e for error
  lc_nContinue number :=0; --default to 0 to dissalow continuing
  
  /* function specific variables */
  lc_vValue varchar2(32000);
begin
 
  return to_date(in_vValue);

exception     
  when others then
      execute immediate 'insert into debug (FUNCTIONNAME, INPUT, MESSAGE) values (''' || 'SQL_STATEMENT_FUNCTIONS.' || lc_vFunctionName || ''',''' || 'An error occured in isDate' ||''','''||  sqlerrm || ''')';
      return null;
end; 


function formatDate(in_vValue varchar2) return varchar2 as 
  /* template variables */
  lc_vFunctionName varchar2(255) := 'formatDate';
  lc_vReturn varchar2(32000) := '{"error":"default"}'; --defaul the return to e for error
  lc_nContinue number :=0; --default to 0 to dissalow continuing
  
  /* function specific variables */
  lc_vValue varchar2(32000);
  lc_dDate Date;
  
begin
  lc_dDate := isdate(in_vValue);
  
  --THIS DOES NOT APPEAR TO BE USED
  if lc_dDate IS Null then
    lc_vValue := 'null';
  else
    lc_vValue := 'to_date(''' || in_vValue || ''',''DD-MON-YYYY'')';
  end if;
  

  lc_vReturn := lc_vValue;
  return lc_vReturn;

exception     
  when others then
      execute immediate 'insert into debug (FUNCTIONNAME, INPUT, MESSAGE) values (''' || 'SQL_STATEMENT_FUNCTIONS.' || lc_vFunctionName || ''',''' || 'An error occured in '|| lc_vFunctionName ||''','''||  sqlerrm || ''')';
      return '{"error":"' || sqlerrm || '"}';
end; 

function formatString(in_vValue varchar2) return varchar2 as 
  /* template variables */
  lc_vFunctionName varchar2(255) := 'formatString';
  lc_vReturn varchar2(32000) := '{"error":"default"}'; --defaul the return to e for error
  lc_nContinue number :=0; --default to 0 to dissalow continuing
  
  /* function specific variables */
  lc_vValue varchar2(32000);
begin

  if in_vValue = null then
    lc_vValue := null;
  else 
    lc_vValue := 'q''[' || in_vValue || ']''';
    --THIS REMOVES THE DOUBLE SINGLE QUOTE THAT WAS GETTING ADDED TO EMPTY STRINGS MR 20170913
    if(lc_vValue = 'q''['''']''') then
      lc_vValue := '''''';
    end if;
  end if;
  
  lc_vReturn := lc_vValue;
  return lc_vReturn;

exception     
  when others then
      execute immediate 'insert into debug (FUNCTIONNAME, INPUT, MESSAGE) values (''' || 'SQL_STATEMENT_FUNCTIONS.' || lc_vFunctionName || ''',''' || 'An error occured in '|| lc_vFunctionName ||''','''||  sqlerrm || ''')';
      return '{"error":"' || sqlerrm || '"}';
end; 

function isNumber(in_vString varchar2) return number as
v_new_num NUMBER;
lc_vFunctionName varchar2(30) := 'isNumber';
BEGIN
   v_new_num := TO_NUMBER(in_vString);
   RETURN 1;
EXCEPTION
WHEN VALUE_ERROR THEN
   --execute immediate 'insert into debug (FUNCTIONNAME, INPUT, MESSAGE) values (''' || 'SQL_STATEMENT_FUNCTIONS.' || lc_vFunctionName || ''',''' || 'An error occured in '|| lc_vFunctionName ||''','''||  sqlerrm || ''')';
   RETURN 0;
end;


function formatNumber(in_vValue varchar2) return varchar2 as 
  /* template variables */
  lc_vFunctionName varchar2(255) := 'formatNumber';
  lc_vReturn varchar2(32000) := '{"error":"default"}'; --defaul the return to e for error
  lc_nContinue number :=0; --default to 0 to dissalow continuing
  
  /* function specific variables */
  lc_vValue varchar2(32000) := null;
begin



  lc_nContinue := isNumber(in_vValue);
  
  -- If the in_vValue is a number 
  if lc_nContinue = 0 then 
    lc_vValue := 'null';
  else 
    lc_vValue := in_vValue;
  end if;
  
  lc_vReturn := lc_vValue;
  return lc_vReturn;

exception     
  when others then
  execute immediate 'insert into debug (FUNCTIONNAME, INPUT, MESSAGE) values (''' || 'SQL_STATEMENT_FUNCTIONS.' || lc_vFunctionName || ''',''' || 'An error occured in '|| lc_vFunctionName ||''','''||  sqlerrm || ''')';
   return '{"error":"' || sqlerrm || '"}';
end; 


function getUpdateSegment(in_vTableName varchar2) return varchar2 as 
  /* template variables */
  lc_vFunctionName varchar2(255) := 'getUpdateSegment';
  lc_vReturn varchar2(32000) := '{"error":"default"}'; --defaul the return to e for error
  lc_nContinue number :=0; --default to 0 to dissalow continuing
  
  /* function specific variables */
  lc_vValue varchar2(32000);
begin

  lc_vValue := 'UPDATE ' || in_vTableName || ' '; 
  
  lc_vReturn := lc_vValue;
  return lc_vReturn;

exception     
  when others then
      execute immediate 'insert into debug (FUNCTIONNAME, INPUT, MESSAGE) values (''' || 'SQL_STATEMENT_FUNCTIONS.' || lc_vFunctionName || ''',''' || 'An error occured in '|| lc_vFunctionName ||''','''||  sqlerrm || ''')';
   return '{"error":"' || sqlerrm || '"}';
end; 

function getTablePrimaryKeyColumn(in_vTableName varchar2) return varchar2 as 
  /* template variables */
  lc_vFunctionName varchar2(255) := 'getTablePrimaryKeyColumn';
  lc_vReturn varchar2(32000) := '{"error":"default"}'; --defaul the return to e for error
  lc_nContinue number :=0; --default to 0 to dissalow continuing
  
  /* function specific variables */
  lc_vValue varchar2(32000);
  lc_vUser varchar2(25) := 'SPIDERS_DATA';
begin

  SELECT 
    cols.column_name into lc_vValue
  FROM 
    all_constraints cons, 
    all_cons_columns cols 
  WHERE 
    cons.owner = lc_vUser
    AND cons.constraint_type = 'P' 
    AND cons.constraint_name = cols.constraint_name 
    AND cons.owner = cols.owner 
    AND cons.table_name = in_vTableName;
  
  lc_vReturn := lc_vValue;
  return lc_vReturn;

exception     
  when others then
     execute immediate 'insert into debug (FUNCTIONNAME, INPUT, MESSAGE) values (''' || 'SQL_STATEMENT_FUNCTIONS.' || lc_vFunctionName || ''',''' || 'An error occured in '|| lc_vFunctionName ||''','''||  sqlerrm || ''')';
      return '{"error":"' || sqlerrm || '"}';
end; 

function getWhereSegment(in_jJson json, in_vTableName varchar2) return varchar2 as 
  /* template variables */
  lc_vFunctionName varchar2(255) := 'getWhereSegment';
  lc_vErrorDescription varchar2(255) := 'default';
  lc_vReturn varchar2(32000) := '{"error":"default"}'; --defaul the return to e for error
  lc_nContinue number :=0; --default to 0 to dissalow continuing
  
  /* function specific variables */
  lc_vValue varchar2(32000);
  lc_vID varchar2(255);
  lc_vPrimaryKeyColumn varchar2(255);
  lc_vWhere varchar2(255);
begin

  
  lc_vPrimaryKeyColumn := getTablePrimaryKeyColumn(in_vTableName);
  lc_nContinue := f_test_for_error(lc_vPrimaryKeyColumn);
  
  if lc_nContinue = 1 then
    lc_vID := getCorrespondingJsonColumnVal(in_jJson, lc_vPrimaryKeyColumn);
    lc_nContinue := f_test_for_error(lc_vID);
  else
    lc_vErrorDescription := 'Primary key not found';
    --raise spiders_error; dont raise for now
  end if;
  

  if lc_nContinue = 1 then
    lc_vWhere := lc_vPrimaryKeyColumn || '=' || lc_vID;
    lc_nContinue := f_test_for_error(lc_vWhere);
  else
    lc_vErrorDescription := 'Primary key value not found in json';
    --raise spiders_error; dont raise for now
  end if;
  
  
  lc_vValue := ' WHERE ' || lc_vWhere; 
  
  lc_vReturn := lc_vValue;
  return lc_vReturn;

exception     
  when others then
      execute immediate 'insert into debug (FUNCTIONNAME, INPUT, MESSAGE) values (''' || 'SQL_STATEMENT_FUNCTIONS.' || lc_vFunctionName || ''',''' || 'An error occured in '|| lc_vFunctionName ||''','''||  sqlerrm || ''')';
      return '{"error":"' || sqlerrm || '"}';
end; 


function getSetSegment(in_vValue varchar2, in_vDataType varchar2, in_vColumnName varchar2) return varchar2 as 
  /* template variables */
  json_error EXCEPTION;
  spiders_error EXCEPTION;
  lc_vErrorDescription varchar2(255) := 'default';
  lc_vFunctionName varchar2(255) := 'getSetSegment';
  lc_vReturn varchar2(32000) := '{"error":"default"}'; --defaul the return to e for error
  lc_nContinue number :=0; --default to 0 to dissalow continuing
  
  /* function specific variables */
  lc_vSegment varchar2(32000);

begin

  /* get name of object */
  CASE in_vDataType
    WHEN 'NUMBER' 
      THEN 
        lc_vSegment := in_vColumnName || '=' || nvl(in_vValue,'null');
    WHEN 'DATE' 
      THEN 
        lc_vSegment := in_vColumnName || '=' || formatDate(in_vValue);
    ELSE 
      lc_vSegment := in_vColumnName || '=' || formatString(in_vValue);
  END CASE;
  
  lc_vReturn := lc_vSegment;
  return lc_vReturn;

exception     
  when spiders_error then
      execute immediate 'insert into debug (FUNCTIONNAME, INPUT, MESSAGE) values (''' || 'SQL_STATEMENT_FUNCTIONS.' || lc_vFunctionName || ''',''' || ' SPIDERS ERROR An error occured in '|| lc_vFunctionName ||''','''||  sqlerrm || ''')';
      return '{"error":"' || lc_vErrorDescription || '"}';
  when json_error then
      execute immediate 'insert into debug (FUNCTIONNAME, INPUT, MESSAGE) values (''' || 'SQL_STATEMENT_FUNCTIONS.' || lc_vFunctionName || ''',''' || 'JSON ERROR An error occured in '|| lc_vFunctionName ||''','''||  sqlerrm || ''')';
      return '{"error":"json"}';
  when others then
      execute immediate 'insert into debug (FUNCTIONNAME, INPUT, MESSAGE) values (''' || 'SQL_STATEMENT_FUNCTIONS.' || lc_vFunctionName || ''',''' || 'An error occured in '|| lc_vFunctionName ||''','''||  sqlerrm || ''')';
      return '{"error":"' || sqlerrm || '"}';
end; 

function getValueSegment(in_vValue varchar2, in_vDataType varchar2, in_vColumnName varchar2) return varchar2 as 
  /* template variables */
  json_error EXCEPTION;
  spiders_error EXCEPTION;
  lc_vErrorDescription varchar2(255) := 'default';
  lc_vFunctionName varchar2(255) := 'getValueSegment';
  lc_vReturn varchar2(32000) := '{"error":"default"}'; --defaul the return to e for error
  lc_nContinue number :=0; --default to 0 to dissalow continuing
  
  /* function specific variables */
  lc_vSegment varchar2(32000);
  lc_vTemp varchar2(32000);
  lc_vComment varchar2(32000);
begin

  /* get name of object */
  CASE in_vDataType
    WHEN 'NUMBER' 
      THEN 
        lc_vSegment := formatNumber(in_vValue);
    WHEN 'DATE' 
      THEN 
        lc_vSegment := formatDate(in_vValue);
    ELSE 
        lc_vSegment := formatString(in_vValue);
  END CASE;
  
  lc_vReturn := lc_vSegment;
  return lc_vReturn;

exception     
  when spiders_error then
      execute immediate 'insert into debug (FUNCTIONNAME, INPUT, MESSAGE) values (''' || 'SQL_STATEMENT_FUNCTIONS.' || lc_vFunctionName || ''',''' || 'SPIDERS ERROR : An error occured in '|| lc_vFunctionName ||''','''||  sqlerrm || ''')';
      return '{"error":"' || lc_vErrorDescription || '"}';
  when json_error then
      execute immediate 'insert into debug (FUNCTIONNAME, INPUT, MESSAGE) values (''' || 'SQL_STATEMENT_FUNCTIONS.' || lc_vFunctionName || ''',''' || 'JSON ERROR : An error occured in '|| lc_vFunctionName ||''','''||  sqlerrm || ''')';
      return '{"error":"json"}';
  when others then
      execute immediate 'insert into debug (FUNCTIONNAME, INPUT, MESSAGE) values (''' || 'SQL_STATEMENT_FUNCTIONS.' || lc_vFunctionName || ''',''' || 'An error occured in '|| lc_vFunctionName ||''','''||  sqlerrm || ''')';
      return '{"error":"' || sqlerrm || '"}';
end; 

function getColumnsForSet(in_jJson json, in_vTableName varchar2) return varchar2 as 
  /* template variables */
  json_error EXCEPTION;
  spiders_error EXCEPTION;
  lc_vErrorDescription varchar2(255) := 'default';
  lc_vFunctionName varchar2(255) := 'getColumnsForSet';
  lc_vReturn varchar2(32000) := '{"error":"default"}'; --defaul the return to e for error
  lc_nContinue number :=0; --default to 0 to dissalow continuing
  
  /* function specific variables */
  lc_vSetSegment varchar2(32000);
  lc_vTemp varchar2(32000);
  lc_vValue varchar2(32000);
  lc_vComment varchar2(32000);
  lc_vUser varchar2(25) := 'SPIDERS_DATA';
  lc_nCount number;
begin
   
  lc_nCount := 1;
  /* get name of object */
  for rec in (
    select 
      column_name, 
      data_type, 
      rownum 
    from 
      user_tab_cols 
    where 
      table_name = in_vTableName 
      and column_name not in (SELECT 
                                cols.column_name
                              FROM 
                                all_constraints cons, 
                                all_cons_columns cols 
                              WHERE 
                                cons.owner = lc_vUser
                                AND cons.constraint_type = 'P' 
                                AND cons.constraint_name = cols.constraint_name 
                                AND cons.owner = cols.owner 
                                AND cons.table_name = in_vTableName) 
    order by 
      column_id)
  loop
    lc_vValue := getCorrespondingJsonColumnVal(in_jJson, rec.column_name);
    lc_nContinue := f_test_for_error(lc_vValue);
    /*!!I LEFT OFF NOT KNOWING WHY THE VALUES ARENT GETTING TO THE GETSEGMENT FUNCTION*/
    if lc_nContinue = 1 then
      lc_vTemp := getSetSegment(lc_vValue,rec.data_type, rec.column_name);
      lc_nContinue := f_test_for_error(lc_vTemp);
    else
      lc_vErrorDescription := rec.column_name || ' value not found in json';
      --raise spiders_error; dont raise for now
    end if;
    
    if lc_nContinue = 1 then
      if lc_nCount = 1 then
        lc_vSetSegment := lc_vSetSegment || lc_vTemp;
      else
        lc_vSetSegment := lc_vSetSegment || ',' || lc_vTemp;
      end if;
      lc_nCount := lc_nCount + 1;
    end if;
    --lc_nCount := lc_nCount + 1;
  end loop;
  
  lc_vReturn := ' SET ' || lc_vSetSegment;
  return lc_vReturn;

exception     
  when spiders_error then
      execute immediate 'insert into debug (FUNCTIONNAME, INPUT, MESSAGE) values (''' || 'SQL_STATEMENT_FUNCTIONS.' || lc_vFunctionName || ''',''' || 'SPIDERS ERROR An error occured in '|| lc_vFunctionName ||''','''||  sqlerrm || ''')';
      return '{"error":"' || lc_vErrorDescription || '"}';
  when json_error then
      execute immediate 'insert into debug (FUNCTIONNAME, INPUT, MESSAGE) values (''' || 'SQL_STATEMENT_FUNCTIONS.' || lc_vFunctionName || ''',''' || 'JSON ERROR An error occured in '|| lc_vFunctionName ||''','''||  sqlerrm || ''')';
      return '{"error":"json"}';
  when others then
      execute immediate 'insert into debug (FUNCTIONNAME, INPUT, MESSAGE) values (''' || 'SQL_STATEMENT_FUNCTIONS.' || lc_vFunctionName || ''',''' || 'An error occured in '|| lc_vFunctionName ||''','''||  sqlerrm || ''')';
      return '{"error":"' || sqlerrm || '"}';
end; 



function f_update(in_jJson json, in_vTableName varchar2) return varchar2 as 
  /* template variables */
  json_error EXCEPTION;
  spiders_error EXCEPTION;
  lc_vErrorDescription varchar2(255) := 'default';
  lc_vFunctionName varchar2(255) := 'f_update';
  lc_vPrefix varchar2(255) := 'SPD_T_';
  lc_vReturn varchar2(32000) := '{"error":"default"}'; --defaul the return to e for error
  lc_nContinue number :=0; --default to 0 to dissalow continuing
  
  /* function specific variables */
  lc_vTableName varchar2(32000);
  lc_vTemp varchar2(32000);
  lc_vComment varchar2(32000);
  lc_vSetSegment varchar2(32000);
  lc_vUpdateSegment varchar2(32000);
  lc_vWhereSegment varchar2(32000);
  lc_vSql varchar2(32000);
begin

  lc_vTableName := lc_vPrefix || in_vTableName;
  
  /* get name of object */
  lc_vSetSegment := getColumnsForSet(in_jJson, lc_vTableName);
  lc_nContinue := f_test_for_error(lc_vSetSegment);
  
  if lc_nContinue = 1 then
    lc_vUpdateSegment := getUpdateSegment(lc_vTableName);
    lc_nContinue := f_test_for_error(lc_vUpdateSegment);
  end if;
  
  if lc_nContinue = 1 then
    lc_vWhereSegment := getWhereSegment(in_jJson, lc_vTableName);
    lc_nContinue := f_test_for_error(lc_vWhereSegment);
  end if;
  
  if lc_nContinue = 1 then
    lc_vSql := lc_vUpdateSegment || lc_vSetSegment || lc_vWhereSegment;
  end if;
  
  lc_vReturn := lc_vSql;
  return lc_vReturn;

exception     
  when spiders_error then
      execute immediate 'insert into debug (FUNCTIONNAME, INPUT, MESSAGE) values (''' || 'SQL_STATEMENT_FUNCTIONS.' || lc_vFunctionName || ''',''' || 'SPIDERS ERROR An error occured in '|| lc_vFunctionName ||''','''||  sqlerrm || ''')';
      return '{"error":"' || lc_vErrorDescription || '"}';
  when json_error then
      execute immediate 'insert into debug (FUNCTIONNAME, INPUT, MESSAGE) values (''' || 'SQL_STATEMENT_FUNCTIONS.' || lc_vFunctionName || ''',''' || 'JSON ERROR An error occured in '|| lc_vFunctionName ||''','''||  sqlerrm || ''')';
      return '{"error":"json"}';
  when others then
      execute immediate 'insert into debug (FUNCTIONNAME, INPUT, MESSAGE) values (''' || 'SQL_STATEMENT_FUNCTIONS.' || lc_vFunctionName || ''',''' || 'An error occured in '|| lc_vFunctionName ||''','''||  sqlerrm || ''')';
      return '{"error":"' || sqlerrm || '"}';
end; 


function getNextSequenceValue(in_vTableName varchar2) return varchar2 as 
  /* template variables */
  lc_vFunctionName varchar2(255) := 'getNextSequence';
  lc_vReturn varchar2(32000) := '{"error":"default"}'; --defaul the return to e for error
  lc_vPrefix varchar2(255) := 'SPD_S_';
  lc_nContinue number :=0; --default to 0 to dissalow continuing
  
  /* function specific variables */
  lc_vValue varchar2(32000);
begin

  execute immediate 'select ' || lc_vPrefix || in_vTableName || '.nextval from dual' into lc_vValue; 
  
  lc_vReturn := lc_vValue;
  return lc_vReturn;

exception     
  when others then
      execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' dml error: ' || sqlerrm || ''')';
      return '{"error":"' || sqlerrm || '"}';
end; 


function getInsert(in_vTableName varchar2) return varchar2 as 
  /* template variables */
  lc_vFunctionName varchar2(255) := 'getInsert';
  lc_vReturn varchar2(32000) := '{"error":"default"}'; --defaul the return to e for error
  lc_nContinue number :=0; --default to 0 to dissalow continuing
  
  /* function specific variables */
  lc_vValue varchar2(32000);
begin

  lc_vValue := 'INSERT INTO ' || in_vTableName || ' '; 
  
  lc_vReturn := lc_vValue;
  return lc_vReturn;

exception     
  when others then
      execute immediate 'insert into debug (FUNCTIONNAME, INPUT, MESSAGE) values (''' || 'SQL_STATEMENT_FUNCTIONS.' || lc_vFunctionName || ''',''' || 'An error occured in '|| lc_vFunctionName ||''','''||  sqlerrm || ''')';
      return '{"error":"' || sqlerrm || '"}';
end; 

function getDelete(in_vTableName varchar2) return varchar2 as 
  /* template variables */
  lc_vFunctionName varchar2(255) := 'getInsert';
  lc_vReturn varchar2(32000) := '{"error":"default"}'; --defaul the return to e for error
  lc_nContinue number :=0; --default to 0 to dissalow continuing
  
  /* function specific variables */
  lc_vValue varchar2(32000);
begin

  lc_vValue := 'delete from ' || in_vTableName || ' '; 
  
  lc_vReturn := lc_vValue;
  return lc_vReturn;

exception     
  when others then
      execute immediate 'insert into debug (FUNCTIONNAME, INPUT, MESSAGE) values (''' || 'SQL_STATEMENT_FUNCTIONS.' || lc_vFunctionName || ''',''' || 'An error occured in '|| lc_vFunctionName ||''','''||  sqlerrm || ''')';
      return '{"error":"' || sqlerrm || '"}';
end; 

function getDeleteWhere(in_vID number, in_vColumnName varchar2) return varchar2 as 
  /* template variables */
  lc_vFunctionName varchar2(255) := 'getInsert';
  lc_vReturn varchar2(32000) := '{"error":"default"}'; --defaul the return to e for error
  lc_nContinue number :=0; --default to 0 to dissalow continuing
  
  /* function specific variables */
  lc_vValue varchar2(32000);
begin

  lc_vValue := 'where ' || in_vColumnName || '=' || in_vID; 
  
  lc_vReturn := lc_vValue;
  return lc_vReturn;

exception     
  when others then
      execute immediate 'insert into debug (FUNCTIONNAME, INPUT, MESSAGE) values (''' || 'SQL_STATEMENT_FUNCTIONS.' || lc_vFunctionName || ''',''' || 'An error occured in '|| lc_vFunctionName ||''','''||  sqlerrm || ''')';
      return '{"error":"' || sqlerrm || '"}';
end; 

function getValuesFromJsonForInsert(in_jJson json, in_vTableName varchar2, o_nSequenceId out number) return varchar2 as 
  /* template variables */
  json_error EXCEPTION;
  spiders_error EXCEPTION;
  lc_vErrorDescription varchar2(255) := 'default';
  lc_vFunctionName varchar2(255) := 'getValuesFromJsonForInsert';
  lc_vReturn varchar2(32000) := '{"error":"default"}'; --defaul the return to e for error
  lc_nContinue number :=0; --default to 0 to dissalow continuing
  
  /* function specific variables */
  lc_vValuesSegment varchar2(32000);
  lc_vTemp varchar2(32000);
  lc_vValue varchar2(32000);
  lc_vComment varchar2(32000);
  lc_vPrimaryKeyColumn varchar2(255);
  lc_nCount number;
begin

  lc_nCount := 1;
  lc_vPrimaryKeyColumn := getTablePrimaryKeyColumn(in_vTableName);
  lc_nContinue := f_test_for_error(lc_vPrimaryKeyColumn);
  
  if lc_nContinue = 1 then
    for rec in (
      select 
        column_name, 
        data_type, 
        rownum 
      from 
        user_tab_cols 
      where 
        table_name = in_vTableName 
        AND column_name not in ('UPDATED_TIMESTAMP','UPDATED_BY','CREATED_TIMESTAMP','CREATED_BY')
      order by 
        column_id)
    loop
    
      if rec.column_name = lc_vPrimaryKeyColumn then
        lc_vTemp := getNextSequenceValue(getRawTableName (in_vTableName));
        o_nSequenceId := lc_vTemp;
        lc_nContinue := f_test_for_error(lc_vTemp);
      else
        lc_vValue := getCorrespondingJsonColumnVal(in_jJson, rec.column_name);
        lc_nContinue := f_test_for_error(lc_vValue);
        /*!!I LEFT OFF NOT KNOWING WHY THE VALUES ARENT GETTING TO THE GETSEGMENT FUNCTION*/
        if lc_nContinue = 1 then
          lc_vTemp := getValueSegment(lc_vValue,rec.data_type, rec.column_name);
          lc_nContinue := f_test_for_error(lc_vTemp);
        else
          lc_vErrorDescription := rec.column_name || ' value not found in json';
          --raise spiders_error; dont raise for now
        end if;
      end if;
      
      if lc_nContinue = 1 then
        if lc_nCount = 1 then
          lc_vValuesSegment := lc_vValuesSegment || lc_vTemp;
        else
          lc_vValuesSegment := lc_vValuesSegment || ',' || lc_vTemp;
        end if;
      end if;
      lc_nCount := lc_nCount + 1;
    end loop;
  else
    lc_vErrorDescription := 'Primary key not found';
    --raise spiders_error; dont raise for now
  end if;
  

  
  lc_vReturn := lc_vValuesSegment;
  return lc_vReturn;

exception     
  when spiders_error then
      execute immediate 'insert into debug (FUNCTIONNAME, INPUT, MESSAGE) values (''' || 'SQL_STATEMENT_FUNCTIONS.' || lc_vFunctionName || ''',''' || 'SPIDERS ERROR An error occured in '|| lc_vFunctionName ||''','''||  sqlerrm || ''')';
      return '{"error":"' || lc_vErrorDescription || '"}';
  when json_error then
      execute immediate 'insert into debug (FUNCTIONNAME, INPUT, MESSAGE) values (''' || 'SQL_STATEMENT_FUNCTIONS.' || lc_vFunctionName || ''',''' || 'JSON ERROR An error occured in '|| lc_vFunctionName ||''','''||  sqlerrm || ''')';
      return '{"error":"json"}';
  when others then
      execute immediate 'insert into debug (FUNCTIONNAME, INPUT, MESSAGE) values (''' || 'SQL_STATEMENT_FUNCTIONS.' || lc_vFunctionName || ''',''' || 'An error occured in '|| lc_vFunctionName ||''','''||  sqlerrm || ''')';
      return '{"error":"' || sqlerrm || '"}';
end; 

function buildValuesFromJsonForInsert(in_jJson json, in_vTableName varchar2, o_nSequenceId out number) return varchar2 as 
  /* template variables */
  json_error EXCEPTION;
  spiders_error EXCEPTION;
  lc_vErrorDescription varchar2(255) := 'default';
  lc_vFunctionName varchar2(255) := 'buildValuesFromJsonForInsert';
  lc_vReturn varchar2(32000) := '{"error":"default"}'; --defaul the return to e for error
  lc_nContinue number :=0; --default to 0 to dissalow continuing
  
  /* function specific variables */
  lc_vValuesSegment varchar2(32000);
  lc_vTemp varchar2(32000);
  lc_vValue varchar2(32000);
  lc_vComment varchar2(32000);
  lc_vPrimaryKeyColumn varchar2(255);
begin

  --insert into debug (message) values ('above is the table name causing primary key problem');
  --insert into debug (input) values (in_vTableName);
  lc_vPrimaryKeyColumn := getTablePrimaryKeyColumn(in_vTableName);
  lc_nContinue := f_test_for_error(lc_vPrimaryKeyColumn);
  
  if lc_nContinue = 1 then
    lc_vValuesSegment := getValuesFromJsonForInsert(in_jJson, in_vTableName, o_nSequenceId);
    lc_nContinue := f_test_for_error(lc_vValuesSegment);
  else
    lc_vErrorDescription := 'Primary key not found';
    --raise spiders_error; dont raise for now
  end if;
  
  if lc_nContinue = 1 then
    lc_vReturn := ' values ( ' || lc_vValuesSegment || ')';
  else
    lc_vErrorDescription := 'Error in building the values segment';
    --raise spiders_error; dont raise for now
  end if;
  
  
  return lc_vReturn;

exception     
  when spiders_error then
      execute immediate 'insert into debug (FUNCTIONNAME, INPUT, MESSAGE) values (''' || 'SQL_STATEMENT_FUNCTIONS.' || lc_vFunctionName || ''',''' || 'SPIDERS ERROR : An error occured in '|| lc_vFunctionName ||''','''||  sqlerrm || ''')';
      return '{"error":"' || lc_vErrorDescription || '"}';
  when json_error then
      execute immediate 'insert into debug (FUNCTIONNAME, INPUT, MESSAGE) values (''' || 'SQL_STATEMENT_FUNCTIONS.' || lc_vFunctionName || ''',''' || 'JSON ERROR : An error occured in '|| lc_vFunctionName ||''','''||  sqlerrm || ''')';
      return '{"error":"json"}';
  when others then
      execute immediate 'insert into debug (FUNCTIONNAME, INPUT, MESSAGE) values (''' || 'SQL_STATEMENT_FUNCTIONS.' || lc_vFunctionName || ''',''' || 'An error occured in '|| lc_vFunctionName ||''','''||  sqlerrm || ''')';
      return '{"error":"' || sqlerrm || '"}';
end; 


function getColumnsForInsert(in_jJson json, in_vTableName varchar2) return varchar2 as 
  /* template variables */
  json_error EXCEPTION;
  spiders_error EXCEPTION;
  lc_vErrorDescription varchar2(255) := 'default';
  lc_vFunctionName varchar2(255) := 'getColumnsForInsert';
  lc_vReturn varchar2(32000) := '{"error":"default"}'; --defaul the return to e for error
  lc_nContinue number :=0; --default to 0 to dissalow continuing
  
  /* function specific variables */
  lc_vColumnsSegment varchar2(32000);
  lc_vTemp varchar2(32000);
  lc_vValue varchar2(32000);
  lc_vComment varchar2(32000);
  lc_nCount number;
begin

  lc_nCount := 1;
  /* get name of object */
  for rec in (
    select 
      column_name, 
      data_type, 
      rownum 
    from 
      user_tab_cols 
    where 
      table_name = in_vTableName  
    order by 
      column_id)
  loop
    lc_vValue := getCorrespondingJsonColumnVal(in_jJson, rec.column_name);
    lc_nContinue := f_test_for_error(lc_vValue);
    
    if lc_nContinue = 1 then
      if lc_nCount = 1 then
        lc_vColumnsSegment := lc_vColumnsSegment || rec.column_name;
      else
        lc_vColumnsSegment := lc_vColumnsSegment || ',' || rec.column_name;
      end if;
    else
      lc_vErrorDescription := rec.column_name || ' value not found in json';
      --raise spiders_error; dont raise for now
    end if;
    lc_nCount:= lc_nCount+1;
  end loop;
  
  lc_vReturn := ' ( ' || lc_vColumnsSegment || ') ';
  return lc_vReturn;

exception     
  when spiders_error then
      execute immediate 'insert into debug (FUNCTIONNAME, INPUT, MESSAGE) values (''' || 'SQL_STATEMENT_FUNCTIONS.' || lc_vFunctionName || ''',''' || 'SPIDERS ERROR : An error occured in '|| lc_vFunctionName ||''','''||  sqlerrm || ''')';
      return '{"error":"' || lc_vErrorDescription || '"}';
  when json_error then
      execute immediate 'insert into debug (FUNCTIONNAME, INPUT, MESSAGE) values (''' || 'SQL_STATEMENT_FUNCTIONS.' || lc_vFunctionName || ''',''' || 'JSON ERROR : An error occured in '|| lc_vFunctionName ||''','''||  sqlerrm || ''')';
      return '{"error":"json"}';
  when others then
      execute immediate 'insert into debug (FUNCTIONNAME, INPUT, MESSAGE) values (''' || 'SQL_STATEMENT_FUNCTIONS.' || lc_vFunctionName || ''',''' || 'An error occured in '|| lc_vFunctionName ||''','''||  sqlerrm || ''')';
      return '{"error":"' || sqlerrm || '"}';
end; 

function f_insert(in_jJson json, in_vTableName varchar2, o_nSequenceId2 out number) return varchar2 as 
  /* template variables */
  json_error EXCEPTION;
  spiders_error EXCEPTION;
  lc_vErrorDescription varchar2(255) := 'default';
  lc_vFunctionName varchar2(255) := 'f_insert';
  lc_vPrefix varchar2(255) := 'SPD_T_';
  lc_vReturn varchar2(32000) := '{"error":"default"}'; --defaul the return to e for error
  lc_nContinue number :=0; --default to 0 to dissalow continuing
  
  /* function specific variables */
  lc_vTableName varchar2(32000);
  lc_vTemp varchar2(32000);
  lc_vComment varchar2(32000);
  lc_vValuesSegment varchar2(32000);
  lc_vColumnsSegment varchar2(32000);
  lc_vInsertSegment varchar2(32000);
  lc_vSql varchar2(32000);
  lc_nSequenceId number;
begin

  --execute immediate 'insert into debug (FUNCTIONNAME, INPUT, MESSAGE) values (''' || 'SQL_STATEMENT_FUNCTIONS.' || lc_vFunctionName || ''',''' || 'Table Name : '|| in_vTableName ||''','''||  in_jJson.to_char || ''')';
      

  lc_vTableName := lc_vPrefix || in_vTableName;
  /* get name of object */
  lc_vColumnsSegment := getColumnsForInsert(in_jJson, lc_vTableName);
  lc_nContinue := f_test_for_error(lc_vColumnsSegment);
  
  
  if lc_nContinue = 1 then
    lc_vValuesSegment := buildValuesFromJsonForInsert(in_jJson, lc_vTableName, lc_nSequenceId);
    o_nSequenceId2 := lc_nSequenceId;
    lc_nContinue := f_test_for_error(lc_vValuesSegment);
  end if;
  
  if lc_nContinue = 1 then
    lc_vInsertSegment := getInsert(lc_vTableName);
    lc_nContinue := f_test_for_error(lc_vInsertSegment);
  end if;
  
  if lc_nContinue = 1 then
    lc_vSql := lc_vInsertSegment || lc_vColumnsSegment || lc_vValuesSegment;
  end if;
      
  lc_vReturn := lc_vSql;
  return lc_vReturn;

exception     
  when spiders_error then
      execute immediate 'insert into debug (FUNCTIONNAME, INPUT, MESSAGE) values (''' || 'SQL_STATEMENT_FUNCTIONS.' || lc_vFunctionName || ''',''' || 'SPIDERS ERROR : An error occured in '|| lc_vFunctionName ||''','''||  sqlerrm || ''')';
      return '{"error":"' || lc_vErrorDescription || '"}';
  when json_error then
      execute immediate 'insert into debug (FUNCTIONNAME, INPUT, MESSAGE) values (''' || 'SQL_STATEMENT_FUNCTIONS.' || lc_vFunctionName || ''',''' || 'JSON ERROR : An error occured in '|| lc_vFunctionName ||''','''||  sqlerrm || ''')';
      return '{"error":"json"}';
  when others then
      execute immediate 'insert into debug (FUNCTIONNAME, INPUT, MESSAGE) values (''' || 'SQL_STATEMENT_FUNCTIONS.' || lc_vFunctionName || ''',''' || 'An error occured in '|| lc_vFunctionName ||''','''||  sqlerrm || ''')';
      return '{"error":"' || sqlerrm || '"}';
end; 

function f_delete(in_jJson json, in_vTableName varchar2) return varchar2 as 
  /* template variables */
  json_error EXCEPTION;
  spiders_error EXCEPTION;
  lc_vErrorDescription varchar2(255) := 'default';
  lc_vFunctionName varchar2(255) := 'f_delete';
  lc_vPrefix varchar2(255) := 'SPD_T_';
  lc_vReturn varchar2(32000) := '{"error":"default"}'; --defaul the return to e for error
  lc_nContinue number :=0; --default to 0 to dissalow continuing
  
  /* function specific variables */
  lc_vTableName varchar2(32000);
  lc_vPrimaryKeyColumn varchar2(32000);
  lc_vID varchar2(32000);
  lc_vComment varchar2(32000);
  lc_vDeleteWhereSegment varchar2(32000);
  lc_vDeleteSegment varchar2(32000);
  lc_vSql varchar2(32000);
begin

  lc_vTableName:= lc_vPrefix || in_vTableName;
  /* get name of object */
  lc_vPrimaryKeyColumn := getTablePrimaryKeyColumn(lc_vTableName);
  lc_nContinue := f_test_for_error(lc_vPrimaryKeyColumn);
  
  if lc_nContinue = 1 then
    lc_vID := getCorrespondingJsonColumnVal(in_jJson, lc_vPrimaryKeyColumn);
    lc_nContinue := f_test_for_error(lc_vID);
  else
    lc_vErrorDescription := 'Primary key not found';
    --raise spiders_error; dont raise for now
  end if;
  
  if lc_nContinue = 1 then
    lc_vDeleteWhereSegment := getDeleteWhere(lc_vID, lc_vPrimaryKeyColumn);
    lc_nContinue := f_test_for_error(lc_vDeleteWhereSegment);
  else
    lc_vErrorDescription := 'Primary key not found';
    --raise spiders_error; dont raise for now
  end if; 
  
  if lc_nContinue = 1 then
    lc_vDeleteSegment := getDelete(lc_vTableName);
    lc_nContinue := f_test_for_error(lc_vDeleteSegment);
  end if;
  
  if lc_nContinue = 1 then
    lc_vSql := lc_vDeleteSegment || lc_vDeleteWhereSegment;
  end if;
  
  lc_vReturn := lc_vSql;
  return lc_vReturn;

exception     
  when spiders_error then
      execute immediate 'insert into debug (FUNCTIONNAME, INPUT, MESSAGE) values (''' || 'SQL_STATEMENT_FUNCTIONS.' || lc_vFunctionName || ''',''' || 'SPIDERS ERROR : An error occured in '|| lc_vFunctionName ||''','''||  sqlerrm || ''')';
      return '{"error":"' || lc_vErrorDescription || '"}';
  when json_error then
      execute immediate 'insert into debug (FUNCTIONNAME, INPUT, MESSAGE) values (''' || 'SQL_STATEMENT_FUNCTIONS.' || lc_vFunctionName || ''',''' || 'JSON ERROR : An error occured in '|| lc_vFunctionName ||''','''||  sqlerrm || ''')';
      return '{"error":"json"}';
  when others then
      execute immediate 'insert into debug (FUNCTIONNAME, INPUT, MESSAGE) values (''' || 'SQL_STATEMENT_FUNCTIONS.' || lc_vFunctionName || ''',''' || 'An error occured in '|| lc_vFunctionName ||''','''||  sqlerrm || ''')';
      return '{"error":"' || sqlerrm || '"}';
end; 
end;

/
