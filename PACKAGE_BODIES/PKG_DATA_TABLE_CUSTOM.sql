--------------------------------------------------------
--  DDL for Package Body PKG_DATA_TABLE_CUSTOM
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE PACKAGE BODY "SPIDERS_DATA"."PKG_DATA_TABLE_CUSTOM" as

/*note, public functions are underscores, local functions are camel case*/

procedure p_log(in_vString varchar2) as 

begin
 dbms_output.put_line(in_vString);
end;

procedure p_log_htp(in_vString varchar2) as 

begin
 htp.p(in_vString);
end;

function f_test_for_error (in_vReturned varchar2) return varchar2 as 
 /* template variables */
 lc_vFunctionName varchar2(255) := 'f_test_for_error';
 lc_vReturn varchar2(32000) := '{"error":"default"}'; --defaul the return to e for error
 lc_nContinue number :=0; --default to 0 to dissalow continuing

 /* function specific variables */
 lc_vTest varchar2(32000) := in_vReturned;
begin

 if substr(lc_vTest, 1, 9) = '{"error":' then
   lc_vReturn := '0';
 else
   lc_vReturn := '1';
 end if;

 return lc_vReturn;

exception     
 when others then
     execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' dml error: ' || sqlerrm || ''')';
     return '{"error":"' || sqlerrm || '"}';
end; 

function f_parseGroupFilterData(in_jJson json, in_vTableName varchar2) return varchar2 as
 /* template variables */
 lc_vFunctionName varchar2(255) := 'f_parseGroupFilterData';

 json_error EXCEPTION;
 spiders_error EXCEPTION;
 lc_vErrorDescription varchar2(255) := 'default';

 lc_vReturn varchar2(32000) := '{"error":"default"}'; --defaul the return to e for error
 lc_nContinue number :=0; --default to 0 to dissalow continuing


 /* function specific variables */
 lc_jlData json_list;
 lc_jvRec json_value;
 lc_jValue json;
 lc_vGroupName varchar2(255);
 lc_vInClause long;
 lc_jvValue json_value;
 lc_jlDataValue json_list;
 lc_vValue varchar2(255);
 lc_vGroupWhere varchar(255);
 lc_nGroupCount number := 1;
begin

 if(in_jJson.exist('groupFilterData')) then
     if(in_jJson.get('groupFilterData').is_array) then
         lc_jlData := json_list(in_jJson.get('groupFilterData'));
         if(lc_jlData.count>0) then
           for rec in 1..lc_jlData.count loop
             lc_jvRec := lc_jlData.get(rec);
             lc_jValue := json(lc_jvRec);
             if (lc_jValue.exist('group_name')) then
                 lc_jvValue := lc_jValue.get('group_name');
                 lc_vGroupName := getVal(lc_jvValue); 
                 if (lc_jValue.exist('data')) then
                   lc_jlDataValue := json_list(lc_jValue.get('data'));
                   lc_vInClause := '' || lc_vGroupName || ' in (';
                   for rec2 in 1..lc_jlDataValue.count loop
                     lc_jvValue := lc_jlDataValue.get(rec2);                  
                     lc_vValue := getVal(lc_jvValue,1);   
                     if(rec2 = 1) then
                       lc_vInClause := lc_vInClause || lc_vValue;
                     else
                       lc_vInClause := lc_vInClause || ',' || lc_vValue;
                     end if;  
                   end loop;
                   lc_vInClause := lc_vInClause || ')';
                   if(lc_nGroupCount=1) then
                     lc_vGroupWhere := lc_vGroupWhere || lc_vInClause;
                   else
                     lc_vGroupWhere := lc_vGroupWhere || ' AND ' || lc_vInClause;
                   end if;
                   lc_nGroupCount := lc_nGroupCount + 1;
                 else
                   lc_vErrorDescription := 'data does not exist';
                 end if; 
             else
               lc_vErrorDescription := 'group_name does not exist';
             end if;
           end loop;
         else
           lc_vGroupWhere := '';
         end if;
     else
       lc_vErrorDescription := 'groupFilterData does not an array';
     end if;
 else
   lc_vErrorDescription := 'groupFilterData does not exist';
   --raise spiders_error;
   lc_vGroupWhere := '';
 end if;

 lc_vReturn := lc_vGroupWhere;
 return lc_vReturn;

exception   
 when spiders_error then
     execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' SPIDERS error ' || lc_vErrorDescription || ''')';
     return '{"error":"' || lc_vErrorDescription || '"}';
 when json_error then
     execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' JSON error'')';
     return '{"error":"json"}';
 when others then
     execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' dml error: ' || sqlerrm || ''')';
     return '{"error":"' || sqlerrm || '"}';
end; 

function f_parseAoDataSearch(in_jJson json) return varchar2 as
 /* template variables */
 lc_vFunctionName varchar2(255) := 'f_parseAoDataSearch';

 json_error EXCEPTION;
 spiders_error EXCEPTION;
 lc_vErrorDescription varchar2(255) := 'default';

 lc_vReturn varchar2(32000) := '{"error":"default"}'; --defaul the return to e for error
 lc_nContinue number :=0; --default to 0 to dissalow continuing


 /* function specific variables */
 lc_jlAoData json_list;
 lc_jvSearch json_value;
 lc_jValue json;
 lc_jValue2 json;
 lc_jvValue json_value;
 lc_jvValue2 json_value;
 lc_vSearch varchar2(255);
 lc_vValue varchar2(255);
begin


 if(in_jJson.exist('aoData')) then
   lc_jlAoData := json_list(in_jJson.get('aoData'));

   lc_jvSearch := lc_jlAoData.get(6);
   lc_jValue := json(lc_jvSearch);

   lc_jvValue := lc_jValue.get('value');
   lc_jValue2 := json(lc_jvValue);

   lc_jvValue2 := lc_jValue2.get('value');

   lc_vSearch := getVal(lc_jvValue2);
   lc_vSearch := lower(lc_vSearch);
   lc_vReturn := lc_vSearch;
 else
   lc_vErrorDescription := 'aoData does not exist';
   raise spiders_error;
 end if;

 return lc_vReturn;

exception   
 when spiders_error then
     execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' SPIDERS error ' || lc_vErrorDescription || ''')';
     return '{"error":"' || lc_vErrorDescription || '"}';
 when json_error then
     execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' JSON error'')';
     return '{"error":"json"}';
 when others then
     execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' dml error: ' || sqlerrm || ''')';
     return '{"error":"' || sqlerrm || '"}';
end; 

function f_parseAoDataStart(in_jJson json) return varchar2 as
 /* template variables */
 lc_vFunctionName varchar2(255) := 'f_parseAoDataStart';

 json_error EXCEPTION;
 spiders_error EXCEPTION;
 lc_vErrorDescription varchar2(255) := 'default';

 lc_vReturn varchar2(32000) := '{"error":"default"}'; --defaul the return to e for error
 lc_nContinue number :=0; --default to 0 to dissalow continuing


 /* function specific variables */
 lc_jlAoData json_list;
 lc_jvStart json_value;
 lc_jValue json;
 lc_jvValue json_value;
 lc_nStart number;
begin

 if(in_jJson.exist('aoData')) then
   lc_jlAoData := json_list(in_jJson.get('aoData'));
   lc_jvStart := lc_jlAoData.get(4);
   lc_jValue := json(lc_jvStart);
   lc_jvValue := lc_jValue.get('value');
   lc_nStart := getVal(lc_jvValue);
   lc_nStart := lc_nStart + 1;
   lc_vReturn := lc_nStart;
 else
   lc_vErrorDescription := 'aoData does not exist';
   raise spiders_error;
 end if;

 return lc_vReturn;

exception   
 when spiders_error then
     execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' SPIDERS error ' || lc_vErrorDescription || ''')';
     return '{"error":"' || lc_vErrorDescription || '"}';
 when json_error then
     execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' JSON error'')';
     return '{"error":"json"}';
 when others then
     execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' dml error: ' || sqlerrm || ''')';
     return '{"error":"' || sqlerrm || '"}';
end; 

function f_parseAoDataRecPerPage(in_jJson json) return varchar2 as
 /* template variables */
 lc_vFunctionName varchar2(255) := 'f_parseAoDataRecPerPage';

 json_error EXCEPTION;
 spiders_error EXCEPTION;
 lc_vErrorDescription varchar2(255) := 'default';

 lc_vReturn varchar2(32000) := '{"error":"default"}'; --defaul the return to e for error
 lc_nContinue number :=0; --default to 0 to dissalow continuing


 /* function specific variables */
 lc_jlAoData json_list;
 lc_jvRecordsPerPage json_value;
 lc_jValue json;
 lc_jvValue json_value;
 lc_nRecordsPerPage number;
begin

 if(in_jJson.exist('aoData')) then
   lc_jlAoData := json_list(in_jJson.get('aoData'));
   lc_jvRecordsPerPage := lc_jlAoData.get(5);
   lc_jValue := json(lc_jvRecordsPerPage);
   lc_jvValue := lc_jValue.get('value');
   lc_nRecordsPerPage := getVal(lc_jvValue);
   lc_vReturn := lc_nRecordsPerPage;
 else
   lc_vErrorDescription := 'aoData does not exist';
   raise spiders_error;
 end if;

 return lc_vReturn;

exception   
 when spiders_error then
     execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' SPIDERS error ' || lc_vErrorDescription || ''')';
     return '{"error":"' || lc_vErrorDescription || '"}';
 when json_error then
     execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' JSON error'')';
     return '{"error":"json"}';
 when others then
     execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' dml error: ' || sqlerrm || ''')';
     return '{"error":"' || sqlerrm || '"}';
end; 

function getAoDataOrderByColumnName (in_jJson json, in_nColumn number) return varchar2 as
 /* template variables */
 lc_vFunctionName varchar2(255) := 'getAoDataOrderByColumnName';

 json_error EXCEPTION;
 spiders_error EXCEPTION;
 lc_vErrorDescription varchar2(255) := 'default';

 lc_vReturn varchar2(32000) := '{"error":"default"}'; --defaul the return to e for error
 lc_nContinue number :=0; --default to 0 to dissalow continuing


 /* function specific variables */
 lc_jlAoData json_list;
 lc_jvColumns json_value;
 lc_jValue json;
 lc_jvValue json_value;
 lc_jlOrderByColumns json_list;
 lc_jValue2 json;
 lc_jvOrderByColumn json_value;
 lc_jColumn json;
 lc_jvColumnIndex json_value;  
 lc_jDirection json;
 lc_jvDirection json_value;  
 lc_vDirection varchar2(255);
 lc_vOrderBy varchar2(255);
 lc_nColumnIndex number;
 lc_jvColumnData json_value;
 lc_vColumnData varchar2(255);
begin

 if(in_jJson.exist('aoData')) then
   lc_jlAoData := json_list(in_jJson.get('aoData'));
   lc_jvColumns := lc_jlAoData.get(2);
   lc_jValue := json(lc_jvColumns);

   lc_jvValue := lc_jValue.get('value');

   lc_jlOrderByColumns := json_list(lc_jvValue);

   --set the column index
   lc_jvOrderByColumn := lc_jlOrderByColumns.get(in_nColumn);


   --NEED TO GET THE COLUMN NAME FROM THE INDEX
   lc_jColumn := json(lc_jvOrderByColumn);
   lc_jvColumnData := lc_jColumn.get('data');
   lc_vColumnData := getVal(lc_jvColumnData);

   lc_vReturn := lc_vColumnData;

 else
   lc_vErrorDescription := 'aoData does not exist';
   raise spiders_error;
 end if;

 return lc_vReturn;

exception   
 when spiders_error then
     execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' SPIDERS error ' || lc_vErrorDescription || ''')';
     return '{"error":"' || lc_vErrorDescription || '"}';
 when json_error then
     execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' JSON error'')';
     return '{"error":"json"}';
 when others then
     execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' dml error: ' || sqlerrm || ''')';
     return '{"error":"' || sqlerrm || '"}';
end; 


function f_parseAoDataOrderBy(in_jJson json) return varchar2 as
 /* template variables */
 lc_vFunctionName varchar2(255) := 'f_parseAoDataOrderBy';

 json_error EXCEPTION;
 spiders_error EXCEPTION;
 lc_vErrorDescription varchar2(255) := 'default';

 lc_vReturn varchar2(32000) := '{"error":"default"}'; --defaul the return to e for error
 lc_nContinue number :=0; --default to 0 to dissalow continuing


 /* function specific variables */
 lc_jlAoData json_list;
 lc_jvOrder json_value;
 lc_jValue json;
 lc_jvValue json_value;
 lc_jlOrderByColumns json_list;
 lc_jValue2 json;
 lc_jvOrderByColumn json_value;
 lc_jColumnIndex json;
 lc_jvColumnIndex json_value;  
 lc_jDirection json;
 lc_jvDirection json_value;  
 lc_vDirection varchar2(255);
 lc_vOrderBy varchar2(255);
 lc_nColumnIndex number;
 lc_vColumnName varchar2(255);
begin

 if(in_jJson.exist('aoData')) then
   lc_jlAoData := json_list(in_jJson.get('aoData'));
   lc_jvOrder := lc_jlAoData.get(3);
   lc_jValue := json(lc_jvOrder);

   lc_jvValue := lc_jValue.get('value');

   lc_jlOrderByColumns := json_list(lc_jvValue);
   for rec in 1..lc_jlOrderByColumns.count loop
     --set the column index
     lc_jvOrderByColumn := lc_jlOrderByColumns.get(rec);


     --NEED TO GET THE COLUMN NAME FROM THE INDEX
     lc_jColumnIndex := json(lc_jvOrderByColumn);
     lc_jvColumnIndex := lc_jColumnIndex.get('column');
     lc_nColumnIndex := getVal(lc_jvColumnIndex);
     lc_nColumnIndex := lc_nColumnIndex + 1;
     lc_vColumnName := getAoDataOrderByColumnName (in_jJson,lc_nColumnIndex);

     --set the order by direction
     lc_jvDirection := lc_jColumnIndex.get('dir');
     lc_vDirection := getVal(lc_jvDirection);

     if rec = 1 then
       lc_vOrderBy := 'lower(' || lc_vColumnName || ') ' || lc_vDirection;
     else
       lc_vOrderBy := lc_vOrderBy || ', lower(' || lc_vColumnName || ') ' || lc_vDirection;
     end if;
   end loop;

   lc_vReturn := lc_vOrderBy;

 else
   lc_vErrorDescription := 'aoData does not exist';
   raise spiders_error;
 end if;

 return lc_vReturn;

exception   
 when spiders_error then
     execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' SPIDERS error ' || lc_vErrorDescription || ''')';
     return '{"error":"' || lc_vErrorDescription || '"}';
 when json_error then
     execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' JSON error'')';
     return '{"error":"json"}';
 when others then
     execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' dml error: ' || sqlerrm || ''')';
     return '{"error":"' || sqlerrm || '"}';
end; 

function f_getDynamicWhere(in_vSearch varchar2, in_vTableName varchar2) return varchar2 as
 /* template variables */
 lc_vFunctionName varchar2(255) := 'f_getDynamicWhere';

 json_error EXCEPTION;
 spiders_error EXCEPTION;
 lc_vErrorDescription varchar2(255) := 'default';

 lc_vReturn varchar2(32000) := '{"error":"default"}'; --defaul the return to e for error
 lc_nContinue number :=0; --default to 0 to dissalow continuing


 /* function specific variables */
 lc_vWhereSegment long;
 lc_vTemp varchar2(255);
 lc_vPrefix varchar2(255) :='SPD_V_DT_';
 lc_nCount number;
 lc_vColumnWhere long;
begin

--note: probably want to check if the object exists first.
 lc_nCount := 1;

 for rec in (
   select 
     column_name, 
     data_type, 
     rownum 
   from 
     user_tab_cols 
   where 
     table_name = lc_vPrefix || in_vTableName 
     --and lower(column_name) not like '%id' this doesnt work with FACILITY_ID for an NFA number
     and lower(column_name) not like '%count%'
     and data_type = 'VARCHAR2'
   order by 
     column_id)
 loop
   lc_vTemp := rec.column_name;
   if lc_nCount = 1 then
     lc_vWhereSegment := '(upper(' || lc_vTemp|| ') like ''%' || upper(in_vSearch) || '%''';
   else
     lc_vColumnWhere := 'upper(' || lc_vTemp|| ') like ''%' || upper(in_vSearch) || '%''';
     lc_vWhereSegment := lc_vWhereSegment || ' or ' || lc_vColumnWhere;
   end if;
   lc_nCount := lc_nCount + 1;
 end loop;

 if length(lc_vWhereSegment)>0 then
   lc_vWhereSegment := lc_vWhereSegment || ')';
 end if;

 lc_vReturn := lc_vWhereSegment;
 return lc_vReturn;

exception   
 when spiders_error then
     execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' SPIDERS error ' || lc_vErrorDescription || ''')';
     return '{"error":"' || lc_vErrorDescription || '"}';
 when json_error then
     execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' JSON error'')';
     return '{"error":"json"}';
 when others then
     execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' dml error: ' || sqlerrm || ''')';
     return '{"error":"' || sqlerrm || '"}';
end; 

function f_buildFullWhere(in_vWhereClause varchar2, in_vDynamicWhere varchar2, in_vGroupWhere varchar2 ) return varchar2 as
 /* template variables */
 lc_vFunctionName varchar2(255) := 'f_buildFullWhere';

 json_error EXCEPTION;
 spiders_error EXCEPTION;
 lc_vErrorDescription varchar2(255) := 'default';

 lc_vReturn varchar2(32000) := '{"error":"default"}'; --defaul the return to e for error
 lc_nContinue number :=0; --default to 0 to dissalow continuing


 /* function specific variables */
 lc_vWhereSegment long;
begin

 lc_vWhereSegment := in_vWhereClause;
 if length(in_vWhereClause) > 0 then
   lc_vWhereSegment := in_vWhereClause;

   if length(in_vDynamicWhere) > 0 then
     if length(in_vGroupWhere) > 0 then
       lc_vWhereSegment := lc_vWhereSegment || ' AND ' || in_vDynamicWhere || ' AND ' || in_vGroupWhere ; /*WE MAY NEED TO WRAP GROUP IN A PARENTHESIS*/
     else
       lc_vWhereSegment := lc_vWhereSegment || ' AND ' || in_vDynamicWhere;
     end if;
   else
     null; -- NO OTHER WHERE STUFF
   end if;
 else
   if length(in_vDynamicWhere) > 0 then
     if length(in_vGroupWhere) > 0 then
       lc_vWhereSegment := in_vDynamicWhere || ' AND ' || in_vGroupWhere ; /*WE MAY NEED TO WRAP GROUP IN A PARENTHESIS*/
     else
       lc_vWhereSegment := in_vDynamicWhere;
     end if;
   else
     if length(in_vGroupWhere) > 0 then
       lc_vWhereSegment := in_vGroupWhere;
     else
       lc_vWhereSegment := '1=1';
     end if;
   end if;
 end if;


 lc_vReturn := lc_vWhereSegment;
 return lc_vReturn;

exception   
 when spiders_error then
     execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' SPIDERS error ' || lc_vErrorDescription || ''')';
     return '{"error":"' || lc_vErrorDescription || '"}';
 when json_error then
     execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' JSON error'')';
     return '{"error":"json"}';
 when others then
     execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' dml error: ' || sqlerrm || ''')';
     return '{"error":"' || sqlerrm || '"}';
end; 

function f_setOrderByFunction(in_vOrderBy varchar2, in_vTarget varchar2) return varchar2 as
 /* template variables */
 lc_vFunctionName varchar2(255) := 'f_setOrderByFunction';

 json_error EXCEPTION;
 spiders_error EXCEPTION;
 lc_vErrorDescription varchar2(255) := 'default';

 lc_vReturn varchar2(32000) := '{"error":"default"}'; --defaul the return to e for error
 lc_nContinue number :=0; --default to 0 to dissalow continuing


 /* function specific variables */
 lc_vOrderBy long;
begin

 lc_vOrderBy := in_vOrderBy;

 if in_vTarget = 'FACILITY' then

   lc_vOrderBy := replace(lc_vOrderBy,'lower(','');
   lc_vOrderBy := replace(lc_vOrderBy,')','');

 else

   if instr(in_vOrderBy, 'MIN') > 0 then
     lc_vOrderBy := replace(in_vOrderBy,'lower','to_number');
   end if;

   if instr(in_vOrderBy, 'MAX') > 0 then
     lc_vOrderBy := replace(in_vOrderBy,'lower','to_number');
   end if;

   if instr(in_vOrderBy, 'AVG') > 0 then
     lc_vOrderBy := replace(in_vOrderBy,'lower','to_number');
   end if;

   if instr(in_vOrderBy, 'COUNT') > 0 then
     lc_vOrderBy := replace(in_vOrderBy,'lower','to_number');
   end if;

   if instr(in_vOrderBy, 'DATE') > 0 then
     lc_vOrderBy := replace(in_vOrderBy,'lower','to_date');
   end if;

 end if;

 lc_vReturn := lc_vOrderBy;
 return lc_vReturn;

exception   
 when spiders_error then
     execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' SPIDERS error ' || lc_vErrorDescription || ''')';
     return '{"error":"' || lc_vErrorDescription || '"}';
 when json_error then
     execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' JSON error'')';
     return '{"error":"json"}';
 when others then
     execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' dml error: ' || sqlerrm || ''')';
     return '{"error":"' || sqlerrm || '"}';
end; 

function f_buildFullSql (in_vOrderBy varchar2, in_vStart varchar2, in_nRecordsPerPage number, in_vModule varchar2, in_vTarget varchar2) return json as
 /* template variables */
 lc_vFunctionName varchar2(255) := 'f_buildFullSql';

 json_error EXCEPTION;
 spiders_error EXCEPTION;
 lc_vErrorDescription varchar2(255) := 'default';

 lc_vReturn varchar2(32000) := '{"error":"default"}'; --defaul the return to e for error
 lc_nContinue number :=0; --default to 0 to dissalow continuing


 /* function specific variables */
 lc_vSql long;
 lc_vWhere long;
 lc_jJson json;
 lc_vInnerSql long;
 lc_vOrderBy long;
begin

 for rec in (
   select
     *
   from
     spd_t_query_defs
   where
     rest_endpoint = 'READDATATABLECUSTOM'
     and module = in_vModule
     and target = in_vTarget
 ) loop


 --THE ORDER BY NEEDS TO WORK FOR NUMBERS!!!!
 if length(in_vOrderBy) > 0 then
   lc_vOrderBy := ' order by ' || f_setOrderByFunction(in_vOrderBy, in_vTarget);
 else
   lc_vOrderBy := rec.outer_where_text;
 end if;

   lc_vSql := 'SELECT  
   *
 FROM    
   (
     SELECT  
       t.*, 
       ROWNUM AS rn
     FROM
       (' || rec.outer_select_text || ' ' || rec.select_text || ' ' || rec.from_text || ' ' || pkg_query_collection_custom.f_getFilterWhereClause || ' ' || rec.group_by_text || ' ' || lc_vOrderBy || 
       ') t
   )
 WHERE   
   rn >= ' || in_vStart || '
   AND rownum <= ' || in_nRecordsPerPage;
   lc_nContinue :=1;
 end loop;

 --htp.p(lc_vSql || '<br>');
--htp.p(' pkg_data_table_custom.f_buildFullSql-lc_vSql: '||lc_vSql||' ');
--insert into debug (message) values (lc_vSql);

 lc_jJson := json_dyn.executeObject(lc_vSql);
 return lc_jJson;

exception   
 when spiders_error then
     execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' SPIDERS error ' || lc_vErrorDescription || ''')';
     return json('{"error":"' || lc_vErrorDescription || '"}');
 when json_error then
     execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' JSON error'')';
     return json('{"error":"json"}');
 when others then
     execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' dml error: ' || sqlerrm || ''')';
     return json('{"error":"' || sqlerrm || '"}');
end; 

function f_buildFullSqlString (in_vTableName varchar2, in_vWhere varchar2, in_vOrderBy varchar2) return varchar2 as
  /* template variables */
  lc_vFunctionName varchar2(255) := 'f_buildFullSqlString';

  json_error EXCEPTION;
  spiders_error EXCEPTION;
  lc_vErrorDescription varchar2(255) := 'default';

  lc_vReturn varchar2(32000) := '{"error":"default"}'; --defaul the return to e for error
  lc_nContinue number :=0; --default to 0 to dissalow continuing

  
  /* function specific variables */
  lc_vSql long;
  lc_vWhere long;
  lc_jJson json;
begin

  lc_vSql := 'SELECT  
        t.*, 
        ROWNUM AS rn
      FROM
        (
          select 
            *
          from 
            spd_v_dt_' || in_vTableName || '
          where
            ' || in_vWhere || '
          order by 
            ' || in_vOrderBy || '
        ) t';

  lc_vReturn := lc_vSql;
  return lc_vReturn;

exception   
  when spiders_error then
      execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' SPIDERS error ' || lc_vErrorDescription || ''')';
      return '{"error":"' || lc_vErrorDescription || '"}';
  when json_error then
      execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' JSON error'')';
      return '{"error":"json"}';
  when others then
      execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' dml error: ' || sqlerrm || ''')';
      return '{"error":"' || sqlerrm || '"}';
end; 

function f_buildFullSqlString (in_vOrderBy varchar2, in_vStart varchar2, in_nRecordsPerPage number, in_vModule varchar2, in_vTarget varchar2) return varchar2 as
 /* template variables */
 lc_vFunctionName varchar2(255) := 'f_buildFullSqlString';

 json_error EXCEPTION;
 spiders_error EXCEPTION;
 lc_vErrorDescription varchar2(255) := 'default';

 lc_vReturn varchar2(32000) := '{"error":"default"}'; --defaul the return to e for error
 lc_nContinue number :=0; --default to 0 to dissalow continuing


 /* function specific variables */
 lc_vSql long;
 lc_vWhere long;
 lc_jJson json;
 lc_vInnerSql long;
 lc_vOrderBy long;
begin

 for rec in (
   select
     *
   from
     spd_t_query_defs
   where
     rest_endpoint = 'READDATATABLECUSTOM'
     and module = in_vModule
     and target = in_vTarget
 ) loop


 --THE ORDER BY NEEDS TO WORK FOR NUMBERS!!!!
 if length(in_vOrderBy) > 0 then
   lc_vOrderBy := ' order by ' || f_setOrderByFunction(in_vOrderBy, in_vTarget);
 else
   lc_vOrderBy := rec.outer_where_text;
 end if;

   lc_vSql := '
     SELECT  
       t.*, 
       ROWNUM AS rn
     FROM
       (' || rec.outer_select_text || ' ' || rec.select_text || ' ' || rec.from_text || ' ' || pkg_query_collection_custom.f_getFilterWhereClause || ' ' || rec.group_by_text || ' ' || lc_vOrderBy || 
       ') t';
       
       
   lc_nContinue :=1;
 end loop;

 --htp.p(lc_vSql || '<br>');
--htp.p(' pkg_data_table_custom.f_buildFullSql-lc_vSql: '||lc_vSql||' ');
--insert into debug (message) values (lc_vSql);
--htp.p('the sql = '||lc_vSql);
 lc_vReturn := lc_vSql;
 return lc_vReturn;

exception   
 when spiders_error then
     execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' SPIDERS error ' || lc_vErrorDescription || ''')';
     return json('{"error":"' || lc_vErrorDescription || '"}').to_char();
 when json_error then
     execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' JSON error'')';
     return json('{"error":"json"}').to_char();
 when others then
     execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' dml error: ' || sqlerrm || ''')';
     return json('{"error":"' || sqlerrm || '"}').to_char();
end; 

function f_getInnerForSelectRecordCount (in_vGroupBy varchar2) return varchar2 as
 /* template variables */
 lc_vFunctionName varchar2(255) := 'f_getInnerForSelectRecordCount';

 json_error EXCEPTION;
 spiders_error EXCEPTION;
 lc_vErrorDescription varchar2(255) := 'default';

 lc_vReturn varchar2(32000) := '{"error":"default"}'; --defaul the return to e for error
 lc_nContinue number :=0; --default to 0 to dissalow continuing


 /* function specific variables */
 lc_vSql long;
begin

 lc_vSql := replace(in_vGroupBy,'group by','select ');
 lc_vReturn := lc_vSql;
 return lc_vReturn;

exception   
 when spiders_error then
     execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' SPIDERS error ' || lc_vErrorDescription || ''')';
     return '{"error":"' || lc_vErrorDescription || '"}';
 when json_error then
     execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' JSON error'')';
     return '{"error":"json"}';
 when others then
     execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' dml error: ' || sqlerrm || ''')';
     return '{"error":"' || sqlerrm || '"}';
end; 


function f_getRecordCount (in_vWhere varchar2, in_vOrderBy varchar2, in_vStart varchar2, in_nRecordsPerPage number, in_vModule varchar2, in_vTarget varchar2) return json as
 /* template variables */
 lc_vFunctionName varchar2(255) := 'f_getRecordCount';

 json_error EXCEPTION;
 spiders_error EXCEPTION;
 lc_vErrorDescription varchar2(255) := 'default';

 lc_vReturn varchar2(32000) := '{"error":"default"}'; --defaul the return to e for error
 lc_nContinue number :=0; --default to 0 to dissalow continuing


 /* function specific variables */
 lc_vSql long;
 lc_vWhere long;
 lc_jJson json;
 lc_vInnerSelect varchar2(2000);
begin

 for rec in (
   select
     *
   from
     spd_t_query_defs
   where
     rest_endpoint = 'READDATATABLECUSTOM'
     and module = in_vModule
     and target = in_vTarget
 ) loop


   lc_vInnerSelect := f_getInnerForSelectRecordCount(rec.group_by_text);
   lc_nContinue := f_test_for_error(lc_vInnerSelect);

   if lc_nContinue = 1 then
     lc_vSql := 'SELECT  
         count(*) recordCount
       FROM    
         (
           SELECT  
             t.*, 
             ROWNUM AS rn
           FROM
             (' || rec.outer_select_text || ' ' || lc_vInnerSelect || ' ' || rec.from_text || ' ' || pkg_query_collection_custom.f_getFilterWhereClause || ' ' || rec.group_by_text || 
             ') t
         )';
     lc_nContinue := f_test_for_error(lc_vSql); --!!may need to overload the function to make a test for error json
   else
     lc_vErrorDescription := 'something went wrong f_getInnerForSelectRecordCount the whole sql statement';
     raise spiders_error;
   end if;

   lc_nContinue :=1;
 end loop;

 --htp.p(lc_vSql || '<br/>');

 lc_jJson := json_dyn.executeObject(lc_vSql);
 return lc_jJson;

exception   
 when spiders_error then
     execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' SPIDERS error ' || lc_vErrorDescription || ''')';
     return json('{"error":"' || lc_vErrorDescription || '"}');
 when json_error then
     execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' JSON error'')';
     return json('{"error":"json"}');
 when others then
     execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' dml error: ' || sqlerrm || ''')';
     return json('{"error":"' || sqlerrm || '"}');
end; 

function f_parseWhere(in_jJson json) return varchar2 as
/* template variables */
 lc_vFunctionName varchar2(255) := 'f_parseWhere';

 json_error EXCEPTION;
 spiders_error EXCEPTION;
 lc_vErrorDescription varchar2(255) := 'default';

 lc_vReturn varchar2(32000) := '{"error":"default"}'; --defaul the return to e for error
 lc_nContinue number :=0; --default to 0 to dissalow continuing


 /* function specific variables */
 lc_jlWhereClause json_list;
 lc_jlWhereItem json_list;
 lc_vColumnName varchar2(255);
 lc_vWhereValue varchar2(255);
 lc_vWhereClauseSegment long;
begin

 if(in_jJson.exist('whereClause')) then
   if(in_jJson.get('whereClause').is_array) then
     lc_jlWhereClause := json_list(in_jJson.get('whereClause'));
     for rec in 1..lc_jlWhereClause.count loop

       lc_jlWhereItem := json_list(lc_jlWhereClause.get(rec));
       lc_vColumnName := getVal(lc_jlWhereItem.get(1));
       lc_vWhereValue := getVal(lc_jlWhereItem.get(2));

       if rec = 1 then
         lc_vWhereClauseSegment := '(' || lc_vColumnName || '=''' || lc_vWhereValue  || '''';
       else
         lc_vWhereClauseSegment := lc_vWhereClauseSegment || ' and ' || lc_vColumnName || '=''' || lc_vWhereValue  || '''';
       end if;

       if rec = lc_jlWhereClause.count then
         lc_vWhereClauseSegment := lc_vWhereClauseSegment || ')';
       end if;
     end loop;
   else
     lc_vErrorDescription := 'whereClause is not an array';
     lc_vWhereClauseSegment := '';
     --raise spiders_error;
   end if;
 else
   lc_vErrorDescription := 'whereClause does not exist';
   lc_vWhereClauseSegment := '';
   --raise spiders_error;
 end if;



 lc_vReturn := lc_vWhereClauseSegment;
 return lc_vReturn;

exception   
 when spiders_error then
     execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' SPIDERS error ' || lc_vErrorDescription || ''')';
     return '{"error":"' || lc_vErrorDescription || '"}';
 when json_error then
     execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' JSON error'')';
     return '{"error":"json"}';
 when others then
     execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' dml error: ' || sqlerrm || ''')';
     return '{"error":"' || sqlerrm || '"}';
end; 


function getAttributeValue(in_vAttribute varchar2, in_jJSON json) return varchar2 as 
 /* template variables */
 json_error EXCEPTION;
 spiders_error EXCEPTION;
 lc_vErrorDescription varchar2(255) := 'default';
 lc_vFunctionName varchar2(255) := 'getAttributeValue';
 lc_vReturn varchar2(32000) := '{"error":"default"}'; --defaul the return to e for error
 lc_nContinue number :=0; --default to 0 to dissalow continuing

 /* function specific variables */
 lc_jvValue json_value;
begin

 if(in_jJSON.exist(in_vAttribute)) then       
   lc_jvValue := in_jJSON.get(in_vAttribute);
   lc_vReturn := getVal(lc_jvValue);
 else
   lc_vErrorDescription := in_vAttribute || ' attribute not found in json';
   raise spiders_error;
 end if;

 /* get name of object */
 return lc_vReturn;

exception     
 when spiders_error then
     execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' SPIDERS error ' || lc_vErrorDescription || ''')';
     return '{"error":"' || lc_vErrorDescription || '"}';
 when json_error then
     execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' JSON error'')';
     return '{"error":"json"}';
 when others then
     execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' dml error: ' || sqlerrm || ''')';
     return '{"error":"' || sqlerrm || '"}';
end; 

function f_read(in_cJson clob) return json as
 /* template variables */
 json_error EXCEPTION;
 spiders_error EXCEPTION;
 lc_vErrorDescription varchar2(255) := 'default';
 lc_vFunctionName varchar2(255) := 'f_read';
 lc_jReturn json; --defaul the return to e for error
 lc_nContinue number :=0; --default to 0 to dissalow continuing


 /* function specific variables */
 lc_vSql long;
 lc_jJson json;
 lc_vWhere long;
 lc_vSearch long := '';
 lc_nStart number :=1;
 lc_vGroupWhere long :='';
 lc_nRecordsPerPage number := 10;
 lc_vOrderBy long :='';
 lc_vDynamicWhere long := '';
 lc_jGroupFilter json;
 lc_jDataTable json;
 lc_jRecordCount json;
 lc_vWhereClause long;
 lc_vModule varchar2(255);
 lc_vTarget varchar2(255);
 lc_jParameterJson json;

begin
 lc_jReturn := json('{"error":"default"}');
 lc_jGroupFilter := json();
 lc_jDataTable := json();

 lc_vSearch :='';
 lc_nStart :=1;
 lc_nRecordsPerPage :=10;
 lc_vOrderBy :=' REPORTID asc '; --need a default sort by

 lc_jParameterJson := json(in_cJson);

 lc_vModule := getAttributeValue('module', lc_jParameterJson);
 lc_nContinue := f_test_for_error(lc_vModule);

 if lc_nContinue = 1 then
   lc_vTarget := getAttributeValue('target', lc_jParameterJson);
   lc_nContinue := f_test_for_error(lc_vTarget);
 else
   lc_vErrorDescription := 'Module attribute not found';
   raise spiders_error;
 end if;

/*
 if lc_nContinue = 1 then
   lc_vSearch := f_parseAoDataSearch (in_jJson);
   lc_nContinue := f_test_for_error(lc_vSearch);
   if length(lc_vSearch) is not null then
     if lc_nContinue = 1 then
       lc_vDynamicWhere := f_getDynamicWhere (lc_vSearch, in_vTableName);
       lc_nContinue := f_test_for_error(lc_vDynamicWhere);
     else
       lc_vErrorDescription := 'something went wrong in aoData search parsing';
       raise spiders_error;
     end if;
   end if;
 else
   lc_vErrorDescription := 'something went wrong in group filters parsing';
   raise spiders_error;
 end if;


 if lc_nContinue = 1 then
   lc_vWhereClause := f_parseWhere (in_jJson);
   lc_nContinue := f_test_for_error(lc_vWhereClause);
 else
   lc_vErrorDescription := 'something went wrong in group filters parsing';
   raise spiders_error;
 end if;
*/


 if lc_nContinue = 1 then
   lc_nStart := f_parseAoDataStart (lc_jParameterJson);
   lc_nContinue := f_test_for_error(lc_vSearch);
 else
   lc_vErrorDescription := 'something went wrong in whereClause parsing';
   raise spiders_error;
 end if;

 if lc_nContinue = 1 then
   lc_nRecordsPerPage := f_parseAoDataRecPerPage (lc_jParameterJson);
   lc_nContinue := f_test_for_error(lc_nRecordsPerPage);
 else
   lc_vErrorDescription := 'something went wrong in aoData start parsing';
   raise spiders_error;
 end if;

 if lc_nContinue = 1 then
   lc_vOrderBy := f_parseAoDataOrderBy (lc_jParameterJson);
   lc_nContinue := f_test_for_error(lc_vOrderBy);
 else
   lc_vErrorDescription := 'something went wrong in aoData records per page parsing';
   raise spiders_error;
 end if;

 if lc_nContinue = 1 then
   lc_vWhere := f_buildFullWhere (lc_vWhereClause, lc_vDynamicWhere, lc_vGroupWhere);
   lc_nContinue := f_test_for_error(lc_vWhere);
 else
   lc_vErrorDescription := 'something went wrong getting the dynamic where';
   raise spiders_error;
 end if;
 p_log(lc_vWhere);

 if lc_nContinue = 1 then
   lc_jGroupFilter := json(); -- ADD THIS IN A MINUTE f_buildGroupFilterSql (in_vTableName, lc_vWhere, in_vModule);
   --lc_nContinue := f_test_for_error(lc_jGroupFilter); !!may need to overload the function to make a test for error json
 else
   lc_vErrorDescription := 'something went wrong getting the dynamic where';
   raise spiders_error;
 end if;

 if lc_nContinue = 1 then
   --htp.p(' pkg_data_table_custom.f_read-lc_vOrderBy, lc_nStart, lc_nRecordsPerPage, lc_vModule, lc_vTarget: '||lc_vOrderBy||', '||lc_nStart||', '||lc_nRecordsPerPage||', '||lc_vModule||', '||lc_vTarget);
   insert into debug (message) values (' pkg_data_table_custom.f_read-lc_vOrderBy, lc_nStart, lc_nRecordsPerPage, lc_vModule, lc_vTarget: '||lc_vOrderBy||', '||lc_nStart||', '||lc_nRecordsPerPage||', '||lc_vModule||', '||lc_vTarget);
   lc_jDataTable := f_buildFullSql (lc_vOrderBy, lc_nStart, lc_nRecordsPerPage, lc_vModule, lc_vTarget);
   --lc_nContinue := f_test_for_error(lc_jDataTable); !!may need to overload the function to make a test for error json
 else
   lc_vErrorDescription := 'something went wrong getting the group filter sql where';
   raise spiders_error;
 end if;

 if lc_nContinue = 1 then
   lc_jRecordCount := f_getRecordCount (lc_vWhere, lc_vOrderBy, lc_nStart, lc_nRecordsPerPage, lc_vModule, lc_vTarget);
   --lc_nContinue := f_test_for_error(lc_nRecordCount); --!!may need to overload the function to make a test for error json
 else
   lc_vErrorDescription := 'something went wrong building the whole sql statement';
   raise spiders_error;
 end if;

 if lc_nContinue = 1 then
   lc_jReturn := json();
   lc_jReturn.put('groupFilter',lc_jGroupFilter);
   lc_jReturn.put('recordCount',lc_jRecordCount);
   lc_jReturn.put('dataTable',lc_jDataTable);

 else
   lc_vErrorDescription := 'something went wrong getting the record count';
   raise spiders_error;
 end if;

 return lc_jReturn;

exception   
 when spiders_error then
     execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' SPIDERS error ' || lc_vErrorDescription || ''')';
     return json('{"error":"' || lc_vErrorDescription || '"}');
 when json_error then
     execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' JSON error'')';
     return json('{"error":"json"}');
 when others then
     execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' dml error: ' || sqlerrm || ''')';
     return json('{"error":"' || sqlerrm || '"}');
end; 

function f_makeSpreadsheetFile (in_vSql long, in_vFolder varchar2, in_vDatatableName varchar2, in_vAO_DATA varchar2) return json as
  /* template variables */
  lc_vFunctionName varchar2(255) := 'f_makeSpreadsheetFile';

  json_error EXCEPTION;
  spiders_error EXCEPTION;
  lc_vErrorDescription varchar2(255) := 'default';

  lc_vReturn varchar2(32000) := '{"error":"default"}'; --defaul the return to e for error
  lc_nContinue number :=0; --default to 0 to dissalow continuing

  
  /* function specific variables */
  lc_vFolderid varchar2(32000) := 'SPD_1234';
  lc_nSequence number;
  lc_vFilename varchar2(32000);
  lc_vSql long;
  lc_jlDML json_list;
  lc_jResults json;
  lc_vDMLResult varchar2(255);
  lc_jMessages json;
  
  
begin

lc_jlDML := json_list();
lc_jResults := json();
lc_jMessages := json();     
      
lc_vFilename := in_vDatatableName || '_' || v('APP_USER') || '_' || to_char(sysdate,'YYYYDDMM_HH24MI') || '.xlsx';

as_xlsx.query2sheet(in_vSql);
as_xlsx.save(in_vFolder,lc_vFilename);
      
--Add the file to the table
select spd_s_dt_downloads.nextval into lc_nSequence from dual;
--Insert record into table
lc_vSql := '
  insert into 
    spd_t_dt_downloads (
      DTDOWNLOADID,
      DATATABLENAME,
      AODATA,
      the_bfile
    )
  
    values (
      ''' || lc_nSequence || ''',
      ''' || in_vDatatableName || ''',
      ''' || in_vAO_DATA || ''',
      BFILENAME(''' || lc_vFolderid || ''',''' || lc_vFILENAME || ''')
    )
';

lc_jlDML.append(lc_vSql);
lc_jResults.put('results',lc_jlDML);
lc_vDMLResult := BB_F_DML(lc_jResults, lc_jMessages);
lc_jMessages.put('id',lc_nSequence);


  
  return lc_jMessages;

exception   
  when spiders_error then
      execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' SPIDERS error ' || lc_vErrorDescription || ''')';
      return json('{"error":"' || lc_vErrorDescription || '"}');
  when json_error then
      execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' JSON error'')';
      return json('{"error":"json"}');
  when others then
      execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' dml error: ' || sqlerrm || ''')';
      return json('{"error":"' || sqlerrm || '"}');
end; 


function f_download(in_cJson clob) return json as
 /* template variables */
 json_error EXCEPTION;
 spiders_error EXCEPTION;
 lc_vErrorDescription varchar2(255) := 'default';
 lc_vFunctionName varchar2(255) := 'f_download';
 lc_jReturn json; --defaul the return to e for error
 lc_nContinue number :=0; --default to 0 to dissalow continuing


 /* function specific variables */
 lc_vSql long;
 lc_jJson json;
 lc_vWhere long;
 lc_vSearch long := '';
 lc_nStart number :=1;
 lc_vGroupWhere long :='';
 lc_nRecordsPerPage number := 10;
 lc_vOrderBy long :='';
 lc_vDynamicWhere long := '';
 lc_jGroupFilter json;
 lc_jDataTable json;
 lc_jRecordCount json;
 lc_vWhereClause long;
 lc_vModule varchar2(255);
 lc_vTarget varchar2(255);
 lc_jParameterJson json;

begin
 lc_jReturn := json('{"error":"default"}');
 lc_jGroupFilter := json();
 lc_jDataTable := json();

 lc_vSearch :='';
 lc_nStart :=1;
 lc_nRecordsPerPage :=10;
 lc_vOrderBy :='1';-- REPORTID asc '; --need a default sort by

 lc_jParameterJson := json(in_cJson);

 lc_vModule := getAttributeValue('module', lc_jParameterJson);
 lc_nContinue := f_test_for_error(lc_vModule);

 if lc_nContinue = 1 then
   lc_vTarget := getAttributeValue('target', lc_jParameterJson);
   lc_nContinue := f_test_for_error(lc_vTarget);
 else
   lc_vErrorDescription := 'Module attribute not found';
   raise spiders_error;
 end if; 
 
/* without checking lc_nContinue, determine whether or not there is a where clause in json */
 
/*
 if lc_nContinue = 1 then
   lc_vSearch := f_parseAoDataSearch (in_jJson);
   lc_nContinue := f_test_for_error(lc_vSearch);
   if length(lc_vSearch) is not null then
     if lc_nContinue = 1 then
       lc_vDynamicWhere := f_getDynamicWhere (lc_vSearch, in_vTableName);
       lc_nContinue := f_test_for_error(lc_vDynamicWhere);
     else
       lc_vErrorDescription := 'something went wrong in aoData search parsing';
       raise spiders_error;
     end if;
   end if;
 else
   lc_vErrorDescription := 'something went wrong in group filters parsing';
   raise spiders_error;
 end if;


 if lc_nContinue = 1 then
   lc_vWhereClause := f_parseWhere (in_jJson);
   lc_nContinue := f_test_for_error(lc_vWhereClause);
 else
   lc_vErrorDescription := 'something went wrong in group filters parsing';
   raise spiders_error;
 end if;
*/

/* if there is a where clause (if lc_vWhere = 'y') then parse it out using above f_parseWhere */


 if lc_nContinue = 1 then
   lc_vOrderBy := f_parseAoDataOrderBy (lc_jParameterJson);
   lc_nContinue := f_test_for_error(lc_vOrderBy);
 else
   lc_vErrorDescription := 'something went wrong in aoData records per page parsing';
   raise spiders_error;
 end if;

 if lc_nContinue = 1 then
   lc_vWhere := f_buildFullWhere (lc_vWhereClause, lc_vDynamicWhere, lc_vGroupWhere);
   lc_nContinue := f_test_for_error(lc_vWhere);
 else
   lc_vErrorDescription := 'something went wrong getting the dynamic where';
   raise spiders_error;
 end if;
 p_log(lc_vWhere);

 if lc_nContinue = 1 then
   lc_jGroupFilter := json(); -- ADD THIS IN A MINUTE f_buildGroupFilterSql (in_vTableName, lc_vWhere, in_vModule);
   --lc_nContinue := f_test_for_error(lc_jGroupFilter); !!may need to overload the function to make a test for error json
 else
   lc_vErrorDescription := 'something went wrong getting the dynamic where';
   raise spiders_error;
 end if;

 if lc_nContinue = 1 then
   --htp.p(' pkg_data_table_custom.f_read-lc_vOrderBy, lc_nStart, lc_nRecordsPerPage, lc_vModule, lc_vTarget: '||lc_vOrderBy||', '||lc_nStart||', '||lc_nRecordsPerPage||', '||lc_vModule||', '||lc_vTarget);
   insert into debug (message) values (' pkg_data_table_custom.f_download-lc_vOrderBy, lc_nStart, lc_nRecordsPerPage, lc_vModule, lc_vTarget: '||lc_vOrderBy||', '||lc_nStart||', '||lc_nRecordsPerPage||', '||lc_vModule||', '||lc_vTarget);
   lc_vSql := f_buildFullSqlString (lc_vOrderBy, lc_nStart, lc_nRecordsPerPage, lc_vModule, lc_vTarget);
   --lc_nContinue := f_test_for_error(lc_jDataTable); !!may need to overload the function to make a test for error json
 else
   lc_vErrorDescription := 'something went wrong getting the group filter sql where';
   raise spiders_error;
 end if;

 if lc_nContinue = 1 then
   lc_jRecordCount := f_getRecordCount (lc_vWhere, lc_vOrderBy, lc_nStart, lc_nRecordsPerPage, lc_vModule, lc_vTarget);
   --lc_nContinue := f_test_for_error(lc_nRecordCount); --!!may need to overload the function to make a test for error json
 else
   lc_vErrorDescription := 'something went wrong building the whole sql statement';
   raise spiders_error;
 end if;

 if lc_nContinue = 1 then
   lc_jReturn := json();
   lc_jReturn.put('groupFilter',lc_jGroupFilter);
   lc_jReturn.put('recordCount',lc_jRecordCount);
   lc_jReturn.put('dataTable',lc_jDataTable);
   
  --call the xlsx function
  lc_jReturn := f_makeSpreadsheetFile (lc_vSql, 'SPD_1234', lc_vTarget, lc_vSearch);


 else
   lc_vErrorDescription := 'something went wrong getting the record count';
   raise spiders_error;
 end if;

 return lc_jReturn;

exception   
 when spiders_error then
     execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' SPIDERS error ' || lc_vErrorDescription || ''')';
     return json('{"error":"' || lc_vErrorDescription || '"}');
 when json_error then
     execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' JSON error'')';
     return json('{"error":"json"}');
 when others then
     execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' dml error: ' || sqlerrm || ''')';
     return json('{"error":"' || sqlerrm || '"}');
end; 

end;



/*template function
function f_read(in_jJson json, in_vTableName varchar2) return varchar2 as
 /* template variables * /
 lc_vFunctionName varchar2(255) := 'f_read';

 json_error EXCEPTION;
 spiders_error EXCEPTION;
 lc_vErrorDescription varchar2(255) := 'default';

 lc_vReturn varchar2(32000) := '{"error":"default"}'; --defaul the return to e for error
 lc_nContinue number :=0; --default to 0 to dissalow continuing


 /* function specific variables * /


begin

 if lc_nContinue = 1 then
   lc_vPrimaryKey := getPrimaryKeyColumn (in_jTable);
   lc_nContinue := f_test_for_error(lc_vPrimaryKey);
 else
   lc_vErrorDescription := 'name attribute not found in json';
   raise spiders_error;
 end if;

 return lc_vReturn;

exception   
 when spiders_error then
     execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' SPIDERS error ' || lc_vErrorDescription || ''')';
     return '{"error":"' || lc_vErrorDescription || '"}';
 when json_error then
     execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' JSON error'')';
     return '{"error":"json"}';
 when others then
     execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' dml error: ' || sqlerrm || ''')';
     return '{"error":"' || sqlerrm || '"}';
end; 
*/

/
