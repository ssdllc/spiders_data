--------------------------------------------------------
--  DDL for Package Body BB_REST
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE PACKAGE BODY "SPIDERS_DATA"."BB_REST" AS

procedure create_(in_cJSON clob) AS

    obj json;
    lc_target json_value;
    lc_module json_value;
    lc_json json;
    isValid varchar2(1);
    lc_jResults json;
    lc_jMessages json;
    lc_jlMessages json_list;
    lc_vDMLResult varchar2(1);
    isAuthorized varchar2(1);
    lc_vDebug varchar2(2000);

    --error stuff
    lc_jError json;
    json_error EXCEPTION;
begin
    obj := json(in_cJSON);
    lc_jMessages := json();
    lc_jlMessages := json_list();

    --Check if module exits, do nothing if not
    if(obj.exist('module')) THEN


        --Check if target exist, do nothing if not
        if(obj.exist('target')) THEN
            --RUN APPROPRIATE CREATE FUNCTION BASED ON module and target
            lc_module := obj.get('module');
            lc_target := obj.get('target');

            isAuthorized := BB_F_AUTHORIZATION(in_vRestEndpoint=>'BB_REST.create_', in_vModule => lc_module.get_string);
            if(isAuthorized='y') THEN--RUN APPROPRIATE CREATE FUNCTION BASED ON target

                CASE lc_module.get_string
                    WHEN 'userRoleManager'
                      THEN CASE lc_target.get_string
                                WHEN 'userRoles' THEN
                                    --step 1
                                    --validate json object from client
                                    isValid := bb_f_val_create_user_roles(obj, lc_jResults, lc_jMessages);
                                    --step 2
                                    --if json object is valid, run dml as a result;
                                    if isValid = 'y' then
                                        lc_vDMLResult := BB_F_DML(lc_jResults, lc_jMessages);
                                        lc_json := lc_jMessages;
                                    else
                                    -- json object is not valid, return errors only from lc_jMessages
                                        lc_json := lc_jMessages;
                                    end if;
                                ELSE
                                    lc_jlMessages.append(json_list('[{error:''Error C.0005 There has been an error processing the request''}]'));
                            END CASE;

                    /* THIS IS WHERE YOU WOULD ADD CHECKS FOR OTHER MODULES */

                    WHEN 'reportLibrary'

                      THEN

                      CASE lc_target.get_string
                            WHEN 'createReportLibraryRecord' THEN
                                isValid := BB_F_CU_REPORT_LIBRARY_REC(obj, lc_jMessages);
                                lc_json := lc_jMessages;
                            WHEN 'createReportLibraryFile' THEN
                                isValid := BB_F_C_REPORT_LIBRARY_FILE(obj, lc_jMessages);
                                lc_json := lc_jMessages;
                            WHEN 'createReportFileKeyword' THEN
                                isValid := BB_F_C_REPORT_LIBRARY_KEYWORD(obj, lc_jMessages);
                                lc_json := lc_jMessages;
                            ELSE
                                lc_jlMessages.append(json_list('[{error:''Error C.0006 There has been an error processing the request''}]'));
                      END CASE;


                    WHEN 'customShapes'
                      THEN CASE lc_target.get_string
                           WHEN 'createCustomShape' THEN
                                isValid := BB_F_CU_CUSTOM_SHAPE(obj, lc_jMessages);
                                lc_json := lc_jMessages;
                            WHEN 'finishCSVImport' THEN
                                isValid := BB_F_FINISH_CS_CSV_IMPORT(obj, lc_jMessages);
                                lc_json := lc_jMessages;
                            ELSE
                                lc_jlMessages.append(json_list('[{error:''Error C.0008 There has been an error processing the request''}]'));
                            END CASE;


                ELSE
                    lc_jlMessages.append(json_list('[{error:''Error C.0007 There has been an error processing the request''}]'));
                END CASE;
            else
                lc_jlMessages.append(json_list('[{error:''Error C.0003 There has been an error processing the request''}]'));
            end if;
        else
            lc_jlMessages.append(json_list('[{error:''Error C.0002 There has been an error processing the request''}]'));
        end if;

    else
        lc_jlMessages.append(json_list('[{error:''Error C.0004 There has been an error processing the request''}]'));
    end if;

    if isValid != 'e' then --if a child function failed, it handled its own exception
      if (lc_jlMessages.count > 0) then
          lc_jMessages.put('status','error');
          lc_jMessages.put('messages',lc_jlMessages);
          dbms_output.put_line('1');
          raise json_error;
      else
          lc_json.htp;
      end if;
    end if;

  exception
    when json_error then
        dbms_output.put_line('3');
        lc_jError := json();
        lc_jError.put('function','BB_REST.CREATE_');
        lc_jError.put('input',in_cJSON);
        lc_jError.put('messages',lc_jlMessages);
        BB_P_ERROR(lc_jError);

    when others then
        dbms_output.put_line('4');
        lc_jError := json();
        lc_jlMessages := json_list();
        lc_jError.put('function','BB_REST.CREATE_');
        lc_jError.put('input',in_cJSON);
        lc_jlMessages.append(json_list('[{error:''C.000X ' || replace(replace(SQLERRM,chr(10),''),chr(13),'') || '''}]'));
        lc_jError.put('messages',lc_jlMessages);
        BB_P_ERROR(lc_jError);
END create_;


procedure update_(in_cJSON clob) AS

    obj json;
    lc_target json_value;
    lc_module json_value;
    lc_json json;
    isValid varchar2(1);
    lc_jResults json;
    lc_jMessages json;
    lc_jlMessages json_list;
    lc_vDMLResult varchar2(1);
    isAuthorized varchar2(1);
    lc_vJPCallback varchar2(25);
    lc_vDebug varchar2(2000);

    --error stuff
    lc_jError json;
    json_error EXCEPTION;

begin
    obj := json(in_cJSON);
    lc_jMessages := json();
    lc_jlMessages := json_list();

    --Check if module exits, do nothing if not
    if(obj.exist('module')) THEN

        --Check if target exist, do nothing if not
        if(obj.exist('target')) THEN

            --RUN APPROPRIATE CREATE FUNCTION BASED ON module and target
            lc_module := obj.get('module');
            lc_target := obj.get('target');


            isAuthorized := BB_F_AUTHORIZATION(in_vRestEndpoint=>'BB_REST.update_', in_vModule => lc_module.get_string);

isAuthorized := 'y'; /*toggle comment for testing in sqldeveloper...*/
            if(isAuthorized='y') THEN--RUN APPROPRIATE CREATE FUNCTION BASED ON target

                CASE lc_module.get_string

                    WHEN 'userRoleManager' THEN
                        CASE lc_target.get_string
                            WHEN 'userRoles' THEN
                                --step 1
                                --validate json object from client
                                isValid := bb_f_val_update_user_roles(obj, lc_jResults, lc_jMessages);
                                --step 2
                                --if json object is valid, run dml as a result;
                                if isValid = 'y' then
                                  --htp.p('running dml');
                                    lc_vDMLResult := BB_F_DML(lc_jResults, lc_jMessages);

                                    lc_json := lc_jMessages;
                                else
                                -- json object is not valid, return errors only from lc_jMessages
                                    lc_json := lc_jMessages;

                                end if;
                            ELSE
                                lc_jlMessages.append(json_list('[{error:''Error U.0001 There has been an error processing the request''}]'));
                        END CASE;

                    /* THIS IS WHERE YOU WOULD ADD CHECKS FOR OTHER MODULES */
                    WHEN 'xmlImportManager' THEN

                        CASE lc_target.get_string
                            WHEN 'matchFacilities' THEN

                                isValid := BB_F_UPDATE_XML_FACILITIES(obj, lc_jResults, lc_jMessages);
                                --htp.p('BB_F_UPDATE_XML_FACILITIES:' || isValid);
                                --if json object is valid, run dml as a result;
                                if isValid = 'y' then
                                    lc_vDMLResult := BB_F_DML(lc_jResults, lc_jMessages);
                                    --Run the check for completed facilities
                                    BB_P_UPDATE_XML_FAC_COMPLETE(obj);
                                    lc_json := lc_jMessages;
                                else
                                -- json object is not valid, return errors only from lc_jMessages
                                    lc_json := lc_jMessages;
                                end if;
                            ELSE
                                lc_jlMessages.append(json_list('[{error:''Error U.0005 There has been an error processing the request''}]'));
                        END CASE;
                      WHEN 'reportLibrary'
                        THEN CASE lc_target.get_string
                              WHEN 'updateReportLibraryRecord' THEN
                                  isValid := BB_F_CU_REPORT_LIBRARY_REC(obj, lc_jMessages);
                                  lc_json := lc_jMessages;
                              WHEN 'updateReportLibraryFile' THEN
                                  isValid := BB_F_U_REPORT_LIBRARY_FILE(obj, lc_jMessages);
                                  lc_json := lc_jMessages;
                              ELSE
                                  lc_jlMessages.append(json_list('[{error:''Error U.0007 There has been an error processing the request''}]'));
                        END CASE;
                      WHEN 'customShapes'
                        THEN CASE lc_target.get_string
                           WHEN 'updateCustomShape' THEN
                                isValid := BB_F_CU_CUSTOM_SHAPE(obj, lc_jMessages);
                                lc_json := lc_jMessages;
                            ELSE
                                lc_jlMessages.append(json_list('[{error:''Error U.0008 There has been an error processing the request''}]'));
                            END CASE;
                    ELSE
                        lc_jlMessages.append(json_list('[{error:''Error U.0006 There has been an error processing the request''}]'));
                END CASE;
            else
                lc_jlMessages.append(json_list('[{error:''Error U.0004 There has been an error processing the request''}]'));
            end if;
        else
            isValid := 'n';
            lc_jlMessages.append(json_list('[{error:''Error U.0002 There has been an error processing the request''}]'));
        end if;
    else
        isValid := 'n';
        lc_jlMessages.append(json_list('[{error:''Error U.0003 There has been an error processing the request''}]'));
    end if;

    if isValid != 'e' then --if a child function failed, it handled its own exception
      if (lc_jlMessages.count > 0) then
          lc_jMessages.put('status','error');
          lc_jMessages.put('messages',lc_jlMessages);
          raise json_error;
      else
          lc_json.htp;
      end if;
    end if;
  exception
    when json_error then
        lc_jError := json();
        lc_jError.put('function','BB_REST.update_');
        lc_jError.put('input',in_cJSON);
        lc_jError.put('messages',lc_jlMessages);
        BB_P_ERROR(lc_jError);

    when others then
        --htp.p('OTHERS ERROR ON BBE_REST'); --this fires after the the error handler fires in the update function
        lc_jError := json();
        lc_jError.put('function','BB_REST.update_');
        lc_jError.put('input',in_cJSON);
        lc_jlMessages.append(json_list('[{error:''U.000X ' || SQLERRM || '''}]'));
        lc_jError.put('messages',lc_jlMessages);
        BB_P_ERROR(lc_jError);
END update_;

procedure delete_(in_cJSON clob) AS

    obj json;
    lc_target json_value;
    lc_module json_value;
    lc_json json;
    isValid varchar2(1);
    lc_jResults json;
    lc_jMessages json;
    lc_jlMessages json_list;
    lc_vDMLResult varchar2(1);
    isAuthorized varchar2(1);
    lc_vJPCallback varchar2(25);
    lc_vDebug varchar2(2000);

    --error stuff
    lc_jError json;
    json_error EXCEPTION;
begin
    obj := json(in_cJSON);
    lc_jMessages := json();
    lc_jlMessages := json_list();

    --Check if module exits, do nothing if not
    if(obj.exist('module')) THEN


        --Check if target exist, do nothing if not
        if(obj.exist('target')) THEN
            --RUN APPROPRIATE CREATE FUNCTION BASED ON module and target
            lc_module := obj.get('module');
            lc_target := obj.get('target');

            isAuthorized := BB_F_AUTHORIZATION(in_vRestEndpoint=>'BB_REST.delete_', in_vModule => lc_module.get_string);
            if(isAuthorized='y') THEN--RUN APPROPRIATE CREATE FUNCTION BASED ON target

                CASE lc_module.get_string
                    WHEN 'userRoleManager' THEN
                        CASE lc_target.get_string
                            WHEN 'userRoles' THEN
                                isValid := bb_f_val_delete_user_roles(obj, lc_jResults, lc_jMessages);

                                --step 2
                                --if json object is valid, run dml as a result;
                                if isValid = 'y' then
                                    lc_vDMLResult := BB_F_DML(lc_jResults, lc_jMessages);
                                    lc_json := lc_jMessages;
                                else
                                -- json object is not valid, return errors only from lc_jMessages
                                    lc_json := lc_jMessages;
                                end if;
                            ELSE
                                lc_jlMessages.append(json_list('[{error:''Error D.0001 There has been an error processing the request''}]'));
                        END CASE;
                    WHEN 'xmlImportManager' THEN
                        CASE lc_target.get_string
                            WHEN 'resetXMLImport' THEN
                                isValid := 'y';
                                lc_json := BB_F_XML_IMPORT_RESET(obj);
                            ELSE
                                lc_jlMessages.append(json_list('[{error:''Error D.0006 There has been an error processing the request''}]'));
                        END CASE;
                    WHEN 'reportLibrary' THEN
                        CASE lc_target.get_string
                            WHEN 'deleteReportLibraryKeyword' THEN
                                isValid := 'y';
                                lc_vDMLResult := BB_F_D_REPORT_LIBRARY_KEYWORD(obj,lc_jMessages);
                                lc_json := lc_jMessages;
                            WHEN 'deleteReportLibraryFile' THEN
                                isValid := 'y';
                                lc_vDMLResult := BB_F_D_REPORT_LIBRARY_FILE(obj,lc_jMessages);
                                lc_json := lc_jMessages;
                            WHEN 'deleteReportLibraryRecord' THEN
                                isValid := 'y';
                                lc_vDMLResult := BB_F_D_REPORT_LIBRARY_REC(obj,lc_jMessages);
                                lc_json := lc_jMessages;
                            ELSE
                                lc_jlMessages.append(json_list('[{error:''Error D.0006 There has been an error processing the request''}]'));
                        END CASE;

                     WHEN 'customShapes' THEN
                        CASE lc_target.get_string
                            WHEN 'deleteCustomShape' THEN
                                isValid := 'y';
                                lc_vDMLResult := BB_F_D_CUSTOM_SHAPE(obj,lc_jMessages);
                                lc_json := lc_jMessages;
                            ELSE
                                lc_jlMessages.append(json_list('[{error:''Error D.0006 There has been an error processing the request''}]'));
                        END CASE;

                      WHEN 'dryDock' THEN
                        CASE lc_target.get_string
                            WHEN 'deleteDryDockCondition' THEN
                                isValid := 'y';
                                lc_vDMLResult := BB_F_D_DD_CONDITION_DATA(obj,lc_jMessages);
                                lc_json := lc_jMessages;
                            ELSE
                                lc_jlMessages.append(json_list('[{error:''Error D.0007 There has been an error processing the request''}]'));
                        END CASE;
                    ELSE
                        lc_jlMessages.append(json_list('[{error:''Error D.0003 There has been an error processing the request''}]'));
                END CASE;
            else
                lc_jlMessages.append(json_list('[{error:''Error D.0004 There has been an error processing the request''}]'));
            end if;
        else
            lc_jlMessages.append(json_list('[{error:''Error D.0002 There has been an error processing the request''}]'));
        end if;
    else
        lc_jlMessages.append(json_list('[{error:''Error D.0005 There has been an error processing the request''}]'));
    end if;

    if isValid != 'e' then --if a child function failed, it handled its own exception
      if (lc_jlMessages.count > 0) then
          lc_jMessages.put('status','error');
          lc_jMessages.put('messages',lc_jlMessages);
          raise json_error;
      else
          lc_json.htp;
      end if;
    end if;

  exception
    when json_error then
        lc_jError := json();
        lc_jError.put('function','BB_REST.delete_');
        lc_jError.put('input',in_cJSON);
        lc_jError.put('messages',lc_jlMessages);
        BB_P_ERROR(lc_jError);

    when others then
        lc_jError := json();
        lc_jError.put('function','BB_REST.delete_');
        lc_jError.put('input',in_cJSON);
        lc_jlMessages.append(json_list('[{error:''D.000X ' || SQLERRM || '''}]'));
        lc_jError.put('messages',lc_jlMessages);
        BB_P_ERROR(lc_jError);
END delete_;

procedure read_(in_cJSON clob) AS

  obj json;
  lc_target json_value;
  lc_module json_value;
  lc_json json;
  isValid varchar2(1);
  lc_jResults json;
  lc_jMessages json;
  lc_jlMessages json_list;
  lc_vDMLResult varchar2(1);
  isAuthorized varchar2(1);
  lc_vDebug varchar2(2000);


    --error stuff
    lc_jError json;
    json_error EXCEPTION;
    test_err EXCEPTION;
begin

    --insert into debug (input) values ('111: ' || in_cJSON);
    obj := json(in_cJSON);
    lc_jMessages := json();
    lc_jlMessages := json_list();


    --insert into debug (input) values (json_printer.pretty_print(obj));
    --Check if module exits, do nothing if not
    if(obj.exist('module')) THEN

        --Check if target exist, do nothing if not
        if(obj.exist('target')) THEN
            --RUN APPROPRIATE CREATE FUNCTION BASED ON module and target
            lc_module := obj.get('module');
            --execute immediate 'insert into debug (input)'
            lc_target := obj.get('target');

            --insert into debug (input) values ('BB_REST.read_' || lc_module.get_string || ' : ' || lc_target.get_string || ' : ' || v('APP_USER'));
            isAuthorized := BB_F_AUTHORIZATION(in_vRestEndpoint=>'BB_REST.read_', in_vModule => lc_module.get_string);

isAuthorized := 'y'; /*toggle comment for testing in sqldeveloper...*/

            if(isAuthorized='y') THEN--RUN APPROPRIATE CREATE FUNCTION BASED ON target

                CASE lc_module.get_string
                    WHEN 'userRoleManager'
                      THEN CASE lc_target.get_string
                                WHEN 'userRoles' THEN
                                    --test valid json for sending to read

                                    --!!!TODO make validator function
                                    --isValid := bb_f_read_user_roles(obj);--, lc_jResults);

                                    --do the read if valid
                                    lc_json := bb_f_read_user_roles(obj);
                                WHEN 'users' THEN
                                    lc_json := bb_f_read_users(obj);
                                WHEN 'isAdmin' THEN
                                    lc_json := BB_F_READ_IS_ADMIN(obj);
                                ELSE
                                    lc_jlMessages.append(json_list('[{error:''Error R.0001 There has been an error processing the request''}]'));
                            END CASE;

                    /* THIS IS WHERE YOU WOULD ADD CHECKS FOR OTHER MODULES */

                    WHEN 'xmlImportManager'
                      THEN CASE lc_target.get_string
                            WHEN 'xmlFacilities' THEN
                                lc_json := bb_f_read_xml_facilities(obj);
                            WHEN 'spidersDeliveryOrderFacs' THEN
                                lc_json := BB_F_READ_SPIDERS_DO_FACS_XML(obj);
                            WHEN 'spidersDeliveryOrderWithXML' THEN
                                lc_json := BB_F_READ_DOS_WITH_XML_FILE(obj);
                            WHEN 'spidersDeliveryOrderImportLog' THEN
                                lc_json := BB_F_READ_DO_XML_IMPORT_LOG(obj);
                            WHEN 'spidersDeliveryOrderImportErrors' THEN
                                lc_json := BB_F_READ_DO_XML_IMPORT_ERRORS(obj);
                            WHEN 'spidersDeliveryOrderProcessingStatus' THEN
                                lc_json := BB_F_READ_DO_PROC_STATUS(obj);
                            WHEN 'spidersDeliveryOrderErrorStatus' THEN
                                lc_json := BB_F_READ_DO_PROC_ERRORS(obj);
                            WHEN 'spidersDeliveryOrderInformation' THEN
                                lc_json := BB_F_READ_DO_INFORMATION(obj);
                            WHEN 'spidersCopyFilesAndProcess' THEN
                                lc_json := BB_F_COPY_FILES_BEGIN_PROC(obj);
                            ELSE
                                lc_jlMessages.append(json_list('[{error:''Error R.0004 There has been an error processing the request''}]'));
                        END CASE;

                    WHEN 'testModule'
                      THEN CASE lc_target.get_string
                                WHEN 'xmlImportErrorsDataTable' THEN
                                    lc_json := BB_F_TEST_DO_XML_IMPORT_ERRORS(obj);
                                WHEN 'users' THEN
                                    lc_json := bb_f_read_users(obj);
                                WHEN 'isAdmin' THEN
                                    lc_json := BB_F_READ_IS_ADMIN(obj);
                                ELSE
                                    lc_jlMessages.append(json_list('[{error:''Error R.0007 There has been an error processing the request''}]'));
                            END CASE;

                    WHEN 'reportLibrary'
                      THEN CASE lc_target.get_string
                                WHEN 'sidebarGroupFilterProductLine' THEN
                                    lc_json := BB_F_READ_GF_PRODUCTLINE(obj);
                                WHEN 'sidebarGroupFilterYear' THEN
                                    lc_json := BB_F_READ_GF_YEAR(obj);
                                WHEN 'sidebarGroupFilterAuthor' THEN
                                    lc_json := BB_F_READ_GF_AUTHOR(obj);
                                WHEN 'sidebarGroupFilterContractor' THEN
                                    lc_json := BB_F_READ_GF_CONTRACTOR(obj);
                                WHEN 'reportLibrary' THEN
                                    lc_json := BB_F_READ_REPORT_LIBRARY(obj);
                                WHEN 'reportFiles' THEN
                                    lc_json := BB_F_R_REPORT_LIBRARY_FILES(obj);
                                WHEN 'reportLibraryActions' THEN
                                    lc_json := BB_F_R_REPORT_LIBRARY_ACTIONS(obj,lc_vDebug);
                                WHEN 'reportLibraryDropDownYears' THEN
                                    lc_json := BB_F_R_REPORT_LIBRARY_DD_YEARS(obj);
                                WHEN 'reportLibraryDropDownProductlines' THEN
                                    lc_json := BB_F_R_REPORT_LIBRARY_DD_PL(obj);
                                WHEN 'reportLibraryDropDownStates' THEN
                                    lc_json := BB_F_R_REPORT_LIBRARY_DD_STATE(obj);
                                WHEN 'reportLibraryRecord' THEN
                                    lc_json := BB_F_R_REPORT_LIBRARY_REC(obj);
                                WHEN 'reportLibraryKeywords' THEN
                                    lc_json := BB_F_R_REPORT_LIBRARY_KEYWORDS(obj);


                                ELSE
                                    lc_jlMessages.append(json_list('[{error:''Error R.0008 There has been an error processing the request''}]'));

                          END CASE;
                     WHEN 'customShapes'
                      THEN CASE lc_target.get_string
                                WHEN 'homeDatatable' THEN
                                    lc_json := BB_F_R_CUSTOM_SHAPES(obj);
                                WHEN 'importDatatable' THEN
                                    lc_json := BB_F_R_CUSTOM_SHAPES_TMP(obj);
                                WHEN 'sceneDescription' THEN
                                    lc_json := BB_F_R_CUSTOM_SHAPES_SCENE(obj);
                                ELSE
                                    lc_jlMessages.append(json_list('[{error:''Error R.0009 There has been an error processing the request''}]'));

                          END CASE;
                    WHEN 'dryDock'
                      THEN CASE lc_target.get_string
                                WHEN 'homeDatatable' THEN
                                    lc_json := BB_F_R_DRY_DOCK_FACS(obj);
                                WHEN 'csvDatatable' THEN
                                    lc_json := BB_F_R_DD_CSVDATATABLE(obj);
                                WHEN 'importCSVDatatable' THEN

                                    --insert into debug (functionname, input, message) values ('BB_REST_BODY','[SPD_BUG_FIX][03] Dry Dock Upload','importCSVDatatable');

                                    --insert into debug (input) values ('importCSVDatatable');
                                    lc_json := BB_F_R_DD_CSV_IMPORT_TMP(obj);
                                WHEN 'dryDockConditionData' THEN
                                    lc_json := BB_F_R_DD_CONDITION_DATA(obj);
                                WHEN 'completeDryDockImport' THEN
                                    isValid := 'y';
                                    lc_vDMLResult := BB_F_R_FINISH_DD_CSV_IMPORT(obj,lc_jMessages);
                                    lc_json := lc_jMessages;
                                ELSE
                                    lc_jlMessages.append(json_list('[{error:''Error R.0010 There has been an error processing the request''}]'));
                          END CASE;


                    ELSE
                        lc_jlMessages.append(json_list('[{error:''Error R.0005 There has been an error processing the request''}]'));
                END CASE;
            else
                lc_jlMessages.append(json_list('[{error:''Error R.0002 There has been an error processing the request''}]'));
            end if;
        else
            lc_jlMessages.append(json_list('[{error:''Error R.0003 There has been an error processing the request''}]'));
        end if;
    else
        lc_jlMessages.append(json_list('[{error:''Error R.0006 There has been an error processing the request''}]'));
    end if;


    --lc_jMessages.put('status','error');
    --lc_jMessages.put('messages',lc_jlMessages);
    --lc_jlMessages.append(json_list('[{error:''ZZError R.0006 There has been an error processing the request''}]'));
    --raise json_error; --test_err;

    --if isValid != 'e' then --if a child function failed, it handled its own exception -- read is different than executing a command
      if (lc_jlMessages.count > 0) then
          lc_jMessages.put('status','error');
          lc_jMessages.put('messages',lc_jlMessages);
          raise json_error;
      else
          lc_json.htp;
      end if;
    --end if;

  exception
    when json_error then
        lc_jError := json();
        lc_jError.put('function','BB_REST.read_');
        lc_jError.put('input',in_cJSON);
        lc_jError.put('messages',lc_jlMessages);
        BB_P_ERROR(lc_jError);

    when others then
        execute immediate 'insert into debug (functionname,input) values (''BB_REST.read_'','''||sqlerrm||''')';
        /*lc_jError := json();
        lc_jError.put('function','BB_REST.read_');
        lc_jError.put('input',in_cJSON);
        lc_jlMessages.append(json_list('[{error:''R.000X ' || SQLERRM || '''}]'));
        lc_jError.put('messages',lc_jlMessages);
        BB_P_ERROR(lc_jError);*/
END read_;



function f_test_for_error (in_vReturned varchar2) return varchar2 as
  /* template variables */
  lc_vFunctionName varchar2(255) := 'f_test_for_error';
  lc_vReturn varchar2(32000) := '{"error":"default"}'; --defaul the return to e for error
  lc_nContinue number :=0; --default to 0 to dissalow continuing

  /* function specific variables */
  lc_vTest varchar2(32000) := in_vReturned;
begin

  if substr(lc_vTest, 1, 9) = '{"error":' then
    lc_vReturn := '0';
  else
    lc_vReturn := '1';
  end if;

  return lc_vReturn;

exception
  when others then
      execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' dml error: ' || sqlerrm || ''')';
      return '{"error":"' || sqlerrm || '"}';
end;

function getAttributeValue(in_vAttribute varchar2, in_jJSON json) return varchar2 as
  /* template variables */
  json_error EXCEPTION;
  spiders_error EXCEPTION;
  lc_vErrorDescription varchar2(255) := 'default';
  lc_vFunctionName varchar2(255) := 'getAttributeValue';
  lc_vReturn varchar2(32000) := '{"error":"default"}'; --defaul the return to e for error
  lc_nContinue number :=0; --default to 0 to dissalow continuing

  /* function specific variables */
  lc_jvValue json_value;
begin

  if(in_jJSON.exist(in_vAttribute)) then
    lc_jvValue := in_jJSON.get(in_vAttribute);
    lc_vReturn := getVal(lc_jvValue);
  else
    lc_vErrorDescription := in_vAttribute || ' attribute not found in json';
    raise spiders_error;
  end if;

  /* get name of object */
  return lc_vReturn;

exception
  when spiders_error then
      execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' SPIDERS error ' || lc_vErrorDescription || ''')';
      return '{"error":"' || lc_vErrorDescription || '"}';
  when json_error then
      execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' JSON error'')';
      return '{"error":"json"}';
  when others then
      execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' dml error: ' || sqlerrm || ''')';
      return '{"error":"' || sqlerrm || '"}';
end;


function f_getJsonDataRecordCount(in_jJSON json) return varchar2 as
  /* template variables */
  json_error EXCEPTION;
  spiders_error EXCEPTION;
  lc_vErrorDescription varchar2(255) := 'default';
  lc_vFunctionName varchar2(255) := 'f_getJsonDataRecordCount';
  lc_vReturn varchar2(32000) := '{"error":"default"}'; --defaul the return to e for error
  lc_nContinue number :=0; --default to 0 to dissalow continuing

  /* function specific variables */
  lc_jvValue  json_value;
  lc_jlData   json_list;
begin

  if(in_jJson.exist('data')) then
    if(in_jJson.get('data').is_array) then
      lc_jlData := json_list(in_jJson.get('data'));
      lc_vReturn := lc_jlData.count;
    end if;
  end if;

  return lc_vReturn;

exception
  when spiders_error then
      execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' SPIDERS error ' || lc_vErrorDescription || ''')';
      return '{"error":"' || lc_vErrorDescription || '"}';
  when json_error then
      execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' JSON error'')';
      return '{"error":"json"}';
  when others then
      execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' dml error: ' || sqlerrm || ''')';
      return '{"error":"' || sqlerrm || '"}';
end;

function f_getIndividualDataJson(in_jJSON json, in_nI number, in_vModule varchar2, in_vTarget varchar2) return json as
  /* template variables */
  json_error EXCEPTION;
  spiders_error EXCEPTION;
  lc_vErrorDescription varchar2(255) := 'default';
  lc_vFunctionName varchar2(255) := 'f_getJsonDataRecordCount';
  lc_vReturn varchar2(32000) := '{"error":"default"}'; --defaul the return to e for error
  lc_nContinue number :=0; --default to 0 to dissalow continuing

  /* function specific variables */
  lc_jvData       json_value;
  lc_jlData       json_list;
  lc_jlNewData    json_list;
  lc_jIndividual  json;
begin

  lc_jIndividual := json();

  lc_jvData := in_jJson.get('data');
  lc_jlData := json_list(lc_jvData);
  lc_jlNewData := json_list();
  lc_jlNewData.append(lc_jlData.get(in_nI));

  lc_jIndividual.put('module',in_vModule);
  lc_jIndividual.put('target',in_vTarget);
  lc_jIndividual.put('data',lc_jlNewData);

  return lc_jIndividual;

exception
  when spiders_error then
      execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' SPIDERS error ' || lc_vErrorDescription || ''')';
      return json('{"error":"' || lc_vErrorDescription || '"}');
  when json_error then
      execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' JSON error'')';
      return json('{"error":"json"}');
  when others then
      execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' dml error: ' || sqlerrm || ''')';
      return json('{"error":"' || sqlerrm || '"}');
end;

procedure createModel(in_cJSON clob) as
  /* template variables */
  json_error EXCEPTION;
  spiders_error EXCEPTION;
  lc_vErrorDescription varchar2(255) := 'default';
  lc_vRest varchar2(255) := 'createModel';
  lc_vFunctionName varchar2(255) := 'createModel';
  lc_vReturn varchar2(32000) := '{"error":"default"}'; --defaul the return to e for error
  lc_nContinue number :=0; --default to 0 to dissalow continuing

  /* function specific variables */
  lc_vTarget varchar2(255);
  lc_vModule varchar2(255);
  lc_vTableName varchar2(255);
  lc_jJson json;
  lc_jParameterJson json;
  lc_jResults json;
  lc_jMessages json;
  lc_jlMessages json_list;
  lc_jlDML json_list;
  lc_vDMLResult varchar2(1);
  lc_vIsAuthorized varchar2(1);
  lc_vDebug varchar2(2000);
  lc_vSql long;
  lc_nSequenceId number;
  lc_jIndividual  json;
  lc_nRecordCount number;

begin
  --htp.p(in_cJson);

  lc_jParameterJson := json(in_cJson);

  lc_vModule := getAttributeValue('module', lc_jParameterJson);

  lc_nContinue := f_test_for_error(lc_vModule);



  if lc_nContinue = 1 then
    lc_vTarget := getAttributeValue('target', lc_jParameterJson);
    lc_nContinue := f_test_for_error(lc_vTarget);
  else
    lc_vErrorDescription := 'Module attribute not found';
    raise spiders_error;
  end if;

/*
  if lc_nContinue = 1 then
    lc_vIsAuthorized := BB_F_AUTHORIZATION('BB_REST.create_',lc_vModule);
  else
    lc_vErrorDescription := 'target attribute not found';
    raise spiders_error;
  end if;
*/

  lc_vIsAuthorized := 'y'; --!!REMOVE AFTER TESTING

  if lc_vIsAuthorized = 'y' then
    lc_vTableName := getTargetTableName(lc_vRest,lc_vModule,lc_vTarget);
    lc_nContinue := f_test_for_error(lc_vTableName);
  else
    lc_vErrorDescription := 'NOT AUTHORIZED';
    raise spiders_error;
  end if;

  if lc_nContinue = 1 then
    lc_nRecordCount := f_getJsonDataRecordCount(lc_jParameterJson);
    lc_nContinue := f_test_for_error(lc_vSql);
  else
    lc_vErrorDescription := 'Table map record not found';
    raise spiders_error;
  end if;

  for i in 1..lc_nRecordCount loop

    if lc_nContinue = 1 then
      lc_jIndividual := f_getIndividualDataJson(lc_jParameterJson,i,lc_vModule,lc_vTarget);
      lc_nContinue := 1;
    else
      lc_vErrorDescription := 'Table map record not found';
      raise spiders_error;
    end if;

    if lc_nContinue = 1 then
      --insert into debug (message) values ('in bb_rest line 862');
      lc_vSql := sql_statement_functions.f_insert(lc_jIndividual, lc_vTableName, lc_nSequenceId);
      insert  into debug (input) values (lc_vSql);
      lc_nContinue := f_test_for_error(lc_vSql);
    else
      lc_vErrorDescription := 'Error with json data values';
      raise spiders_error;
    end if;

    if lc_nContinue = 1 then
      lc_jlDML := json_list();
      lc_jlDML.append(lc_vSql);

      lc_jResults := json();
      lc_jResults.put('results',lc_jlDML);

      lc_jMessages := json();
      lc_vDMLResult := BB_F_DML(lc_jResults, lc_jMessages);
      lc_jMessages.put('id', lc_nSequenceId);
      lc_vReturn := 'SUCCESS';
    else
      lc_vErrorDescription := 'SQL Statement failed';
      raise spiders_error;
    end if;

  end loop;

  lc_jMessages.htp;

exception
  when spiders_error then
      execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' SPIDERS error ' || lc_vErrorDescription || ''')';
      htp.p('{"error":"' || lc_vFunctionName || ' SPIDERS error ' || lc_vErrorDescription || '"}');
  when json_error then
      execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' JSON error'')';
      htp.p('{"error":"' || lc_vFunctionName || ' JSON error ' || lc_vErrorDescription || '"}');
  when others then
      execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' dml error: ' || sqlerrm || ''')';
     htp.p('{"error":"' || lc_vFunctionName || ' dml error ' || sqlerrm || '"}');
end;

procedure updateModel(in_cJSON clob) as
  /* template variables */
  json_error EXCEPTION;
  spiders_error EXCEPTION;
  lc_vErrorDescription varchar2(255) := 'default';
  lc_vFunctionName varchar2(255) := 'updateModel';
  lc_vRest varchar2(255) := 'updateModel';
  lc_vReturn varchar2(32000) := '{"error":"default"}'; --defaul the return to e for error
  lc_nContinue number :=0; --default to 0 to dissalow continuing

  /* function specific variables */
  lc_vTarget varchar2(255);
  lc_vModule varchar2(255);
  lc_vTableName varchar2(255);
  lc_jJson json;
  lc_jParameterJson json;
  lc_jResults json;
  lc_jMessages json;
  lc_jlMessages json_list;
  lc_jlDML json_list;
  lc_vDMLResult varchar2(1);
  lc_vIsAuthorized varchar2(1);
  lc_vDebug varchar2(2000);
  lc_vSql long;
  lc_jIndividual  json;
  lc_nRecordCount number;


begin

  /*!! I LEFT OFF just at the end of the createModel function, have not tested*/
  lc_jParameterJson := json(in_cJson);

  lc_vModule := getAttributeValue('module', lc_jParameterJson);

  lc_nContinue := f_test_for_error(lc_vModule);



  if lc_nContinue = 1 then
    lc_vTarget := getAttributeValue('target', lc_jParameterJson);
    lc_nContinue := f_test_for_error(lc_vTarget);
  else
    lc_vErrorDescription := 'Module attribute not found';
    raise spiders_error;
  end if;

/*
  if lc_nContinue = 1 then
    lc_vIsAuthorized := BB_F_AUTHORIZATION('BB_REST.create_',lc_vModule);
  else
    lc_vErrorDescription := 'target attribute not found';
    raise spiders_error;
  end if;
*/

  lc_vIsAuthorized := 'y'; --!!REMOVE AFTER TESTING

  if lc_vIsAuthorized = 'y' then
    lc_vTableName := getTargetTableName(lc_vRest,lc_vModule,lc_vTarget);
    lc_nContinue := f_test_for_error(lc_vTableName);
  else
    lc_vErrorDescription := 'NOT AUTHORIZED';
    raise spiders_error;
  end if;



  if lc_nContinue = 1 then
    lc_nRecordCount := f_getJsonDataRecordCount(lc_jParameterJson);
    lc_nContinue := f_test_for_error(lc_vSql);
  else
    lc_vErrorDescription := 'Table map record not found';
    raise spiders_error;
  end if;

  lc_jlDML := json_list(); --tc moved this here 20171218
  for i in 1..lc_nRecordCount loop

    if lc_nContinue = 1 then
      lc_jIndividual := f_getIndividualDataJson(lc_jParameterJson,i,lc_vModule,lc_vTarget);
      lc_nContinue := 1;
    else
      lc_vErrorDescription := 'Table map record not found';
      raise spiders_error;
    end if;

    if lc_nContinue = 1 then
      lc_vSql := sql_statement_functions.f_update(lc_jIndividual, lc_vTableName);
      --htp.p('update seg: '||lc_vSql);
      lc_nContinue := f_test_for_error(lc_vSql);
    else
      lc_vErrorDescription := 'Error with json data values';
      raise spiders_error;
    end if;

    if lc_nContinue = 1 then
      --lc_jlDML := json_list(); --this was resetting the array
      --htp.p('appending to lc_jlDML: '||lc_vSql);
      lc_jlDML.append(lc_vSql);
    else
      lc_vErrorDescription := 'SQL Statement failed';
      raise spiders_error;
    end if;

  end loop;


  if lc_nContinue = 1 then
    lc_jResults := json();
    lc_jResults.put('results',lc_jlDML);

    lc_jMessages := json();
    lc_vDMLResult := BB_F_DML(lc_jResults, lc_jMessages);
    lc_jMessages.htp;
    lc_vReturn := 'SUCCESS';
  else
    lc_vErrorDescription := 'SQL Statement failed';
    raise spiders_error;
  end if;


exception
  when spiders_error then
      execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' SPIDERS error ' || lc_vErrorDescription || ''')';
      htp.p('{"error":"' || lc_vFunctionName || ' SPIDERS error ' || lc_vErrorDescription || '"}');
  when json_error then
      execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' JSON error'')';
      htp.p('{"error":"' || lc_vFunctionName || ' JSON error ' || lc_vErrorDescription || '"}');
  when others then
      execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' dml error: ' || sqlerrm || ''')';
     htp.p('{"error":"' || lc_vFunctionName || ' dml error ' || sqlerrm || '"}');
end;

procedure deleteModel(in_cJSON clob) as
  /* template variables */
  json_error EXCEPTION;
  spiders_error EXCEPTION;
  lc_vErrorDescription varchar2(255) := 'default';
  lc_vFunctionName varchar2(255) := 'deleteModel';
  lc_vRest varchar2(255) := 'deleteModel';
  lc_vReturn varchar2(32000) := '{"error":"default"}'; --defaul the return to e for error
  lc_nContinue number :=0; --default to 0 to dissalow continuing

  /* function specific variables */
  lc_vTarget varchar2(255);
  lc_vModule varchar2(255);
  lc_vTableName varchar2(255);
  lc_jJson json;
  lc_jParameterJson json;
  lc_jResults json;
  lc_jMessages json;
  lc_jlMessages json_list;
  lc_jlDML json_list;
  lc_vDMLResult varchar2(1);
  lc_vIsAuthorized varchar2(1);
  lc_vDebug varchar2(2000);
  lc_vSql long;
  lc_jIndividual  json;
  lc_nRecordCount number;

begin

  /*!! I LEFT OFF just at the end of the createModel function, have not tested*/
  lc_jParameterJson := json(in_cJson);
  lc_vModule := getAttributeValue('module', lc_jParameterJson);

  lc_nContinue := f_test_for_error(lc_vModule);



  if lc_nContinue = 1 then
    lc_vTarget := getAttributeValue('target', lc_jParameterJson);
    lc_nContinue := f_test_for_error(lc_vTarget);
  else
    lc_vErrorDescription := 'Module attribute not found';
    raise spiders_error;
  end if;

/*
  if lc_nContinue = 1 then
    lc_vIsAuthorized := BB_F_AUTHORIZATION('BB_REST.create_',lc_vModule);
  else
    lc_vErrorDescription := 'target attribute not found';
    raise spiders_error;
  end if;
*/

  lc_vIsAuthorized := 'y'; --!!REMOVE AFTER TESTING

  if lc_vIsAuthorized = 'y' then
    lc_vTableName := getTargetTableName(lc_vRest,lc_vModule,lc_vTarget);
    lc_nContinue := f_test_for_error(lc_vTableName);
  else
    lc_vErrorDescription := 'NOT AUTHORIZED';
    raise spiders_error;
  end if;


  if lc_nContinue = 1 then
    lc_nRecordCount := f_getJsonDataRecordCount(lc_jParameterJson);
    lc_nContinue := f_test_for_error(lc_vSql);
  else
    lc_vErrorDescription := 'Table map record not found';
    raise spiders_error;
  end if;

  for i in 1..lc_nRecordCount loop

    if lc_nContinue = 1 then
      lc_jIndividual := f_getIndividualDataJson(lc_jParameterJson,i,lc_vModule,lc_vTarget);
      lc_nContinue := 1;
    else
      lc_vErrorDescription := 'Table map record not found';
      raise spiders_error;
    end if;

    if lc_nContinue = 1 then
      lc_vSql := sql_statement_functions.f_delete(lc_jIndividual, lc_vTableName);
      lc_nContinue := f_test_for_error(lc_vSql);
    else
      lc_vErrorDescription := 'Error with json data values';
      raise spiders_error;
    end if;

    if lc_nContinue = 1 then
      lc_jlDML := json_list();
      lc_jlDML.append(lc_vSql);
    else
      lc_vErrorDescription := 'SQL Statement failed';
      raise spiders_error;
    end if;

  end loop;


  if lc_nContinue = 1 then
    lc_jResults := json();
    lc_jResults.put('results',lc_jlDML);

    lc_jMessages := json();
    lc_vDMLResult := BB_F_DML(lc_jResults, lc_jMessages);
    lc_jMessages.htp;
    lc_vReturn := 'SUCCESS';
  else
    lc_vErrorDescription := 'SQL Statement failed';
    raise spiders_error;
  end if;


exception
  when spiders_error then
      execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' SPIDERS error ' || lc_vErrorDescription || ''')';
      htp.p('{"error":"' || lc_vFunctionName || ' SPIDERS error ' || lc_vErrorDescription || '"}');
  when json_error then
      execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' JSON error'')';
      htp.p('{"error":"' || lc_vFunctionName || ' JSON error ' || lc_vErrorDescription || '"}');
  when others then
      execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' dml error: ' || sqlerrm || ''')';
     htp.p('{"error":"' || lc_vFunctionName || ' dml error ' || sqlerrm || '"}');
end;



function f_perform_read_model(in_vTableName varchar2, in_vPrimaryKeyColumn varchar2, in_vPrimaryKeyValue varchar2) return json as
  /* template variables */
  json_error EXCEPTION;
  spiders_error EXCEPTION;
  lc_vErrorDescription varchar2(255) := 'default';
  lc_vPrefix varchar2(255) := 'SPD_V_M_';
  lc_vFunctionName varchar2(255) := 'f_perform_read_model';
  lc_vReturn varchar2(32000) := '{"error":"default"}'; --defaul the return to e for error
  lc_nContinue number :=0; --default to 0 to dissalow continuing

  /* function specific variables */
  lc_jJson json;
  lc_vSql long;
begin

  lc_vSql := 'Select * from ' || lc_vPrefix || in_vTableName || ' where ' || in_vPrimaryKeyColumn || '=' || in_vPrimaryKeyValue;
  lc_jJson := json_dyn.executeObject(lc_vSql);

  return lc_jJson;

exception
  when spiders_error then
      execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' SPIDERS error ' || lc_vErrorDescription || ''')';
      return json('{"error":"' || lc_vErrorDescription || '"}');
  when json_error then
      execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' JSON error'')';
      return json('{"error":"json"}');
  when others then
      execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' dml error: ' || sqlerrm || ''')';
      return json('{"error":"' || sqlerrm || '"}');
end;


function getOrderBy(in_vTableName varchar2) return varchar2 as
  /* template variables */
  json_error EXCEPTION;
  spiders_error EXCEPTION;
  lc_vErrorDescription varchar2(255) := 'default';
  lc_vPrefix varchar2(255) := 'SPD_V_M_';
  lc_vFunctionName varchar2(255) := 'getOrderBy';
  lc_vReturn varchar2(32000) := '{"error":"default"}'; --defaul the return to e for error
  lc_nContinue number :=0; --default to 0 to dissalow continuing

  /* function specific variables */
  lc_jJson json;
  lc_vSql long;
begin


  for rec in (select order_by_column || ' ' || direction column_direction, rownum from spd_v_m_collection_orders where object_name = in_vTableName order by sequence) loop
    lc_vSql := lc_vSql || rec.column_direction || ',';
  end loop;
  lc_vSql := ' ORDER BY ' || substr(lc_vSql,1,length(lc_vSql)-1);

  lc_vReturn := lc_vSql;
  return lc_vReturn;

exception
  when spiders_error then
      execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' SPIDERS error ' || lc_vErrorDescription || ''')';
      return '{"error":"' || lc_vErrorDescription || '"}';
  when json_error then
      execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' JSON error'')';
      return '{"error":"json"}';
  when others then
      execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' dml error: ' || sqlerrm || ''')';
      return '{"error":"' || sqlerrm || '"}';
end;


function f_perform_read_collection(in_vTableName varchar2) return json as
  /* template variables */
  json_error EXCEPTION;
  spiders_error EXCEPTION;
  lc_vErrorDescription varchar2(255) := 'default';
  lc_vPrefix varchar2(255) := 'SPD_V_M_';
  lc_vFunctionName varchar2(255) := 'f_perform_read_collection';
  lc_vReturn varchar2(32000) := '{"error":"default"}'; --defaul the return to e for error
  lc_nContinue number :=0; --default to 0 to dissalow continuing

  /* function specific variables */
  lc_jJson json;
  lc_vSql long;
  lc_vOrderBy long;
begin

  lc_vOrderBy := getOrderBy(in_vTableName);
  lc_nContinue := f_test_for_error(lc_vOrderBy);

  if lc_nContinue = 1 then
    lc_vSql := 'Select * from ' || lc_vPrefix || in_vTableName || lc_vOrderBy; --!!WE WILL NEED TO ADD A CONFIGURATION ITEM FOR ORDER BY
    lc_nContinue := f_test_for_error(lc_vSql);
  else
    lc_vErrorDescription := 'Order by statement not completed properly';
    raise spiders_error;
  end if;

--insert into debug (message) values (lc_vSql);

  lc_jJson := json_dyn.executeObject(lc_vSql);

  return lc_jJson;

exception
  when spiders_error then
      execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' SPIDERS error ' || lc_vErrorDescription || ''')';
      return json('{"error":"' || lc_vErrorDescription || '"}');
  when json_error then
      execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' JSON error'')';
      return json('{"error":"json"}');
  when others then
      execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' dml error: ' || sqlerrm || ''')';
      return json('{"error":"' || sqlerrm || '"}');
end;


function f_parseWhere(in_jJson json) return varchar2 as
 /* template variables */
  lc_vFunctionName varchar2(255) := 'f_parseWhere';

  json_error EXCEPTION;
  spiders_error EXCEPTION;
  lc_vErrorDescription varchar2(255) := 'default';

  lc_vReturn varchar2(32000) := '{"error":"default"}'; --defaul the return to e for error
  lc_nContinue number :=0; --default to 0 to dissalow continuing


  /* function specific variables */
  lc_jlWhereClause json_list;
  lc_jlWhereItem json_list;
  lc_vColumnName varchar2(255);
  lc_vWhereValue varchar2(255);
  lc_vWhereClauseSegment long;
  lc_vOperator varchar2(3) := 'and';

  lc_vComparatorStr varchar2(3) := '=';
  lc_vComparator varchar2(3) := '=';

  lc_vFunctionStr varchar2(64);

  lc_vValueWrapper varchar2(3) := '''';

/*EDITED 20170831 MR DEBUG: [SPD_BUG_FIX][XX] MULTIPLE WHERE CLAUSE UPDATE*/

begin

  if(in_jJson.exist('whereClause')) then
    if(in_jJson.get('whereClause').is_array) then
      lc_jlWhereClause := json_list(in_jJson.get('whereClause'));
      for rec in 1..lc_jlWhereClause.count loop

        lc_jlWhereItem := json_list(lc_jlWhereClause.get(rec));
        lc_vColumnName := getVal(lc_jlWhereItem.get(1));
        lc_vWhereValue := getVal(lc_jlWhereItem.get(2));

        if(lc_jlWhereItem.count>2)then
          --this might need to be reset to and, but for now i'm leaving it as is
          --lc_vOperator := 'or'; --tc did this 20171012
          lc_vOperator  := getVal(lc_jlWhereItem.get(3));
        end if;

        if(lc_jlWhereItem.count>3)then
          --get the comparator if it is included
          lc_vComparatorStr  := getVal(lc_jlWhereItem.get(4));
          --htp.p('COMPARATOR = '||lc_vComparatorStr);
          if(lc_vComparatorStr = 'gt')then
            lc_vComparator := '>';
          elsif(lc_vComparatorStr = 'lt')then
            lc_vComparator := '<';
          end if;
        end if;

        if(lc_jlWhereItem.count>4)then
          lc_vFunctionStr  := getVal(lc_jlWhereItem.get(5));
          if(lc_vFunctionStr = 'TO_DATE') then
            lc_vWhereValue := 'TO_DATE('''|| lc_vWhereValue ||''',''dd-mm-yyyy HH:MI:SS AM'')';
            lc_vValueWrapper := '';
          end if;
        end if;

        if rec = 1 then
          --lc_vWhereClauseSegment := ' WHERE ' || lc_vColumnName || '=''' || lc_vWhereValue  || '''';
          lc_vWhereClauseSegment := ' WHERE ' || lc_vColumnName || lc_vComparator || lc_vValueWrapper || lc_vWhereValue  || lc_vValueWrapper;
        else
          lc_vWhereClauseSegment := lc_vWhereClauseSegment || ' ' || lc_vOperator || ' ' || lc_vColumnName || lc_vComparator || lc_vValueWrapper || lc_vWhereValue  || lc_vValueWrapper;
        end if;
      end loop;
    else
      lc_vErrorDescription := 'whereClause is not an array';
      lc_vWhereClauseSegment := '';
      --raise spiders_error;
    end if;
  else
    lc_vErrorDescription := 'whereClause does not exist';
    lc_vWhereClauseSegment := '';
    --raise spiders_error;
  end if;


  lc_vReturn := lc_vWhereClauseSegment;
  return lc_vReturn;

exception
  when spiders_error then
      execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' SPIDERS error ' || lc_vErrorDescription || ''')';
      return '{"error":"' || lc_vErrorDescription || '"}';
  when json_error then
      execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' JSON error'')';
      return '{"error":"json"}';
  when others then
      execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' dml error: ' || sqlerrm || ''')';
      return '{"error":"' || sqlerrm || '"}';
end;

function f_parseOrderBy(in_jJson json) return varchar2 as
 /* template variables */
  lc_vFunctionName varchar2(255) := 'f_parseOrderBy';

  json_error EXCEPTION;
  spiders_error EXCEPTION;
  lc_vErrorDescription varchar2(255) := 'default';

  lc_vReturn varchar2(32000) := '{"error":"default"}'; --defaul the return to e for error
  lc_nContinue number :=0; --default to 0 to dissalow continuing


  /* function specific variables */
  lc_jlOrderBy json_list;
  lc_jlOrderItem json_list;
  lc_vColumnName varchar2(255);
  lc_vSortDirection varchar2(255);
  lc_vOrderBySegment long;
begin

  if(in_jJson.exist('orderBy')) then
    if(in_jJson.get('orderBy').is_array) then
      lc_jlOrderBy := json_list(in_jJson.get('orderBy'));
      for rec in 1..lc_jlOrderBy.count loop

        lc_jlOrderItem := json_list(lc_jlOrderBy.get(rec));
        lc_vColumnName := getVal(lc_jlOrderItem.get(1));
        lc_vSortDirection := getVal(lc_jlOrderItem.get(2));

        if rec = 1 then
          lc_vOrderBySegment := ' ORDER BY ' || lc_vColumnName || ' ' || lc_vSortDirection ;
        else
          lc_vOrderBySegment := lc_vOrderBySegment || ',' || lc_vColumnName || ' ' || lc_vSortDirection;
        end if;
      end loop;
    else
      lc_vErrorDescription := 'orderBy is not an array';
      lc_vOrderBySegment := '';
      --raise spiders_error;
    end if;
  else
    lc_vErrorDescription := 'orderBy does not exist';
    lc_vOrderBySegment := '';
    --raise spiders_error;
  end if;


  lc_vReturn := lc_vOrderBySegment;
  return lc_vReturn;

exception
  when spiders_error then
      execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' SPIDERS error ' || lc_vErrorDescription || ''')';
      return '{"error":"' || lc_vErrorDescription || '"}';
  when json_error then
      execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' JSON error'')';
      return '{"error":"json"}';
  when others then
      execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' dml error: ' || sqlerrm || ''')';
      return '{"error":"' || sqlerrm || '"}';
end;

function f_buildQueryCollectionSql(in_vPrefix varchar2, in_vTableName varchar2, in_vWhere varchar2, in_vOrderBy varchar2) return varchar2 as
 /* template variables */
  lc_vFunctionName varchar2(255) := 'f_buildQueryCollectionSql';

  json_error EXCEPTION;
  spiders_error EXCEPTION;
  lc_vErrorDescription varchar2(255) := 'default';

  lc_vReturn varchar2(32000) := '{"error":"default"}'; --defaul the return to e for error
  lc_nContinue number :=0; --default to 0 to dissalow continuing


  /* function specific variables */
  lc_vSql long;
begin

  lc_vSql := 'Select * from (select * from ' || in_vPrefix || in_vTableName || ') ' || in_vWhere || in_vOrderBy;


  lc_vReturn := lc_vSql;
  return lc_vReturn;

exception
  when spiders_error then
      execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' SPIDERS error ' || lc_vErrorDescription || ''')';
      return '{"error":"' || lc_vErrorDescription || '"}';
  when json_error then
      execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' JSON error'')';
      return '{"error":"json"}';
  when others then
      execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' dml error: ' || sqlerrm || ''')';
      return '{"error":"' || sqlerrm || '"}';
end;

function f_perform_read_qry_collection(in_jJson json, in_vTableName varchar2) return json as
  /* template variables */
  json_error EXCEPTION;
  spiders_error EXCEPTION;
  lc_vErrorDescription varchar2(255) := 'default';
  lc_vPrefix varchar2(255) := 'SPD_V_QC_';
  lc_vFunctionName varchar2(255) := 'f_perform_read_qry_collection';
  lc_vReturn varchar2(32000) := '{"error":"default"}'; --defaul the return to e for error
  lc_nContinue number :=0; --default to 0 to dissalow continuing

  /* function specific variables */
  lc_jJson json;
  lc_vSql long;
  lc_vOrderBy long;
  lc_vWhere long;
begin

  lc_vOrderBy := f_parseOrderBy(in_jJson);
  lc_nContinue := f_test_for_error(lc_vOrderBy);

  if lc_nContinue = 1 then
    lc_vWhere := f_parseWhere(in_jJson);
    lc_nContinue := f_test_for_error(lc_vWhere);
  else
    lc_vErrorDescription := 'Order by statement not completed properly';
    raise spiders_error;
  end if;


  if lc_nContinue = 1 then
    lc_vSql := f_buildQueryCollectionSql(lc_vPrefix,in_vTableName,lc_vWhere,lc_vOrderBy);
    lc_nContinue := f_test_for_error(lc_vSql);
  else
    lc_vErrorDescription := 'Where statement not completed properly';
    raise spiders_error;
  end if;
--htp.p('read qry coll-lc_vSql: '||lc_vSql);
  --insert into debug (input) values (lc_vSql);
  lc_jJson := json_dyn.executeObject(lc_vSql);

  return lc_jJson;

exception
  when spiders_error then
      execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' SPIDERS error ' || lc_vErrorDescription || ''')';
      return json('{"error":"' || lc_vErrorDescription || '"}');
  when json_error then
      execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' JSON error'')';
      return json('{"error":"json"}');
  when others then
      execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' dml error: ' || sqlerrm || ''')';
      return json('{"error":"' || sqlerrm || '"}');
end;

function getPrimaryKeyColumn(in_vTableName varchar2) return varchar2 as
  /* template variables */
  json_error EXCEPTION;
  spiders_error EXCEPTION;
  lc_vErrorDescription varchar2(255) := 'default';
  lc_vPrefix varchar2(255) := 'SPD_T_';
  lc_vFunctionName varchar2(255) := 'getPrimaryKeyColumn';
  lc_vReturn varchar2(32000) := '{"error":"default"}'; --defaul the return to e for error
  lc_nContinue number :=0; --default to 0 to dissalow continuing

  /* function specific variables */
  lc_vUser varchar2(25) := 'SPIDERS_DATA';
begin

  --htp.p('in_vTableName = '||in_vTableName);

  SELECT
    cols.column_name into lc_vReturn
  FROM
    all_constraints cons,
    all_cons_columns cols
  WHERE
    cons.owner = lc_vUser
    AND cons.constraint_type = 'P'
    AND cons.constraint_name = cols.constraint_name
    AND cons.owner = cols.owner
    AND cons.table_name = lc_vPrefix || in_vTableName;

  return lc_vReturn;

exception
  when spiders_error then
      execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' SPIDERS error ' || lc_vErrorDescription || ''')';
      return '{"error":"' || lc_vErrorDescription || '"}';
  when json_error then
      execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' JSON error'')';
      return '{"error":"json"}';
  when others then
      execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' dml error: ' || sqlerrm || ''')';
      return '{"error":"' || sqlerrm || '"}';
end;

function getCorrespondingJsonColumnVal(in_jJson json, in_vColumn varchar2) return varchar2 as
  /* template variables */
  json_error EXCEPTION;
  spiders_error EXCEPTION;
  lc_vErrorDescription varchar2(255) := 'default';
  lc_vFunctionName varchar2(255) := 'getCorrespondingJsonColumnVal';
  lc_vReturn varchar2(32000) := '{"error":"default"}'; --defaul the return to e for error
  lc_nContinue number :=0; --default to 0 to dissalow continuing

  /* function specific variables */
  lc_vName varchar2(2000);
  lc_vTemp varchar2(2000);
  lc_vComment varchar2(2000);
  lc_jvValue json_value;
  lc_jlData json_list;
  lc_jvRecVal json_value;
  lc_jValue json;

  lc_vTest varchar2(2000);
begin

  lc_vTest := in_jJson.to_char();
  --execute immediate 'insert into debug (message) values (''' || lc_vTest || ''')';

  /* i could not convince myself that we needed multiple functions here, since we still need to check if attributes exist in the sub functions*/
  if(in_jJson.exist('data')) then
      if(in_jJson.get('data').is_array) then
          lc_jlData := json_list(in_jJson.get('data'));
          lc_jvRecVal := lc_jlData.get(1); --return null on outofbounds
          if (lc_jvRecVal is not null) then
            lc_jValue := json(lc_jvRecVal);

            if(lc_jValue.exist(in_vColumn)) then
              lc_jvValue := lc_jValue.get(in_vColumn);
              lc_vReturn := getVal(lc_jvValue);
            else
              lc_vErrorDescription := in_vColumn || ' attribute not found in json';
              raise spiders_error;
            end if;
          else
            lc_vErrorDescription := 'Rec val is null';
            raise spiders_error;
          end if;
      else
        lc_vErrorDescription := 'Data is not an array';
        raise spiders_error;
      end if;
  else
    lc_vErrorDescription := 'Data attribute not found in json';
    raise spiders_error;
  end if;

  return lc_vReturn;

exception
  when spiders_error then
      execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' SPIDERS error ' || lc_vErrorDescription || ''')';
      return '{"error":"' || lc_vErrorDescription || '"}';
  when json_error then
      execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' JSON error'')';
      return '{"error":"json"}';
  when others then
      execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' dml error: ' || sqlerrm || ''')';
      return '{"error":"' || sqlerrm || '"}';
end;

procedure readModel(in_cJSON clob) as
  /* template variables */
  json_error EXCEPTION;
  spiders_error EXCEPTION;
  lc_vErrorDescription varchar2(255) := 'default';
  lc_vFunctionName varchar2(255) := 'readModel';
  lc_vRest varchar2(255) := 'readModel';
  lc_vReturn varchar2(32000) := '{"error":"default"}'; --defaul the return to e for error
  lc_nContinue number :=0; --default to 0 to dissalow continuing

  /* function specific variables */
  lc_vTarget varchar2(255);
  lc_vModule varchar2(255);
  lc_vTableName varchar2(255);
  lc_jJson json;
  lc_jParameterJson json;
  lc_jResults json;
  lc_jMessages json;
  lc_jlMessages json_list;
  lc_jlDML json_list;
  lc_vDMLResult varchar2(1);
  lc_vIsAuthorized varchar2(1);
  lc_vDebug varchar2(2000);
  lc_vSql long;
  lc_vPrimaryKeyValue varchar2(255);
  lc_vPrimaryKeyColumn varchar2(255);

begin

  /*!! I LEFT OFF just at the end of the createModel function, have not tested*/
  lc_jParameterJson := json(in_cJson);
  lc_vModule := getAttributeValue('module', lc_jParameterJson);
  lc_nContinue := f_test_for_error(lc_vModule);

  if lc_nContinue = 1 then
    lc_vTarget := getAttributeValue('target', lc_jParameterJson);
    lc_nContinue := f_test_for_error(lc_vTarget);
  else
    lc_vErrorDescription := 'Module attribute not found';
    raise spiders_error;
  end if;

/*
  if lc_nContinue = 1 then
    lc_vIsAuthorized := BB_F_AUTHORIZATION('BB_REST.create_',lc_vModule);
  else
    lc_vErrorDescription := 'target attribute not found';
    raise spiders_error;
  end if;
*/

  lc_vIsAuthorized := 'y'; --!!REMOVE AFTER TESTING

  if lc_vIsAuthorized = 'y' then
    lc_vTableName := getTargetTableName(lc_vRest,lc_vModule,lc_vTarget);
    lc_nContinue := f_test_for_error(lc_vTableName);
  else
    lc_vErrorDescription := 'NOT AUTHORIZED';
    raise spiders_error;
  end if;

  if lc_nContinue = 1 then
    lc_vPrimaryKeyColumn := getPrimaryKeyColumn(lc_vTableName);
    lc_nContinue := f_test_for_error(lc_vPrimaryKeyColumn);
  else
    lc_vErrorDescription := 'Table map record not found';
    raise spiders_error;
  end if;

  if lc_nContinue = 1 then
    lc_vPrimaryKeyValue := getCorrespondingJsonColumnVal(lc_jParameterJson, lc_vPrimaryKeyColumn);
    lc_nContinue := f_test_for_error(lc_vPrimaryKeyColumn);
  else
    lc_vErrorDescription := 'Primary key column not found';
    raise spiders_error;
  end if;

  if lc_nContinue = 1 then
    lc_jJson := f_perform_read_model(lc_vTableName, lc_vPrimaryKeyColumn, lc_vPrimaryKeyValue);
    lc_jJson.htp;
  else
    lc_vErrorDescription := 'Primary key value not found';
    raise spiders_error;
  end if;



exception
  when spiders_error then
      execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' SPIDERS error ' || lc_vErrorDescription || ''')';
      htp.p('{"error":"' || lc_vFunctionName || ' SPIDERS error ' || lc_vErrorDescription || '"}');
  when json_error then
      execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' JSON error'')';
      htp.p('{"error":"' || lc_vFunctionName || ' JSON error ' || lc_vErrorDescription || '"}');
  when others then
      execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' dml error: ' || sqlerrm || ''')';
     htp.p('{"error":"' || lc_vFunctionName || ' dml error ' || sqlerrm || '"}');
end;

procedure readCollection(in_cJSON clob) as
  /* template variables */
  json_error EXCEPTION;
  spiders_error EXCEPTION;
  lc_vErrorDescription varchar2(255) := 'default';
  lc_vFunctionName varchar2(255) := 'readCollection';
  lc_vRest varchar2(255) := 'readCollection';
  lc_vReturn varchar2(32000) := '{"error":"default"}'; --defaul the return to e for error
  lc_nContinue number :=0; --default to 0 to dissalow continuing

  /* function specific variables */
  lc_vTarget varchar2(255);
  lc_vModule varchar2(255);
  lc_vTableName varchar2(255);
  lc_jJson json;
  lc_jParameterJson json;
  lc_jResults json;
  lc_jMessages json;
  lc_jlMessages json_list;
  lc_jlDML json_list;
  lc_vDMLResult varchar2(1);
  lc_vIsAuthorized varchar2(1);
  lc_vDebug varchar2(2000);
  lc_vSql long;
  lc_vPrimaryKeyValue varchar2(255);
  lc_vPrimaryKeyColumn varchar2(255);

begin

  /*!! I LEFT OFF just at the end of the createModel function, have not tested*/
  lc_jParameterJson := json(in_cJson);
  lc_vModule := getAttributeValue('module', lc_jParameterJson);
  lc_nContinue := f_test_for_error(lc_vModule);



  if lc_nContinue = 1 then
    lc_vTarget := getAttributeValue('target', lc_jParameterJson);
    lc_nContinue := f_test_for_error(lc_vTarget);
  else
    lc_vErrorDescription := 'Module attribute not found';
    raise spiders_error;
  end if;

/*
  if lc_nContinue = 1 then
    lc_vIsAuthorized := BB_F_AUTHORIZATION('BB_REST.create_',lc_vModule);
  else
    lc_vErrorDescription := 'target attribute not found';
    raise spiders_error;
  end if;
*/

  lc_vIsAuthorized := 'y'; --!!REMOVE AFTER TESTING

  if lc_vIsAuthorized = 'y' then
    lc_vTableName := getTargetTableName(lc_vRest,lc_vModule,lc_vTarget);
    lc_nContinue := f_test_for_error(lc_vTableName);
  else
    lc_vErrorDescription := 'NOT AUTHORIZED';
    raise spiders_error;
  end if;


/*I LEFT OFF JUST ABOUT TO GET THE PRIMARY KEY VALUE FROM THE JSON*/


  if lc_nContinue = 1 then
    lc_jJson := f_perform_read_collection(lc_vTableName);
    lc_jJson.htp;
  else
    lc_vErrorDescription := 'Table name not found';
    raise spiders_error;
  end if;



exception
  when spiders_error then
      execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' SPIDERS error ' || lc_vErrorDescription || ''')';
      htp.p('{"error":"' || lc_vFunctionName || ' SPIDERS error ' || lc_vErrorDescription || '"}');
  when json_error then
      execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' JSON error'')';
      htp.p('{"error":"' || lc_vFunctionName || ' JSON error ' || lc_vErrorDescription || '"}');
  when others then
      execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' dml error: ' || sqlerrm || ''')';
     htp.p('{"error":"' || lc_vFunctionName || ' dml error ' || sqlerrm || '"}');
end;

procedure readQueryCollection(in_cJSON clob) as
  /* template variables */
  json_error EXCEPTION;
  spiders_error EXCEPTION;
  lc_vErrorDescription varchar2(255) := 'default';
  lc_vFunctionName varchar2(255) := 'readQueryCollection';
  lc_vRest varchar2(255) := 'readQueryCollection';
  lc_vReturn varchar2(32000) := '{"error":"default"}'; --defaul the return to e for error
  lc_nContinue number :=0; --default to 0 to dissalow continuing

  /* function specific variables */
  lc_vTarget varchar2(255);
  lc_vModule varchar2(255);
  lc_vTableName varchar2(255);
  lc_jJson json;
  lc_jParameterJson json;
  lc_jResults json;
  lc_jMessages json;
  lc_jlMessages json_list;
  lc_jlDML json_list;
  lc_vDMLResult varchar2(1);
  lc_vIsAuthorized varchar2(1);
  lc_vDebug varchar2(2000);
  lc_vSql long;
  lc_vPrimaryKeyValue varchar2(255);
  lc_vPrimaryKeyColumn varchar2(255);

begin

  /*!! I LEFT OFF just at the end of the createModel function, have not tested*/
  lc_jParameterJson := json(in_cJson);
  lc_vModule := getAttributeValue('module', lc_jParameterJson);
  lc_nContinue := f_test_for_error(lc_vModule);



  if lc_nContinue = 1 then
    lc_vTarget := getAttributeValue('target', lc_jParameterJson);
    lc_nContinue := f_test_for_error(lc_vTarget);
  else
    lc_vErrorDescription := 'Module attribute not found';
    raise spiders_error;
  end if;

/*
  if lc_nContinue = 1 then
    lc_vIsAuthorized := BB_F_AUTHORIZATION('BB_REST.create_',lc_vModule);
  else
    lc_vErrorDescription := 'target attribute not found';
    raise spiders_error;
  end if;
*/

  lc_vIsAuthorized := 'y'; --!!REMOVE AFTER TESTING

  if lc_vIsAuthorized = 'y' then
    lc_vTableName := getTargetTableWithoutModule(lc_vRest,lc_vTarget);
    lc_nContinue := f_test_for_error(lc_vTableName);
  else
    lc_vErrorDescription := 'NOT AUTHORIZED';
    raise spiders_error;
  end if;


/*I LEFT OFF JUST ABOUT TO GET THE PRIMARY KEY VALUE FROM THE JSON*/


  if lc_nContinue = 1 then

    lc_jJson := f_perform_read_qry_collection(lc_jParameterJson, lc_vTableName);
    lc_jJson.htp;
  else
    lc_vErrorDescription := 'Table name not found';
    raise spiders_error;
  end if;



exception
  when spiders_error then
      execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' SPIDERS error ' || lc_vErrorDescription || ''')';
      htp.p('{"error":"' || lc_vFunctionName || ' SPIDERS error ' || lc_vErrorDescription || '"}');
  when json_error then
      execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' JSON error'')';
      htp.p('{"error":"' || lc_vFunctionName || ' JSON error ' || lc_vErrorDescription || '"}');
  when others then
      execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' dml error: ' || sqlerrm || ''')';
     htp.p('{"error":"' || lc_vFunctionName || ' dml error ' || sqlerrm || '"}');
end;

procedure readDataTable(in_cJSON clob) as
  /* template variables */
  json_error EXCEPTION;
  spiders_error EXCEPTION;
  lc_vErrorDescription varchar2(255) := 'default';
  lc_vFunctionName varchar2(255) := 'readDataTable';
  lc_vRest varchar2(255) := 'readDataTable';
  lc_vReturn varchar2(32000) := '{"error":"default"}'; --defaul the return to e for error
  lc_nContinue number :=0; --default to 0 to dissalow continuing

  /* function specific variables */
  lc_vTarget varchar2(255);
  lc_vModule varchar2(255);
  lc_vTableName varchar2(255);
  lc_jJson json;
  lc_jParameterJson json;
  lc_jResults json;
  lc_jMessages json;
  lc_jlMessages json_list;
  lc_jlDML json_list;
  lc_vDMLResult varchar2(1);
  lc_vIsAuthorized varchar2(1);
  lc_vDebug varchar2(2000);
  lc_vSql long;
  lc_vPrimaryKeyValue varchar2(255);
  lc_vPrimaryKeyColumn varchar2(255);

begin

  /*!! I LEFT OFF just at the end of the createModel function, have not tested*/
  lc_jParameterJson := json(in_cJson);
  lc_vModule := getAttributeValue('module', lc_jParameterJson);
  lc_nContinue := f_test_for_error(lc_vModule);




  if lc_nContinue = 1 then
    lc_vTarget := getAttributeValue('target', lc_jParameterJson);
    lc_nContinue := f_test_for_error(lc_vTarget);
  else
    lc_vErrorDescription := 'Module attribute not found';
    raise spiders_error;
  end if;

/*
  if lc_nContinue = 1 then
    lc_vIsAuthorized := BB_F_AUTHORIZATION('BB_REST.create_',lc_vModule);
  else
    lc_vErrorDescription := 'target attribute not found';
    raise spiders_error;
  end if;
*/

  lc_vIsAuthorized := 'y'; --!!REMOVE AFTER TESTING

  if lc_vIsAuthorized = 'y' then
    lc_vTableName := getTargetTableName(lc_vRest,lc_vModule,lc_vTarget);
    lc_nContinue := f_test_for_error(lc_vTableName);
  else
    lc_vErrorDescription := 'NOT AUTHORIZED';
    raise spiders_error;
  end if;


/*I LEFT OFF JUST ABOUT TO GET THE PRIMARY KEY VALUE FROM THE JSON*/


  if lc_nContinue = 1 then
    lc_jJson := pkg_read_data_table.f_read_data_table(lc_jParameterJson, lc_vTableName, lc_vModule);
    lc_jJson.htp;
  else
    lc_vErrorDescription := 'Table name not found';
    raise spiders_error;
  end if;



exception
  when spiders_error then
      execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' SPIDERS error ' || lc_vErrorDescription || ''')';
      htp.p('{"error":"' || lc_vFunctionName || ' SPIDERS error ' || lc_vErrorDescription || '"}');
  when json_error then
      execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' JSON error'')';
      htp.p('{"error":"' || lc_vFunctionName || ' JSON error ' || lc_vErrorDescription || '"}');
  when others then
      execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' dml error: ' || sqlerrm || ''')';
     htp.p('{"error":"' || lc_vFunctionName || ' dml error ' || sqlerrm || '"}');
end;

procedure downloadDataTable(in_cJSON clob) as
  /* template variables */
  json_error EXCEPTION;
  spiders_error EXCEPTION;
  lc_vErrorDescription varchar2(255) := 'default';
  lc_vFunctionName varchar2(255) := 'readDataTable';
  lc_vRest varchar2(255) := 'readDataTable';
  lc_vReturn varchar2(32000) := '{"error":"default"}'; --defaul the return to e for error
  lc_nContinue number :=0; --default to 0 to dissalow continuing

  /* function specific variables */
  lc_vTarget varchar2(255);
  lc_vModule varchar2(255);
  lc_vTableName varchar2(255);
  lc_jJson json;
  lc_jParameterJson json;
  lc_jResults json;
  lc_jMessages json;
  lc_jlMessages json_list;
  lc_jlDML json_list;
  lc_vDMLResult varchar2(1);
  lc_vIsAuthorized varchar2(1);
  lc_vDebug varchar2(2000);
  lc_vSql long;
  lc_vPrimaryKeyValue varchar2(255);
  lc_vPrimaryKeyColumn varchar2(255);

begin

  --htp.p('AT BEGINNING OF DOWNLOAD DATATABLE');
  
  /*!! I LEFT OFF just at the end of the createModel function, have not tested*/
  lc_jParameterJson := json(in_cJson);
  lc_vModule := getAttributeValue('module', lc_jParameterJson);
  lc_nContinue := f_test_for_error(lc_vModule);


  --insert into debug (misc) values ( in_cJson );
  --insert into debug (message) values ( lc_vModule );
  --insert into debug (functionname, input) values ( 'bb_rest','here1');

  if lc_nContinue = 1 then
    lc_vTarget := getAttributeValue('target', lc_jParameterJson);
    --insert into debug (message) values ( lc_vTarget );
    lc_nContinue := f_test_for_error(lc_vTarget);
  else
    lc_vErrorDescription := 'Module attribute not found';
    raise spiders_error;
  end if;

/*
  if lc_nContinue = 1 then
    lc_vIsAuthorized := BB_F_AUTHORIZATION('BB_REST.create_',lc_vModule);
  else
    lc_vErrorDescription := 'target attribute not found';
    raise spiders_error;
  end if;
*/

  lc_vIsAuthorized := 'y'; --!!REMOVE AFTER TESTING

  if lc_vIsAuthorized = 'y' then
    lc_vTableName := getTargetTableName(lc_vRest,lc_vModule,lc_vTarget);
    --insert into debug (message) values ( lc_vTableName );
    lc_nContinue := f_test_for_error(lc_vTableName);
  else
    lc_vErrorDescription := 'NOT AUTHORIZED';
    raise spiders_error;
  end if;


/*I LEFT OFF JUST ABOUT TO GET THE PRIMARY KEY VALUE FROM THE JSON*/

  --htp.p('made it here continue - '||lc_nContinue);
  if lc_nContinue = 1 then
    lc_jJson := pkg_read_data_table.f_download(lc_jParameterJson, lc_vTableName, lc_vModule);
    --insert into debug (functionname, input) values ( 'bb_rest','here2');
    lc_jJson.htp;

  else
    lc_vErrorDescription := 'Table name not found';
    raise spiders_error;
  end if;



exception
  when spiders_error then
      execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' SPIDERS error ' || lc_vErrorDescription || ''')';
      htp.p('{"error":"' || lc_vFunctionName || ' SPIDERS error ' || lc_vErrorDescription || '"}');
  when json_error then
      execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' JSON error'')';
      htp.p('{"error":"' || lc_vFunctionName || ' JSON error ' || lc_vErrorDescription || '"}');
  when others then
      execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' dml error: ' || sqlerrm || ''')';
     htp.p('{"error":"' || lc_vFunctionName || ' dml error ' || sqlerrm || '"}');
end;


procedure createModelCustom(in_cJSON clob) as
  /* template variables */
  json_error EXCEPTION;
  spiders_error EXCEPTION;
  lc_vErrorDescription varchar2(255) := 'default';
  lc_vFunctionName varchar2(255) := 'createModelCustom';
  lc_vRest varchar2(255) := 'createModelCustom';
  lc_vReturn varchar2(32000) := '{"error":"default"}'; --defaul the return to e for error
  lc_nContinue number :=0; --default to 0 to dissalow continuing

  /* function specific variables */
  lc_vTarget varchar2(255);
  lc_vModule varchar2(255);
  lc_vTargetFunctionName varchar2(255);
  lc_jJson json;
  lc_vIsAuthorized varchar2(1);
  lc_jParameterJson json;


  /*EDITED 20170915 MR Updated REMOVED DEBUG: [SPD3D][45] Sync*/

begin

  /*!! I LEFT OFF just at the end of the createModel function, have not tested*/
  lc_jParameterJson := json(in_cJson);
  lc_vModule := getAttributeValue('module', lc_jParameterJson);
  lc_nContinue := f_test_for_error(lc_vModule);



  if lc_nContinue = 1 then
    lc_vTarget := getAttributeValue('target', lc_jParameterJson);
    lc_nContinue := f_test_for_error(lc_vTarget);
  else
    lc_vErrorDescription := 'Module attribute not found';
    raise spiders_error;
  end if;

/*
  if lc_nContinue = 1 then
    lc_vIsAuthorized := BB_F_AUTHORIZATION('BB_REST.create_',lc_vModule);
  else
    lc_vErrorDescription := 'target attribute not found';
    raise spiders_error;
  end if;
*/

  lc_vIsAuthorized := 'y'; --!!REMOVE AFTER TESTING
  if lc_vIsAuthorized = 'y' then
    --htp.p('REST: ' || lc_vRest || ' MODULE: ' || lc_vModule || ' TARGET: ' || lc_vTarget);
    lc_vTargetFunctionName := getTargetFunctionName(lc_vRest,lc_vModule,lc_vTarget);
    --htp.p('TARGET FUNCTION: ' || lc_vTargetFunctionName);
    lc_nContinue := f_test_for_error(lc_vTargetFunctionName);
  else
    lc_vErrorDescription := 'NOT AUTHORIZED';
    raise spiders_error;
  end if;

  if lc_nContinue = 1 then
    --htp.p('declare result json begin result:= ' || lc_vTargetFunctionName || '(''' || in_cJSON || '''); end;');
    execute immediate 'begin :result := ' || lc_vTargetFunctionName || '(''' || in_cJSON || '''); end;' using out lc_jJson;
    lc_jJson.htp;
  else
    lc_vErrorDescription := 'Table name not found';
    raise spiders_error;
  end if;



exception
  when spiders_error then
      execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' SPIDERS error ' || lc_vErrorDescription || ''')';
      htp.p('{"error":"' || lc_vFunctionName || ' SPIDERS error ' || lc_vErrorDescription || '"}');
  when json_error then
      execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' JSON error'')';
      htp.p('{"error":"' || lc_vFunctionName || ' JSON error ' || lc_vErrorDescription || '"}');
  when others then
      execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' dml error: ' || sqlerrm || ''')';
     htp.p('{"error":"' || lc_vFunctionName || ' dml error ' || sqlerrm || '"}');
end;

procedure deleteModelCustom(in_cJSON clob) as
  /* template variables */
  json_error EXCEPTION;
  spiders_error EXCEPTION;
  lc_vErrorDescription varchar2(255) := 'default';
  lc_vFunctionName varchar2(255) := 'deleteModelCustom';
  lc_vRest varchar2(255) := 'deleteModelCustom';
  lc_vReturn varchar2(32000) := '{"error":"default"}'; --defaul the return to e for error
  lc_nContinue number :=0; --default to 0 to dissalow continuing

  /* function specific variables */
  lc_vTarget varchar2(255);
  lc_vModule varchar2(255);
  lc_vTargetFunctionName varchar2(255);
  lc_jJson json;
  lc_vIsAuthorized varchar2(1);
  lc_jParameterJson json;


begin

  /*!! I LEFT OFF just at the end of the createModel function, have not tested*/
  lc_jParameterJson := json(in_cJson);
  lc_vModule := getAttributeValue('module', lc_jParameterJson);
  lc_nContinue := f_test_for_error(lc_vModule);



  if lc_nContinue = 1 then
    lc_vTarget := getAttributeValue('target', lc_jParameterJson);
    lc_nContinue := f_test_for_error(lc_vTarget);
  else
    lc_vErrorDescription := 'Module attribute not found';
    raise spiders_error;
  end if;

/*
  if lc_nContinue = 1 then
    lc_vIsAuthorized := BB_F_AUTHORIZATION('BB_REST.create_',lc_vModule);
  else
    lc_vErrorDescription := 'target attribute not found';
    raise spiders_error;
  end if;
*/

  lc_vIsAuthorized := 'y'; --!!REMOVE AFTER TESTING

  if lc_vIsAuthorized = 'y' then
    lc_vTargetFunctionName := getTargetFunctionName(lc_vRest,lc_vModule,lc_vTarget);
    lc_nContinue := f_test_for_error(lc_vTargetFunctionName);
  else
    lc_vErrorDescription := 'NOT AUTHORIZED';
    raise spiders_error;
  end if;

  if lc_nContinue = 1 then
    execute immediate 'begin :result := ' || lc_vTargetFunctionName || '(''' || in_cJSON || '''); end;' using out lc_jJson;
    lc_jJson.htp;
  else
    lc_vErrorDescription := 'Table name not found';
    raise spiders_error;
  end if;



exception
  when spiders_error then
      execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' SPIDERS error ' || lc_vErrorDescription || ''')';
      htp.p('{"error":"' || lc_vFunctionName || ' SPIDERS error ' || lc_vErrorDescription || '"}');
  when json_error then
      execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' JSON error'')';
      htp.p('{"error":"' || lc_vFunctionName || ' JSON error ' || lc_vErrorDescription || '"}');
  when others then
      execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' dml error: ' || sqlerrm || ''')';
     htp.p('{"error":"' || lc_vFunctionName || ' dml error ' || sqlerrm || '"}');
end;

procedure readQueryCollectionCustom(in_cJSON clob) as
  /* template variables */
  json_error EXCEPTION;
  spiders_error EXCEPTION;
  lc_vErrorDescription varchar2(255) := 'default';
  lc_vFunctionName varchar2(255) := 'readQueryCollectionCustom';
  lc_vRest varchar2(255) := 'readQueryCollectionCustom';
  lc_vReturn varchar2(32000) := '{"error":"default"}'; --defaul the return to e for error
  lc_nContinue number :=0; --default to 0 to dissalow continuing

  /* function specific variables */
  lc_vTarget varchar2(255);
  lc_vModule varchar2(255);
  lc_vTargetFunctionName varchar2(255);
  lc_jJson json;
  lc_vIsAuthorized varchar2(1);
  lc_jParameterJson json;


begin


  /*!! I LEFT OFF just at the end of the createModel function, have not tested*/
  lc_jParameterJson := json(in_cJson);
  lc_vModule := getAttributeValue('module', lc_jParameterJson);
  lc_nContinue := f_test_for_error(lc_vModule);



  if lc_nContinue = 1 then
    lc_vTarget := getAttributeValue('target', lc_jParameterJson);
    lc_nContinue := f_test_for_error(lc_vTarget);
  else
    lc_vErrorDescription := 'Module attribute not found';
    raise spiders_error;
  end if;


/*
  if lc_nContinue = 1 then
    lc_vIsAuthorized := BB_F_AUTHORIZATION('BB_REST.create_',lc_vModule);
  else
    lc_vErrorDescription := 'target attribute not found';
    raise spiders_error;
  end if;
*/

  lc_vIsAuthorized := 'y'; --!!REMOVE AFTER TESTING

  if lc_vIsAuthorized = 'y' then
    lc_vTargetFunctionName := getTargetFunctionName(lc_vRest,lc_vModule,lc_vTarget);
    lc_nContinue := f_test_for_error(lc_vTargetFunctionName);
  else
    lc_vErrorDescription := 'NOT AUTHORIZED';
    raise spiders_error;
  end if;
      --insert into debug (input) values ('target function name : '||lc_vTargetFunctionName);

  if lc_nContinue = 1 then
    --insert into debug (input) values ('before : '|| lc_vTargetFunctionName || '(''' || in_cJSON || ''');');
    lc_jJson := json();
    execute immediate 'begin :result := ' || lc_vTargetFunctionName || '(''' || in_cJSON || '''); end;' using out lc_jJson;
        --insert into debug (input) values ('after');
    lc_jJson.htp;
  else
    lc_vErrorDescription := 'Table name not found';
    raise spiders_error;
  end if;



exception
  when spiders_error then
      execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' SPIDERS error ' || lc_vErrorDescription || ''')';
      htp.p('{"error":"' || lc_vFunctionName || ' SPIDERS error ' || lc_vErrorDescription || '"}');
  when json_error then
      execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' JSON error'')';
      htp.p('{"error":"' || lc_vFunctionName || ' JSON error ' || lc_vErrorDescription || '"}');
  when others then
      execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' dml error: ' || sqlerrm || ''')';
     htp.p('{"error":"' || lc_vFunctionName || ' dml error ' || sqlerrm || '"}');
end;

procedure readDataTableCustom(in_cJSON clob) as
  /* template variables */
  json_error EXCEPTION;
  spiders_error EXCEPTION;
  lc_vErrorDescription varchar2(255) := 'default';
  lc_vFunctionName varchar2(255) := 'readDataTableCustom';
  lc_vRest varchar2(255) := 'readDataTableCustom';
  lc_vReturn varchar2(32000) := '{"error":"default"}'; --defaul the return to e for error
  lc_nContinue number :=0; --default to 0 to dissalow continuing

  /* function specific variables */
  lc_vTarget varchar2(255);
  lc_vModule varchar2(255);
  lc_vTargetFunctionName varchar2(255);
  lc_jJson json;
  lc_vIsAuthorized varchar2(1);
  lc_jParameterJson json;


begin

  /*!! I LEFT OFF just at the end of the createModel function, have not tested*/
  lc_jParameterJson := json(in_cJson);
  lc_vModule := getAttributeValue('module', lc_jParameterJson);
  lc_nContinue := f_test_for_error(lc_vModule);



  if lc_nContinue = 1 then
    lc_vTarget := getAttributeValue('target', lc_jParameterJson);
    lc_nContinue := f_test_for_error(lc_vTarget);
  else
    lc_vErrorDescription := 'Module attribute not found';
    raise spiders_error;
  end if;

/*
  if lc_nContinue = 1 then
    lc_vIsAuthorized := BB_F_AUTHORIZATION('BB_REST.create_',lc_vModule);
  else
    lc_vErrorDescription := 'target attribute not found';
    raise spiders_error;
  end if;
*/

  lc_vIsAuthorized := 'y'; --!!REMOVE AFTER TESTING

  if lc_vIsAuthorized = 'y' then
    lc_vTargetFunctionName := getTargetFunctionName(lc_vRest,lc_vModule,lc_vTarget);
    lc_nContinue := f_test_for_error(lc_vTargetFunctionName);
  else
    lc_vErrorDescription := 'NOT AUTHORIZED';
    raise spiders_error;
  end if;

  if lc_nContinue = 1 then
    execute immediate 'begin :result := ' || lc_vTargetFunctionName || '(''' || in_cJSON || '''); end;' using out lc_jJson;
    lc_jJson.htp;
  else
    lc_vErrorDescription := 'Table name not found';
    raise spiders_error;
  end if;



exception
  when spiders_error then
      execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' SPIDERS error ' || lc_vErrorDescription || ''')';
      htp.p('{"error":"' || lc_vFunctionName || ' SPIDERS error ' || lc_vErrorDescription || '"}');
  when json_error then
      execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' JSON error'')';
      htp.p('{"error":"' || lc_vFunctionName || ' JSON error ' || lc_vErrorDescription || '"}');
  when others then
      execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' dml error: ' || sqlerrm || ''')';
     htp.p('{"error":"' || lc_vFunctionName || ' dml error ' || sqlerrm || '"}');
end;

procedure downloadDataTableCustom(in_cJSON clob) as
  /* template variables */
  json_error EXCEPTION;
  spiders_error EXCEPTION;
  lc_vErrorDescription varchar2(255) := 'default';
  lc_vFunctionName varchar2(255) := 'readDataTableCustom';
  lc_vRest varchar2(255) := 'downloadDataTableCustom';
  lc_vReturn varchar2(32000) := '{"error":"default"}'; --defaul the return to e for error
  lc_nContinue number :=0; --default to 0 to dissalow continuing

  /* function specific variables */
  lc_vTarget varchar2(255);
  lc_vModule varchar2(255);
  lc_vTargetFunctionName varchar2(255);
  lc_jJson json;
  lc_vIsAuthorized varchar2(1);
  lc_jParameterJson json;


begin

  /*!! I LEFT OFF just at the end of the createModel function, have not tested*/
  lc_jParameterJson := json(in_cJson);
  lc_vModule := getAttributeValue('module', lc_jParameterJson);
  lc_nContinue := f_test_for_error(lc_vModule);

  --insert into debug (functionname,input) values ('bb_rest','downloadDatatableCustom');

  if lc_nContinue = 1 then
    lc_vTarget := getAttributeValue('target', lc_jParameterJson);
    lc_nContinue := f_test_for_error(lc_vTarget);
  else
    lc_vErrorDescription := 'Module attribute not found';
    raise spiders_error;
  end if;

/*
  if lc_nContinue = 1 then
    lc_vIsAuthorized := BB_F_AUTHORIZATION('BB_REST.create_',lc_vModule);
  else
    lc_vErrorDescription := 'target attribute not found';
    raise spiders_error;
  end if;
*/

  lc_vIsAuthorized := 'y'; --!!REMOVE AFTER TESTING

  if lc_vIsAuthorized = 'y' then
    lc_vTargetFunctionName := getTargetFunctionName(lc_vRest,lc_vModule,lc_vTarget);
    lc_nContinue := f_test_for_error(lc_vTargetFunctionName);
  else
    lc_vErrorDescription := 'NOT AUTHORIZED';
    raise spiders_error;
  end if;

  if lc_nContinue = 1 then
    execute immediate 'begin :result := ' || lc_vTargetFunctionName || '(''' || in_cJSON || '''); end;' using out lc_jJson;
    lc_jJson.htp;
  else
    lc_vErrorDescription := 'Table name not found';
    raise spiders_error;
  end if;



exception
  when spiders_error then
      execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' SPIDERS error ' || lc_vErrorDescription || ''')';
      htp.p('{"error":"' || lc_vFunctionName || ' SPIDERS error ' || lc_vErrorDescription || '"}');
  when json_error then
      execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' JSON error'')';
      htp.p('{"error":"' || lc_vFunctionName || ' JSON error ' || lc_vErrorDescription || '"}');
  when others then
      execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' dml error: ' || sqlerrm || ''')';
     htp.p('{"error":"' || lc_vFunctionName || ' dml error ' || sqlerrm || '"}');
end;
END BB_REST;

/
