--------------------------------------------------------
--  DDL for Package Body PKG_DELIVERY_ORDER_FACILITY
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE PACKAGE BODY "SPIDERS_DATA"."PKG_DELIVERY_ORDER_FACILITY" as 

function f_test_for_error (in_vReturned varchar2) return varchar2 as 
  /* template variables */
  lc_vFunctionName varchar2(255) := 'f_test_for_error';
  lc_vReturn varchar2(32000) := '{"error":"default"}'; --defaul the return to e for error
  lc_nContinue number :=0; --default to 0 to dissalow continuing
  
  /* function specific variables */
  lc_vTest varchar2(32000) := in_vReturned;
begin

  if substr(lc_vTest, 1, 9) = '{"error":' then
    lc_vReturn := '0';
  else
    lc_vReturn := '1';
  end if;
  
  return lc_vReturn;

exception     
  when others then
      execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' dml error: ' || sqlerrm || ''')';
      return '{"error":"' || sqlerrm || '"}';
end; 


function getAttributeValue(in_vAttribute varchar2, in_jJSON json) return varchar2 as 
  /* template variables */
  json_error EXCEPTION;
  spiders_error EXCEPTION;
  lc_vErrorDescription varchar2(255) := 'default';
  lc_vFunctionName varchar2(255) := 'getAttributeValue';
  lc_vReturn varchar2(32000) := '{"error":"default"}'; --defaul the return to e for error
  lc_nContinue number :=0; --default to 0 to dissalow continuing
  
  /* function specific variables */
  lc_jvValue json_value;
begin
 
  if(in_jJSON.exist(in_vAttribute)) then       
    lc_jvValue := in_jJSON.get(in_vAttribute);
    lc_vReturn := getVal(lc_jvValue);
  else
    lc_vErrorDescription := in_vAttribute || ' attribute not found in json';
    raise spiders_error;
  end if;
  
  /* get name of object */
  return lc_vReturn;

exception     
  when spiders_error then
      execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' SPIDERS error ' || lc_vErrorDescription || ''')';
      return '{"error":"' || lc_vErrorDescription || '"}';
  when json_error then
      execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' JSON error'')';
      return '{"error":"json"}';
  when others then
      execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' dml error: ' || sqlerrm || ''')';
      return '{"error":"' || sqlerrm || '"}';
end; 

function getSpidersFacilityId(in_vFACILITY_ID varchar2) return number as
 /* template variables */
  lc_vFunctionName varchar2(255) := 'getSpidersFacilityId';

  json_error EXCEPTION;
  spiders_error EXCEPTION;
  lc_vErrorDescription varchar2(255) := 'default';

  lc_nReturn number;
  lc_nContinue number :=0; --default to 0 to dissalow continuing

  
  /* function specific variables */
begin
  lc_nReturn :=0;

  --THERE WILL BE ISSUES WITH THIS IF THERE IS MORE THAN RECORD FOR THE SAME NFAID IN SPIDERS TABLE
  for rec in (select facid from spd_t_facilities where upper(infads_facility_id) = upper(in_vFACILITY_ID)) loop
    lc_nReturn := rec.facid;
  end loop;


  lc_nReturn := lc_nReturn;
  return lc_nReturn;

exception   
  when spiders_error then
      execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' SPIDERS error ' || lc_vErrorDescription || ''')';
      return -1;
  when json_error then
      execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' JSON error'')';
      return -2;
  when others then
      execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' dml error: ' || sqlerrm || ''')';
      return -3;
end; 

function getJsonList(in_vAttribute varchar2, in_jJSON json) return json_list as 
  /* template variables */
  lc_vFunctionName varchar2(255) := 'getJsonList';
  
  json_error EXCEPTION;
  spiders_error EXCEPTION;
  lc_vErrorDescription varchar2(255) := 'default';
  lc_jlReturn json_list; --defaul the return to e for error
  lc_nContinue number :=0; --default to 0 to dissalow continuing
  
  /* function specific variables */
  lc_jlData json_list;
begin
  
  if(in_jJSON.exist(in_vAttribute)) then 
    if(in_jJson.get(in_vAttribute).is_array) then
      lc_jlData := json_list(in_jJson.get(in_vAttribute));
    else
      lc_jlData := json_list();
    end if;
  else
    lc_vErrorDescription := in_vAttribute || ' attribute not found in json';
    raise spiders_error;
  end if;
  
  lc_jlReturn := lc_jlData;
  return lc_jlReturn;

exception     
  when spiders_error then
      execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' SPIDERS error ' || lc_vErrorDescription || ''')';
      return json_list('{"error":"' || lc_vErrorDescription || '"}');
  when json_error then
      execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' JSON error'')';
      return json_list('{"error":"json"}');
  when others then
      execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' dml error: ' || sqlerrm || ''')';
      return json_list('{"error":"' || sqlerrm || '"}');
end; 


function getJsonListItem(in_nRow number, in_jlJSON json_list) return json as 
  /* template variables */
  lc_vFunctionName varchar2(255) := 'getJsonList';
  
  json_error EXCEPTION;
  spiders_error EXCEPTION;
  lc_vErrorDescription varchar2(255) := 'default';
  lc_jReturn json; --defaul the return to e for error
  lc_nContinue number :=0; --default to 0 to dissalow continuing
  
  /* function specific variables */
  lc_jData json;
begin
  
  lc_jData := json(in_jlJSON.get(in_nRow));
  
  lc_jReturn := lc_jData;
  return lc_jReturn;

exception     
  when spiders_error then
      execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' SPIDERS error ' || lc_vErrorDescription || ''')';
      return json('{"error":"' || lc_vErrorDescription || '"}');
  when json_error then
      execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' JSON error'')';
      return json('{"error":"json"}');
  when others then
      execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' dml error: ' || sqlerrm || ''')';
      return json('{"error":"' || sqlerrm || '"}');
end; 




function makeInsertFacilitySql(in_nFacid number, in_jJSON json) return varchar2 as 
  /* template variables */
  lc_vFunctionName varchar2(255) := 'makeInsertFacilitySql';
  json_error EXCEPTION;
  spiders_error EXCEPTION;
  lc_vErrorDescription varchar2(255) := 'default';
  lc_vReturn long := '{"error":"default"}';
  lc_nContinue number :=0; --default to 0 to dissalow continuing
  
  /* function specific variables */
  lc_jJson json;
  lc_vINFADS_FACILITY_ID varchar2(255);
  lc_vPRODUCT_LINE_LISTID varchar2(255);
  lc_vSPD_FAC_NAME varchar2(255);
  lc_vSPD_FAC_NO varchar2(255);
  lc_vSql long;
begin

  
  lc_vINFADS_FACILITY_ID :=   getAttributeValue('FACILITY_ID', in_jJSON);
  lc_vPRODUCT_LINE_LISTID :=  getAttributeValue('PRODUCTLINE_LISTID', in_jJSON);
  lc_vSPD_FAC_NAME :=         getAttributeValue('FACILITY_NAME', in_jJSON);
  lc_vSPD_FAC_NO :=           getAttributeValue('FACILITY_NO', in_jJSON);
  
  lc_vSql := 'INSERT INTO SPD_T_FACILITIES (FACID, INFADS_FACILITY_ID, PRODUCT_LINE_LISTID, SPD_FAC_NAME, SPD_FAC_NO) VALUES (' || in_nFacid || ',''' || lc_vINFADS_FACILITY_ID || ''',' || lc_vPRODUCT_LINE_LISTID || ',''' || lc_vSPD_FAC_NAME || ''',''' || lc_vSPD_FAC_NO || ''')'; 
  
  lc_vReturn := lc_vSql;
  /* get name of object */
  return lc_vReturn;

exception     
  when spiders_error then
      execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' SPIDERS error ' || lc_vErrorDescription || ''')';
      return '{"error":"' || lc_vErrorDescription || '"}';
  when json_error then
      execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' JSON error'')';
      return '{"error":"json"}';
  when others then
      execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' dml error: ' || sqlerrm || ''')';
      return '{"error":"' || sqlerrm || '"}';
end; 

function getNextFacilityId return number as 
  /* template variables */
  lc_vFunctionName varchar2(255) := 'getNextFacilitiesId';
  json_error EXCEPTION;
  spiders_error EXCEPTION;
  lc_vErrorDescription varchar2(255) := 'default';
  lc_nReturn number;
  lc_nContinue number :=0; --default to 0 to dissalow continuing
  
  /* function specific variables */
begin

  select spd_s_facilities.nextval into lc_nReturn from dual;
  

  return lc_nReturn;

exception     
  when spiders_error then
      execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' SPIDERS error ' || lc_vErrorDescription || ''')';
      return -1;
  when json_error then
      execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' JSON error'')';
      return -2;
  when others then
      execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' dml error: ' || sqlerrm || ''')';
      return -3;
end;





function f_add_delivery_order_fac(in_cJson clob) return json as 
  /* template variables */
  json_error EXCEPTION;
  spiders_error EXCEPTION;
  lc_vErrorDescription varchar2(255) := 'default';
  lc_vFunctionName varchar2(255) := 'f_add_delivery_order_fac';
  lc_vRest varchar2(255) := 'f_add_delivery_order_fac';
  lc_jReturn json; 
  lc_nContinue number :=0; --default to 0 to dissalow continuing
  
  /* function specific variables */
  lc_vModule varchar2(255);
  lc_vTarget varchar2(255);
  lc_jParameterJson json;
  lc_vFACILITY_ID varchar2(255);
  lc_nDELIVERYORDERID number;
  lc_vPRODUCTLINE_LISTID number;
  lc_nSpidersFacilityId number;
  lc_jlData json_list;
  lc_jData json;
  lc_vFacilityAdded varchar2(255);
  lc_jTransformedData json;
  lc_vSql long;
  
  lc_jResults json;
  lc_jMessages json;
  lc_vDMLResult long;
  lc_jlDML json_list;
  lc_nDeliveryOrderFacExists number;
begin

  lc_jReturn := json();
  lc_jlDML := json_list();
  
  lc_jParameterJson := json(in_cJson);
  

  lc_vModule := getAttributeValue('module', lc_jParameterJson);
  lc_nContinue := f_test_for_error(lc_vModule);
  
  if lc_nContinue = 1 then
    lc_jlData := getJsonList('data', lc_jParameterJson);
    --lc_nContinue := f_test_for_error(lc_vFACILITY_ID);
  else
    lc_vErrorDescription := 'Module attribute not found';
    raise spiders_error;
  end if;
  
  if lc_nContinue = 1 then
    lc_jData := getJsonListItem('1', lc_jlData);
    --lc_nContinue := f_test_for_error(lc_vFACILITY_ID);
  else
    lc_vErrorDescription := 'Module attribute not found';
    raise spiders_error;
  end if;
  
  if lc_nContinue = 1 then
    lc_vFACILITY_ID := getAttributeValue('FACILITY_ID', lc_jData);
    lc_nContinue := f_test_for_error(lc_vFACILITY_ID);
  else
    lc_vErrorDescription := 'NFAID attribute not found';
    raise spiders_error;
  end if;
  
  if lc_nContinue = 1 then
    lc_nDELIVERYORDERID := getAttributeValue('DELIVERYORDERID', lc_jData);
    lc_nContinue := f_test_for_error(lc_nDELIVERYORDERID);
  else
    lc_vErrorDescription := 'FACILITY_ID attribute not found';
    raise spiders_error;
  end if;
  
  if lc_nContinue = 1 then
    lc_vPRODUCTLINE_LISTID := getAttributeValue('PRODUCTLINE_LISTID', lc_jData);
    lc_nContinue := f_test_for_error(lc_vPRODUCTLINE_LISTID);
  else
    lc_vErrorDescription := 'DELIVERYORDERID attribute not found';
    raise spiders_error;
  end if;
  
  if lc_nContinue = 1 then
    lc_nSpidersFacilityId := getSpidersFacilityId(lc_vFACILITY_ID);
    lc_nContinue := f_test_for_error(lc_nSpidersFacilityId);
  else
    lc_vErrorDescription := 'PRODUCTLINE_LISTID attribute not found';
    raise spiders_error;
  end if;
  
  if lc_nContinue = 1 then
    if lc_nSpidersFacilityId = 0 then
      lc_nSpidersFacilityId := getNextFacilityId;
      lc_nContinue := f_test_for_error(lc_nSpidersFacilityId);
      
      if lc_nContinue = 1 then
        lc_vSql := makeInsertFacilitySql(lc_nSpidersFacilityId,lc_jData);
        lc_jlDML.append(lc_vSql);
      else
        lc_vErrorDescription := 'PRODUCTLINE_LISTID attribute not found';
        raise spiders_error;
      end if;     
      
    else
      lc_nContinue := 1;
    end if;
  else
    lc_vErrorDescription := 'something went wrong with getSpidersFacilityId';
    raise spiders_error;
  end if;
  
  if lc_nContinue = 1 then
    lc_vSql := 'INSERT INTO SPD_T_DELIVERY_ORDER_FACS (DELIVERYORDERID,FACID) values (' || lc_nDELIVERYORDERID || ',' || lc_nSpidersFacilityId || ')';
    lc_jlDML.append(lc_vSql);
  else
    lc_vErrorDescription := 'Delivery Order Facility Already Exists';
    raise spiders_error;
  end if;


  if lc_nContinue = 1 then
    lc_jResults := json();
    lc_jResults.put('results',lc_jlDML);
    lc_jMessages := json();
    lc_vDMLResult := BB_F_DML(lc_jResults, lc_jMessages);
  else
    lc_vErrorDescription := 'something went wrong with the sql statement creation';
    raise spiders_error;
  end if;
  
  
/*
if lc_nContinue = 1 then
    lc_nSpidersFacilityCount := getFacilityCountForNFAID(lc_vNFAID);
    lc_nContinue := f_test_for_error(lc_nSpidersFacilityCount);
  else
    lc_vErrorDescription := 'PRODUCTLINE_LISTID attribute not found';
    raise spiders_error;
  end if;
*/
  
  lc_jReturn:= lc_jMessages;
  return lc_jReturn;
  

exception     
  when spiders_error then
      execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' SPIDERS error ' || lc_vErrorDescription || ''')';
      return json('{"error":"' || lc_vFunctionName || ' SPIDERS error ' || lc_vErrorDescription || '"}');
  when json_error then
      execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' JSON error'')';
      return json('{"error":"' || lc_vFunctionName || ' JSON error ' || lc_vErrorDescription || '"}');
  when others then
      execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' dml error: ' || sqlerrm || ''')';
     return json('{"error":"' || lc_vFunctionName || ' dml error ' || sqlerrm || '"}');
end; 

end;

/
