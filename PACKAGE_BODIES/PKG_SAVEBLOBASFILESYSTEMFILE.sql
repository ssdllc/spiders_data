--------------------------------------------------------
--  DDL for Package Body PKG_SAVEBLOBASFILESYSTEMFILE
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE PACKAGE BODY "SPIDERS_DATA"."PKG_SAVEBLOBASFILESYSTEMFILE" as

function pdf(in_vFlowsFilesName in varchar2, o_nFileValid out number) return varchar2 IS
  /* template variables */
  json_error EXCEPTION;
  spiders_error EXCEPTION;
  lc_vErrorDescription varchar2(255) := 'default';
  lc_vFunctionName varchar2(255) := 'pdf';
  lc_vReturn varchar2(32000) := '{"error":"default"}'; --defaul the return to e for error
  lc_nContinue number :=0; --default to 0 to dissalow continuing

  /* function specific variables */
  lc_vFilename varchar2(2550);
  lc_bBlob BLOB;
  lc_nStart NUMBER := 1;
  lc_nBytelen NUMBER := 32000;
  lc_nLen NUMBER;
  lc_rRaw RAW(32000);
  x NUMBER;

  l_output utl_file.file_type;
  lc_nSequence number :=0;
  lc_vMimeType varchar2(2000);
  lc_vFileType varchar2(2000);

begin

select
    wwv_flow_files.FILENAME,
    wwv_flow_files.BLOB_CONTENT,
    dbms_lob.getlength(wwv_flow_files.BLOB_CONTENT),
    wwv_flow_files.MIME_TYPE,
    wwv_flow_files.FILE_TYPE
  into
    lc_vFilename,
    lc_bBlob,
    lc_nLen,
    lc_vMimeType,
    lc_vFileType
from
    wwv_flow_files
where
    name = in_vFlowsFilesName;


select spd_s_xml_files.nextval into lc_nSequence from dual;
insert into spd_t_xml_files (xmlfileid, filename, blob_content, doc_size, mime_type, file_type) values (lc_nSequence, lc_vFilename,lc_bBlob,lc_nLen,lc_vMimeType,lc_vFileType);
commit;



if instr(lower(lc_vFilename),'.pdf') > 0 then
  lc_nContinue :=1;
end if;


if lc_nContinue = 1 then

  -- define output directory
  l_output := utl_file.fopen('SPD_PDFS', lc_vFilename,'wb', 32760);

  lc_nStart := 1;
  lc_nBytelen := 32000;

  -- save blob length
  x := lc_nLen;


  -- if small enough for a single write
  IF lc_nLen < 32760 THEN
      utl_file.put_raw(l_output,lc_bBlob);
      utl_file.fflush(l_output);
  ELSE -- write in pieces
      lc_nStart := 1;
      WHILE lc_nStart < lc_nLen and lc_nBytelen > 0
      LOOP
         dbms_lob.read(lc_bBlob,lc_nBytelen,lc_nStart,lc_rRaw);

         utl_file.put_raw(l_output,lc_rRaw);
         utl_file.fflush(l_output);

         -- set the start position for the next cut
         lc_nStart := lc_nStart + lc_nBytelen;

         -- set the end position if less than 32000 bytes
         x := x - lc_nBytelen;
         IF x < 32000 THEN
            lc_nBytelen := x;
         END IF;
      end loop;
  END IF;
  utl_file.fclose(l_output);

  o_nFileValid := 1;
else
  o_nFileValid := 0;
  lc_vErrorDescription := 'Continue not 1';
  raise spiders_error;
end if;

return lc_nSequence;

exception
  when spiders_error then
      execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' SPIDERS error ' || lc_vErrorDescription || ''')';
      return '{"error":"' || lc_vErrorDescription || '"}';
  when json_error then
      execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' JSON error'')';
      return '{"error":"json"}';
  when others then
      execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' dml error: ' || sqlerrm || ''')';
      return '{"error":"' || sqlerrm || '"}';
end;

--

function x3d(in_vFlowsFilesName in varchar2, o_nFileValid out number) return varchar2 IS
  /* template variables */
  json_error EXCEPTION;
  spiders_error EXCEPTION;
  lc_vErrorDescription varchar2(255) := 'default';
  lc_vFunctionName varchar2(255) := 'x3d';
  lc_vReturn varchar2(32000) := '{"error":"default"}'; --defaul the return to e for error
  lc_nContinue number :=0; --default to 0 to dissalow continuing

  /* function specific variables */
  lc_vFilename varchar2(2550);
  lc_bBlob BLOB;
  lc_nStart NUMBER := 1;
  lc_nBytelen NUMBER := 32000;
  lc_nLen NUMBER;
  lc_rRaw RAW(32000);
  x NUMBER;

  l_output utl_file.file_type;
  lc_nSequence number :=0;
  lc_vMimeType varchar2(2000);
  lc_vFileType varchar2(2000);

begin

select
    wwv_flow_files.FILENAME,
    wwv_flow_files.BLOB_CONTENT,
    dbms_lob.getlength(wwv_flow_files.BLOB_CONTENT),
    wwv_flow_files.MIME_TYPE,
    wwv_flow_files.FILE_TYPE
  into
    lc_vFilename,
    lc_bBlob,
    lc_nLen,
    lc_vMimeType,
    lc_vFileType
from
    wwv_flow_files
where
    name = in_vFlowsFilesName;


select spd_s_xml_files.nextval into lc_nSequence from dual;
insert into spd_t_xml_files (xmlfileid, filename, blob_content, doc_size, mime_type, file_type) values (lc_nSequence, lc_vFilename,lc_bBlob,lc_nLen,lc_vMimeType,lc_vFileType);
commit;


if instr(lower(lc_vFilename),'.x3d') > 0 then
  lc_nContinue :=1;
end if;
if instr(lower(lc_vFilename),'.jpg') > 0 then
  lc_nContinue :=1;
end if;
if instr(lower(lc_vFilename),'.wrl') > 0 then
  lc_nContinue :=1;
end if;
if instr(lower(lc_vFilename),'.wrz') > 0 then
  lc_nContinue :=1;
end if;
if instr(lower(lc_vFilename),'.x3db') > 0 then
  lc_nContinue :=1;
end if;



if lc_nContinue = 1 then

  -- define output directory
  l_output := utl_file.fopen('SPD_BINARYX3D', lc_vFilename,'wb', 32760);

  lc_nStart := 1;
  lc_nBytelen := 32000;

  -- save blob length
  x := lc_nLen;


  -- if small enough for a single write
  IF lc_nLen < 32760 THEN
      utl_file.put_raw(l_output,lc_bBlob);
      utl_file.fflush(l_output);
  ELSE -- write in pieces
      lc_nStart := 1;
      WHILE lc_nStart < lc_nLen and lc_nBytelen > 0
      LOOP
         dbms_lob.read(lc_bBlob,lc_nBytelen,lc_nStart,lc_rRaw);

         utl_file.put_raw(l_output,lc_rRaw);
         utl_file.fflush(l_output);

         -- set the start position for the next cut
         lc_nStart := lc_nStart + lc_nBytelen;

         -- set the end position if less than 32000 bytes
         x := x - lc_nBytelen;
         IF x < 32000 THEN
            lc_nBytelen := x;
         END IF;
      end loop;
  END IF;
  utl_file.fclose(l_output);

  o_nFileValid := 1;
else
  o_nFileValid := 0;
  lc_vErrorDescription := 'Continue not 1';
  raise spiders_error;
end if;

return lc_nSequence;

exception
  when spiders_error then
      execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' SPIDERS error ' || lc_vErrorDescription || ''')';
      return '{"error":"' || lc_vErrorDescription || '"}';
  when json_error then
      execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' JSON error'')';
      return '{"error":"json"}';
  when others then
      execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' dml error: ' || sqlerrm || ''')';
      return '{"error":"' || sqlerrm || '"}';
end;

--

function zip(in_vFlowsFilesName in varchar2, o_nFileValid out number) return varchar2 IS
  /* template variables */
  json_error EXCEPTION;
  spiders_error EXCEPTION;
  lc_vErrorDescription varchar2(255) := 'default';
  lc_vFunctionName varchar2(255) := 'zip';
  lc_vReturn varchar2(32000) := '{"error":"default"}'; --defaul the return to e for error
  lc_nContinue number :=0; --default to 0 to dissalow continuing

  /* function specific variables */
  lc_vFilename varchar2(2550);
  lc_bBlob BLOB;
  lc_nStart NUMBER := 1;
  lc_nBytelen NUMBER := 32000;
  lc_nLen NUMBER;
  lc_rRaw RAW(32000);
  x NUMBER;

  l_output utl_file.file_type;
  lc_nSequence number :=0;
  lc_vMimeType varchar2(2000);
  lc_vFileType varchar2(2000);

begin

select
    wwv_flow_files.FILENAME,
    wwv_flow_files.BLOB_CONTENT,
    dbms_lob.getlength(wwv_flow_files.BLOB_CONTENT),
    wwv_flow_files.MIME_TYPE,
    wwv_flow_files.FILE_TYPE
  into
    lc_vFilename,
    lc_bBlob,
    lc_nLen,
    lc_vMimeType,
    lc_vFileType
from
    wwv_flow_files
where
    name = in_vFlowsFilesName;


select spd_s_xml_files.nextval into lc_nSequence from dual;
insert into spd_t_xml_files (xmlfileid, filename, blob_content, doc_size, mime_type, file_type) values (lc_nSequence, lc_vFilename,lc_bBlob,lc_nLen,lc_vMimeType,lc_vFileType);
commit;



if instr(lower(lc_vFilename),'.zip') > 0 then
  lc_nContinue :=1;
end if;


if lc_nContinue = 1 then
/*
  -- define output directory
  l_output := utl_file.fopen('SPD_OTHER', lc_vFilename,'wb', 32760);

  lc_nStart := 1;
  lc_nBytelen := 32000;

  -- save blob length
  x := lc_nLen;


  -- if small enough for a single write
  IF lc_nLen < 32760 THEN
      utl_file.put_raw(l_output,lc_bBlob);
      utl_file.fflush(l_output);
  ELSE -- write in pieces
      lc_nStart := 1;
      WHILE lc_nStart < lc_nLen and lc_nBytelen > 0
      LOOP
         dbms_lob.read(lc_bBlob,lc_nBytelen,lc_nStart,lc_rRaw);

         utl_file.put_raw(l_output,lc_rRaw);
         utl_file.fflush(l_output);

         -- set the start position for the next cut
         lc_nStart := lc_nStart + lc_nBytelen;

         -- set the end position if less than 32000 bytes
         x := x - lc_nBytelen;
         IF x < 32000 THEN
            lc_nBytelen := x;
         END IF;
      end loop;
  END IF;
  utl_file.fclose(l_output);
*/
  o_nFileValid := 1;
else
  o_nFileValid := 0;
  lc_vErrorDescription := 'Continue not 1';
  raise spiders_error;
end if;

return lc_nSequence;

exception
  when spiders_error then
      execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' SPIDERS error ' || lc_vErrorDescription || ''')';
      return '{"error":"' || lc_vErrorDescription || '"}';
  when json_error then
      execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' JSON error'')';
      return '{"error":"json"}';
  when others then
      execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' dml error: ' || sqlerrm || ''')';
      return '{"error":"' || sqlerrm || '"}';
end;

--

function jpg(in_vFlowsFilesName in varchar2, o_nFileValid out number) return varchar2 IS
  /* template variables */
  json_error EXCEPTION;
  spiders_error EXCEPTION;
  lc_vErrorDescription varchar2(255) := 'default';
  lc_vFunctionName varchar2(255) := 'jpg';
  lc_vReturn varchar2(32000) := '{"error":"default"}'; --defaul the return to e for error
  lc_nContinue number :=0; --default to 0 to dissalow continuing

  /* function specific variables */
  lc_vFilename varchar2(2550);
  lc_bBlob BLOB;
  lc_nStart NUMBER := 1;
  lc_nBytelen NUMBER := 32000;
  lc_nLen NUMBER;
  lc_rRaw RAW(32000);
  x NUMBER;

  l_output utl_file.file_type;
  lc_nSequence number :=0;
  lc_vMimeType varchar2(2000);
  lc_vFileType varchar2(2000);

begin

select
    wwv_flow_files.FILENAME,
    wwv_flow_files.BLOB_CONTENT,
    dbms_lob.getlength(wwv_flow_files.BLOB_CONTENT),
    wwv_flow_files.MIME_TYPE,
    wwv_flow_files.FILE_TYPE
  into
    lc_vFilename,
    lc_bBlob,
    lc_nLen,
    lc_vMimeType,
    lc_vFileType
from
    wwv_flow_files
where
    name = in_vFlowsFilesName;


select spd_s_xml_files.nextval into lc_nSequence from dual;
insert into spd_t_xml_files (xmlfileid, filename, blob_content, doc_size, mime_type, file_type) values (lc_nSequence, lc_vFilename,lc_bBlob,lc_nLen,lc_vMimeType,lc_vFileType);
commit;



if instr(lower(lc_vFilename),'.jpg') > 0 then
  lc_nContinue :=1;
end if;
if instr(lower(lc_vFilename),'.jpeg') > 0 then
  lc_nContinue :=1;
end if;
if instr(lower(lc_vFilename),'.png') > 0 then
  lc_nContinue :=1;
end if;
if instr(lower(lc_vFilename),'.gif') > 0 then
  lc_nContinue :=1;
end if;

if lc_nContinue = 1 then

  -- define output directory
  l_output := utl_file.fopen('SPD_THUMBNAILS', lc_vFilename,'wb', 32760);

  lc_nStart := 1;
  lc_nBytelen := 32000;

  -- save blob length
  x := lc_nLen;


  -- if small enough for a single write
  IF lc_nLen < 32760 THEN
      utl_file.put_raw(l_output,lc_bBlob);
      utl_file.fflush(l_output);
  ELSE -- write in pieces
      lc_nStart := 1;
      WHILE lc_nStart < lc_nLen and lc_nBytelen > 0
      LOOP
         dbms_lob.read(lc_bBlob,lc_nBytelen,lc_nStart,lc_rRaw);

         utl_file.put_raw(l_output,lc_rRaw);
         utl_file.fflush(l_output);

         -- set the start position for the next cut
         lc_nStart := lc_nStart + lc_nBytelen;

         -- set the end position if less than 32000 bytes
         x := x - lc_nBytelen;
         IF x < 32000 THEN
            lc_nBytelen := x;
         END IF;
      end loop;
  END IF;
  utl_file.fclose(l_output);

  o_nFileValid := 1;
else
  o_nFileValid := 0;
  lc_vErrorDescription := 'Continue not 1 and filename='||lc_vFilename;
  raise spiders_error;
end if;

return lc_nSequence;

exception
  when spiders_error then
      execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' SPIDERS error ' || lc_vErrorDescription || ''')';
      return '{"error":"' || lc_vErrorDescription || '"}';
  when json_error then
      execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' JSON error'')';
      return '{"error":"json"}';
  when others then
      execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' dml error: ' || sqlerrm || ''')';
      return '{"error":"' || sqlerrm || '"}';
end;


end;

/
