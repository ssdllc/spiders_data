--------------------------------------------------------
--  File created - Tuesday-August-21-2018   
--------------------------------------------------------
--------------------------------------------------------
--  DDL for Package Body PKG_XML_PARSE
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE PACKAGE BODY "SPIDERS_DATA"."PKG_XML_PARSE" AS 

lg_vPackageName varchar2(20) := 'PKG_XML_PARSE.';

function getCorrespondingJsonColumnVal(in_jJson json, in_vColumn varchar2) return varchar2 as 
  /* template variables */
  json_error EXCEPTION;
  spiders_error EXCEPTION;
  lc_vErrorDescription varchar2(255) := 'default';
  lc_vFunctionName varchar2(255) := 'getCorrespondingJsonColumnVal';
  lc_vReturn varchar2(32000) := '{"error":"default"}'; --defaul the return to e for error
  lc_nContinue number :=0; --default to 0 to dissalow continuing
  
  /* function specific variables */
  lc_vName varchar2(2000);
  lc_vTemp varchar2(2000);
  lc_vComment varchar2(2000);
  lc_jvValue json_value;
  lc_jlData json_list;
  lc_jvRecVal json_value;
  lc_jValue json;
begin

if in_vColumn not in ('UPDATED_TIMESTAMP','UPDATED_BY','CREATED_TIMESTAMP','CREATED_BY') then
  /* i could not convince myself that we needed multiple functions here, since we still need to check if attributes exist in the sub functions*/
  if(in_jJson.exist('data')) then
      if(in_jJson.get('data').is_array) then
          lc_jlData := json_list(in_jJson.get('data'));        
          lc_jvRecVal := lc_jlData.get(1); --return null on outofbounds
          if (lc_jvRecVal is not null) then
            lc_jValue := json(lc_jvRecVal);            
            if(lc_jValue.exist(in_vColumn)) then 
              lc_jvValue := lc_jValue.get(in_vColumn);
              lc_vReturn := getVal(lc_jvValue);
            else
              lc_vErrorDescription := in_vColumn || ' attribute not found in json';
              return '{"error":"'||lc_vErrorDescription||'"}';
            end if;
          else
            lc_vErrorDescription := 'Rec val is null';
            return '{"error":"'||lc_vErrorDescription||'"}';
          end if;
      else
        lc_vErrorDescription := 'Data is not an array';
        return '{"error":"'||lc_vErrorDescription||'"}';
      end if;
  else
    lc_vErrorDescription := 'data does not exist!';
    return '{"error":"'||lc_vErrorDescription||'"}';
  end if;
else
  lc_vErrorDescription := 'in_vColumn is and UPDATED or CREATED column';
  return '{"error":"'||lc_vErrorDescription||'"}';
end if;  

return lc_vReturn;

exception     
  when others then
      execute immediate 'insert into debug (FUNCTIONNAME, INPUT, MESSAGE) values (''' || lg_vPackageName || lc_vFunctionName || ''', '''||  'Something when wrong when you tried to get '||in_vColumn|| ' out of the JSON ('|| lc_vErrorDescription ||')'|| ''',''' || 'sqlerrm: ' || sqlerrm || ''')';
      return '{"error":"'||lc_vErrorDescription||'"}';
end; 

--------------------------------------------------------------------------------------------------------------------------------
FUNCTION f_test_for_error (in_vReturned json) RETURN varchar2 AS 
	/* template variables */
	lc_vFunctionName varchar2(255) := 'f_test_for_error';
	lc_vReturn varchar2(32000) := '{"error":"default"}'; --defaul the return to e for error
	/* function specific variables */
	--lc_vTest varchar2(32000) := in_vReturned;
	
	BEGIN
  --!!NOT REALLY TESTING THIS YET, PLEASE FIX CAUSE IM LAZY
  lc_vReturn := '1';
		/*
    IF substr(lc_vTest, 1, 9) = '{"error":' THEN
			lc_vReturn := '0';
		ELSE
			lc_vReturn := '1';
		END IF;
    */
		
	RETURN lc_vReturn;

	EXCEPTION WHEN OTHERS THEN
		execute immediate 'insert into debug (FUNCTIONNAME, INPUT, MESSAGE) values (''' || lg_vPackageName || lc_vFunctionName || ''', '|| 'An error occured in the guard function for varchar'|| ''','''|| sqlerrm ||''')';
		RETURN '0';
END f_test_for_error;


FUNCTION f_test_for_error (in_vReturned varchar2) RETURN varchar2 AS 
	/* template variables */
	lc_vFunctionName varchar2(255) := 'f_test_for_error';
	lc_vReturn varchar2(32000) := '{"error":"default"}'; --defaul the return to e for error
	/* function specific variables */
	lc_vTest varchar2(32000) := in_vReturned;
	
	BEGIN

		IF substr(lc_vTest, 1, 9) = '{"error":' THEN
			lc_vReturn := '0';
		ELSE
			lc_vReturn := '1';
		END IF;
		
	RETURN lc_vReturn;

	EXCEPTION WHEN OTHERS THEN
		execute immediate 'insert into debug (FUNCTIONNAME, INPUT, MESSAGE) values (''' || lg_vPackageName || lc_vFunctionName || ''', '|| 'An error occured in the guard function for varchar'|| ''','''|| sqlerrm ||''')';
		RETURN '0';
END f_test_for_error;

--------------------------------------------------------------------------------------------------------------------------------

FUNCTION f_test_for_error (in_vReturned number) RETURN varchar2 AS 
	lc_vFunctionName varchar2(255) := 'f_test_for_error';
	lc_vReturn number := 0; --defaul the return to e for error
	
	BEGIN

		IF in_vReturned = -1 THEN
		lc_vReturn := '0';
		ELSE
		lc_vReturn := '1';
		END IF;
		
	RETURN lc_vReturn;

	EXCEPTION WHEN OTHERS THEN
		execute immediate 'insert into debug (FUNCTIONNAME, INPUT, MESSAGE) values (''' ||  lg_vPackageName ||lc_vFunctionName || ''', '|| 'An error occured in the guard function for number'|| ''','''|| sqlerrm ||''')';
		RETURN '0';
END f_test_for_error;

--------------------------------------------------------------------------------------------------------------------------------

FUNCTION getMaxLevel(in_vXPath varchar2, in_vUser varchar2, in_nADOID number) RETURN number AS
	lc_Return number := -1;
	lc_vFunctionName varchar2(255) := 'getMaxLevel';
	
	BEGIN

	FOR rec in (SELECT MAX(XMLLEVEL) MAXLEVEL FROM SPD_T_XML_FLATTEN WHERE xpath LIKE in_vXPath||'%' AND CREATED_BY = in_vUser AND ADOID = in_nADOID ) LOOP
    lc_Return := rec.MAXLEVEL;
  END LOOP;
  
	RETURN lc_Return;

	EXCEPTION WHEN OTHERS THEN
		execute immediate 'insert into debug (FUNCTIONNAME, INPUT, MESSAGE) values (''' || lg_vPackageName || lc_vFunctionName || ''' , '''|| 'Something went wrong while getting the Max Level of '||in_vXPath||''',''' || sqlerrm || ''')';
		return -1;
END getMaxLevel;

--------------------------------------------------------------------------------------------------------------------------------

FUNCTION getExceptions(in_vTableName varchar2) RETURN json AS
	lc_Return json;
	lc_vFunctionName varchar2(255);
	
	BEGIN

	lc_Return := json('{}');
	
	FOR rec IN (SELECT * FROM SPD_T_XML_COLUMN_EXCEPTION WHERE TABLE_NAME = in_vTableName) LOOP
		lc_Return.put(UPPER(rec.NODE_NAME), UPPER(rec.COLUMN_NAME));
	END LOOP;
	
	RETURN lc_Return;

	EXCEPTION WHEN OTHERS THEN
		execute immediate 'insert into debug (FUNCTIONNAME, INPUT, MESSAGE) values (''' ||  lg_vPackageName ||lc_vFunctionName || ''','''|| 'An error occured while requesting Column/Node Exception (SPD_T_XML_COLUMN_EXCEPTION) for the table '|| in_vTableName ||''', ''' || sqlerrm || ''')';
		return json('{"error":"' || sqlerrm || '"}');
END getExceptions;

--------------------------------------------------------------------------------------------------------------------------------

FUNCTION getTableName(in_vFPath varchar2) RETURN varchar2 AS
	lc_vTableName varchar2(255) := '{"error":"default"}'; --defaul the return to e for error
	lc_vFunctionName varchar2(255) := 'getTableName';
	
	BEGIN

	FOR rec in (SELECT TABLE_NAME FROM SPD_T_XML_PATH_TABLE_MAPS WHERE FPATH = in_vFPath)LOOP
    lc_vTableName := rec.TABLE_NAME;
  END LOOP;
  
	RETURN lc_vTableName;

	EXCEPTION WHEN OTHERS THEN
		execute immediate 'insert into debug (FUNCTIONNAME, INPUT, MESSAGE) values (''' ||  lg_vPackageName ||lc_vFunctionName || ''','''|| 'An error occured while getting the Table Name in SPD_T_XML_PATH_TABLE_MAPS for the FPATH : ' ||in_vFPath||''',''' || sqlerrm || ''')';
		return '{"error":"default"}';
END getTableName;

--------------------------------------------------------------------------------------------------------------------------------

FUNCTION getTablePrimaryKeyColumn(in_vTableName varchar2) RETURN varchar2 AS 
	/* template variables */
	lc_vFunctionName varchar2(255) := 'getTablePrimaryKeyColumn';
	lc_vReturn varchar2(32000) := '{"error":"default"}'; --defaul the return to e for error
	lc_nContinue number :=0; --default to 0 to dissalow continuing
	 /* function specific variables */
	lc_vUser varchar2(25) := 'SPIDERS_DATA';
	
	BEGIN

	FOR rec in (SELECT 
		cols.column_name COLUMN_NAME
	FROM 
		all_constraints cons, 
		all_cons_columns cols 
	 WHERE 
		cons.owner = lc_vUser
		AND cons.constraint_type = 'P' 
		AND cons.constraint_name = cols.constraint_name 
		AND cons.owner = cols.owner 
		AND cons.table_name = in_vTableName) LOOP
  
  lc_vReturn := rec.COLUMN_NAME;
    
  END LOOP;

	RETURN lc_vReturn;

	EXCEPTION WHEN OTHERS THEN
		 execute immediate 'insert into debug (FUNCTIONNAME, INPUT, MESSAGE) values (''' ||  lg_vPackageName ||lc_vFunctionName || ''','''|| 'An error occured while getting the Primary key for the table : '||in_vTableName ||''','''|| sqlerrm || ''')';
		 return '{"error":"' || sqlerrm || '"}';
END getTablePrimaryKeyColumn; 

--------------------------------------------------------------------------------------------------------------------------------

FUNCTION setAttributes(in_nCurrentLevel number, in_vXPath varchar2, in_jExceptions json, in_jParentId json, in_vUser varchar2, in_nADOID number) RETURN json AS 
	lc_vFunctionName varchar2(255) := 'setAttributes';

	lc_jAttributesValues json;
	lc_vErrorDescription varchar2(255);

	BEGIN
  
  lc_jAttributesValues := json('{}');

	-- Set the parent ID
	IF in_jParentId IS NOT NULL THEN
		lc_jAttributesValues := in_jParentId;
	ELSE
		lc_jAttributesValues := json('{}');
	END IF;

	 -- Set attribute name according to the exceptions
	FOR rec2 IN (SELECT UPPER(NODE_NAME) NODE_NAME, NODE_VALUE FROM SPD_T_XML_FLATTEN WHERE XMLLEVEL = in_nCurrentLevel+1 AND  xpath LIKE in_vXPath||'%' AND CREATED_BY = in_vUser AND ADOID = in_nADOID ORDER BY NODE_POSITION)LOOP
		BEGIN
        /*
        insert into debug (functionname, input) values ('pkg_xml_parse.setAttributes',rec2.node_name || ' (' || rec2.node_value || ')');
        commit;
        */
			IF in_jExceptions.exist(rec2.NODE_NAME) THEN
				lc_jAttributesValues.put(getVal(in_jExceptions.get(rec2.NODE_NAME)), rec2.NODE_VALUE);
			ELSE
				lc_jAttributesValues.put(rec2.NODE_NAME,rec2.NODE_VALUE);
			END IF;
		EXCEPTION WHEN OTHERS THEN 
      execute immediate 'insert into debug (FUNCTIONNAME, INPUT, MESSAGE) values (''' ||  lg_vPackageName ||lc_vFunctionName || ''',''' || 'Something happend while setting the attribute '|| rec2.NODE_NAME ||''',''' || sqlerrm || ''')';
		END;
	END LOOP;


	RETURN lc_jAttributesValues;

	EXCEPTION WHEN OTHERS THEN
	 	execute immediate 'insert into debug (FUNCTIONNAME, INPUT, MESSAGE) values (''' ||  lg_vPackageName ||lc_vFunctionName || ''','''|| 'An error occured while setting the attribute for the path : '|| in_vXPath ||''',''' || sqlerrm || ''')';
	 	lc_jAttributesValues:= json('{"error":"default"}');
    return lc_jAttributesValues;
END; 

--------------------------------------------------------------------------------------------------------------------------------
    
FUNCTION f_insert_siblings(in_vXPath varchar2, in_jParent json, in_vUser varchar2, in_nADOID number) return varchar2 as
  lc_vFunctionName varchar2(255) := 'f_insert_siblings';
	lc_vTempTableName varchar2(255);
	lc_vTableName varchar2(255);
	lc_vXPath varchar2(255);
	lc_nContinue number :=0; --default to 0 to dissalow continuing
	lc_vPrimaryKey varchar2(255);
  lc_jJson json;
  lc_jlJson json_list;

	lc_jAttributesValues json;
	lc_nFakeId number;

	lc_vSQL varchar2(4000) := '{"error":"default"}';

	lc_jlSQL json_list := json_list();

	BEGIN

	FOR rec in (SELECT DISTINCT(FPATH), XPATH FROM SPD_T_XML_FLATTEN WHERE XPATH LIKE in_vXPath||'/%' AND XPATH NOT LIKE '%@%' AND CREATED_BY = in_vUser AND ADOID = in_nADOID )LOOP
		lc_vTempTableName := getTableName(rec.FPATH);
		lc_nContinue :=  f_test_for_error(lc_vTableName);
		
		IF lc_nContinue = 1 THEN
			lc_vTableName := lc_vTempTableName;
			lc_vXPath := rec.XPATH;
			EXIT;
		END IF;
		
	END LOOP;

	FOR rec2 in (SELECT XPATH FROM SPD_T_XML_FLATTEN WHERE XPATH LIKE in_vXPath||'/%' AND XPATH NOT LIKE '%@%' AND CREATED_BY = in_vUser AND ADOID = in_nADOID )LOOP
		lc_jAttributesValues := in_jParent;
		lc_vPrimaryKey := getTablePrimaryKeyColumn('SPD_T_'||lc_vTableName);
		lc_jAttributesValues.put(lc_vPrimaryKey, '');
		
		FOR rec3 in (SELECT UPPER(NODE_NAME) NODE_NAME, NODE_VALUE FROM SPD_T_XML_FLATTEN WHERE NODE_VALUE IS NOT NULL AND xpath LIKE rec2.XPATH||'%' AND CREATED_BY = in_vUser AND ADOID = in_nADOID ORDER BY NODE_POSITION)LOOP
			
			lc_jAttributesValues.put(rec3.NODE_NAME, rec3.NODE_VALUE);
			
		END LOOP;
		
    lc_jJSON := json();
    lc_jlJson := json_list();
    lc_jlJson.append(lc_jAttributesValues.to_json_value);
    lc_jJson.put('data',lc_jlJson);
    
		--lc_jJson := json('{data:['|| lc_jAttributesValues.to_char() ||']}');
		lc_vSQL := SQL_STATEMENT_FUNCTIONS.f_insert(lc_jJson, lc_vTableName, lc_nFakeId);
		lc_jlSQL.append(lc_vSQL);

	END LOOP;

	executeSQL(lc_jlSQL);

	RETURN null;
  
  EXCEPTION WHEN OTHERS THEN 
    execute immediate 'insert into debug (FUNCTIONNAME, INPUT, MESSAGE) values (''' ||  lg_vPackageName ||lc_vFunctionName || ''','''|| 'An error occured while inserting siblings for path that start by '||in_vXPath ||''',''' || sqlerrm || ''')';
    return '{"error":"default"}';
END;

--------------------------------------------------------------------------------------------------------------------------------

FUNCTION getSiblingCount(in_nCurrentLevel number, in_vXPath varchar2, in_vUser varchar2, in_nADOID number) RETURN number AS
  lc_vFunctionName varchar2(255) := 'getSiblingCount';
  lc_vReturn varchar2(32000) := '{"error":"default"}'; --defaul the return to e for error
      
  lc_jAttributesValues json;
  lc_vErrorDescription varchar2(255);
  lc_Return number := 0;
  
  lc_jAttributes json := json('{}');
  lc_nBool number := 0;
  
	BEGIN

		FOR rec IN (SELECT UPPER(NODE_NAME) NODE_NAME, NODE_VALUE FROM SPD_T_XML_FLATTEN WHERE XMLLEVEL = in_nCurrentLevel+1 AND node_value IS NOT NULL AND xpath LIKE in_vXPath||'%' AND CREATED_BY = in_vUser AND ADOID = in_nADOID ORDER BY NODE_POSITION)LOOP
		  IF (lc_jAttributes.exist(rec.NODE_NAME)) THEN
		  -- it is a sibling
		  lc_nBool := 1;
		  ELSE
			lc_jAttributes.put(rec.NODE_NAME, rec.NODE_VALUE);
		  END IF;
		END LOOP;
	
		IF lc_nBool = 1 THEN
		  SELECT COUNT(node_name) INTO lc_Return FROM SPD_T_XML_FLATTEN WHERE XMLLEVEL = in_nCurrentLevel+1 AND xpath LIKE in_vXPath||'%' AND NODE_VALUE IS NOT NULL AND CREATED_BY = in_vUser AND ADOID = in_nADOID ORDER BY NODE_POSITION;
		END IF;
  
	  RETURN lc_Return;
  
	EXCEPTION WHEN OTHERS THEN
    execute immediate 'insert into debug (FUNCTIONNAME, INPUT, MESSAGE) values (''' || lg_vPackageName || lc_vFunctionName || ''','''|| 'An error occured while counting the number of siblings for paths that start by '||in_vXPath ||''',''' || sqlerrm || ''')';
    RETURN 0;
END;

--------------------------------------------------------------------------------------------------------------------------------

PROCEDURE extractDataFromFlattenTable(in_nXMLLevel number, in_vXPath varchar2, in_jParentId json, in_vUser varchar2, in_nADOID number) AS
  lc_vFunctionName varchar2(255) := 'extractDataFromFlattenTable';

begin


  begin
    insert /*+ append */ into spd_t_xml_actions (
      XMLACTIONID,
      ESFILE,
      ACTIONID,
      CONTRACTNUMBER,
      DELIVERYORDERNUMBER,
      FUNDINGSOURCE,
      REPORTNUMBER,
      CONTRACTORCOMPANY,
      ADDRESS1,
      ADDRESS2,
      CITY,
      ADDRESSSTATE,
      ZIPCODE,
      ENGINEEROFRECORD,
      EORCONTACTEMAIL,
      EORPHONENUMBER,
      ONSITESTARTDATE,
      ONSITEENDDATE,
      SUBMITTALDATE,
      ACTIVITYNAME,
      ACTCITY,
      ACTSTATE,
      ACTZIPCODE,
      ACTIVITYUIC,
      CREATED_BY,
      ADOID,
      DATAREQREVISION)
    
    SELECT 
        XMLACTIONID,
        ESFILE,
        ACTIONID,
        CONTRACTNUMBER,
        DELIVERYORDERNUMBER,
        FUNDINGSOURCE,
        REPORTNUMBER,
        CONTRACTORCOMPANY,
        ADDRESS1,
        ADDRESS2,
        CITY,
        ADDRESSSTATE,
        ZIPCODE,
        ENGINEEROFRECORD,
        EORCONTACTEMAIL,
        EORPHONENUMBER,
        ONSITESTARTDATE,
        ONSITEENDDATE,
        SUBMITTALDATE,
        ACTIVITYNAME,
        ACTCITY,
        ACTSTATE,
        ACTZIPCODE,
        ACTIVITYUIC,
        CREATED_BY,
        ADOID,
        DATAREQREVISION
      FROM TABLE(pkg_rowify.f_XML_ACTION(0,in_nADOID, in_nADOID,in_vUser));
    exception when others then
      htp.p('error: ' || sqlerrm);
    end;
    
    insertLog2(in_nADOID,'EXTRACTING', 'PROCESSING',1,24);
  begin    
--insert action drawings
insert /*+ append */ into spd_t_xml_action_drawings (
  XMLACTIONID,
	NAVFACDRAWINGNUMBER,
	DESCRIPTION,
	DRAWINGTYPE,
	FILENAME,
	ACT_DRAWINGID,
	CREATED_BY)
select
  XMLACTIONID,
	NAVFACDRAWINGNUMBER,
	DESCRIPTION,
	DRAWINGTYPE,
	FILENAME,
	ACT_DRAWINGID,
	CREATED_BY
FROM TABLE(pkg_rowify.f_XML_ACTION_DRAWING(0,in_nADOID, in_nADOID,in_vUser));
    exception when others then
      htp.p('error: ' || sqlerrm);
    end;
    insertLog2(in_nADOID,'EXTRACTING', 'PROCESSING',2,24);
  begin    
--insert action photos
insert /*+ append */ into spd_t_xml_action_photos (
  PHOTOID,
	XMLACTIONID,
	UNIFORMATIICODE,
	IMAGEFILENAME,
	CAPTION,
	PHOTOTYPE,
	CREATED_BY)
select
  PHOTOID,
	XMLACTIONID,
	UNIFORMATIICODE,
	IMAGEFILENAME,
	CAPTION,
	PHOTOTYPE,
	CREATED_BY
FROM TABLE(pkg_rowify.f_XML_ACTION_PHOTO(0,in_nADOID, in_nADOID,in_vUser));
    exception when others then
      htp.p('error: ' || sqlerrm);
    end;
    
    insertLog2(in_nADOID,'EXTRACTING', 'PROCESSING',3,24);
  begin
--insert action supporting files
insert /*+ append */ into spd_t_xml_actsupport_files (
  FILEID,
	XMLACTIONID,
	SUPPORTINGFILEID,
	FILENAME,
	DESCRIPTION,
	CREATED_BY)
select
  FILEID,
	XMLACTIONID,
	SUPPORTINGFILEID,
	FILENAME,
	DESCRIPTION,
	CREATED_BY
FROM TABLE(pkg_rowify.f_XML_ACTSUPPORT_FILE(0,in_nADOID, in_nADOID,in_vUser));
    exception when others then
      htp.p('error: ' || sqlerrm);
    end;
    
    insertLog2(in_nADOID,'EXTRACTING', 'PROCESSING',4,24);

  begin
--insert facility
insert /*+ append */ into spd_t_xml_facilities (
  FACILITYID,
	XMLACTIONID,
	FSFILENAME,
	ONSITESTARTDATE,
	ONSITEENDDATE,
	SUBMITTALDATE,
	FACILITYNAME,
	FACILITYNO,
	NFAID,
	SPIDERS_FACILITYID,
	COSTESTIMATEFILENAME,
	DD1391FILENAME,
	DELIVERYORDERFACILITYID,
	CREATED_BY)
select
  FACILITYID,
	XMLACTIONID,
	FSFILENAME,
	ONSITESTARTDATE,
	ONSITEENDDATE,
	SUBMITTALDATE,
	FACILITYNAME,
	FACILITYNO,
	NFAID,
	SPIDERS_FACILITYID,
	COSTESTIMATEFILENAME,
	DD1391FILENAME,
	DELIVERYORDERFACILITYID,
	CREATED_BY
FROM TABLE(pkg_rowify.f_XML_FACILITY(0,in_nADOID, in_nADOID,in_vUser));
    exception when others then
      htp.p('error: ' || sqlerrm);
    end;
    
    insertLog2(in_nADOID,'EXTRACTING', 'PROCESSING',5,24);

  begin
--insert exit brief  !!!issue
insert /*+ append */ into spd_t_xml_exit_briefs (
  EXITBRIEFID,
	XMLACTIONID,
	EXITBRIEFTIME,
	EXITBRIEFLOC,
	EXITBRIEFDATE,
	CREATED_BY
)
select
  EXITBRIEFID,
	XMLACTIONID,
	EXITBRIEFTIME,
	EXITBRIEFLOC,
	EXITBRIEFDATE,
	CREATED_BY
FROM TABLE(pkg_rowify.f_XML_EXIT_BRIEF(0,in_nADOID, in_nADOID,in_vUser));
    exception when others then
      htp.p('error: ' || sqlerrm);
    end;
    
    insertLog2(in_nADOID,'EXTRACTING', 'PROCESSING',6,24);

  begin
--insert executive table
insert /*+ append */ into spd_t_xml_exec_tables (
  EXECTABLEID,
	FACILITYID,
	FACNAME,
	FACID,
	NFAID,
	PRN,
	INSPECTYPE,
	INSPECTED,
	ENGRATING,
	CIRATING,
	FIVEYEARCI,
	TENYEARCI,
	OPRATING,
	RESTRICTIONS,
	LOADING,
	MOORING,
	BERTHING,
	REPAIRREC,
	USAGEDESC,
	CREATED_BY
)
Select
  EXECTABLEID,
	FACILITYID,
	FACNAME,
	FACID,
	NFAID,
	PRN,
	INSPECTYPE,
	INSPECTED,
	ENGRATING,
	CIRATING,
	FIVEYEARCI,
	TENYEARCI,
	OPRATING,
	RESTRICTIONS,
	LOADING,
	MOORING,
	BERTHING,
	REPAIRREC,
	USAGEDESC,
	CREATED_BY
FROM TABLE(pkg_rowify.f_XML_EXEC_TABLE(0,in_nADOID, in_nADOID,in_vUser));
    exception when others then
      htp.p('error: ' || sqlerrm);
    end;
    
    insertLog2(in_nADOID,'EXTRACTING', 'PROCESSING',7,24);

begin
---insert asset summaries
insert /*+ append */ into spd_t_xml_asset_summary (
  LINEITEMID,
	FACILITYID,
	UNIFORMATIICODE,
	DESCRIPTION,
	ASSETTYPE,
	FACILITYSECTION,
	CRITICALITYPERCENTAGE,
	NOOFASSETSCOUNT,
	QUANTITYOFUNITS,
	UNITTYPE,
	CURRENTCI,
	APPROXIMATEREPAIRCOSTS,
	FIVEYEARCIIFREPAIRED,
	TENYEARCIIFREPAIRED,
	FIVEYEARCIIFNOTREPAIRED,
	TENYEARCIIFNOTREPAIRED,
	CREATED_BY      
)
select
  LINEITEMID,
	FACILITYID,
	UNIFORMATIICODE,
	DESCRIPTION,
	ASSETTYPE,
	FACILITYSECTION,
	CRITICALITYPERCENTAGE,
	NOOFASSETSCOUNT,
	QUANTITYOFUNITS,
	UNITTYPE,
	CURRENTCI,
	APPROXIMATEREPAIRCOSTS,
	FIVEYEARCIIFREPAIRED,
	TENYEARCIIFREPAIRED,
	FIVEYEARCIIFNOTREPAIRED,
	TENYEARCIIFNOTREPAIRED,
	CREATED_BY
FROM TABLE(pkg_rowify.f_XML_ASSET_SUMMARY(0,in_nADOID, in_nADOID,in_vUser));
    exception when others then
      htp.p('error: ' || sqlerrm);
    end;
    
    insertLog2(in_nADOID,'EXTRACTING', 'PROCESSING',8,24);

  begin
-----insert asset inventory
insert /*+ append */ into spd_t_xml_asset_inventory (
  INVENTORYID,
	LINEITEMID,
	UNIFORMATIICODE,
	UNIFORMATIINAME,
	ASSETTYPE,
	ASSETTYPEDESCRIPTION,
	FACILITYSECTION,
	CRITICALITYFACTOR,
	NOOFASSETSCOUNT,
	QUANTITYOFUNITS,
	UNITTYPE,
	MATERIAL,
	MATERAILDESCRIPTION,
	CREATED_BY
)
select
  INVENTORYID,
	LINEITEMID,
	UNIFORMATIICODE,
	UNIFORMATIINAME,
	ASSETTYPE,
	ASSETTYPEDESCRIPTION,
	FACILITYSECTION,
	CRITICALITYFACTOR,
	NOOFASSETSCOUNT,
	QUANTITYOFUNITS,
	UNITTYPE,
	MATERIAL,
	MATERAILDESCRIPTION,
	CREATED_BY
FROM TABLE(pkg_rowify.f_XML_ASSET_INVENTORY(0,in_nADOID, in_nADOID,in_vUser));
    exception when others then
      htp.p('error: ' || sqlerrm);
    end;
    
    insertLog2(in_nADOID,'EXTRACTING', 'PROCESSING',9,24);

  begin

---insert defects
insert /*+ append */ into spd_t_xml_defects (
  DEFECTID,
	INVENTORYID,
	DEFECTIDNO,
	DEFECTLOCATION,
	DEFECTCENTER,
	CRITICALITYFACTOR,
	DEFECTTYPE,
	SRMCLASSIFICATION,
	CREATED_BY 
)
select
  DEFECTID,
	INVENTORYID,
	DEFECTIDNO,
	DEFECTLOCATION,
	DEFECTCENTER,
	CRITICALITYFACTOR,
	DEFECTTYPE,
	SRMCLASSIFICATION,
	CREATED_BY   
FROM TABLE(pkg_rowify.f_XML_DEFECT(0,in_nADOID, in_nADOID,in_vUser));
    exception when others then
      htp.p('error: ' || sqlerrm);
    end;
    
    
  insertLog2(in_nADOID,'EXTRACTING', 'PROCESSING',10,24);

  begin
------insert executive summary
insert /*+ append */ into spd_t_xml_exec_summaries (
  EXECSUMMARYID,
	XMLACTIONID,
	CREATED_BY
)
select
  EXECSUMMARYID,
	XMLACTIONID,
	CREATED_BY
FROM TABLE(pkg_rowify.f_XML_EXEC_SUMMARY(0,in_nADOID, in_nADOID,in_vUser));
    exception when others then
      htp.p('error: ' || sqlerrm);
    end;
    
    insertLog2(in_nADOID,'EXTRACTING', 'PROCESSING',11,24);

  begin
------insert es paragraphs
insert /*+ append */ into spd_t_xml_es_paragraph (
  PARAGRAPHID,
  EXECSUMMARYID,
	CREATED_BY
)
select
  PARAGRAPHID,
  EXECSUMMARYID,
	CREATED_BY
FROM TABLE(pkg_rowify.f_XML_ES_PARAGRAPH(0,in_nADOID, in_nADOID,in_vUser));
    exception when others then
      htp.p('error: ' || sqlerrm);
    end;
    
    insertLog2(in_nADOID,'EXTRACTING', 'PROCESSING',12,24);

  begin
-------insert sentences
insert /*+ append */ into spd_t_xml_sentences (
  SENTENCEID,
	PARAGRAPHID,
	SENTENCE,
	CREATED_BY
)
select
  SENTENCEID,
	PARAGRAPHID,
	SENTENCE,
	CREATED_BY
FROM TABLE(pkg_rowify.f_XML_SENTENCE(0,in_nADOID, in_nADOID,in_vUser));
    exception when others then
      htp.p('error: ' || sqlerrm);
    end;
    
    insertLog2(in_nADOID,'EXTRACTING', 'PROCESSING',13,24);

  begin
-------insert ai fittings
insert /*+ append */ into spd_t_xml_ai_fittings (
  FITTINGID,
	INVENTORYID,
	ASSETTYPE,
	FITTINGIDNUMBER,
	TYPE,
	DESIGNCAPACITY,
	RATINGCAPACITY,
	CONDITIONFITTING,
	CONDITIONBASE,
	CONDITIONCONNECTIONHARDWARE,
	PHOTOFILENAME,
	CREATED_BY      
)
select
  FITTINGID,
	INVENTORYID,
	ASSETTYPE,
	FITTINGIDNUMBER,
	TYPE,
	DESIGNCAPACITY,
	RATINGCAPACITY,
	CONDITIONFITTING,
	CONDITIONBASE,
	CONDITIONCONNECTIONHARDWARE,
	PHOTOFILENAME,
	CREATED_BY 
FROM TABLE(pkg_rowify.f_XML_AI_FITTING(0,in_nADOID, in_nADOID,in_vUser));
    exception when others then
      htp.p('error: ' || sqlerrm);
    end;
    
    insertLog2(in_nADOID,'EXTRACTING', 'PROCESSING',14,24);
    
  begin
-------insert into facility drawings
insert /*+ append */ into spd_t_xml_facility_drawings (
  FACDRAWINGID,
	FACILITYID,
	DRAWINGID,
	NAVFACDRAWINGNUMBER,
	DESCRIPTION,
	DRAWINGTYPE,
	FILENAME,
	CREATED_BY
)
select
  FACDRAWINGID,
	FACILITYID,
	DRAWINGID,
	NAVFACDRAWINGNUMBER,
	DESCRIPTION,
	DRAWINGTYPE,
	FILENAME,
	CREATED_BY
FROM TABLE(pkg_rowify.f_XML_FACILITY_DRAWING(0,in_nADOID, in_nADOID,in_vUser));
    exception when others then
      htp.p('error: ' || sqlerrm);
    end;
    
  insertLog2(in_nADOID,'EXTRACTING', 'PROCESSING',15,24);
  
  begin
----insert facility photo
insert /*+ append */ into spd_t_xml_facility_photos (
  PHOTOID,
	FACILITYID,
	UNIFORMATIICODE,
	IMAGEFILENAME,
	CAPTION,
	PHOTOTYPE,
	CREATED_BY 
)
select
  PHOTOID,
	FACILITYID,
	UNIFORMATIICODE,
	IMAGEFILENAME,
	CAPTION,
	PHOTOTYPE,
	CREATED_BY 
FROM TABLE(pkg_rowify.f_XML_FACILITY_PHOTO(0,in_nADOID, in_nADOID,in_vUser));
    exception when others then
      htp.p('error: ' || sqlerrm);
    end;
    
    insertLog2(in_nADOID,'EXTRACTING', 'PROCESSING',16,24);

  begin
--------insert facility repair costs
insert /*+ append */ into spd_t_xml_facility_repairs (
  REPAIRID,
	FACILITYID,
	UNIFORMATIICODE,
	REPAIRDESCRIPTION,
	QUANTITY,
	UNITS,
	MATERIALUNITCOST,
	LABORUNITCOST,
	ASSETTYPE,
	CREATED_BY
)
select
  REPAIRID,
	FACILITYID,
	UNIFORMATIICODE,
	REPAIRDESCRIPTION,
	QUANTITY,
	UNITS,
	MATERIALUNITCOST,
	LABORUNITCOST,
	ASSETTYPE,
	CREATED_BY
FROM TABLE(pkg_rowify.f_XML_FACILITY_REPAIR(0,in_nADOID, in_nADOID,in_vUser));
    exception when others then
      htp.p('error: ' || sqlerrm);
    end;
    
    insertLog2(in_nADOID,'EXTRACTING', 'PROCESSING',17,24);

  begin
-------insert facility supporting files
insert /*+ append */ into spd_t_xml_fac_sup_files (
  FILEID,
	FACILITYID,
	SUPPORTINGFILEID,
	FILENAME,
	DESCRIPTION,
	CREATED_BY
)
select
  FILEID,
	FACILITYID,
	SUPPORTINGFILEID,
	FILENAME,
	DESCRIPTION,
	CREATED_BY
FROM TABLE(PKG_ROWIFY.F_XML_FAC_SUP_FILE(0,in_nADOID, in_nADOID,in_vUser));
    exception when others then
      htp.p('error: ' || sqlerrm);
    end;
    
    insertLog2(in_nADOID,'EXTRACTING', 'PROCESSING',18,24);

  begin
-----insert finding recommendation
insert /*+ append */ into spd_t_xml_findrec (
  RECID,
	FACILITYID,
	UNIFORMATIICODE,
	DESCRIPTION,
	FINDINGS,
	RECOMMENDATIONS,
	CREATED_BY
)
select
  RECID,
	FACILITYID,
	UNIFORMATIICODE,
	DESCRIPTION,
	FINDINGS,
	RECOMMENDATIONS,
	CREATED_BY
FROM TABLE(pkg_rowify.f_XML_FINDREC(0,in_nADOID, in_nADOID,in_vUser));
    exception when others then
      htp.p('error: ' || sqlerrm);
    end;
    
    insertLog2(in_nADOID,'EXTRACTING', 'PROCESSING',19,24);

  begin
------insert repair percentages
insert /*+ append */ into spd_t_xml_repair_percentage (
  PERCENTID,
	FACILITYID,
	Z1010,
	Z1020,
	Z1030,
	Z1040,
	Z2010,
	Z2020,
	Z2030,
	Z2040,
	Z2050,
	Z3010,
	IMPACT,
	ENGSTUDY,
	CREATED_BY
)
select
  PERCENTID,
	FACILITYID,
	Z1010,
	Z1020,
	Z1030,
	Z1040,
	Z2010,
	Z2020,
	Z2030,
	Z2040,
	Z2050,
	Z3010,
	IMPACT,
	ENGSTUDY,
	CREATED_BY
FROM TABLE(pkg_rowify.f_XML_REPAIR_PERCENTAGE(0,in_nADOID, in_nADOID,in_vUser));
    exception when others then
      htp.p('error: ' || sqlerrm);
    end;
    
    insertLog2(in_nADOID,'EXTRACTING', 'PROCESSING',20,24);

  begin
-----insert et metadata
insert /*+ append */ into spd_t_xml_et_metadata (
  METADATAID,
	EXECTABLEID,
	DISPLAY,
	VALUE,
	VISUALORDER,
	CREATED_BY     
)
select
  METADATAID,
	EXECTABLEID,
	DISPLAY,
	VALUE,
	VISUALORDER,
	CREATED_BY
FROM TABLE(pkg_rowify.f_XML_ET_METADATA(0,in_nADOID, in_nADOID,in_vUser));
    exception when others then
      htp.p('error: ' || sqlerrm);
    end;
    
    insertLog2(in_nADOID,'EXTRACTING', 'PROCESSING',21,24);

  begin
----insert attendee
insert /*+ append */ into spd_t_xml_attendees (
  ATTENDEEID,
	EXITBRIEFID,
	ATTENDEE_NAME,
	ATTENDEE_ORG,
	PHONENUMBER,
	EMAIL,
	CREATED_BY      
)
select
  ATTENDEEID,
	EXITBRIEFID,
	ATTENDEE_NAME,
	ATTENDEE_ORG,
	PHONENUMBER,
	EMAIL,
	CREATED_BY
FROM TABLE(pkg_rowify.f_XML_ATTENDEE(0,in_nADOID, in_nADOID,in_vUser));
    exception when others then
      htp.p('error: ' || sqlerrm);
    end;
    
    insertLog2(in_nADOID,'EXTRACTING', 'PROCESSING',22,24);
    
  begin

------insert ai meta
insert /*+ append */ into spd_t_xml_ai_metadata (
  METADATAID,
	INVENTORYID,
	DISPLAY,
	VALUE,
	VISUALORDER,
	CREATED_BY  
)
select
  METADATAID,
	INVENTORYID,
	DISPLAY,
	VALUE,
	VISUALORDER,
	CREATED_BY 
FROM TABLE(pkg_rowify.f_XML_AI_METADATA(0,in_nADOID, in_nADOID,in_vUser));
    exception when others then
      htp.p('error: ' || sqlerrm);
    end;
    
    insertLog2(in_nADOID,'EXTRACTING', 'PROCESSING',23,24);

  begin
  
  insert /*+ append */ into spd_t_xml_defect_metadata (
  METADATAID,
	DEFECTID,
	DISPLAY,
	VALUE,
	VISUALORDER,
	CREATED_BY     
)
select
  METADATAID,
	DEFECTID,
	DISPLAY,
	VALUE,
	VISUALORDER,
	CREATED_BY
FROM TABLE(pkg_rowify.f_XML_DEFECT_METADATA(0,in_nADOID, in_nADOID,in_vUser));
    exception when others then
      htp.p('error: ' || sqlerrm);
    end;

	insertLog2(in_nADOID,'EXTRACTING', 'PROCESSING',23,24);
	
for rec in (
select 'spd_t_xml_actions' table_name, count(*) count_of_records from (select * from spd_t_xml_actions) union all
select 'spd_t_xml_action_drawings', count(*) from (select * from spd_t_xml_action_drawings) union all
select 'spd_t_xml_facilities', count(*) from (select * from spd_t_xml_facilities) union all
select 'spd_t_xml_exit_briefs', count(*) from (select * from spd_t_xml_exit_briefs) union all
select 'spd_t_xml_exec_tables', count(*) from (select * from spd_t_xml_exec_tables) union all
select 'spd_t_xml_asset_summary', count(*) from (select * from spd_t_xml_asset_summary) union all
select 'spd_t_xml_asset_inventory', count(*) from (select * from spd_t_xml_asset_inventory) union all
select 'spd_t_xml_defects', count(*) from (select * from spd_t_xml_defects) union all
select 'spd_t_xml_exec_summaries', count(*) from (select * from spd_t_xml_exec_summaries) union all
select 'spd_t_xml_es_paragraph', count(*) from (select * from spd_t_xml_es_paragraph) union all
select 'spd_t_xml_sentences', count(*) from (select * from spd_t_xml_sentences) union all
select 'spd_t_xml_ai_fittings', count(*) from (select * from spd_t_xml_ai_fittings) union all
select 'spd_t_xml_ai_metadata', count(*) from (select * from spd_t_xml_ai_metadata) union all
select 'spd_t_xml_facility_drawings', count(*) from (select * from spd_t_xml_facility_drawings) union all
select 'spd_t_xml_facility_photos', count(*) from (select * from spd_t_xml_facility_photos) union all
select 'spd_t_xml_facility_repairs', count(*) from (select * from spd_t_xml_facility_repairs) union all
select 'spd_t_xml_fac_sup_files', count(*) from (select * from spd_t_xml_fac_sup_files) union all
select 'spd_t_xml_findrec', count(*) from (select * from spd_t_xml_findrec) union all
select 'spd_t_xml_repair_percentage', count(*) from (select * from spd_t_xml_repair_percentage) union all
select 'spd_t_xml_et_metadata', count(*) from (select * from spd_t_xml_et_metadata) union all
select 'spd_t_xml_attendees', count(*) from (select * from spd_t_xml_attendees)
)
loop

insertLog2(in_nADOID,rec.table_name, 'PROCESSED (' ||rec.count_of_records||')' ,rec.count_of_records,rec.count_of_records);

end loop;


exception WHEN OTHERS THEN
execute immediate 'insert into debug (FUNCTIONNAME, INPUT, MESSAGE) values (''' ||  lg_vPackageName ||lc_vFunctionName || ''','''|| 'An error occured while extracting the data from the SPD_T_XML_FLATTEN' ||''',''' || sqlerrm || ''')';
END extractDataFromFlattenTable;

--------------------------------------------------------------------------------------------------------------------------------

FUNCTION insertLog(in_nDOID number, in_vAction varchar2, in_vStatus varchar2) return varchar2 AS
lc_vImportSection varchar2(500);
lc_nExistingImportID number := 0;
lc_nCountOfRecord number:= 0;
lc_vFunctionName varchar2(10) := 'insertLog';
lc_Return varchar2(255) := '{"status":"Success"}';
pragma autonomous_transaction;

BEGIN
  
  lc_vImportSection := in_vAction || ' ' || in_vStatus;
  
  IF in_vStatus = 'COMPLETED' THEN
    lc_nCountOfRecord := 1;
  END IF;
  
  
  FOR rec in (SELECT IMPORTLOGID FROM SPD_T_IMPORT_LOG WHERE ADOID = in_nDOID AND IMPORT_SECTION = in_vAction) LOOP
    lc_nExistingImportID := rec.IMPORTLOGID;
  END LOOP;
  
  IF lc_nExistingImportID = 0 OR lc_nExistingImportID IS NULL THEN
    INSERT INTO SPD_T_IMPORT_LOG (IMPORTLOGID, ADOID, IMPORT_SECTION, GROUPING, TOTAL_RECORDS, COUNT_OF_RECORDS) values ('',in_nDOID,in_vAction,lc_vImportSection,1,lc_nCountOfRecord);
  ELSE
    UPDATE SPD_T_IMPORT_LOG SET IMPORT_SECTION = in_vAction, GROUPING = lc_vImportSection, COUNT_OF_RECORDS = lc_nCountOfRecord WHERE IMPORTLOGID = lc_nExistingImportID;
  END IF;
  
  commit;
  
  return lc_Return ;

EXCEPTION WHEN OTHERS THEN
  execute immediate 'insert into debug (FUNCTIONNAME, INPUT, MESSAGE) values (''' ||  lg_vPackageName ||lc_vFunctionName || ''','''|| 'An error occured while inserting log : ADOID'|| in_nDOID ||' ACTION'|| in_vAction ||' STATUS' || in_vStatus ||''',''' || sqlerrm || ''')';
  return '{"error":"' || sqlerrm || '"}';
END;

--------------------------------------------------------------------------------------------------------------------------------
-- SPD_T_XML_SENTENCES might not have a constraint on the ACTION
FUNCTION cleanActions(in_nADOID number) RETURN varchar2 AS
lc_vFunctionName varchar2(255):='cleanActions';
BEGIN

delete from SPD_T_XML_ACTIONS where ADOID = in_nADOID;
    
return 'y';
    
EXCEPTION WHEN OTHERS THEN
  execute immediate 'insert into debug (FUNCTIONNAME, INPUT, MESSAGE) values (''' || lg_vPackageName || lc_vFunctionName || ''','''|| 'An error occured while cleaning the actions' ||''',''' || sqlerrm || ''')';
  return '{"error":"default"}';
END;

--------------------------------------------------------------------------------------------------------------------------------


FUNCTION cleanFlatten(in_vUser varchar2, in_nADOID number) RETURN varchar2 AS
lc_vFunctionName varchar2(255):='cleanFlatten';
BEGIN

delete from SPD_T_XML_FLATTEN where ADOID = in_nADOID AND CREATED_BY = in_vUser;
commit;
    
return 'y';
    
EXCEPTION WHEN OTHERS THEN
  execute immediate 'insert into debug (FUNCTIONNAME, INPUT, MESSAGE) values (''' || lg_vPackageName || lc_vFunctionName || ''','''|| 'An error occured while cleaning the actions' ||''',''' || sqlerrm || ''')';
  return '{"error":"default"}';
END;


--------------------------------------------------------------------------------------------------------------------------------


FUNCTION isProcessing (in_cJSON clob) return json as 
  /* template variables */
  lc_vErrorDescription varchar2(255) := 'default';

  lc_vFunctionName varchar2(255) := 'getTargetTableName';
  
  lc_nResult number := 0 ;
  lc_jReturn json;
  
  lc_jJson json;
  lc_nADOID number;

begin

  lc_jReturn := json('{"status":"notProcessing"}');

  lc_jJson := json(in_cJSON);

  -- Get the ADOID out of the JSON 
  lc_nADOID := getCorrespondingJsonColumnVal(lc_jJson, 'ADOID');
  --lc_nContinue := f_test_for_error(lc_nADOID);
  
  for rec in (SELECT * FROM SPD_T_IMPORT_LOG WHERE adoid = lc_nADOID and lower(GROUPING) like '%processing%')LOOP
    lc_jReturn := json('{"status":"processing"}');
  END LOOP;
  

  return lc_jReturn;

exception     
  when others then
      execute immediate 'insert into debug (FUNCTIONNAME, INPUT, MESSAGE) values (''' ||  lg_vPackageName ||lc_vFunctionName || ''','''|| 'An error occured while requesting the process status' ||''',''' || sqlerrm || ''')';
      return json('{"error":"' || sqlerrm || '"}');
end;
--------------------------------------------------------------------------------------------------------------------------------


FUNCTION setDOFACIDS (in_nADOID number) return varchar2 as 
  /* template variables */
  lc_vErrorDescription varchar2(255) := 'default';

  lc_vFunctionName varchar2(255) := 'setDOFACIDS';
  
  lc_nResult number := 0 ;
  lc_vReturn varchar2(255);
  
  lc_jJson json;
  lc_nADOID number;

begin

  for rec in (
    select 
      xml_facility.facilityid,
      xml_facility_match.dofid
    from 
      spd_t_xml_facilities        xml_facility,
      spd_t_xml_facilities_match  xml_facility_match,
      spd_t_xml_actions           xml_action
    where 
      xml_facility.xmlactionid = xml_action.xmlactionid and  
      xml_facility_match.adoid = xml_action.adoid and
      xml_action.adoid = in_nADOID and
      xml_facility_match.xml_nfaid = xml_facility.NFAID)
  loop
    update spd_t_xml_facilities set deliveryorderfacilityid = rec.dofid where facilityid = rec.facilityid;
    commit;
  
  end loop;
  lc_vReturn := '1';
  return lc_vReturn;

exception     
  when others then
      execute immediate 'insert into debug (FUNCTIONNAME, INPUT, MESSAGE) values (''' ||  lg_vPackageName ||lc_vFunctionName || ''','''|| 'An error occured while requesting the process status' ||''',''' || sqlerrm || ''')';
      return '{"error":"' || sqlerrm || '"}';
end;
--------------------------------------------------------------------------------------------------------------------------------


FUNCTION rebuildMatView return varchar2 as 
  /* template variables */
  lc_vErrorDescription varchar2(255) := 'default';

  lc_vFunctionName varchar2(255) := 'rebuildMatView';
  
  lc_nResult number := 0 ;
  lc_vReturn varchar2(255);


begin
  DBMS_MVIEW.REFRESH('SPD_MV_DATA_ANALYSIS', 'C', '', TRUE, FALSE, 0,0,0, FALSE, FALSE);
  lc_vReturn := 1;
  
  return lc_vReturn;

exception     
  when others then
      execute immediate 'insert into debug (FUNCTIONNAME, INPUT, MESSAGE) values (''' ||  lg_vPackageName ||lc_vFunctionName || ''','''|| 'An error occured while requesting the process status' ||''',''' || sqlerrm || ''')';
      return '{"error":"' || sqlerrm || '"}';
end;
--------------------------------------------------------------------------------------------------------------------------------

FUNCTION collectXMLData(in_cJSON clob, in_vUser varchar2) RETURN json AS
lc_vUser varchar2(255) := 'DEFAULT'; 
lc_nContinue number := 0;
lc_Return json;
lc_vCleanAction varchar2(255);
lc_vFlattenTable varchar2(255);
lc_vDataFromFlatten varchar2(255);

lc_vLOGResults varchar2(255);
lc_jProcessingStatus json;
lc_vProcessingStatus varchar2(500);
lc_jParentID json;

lc_jJson json;
lc_jvValue json_value;

lc_nADOID number;
lc_vADOID varchar2(500);
lc_nFileId number := 0;

lc_vErrorDescription varchar2(500);

getFileIdException EXCEPTION;

lc_vFunctionName varchar2(300):= 'collectXMLData';

lc_cleanAction varchar2(255);
lc_vCleanFlatten varchar2(255);
lc_vSetDOFacIds varchar2(255);
lc_vRebuildMatView varchar2(255);

BEGIN

  lc_jParentID := json('{}');


  lc_Return := json('{"status":"success"}');

  lc_jJson := json(in_cJSON);

  -- Get the ADOID out of the JSON 

  lc_vADOID := getCorrespondingJsonColumnVal(lc_jJson, 'ADOID');
  
  lc_nADOID := to_number(lc_vADOID);
  
  lc_jParentID.put('ADOID', lc_nADOID);
  
  lc_nContinue := f_test_for_error(lc_nADOID);
  
  lc_jProcessingStatus := ISPROCESSING(in_cJSON);

  
  if lc_nContinue = 1 then
    if(lc_jProcessingStatus.exist('status')) then 
      lc_jvValue := lc_jProcessingStatus.get('status');
      lc_vProcessingStatus := getVal(lc_jvValue);

  
      IF lc_nContinue = 1 THEN
      
      lc_cleanAction := cleanActions(lc_nADOID);
      lc_vCleanFlatten := cleanFlatten(in_vUser,lc_nADOID);
      
      insertLog2(lc_nADOID,'CLEAN TABLES', 'PROCESSING',0,1);
    
      
        -- Get the File linked to this ADOID
        FOR rec in (select xmlfileid from spd_t_xml_files where adoid = lc_nADOID ORDER BY XMLFILEID DESC) LOOP
          IF lc_nFileId = 0 AND rec.xmlfileid IS NOT NULL THEN
            lc_nFileId := rec.xmlfileid;
          ELSE
            raise getFileIdException;
            EXIT;
          END IF;
          EXIT;
        END LOOP;
      END IF;

      
      -- Clear the tables from that user
      /*IF lc_nFileId != 0 THEN
        lc_vCleanAction := cleanActions(lc_nADOID);
        lc_nContinue := f_test_for_error(lc_vCleanAction);
      END IF;*/
      --lc_vCleanAction := cleanActions(lc_nADOID);
      
      -- Clear the SPD_T_XML_FLATTEN tables from that user
      /*IF lc_nContinue = '1' THEN
        lc_vCleanFlatten := cleanFlatten(in_vUser,lc_nADOID); 
        lc_nContinue := f_test_for_error(lc_vCleanFlatten);
      END IF;*/
      --lc_vCleanFlatten := cleanFlatten(in_vUser,lc_nADOID); 
    
      
      -- Populate SPD_T_XML_FLATTEN
      IF lc_nContinue = '1' THEN
      insertLog2(lc_nADOID,'CLEAN TABLES', 'COMPLETED',1,1);
      insertLog2(lc_nADOID,'FLATTEN XML', 'PROCESSING',0,1);
      lc_vFlattenTable := PKG_XML_FLATTEN.flattenXML(lc_nFileId,lc_nADOID, in_vUser);
      lc_nContinue := f_test_for_error(lc_vFlattenTable);
      END IF;
      
      -- Extract Data from SPD_T_XML_FLATTEN
      IF lc_nContinue = '1' THEN
      insertLog2(lc_nADOID,'FLATTEN XML', 'COMPLETED',1,1);
      -- SAVEPOINT in case an error occurs during the parsing (NOT OPERATIONAL YET)
      --SAVEPOINT save_point;
      
      begin
      
      insertLog2(lc_nADOID,'EXTRACTING', 'PROCESSING',0,1);
      extractDataFromFlattenTable(1,'/',lc_jParentID, in_vUser, lc_nADOID);
      EXCEPTION WHEN OTHERS THEN
        insertLog2(lc_nADOID,'FATAL ERROR', 'FATAL ERROR',0,1);
        --ROLLBACK TO save_point;
        --delete from spd_t_xml_actions where adoid = lc_nADOID;
        --delete from spd_t_xml_flatten where adoid = lc_nADOID;
        commit;
      end;
      
      --for now
      --delete from spd_t_xml_flatten where adoid = lc_nADOID;
      commit;
        
      insertLog2(lc_nADOID,'EXTRACTING', 'COMPLETED',1,1);
      
      insertLog2(lc_nADOID,'CLEANING', 'PROCESSING',0,1);
      commit;
      
      lc_vCleanFlatten := cleanFlatten(in_vUser,lc_nADOID); 
      
      insertLog2(lc_nADOID,'CLEANING', 'COMPLETED',1,1);
      commit;
      
      END IF;
    
    end if;  
    
  else
    lc_vErrorDescription := 'problem getting contractcontractorid';
    --raise spiders_error;
  end if;

  insertLog2(lc_nADOID,'ASSOCIATE WITH FACILITIES', 'PROCESSING',0,1);
	lc_vSetDOFacIds := setDOFACIDS(lc_nADOID);
  insertLog2(lc_nADOID,'ASSOCIATE WITH FACILITIES', 'COMPLETED',1,1);
  
  insertLog2(lc_nADOID,'REBUILD DATA ANALYST', 'PROCESSING',0,1);
  lc_vRebuildMatView := rebuildMatView;
  insertLog2(lc_nADOID,'REBUILD DATA ANALYST', 'COMPLETED',1,1);
  
  
  return lc_Return;
  
  EXCEPTION WHEN getFileIdException THEN
    execute immediate 'insert into debug (FUNCTIONNAME, INPUT, MESSAGE) values (''' || lg_vPackageName || lc_vFunctionName || ''','''|| 'An error occured while requesting file with the ADOID : '|| lc_nADOID ||''',''' || sqlerrm || ''')';
    lc_Return := json('{"error":"' || sqlerrm || '"}');
    insertLog2(lc_nADOID,'CLEAN TABLES', 'FATAL ERROR',0,1);
    insertLog2(lc_nADOID,'FLATTEN XML', 'FATAL ERROR',0,1);
    insertLog2(lc_nADOID,'EXTRACTING FROM FLATTEN XML', 'FATAL ERROR',0,1);
	 	return lc_Return;
  WHEN OTHERS THEN
    execute immediate 'insert into debug (FUNCTIONNAME, INPUT, MESSAGE) values (''' ||  lg_vPackageName ||lc_vFunctionName || ''','''|| 'An error occured while collecting data '|| lc_nADOID ||''',''' || sqlerrm || ''')';
    insertLog2(lc_nADOID,'CLEAN TABLES', 'FATAL ERROR',0,1);
    insertLog2(lc_nADOID,'FLATTEN XML', 'FATAL ERROR',0,1);
    insertLog2(lc_nADOID,'EXTRACTING FROM FLATTEN XML', 'FATAL ERROR',0,1);
	 	lc_Return := json('{"error":"' || sqlerrm || '"}');
    return lc_Return;
    
END collectXMLData;

END PKG_XML_PARSE;

/
