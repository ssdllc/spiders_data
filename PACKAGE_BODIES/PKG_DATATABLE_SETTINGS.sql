--------------------------------------------------------
--  DDL for Package Body PKG_DATATABLE_SETTINGS
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE PACKAGE BODY "SPIDERS_DATA"."PKG_DATATABLE_SETTINGS" as 


function f_test_for_error (in_vReturned varchar2) return varchar2 as 
  /* template variables */
  lc_vFunctionName varchar2(255) := 'f_test_for_error';
  lc_vReturn varchar2(32000) := '{"error":"default"}'; --defaul the return to e for error
  lc_nContinue number :=0; --default to 0 to dissalow continuing
  
  /* function specific variables */
  lc_vTest varchar2(32000) := in_vReturned;
begin

  if substr(lc_vTest, 1, 9) = '{"error":' then
    lc_vReturn := '0';
  else
    lc_vReturn := '1';
  end if;
  
  return lc_vReturn;

exception     
  when others then
      execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' dml error: ' || sqlerrm || ''')';
      return '{"error":"' || sqlerrm || '"}';
end; 

function getCorrespondingJsonColumnVal(in_jJson json, in_vColumn varchar2) return varchar2 as 
  /* template variables */
  json_error EXCEPTION;
  spiders_error EXCEPTION;
  lc_vErrorDescription varchar2(255) := 'default';
  lc_vFunctionName varchar2(255) := 'getCorrespondingJsonColumnVal';
  lc_vReturn varchar2(32000) := '{"error":"default"}'; --defaul the return to e for error
  lc_nContinue number :=0; --default to 0 to dissalow continuing
  
  /* function specific variables */
  lc_vName varchar2(2000);
  lc_vTemp varchar2(2000);
  lc_vComment varchar2(2000);
  lc_jvValue json_value;
  lc_jlData json_list;
  lc_jvRecVal json_value;
  lc_jValue json;
begin


  if(in_jJson.exist('data')) then
      if(in_jJson.get('data').is_array) then
          lc_jlData := json_list(in_jJson.get('data'));        
          lc_jvRecVal := lc_jlData.get(1); --return null on outofbounds
          if (lc_jvRecVal is not null) then
            lc_jValue := json(lc_jvRecVal);            
            if(lc_jValue.exist(in_vColumn)) then       
              lc_jvValue := lc_jValue.get(in_vColumn);
              lc_vReturn := getVal(lc_jvValue);
            else
              lc_vErrorDescription := in_vColumn || ' attribute not found in json';
              raise spiders_error;
            end if;
          else 
            lc_vErrorDescription := 'Rec val is null';
            raise spiders_error;
          end if;
      else
        lc_vErrorDescription := 'Data is not an array';
        raise spiders_error;
      end if;
  else
    lc_vErrorDescription := 'Data attribute not found in json';
    raise spiders_error;
  end if;

  return lc_vReturn;

exception     
  when spiders_error then
      execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' SPIDERS error ' || lc_vErrorDescription || ''')';
      return '{"error":"' || lc_vErrorDescription || '"}';
  when json_error then
      execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' JSON error'')';
      return '{"error":"json"}';
  when others then
      execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' dml error: ' || sqlerrm || ''')';
      return '{"error":"' || sqlerrm || '"}';
end; 

function getUser return varchar2 as
  lc_Return varchar2(250) := '{"error":"User not found"}';
  lc_vFunctionName varchar2(25) := 'getUser';
begin
  lc_Return := nvl(v('APP_USER'),'NONE');
  return lc_Return;
  
exception     
  when others then
      execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' dml error: ' || sqlerrm || ''')';
      return '{"error":"' || sqlerrm || '"}';
end;

function buildDeleteStatement(in_vTableName varchar2, in_vUser varchar2) return varchar2 as
  lc_vSQL varchar2(255) := 'DELETE FROM SPD_T_DATATABLE_SETTINGS WHERE ';
  lc_vFunctionName varchar2(255) := 'buildDeleteStatement';
begin

  lc_vSQL := lc_vSQL || 'DATATABLENAME = ''' || in_vTableName || ''' AND CREATED_BY = ''' || in_vUser || '''';
  return lc_vSQL;

exception when others then 
  execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' dml error: ' || sqlerrm || ''')';
      return '{"error":"' || sqlerrm || '"}';
end;

function buildSelectStatement(in_vTableName varchar2, in_vUser varchar2) return varchar2 as
  lc_vSQL varchar2(255) := 'SELECT COLUMNID FROM SPD_T_DATATABLE_SETTINGS WHERE ';
  lc_vFunctionName varchar2(255) := 'buildDeleteStatement';
begin
  lc_vSQL := lc_vSQL || 'DATATABLENAME = ''' || in_vTableName || ''' AND CREATED_BY = ''' || in_vUser || '''';
  return lc_vSQL;

exception when others then 
  execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' dml error: ' || sqlerrm || ''')';
      return '{"error":"' || sqlerrm || '"}';
end;



function deleteSettings(in_cClob clob) return JSON as
  spiders_error EXCEPTION;
  lc_jParameterJson json;
  lc_vDatatableName varchar2(255);
  lc_nContinue number := 0;
  lc_vSql varchar2(500);
  lc_jResults json;
  lc_jMessages json;
  lc_jlDML json_list;
  lc_vUser varchar2(50);
  lc_vErrorDescription varchar2(255);
  lc_vReturn json;
  lc_vDMLResult varchar2(500);
  lc_vFunctionName varchar2(255) := 'deleteSettings';
begin

  lc_vReturn := json('{status:"error"}');

  lc_jParameterJson := json(in_cClob);
  lc_vDatatableName := getCorrespondingJsonColumnVal(lc_jParameterJson, 'datatableName');
  lc_nContinue := f_test_for_error(lc_vDatatableName);
  
  
  if lc_nContinue = 1 then
    lc_vUser := getUser();
    lc_nContinue := f_test_for_error(lc_vUser);
  else
    lc_vErrorDescription := 'datatableName attribute not found';
    raise spiders_error;
  end if;
  
  if lc_nContinue = 1 then
    -- Build sql statsement
    lc_vSql := buildDeleteStatement(lc_vDatatableName, lc_vUser);
    lc_nContinue := f_test_for_error(lc_vUser);
  else
    lc_vErrorDescription := 'User not found';
    raise spiders_error;
  end if;
  
  
  if lc_nContinue = 1 then
      lc_jlDML := json_list();
      lc_jlDML.append(lc_vSql);
    else
      lc_vErrorDescription := 'SQL Statement failed';
      raise spiders_error; 
    end if;
  
  
  if lc_nContinue = 1 then
    lc_jResults := json();
    lc_jResults.put('results',lc_jlDML);
    
    lc_jMessages := json();
    lc_vDMLResult := BB_F_DML(lc_jResults, lc_jMessages);
  insert into debug (input) values (lc_vDMLResult);
    lc_vReturn := lc_jResults;
    
  else
    lc_vErrorDescription := 'Error in building the Delete SQl Statement';
    raise spiders_error;
  end if;
 
  return lc_vReturn;


exception 
    when spiders_error then 
      execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' '|| lc_vErrorDescription ||'dml error: ' || sqlerrm || ''')';
      return json('{"error":"' || sqlerrm || '"}');
    when others then 
      execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' dml error: ' || sqlerrm || ''')';
      return json('{"error":"' || sqlerrm || '"}');
end;

function getSettings (in_cClob clob) return JSON as
  spiders_error EXCEPTION;
  lc_jParameterJson json;
  lc_vDatatableName varchar2(255);
  lc_nContinue number := 0;
  lc_vSql varchar2(500);
  lc_jResults json;
  lc_jMessages json;
  lc_jlDML json_list;
  lc_vUser varchar2(50);
  lc_vErrorDescription varchar2(255);
  lc_vReturn json;
  lc_vDMLResult varchar2(500);
  lc_vFunctionName varchar2(255) := 'deleteSettings';
begin

  lc_vReturn := json('{status:"error"}');

  lc_jParameterJson := json(in_cClob);
  lc_vDatatableName := getCorrespondingJsonColumnVal(lc_jParameterJson, 'datatableName');
  lc_nContinue := f_test_for_error(lc_vDatatableName);
  
  
  if lc_nContinue = 1 then
    lc_vUser := getUser();
    lc_nContinue := f_test_for_error(lc_vUser);
  else
    lc_vErrorDescription := 'datatableName attribute not found';
    raise spiders_error;
  end if;
  
  if lc_nContinue = 1 then
    -- Build sql statsement
    lc_vSql := buildSelectStatement(lc_vDatatableName, lc_vUser);
    lc_nContinue := f_test_for_error(lc_vUser);
  else
    lc_vErrorDescription := 'User not found';
    raise spiders_error;
  end if;
  
  
  if lc_nContinue = 1 then
      lc_jlDML := json_list();
      lc_jlDML.append(lc_vSql);
    else
      lc_vErrorDescription := 'SQL Statement failed';
      raise spiders_error; 
    end if;
  
  
  if lc_nContinue = 1 then
    lc_jResults := json();
    lc_jResults.put('results',lc_jlDML);
    
    lc_jMessages := json();
    lc_vDMLResult := BB_F_DML(lc_jResults, lc_jMessages);

    lc_vReturn := lc_jMessages;
    
  else
    lc_vErrorDescription := 'Error in building the Delete SQl Statement';
    raise spiders_error;
  end if;
 
  return lc_vReturn;


exception 
    when spiders_error then 
      execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' '|| lc_vErrorDescription ||'dml error: ' || sqlerrm || ''')';
      return json('{"error":"' || sqlerrm || '"}');
    when others then 
      execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' dml error: ' || sqlerrm || ''')';
      return json('{"error":"' || sqlerrm || '"}');
end;

end;

/
