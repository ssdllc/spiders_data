--------------------------------------------------------
--  DDL for Package Body PKG_JOB_TITLES
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE PACKAGE BODY "SPIDERS_DATA"."PKG_JOB_TITLES" as

function f_test_for_error (in_vReturned varchar2) return varchar2 as
  /* template variables */
  lc_vFunctionName varchar2(255) := 'f_test_for_error';
  lc_vReturn varchar2(32000) := '{"error":"default"}'; --defaul the return to e for error
  lc_nContinue number :=0; --default to 0 to dissalow continuing

  /* function specific variables */
  lc_vTest varchar2(32000) := in_vReturned;
begin

  if substr(lc_vTest, 1, 9) = '{"error":' then
    lc_vReturn := '0';
  else
    lc_vReturn := '1';
  end if;

  return lc_vReturn;

exception
  when others then
      execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' dml error: ' || sqlerrm || ''')';
      return '{"error":"' || sqlerrm || '"}';
end;


function getAttributeValue(in_vAttribute varchar2, in_jJSON json) return varchar2 as
  /* template variables */
  json_error EXCEPTION;
  spiders_error EXCEPTION;
  lc_vErrorDescription varchar2(255) := 'default';
  lc_vFunctionName varchar2(255) := 'getAttributeValue';
  lc_vReturn varchar2(32000) := '{"error":"default"}'; --defaul the return to e for error
  lc_nContinue number :=0; --default to 0 to dissalow continuing

  /* function specific variables */
  lc_jvValue json_value;
begin

  if(in_jJSON.exist(in_vAttribute)) then
    lc_jvValue := in_jJSON.get(in_vAttribute);
    lc_vReturn := getVal(lc_jvValue);
  else
    lc_vErrorDescription := in_vAttribute || ' attribute not found in json';
    raise spiders_error;
  end if;

  /* get name of object */
  return lc_vReturn;

exception
  when spiders_error then
      execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' SPIDERS error ' || lc_vErrorDescription || ''')';
      return '{"error":"' || lc_vErrorDescription || '"}';
  when json_error then
      execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' JSON error'')';
      return '{"error":"json"}';
  when others then
      execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' dml error: ' || sqlerrm || ''')';
      return '{"error":"' || sqlerrm || '"}';
end;



function getCorrespondingJsonColumnVal(in_jJson json, in_vColumn varchar2) return varchar2 as 
  /* template variables */
  json_error EXCEPTION;
  spiders_error EXCEPTION;
  lc_vErrorDescription varchar2(255) := 'default';
  lc_vFunctionName varchar2(255) := 'getCorrespondingJsonColumnVal';
  lc_vReturn varchar2(32000) := '{"error":"default"}'; --defaul the return to e for error
  lc_nContinue number :=0; --default to 0 to dissalow continuing
  
  /* function specific variables */
  lc_vName varchar2(2000);
  lc_vTemp varchar2(2000);
  lc_vComment varchar2(2000);
  lc_jvValue json_value;
  lc_jlData json_list;
  lc_jvRecVal json_value;
  lc_jValue json;
begin

  /* i could not convince myself that we needed multiple functions here, since we still need to check if attributes exist in the sub functions*/
  if(in_jJson.exist('data')) then
      if(in_jJson.get('data').is_array) then
          lc_jlData := json_list(in_jJson.get('data'));        
          lc_jvRecVal := lc_jlData.get(1); --return null on outofbounds
          if (lc_jvRecVal is not null) then
            lc_jValue := json(lc_jvRecVal);            
            if(lc_jValue.exist(in_vColumn)) then       
              lc_jvValue := lc_jValue.get(in_vColumn);
              lc_vReturn := getVal(lc_jvValue);
            else
              lc_vErrorDescription := in_vColumn || ' attribute not found in json';
              raise spiders_error;
            end if;
          else 
            lc_vErrorDescription := 'Rec val is null';
            raise spiders_error;
          end if;
      else
        lc_vErrorDescription := 'Data is not an array';
        raise spiders_error;
      end if;
  else
    lc_vErrorDescription := 'Data attribute not found in json';
    raise spiders_error;
  end if;
            
  return lc_vReturn;

exception     
  when spiders_error then
      execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' SPIDERS error ' || lc_vErrorDescription || ''')';
      return '{"error":"' || lc_vErrorDescription || '"}';
  when json_error then
      execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' JSON error'')';
      return '{"error":"json"}';
  when others then
      execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' dml error: ' || sqlerrm || ''')';
      return '{"error":"' || sqlerrm || '"}';
end; 


function f_delete(in_cJSON clob) return json as
  /* template variables */
  json_error EXCEPTION;
  spiders_error EXCEPTION;
  lc_vErrorDescription varchar2(255) := 'default';
  lc_vFunctionName varchar2(255) := 'f_delete';
  lc_vRest varchar2(255) := 'f_delete';
  lc_jReturn json;
  lc_nContinue number :=0; --default to 0 to dissalow continuing

  /* function specific variables */
  lc_vModule varchar2(255);
  lc_jParameterJson json;
  lc_jlDML json_list;
  lc_jResults json;
  lc_jMessages json;
  lc_vDMLResult varchar2(255);
  lc_vSql long;


v1 varchar2(255);
j1 json;
jl1 json_list;
j2 json;
v2 varchar2(255);

  lc_jJson json;
  lc_vData varchar2(255);
  lc_jlDataValue json_list;
  lc_jvValue json_value;
  lc_vCONTRACTCONTRACTORID varchar2(255);
  lc_jData json;
  
  lc_vTemp varchar2(255);
begin

  lc_jlDML := json_list();
  
  ----------NEW BELOW-----------
  lc_jJson := json(in_cJson);
  lc_vCONTRACTCONTRACTORID := getCorrespondingJsonColumnVal(lc_jJson, 'CONTRACTCONTRACTORID');
  lc_nContinue := f_test_for_error(lc_vCONTRACTCONTRACTORID);
  ----------NEW ABOVE-----
  /*
  j1 := json(in_cJson);
  jl1 := json_list(j1.get('data'));
  j2 := json(jl1.get(1));
  lc_vPWSID := getVal(j2.get('PWSID'));
  lc_nContinue := f_test_for_error(lc_vPWSID);
  */
  if lc_nContinue = 1 then
    lc_vSql := 'delete from spd_t_job_titles where contractcontractorid = '||lc_vCONTRACTCONTRACTORID;
    lc_jlDML.append(lc_vSql);

  else
    lc_vErrorDescription := 'problem getting contractcontractorid';
    raise spiders_error;
  end if;

  if lc_nContinue = 1 then
    lc_jResults := json();
    lc_jResults.put('results',lc_jlDML);
    lc_jMessages := json();
    lc_vDMLResult := BB_F_DML(lc_jResults, lc_jMessages);
    lc_jReturn := lc_jMessages;
  else
    lc_vErrorDescription := 'problem getting contractcontractorid';
    raise spiders_error;
  end if;

  return lc_jReturn;


exception
  when spiders_error then
      execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' SPIDERS error ' || lc_vErrorDescription || ''')';
      return json('{"error":"' || lc_vFunctionName || ' SPIDERS error ' || lc_vErrorDescription || '"}');
  when json_error then
      execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' JSON error'')';
      return json('{"error":"' || lc_vFunctionName || ' JSON error ' || lc_vErrorDescription || '"}');
  when others then
      execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' dml error: ' || sqlerrm || ''')';
     return json('{"error":"' || lc_vFunctionName || ' dml error ' || sqlerrm || '"}');
end;

end;

/
