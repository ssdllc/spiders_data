--------------------------------------------------------
--  DDL for Package Body DATARECEIVER
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE PACKAGE BODY "SPIDERS_DATA"."DATARECEIVER" AS


/*

private functions/procedures

*/
function set_plan_locked_flag(IN_nAPID number) return varchar2;
function set_plan_locked_flag(IN_nAPID number) return varchar2

is 

lc_nCount number :=0;
lc_vReturn varchar2(1) := '';

begin

select 
  count(*) 
into
  lc_nCount
from 
  spd_t_action_planner 
where 
  apid in (select apid from spd_t_inter_set_plans where setplanid in (select setplanid from  spd_t_inter_set_plans where apid = in_napid))
  and (rec_date is not null or rec_inhouse_amt is not null or rec_ktr_amt is not null);
  
if lc_nCount > 0 then
  --this has planned actions that have received money.
  lc_vReturn := '1';
  update spd_t_list_set_plans set money_approved = 1, money_approved_date = sysdate where setplanid in (select setplanid from  spd_t_inter_set_plans where apid = in_napid);
  commit;

else
  lc_vReturn := '0';
  update spd_t_list_set_plans set money_approved = null, money_approved_date = null where setplanid in (select setplanid from  spd_t_inter_set_plans where apid = in_napid);
  commit;

end if;

return lc_vReturn;

end;

procedure add_program_oversight(IN_nSETPLANID number, IN_nPRODUCTLINE_LISTID number);
procedure add_program_oversight(IN_nSETPLANID number, IN_nPRODUCTLINE_LISTID number)

is
lc_nContinue number :=0;

begin

  for rec in (select * from spd_t_program_oversight where setplanid = IN_nSETPLANID and productline_listid = IN_nPRODUCTLINE_LISTID)
  loop
    lc_nContinue := 1;
  end loop;

  if lc_nContinue = 0 then
--add a record to the program_oversight table
    insert into   
      spd_t_program_oversight
      (setplanid, productline_listid)
    values
      (IN_nSetPlanid,IN_nProductline_Listid);
    commit;
  end if;
end;


procedure test_do_confirmation_fac_si(IN_nDOFID number, IN_vUSER varchar2);


procedure upd_actn_do(IN_JSON varchar2)

is 
    messages json;
    
    lc_nADOID                 number;
    lc_nAPID                  number;
    lc_nFUND_SOURCE_LISTID    number;
    lc_nREPORTID              number;
    lc_vSTART_DATE            varchar2(22);
    lc_vEND_DATE              varchar2(22);
    lc_vCONTRACT_NUMBER       varchar2(22);
    lc_nDELIVERY_ORDER_NUMBER  number;
    lc_vKTR_SUBMITTAL_DATE    varchar2(22);
    lc_vSPIDERS_DATA_VERSION  varchar2(22);
    lc_vCONTRACTOR            varchar2(22);
    lc_nWORKORDERID           number;
    lc_vDeliveryOrderName     varchar2(255);
    
begin
    --htp.p(IN_JSON);
    messages := json(IN_JSON);
    /*
      1	"ADOID"
      2	"APID"
      3	"FUND_SOURCE_LISTID"
      4	"REPORTID"
      5	"START_DATE"
      6	"END_DATE"
      7	"CONTRACT_NUMBER"
      8	"DELIVERY_ORDER_NUMBER"
      9	"KTR_SUBMITTAL_DATE"
      10	"SPIDERS_DATA_VERSION"
      11	"CONTRACTOR"
      12	"WORKORDERID"
    */
    
    
    lc_nADOID := json_ext.get_number(messages, 'data[1]');
    lc_nAPID:= json_ext.get_number(messages, 'data[2]');
    lc_nREPORTID:= json_ext.get_number(messages, 'data[4]');
    lc_vSTART_DATE:= json_ext.get_string(messages, 'data[5]');
    lc_vEND_DATE:= json_ext.get_string(messages, 'data[6]');
    lc_vCONTRACT_NUMBER:= json_ext.get_string(messages, 'data[7]');
    lc_nDELIVERY_ORDER_NUMBER:= json_ext.get_number(messages, 'data[8]');
    lc_vSPIDERS_DATA_VERSION := json_ext.get_string(messages, 'data[10]');
    lc_vCONTRACTOR:= json_ext.get_string(messages, 'data[11]');
    lc_nWORKORDERID:= json_ext.get_number(messages, 'data[12]');
    lc_vDeliveryOrderName:= json_ext.get_string(messages, 'data[13]');
    
    update 
      spd_t_actn_dos 
    set
      REPORTID = lc_nREPORTID ,
      START_DATE = to_date(lc_vSTART_DATE,'DD-MON-YYYY'),
      END_DATE = to_date(lc_vEND_DATE,'DD-MON-YYYY') ,
      CONTRACT_NUMBER = lc_vCONTRACT_NUMBER,
      DELIVERY_ORDER_NUMBER = lc_nDELIVERY_ORDER_NUMBER,
      SPIDERS_DATA_VERSION = lc_vSPIDERS_DATA_VERSION,
      CONTRACTOR = lc_vCONTRACTOR,
      WORKORDERID = lc_nWORKORDERID,
      DELIVERY_ORDER_NAME = lc_vDeliveryOrderName
    where
      ADOID = lc_nADOID;
    commit;
    
    
tobrowser.message('success','Updated Delivery Order Record');
    

exception
        when others then
        tobrowser.message('error', 'error updating action delivery order record' ||
        'REPORTID = ' ||  lc_nREPORTID || ',
        START_DATE = ' ||  lc_vSTART_DATE || ',
        END_DATE = ' ||  lc_vEND_DATE || ',
        CONTRACT_NUMBER = ' ||  lc_vCONTRACT_NUMBER || ',
        DELIVERY_ORDER_NUMBER = ' ||  lc_nDELIVERY_ORDER_NUMBER || ',
        SPIDERS_DATA_VERSION = ' ||  lc_vSPIDERS_DATA_VERSION || ',
        CONTRACTOR = ' ||  lc_vCONTRACTOR || ',
        DELIVERY_ORDER_NAME = ' ||  lc_vDeliveryOrderName || ', 
        WORKORDERID = ' ||  lc_nWORKORDERID || ' : ' ||
        SQLERRM
        );

      

end;


procedure del_do_dc_type(IN_nADOID number, IN_nLISTID number)


is
lc_nCount number :=0;
lc_nListid number :=0;
lc_vStringOfIds varchar2(2000) :='';
lc_vReturn varchar2(2000) :='';
lc_nContinue number :=0;

begin

--we do not delete list items that are default

for rec in (select grouping from spd_t_lists where listid = in_nListid)
loop
  if rec.grouping = 0 then
--this is a default, dont do anything to any table
    lc_nContinue := 1;
  end if;
end loop;

if lc_nContinue = 0 then
--this is not a default list item, delete away!
    --delete from spd_t_do_facility_cd_dc where dofid = DOFID and directcost_listid = DLISTID
    delete 
    from 
      spd_t_do_ce_dc 
    where 
      directcost_listid = IN_nLISTID and
      adoid = IN_nADOID;
    commit;    
                              
    
    
    
    --delete from spd_t_lists if the directcost_listid is not used by any other do facility
    for rec in (select * from spd_t_do_ce_dc where directcost_listid = IN_nLISTID)
    loop
      lc_nCount :=1;  
      lc_vStringOfIds := lc_vStringOfIds || rec.docedcid || ',';
    end loop;
    
    if lc_nCount = 0 then
      delete from spd_t_lists where listid = IN_nLISTID;
      commit;
      lc_vReturn := 'success';
    else
      lc_vReturn := 'no action (' || lc_vStringOfIds ||')';
    end if;
    
    commit;
    
    
    tobrowser.message('success', 'Deleting delivery order direct cost type from spd_t_do_ce_dc: success, Delete from spd_t_lists: ' || lc_vReturn);
else
--this is a default list item, no action on any table
  tobrowser.message('no action', 'No action has been taken, this is a default list item');
end if;

  exception
        when others then
        tobrowser.message('error', 'error deleting action delivery direct cost type: ' || 
        'IN_nADOID = ' || to_char(IN_nADOID) || ',
        IN_nLISTID = ' ||  to_char(IN_nLISTID) || ' : ' ||
        SQLERRM
        );

end;


procedure reset_actn_do_dc(IN_APID number)


is


begin
  
  
  insert into spd_t_do_ce_dc (directcost_listid, adoid) 
  select 
    listid,
    IN_APID adoid
  from 
    spd_t_lists 
  where 
    listname = 'list_delivery_order_direct_cost_types' 
    and listid not in (
                        select 
                          directcost_listid 
                        from 
                          spd_t_do_ce_dc 
                        where 
                          adoid = IN_APID
                      );
  commit;
  
  exception
        when others then
        tobrowser.message('error', 'error reseting action delivery direct cost types: ' || 
        'IN_APID = ' || IN_APID || ': ' ||
        SQLERRM
        );

end;



procedure add_actn_do_fac(IN_ADOID number, IN_NFA varchar2, IN_PL number, IN_nDOFID number default 0)


is 
lc_jObj json;
lc_jDOFac json;
lc_jDOFacDC json;
lc_jDOFacSpecInstr json;
lc_jDOFacInfads json;
i number;
lc_nDOFID number;
lc_nCount number :=0;
lc_nColCount number;
lc_nDOFacCount number :=0;
lc_nFacid number :=0;
lc_nDOWorkTypeListid number :=0;


begin

/*
1. Check to see if this facility is in the table spd_t_facilities
  if it is not already there
    Add the facility
    Check to see if the activity exists in spd_t_activities
      if it is not already there, add it.

2. Check to see if this facility is already associated with this doid
  if it is not already associated
    Add this facility to the spd_t_do_facilities
    Add defaults to the spd_t_do_fac_spec_inst table  !!!THIS IS WHERE WE WILL HAVE TO EDIT DEFAULTS BASED ON THE DO TYPE
    Add defaults to spd_t_do_facility_ce_dc !!!THIS IS WHERE WE WILL HAVE TO EDIT DEFAULTS BASED ON THE DO TYPE

*/




for rec in (select worktype_listid from spd_t_actn_dos where adoid = IN_ADOID) loop
  lc_nDOWorkTypeListid := rec.worktype_listid;
end loop;

  select count(*) into lc_nColCount from user_tab_cols  where table_name = 'SPD_T_DO_FACILITIES';
  lc_nColCount := lc_nColCount + 1;--This accounts for adding the row number into the query below...testing.
/*
When adding a facility to a scope, we need to check if its in the spd_t_facilities
if it is, then get the spiders facid
else add it and then get the spiders facid
*/
for rec in (select * from spd_t_facilities where infads_facility_id = IN_NFA) loop
  lc_nCount := 1;
  --here is an issue, if we have a facility with the same NFA we will return two rows. THIS IS IMPORTANT TO UNDERSTAND as it will create an issue when adding facilities.
  lc_nFacid := rec.facid;
end loop;

if lc_nCount = 0 then
  /*
    add this facility to the spd_t_facilites table
  */
  insert into spd_t_facilities (infads_facility_id, product_line_listid, spd_fac_name, spd_fac_no) select IN_NFA, IN_PL, facility_name, facility_no from spd_mv_inf_facility where facility_id = IN_NFA;
  commit;
  
end if;

lc_nCount := 0;

for rec in (select * from spd_t_activities where spd_uic in (select inf.activity_uic from spd_mv_inf_facility inf where inf.facility_id = IN_NFA)) loop
  lc_nCount := 1;
end loop;

if lc_nCount = 0 then
  /*
    add the activity record to spd_t_activities table
  */
  insert into spd_t_activities (spd_activity_name, spd_uic, state, country) select unit_title, uic, state, country from spd_mv_inf_cur_activities where uic in (select activity_uic from spd_mv_inf_facility where facility_id = IN_NFA);
  commit;

end if;



/*
Is this NFA already associated with the DOID?
*/

select 
  count(*) into lc_nDOFacCount
from 
  spd_t_do_facilities dofac
where 
  dofac.facid = lc_nFacid
  and adoid  = IN_ADOID;
    


if lc_nDOFacCount = 0 then
    
    /*
    get the new dofid to be used when adding special instructions and direct cost lists
    if the dofid is supplied, use the passed in value
    */
    if IN_nDOFID <> 0 then
      lc_nDOFID := IN_nDOFID;
    else
      select spd_s_do_facilities.nextval into lc_nDOFID from dual;
    end if;
    
    
    insert into 
      spd_t_do_facilities 
        (
          dofid, 
          adoid, 
          facid--,
          --ACTION_TYPE_LISTID
        ) 
    values 
      (
        lc_nDOFID, 
        IN_ADOID,--(select adoid from spd_t_actn_dos where apid = IN_APID and rownum = 1), 
        (select facid from spd_t_facilities where infads_facility_id = IN_NFA)--,
        --(select listid from spd_t_lists where listname = 'list_planned_action_types' and returnvalue = 'Routine Inspection' and  productline_listid = IN_PL)
      );
    commit;
    
    insert into spd_t_do_fac_spec_inst (dofid, spec_instr_listid) select lc_nDOFID, listid from spd_t_lists where listid in 
    (
      select 
        listid 
      from 
        spd_t_lists
      where 
        productline_listid = IN_PL 
        and listname = 'list_facility_special_instructions'
        and grouping = (select displayvalue from spd_t_lists where listid = lc_nDOWorkTypeListid)
      );
    commit;
    
    insert into spd_t_do_facility_ce_dc (dofid, directcost_listid) select lc_nDOFID, listid from spd_t_lists where listname = 'list_facility_direct_cost_types' and productline_listid = IN_PL and grouping = lc_nDOWorkTypeListid;
    commit;
    
    tobrowser.message('success', to_char(lc_nDOFID));

else

    tobrowser.message('no action', 'Facility Already Associated to Delivery Order');

end if;


  exception
        when others then
        tobrowser.message('error', 'error inserting delivery order facility, special instruction or facility direct cost types: ' || 
        'IN_APID = ' || IN_ADOID || ',
        IN_NFA = ' || IN_NFA || ',
        IN_PL = ' || IN_PL || ': ' ||
        SQLERRM
        );

end;


procedure del_actn_do_fac(IN_DOFID number)


is 



begin

/*
delete all records from all tables related to this DOFID, constraints could take care of this, but we'll make sure it happens manually for now
*/

delete from spd_t_do_fac_spec_inst where DOFID = IN_DOFID;
delete from spd_t_do_facility_ce_dc where DOFID = IN_DOFID;
delete from spd_t_do_facilities where DOFID = IN_DOFID;
commit;

tobrowser.message('success', 'Delete from facilities, special instructions and direct costs successful');

exception
        when others then
        tobrowser.message('error', 'error deleting delivery order facility, special instruction or facility direct cost types: ' || 
        'IN_DOFID = ' || IN_DOFID || ': ' ||
        SQLERRM
        );

end;



procedure do_fac_specinstr(IN_JSON varchar2)

is
    messages json;
    
    lc_nAFSIID                number;
    lc_nSpecInstrListid       number;
    
begin
    htp.p('starting update...');
    messages := json(IN_JSON);
    /*
      1	"ADOID"
      2	"APID"
      3	"FUND_SOURCE_LISTID"
      4	"REPORTID"
      5	"START_DATE"
      6	"END_DATE"
      7	"CONTRACT_NUMBER"
      8	"DELIVERY_ORDER_NUMBER"
      9	"KTR_SUBMITTAL_DATE"
      10	"SPIDERS_DATA_VERSION"
      11	"CONTRACTOR"
      12	"WORKORDERID"
    */
    
    --lc_nADOID := json_ext.get_number(messages, 'data[1][1]');
    
    
    
    for i in 1..100 loop
      if json_ext.get_json_value(messages, 'data[' ||  i ||  '][1]') is not null then 
        lc_nAFSIID :=json_ext.get_number(messages, 'data[' ||  i ||  '][1]');
        lc_nSpecInstrListid :=json_ext.get_number(messages, 'data[' ||  i ||  '][4]');
        update
          spd_t_do_fac_spec_inst
        set 
          spec_instr_value = lc_nSpecInstrListid
        where
          afsiid = lc_nAFSIID;
        commit;
      else
        exit;
      end if;
    end loop;
    

    
    htp.p('success updating action delivery order special instruction record');
    
    exception
        when others then
        tobrowser.message('error', 'error updating action delivery order special instruction record: ' || 
        'lc_nAFSIID = ' || to_char(lc_nAFSIID) || ',
        lc_nSpecInstrListid = ' ||  to_char(lc_nSpecInstrListid) || ' : ' ||
        SQLERRM
        );
end;



procedure add_do_fac_dc_type(IN_nADOID number, IN_vITEM varchar2, IN_DEFAULT varchar2 default '1', IN_PL number default 100085, IN_APPLYTOALL number default 0)


is

lc_nListID number;
lc_nMaxVisualOrder number :=0;
lc_nContinue number :=0;

begin
--We should not be adding empty strings
if length(replace(IN_vITEM,' ','')) > 0 then
    
    --Check to see if we already have this, uppercase and remove spaces to verify
    for rec in (select * from spd_t_lists where upper(replace(displayvalue,' ','')) = upper(replace(IN_vITEM,' ','')))
    loop
      --we have found something that matches what we entered. do not add this again.
      lc_nContinue := 1;
    end loop;
    
    
    
    if lc_nContinue = 0 then
      select spd_s_lists.nextval into lc_nListID from dual;
      
      select count(*) into lc_nMaxVisualOrder from spd_t_lists where listname = 'list_facility_direct_cost_types' and productline_listid = IN_PL;--lc_nMaxVisualOrder := rec.visualorder + 1;
      lc_nMaxVisualOrder := lc_nMaxVisualOrder + 1;
      
      insert into spd_t_lists (listid, displayvalue, returnvalue, grouping, listname, visualorder, notes, productline_listid) values (lc_nListID, IN_vITEM, IN_vITEM, IN_DEFAULT, 'list_facility_direct_cost_types', lc_nMaxVisualOrder, 'Entered from website', IN_PL);
      commit;
      
      if IN_APPLYTOALL = 0 then
      
        for rec in (select * from spd_t_do_facilities where adoid = IN_nADOID) loop
          insert into spd_t_do_facility_ce_dc (dofid, directcost_listid) values (rec.dofid, lc_nListID);
          commit;
        end loop;
        
      end if;
      
      
      tobrowser.message('success', 'Adding delivery order facility direct cost type');
    else
      tobrowser.message('no action', 'Delivery order facility direct cost type already exists');
    end if;

else
  tobrowser.message('no action', 'Can not add an empty facility direct cost type');
end if;
    
    exception
        when others then
        tobrowser.message('error', 'error inserting delivery order facility direct cost type : ' || 
        'IN_nADOID = ' || to_char(IN_nADOID) || ',
        IN_vITEM = ' ||  to_char(IN_vITEM) || ' : ' ||
        SQLERRM
        );
end;

procedure del_do_fac_dc_type(IN_nADOID number, IN_nLISTID number)

is

lc_nCount number :=0;
lc_nListid number :=0;
lc_vStringOfIds varchar2(2000) :='';
lc_vReturn varchar2(2000) :='';
lc_nContinue number :=0;

begin

--we do not delete list items that are default

for rec in (select grouping from spd_t_lists where listid = in_nListid)
loop
  if rec.grouping = 0 then
--this is a default, dont do anything to any table
    lc_nContinue := 1;
  end if;
end loop;

if lc_nContinue = 0 then
--this is not a default list item, delete away!
    --delete from spd_t_do_facility_cd_dc where dofid = DOFID and directcost_listid = DLISTID
    delete 
    from 
      spd_t_do_facility_ce_dc 
    where 
      directcost_listid = IN_nLISTID and
      dofid in (
                  select
                    dofid 
                  from 
                    spd_t_do_facilities 
                  where 
                    adoid = IN_nADOID
                );
    commit;    
                              
    
    
    
    --delete from spd_t_lists if the directcost_listid is not used by any other do facility
    for rec in (select * from spd_t_do_facility_ce_dc where directcost_listid = IN_nLISTID)
    loop
      lc_nCount :=1;  
      lc_vStringOfIds := lc_vStringOfIds || rec.afceid || ',';
    end loop;
    
    if lc_nCount = 0 then
      delete from spd_t_lists where listid = IN_nLISTID;
      commit;
      lc_vReturn := 'success';
    else
      lc_vReturn := 'no action (' || lc_vStringOfIds ||')';
    end if;
    
    commit;
    
    
    tobrowser.message('success', 'Deleting delivery order facility direct cost type from spd_t_do_facility_ce_dc: success, Delete from spd_t_lists: ' || lc_vReturn);
else
--this is a default list item, no action on any table
  tobrowser.message('no action', 'No action has been taken, this is a default list item');
end if;
    
    exception
        when others then
        tobrowser.message('error', 'error deleting delivery order facility direct cost type : ' || 
        'IN_nADOID = ' || to_char(IN_nADOID) || ',
        IN_nLISTID = ' || to_char(IN_nLISTID) || ' : ' ||
        SQLERRM
        );

end;


procedure add_actn_do_dc_type(IN_nADOID number, IN_vITEM varchar2, IN_DEFAULT varchar2 default '1', IN_PL number default 100085)


is

lc_nListID number;
lc_nMaxVisualOrder number :=0;
lc_nContinue number :=0;


begin
--We should not be adding empty strings
if length(replace(IN_vITEM,' ','')) > 0 then

	--Check to see if we already have this, uppercase and remove spaces to verify
    for rec in (select * from spd_t_lists where upper(replace(displayvalue,' ','')) = upper(replace(IN_vITEM,' ','')))
    loop
      --we have found something that matches what we entered. do not add this again.
      lc_nContinue := 1;
    end loop;
    
    if lc_nContinue = 0 then
		select spd_s_lists.nextval into lc_nListID from dual;
		
		select count(*) into lc_nMaxVisualOrder from spd_t_lists where listname = 'list_delivery_order_direct_cost_types' and productline_listid = IN_PL;--lc_nMaxVisualOrder := rec.visualorder + 1;
		lc_nMaxVisualOrder := lc_nMaxVisualOrder + 1;
		
		insert into spd_t_lists (listid, displayvalue, returnvalue, grouping, listname, visualorder, notes, productline_listid) values (lc_nListID, IN_vITEM, IN_vITEM, IN_DEFAULT, 'list_delivery_order_direct_cost_types', lc_nMaxVisualOrder, 'Entered from website', IN_PL);
		commit;
	
		insert into spd_t_do_ce_dc (adoid, directcost_listid) values (IN_nADOID, lc_nListID);
		commit;
		tobrowser.message('success', 'Adding delivery order direct cost type');
    else
      	tobrowser.message('no action', 'Delivery order direct cost type already exists');
    end if;

else
  tobrowser.message('no action', 'Can not add an empty direct cost type');
end if;

    exception
        when others then
        tobrowser.message('error', 'error inserting delivery order direct cost type : ' || 
        'IN_nADOID = ' || to_char(IN_nADOID) || ',
        IN_vITEM = ' ||  IN_vITEM || ' : ' ||
        SQLERRM
        );
end;


procedure add_planned_action(IN_YEAR number default 2011, IN_FUNDSOURCELISTID number default 100032, IN_ACTIONTYPELISTID number default 100081, IN_INHOUSE number default 0, IN_KTR number default 0, IN_UIC varchar2, IN_PL number default 100085, IN_QTR varchar2 default 'Q1', IN_nAPID number default 0)


is 

lc_nActivitiesID number :=0;
lc_nCountSetPlans number :=0;
lc_nSetPlanId number :=0;
lc_nContinue number :=0;
lc_nAPID number :=0;
lc_nInterSetPlanId number :=0;
lc_vMessage varchar2(2000);
lc_nNotesKey number :=0;
lc_vFundsource varchar2(2000);
lc_vUser varchar2(2000) := APEX_UTIL.GET_SESSION_STATE('APP_USER'); --when in sso, change to appropriate user query

begin


--if the activity is not in our table, we must add it. 
for rec in (select activitiesid from spd_t_activities where spd_uic = IN_UIC) loop
  lc_nActivitiesID := rec.activitiesid;
end loop;



if lc_nActivitiesID = 0 then
  /*
    add the activity record to spd_t_activities table
  */
  
  select spd_s_activities.nextval into lc_nActivitiesID from dual;
  --htp.p(lc_nActivitiesID);
  insert into spd_t_activities (activitiesid, spd_activity_name, spd_uic, state, country) select lc_nActivitiesID, unit_title, uic, state, country from spd_mv_inf_cur_activities where uic = IN_UIC and rownum=1;
  commit;

end if;

for rec in (select displayvalue from spd_t_lists where listid = IN_FUNDSOURCELISTID)
loop
  lc_vFundsource := rec.displayvalue;
end loop;

/*
We need to have logic built into adding planned actions to do the following:
	Check to see if a set plan exists for this fundsource and year
		if one does check to see if it is locked
			if not locked, add this new planned action to the set plan list (inter)
			if locked, create a new set plan, then add this planned action to the set plan list (inter)
		if one does not, create a new set plan, then add this planned action to the set plan list (inter)
*/

select count(*) into lc_nCountSetPlans from spd_t_list_set_plans where fundsource_listid = IN_FUNDSOURCELISTID and basefy = IN_YEAR;

if lc_nCountSetPlans = 0 then
--there are no set plans for this fundsource and year. A new set plan will be made

    select spd_s_list_set_plans.nextval into lc_nSetPlanId from dual;
    select spiders_data.spd_s_noteskey.nextval into lc_nNotesKey from dual;
    
    /*
      check for in_nAPID. If passed in use that value, if not generate new one.
    */
    
    if IN_nAPID > 0 then
      lc_nAPID := IN_nAPID;
    else
      select spiders_data.spd_s_action_planner.nextval into lc_nAPID from dual;
    end if;
    
    select spiders_data.spd_s_inter_set_plans.nextval into lc_nInterSetPlanId from dual;
    
--Adding the record to the set plans table
	insert into 
      spiders_data.spd_t_list_set_plans
        (
          setplanid,
          set_plan_type_listid,
          basefy,
          endfy,
          fundsource_listid,
          created_by,
          plantitle,
          noteskey
        )
      values
        (
          lc_nSetplanid,
          100560,
          IN_YEAR,
          IN_YEAR,--we are making this a funding request only table. we will only make funding requests for a single year
          IN_FUNDSOURCELISTID,
          lc_vuser,
          lc_vFundsource || '_Funding_Request_' || IN_YEAR || '_' || lc_nCountSetPlans,
          lc_nNoteskey
        );
	
	
--Adding the record to the action planner table
	insert into 
		spd_t_action_planner 
			(
        apid,
				planned_year, 
				planned_fund_source_listid,
				planned_action_type_listid, 
				planned_inhouse_amt, 
				planned_ktr_amt, 
				planned_activitiesid, 
				productline_listid, 
				planned_qtr
			)
		values
			(
        lc_nAPID,
				IN_YEAR, 
				IN_FUNDSOURCELISTID, 
				IN_ACTIONTYPELISTID, 
				IN_INHOUSE, 
				IN_KTR, 
				lc_nActivitiesID, 
				IN_PL, 
				IN_QTR
			);
			add_program_oversight(lc_nSetPlanid,IN_PL);
--Adding the record to the inter set plans table
	insert into 
		spd_t_inter_set_plans 
			(
				setplanid, 
				apid
			) 
		values
			(
				lc_nSetPlanId,
				lc_nAPID
			);
	commit;
	lc_vMessage := 'Added new Funding Request, Planned Action and associated the planned action to the Funding Request';
else
--there is a set plan for this, we must check to see if it is locked
  for rec in (select setplanid from spd_t_list_set_plans where fundsource_listid = IN_FUNDSOURCELISTID and basefy = IN_YEAR and money_approved is null and rownum = 1 order by setplanid)
  loop
    lc_nSetPlanId := rec.setplanid;
  end loop;
	if lc_nSetPlanId = 0 then
--this is locked, add a new everything
		select spd_s_list_set_plans.nextval into lc_nSetPlanId from dual;
		select spiders_data.spd_s_noteskey.nextval into lc_nNotesKey from dual;


    /*
      check for in_nAPID. If passed in use that value, if not generate new one.
    */
    
    if IN_nAPID > 0 then
      lc_nAPID := IN_nAPID;
    else
      select spiders_data.spd_s_action_planner.nextval into lc_nAPID from dual;
    end if;
    
		select spiders_data.spd_s_inter_set_plans.nextval into lc_nInterSetPlanId from dual;
		
--Adding the record to the set plans table
		insert into 
		  spiders_data.spd_t_list_set_plans
			(
			  setplanid,
			  set_plan_type_listid,
			  basefy,
			  endfy,
			  fundsource_listid,
			  created_by,
			  plantitle,
			  noteskey
			)
		  values
			(
			  lc_nSetplanid,
			  100560,
			  IN_YEAR,
			  IN_YEAR,--we are making this a funding request only table. we will only make funding requests for a single year
			  IN_FUNDSOURCELISTID,
			  lc_vUser,
			  lc_vFundsource || '_Funding_Request_' || IN_YEAR|| '_' || lc_nCountSetPlans,
			  lc_nNoteskey
			);
		
		
--Adding the record to the action planner table
		insert into 
			spd_t_action_planner 
				(
          apid,
					planned_year, 
					planned_fund_source_listid,
					planned_action_type_listid, 
					planned_inhouse_amt, 
					planned_ktr_amt, 
					planned_activitiesid, 
					productline_listid, 
					planned_qtr
				)
			values
				(
          lc_nAPID,
					IN_YEAR, 
					IN_FUNDSOURCELISTID, 
					IN_ACTIONTYPELISTID, 
					IN_INHOUSE, 
					IN_KTR, 
					lc_nActivitiesID, 
					IN_PL, 
					IN_QTR
				);
				
--Adding the record to the inter set plans table
		insert into 
			spd_t_inter_set_plans 
				(
					setplanid, 
					apid
				) 
			values
				(
					lc_nSetPlanId,
					lc_nAPID
				);
    add_program_oversight(lc_nSetPlanid,IN_PL);
		commit;
		lc_vMessage := 'Added new Funding Request, Planned Action and associated the planned action to the Funding Request';
	else
--this is not locked, just add the planned action to the inter set plans table
    /*
      check for in_nAPID. If passed in use that value, if not generate new one.
    */
    
    if IN_nAPID > 0 then
      lc_nAPID := IN_nAPID;
    else
      select spiders_data.spd_s_action_planner.nextval into lc_nAPID from dual;
    end if;
		select spiders_data.spd_s_inter_set_plans.nextval into lc_nInterSetPlanId from dual;
--Adding the record to the action planner table
		insert into 
			spd_t_action_planner 
				(
          apid,
					planned_year, 
					planned_fund_source_listid,
					planned_action_type_listid, 
					planned_inhouse_amt, 
					planned_ktr_amt, 
					planned_activitiesid, 
					productline_listid, 
					planned_qtr
				)
			values
				(
          lc_nAPID,
					IN_YEAR, 
					IN_FUNDSOURCELISTID, 
					IN_ACTIONTYPELISTID, 
					IN_INHOUSE, 
					IN_KTR, 
					lc_nActivitiesID, 
					IN_PL, 
					IN_QTR
				);
				
	--Adding the record to the inter set plans table
		insert into 
			spd_t_inter_set_plans 
				(
					setplanid, 
					apid
				) 
			values
				(
					lc_nSetPlanId,
					lc_nAPID
				);
    add_program_oversight(lc_nSetPlanid,IN_PL);
		commit;
	end if;
	lc_vMessage := 'Added new Planned Action and associated the planned action to the existing Funding Request';
end if;



tobrowser.message('success',lc_vMessage);

    exception
        when others then
        tobrowser.message('error', 'error inserting planned action : ' || 
        'IN_YEAR ' || to_char(IN_YEAR) || ',
        IN_FUNDSOURCELISTID ' || to_char(IN_FUNDSOURCELISTID) || ',
        IN_ACTIONTYPELISTID ' || to_char(IN_ACTIONTYPELISTID) || ',
        IN_INHOUSE ' || to_char(IN_INHOUSE) || ',
        IN_KTR ' || to_char(IN_KTR) || ',
        IN_UIC ' || to_char(IN_UIC) || ',
        IN_PL ' || to_char(IN_PL) || ':' || 
        SQLERRM
        );


end;



procedure del_planned_action(IN_APID number) 


is 
lc_nSetPlanId number :=0;
lc_nCount number :=0;
lc_nLocked number := 0;
lc_vPlanTitle varchar2(255);
begin


--we need to get the current setplan id this apid is associated with, to test if this is the last planned action for the set plan.
for rec in (select * from spd_t_list_set_plans where setplanid in (select setplanid from spd_t_inter_set_plans where apid = IN_APID) and rownum = 1)
loop
  lc_nSetPlanId := rec.setplanid;
  lc_vPlanTitle := rec.plantitle;
end loop;


select count(*) into lc_nLocked from spd_t_list_set_plans where setplanid = lc_nSetPlanId and money_approved = 1;
--test if this planned action is associated with a locked set plan. if it is then we cant delete the planned action
if lc_nLocked = 1 then
--this is locked, dont do anything with it
  tobrowser.message('no action','This planned action is associated with a locked funding request (' || lc_vPlanTitle || '), no action taken');
else
--this is open, go nuts
  select count(*) into lc_nCount from spd_t_inter_set_plans where setplanid = lc_nSetPlanId;
  
  if lc_nCount = 0 then
--this set plan is empty, lets remove it for now. oracle will barf at this too if there are any child notese records.
--but first we get rid of any files that were associated with this set plan
    for rec in (select * from spd_t_list_set_plans where setplanid = lc_nSetPlanId)
    loop
      delete from spd_t_xml_files where xmlfileid = rec.exportfile;
      commit;
    end loop;
--remove the program oversight records
    delete from spd_t_program_oversight where setplanid = lc_nSetPlanId;
--delete the set plan
    delete from spd_t_list_set_plans where setplanid = lc_nSetPlanId;
    commit;
  end if;
  
--we have to see how many planned actions on this set plan have the same productline. If this is the last one, we must delete the program oversight record
  select 
    count(*) into lc_nCount
  from 
    spd_t_action_planner 
  where 
    apid in (select apid from spd_t_inter_set_plans where setplanid = lc_nSetPlanId) and 
    productline_listid in (select productline_listid from spd_t_action_planner where apid = IN_APID);
  
  if lc_nCount = 1 then
--this is the last one of its kind, dispose of it. but respectfully
      delete from spd_t_program_oversight where setplanid = lc_nSetPlanId and productline_listid in (select productline_listid from spd_t_action_planner where apid = IN_APID);
      commit;
  end if;
  
  
--delete the planned action inter record
  delete from spd_t_inter_set_plans where apid = IN_APID;
  commit;
  
  
--delete the planned action
  delete from spd_t_action_planner where apid = IN_APID;
  commit;
  
  tobrowser.message('success','Delete planned action');
end if;





exception
        when others then
        tobrowser.message('error', 'IN_APID ' || to_char(IN_APID) || ':' || 
        SQLERRM
        );


end;


procedure add_base_do(IN_APID number, IN_nCONTRACTLISTID number, IN_nWORKTYPELISTID number, IN_vDONAME varchar2, IN_nADOID number default 0) 


is

lc_nADOID number :=0;
lc_vMessage varchar2(255);
lc_vSuccess varchar2(255);
lc_vFolder varchar2(255);
lc_vDONAME varchar2(255) := IN_vDONAME;
lc_nLoadid number;


begin




--check to see if this delivery order name has been used before, if it has append a '_1' where the number is the amount of times the base do name has been used.
for rec in 
(
  select count(*) thecount from spd_t_actn_dos where 
  (
    SUBSTR(delivery_order_name, 1 ,INSTR(delivery_order_name, '_', 1, 1)-1) = IN_vDONAME
    or
    delivery_order_name = IN_vDONAME
  )
)
loop
  if rec.thecount > 0 then
    lc_vDONAME := IN_vDONAME || '_' || rec.thecount;
  end if;
end loop;

lc_vDONAME := replace(lc_vDONAME,' ','_');

/*
  test for passed in DOID, use that value if not 0 otherwise get new value from sequence
*/
if IN_nADOID <> 0 then
  lc_nADOID := IN_nADOID;
else
  select spd_s_actn_dos.nextval into lc_nADOID from dual;
end if;

insert into spd_t_actn_dos (adoid, apid, contract_listid, worktype_listid, delivery_order_name) values (lc_nADOID, IN_APID, IN_nCONTRACTLISTID,IN_nWORKTYPELISTID,lc_vDONAME);
insert into spd_t_do_ce_dc (adoid,directcost_listid) 
  select
    lc_nADOID,
    listid 
  from 
    spd_t_lists 
  where 
    listname = 'list_delivery_order_direct_cost_types' 
    and grouping = IN_nWORKTYPELISTID
  order by 
    visualorder;

--add_do_prerequisite_rows(lc_nADOID,'GCE_download_pre_requisits');
--add_do_prerequisite_rows(lc_nADOID,'list_delivery_orders_milestones');

commit;

--Add the folder to the LOAD directory using spiders_utl.utils
lc_vFolder := 'SPD_' || xml_tools.folderid(lc_vDONAME, lc_nADOID,1);
--add record to the spd_t_load_progress table

select spd_s_load_progress.nextval into lc_nLoadid from dual;
insert into spd_t_load_progress (loadid, foldername, timestamp) values (lc_nLoadid, lc_vFolder, sysdate);
commit; 

spiders_utl.utils.create_spiders_directory (
lc_vDONAME,
lc_vDONAME, 
lc_vMessage, 
lc_vSuccess);--(p_pdir_name=>lc_vDONAME,p_ldir_name=>lc_vFolder,p_message=>lc_vMessage,p_success=>lc_vSuccess);
  
  if lc_vSuccess = '{"message":"success"}' then
    update spd_t_load_progress set uploadtoftp = 'Not Confirmed' where loadid = lc_nLoadid;
  else
    update spd_t_load_progress set uploadtoftp = 'Folder Not Created' where loadid = lc_nLoadid;
  end if;
commit;






tobrowser.message('success', lc_nADOID);

exception
        when others then
        tobrowser.message('error', 'IN_APID ' || to_char(IN_APID) || ':' || 
        SQLERRM
        );

end;

procedure del_delivery_order(IN_ADOID number) 


is 
lc_nCount number :=0;
lc_nDeliverables number :=0;
lc_nAssetInventory number :=0;
lc_nFacilities number :=0;
lc_nFacilityDC number :=0;
lc_nFacilityLabor number :=0;
lc_nFacilityMeta number :=0;


begin
--Ok, a if a delivery order has facilities, we can not delete the do, if it doesnt, then we need to delete the data in the other table that relates to the do.
/*
do_ce
do_ce_dc
do_ce_labor
do_progress

if there is a record in any of the following, you can not delete until you remove the record
do_deliverables (shouldnt have anything here)
do_fac_asset_inventory (shouldnt have anything here)
do_facilities (shouldnt have anything here)
do_facility_ce_dc (shouldnt have anything here)
do_facility_ce_labor (shouldnt have anything here)
do_facility_meta_data (shouldnt have anything here)
do_supporting_files (shouldnt have anything here)

*/

select count(*) into lc_nDeliverables from spd_t_do_deliverables where adoid = IN_ADOID ;
select count(*) into lc_nAssetInventory from spd_t_do_fac_asset_inventory where dofid in (select distinct dofid from spd_t_do_facilities where adoid = IN_ADOID) ;
select count(*) into lc_nFacilities from spd_t_do_facilities where adoid = IN_ADOID ;
select count(*) into lc_nFacilityDC from spd_t_do_facility_ce_dc where dofid in (select distinct dofid from spd_t_do_facilities where adoid = IN_ADOID) ;
select count(*) into lc_nFacilityLabor from spd_t_do_facility_ce_labor where dofid in (select distinct dofid from spd_t_do_facilities where adoid = IN_ADOID) ;
select count(*) into lc_nFacilityMeta from spd_t_do_facility_meta_data where dofid in (select distinct dofid from spd_t_do_facilities where adoid = IN_ADOID);
--select count(*) from spd_t_do_supporting_files where adoid = IN_ADOID !!!need to change this table from an apid to an adoid;

lc_nCount := 
lc_nDeliverables +
lc_nAssetInventory +
lc_nFacilities +
lc_nFacilityDC +
lc_nFacilityLabor +
lc_nFacilityMeta;


if lc_nCount = 0 then
  
  
  delete from spd_t_confirmation where adoid = IN_ADOID;
  delete from spd_t_do_ce_labor where adoid = IN_ADOID;
  delete from spd_t_do_ce_dc where adoid = IN_ADOID;
  delete from spd_t_do_ce where adoid = IN_ADOID;
  delete from spd_t_do_progress where adoid = IN_ADOID;
  delete from spd_t_actn_dos where adoid = IN_ADOID;
  commit;
  
  
  
  tobrowser.message('success', 'delete of delivery order');
else

  tobrowser.message('error', 'Deliverables ' || lc_nDeliverables || ', Asset Inventory ' || lc_nAssetInventory || ', Facilities ' || lc_nFacilities || ', Fac DC ' || lc_nFacilityDC || ', Fac Labor ' || lc_nFacilityMeta);

end if;


exception
        when others then
        tobrowser.message('error', 'IN_ADOID ' || to_char(IN_ADOID) || ':' || 
        SQLERRM
        );


end;



procedure add_do_prerequisite_rows(IN_nADOID number, IN_vCONFIRM_GROUP varchar2)


is


begin
--may want to check to see if we have these already so we dont duplicate, but for now no.
insert into spd_t_confirmation (adoid, confirmation_name_listid, confirm) select in_nAdoid, listid, 0 from spd_t_lists where listname = in_vConfirm_group;
commit;

exception
        when others then
        tobrowser.message('error', 'IN_ADOID ' || to_char(IN_nADOID) || ':' || 
        SQLERRM
        );


end;




procedure upd_do_confirmation(IN_nCID number, IN_nVALUE number, IN_vUSER varchar2)

is

begin

update spd_t_confirmation set confirm = IN_nVALUE, created_by = IN_vUSER, timestamp = sysdate where CID = in_ncid;
commit;

htp.p('success');

exception
        when others then
        tobrowser.message('error', 'IN_nCID ' || to_char(IN_nCID) || ':' ||
        'IN_nVALUE ' || to_char(IN_nVALUE) || ':' ||  
        SQLERRM
        );
        
end;





procedure upd_do_confirmation_fac_si(IN_nDOFID number, IN_nVALUE number, IN_vUSER varchar2)

is

begin

update spd_t_do_facilities set confirm = IN_nVALUE where dofid = IN_nDOFID;
commit;

test_do_confirmation_fac_si(in_ndofid, IN_vUSER);

htp.p('success');

exception
        when others then
        tobrowser.message('error', 'IN_nDOFID ' || to_char(IN_nDOFID) || ':' ||
        'IN_nVALUE ' || to_char(IN_nVALUE) || ':' ||  
        SQLERRM
        );
        
end;




procedure test_do_confirmation_fac_si(IN_nDOFID number, IN_vUSER varchar2)

is

lc_nCountNo number :=0;
lc_nCID number := 0;


begin

select count(*) into lc_nCountNo from spd_t_do_facilities where adoid in (select adoid from spd_t_facilities where dofid = in_ndofid) and confirm = 0;


for rec in (
              select 
                confirm.cid 
              from 
                spd_t_confirmation confirm
              where 
                confirm.confirmation_name_listid in (select listid from spd_t_lists where listname = 'GCE_download_pre_requisits' and returnvalue = 'SpecialInstructions')
                and confirm.adoid in (select adoid from spd_t_do_facilities where dofid = in_ndofid)
          )
loop
  lc_nCID := rec.cid;
end loop;

if lc_nCountNo = 0 then
  --all spec instr locked in, update the confirm list to 1
  upd_do_confirmation(lc_nCID,1,IN_vUSER);
else
  upd_do_confirmation(lc_nCID,0,'');
end if;


htp.p('success');

exception
        when others then
        tobrowser.message('error', 'IN_nCID ' || to_char(IN_nDOFID) || ':' ||
        SQLERRM
        );
        
end;


procedure upd_planned_action(IN_nAPID number, IN_nFundsource number, in_nInhouse number, in_nContractor number, in_nYear number, in_nQuarter number)

is
lc_nSetPlanId number :=0;
lc_vPlanTitle varchar2(255);
lc_nLocked number :=0;

begin


--we need to get the current setplan id this apid is associated with, to test if this is the last planned action for the set plan.
for rec in (select * from spd_t_list_set_plans where setplanid in (select setplanid from spd_t_inter_set_plans where apid = IN_nAPID) and rownum = 1)
loop
  lc_nSetPlanId := rec.setplanid;
  lc_vPlanTitle := rec.plantitle;
end loop;


select count(*) into lc_nLocked from spd_t_list_set_plans where setplanid = lc_nSetPlanId and money_approved = 1;
--test if this planned action is associated with a locked set plan. if it is then we cant delete the planned action
if lc_nLocked = 1 then
--this is locked, dont do anything with it
  tobrowser.message('no action','This planned action is associated with a locked funding request (' || lc_vPlanTitle || '), no action taken');
else
--this is open, go nuts
  
  update 
    spd_t_action_planner 
  set 
    planned_fund_source_listid = IN_nFundsource,
    planned_inhouse_amt = in_nInhouse,
    planned_ktr_amt = in_ncontractor,
    planned_year = in_nyear, 
    planned_qtr = in_nquarter
  where apid = in_nApid;
  commit;
  
  tobrowser.message('success','Planned Action updated');
end if;




exception
        when others then
        tobrowser.message('error', 'in_napid ' || to_char(in_napid) || ':' ||
        'IN_nFundsource ' || to_char(IN_nFundsource) || ':' || 
        'in_nInhouse ' || to_char(in_nInhouse) || ':' || 
        'in_nContractor ' || to_char(in_nContractor) || ':' ||  
        'in_nYear ' || to_char(in_nYear) || ':' ||  
        'in_nQuarter ' || to_char(in_nQuarter) || ':' ||  
        SQLERRM
        );
        
end;



procedure add_set_plan(IN_vNote varchar2, IN_nPlanTypeListID number default 0, IN_vBeginYear varchar2, IN_vEndYear varchar2, IN_nFundsource_Listid number, IN_vUser varchar2)

is 

lc_nSetPlanId number :=0;
lc_nNotesKey number :=0;
lc_nCount number :=1;
lc_nAmtInh number :=0;
lc_nAmtKtr number :=0;
lc_vFundsource varchar2(255) :='na';
lc_vPlanType varchar2(255) :='na';
lc_nContinue number :=0;
lc_nPlanTypeListID number :=0;

begin


--Funding Requests can only be made if there are planned actions for the year and fundsource
select count(*) into lc_nContinue from spd_t_action_planner where planned_year = IN_vBeginYear and planned_fund_source_listid = IN_nFundsource_Listid;

if lc_nContinue > 0 then
    if IN_nPlanTypeListID = 0 then
      select listid into lc_nPlanTypeListID from spd_t_lists where listname = 'set_plan_types' and displayvalue = 'Funding Request'; 
    else
      lc_nPlanTypeListID := IN_nPlanTypeListID;
    end if;
    
    --Add the record to spd_t_list_set_plans
      select spiders_data.spd_s_list_set_plans.nextval into lc_nSetPlanId from dual;
      select spiders_data.spd_s_noteskey.nextval into lc_nNotesKey from dual;
    
    for rec in (select * from spd_t_list_set_plans where set_plan_type_listid = lc_nPlanTypeListID and basefy = in_vbeginyear and endfy = in_vendyear and fundsource_listid = in_nfundsource_listid)
    loop
      lc_nCount := lc_nCount + 1;
      delete from spd_t_inter_set_plans where setplanid = rec.setplanid;
      commit;
    end loop;
    
    for rec in (select displayvalue from spd_t_lists where listid = in_nfundsource_listid)
    loop
      lc_vFundsource := rec.displayvalue;
    end loop;
    
    for rec in (select displayvalue from spd_t_lists where listid = lc_nPlanTypeListID)
    loop
      lc_vPlanType := rec.displayvalue;
    end loop;
    
    insert into 
      spiders_data.spd_t_list_set_plans
        (
          setplanid,
          set_plan_type_listid,
          basefy,
          endfy,
          fundsource_listid,
          created_by,
          plantitle,
          noteskey
        )
      values
        (
          lc_nSetplanid,
          lc_nPlanTypeListID,
          in_vbeginyear,
          in_vbeginyear,--we are making this a funding request only table. we will only make funding requests for a single year
          in_nfundsource_listid,
          in_vuser,
          lc_vfundsource || '_' || lc_vPlantype || '_' || in_vbeginyear || '_v' || lc_nCount,
          lc_nNoteskey
        );
    commit;
    
    --add the Initial note to the notes table
    insert into
      spiders_data.spd_t_notes
        (
          noteskey,
          note,
          created_by
        )
      values
        (
          lc_nNoteskey,
          'Initial Funding Request',
          in_vUser
        );
    commit;
    
    --add the Passed in note to the notes table
    /*
    insert into
      spiders_data.spd_t_notes
        (
          noteskey,
          note,
          created_by
        )
      values
        (
          lc_nNoteskey,
          in_vNote,
          in_vUser
        );
    commit;
    */
    
    
    insert into spd_t_inter_set_plans (setplanid, apid) select lc_nSetPlanId, apid from spd_t_action_planner where planned_year = in_vbeginyear and planned_fund_source_listid = in_nfundsource_listid;
    commit;
    
    
    tobrowser.message('success','Funding request record added');
else
    tobrowser.message('no action','Funding request record not added. No planned actions for the fundsource or year');
end if;
    

exception
        when others then
        tobrowser.message('error', 'IN_vNote ' || IN_vNote || ':' ||
        'IN_nPlanTypeListID ' || to_char(IN_nPlanTypeListID) || ':' || 
        'IN_vBeginYear ' || IN_vBeginYear || ':' ||
        'IN_vEndYear ' || IN_vEndYear || ':' ||
        'IN_nFundsource_Listid ' || to_char(IN_nFundsource_Listid) || ':' || 
        'IN_vUser ' || IN_vUser || ':' ||
        SQLERRM
        );
        
end;


procedure approve_set_plan(in_nSetPlanId number)

is 

begin

update spd_t_list_set_plans set money_approved = 1, money_approved_date = sysdate where setplanid = in_nSetPlanID;
commit;


        
tobrowser.message('success','Funding request record deleted');
    

exception
        when others then
        tobrowser.message('error', 'in_nSetPlanId:' || to_char(in_nSetPlanId) || ':' ||
        SQLERRM
        );

end;



procedure del_set_plan(in_nSetPlanId number)

is 

lc_nFundsourceListID number;
lc_vBaseFY varchar2(5);
begin



select fundsource_listid, basefy into lc_nFundsourceListID, lc_vBaseFY from spd_t_list_set_plans where setplanid = in_nsetplanid;


--fundsource
--year



delete 
from 
  spd_t_notes 
where 
  noteskey in (
                select 
                  noteskey 
                from 
                  spd_t_list_set_plans 
                where 
                  basefy = lc_vBaseFY and 
                  fundsource_listid = lc_nFundsourceListID
              );
commit;



--remove the inter 
delete from spd_t_inter_set_plans where setplanid = in_nsetplanid;
commit;

--remove the records for year and fundsource
delete from spd_t_list_set_plans where fundsource_listid = lc_nFundsourceListID and basefy = lc_vBaseFY;
commit;


tobrowser.message('success','Funding request record deleted');
    

exception
        when others then
        tobrowser.message('error', 'in_nSetPlanId:' || to_char(in_nSetPlanId) || ':' ||
        SQLERRM
        );


end;

procedure del_report_rec(IN_vREPORTID varchar2)

is

begin

  
  delete from spd_t_reports where reportid = IN_vREPORTID;
  commit;
  
  tobrowser.message('success','report record deleted');
  

exception
        when others then
        tobrowser.message('error', IN_vREPORTID ||
        SQLERRM
        );

end;


procedure add_report(IN_vREPORTTITLE varchar2, IN_vREPORTAUTHOR varchar2, IN_vESTSTARTDATE varchar2, IN_vCONTRACTOR varchar2, IN_vCONTRACTNUMBER varchar2, IN_vREPORTTYPE varchar2, IN_vDIVISION varchar2, IN_vREPORTNUM varchar2) 


IS
      
begin

  insert into spd_t_reports
    (reporttitle, reportauthor, eststartdate, contractor, contractnumber, createddate, reportnumber)
  values
    (IN_vREPORTTITLE, IN_vREPORTAUTHOR, IN_vESTSTARTDATE, IN_vCONTRACTOR, IN_vCONTRACTNUMBER, sysdate, IN_vREPORTTYPE||'-'||IN_vREPORTNUM||'-'||IN_vDIVISION);
  
  commit;
      
  
  tobrowser.message('success','report record added');
  

exception
        when others then
        tobrowser.message('error', IN_vREPORTTITLE ||
        SQLERRM
        );     
      /*
       CURSOR curREPORTS IS
            with firstQ as
                    (Select REPORTNUMBER, REPORTTITLE, REPORTAUTHOR, TO_CHAR(ESTSTARTDATE,'MM-DD-YYYY') ESTSTARTDATE, CONTRACTOR, CONTRACTNUMBER From SPD_T_REPORTS  Order By REPORTID DESC) -- ESTSTARTDATE DESC)--
            Select a.* from firstQ a where ROWNUM < 51; -- and REPORTID < 10820; ---not-work--10860;  ---works--10800; --10500;
       strJSON clob;
       i integer;
       p_sql varchar2(512);
       
       IN_REPORTID varchar2(512);
       NEW_REPORTID number;
       NEW_REPORTNUMBER varchar2(512);
       OLD_REPORTNUMBER varchar2(512);
       BEGIN
         IF IN_vREPORTTYPE IS NOT NULL THEN 
             IF IN_REPORTID IS NULL THEN --CREATE A NEW REPORTNUMBER
                 --Select SPD_S_REPORTS.NEXTVAL Into NEW_REPORTID From Dual;
                 
--                 IF Length(IN_FORMAT2)=5 THEN
--                    NEW_REPORTNUMBER:=fGETNEXTALTNUM(IN_FORMAT2);
--                 ELSE
                    Select IN_vREPORTTYPE||'-'||(max(SubStr(a.REPORTNUMBER,InStr(a.REPORTNUMBER,'-')+1,4))+1)||'-'||IN_vDIVISION  into NEW_REPORTNUMBER
                          From SPD_T_REPORTS a 
                            Where SubStr(a.REPORTNUMBER,1,Length(IN_vREPORTTYPE))=IN_vREPORTTYPE;
                    IF InStr(NEW_REPORTNUMBER,'--')>0 THEN
                      NEW_REPORTNUMBER:=Replace(NEW_REPORTNUMBER,'--','-6000-');
                    END IF;
--                  END IF;

                 INSERT INTO SPD_T_REPORTS (REPORTID, REPORTNUMBER, REPORTTITLE, REPORTAUTHOR,  CONTRACTOR, CONTRACTNUMBER, ESTSTARTDATE) values 
                                                 (NEW_REPORTID, NEW_REPORTNUMBER, IN_vREPORTTITLE, IN_vREPORTAUTHOR, IN_vCONTRACTOR, IN_vCONTRACTNUMBER, IN_vESTSTARTDATE);
                 htp.prn('<BR>Created Report Number: '||NEW_REPORTNUMBER);--NEW_REPORTNUMBER);
             ELSE --UPDATE CURRENT REPORT
                Select REPORTNUMBER Into OLD_REPORTNUMBER From SPD_T_REPORTS Where REPORTID = IN_REPORTID;
                IF instr(OLD_REPORTNUMBER,'(')=0 THEN
                  with AfterFirstDash as
                    (select substr(OLD_REPORTNUMBER,instr(OLD_REPORTNUMBER,'-')+1) theval from dual)
                  Select IN_vREPORTTYPE||'-'||substr(theval,1,instr(theval,'-')-1)||'-'||IN_vDIVISION Into NEW_REPORTNUMBER from AfterFirstDash;
                ELSE
                  NEW_REPORTNUMBER:=OLD_REPORTNUMBER;
                END IF;
                
                 UPDATE SPD_T_REPORTS Set REPORTNUMBER=NEW_REPORTNUMBER --IN_vREPORTTYPE||'-'||IN_REPORTID||'-'||IN_vDIVISION
                                                 ,REPORTTITLE=IN_vREPORTTITLE
                                                 ,REPORTAUTHOR=IN_vREPORTAUTHOR
                                                 ,CONTRACTOR=IN_vCONTRACTOR
                                                 ,CONTRACTNUMBER=IN_vCONTRACTNUMBER
                                                 ,ESTSTARTDATE=IN_vESTSTARTDATE
                   WHERE REPORTID = IN_REPORTID;
                   htp.prn('<BR>Updated Report Number: '||NEW_REPORTNUMBER);  --||IN_vREPORTTYPE||'-'||IN_REPORTID||'-'||IN_vDIVISION);
             END IF;
         ELSE
           strJSON := '{"ResultSet":{"firstResultPosition":1,"Result":[ ';
           i:=0;
           --FOR rec IN curREPORTS LOOP
             --  if i=1 then strJSON:=strJSON||','; else i:=1; end if;
             --  strJSON := strJSON  ||'{"REPORTID":"'||to_char(rec.REPORTID)||'"';
               --strJSON := strJSON  ||',"REPORTNUMBER":"' || to_char(rec.REPORTNUMBER)||'"';
             --  strJSON := strJSON  ||',"REPORTTITLE":"' || replace(rec.REPORTTITLE,chr(10),' ') ||'"';
            --   strJSON := strJSON  ||',"REPORTAUTHOR":"' || replace(rec.REPORTAUTHOR,chr(10),' ') ||'"';
            --   strJSON := strJSON  ||',"ESTSTARTDATE":"' || to_char(rec.ESTSTARTDATE) ||'"';
            --   strJSON := strJSON  ||',"CONTRACTOR":"'|| replace(rec.CONTRACTOR,chr(10),' ') ||'"';
            --   strJSON := strJSON  ||',"CONTRACTNUMBER":"' || to_char(rec.CONTRACTNUMBER) ||'"}';
          -- END LOOP;
        --   strJSON := strJSON ||'] }}';
           --htpCLOB(strJSON);
           --htp.prn(strJSON);
         END IF;
         */
END;


procedure update_report_rec(IN_vREPORTID varchar2, IN_vupd_report_title varchar2, IN_vupd_report_author varchar2, IN_vupd_report_Contractor varchar2, IN_vupd_report_Contract varchar2, IN_vupd_Report_StartDate varchar2)

is 

begin

update spd_t_reports set REPORTTITLE = IN_vupd_report_title, REPORTAUTHOR = IN_vupd_report_author, CONTRACTOR = IN_vupd_report_Contractor, CONTRACTNUMBER = IN_vupd_report_Contract, ESTSTARTDATE = IN_vupd_Report_StartDate where reportid = IN_vREPORTID;
commit;

htp.p('success report num changed');

exception
        when others then
        tobrowser.message('error', 'in_nSetPlanId ' || to_char(IN_vREPORTID) || ':' ||
        SQLERRM
        );
        
end;


procedure add_note(IN_nNOTESKEY number, IN_vNote varchar2, IN_vContact varchar2 default '', in_vToolname varchar2)

is

begin

insert into
  spiders_data.spd_t_notes
    (
      noteskey,
      note,
      contact,
      toolname,
      created_by
    )
  values
    (
      IN_nNOTESKEY,
      in_vnote,
      IN_vContact,
      in_vtoolname,
      APEX_UTIL.GET_SESSION_STATE('APP_USER')
    );
commit;

tobrowser.message('success','Added note');
    

exception
        when others then
        tobrowser.message('error', 'IN_nNOTESKEY:' || to_char(IN_nNOTESKEY) || ',IN_vNote:' || IN_vNote|| ',IN_vContact:' || IN_vContact|| ',in_vToolname' || in_vToolname|| ':' ||
        SQLERRM
        );
        
end;

procedure del_note(IN_nNOTEID number)

is

begin

delete from spd_t_notes where noteid = in_nNoteid;
commit;

tobrowser.message('success','Deleted note');
    

exception
        when others then
        tobrowser.message('error', 'IN_nNOTEID:' || to_char(IN_nNOTEID) || ':' ||
        SQLERRM
        );
        
end;


procedure upd_spec_instr_val(IN_nAFSIID number, IN_vVal varchar2)


is

begin

update spd_t_do_fac_spec_inst set spec_instr_value = IN_vVal where afsiid = IN_nAFSIID;
commit;

tobrowser.message('success','Updated special instruction value');
    

exception
        when others then
        tobrowser.message('error', 'IN_nAFSIID:' || to_char(IN_nAFSIID) || ', 
        IN_vVal:' || IN_vVal || ':' ||
        SQLERRM
        );
        
end;


procedure upd_planned_actn_rec_funds(IN_nAPID number, IN_vReserve varchar2, in_vRecDate varchar2, in_vRecInh varchar2, in_vRecKtr varchar2)

is 

lc_vRecDate varchar2(200);
lc_vRecInh varchar2(20);
lc_vRecKtr varchar2(20);
lc_vLocked varchar2(1);
lc_nSumRecInh number :=0;
lc_nSumRecKtr number :=0;
lc_nPlanInh number :=0;
lc_nPlanKtr number :=0;

begin

lc_vRecDate :=in_vRecDate;
lc_vRecInh  :=in_vRecInh;
lc_vRecKtr  :=in_vRecKtr;

if in_vRecDate = '' then
lc_vRecDate := null;
end if;

if in_vRecInh = '' then
lc_vRecInh := null;
end if;

if in_vRecInh = '' then
lc_vRecInh := null;
end if;

update spd_t_action_planner set reserve_flag = IN_vReserve, rec_date = lc_vRecDate, rec_inhouse_amt = lc_vRecInh, rec_ktr_amt = lc_vRecKtr where apid = in_napid;
commit;

lc_vLocked := set_plan_locked_flag(IN_nAPID);


--get the values, locked, rec inh/rec ktr and spit that back out to the browser
for rec in (select sum(rec_inhouse_amt) rec_inh, sum(rec_ktr_amt) rec_ktr,sum(planned_inhouse_amt) planned_inh, sum(planned_ktr_amt) planned_ktr from spd_t_action_planner where apid = in_nApid)
loop
  lc_nSumRecInh := rec.rec_inh;
  lc_nSumRecKtr := rec.rec_ktr;
  lc_nPlanInh := rec.planned_inh;
  lc_nPlanKtr := rec.planned_ktr;
end loop;

tobrowser.message('success',lc_vLocked || ',' || lc_nSumRecInh || ',' || lc_nSumRecKtr || ',' || lc_nPlanInh || ',' || lc_nPlanKtr);


    

exception
        when others then
        tobrowser.message('error', 'IN_nAPID:' || to_char(IN_nAPID) || ':' ||
        SQLERRM
        );

end;



procedure upd_oversight(IN_nPOID number, IN_nValQ1 number, IN_nValQ2 number, IN_nValQ3 number, IN_nValQ4 number)


is

lc_nSum number :=0;

begin

update 
  spd_t_program_oversight 
set 
  program_oversight_amt_q1 = IN_nValQ1,
  program_oversight_amt_q2 = IN_nValQ2,
  program_oversight_amt_q3 = IN_nValQ3,
  program_oversight_amt_q4 = IN_nValQ4
where 
  programoversightid = IN_nPOID;
commit;


--sum up the values and spit it back out to the calling procedure.
for rec in (
              select 
                decode(program_oversight_amt_q1,null,0,program_oversight_amt_q1) program_oversight_amt_q1, 
                decode(program_oversight_amt_q2,null,0,program_oversight_amt_q2) program_oversight_amt_q2,
                decode(program_oversight_amt_q3,null,0,program_oversight_amt_q3) program_oversight_amt_q3,
                decode(program_oversight_amt_q4,null,0,program_oversight_amt_q4) program_oversight_amt_q4
              from 
                spd_t_program_oversight 
              where 
                setplanid in (select setplanid from spd_t_program_oversight where programoversightid = in_nPOID)
          )
loop
  lc_nSum := lc_nSum + (rec.program_oversight_amt_q1 + rec.program_oversight_amt_q2 + rec.program_oversight_amt_q3 + rec.program_oversight_amt_q4);
end loop;

tobrowser.message('success',to_char(lc_nSum));
    

exception
        when others then
        tobrowser.message('error', 'IN_nPOID:' || to_char(IN_nPOID) || ', 
        IN_vVal:' || to_char(IN_nValQ1) || ':' ||
        SQLERRM
        );
        
end;

END datareceiver;

/
