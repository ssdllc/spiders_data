--------------------------------------------------------
--  DDL for Package Body PKG_COST_ESTIMATE
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE PACKAGE BODY "SPIDERS_DATA"."PKG_COST_ESTIMATE" as 

function 

f_test_for_error (in_vReturned varchar2) return varchar2 as 
  /* template variables */
  lc_vFunctionName varchar2(255) := 'f_test_for_error';
  lc_vReturn varchar2(32000) := '{"error":"default"}'; --defaul the return to e for error
  lc_nContinue number :=0; --default to 0 to dissalow continuing
  
  /* function specific variables */
  lc_vTest varchar2(32000) := in_vReturned;
begin

  if substr(lc_vTest, 1, 9) = '{"error":' then
    lc_vReturn := '0';
  else
    lc_vReturn := '1';
  end if;
  
  return lc_vReturn;

exception     
  when others then
      execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' dml error: ' || sqlerrm || ''')';
      return '{"error":"' || sqlerrm || '"}';
end; 


function getAttributeValue(in_vAttribute varchar2, in_jJSON json) return varchar2 as 
  /* template variables */
  json_error EXCEPTION;
  spiders_error EXCEPTION;
  lc_vErrorDescription varchar2(255) := 'default';
  lc_vFunctionName varchar2(255) := 'getAttributeValue';
  lc_vReturn varchar2(32000) := '{"error":"default"}'; --defaul the return to e for error
  lc_nContinue number :=0; --default to 0 to dissalow continuing
  
  /* function specific variables */
  lc_jvValue json_value;
begin
  
  if(in_jJSON.exist(in_vAttribute)) then       
    lc_jvValue := in_jJSON.get(in_vAttribute);
    lc_vReturn := getVal(lc_jvValue);
  else
    lc_vErrorDescription := in_vAttribute || ' attribute not found in json';
    raise spiders_error;
  end if;
  
  /* get name of object */
  return lc_vReturn;

exception     
  when spiders_error then
      execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' SPIDERS error ' || lc_vErrorDescription || ''')';
      return '{"error":"' || lc_vErrorDescription || '"}';
  when json_error then
      execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' JSON error'')';
      return '{"error":"json"}';
  when others then
      execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' dml error: ' || sqlerrm || ''')';
      return '{"error":"' || sqlerrm || '"}';
end; 
 

function getCorrespondingJsonColumnVal(in_jJson json, in_vColumn varchar2) return varchar2 as 
  /* template variables */
  json_error EXCEPTION;
  spiders_error EXCEPTION;
  lc_vErrorDescription varchar2(255) := 'default';
  lc_vFunctionName varchar2(255) := 'getCorrespondingJsonColumnVal';
  lc_vReturn varchar2(32000) := '{"error":"default"}'; --defaul the return to e for error
  lc_nContinue number :=0; --default to 0 to dissalow continuing
  
  /* function specific variables */
  lc_vName varchar2(2000);
  lc_vTemp varchar2(2000);
  lc_vComment varchar2(2000);
  lc_jvValue json_value;
  lc_jlData json_list;
  lc_jvRecVal json_value;
  lc_jValue json;
begin


  if(in_jJson.exist('data')) then
      if(in_jJson.get('data').is_array) then
          lc_jlData := json_list(in_jJson.get('data'));        
          lc_jvRecVal := lc_jlData.get(1); --return null on outofbounds
          if (lc_jvRecVal is not null) then
            lc_jValue := json(lc_jvRecVal);            
            if(lc_jValue.exist(in_vColumn)) then       
              lc_jvValue := lc_jValue.get(in_vColumn);
              lc_vReturn := getVal(lc_jvValue);
            else
              lc_vErrorDescription := in_vColumn || ' attribute not found in json';
              raise spiders_error;
            end if;
          else 
            lc_vErrorDescription := 'Rec val is null';
            raise spiders_error;
          end if;
      else
        lc_vErrorDescription := 'Data is not an array';
        raise spiders_error;
      end if;
  else
    lc_vErrorDescription := 'Data attribute not found in json';
    raise spiders_error;
  end if;

  return lc_vReturn;

exception     
  when spiders_error then
      execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' SPIDERS error ' || lc_vErrorDescription || ''')';
      return '{"error":"' || lc_vErrorDescription || '"}';
  when json_error then
      execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' JSON error'')';
      return '{"error":"json"}';
  when others then
      execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' dml error: ' || sqlerrm || ''')';
      return '{"error":"' || sqlerrm || '"}';
end; 

function getDeliveryOrderFileName(in_vDeliveryOrderId varchar2) return varchar2 as 
  /* template variables */
  json_error EXCEPTION;
  spiders_error EXCEPTION;
  lc_vErrorDescription varchar2(255) := 'default';
  lc_vFunctionName varchar2(255) := 'getDeliveryOrderFileName';
  lc_vReturn varchar2(32000) := '{"error":"default"}'; --defaul the return to e for error
  lc_nContinue number :=0; --default to 0 to dissalow continuing
  
  /* function specific variables */
  lc_vFilename varchar2(255);
begin

  lc_vFilename := 'NONE.xml';
  
  for rec in (
    select 
      nvl(replace(PWS_TYPE,' ','_'),'NOPWSNAME')        PWS_TYPE,
      nvl(replace(MAIN_LOCATION,' ','_'),'NOACTIVITY')  MAIN_LOCATION,
      nvl(replace(FUNDSOURCE,' ','_'),'NOFUNDSOURCE')   FUNDSOURCE,
      nvl(FY,'NOFY')                                    FY
    from 
      SPD_V_DT_DELIVERY_ORDERS
    where 
      deliveryorderid = in_vDeliveryOrderId
  )
  loop
    lc_vFilename := rec.pws_type || '_at_' || rec.MAIN_LOCATION || '_' || rec.FUNDSOURCE || '_' || rec.FY || '.xml';
  end loop;
  
  
  lc_vReturn := lc_vFilename;
  return lc_vReturn;

exception     
  when spiders_error then
      execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' SPIDERS error ' || lc_vErrorDescription || ''')';
      return '{"error":"' || lc_vErrorDescription || '"}';
  when json_error then
      execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' JSON error'')';
      return '{"error":"json"}';
  when others then
      execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' dml error: ' || sqlerrm || ''')';
      return '{"error":"' || sqlerrm || '"}';
end; 

function getSqlClob(in_vDeliveryOrderId varchar2) return clob as 
  /* template variables */
  json_error EXCEPTION;
  spiders_error EXCEPTION;
  lc_vErrorDescription varchar2(255) := 'default';
  lc_vFunctionName varchar2(255) := 'getSqlClob';
  lc_cReturn clob; --defaul the return to e for error
  lc_nContinue number :=0; --default to 0 to dissalow continuing
  
  /* function specific variables */
  lc_vSql long;
  l_xXML  xmltype;

begin
  lc_vSql := '
  select 
    delivery_order.DELIVERYORDERID          DELIVERYORDERID,
    contract.PRODUCTLINE_LISTID             PRODUCTLINE_LISTID,
    delivery_order.EIC                      EIC,
    contract.CONTRACT_NUMBER                CONTRACT_NUMBER,
    delivery_order.DELIVERY_ORDER_NUMBER    DELIVERY_ORDER_NUMBER,
    delivery_order.MAIN_LOCATION            MAIN_LOCATION,
    delivery_order.MAIN_LOCATION_UIC        MAIN_LOCATION_UIC,
    delivery_order.FUNDSOURCE               FUNDSOURCE,
    delivery_order.PROJECTED_START_DATE     PROJECTED_START_DATE,
    delivery_order.PWSID                    PWSID,
    pws.PWS_TYPE                            PWS_TYPE,
    contract.CONTRACTID                     CONTRACTID,
    cursor (
      select 
        delivery_order_fac.DELIVERYORDERFACILITYID    DELIVERYORDERFACILITYID,
        delivery_order_fac.FACILITY_NAME              FACILITY_NAME,
        delivery_order_fac.FACILITY_NUMBER            FACILITY_NUMBER,
        delivery_order_fac.NFAID                      NFAID,
        delivery_order_fac.INSPECT                    INSPECT,
        cursor (
          select 
            facility_instruction.dofacinstructionid   DOFACINSTRUCTIONID,
            instruction.grouping                      GROUPING,
            instruction.instruction                   INSTRUCTION
          from
            spd_t_do_fac_instructions facility_instruction,
            spd_t_instructions        instruction
          where
            facility_instruction.instructionid = instruction.instructionid
            and facility_instruction.dofacid = delivery_order_fac.deliveryorderfacilityid
        ) as Instructions
      from 
        spd_v_qc_delivery_order_facs    delivery_order_fac
      where 
        delivery_order_fac.DELIVERYORDERID = delivery_order.DELIVERYORDERID
    ) as Facilities,
    cursor (
      select 
        contractor.CONTRACTORID       CONTRACTORID,
        contractor.CONTRACTOR_NAME    CONTRACTOR_NAME,
        contract_contractor.OVERHEAD  OVERHEAD,
        contract_contractor.FRINGE    FRINGE,
        cursor (
            select 
              job_title.JOBTITLEID      JOBTITLEID,
              job_title.JOB_TITLE       JOB_TITLE,
              job_title.BASE_RATE       BASE_RATE,
              job_title.OPTION_1_RATE   OPTION_1_RATE,
              job_title.OPTION_2_RATE   OPTION_2_RATE,
              job_title.OPTION_3_RATE   OPTION_3_RATE,
              job_title.OPTION_4_RATE   OPTION_4_RATE
            from 
              spd_t_job_titles    job_title
            where 
              job_title.contractcontractorid = contract_contractor.contractcontractorid
          ) as Job_Titles
      from 
        spd_t_contractors             contractor,
        spd_t_contract_contractors    contract_contractor
      where 
        contractor.contractorid = contract_contractor.contractorid
        and contract_contractor.contractid = contract.contractid
    ) as Contractors,
    cursor (
      select
        direct_cost.DIRECTCOSTID      DIRECTCOSTID,
        direct_cost.DIRECT_COST       DIRECT_COST
      from 
        spd_t_direct_costs    direct_cost
      where 
        direct_cost.pwsid = pws.pwsid
        and direct_cost.direct_cost_type = ''GENERAL''
      order by
        direct_cost.visual_order
    ) as GeneralDirectCosts,
    cursor (
      select
        direct_cost.DIRECTCOSTID      DIRECTCOSTID,
        direct_cost.DIRECT_COST       DIRECT_COST
      from 
        spd_t_direct_costs    direct_cost
      where 
        direct_cost.pwsid = pws.pwsid
        and direct_cost.direct_cost_type = ''FACILITY''
      order by
        direct_cost.visual_order
    ) as FacilityDirectCosts,
    cursor (
      select
        labor_category.LABORCATEGORYID      LABORCATEGORYID,
        labor_category.LABOR_CATEGORY       LABOR_CATEGORY
      from 
        spd_t_labor_categories    labor_category
      where 
        labor_category.pwsid = pws.pwsid
        and labor_category.labor_category_type = ''GENERAL''
      order by
        labor_category.LABOR_CATEGORY
    ) as GeneralLaborCategories,
    cursor (
      select
        labor_category.LABORCATEGORYID      LABORCATEGORYID,
        labor_category.LABOR_CATEGORY       LABOR_CATEGORY
      from 
        spd_t_labor_categories    labor_category
      where 
        labor_category.pwsid = pws.pwsid
        and labor_category.labor_category_type = ''FACILITY''
      order by
        labor_category.LABOR_CATEGORY
    ) as FacilityLaborCategories
  from 
    spd_t_delivery_orders   delivery_order,
    spd_t_pwss              pws,
    spd_t_contracts         contract
  where
    delivery_order.pwsid = pws.pwsid
    and pws.contractid = contract.contractid
    and delivery_order.deliveryorderid = ' || in_vDeliveryOrderid;  
 
  -- Create the XML aus SQL
  l_xXml := itstar_xml_util.sql2xml(lc_vSql);
  lc_cReturn := l_xXml.getclobval();
  return lc_cReturn;

exception     
  when spiders_error then
      execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' SPIDERS error ' || lc_vErrorDescription || ''')';
      return '{"error":"' || lc_vErrorDescription || '"}';
  when json_error then
      execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' JSON error'')';
      return '{"error":"json"}';
  when others then
      execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' dml error: ' || sqlerrm || ''')';
      return '{"error":"' || sqlerrm || '"}';
end; 

function insertIntoXMLFilesTable(in_cClob clob,in_vFilename varchar2) return varchar2 as 
  /* template variables */
  json_error EXCEPTION;
  spiders_error EXCEPTION;
  lc_vErrorDescription varchar2(255) := 'default';
  lc_vFunctionName varchar2(255) := 'insertIntoXMLFilesTable';
  lc_vReturn varchar2(32000) := '{"error":"default"}'; --defaul the return to e for error
  lc_nContinue number :=0; --default to 0 to dissalow continuing
  
  /* function specific variables */
  lc_bBlob blob;
  lc_nXMLFileid number;
begin

  lc_bBlob := CLOB2BLOB(in_cClob);

  select SPIDERS_DATA.SPD_S_XML_FILES.nextval into lc_nXMLFileid from dual;


  insert into spd_t_xml_files (xmlfileid, mime_type, blob_content, apexname,  filename)
  values (lc_nXMLFileid,'text/xml',lc_bBlob, in_vFilename, in_vFilename);
  commit;

  lc_vReturn := lc_nXMLFileid;
  return lc_vReturn;

exception     
  when spiders_error then
      execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' SPIDERS error ' || lc_vErrorDescription || ''')';
      return '{"error":"' || lc_vErrorDescription || '"}';
  when json_error then
      execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' JSON error'')';
      return '{"error":"json"}';
  when others then
      execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' dml error: ' || sqlerrm || ''')';
      return '{"error":"' || sqlerrm || '"}';
end; 



function f_make_cost_estimate_xml(in_cJSON clob) return json as 
  /* template variables */
  json_error EXCEPTION;
  spiders_error EXCEPTION;
  lc_vErrorDescription varchar2(255) := 'default';
  lc_vFunctionName varchar2(255) := 'f_make_cost_estimate_xml';
  lc_vRest varchar2(255) := 'CREATEMODELCUSTOM';
  lc_jReturn json; 
  lc_nContinue number :=0; --default to 0 to dissalow continuing
  
  /* function specific variables */
  lc_vModule varchar2(255);
  lc_vTarget varchar2(255);
  lc_vIsAuthorized varchar2(255);
  lc_jParameterJson json;
  lc_vDELIVERYORDERID varchar2(255);
  lc_vFilename varchar2(1000);
  lc_vXMLFileId varchar2(255);
  lc_cSql clob;
  
begin

  lc_jReturn := json();
  lc_jReturn.put('error','nofileid');
  
  lc_jParameterJson := json(in_cJson);
  lc_vModule := getAttributeValue('module', lc_jParameterJson);
  lc_nContinue := f_test_for_error(lc_vModule);
  
  if lc_nContinue = 1 then
    lc_vTarget := getAttributeValue('target', lc_jParameterJson);
    lc_nContinue := f_test_for_error(lc_vTarget);
  else
    lc_vErrorDescription := 'Module attribute not found';
    raise spiders_error;
  end if;

/*
  if lc_nContinue = 1 then
    lc_vIsAuthorized := BB_F_AUTHORIZATION('BB_REST.create_',lc_vModule);
  else
    lc_vErrorDescription := 'target attribute not found';
    raise spiders_error; 
  end if; 
*/

  lc_vIsAuthorized := 'y'; --!!REMOVE AFTER TESTING

  if lc_vIsAuthorized = 'y' then
    lc_vDELIVERYORDERID := getCorrespondingJsonColumnVal(lc_jParameterJson, 'DELIVERYORDERID');
    lc_nContinue := f_test_for_error(lc_vDELIVERYORDERID);
  else
    lc_vErrorDescription := 'problem getting target function name';
    raise spiders_error;
  end if;
  

  if lc_nContinue = 1 then
    lc_vFileName := getDeliveryOrderFileName(lc_vDELIVERYORDERID);
    lc_nContinue := f_test_for_error(lc_vXMLFileId);
  else
    lc_vErrorDescription := 'problem getting deliveryorderid';
    raise spiders_error; 
  end if;  

  if lc_nContinue = 1 then
    lc_cSql := getSqlClob(lc_vDELIVERYORDERID);
    lc_nContinue := f_test_for_error(lc_vXMLFileId);
  else
    lc_vErrorDescription := 'problem getting deliveryorderid';
    raise spiders_error; 
  end if;  
  
  if lc_nContinue = 1 then
    lc_vXMLFileId := insertIntoXMLFilesTable(lc_cSql,lc_vFileName);
    lc_nContinue := f_test_for_error(lc_vXMLFileId);
  else
    lc_vErrorDescription := 'problem getting filename';
    raise spiders_error; 
  end if;
  
  if lc_nContinue = 1 then
    lc_jReturn := json();
    lc_jReturn.put('fileid',lc_vXMLFileId);
  else
    lc_vErrorDescription := 'problem inserting into xml table';
    raise spiders_error; 
  end if;

  return lc_jReturn;

exception     
  when spiders_error then
      execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' SPIDERS error ' || lc_vErrorDescription || ''')';
      return json('{"error":"' || lc_vFunctionName || ' SPIDERS error ' || lc_vErrorDescription || '"}');
  when json_error then
      execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' JSON error'')';
      return json('{"error":"' || lc_vFunctionName || ' JSON error ' || lc_vErrorDescription || '"}');
  when others then
      execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' dml error: ' || sqlerrm || ''')';
     return json('{"error":"' || lc_vFunctionName || ' dml error ' || sqlerrm || '"}');
end; 

end;

/
