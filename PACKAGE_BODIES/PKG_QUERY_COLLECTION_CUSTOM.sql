--------------------------------------------------------
--  DDL for Package Body PKG_QUERY_COLLECTION_CUSTOM
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE PACKAGE BODY "SPIDERS_DATA"."PKG_QUERY_COLLECTION_CUSTOM" as 

function f_test_for_error (in_vReturned varchar2) return varchar2 as 
  /* template variables */
  lc_vFunctionName varchar2(255) := 'f_test_for_error';
  lc_vReturn varchar2(32000) := '{"error":"default"}'; --defaul the return to e for error
  lc_nContinue number :=0; --default to 0 to dissalow continuing
  
  /* function specific variables */
  lc_vTest varchar2(32000) := in_vReturned;
begin
  insert into debug (input) values ('before');
  if substr(lc_vTest, 1, 9) = '{"error":' then
    lc_vReturn := '0';
  else
    lc_vReturn := '1';
  end if;
    insert into debug (input) values ('after');
  return lc_vReturn;

exception     
  when others then
      execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' dml error: ' || sqlerrm || ''')';
      return '{"error":"' || sqlerrm || '"}';
end; 


function getAttributeValue(in_vAttribute varchar2, in_jJSON json) return varchar2 as 
  /* template variables */
  json_error EXCEPTION;
  spiders_error EXCEPTION;
  lc_vErrorDescription varchar2(255) := 'default';
  lc_vFunctionName varchar2(255) := 'getAttributeValue';
  lc_vReturn varchar2(32000) := '{"error":"default"}'; --defaul the return to e for error
  lc_nContinue number :=0; --default to 0 to dissalow continuing
  
  /* function specific variables */
  lc_jvValue json_value;
begin
  
  if(in_jJSON.exist(in_vAttribute)) then       
    lc_jvValue := in_jJSON.get(in_vAttribute);
    lc_vReturn := getVal(lc_jvValue);
  else
    lc_vErrorDescription := in_vAttribute || ' attribute not found in json';
    raise spiders_error;
  end if;
  
  /* get name of object */
  return lc_vReturn;

exception     
  when spiders_error then
      execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' SPIDERS error ' || lc_vErrorDescription || ''')';
      return '{"error":"' || lc_vErrorDescription || '"}';
  when json_error then
      execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' JSON error'')';
      return '{"error":"json"}';
  when others then
      execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' dml error: ' || sqlerrm || ''')';
      return '{"error":"' || sqlerrm || '"}';
end; 

function f_parseWhere(in_jJson json, in_nFilterLength number) return varchar2 as
 /* template variables */
  lc_vFunctionName varchar2(255) := 'f_parseWhere';

  json_error EXCEPTION;
  spiders_error EXCEPTION;
  lc_vErrorDescription varchar2(255) := 'default';

  lc_vReturn varchar2(32000) := '{"error":"default"}'; --defaul the return to e for error
  lc_nContinue number :=0; --default to 0 to dissalow continuing

  
  /* function specific variables */
  lc_jlWhereClause json_list;
  lc_jlWhereItem json_list;
  lc_vColumnName varchar2(255);
  lc_vWhereValue varchar2(255);
  lc_vWhereClauseSegment long;
begin

  
  if(in_jJson.exist('whereClause')) then
    if(in_jJson.get('whereClause').is_array) then
      lc_jlWhereClause := json_list(in_jJson.get('whereClause'));
      for rec in 1..lc_jlWhereClause.count loop
      
        lc_jlWhereItem := json_list(lc_jlWhereClause.get(rec));
        lc_vColumnName := getVal(lc_jlWhereItem.get(1));
        lc_vWhereValue := getVal(lc_jlWhereItem.get(2));
          insert into debug (input) values ('lc_vColumnName : '||lc_vColumnName);
          insert into debug (input) values ('lc_vWhereValue : '||lc_vWhereValue);
        if rec = 1 then
          lc_vWhereClauseSegment := ' UPPER(' || lc_vColumnName || ') like ''%' || upper(lc_vWhereValue)  || '%''';
        else
          lc_vWhereClauseSegment := lc_vWhereClauseSegment || ' and UPPER(' || lc_vColumnName || ') like ''%' || upper(lc_vWhereValue)  || '%''';
        end if;
      end loop;
      
      if length(lc_vWhereClauseSegment) > 0 then
        lc_vWhereClauseSegment := 'AND (' || lc_vWhereClauseSegment || ')';
      end if;
      
      
    else
      lc_vErrorDescription := 'whereClause is not an array';
      lc_vWhereClauseSegment := '';
      --raise spiders_error;
    end if;
  else
    lc_vErrorDescription := 'whereClause does not exist';
    lc_vWhereClauseSegment := '';
    --raise spiders_error;
  end if;


  lc_vReturn := lc_vWhereClauseSegment;
  return lc_vReturn;

exception   
  when spiders_error then
      execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' SPIDERS error ' || lc_vErrorDescription || ''')';
      return '{"error":"' || lc_vErrorDescription || '"}';
  when json_error then
      execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' JSON error'')';
      return '{"error":"json"}';
  when others then
      execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' dml error: ' || sqlerrm || ''')';
      return '{"error":"' || sqlerrm || '"}';
end; 

function f_getSpecificWhere(in_vATTRIBUTE_TYPE varchar2, in_vVALUE_LIST varchar2, in_vATTRIBUTE_NAME varchar2, in_nATTRIBUTEID number, in_nRANGE_LOW number, in_nRANGE_HIGH number) return varchar2 as
 /* template variables */
  lc_vFunctionName varchar2(255) := 'f_parseWhere';

  json_error EXCEPTION;
  spiders_error EXCEPTION;
  lc_vErrorDescription varchar2(255) := 'default';

  lc_vReturn varchar2(32000) := '{"error":"default"}'; --defaul the return to e for error
  lc_nContinue number :=0; --default to 0 to dissalow continuing

  
  /* function specific variables */
  lc_vWhereSegment long;
begin

  if in_vATTRIBUTE_TYPE='NUMBER' then
    if in_vValue_List != null then
      lc_vWhereSegment := in_vATTRIBUTE_NAME || ' in ( select value_list from spd_v_qc_filter_sets where filtersetid = ' || v('SPD_FILTERSETID') || ' and attributeid = ' || in_nATTRIBUTEID || ') ';
    else
      lc_vWhereSegment := in_vATTRIBUTE_NAME || ' between ' || in_nRange_Low || ' and ' || in_nRange_High;
    end if;  
  else
      lc_vWhereSegment := in_vATTRIBUTE_NAME || ' in ( select value_list from spd_v_qc_filter_sets where filtersetid = ' || v('SPD_FILTERSETID') || ' and attributeid = ' || in_nATTRIBUTEID || ') ';
  end if;


  lc_vReturn := lc_vWhereSegment;
  return lc_vReturn;

exception   
  when spiders_error then
      execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' SPIDERS error ' || lc_vErrorDescription || ''')';
      return '{"error":"' || lc_vErrorDescription || '"}';
  when json_error then
      execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' JSON error'')';
      return '{"error":"json"}';
  when others then
      execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' dml error: ' || sqlerrm || ''')';
      return '{"error":"' || sqlerrm || '"}';
end; 

function f_getValueListOrRange return varchar2 as 
  /* template variables */
  json_error EXCEPTION;
  spiders_error EXCEPTION;
  lc_vErrorDescription varchar2(255) := 'default';
  lc_vFunctionName varchar2(255) := 'f_getValueListOrRange';
  lc_vReturn varchar2(32000) := '{"error":"default"}'; --defaul the return to e for error
  lc_nContinue number :=0; --default to 0 to dissalow continuing
  
  /* function specific variables */
  lc_vWhere long;
  lc_nCount number;
  lc_vSpecificWhere long;
begin

  lc_vWhere := '';
  lc_nCount := 1;

  for rec in (
    select
      filter_set.attributeid  ATTRIBUTEID,
      max(attribute.name)     ATTRIBUTE_NAME,
      max(value_list)         VALUE_LIST,
      max(type)               ATTRIBUTE_TYPE,
      max(range_low)          RANGE_LOW,
      max(range_high)         RANGE_HIGH
    from
      spd_v_qc_filter_sets  filter_set,
      spd_t_attributes      attribute
    where
      filter_set.attributeid = attribute.attributeid
      and filter_set.filtersetid = v('SPD_FILTERSETID')
    group by
      filter_set.attributeid
  ) loop
  
    lc_vSpecificWhere := f_getSpecificWhere(rec.ATTRIBUTE_TYPE, rec.VALUE_LIST, rec.ATTRIBUTE_NAME, rec.ATTRIBUTEID, rec.RANGE_LOW, rec.RANGE_HIGH);
    
    if lc_nCount = 1 then
      lc_vWhere := ' where ' || lc_vSpecificWhere; 
    else
      lc_vWhere := lc_vWhere || ' and ' || lc_vSpecificWhere;
    end if;
    lc_nCount := lc_nCount + 1;
  end loop;

  return lc_vWhere;

exception     
  when spiders_error then
      execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' SPIDERS error ' || lc_vErrorDescription || ''')';
      return '{"error":"' || lc_vErrorDescription || '"}';
  when json_error then
      execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' JSON error'')';
      return '{"error":"json"}';
  when others then
      execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' dml error: ' || sqlerrm || ''')';
      return '{"error":"' || sqlerrm || '"}';
end;

function f_getFilterWhereClause return varchar2 as 
  /* template variables */
  json_error EXCEPTION;
  spiders_error EXCEPTION;
  lc_vErrorDescription varchar2(255) := 'default';
  lc_vFunctionName varchar2(255) := 'f_getFilterWhereClause';
  lc_vReturn varchar2(32000) := '{"error":"default"}'; --defaul the return to e for error
  lc_nContinue number :=0; --default to 0 to dissalow continuing
  
  /* function specific variables */
  lc_vWhere long;
  lc_nCount number;
  lc_vSpecificWhere long;
begin

  lc_vWhere := '';
  lc_nCount := 1;

  for rec in (
    select
      filter_set.attributeid  ATTRIBUTEID,
      max(attribute.name)     ATTRIBUTE_NAME,
      max(value_list)         VALUE_LIST,
      max(type)               ATTRIBUTE_TYPE,
      max(range_low)          RANGE_LOW,
      max(range_high)         RANGE_HIGH
    from
      spd_v_qc_filter_sets  filter_set,
      spd_t_attributes      attribute
    where
      filter_set.attributeid = attribute.attributeid
      and filter_set.filtersetid = v('SPD_FILTERSETID')
    group by filter_set.ATTRIBUTEID
  ) loop
  
    lc_vSpecificWhere := f_getSpecificWhere(rec.ATTRIBUTE_TYPE, rec.VALUE_LIST, rec.ATTRIBUTE_NAME, rec.ATTRIBUTEID, rec.RANGE_LOW, rec.RANGE_HIGH);
    
    if lc_nCount = 1 then
      lc_vWhere := ' where ' || lc_vSpecificWhere; 
    else
      lc_vWhere := lc_vWhere || ' and ' || lc_vSpecificWhere;
    end if;
    lc_nCount := lc_nCount + 1;
  end loop;

  return lc_vWhere;

exception     
  when spiders_error then
      execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' SPIDERS error ' || lc_vErrorDescription || ''')';
      return '{"error":"' || lc_vErrorDescription || '"}';
  when json_error then
      execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' JSON error'')';
      return '{"error":"json"}';
  when others then
      execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' dml error: ' || sqlerrm || ''')';
      return '{"error":"' || sqlerrm || '"}';
end;

function f_perform_read_qry_collection(in_jJson json, in_vModule varchar2, in_vTarget varchar2, in_vTableName varchar2) return json as 
  /* template variables */
  json_error EXCEPTION;
  spiders_error EXCEPTION;
  lc_vErrorDescription varchar2(255) := 'default';
  lc_vPrefix varchar2(255) := 'SPD_V_QC_';
  lc_vFunctionName varchar2(255) := 'f_perform_read_qry_collection';
  lc_vReturn varchar2(32000) := '{"error":"default"}'; --defaul the return to e for error
  lc_nContinue number :=1; --default to 0 to dissalow continuing
  
  /* function specific variables */
  lc_jJson json;
  lc_vSql long;
  lc_vOrderBy long;
  lc_vFilterWhere long := '';
  lc_vSearchWhere long;
begin

  for rec in (
    select
      *
    from
      spd_t_query_defs
    where
      rest_endpoint = 'READQUERYCOLLECTIONCUSTOM'
      and module = in_vModule
      and target = in_vTarget
  ) loop

    if lc_nContinue = 1 then
      lc_vFilterWhere := f_getFilterWhereClause; 
      lc_nContinue := f_test_for_error(lc_vFilterWhere);
    else
      lc_vErrorDescription := 'Sql statement not found';
      raise spiders_error; 
    end if;

    if lc_nContinue = 1 then
      lc_vSearchWhere := f_parseWhere(in_jJson, length(rec.outer_where_text)); 
      lc_nContinue := f_test_for_error(lc_vSearchWhere);
    else
      lc_vErrorDescription := 'Sql statement not found';
      raise spiders_error; 
    end if;
    
    lc_vSql := rec.outer_select_text || ' ' || rec.select_text || ' ' || rec.from_text || ' ' || lc_vFilterWhere || ' ' || rec.group_by_text || ' ' || rec.order_by_text || ' ' || rec.outer_where_text || ' ' || lc_vSearchWhere;
    insert into debug (INPUT) values ('lc_vSql : '||lc_vSql);
    lc_nContinue :=1;
    
  end loop;

  --htp.p(lc_vSql);
  
  lc_jJson := json_dyn.executeObject(lc_vSql);

  return lc_jJson;

exception     
  when spiders_error then
      execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' SPIDERS error ' || lc_vErrorDescription || ''')';
      return json('{"error":"' || lc_vErrorDescription || '"}');
  when json_error then
      execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' JSON error'')';
      return json('{"error":"json"}');
  when others then
      execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' dml error: ' || sqlerrm || ''')';
      return json('{"error":"' || sqlerrm || '"}');
end;


function f_read(in_cJSON clob) return JSON as 
  /* template variables */
  json_error EXCEPTION;
  spiders_error EXCEPTION;
  lc_vErrorDescription varchar2(255) := 'default';
  lc_vFunctionName varchar2(255) := 'readQueryCollectionCustom';
  lc_vRest varchar2(255) := 'readQueryCollectionCustom';
  lc_vReturn varchar2(32000) := '{"error":"default"}'; --defaul the return to e for error
  lc_nContinue number :=0; --default to 0 to dissalow continuing
  
  /* function specific variables */
  lc_vTarget varchar2(255);
  lc_vModule varchar2(255);
  lc_vTableName varchar2(255);
  lc_jJson json;
  lc_jParameterJson json;
  lc_jResults json;
  lc_jMessages json;
  lc_jlMessages json_list;
  lc_jlDML json_list;
  lc_vDMLResult varchar2(1);
  lc_vIsAuthorized varchar2(1);
  lc_vDebug varchar2(2000);
  lc_vSql long;
  lc_vPrimaryKeyValue varchar2(255);
  lc_vPrimaryKeyColumn varchar2(255);

begin
  lc_jJson := json();
  /*!! I LEFT OFF just at the end of the createModel function, have not tested*/
  insert into debug (input) values ('howdy ho');
  lc_jParameterJson := json(in_cJson);
  insert into debug (input) values ('converted parameter json: '|| in_cJson);
  lc_vModule := getAttributeValue('module', lc_jParameterJson);
  lc_nContinue := f_test_for_error(lc_vModule);
  
  
  
  if lc_nContinue = 1 then
    lc_vTarget := getAttributeValue('target', lc_jParameterJson);
    lc_nContinue := f_test_for_error(lc_vTarget);
  else
    lc_vErrorDescription := 'Module attribute not found';
    raise spiders_error;
  end if;
  
/*
  if lc_nContinue = 1 then
    lc_vIsAuthorized := BB_F_AUTHORIZATION('BB_REST.create_',lc_vModule);
  else
    lc_vErrorDescription := 'target attribute not found';
    raise spiders_error; 
  end if; 
*/

  lc_vIsAuthorized := 'y'; --!!REMOVE AFTER TESTING
  
  if lc_vIsAuthorized = 'y' then
  insert into debug (input) values ('just before get target table name');
    lc_vTableName := getTargetTableName('READQUERYCOLLECTIONCUSTOM',lc_vModule,lc_vTarget);
    insert into debug (input) values ('lc_vTableName : '||lc_vTableName||' lc_vModule : '|| lc_vModule||' lc_vTarget: '|| lc_vTarget);
    lc_nContinue := f_test_for_error(lc_vTableName);
    insert into debug (input) values ('just completed get target table name');
  else
    lc_vErrorDescription := 'NOT AUTHORIZED';
    raise spiders_error; 
  end if;  
  
/*I LEFT OFF JUST ABOUT TO GET THE PRIMARY KEY VALUE FROM THE JSON*/
  insert into debug (input) values ('before performing read qry collection');
  if lc_nContinue = 1 then
    lc_jJson := f_perform_read_qry_collection(lc_jParameterJson, lc_vModule, lc_vTarget, lc_vTableName);
    --insert into debug (input) values ('values : '||lc_jJson.to_char());
  else
    lc_vErrorDescription := 'Table name not found';
    raise spiders_error; 
  end if;
  insert into debug (input) values ('after performing read qry collection');
  
    
  return lc_jJson;

exception     
  when spiders_error then
      execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' SPIDERS error ' || lc_vErrorDescription || ''')';
      htp.p('{"error":"' || lc_vFunctionName || ' SPIDERS error ' || lc_vErrorDescription || '"}');
  when json_error then
      execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' JSON error'')';
      htp.p('{"error":"' || lc_vFunctionName || ' JSON error ' || lc_vErrorDescription || '"}');
  when others then
      execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' dml error: ' || sqlerrm || ''')';
     htp.p('{"error":"' || lc_vFunctionName || ' dml error ' || sqlerrm || '"}');
     return json('{"error":"' || lc_vFunctionName || ' dml error ' || sqlerrm || '"}');

end; 

end;

/
