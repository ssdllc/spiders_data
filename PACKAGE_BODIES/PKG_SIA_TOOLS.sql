--------------------------------------------------------
--  DDL for Package Body PKG_SIA_TOOLS
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE PACKAGE BODY "SPIDERS_DATA"."PKG_SIA_TOOLS" AS

/* Function is_number to test if value is numeric or text */
FUNCTION is_number (p_string IN VARCHAR2)
      RETURN INT
   AS
      v_new_num NUMBER;
   BEGIN
      v_new_num := TO_NUMBER(p_string);
      RETURN 1;
   EXCEPTION
   WHEN VALUE_ERROR THEN
      RETURN 0;
END is_number;

/* Function checkSIADisplayValues to test value against values in SPD_T_SIA_DISPLAYVALUES */
/* Returns true = 1, false = 0 if value is valid */
FUNCTION checkSIADisplayValues(in_value varchar2, in_siaquestionid number)
      RETURN number
   AS
      lc_nCount number;
      lc_vReturn varchar2(255) := 0;
      lc_vTrimmedValue varchar2(255);
   BEGIN
      SELECT count(code) into lc_nCount
      FROM spd_t_sia_displayvalues 
      WHERE siaquestionid = in_siaquestionid;

      if lc_nCount > 0 then
         FOR rec IN (SELECT code
                     FROM spd_t_sia_displayvalues 
                     WHERE siaquestionid = in_siaquestionid)
         LOOP
            if is_number(rec.code) = 0 then
               lc_vTrimmedValue := TRIM(in_value);
            else
               lc_vTrimmedValue := in_value;
            end if;

            if lc_vTrimmedValue = rec.code then
               lc_vReturn := 1;
            end if;
         END LOOP;
      else 
         lc_vReturn := 1;
      end if;
   return lc_vReturn;
END checkSIADisplayValues;

/* Function validateSIAValue to test if value is correct length */
/* Adjusts value if value is numeric to add leading zeroes if there is length mismatch */
FUNCTION validateSIAValue(in_value varchar2, in_siaquestionid number)
      RETURN varchar2
   AS
      lc_nRequiredLength number := 0;
      lc_vReturn varchar2(255) := in_value;
      lc_nLengthDifference number := 0;
      lc_vAdjustedValue varchar2(255) := in_value;
      lc_vPaddingCharacter varchar2(255);
      lc_nIsValidDisplayValue number := 0;
   BEGIN
      FOR rec in (SELECT sia_length
                  FROM spd_t_sia_questions
                  WHERE siaquestionid = in_siaquestionid)
      LOOP
         lc_nRequiredLength := rec.sia_length;
      END LOOP;

      if (lc_nRequiredLength > 0) and (lc_nRequiredLength is not null) then
         -- Set padding character to zero or space based on whether value is number
         if is_number(in_value) = 1 then
            lc_vPaddingCharacter := '0';
         else 
            lc_vPaddingCharacter := ' ';
         end if;
         -- Check if required length and value length differ
         if (length(in_value) != lc_nRequiredLength) then
            lc_nLengthDifference := lc_nRequiredLength - (length(in_value));
         end if;
         -- Adjust value by adding leading spaces or leading zeroes
         if lc_nLengthDifference > 0 then
            for i in 1..lc_nLengthDifference
               loop
                  lc_vAdjustedValue := lc_vPaddingCharacter || lc_vAdjustedValue;
               end loop;
            lc_vReturn := lc_vAdjustedValue;
         -- If required length is shorter than value length
         -- For now, we treat this as invalid
         elsif lc_nLengthDifference < 0 then
            lc_vReturn := 'Invalid';
         else 
            lc_vReturn := in_value;
         end if;
      end if;
      -- If we have a valid return value here, we want to test it against display values
      -- This should return true/false (0/1)
      if lc_vReturn != 'Invalid' then
         --insert into debug (message) values ('value before checkSIADisplayValues: ' || lc_vReturn);
         lc_nIsValidDisplayValue := checkSIADisplayValues(lc_vReturn, in_siaquestionid);
         if lc_nIsValidDisplayValue = 0 then
            lc_vReturn := 'Invalid';
         end if;
      end if;
   return lc_vReturn;
END validateSIAValue;

END pkg_sia_tools;

/
