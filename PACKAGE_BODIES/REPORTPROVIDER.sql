--------------------------------------------------------
--  DDL for Package Body REPORTPROVIDER
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE PACKAGE BODY "SPIDERS_DATA"."REPORTPROVIDER" AS
    
    
    
    function clob_to_blob (p_clob_in in clob) return blob is
      v_blob blob;
      v_offset integer;
      v_buffer_varchar varchar2(32000);
      v_buffer_raw raw(32000);
      v_buffer_size binary_integer := 32000;
    begin
      --
      if p_clob_in is null then
        return null;
      end if;
      -- 
      DBMS_LOB.CREATETEMPORARY(v_blob, TRUE);
      v_offset := 1;
      FOR i IN 1..CEIL(DBMS_LOB.GETLENGTH(p_clob_in) / v_buffer_size)
      loop
        dbms_lob.read(p_clob_in, v_buffer_size, v_offset, v_buffer_varchar);
        v_buffer_raw := utl_raw.cast_to_raw(v_buffer_varchar);
        dbms_lob.writeappend(v_blob, utl_raw.length(v_buffer_raw), v_buffer_raw);
        v_offset := v_offset + v_buffer_size;
      end loop;
      
      return v_blob;
    end clob_to_blob;
   
    
    
    
    
    
    PROCEDURE get_funding_request(IN_nSETPLANID varchar2 default '350000921') IS --IN_FY varchar2 default '2012',IN_FUNDSOURCE_LISTID number default 100032) IS --need to add a fundsourceID filter
      l_fy varchar2(4);  
      l_plantitle varchar2(2560);
      l_totalProgramOversight number;
      lc_nXMLFILEID number;
    
      cursor c1 is
        WITH qAllPlans as
          (
            select 
               spd_t_action_planner.productline_listid PRODUCTLINE_LISTID
              ,pl_list.returnvalue PRODUCTLINE
              ,pl_list.displayvalue PL_DISPLAY
              ,spd_t_activities.spd_activity_name ACTIVITY
              ,pl_list.returnvalue||'_'||spd_t_activities.spd_activity_name PL_ACT_KEY
              ,spd_t_action_planner.planned_ktr_amt CONTRACT
              ,spd_t_action_planner.planned_inhouse_amt INHOUSE
              ,spd_t_action_planner.reserve_flag RESERVE
              ,spd_t_action_planner.planned_qtr PLAN_QUARTER
              ,spd_t_action_planner.planned_year PLAN_YEAR
              ,fs_list.displayvalue FUND_SOURCE
            from
               spd_t_action_planner
              ,spd_t_list_set_plans
              ,spd_t_inter_set_plans
              ,spd_t_activities
              ,spd_t_lists fs_list
              ,spd_t_lists pl_list
            where spd_t_action_planner.apid = spd_t_inter_set_plans.apid
              and spd_t_inter_set_plans.setplanid = spd_t_list_set_plans.setplanid
              and spd_t_activities.activitiesid = spd_t_action_planner.planned_activitiesid
              and fs_list.listid = spd_t_action_planner.planned_fund_source_listid
              and pl_list.listid = spd_t_action_planner.productline_listid
              and spd_t_list_set_plans.SETPLANID = to_number(IN_nSETPLANID)
          )
          ,qTotalOMN as
          (
            select 
               PRODUCTLINE
              ,ACTIVITY
              ,PRODUCTLINE||'_'||ACTIVITY PL_ACT_KEY
              ,sum(CONTRACT) CONTRACT
              ,sum(INHOUSE) INHOUSE
            from qAllPlans
            where (RESERVE is null or RESERVE=0)
            group by PRODUCTLINE, ACTIVITY
          )
          ,qTotalOMR as
          (
            select 
               PRODUCTLINE
              ,ACTIVITY
              ,PRODUCTLINE||'_'||ACTIVITY PL_ACT_KEY
              ,sum(CONTRACT) CONTRACT
              ,sum(INHOUSE) INHOUSE
            from qAllPlans
            where (RESERVE is not null and RESERVE != 0)
            group by PRODUCTLINE, ACTIVITY
          )
          ,qQTR_1_OMN as
          (
            select 
               PRODUCTLINE
              ,ACTIVITY
              ,PRODUCTLINE||'_'||ACTIVITY PL_ACT_KEY
              ,sum(CONTRACT) CONTRACT
              ,sum(INHOUSE) INHOUSE
            from qAllPlans
            where (RESERVE is null or RESERVE=0)
              and PLAN_QUARTER = 1
            group by PRODUCTLINE, ACTIVITY
          )
          ,qQTR_1_OMR as
          (
            select 
               PRODUCTLINE
              ,ACTIVITY
              ,PRODUCTLINE||'_'||ACTIVITY PL_ACT_KEY
              ,sum(CONTRACT) CONTRACT
              ,sum(INHOUSE) INHOUSE
            from qAllPlans
            where (RESERVE is not null and RESERVE != 0)
              and PLAN_QUARTER = 1
            group by PRODUCTLINE, ACTIVITY
          )
          ,qQTR_2_OMN as
          (
            select 
               PRODUCTLINE
              ,ACTIVITY
              ,PRODUCTLINE||'_'||ACTIVITY PL_ACT_KEY
              ,sum(CONTRACT) CONTRACT
              ,sum(INHOUSE) INHOUSE
            from qAllPlans
            where (RESERVE is null or RESERVE=0)
              and PLAN_QUARTER = 2
            group by PRODUCTLINE, ACTIVITY
          )
          ,qQTR_2_OMR as
          (
            select 
               PRODUCTLINE
              ,ACTIVITY
              ,PRODUCTLINE||'_'||ACTIVITY PL_ACT_KEY
              ,sum(CONTRACT) CONTRACT
              ,sum(INHOUSE) INHOUSE
            from qAllPlans
            where (RESERVE is not null and RESERVE != 0)
              and PLAN_QUARTER = 2
            group by PRODUCTLINE, ACTIVITY
          )
          ,qQTR_3_OMN as
          (
            select 
               PRODUCTLINE
              ,ACTIVITY
              ,PRODUCTLINE||'_'||ACTIVITY PL_ACT_KEY
              ,sum(CONTRACT) CONTRACT
              ,sum(INHOUSE) INHOUSE
            from qAllPlans
            where (RESERVE is null or RESERVE=0)
              and PLAN_QUARTER = 3
            group by PRODUCTLINE, ACTIVITY
          )
          ,qQTR_3_OMR as
          (
            select 
               PRODUCTLINE
              ,ACTIVITY
              ,PRODUCTLINE||'_'||ACTIVITY PL_ACT_KEY
              ,sum(CONTRACT) CONTRACT
              ,sum(INHOUSE) INHOUSE
            from qAllPlans
            where (RESERVE is not null and RESERVE != 0)
              and PLAN_QUARTER = 3
            group by PRODUCTLINE, ACTIVITY
          )
          ,qQTR_4_OMN as
          (
            select 
               PRODUCTLINE
              ,ACTIVITY
              ,PRODUCTLINE||'_'||ACTIVITY PL_ACT_KEY
              ,sum(CONTRACT) CONTRACT
              ,sum(INHOUSE) INHOUSE
            from qAllPlans
            where (RESERVE is null or RESERVE=0)
              and PLAN_QUARTER = 4
            group by PRODUCTLINE, ACTIVITY
          )
          ,qQTR_4_OMR as
          (
            select 
               PRODUCTLINE
              ,ACTIVITY
              ,PRODUCTLINE||'_'||ACTIVITY PL_ACT_KEY
              ,sum(CONTRACT) CONTRACT
              ,sum(INHOUSE) INHOUSE
            from qAllPlans
            where (RESERVE is not null and RESERVE != 0)
              and PLAN_QUARTER = 4
            group by PRODUCTLINE, ACTIVITY
          )
        select 
           max(qAllPlans.PRODUCTLINE_LISTID) PRODUCTLINE_LISTID
          ,qAllPlans.PRODUCTLINE
          ,max(qAllPlans.PL_DISPLAY) PL_DISPLAY
          ,qAllPlans.ACTIVITY
          ,max(qAllPlans.FUND_SOURCE) FUND_SOURCE
          ,sum(qTotalOMN.CONTRACT) CONTRACT_NR_TOTAL
          ,sum(qTotalOMN.INHOUSE) INHOUSE_NR_TOTAL
          ,sum(qTotalOMR.CONTRACT) CONTRACT_R_TOTAL
          ,sum(qTotalOMR.INHOUSE) INHOUSE_R_TOTAL
          ,sum(qQTR_1_OMN.CONTRACT) CONTRACT_NR_Q1
          ,sum(qQTR_1_OMN.INHOUSE) INHOUSE_NR_Q1
          ,sum(qQTR_1_OMR.CONTRACT) CONTRACT_R_Q1
          ,sum(qQTR_1_OMR.INHOUSE) INHOUSE_R_Q1
          ,sum(qQTR_2_OMN.CONTRACT) CONTRACT_NR_Q2
          ,sum(qQTR_2_OMN.INHOUSE) INHOUSE_NR_Q2
          ,sum(qQTR_2_OMR.CONTRACT) CONTRACT_R_Q2
          ,sum(qQTR_2_OMR.INHOUSE) INHOUSE_R_Q2
          ,sum(qQTR_3_OMN.CONTRACT) CONTRACT_NR_Q3
          ,sum(qQTR_3_OMN.INHOUSE) INHOUSE_NR_Q3
          ,sum(qQTR_3_OMR.CONTRACT) CONTRACT_R_Q3
          ,sum(qQTR_3_OMR.INHOUSE) INHOUSE_R_Q3
          ,sum(qQTR_4_OMN.CONTRACT) CONTRACT_NR_Q4
          ,sum(qQTR_4_OMN.INHOUSE) INHOUSE_NR_Q4
          ,sum(qQTR_4_OMR.CONTRACT) CONTRACT_R_Q4
          ,sum(qQTR_4_OMR.INHOUSE) INHOUSE_R_Q4
        from 
           qAllPlans
          ,qTotalOMN
          ,qTotalOMR
          ,qQTR_1_OMN
          ,qQTR_1_OMR
          ,qQTR_2_OMN
          ,qQTR_2_OMR
          ,qQTR_3_OMN
          ,qQTR_3_OMR
          ,qQTR_4_OMN
          ,qQTR_4_OMR
        where qAllPlans.PL_ACT_KEY = qTotalOMN.PL_ACT_KEY(+)
          and qAllPlans.PL_ACT_KEY = qTotalOMR.PL_ACT_KEY(+)
          and qAllPlans.PL_ACT_KEY = qQTR_1_OMN.PL_ACT_KEY(+)
          and qAllPlans.PL_ACT_KEY = qQTR_1_OMR.PL_ACT_KEY(+)
          and qAllPlans.PL_ACT_KEY = qQTR_2_OMN.PL_ACT_KEY(+)
          and qAllPlans.PL_ACT_KEY = qQTR_2_OMR.PL_ACT_KEY(+)
          and qAllPlans.PL_ACT_KEY = qQTR_3_OMN.PL_ACT_KEY(+)
          and qAllPlans.PL_ACT_KEY = qQTR_3_OMR.PL_ACT_KEY(+)
          and qAllPlans.PL_ACT_KEY = qQTR_4_OMN.PL_ACT_KEY(+)
          and qAllPlans.PL_ACT_KEY = qQTR_4_OMR.PL_ACT_KEY(+)
        group by qAllPlans.PRODUCTLINE, qAllPlans.ACTIVITY
        order by  qAllPlans.PRODUCTLINE, qAllPlans.ACTIVITY;
    
    
      xlsDocument     ExcelDocumentType;
      documentArray    ExcelDocumentLine := ExcelDocumentLine();
    
      theBlob blob;
      
      col01 number:=0; col02 number:=0; col03 number:=0; col04 number:=0;
      col05 number:=0; col06 number:=0; col07 number:=0; col08 number:=0;
      col09 number:=0; col10 number:=0; col11 number:=0; col12 number:=0;
      col13 number:=0; col14 number:=0; col15 number:=0; col16 number:=0;
      col17 number:=0; col18 number:=0; col19 number:=0; col20 number:=0;
      
      --lastPL varchar2(512):='none';
      
      procedure resetColSums is
      begin
        col01:=0; col02:=0; col03:=0; col04:=0;
        col05:=0; col06:=0; col07:=0; col08:=0;
        col09:=0; col10:=0; col11:=0; col12:=0;
        col13:=0; col14:=0; col15:=0; col16:=0;
        col17:=0; col18:=0; col19:=0; col20:=0;
      end resetColSums;
      
      procedure insertBlankRow is
      begin
        xlsDocument.rowOpen;
        
        xlsDocument.addCell(p_style=>'ColumnHeader',p_data=>' ');
        xlsDocument.addCell(p_style=>'ColumnHeader',p_data=>' ');
        
        xlsDocument.addCell(p_style=>'ColumnHeader',p_data=>' ');
        xlsDocument.addCell(p_style=>'ColumnHeader',p_data=>' ');
        xlsDocument.addCell(p_style=>'ColumnHeader',p_data=>' ');
        xlsDocument.addCell(p_style=>'ColumnHeader',p_data=>' ');
        
        xlsDocument.addCell(p_style=>'ColumnHeader',p_data=>' ');
        xlsDocument.addCell(p_style=>'ColumnHeader',p_data=>' ');
        xlsDocument.addCell(p_style=>'ColumnHeader',p_data=>' ');
        xlsDocument.addCell(p_style=>'ColumnHeader',p_data=>' ');
        
        xlsDocument.addCell(p_style=>'ColumnHeader',p_data=>' ');
        xlsDocument.addCell(p_style=>'ColumnHeader',p_data=>' ');
        xlsDocument.addCell(p_style=>'ColumnHeader',p_data=>' ');
        xlsDocument.addCell(p_style=>'ColumnHeader',p_data=>' ');
        
        xlsDocument.addCell(p_style=>'ColumnHeader',p_data=>' ');
        xlsDocument.addCell(p_style=>'ColumnHeader',p_data=>' ');
        xlsDocument.addCell(p_style=>'ColumnHeader',p_data=>' ');
        xlsDocument.addCell(p_style=>'ColumnHeader',p_data=>' ');
        
        xlsDocument.addCell(p_style=>'ColumnHeader',p_data=>' ');
        xlsDocument.addCell(p_style=>'ColumnHeader',p_data=>' ');
        xlsDocument.addCell(p_style=>'ColumnHeader',p_data=>' ');
        xlsDocument.addCell(p_style=>'ColumnHeader',p_data=>' ');
        
        xlsDocument.rowClose;
      end;
      
      procedure insertPLsumRow(IN_CREATE varchar2 := 'MainTable',IN_PL varchar2 := 'WF',IN_PL_DISPLAY varchar2 := 'Waterfront') is
        l_style varchar2(256);
      begin
        xlsDocument.rowOpen;
        
        
        
        if (IN_CREATE = 'MainTable') then
          xlsDocument.addCell(p_style=>'ColumnHeader_B',p_data=>' ');
          xlsDocument.addCell(p_style=>'SumTableRowHeader_B',p_data=>upper(IN_PL_DISPLAY)||' PROGRAM SUBTOTAL:');
          
          xlsDocument.addCell(p_style=>'NumberSubtotal_LB',p_data_type=>'Number',p_data=>col01);
          xlsDocument.addCell(p_style=>'NumberSubtotal_B',p_data_type=>'Number',p_data=>col02);
          xlsDocument.addCell(p_style=>'NumberSubtotal_B',p_data_type=>'Number',p_data=>col03);
          xlsDocument.addCell(p_style=>'NumberSubtotal_RB',p_data_type=>'Number',p_data=>col04);
          
          xlsDocument.addCell(p_style=>'NumberSubtotal_LB',p_data_type=>'Number',p_data=>col05);
          xlsDocument.addCell(p_style=>'NumberSubtotal_B',p_data_type=>'Number',p_data=>col06);
          xlsDocument.addCell(p_style=>'NumberSubtotal_B',p_data_type=>'Number',p_data=>col07);
          xlsDocument.addCell(p_style=>'NumberSubtotal_RB',p_data_type=>'Number',p_data=>col08);
          
          xlsDocument.addCell(p_style=>'NumberSubtotal_LB',p_data_type=>'Number',p_data=>col09);
          xlsDocument.addCell(p_style=>'NumberSubtotal_B',p_data_type=>'Number',p_data=>col10);
          xlsDocument.addCell(p_style=>'NumberSubtotal_B',p_data_type=>'Number',p_data=>col11);
          xlsDocument.addCell(p_style=>'NumberSubtotal_RB',p_data_type=>'Number',p_data=>col12);
          
          xlsDocument.addCell(p_style=>'NumberSubtotal_LB',p_data_type=>'Number',p_data=>col13);
          xlsDocument.addCell(p_style=>'NumberSubtotal_B',p_data_type=>'Number',p_data=>col14);
          xlsDocument.addCell(p_style=>'NumberSubtotal_B',p_data_type=>'Number',p_data=>col15);
          xlsDocument.addCell(p_style=>'NumberSubtotal_RB',p_data_type=>'Number',p_data=>col16);
          
          xlsDocument.addCell(p_style=>'NumberSubtotal_LB',p_data_type=>'Number',p_data=>col17);
          xlsDocument.addCell(p_style=>'NumberSubtotal_B',p_data_type=>'Number',p_data=>col18);
          xlsDocument.addCell(p_style=>'NumberSubtotal_B',p_data_type=>'Number',p_data=>col19);
          xlsDocument.addCell(p_style=>'NumberSubtotal_RB',p_data_type=>'Number',p_data=>col20);
        
        else
          xlsDocument.addCell(p_style=>'ColumnHeader',p_data=>' ');
          xlsDocument.addCell(p_style=>'SumTableRowHeader',p_data=>upper(IN_PL_DISPLAY)||' PROGRAM:');
          
          xlsDocument.addCell(p_style=>'NumberSumTable_tlrb',p_data_type=>'Number',p_data=>col01);
          xlsDocument.addCell(p_style=>'NumberSumTable_tlrb',p_data_type=>'Number',p_data=>col02);
          xlsDocument.addCell(p_style=>'NumberSumTable_tlrb',p_data_type=>'Number',p_data=>col03);
          xlsDocument.addCell(p_style=>'NumberSumTable_tlRb',p_data_type=>'Number',p_data=>col04);
          
          xlsDocument.addCell(p_style=>'NumberSumTable_tlrb',p_data_type=>'Number',p_data=>col05);
          xlsDocument.addCell(p_style=>'NumberSumTable_tlrb',p_data_type=>'Number',p_data=>col06);
          xlsDocument.addCell(p_style=>'NumberSumTable_tlrb',p_data_type=>'Number',p_data=>col07);
          xlsDocument.addCell(p_style=>'NumberSumTable_tlRb',p_data_type=>'Number',p_data=>col08);
          
          xlsDocument.addCell(p_style=>'NumberSumTable_tlrb',p_data_type=>'Number',p_data=>col09);
          xlsDocument.addCell(p_style=>'NumberSumTable_tlrb',p_data_type=>'Number',p_data=>col10);
          xlsDocument.addCell(p_style=>'NumberSumTable_tlrb',p_data_type=>'Number',p_data=>col11);
          xlsDocument.addCell(p_style=>'NumberSumTable_tlRb',p_data_type=>'Number',p_data=>col12);
          
          xlsDocument.addCell(p_style=>'NumberSumTable_tlrb',p_data_type=>'Number',p_data=>col13);
          xlsDocument.addCell(p_style=>'NumberSumTable_tlrb',p_data_type=>'Number',p_data=>col14);
          xlsDocument.addCell(p_style=>'NumberSumTable_tlrb',p_data_type=>'Number',p_data=>col15);
          xlsDocument.addCell(p_style=>'NumberSumTable_tlRb',p_data_type=>'Number',p_data=>col16);
          
          xlsDocument.addCell(p_style=>'NumberSumTable_tlrb',p_data_type=>'Number',p_data=>col17);
          xlsDocument.addCell(p_style=>'NumberSumTable_tlrb',p_data_type=>'Number',p_data=>col18);
          xlsDocument.addCell(p_style=>'NumberSumTable_tlrb',p_data_type=>'Number',p_data=>col19);
          xlsDocument.addCell(p_style=>'NumberSumTable_tlrb',p_data_type=>'Number',p_data=>col20);
        end if;
        
        
        
        xlsDocument.rowClose;
      end;
      
      function getBoarderAttr(in_sides varchar2 := 'TBLR') return varchar2 is
        retVal varchar2(1024);
      begin
        retVal         := '<Borders>';
        if (inStr(in_sides,'L')>0) then
          retVal:=retVal || '<Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="3"/>';
        end if;
        if (inStr(in_sides,'R')>0) then
          retVal:=retVal ||   '<Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="3"/>';
        end if;
        if (inStr(in_sides,'T')>0) then
          retVal:=retVal ||   '<Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="3"/>';
        end if;
        if (inStr(in_sides,'B')>0) then
          retVal:=retVal ||   '<Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="3"/>';
        end if;
        
        if (inStr(in_sides,'l')>0) then
          retVal:=retVal || '<Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>';
        end if;
        if (inStr(in_sides,'r')>0) then
          retVal:=retVal ||   '<Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>';
        end if;
        if (inStr(in_sides,'t')>0) then
          retVal:=retVal ||   '<Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"/>';
        end if;
        if (inStr(in_sides,'b')>0) then
          retVal:=retVal ||   '<Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>';
        end if;
        
        retVal:=retVal || '</Borders>';
        return retVal;
      end getBoarderAttr;
      
      function getProgramOversight(IN_PLID number := 0,IN_QTR number := 0) return number is
        retVal number := 0;
      begin
        for rec in 
          (
            select productline_listid, 
              program_oversight_amt_q1,program_oversight_amt_q2,program_oversight_amt_q3,program_oversight_amt_q4 
            from spd_t_program_oversight 
            where setplanid=to_number(IN_nSETPLANID)
          ) loop
          
          
          if (IN_PLID=0 or IN_PLID=rec.productline_listid) then
            if (IN_QTR=0) then
              if rec.program_oversight_amt_q1 is not null then retVal:=retVal+rec.program_oversight_amt_q1; end if;
              if rec.program_oversight_amt_q2 is not null then retVal:=retVal+rec.program_oversight_amt_q2; end if;
              if rec.program_oversight_amt_q3 is not null then retVal:=retVal+rec.program_oversight_amt_q3; end if;
              if rec.program_oversight_amt_q3 is not null then retVal:=retVal+rec.program_oversight_amt_q4; end if;
              --retVal:=(retVal+rec.program_oversight_amt_q1+rec.program_oversight_amt_q2+rec.program_oversight_amt_q3+rec.program_oversight_amt_q4);
            elsif (IN_QTR=1 and rec.program_oversight_amt_q1 is not null) then
              retVal:=(retVal+rec.program_oversight_amt_q1);
            elsif (IN_QTR=2 and rec.program_oversight_amt_q2 is not null) then
              retVal:=(retVal+rec.program_oversight_amt_q2);
            elsif (IN_QTR=3 and rec.program_oversight_amt_q3 is not null) then
              retVal:=(retVal+rec.program_oversight_amt_q3);
            elsif (IN_QTR=4 and rec.program_oversight_amt_q4 is not null) then
              retVal:=(retVal+rec.program_oversight_amt_q4);
            end if;
          end if;
          
        end loop;
        --retVal:=retVal+1;
        return retVal;   
      end getProgramOversight;
      
      procedure insertProgramOversight(IN_PLID number, IN_PL varchar2) is
        l_count number;
        l_amt number;
      begin
        select count(programoversightid) into l_count from spd_t_program_oversight where setplanid=to_number(IN_nSETPLANID) and productline_listid = in_plid;
        if (l_count = 1) then
          --select program_oversight_amt into l_amt from spd_t_program_oversight where setplanid=to_number(IN_nSETPLANID) and productline_listid = in_plid;
          
          
          xlsDocument.rowOpen;
          xlsDocument.addCell(p_style=>'ColumnHeader',p_data=>' ');
          xlsDocument.addCell(p_style=>'SumTableRowHeader_R',p_data=>IN_PL||' Program Oversight:');
          

          l_amt:=getProgramOversight(in_plid=>in_plid);
          xlsDocument.addCell(p_style=>'ColumnHeader',p_data=>' ');
          xlsDocument.addCell(p_style=>'NumberSubtotal',p_data_type=>'Number',p_data=>l_amt);
          xlsDocument.addCell(p_style=>'ColumnHeader',p_data=>' ');
          xlsDocument.addCell(p_style=>'ColumnHeader_R',p_data=>' ');
          
          l_amt:=getProgramOversight(in_qtr=>1,in_plid=>in_plid);
          xlsDocument.addCell(p_style=>'ColumnHeader',p_data=>' ');
          xlsDocument.addCell(p_style=>'NumberSubtotal',p_data_type=>'Number',p_data=>l_amt);
          xlsDocument.addCell(p_style=>'ColumnHeader',p_data=>' ');
          xlsDocument.addCell(p_style=>'ColumnHeader_R',p_data=>' ');
          
          l_amt:=getProgramOversight(in_qtr=>2,in_plid=>in_plid);
          xlsDocument.addCell(p_style=>'ColumnHeader',p_data=>' ');
          xlsDocument.addCell(p_style=>'NumberSubtotal',p_data_type=>'Number',p_data=>l_amt);
          xlsDocument.addCell(p_style=>'ColumnHeader',p_data=>' ');
          xlsDocument.addCell(p_style=>'ColumnHeader_R',p_data=>' ');
          
          l_amt:=getProgramOversight(in_qtr=>3,in_plid=>in_plid);
          xlsDocument.addCell(p_style=>'ColumnHeader',p_data=>' ');
          xlsDocument.addCell(p_style=>'NumberSubtotal',p_data_type=>'Number',p_data=>l_amt);
          xlsDocument.addCell(p_style=>'ColumnHeader',p_data=>' ');
          xlsDocument.addCell(p_style=>'ColumnHeader_R',p_data=>' ');
          
          l_amt:=getProgramOversight(in_qtr=>4,in_plid=>in_plid);
          xlsDocument.addCell(p_style=>'ColumnHeader',p_data=>' ');
          xlsDocument.addCell(p_style=>'NumberSubtotal',p_data_type=>'Number',p_data=>l_amt);
          xlsDocument.addCell(p_style=>'ColumnHeader',p_data=>' ');
          xlsDocument.addCell(p_style=>'ColumnHeader_R',p_data=>' ');
          xlsDocument.rowClose;
          
        end if;
      end insertProgramOversight;
      
      procedure processCursor(IN_CREATE varchar2 := 'MainTable') is
        lastPL varchar2(512):='none';
        lastPLdisplay varchar2(1024):='none';
      begin
        lastPL:='none';
      
        for rec in c1 loop
          
          if (rec.PRODUCTLINE!=lastPL and IN_CREATE!='RollupTable') then
            if (lastPL!='none') then
              insertPLsumRow(IN_CREATE=>IN_CREATE, IN_PL=>lastPL, IN_PL_DISPLAY=>lastPLdisplay);
            end if;
            resetColSums;
            
            lastPL        :=rec.PRODUCTLINE;
            lastPLdisplay :=rec.PL_DISPLAY;
            
            if (IN_CREATE='MainTable') then
              insertProgramOversight(IN_PLID=>rec.PRODUCTLINE_LISTID,IN_PL=>rec.PL_DISPLAY);
            end if;
          end if;
          
          if (rec.CONTRACT_NR_TOTAL is not null) then col01:=(col01+rec.CONTRACT_NR_TOTAL); end if;
          if (rec.INHOUSE_NR_TOTAL is not null) then col02:=(col02+rec.INHOUSE_NR_TOTAL); end if;
          if (rec.CONTRACT_R_TOTAL is not null) then col03:=(col03+rec.CONTRACT_R_TOTAL); end if;
          if (rec.INHOUSE_R_TOTAL is not null) then col04:=(col04+rec.INHOUSE_R_TOTAL); end if;
          if (rec.CONTRACT_NR_Q1 is not null) then col05:=(col05+rec.CONTRACT_NR_Q1); end if;
          if (rec.INHOUSE_NR_Q1 is not null) then col06:=(col06+rec.INHOUSE_NR_Q1); end if;
          if (rec.CONTRACT_R_Q1 is not null) then col07:=(col07+rec.CONTRACT_R_Q1); end if;
          if (rec.INHOUSE_R_Q1 is not null) then col08:=(col08+rec.INHOUSE_R_Q1); end if;
          if (rec.CONTRACT_NR_Q2 is not null) then col09:=(col09+rec.CONTRACT_NR_Q2); end if;
          if (rec.INHOUSE_NR_Q2 is not null) then col10:=(col10+rec.INHOUSE_NR_Q2); end if;
          if (rec.CONTRACT_R_Q2 is not null) then col11:=(col11+rec.CONTRACT_R_Q2); end if;
          if (rec.INHOUSE_R_Q2 is not null) then col12:=(col12+rec.INHOUSE_R_Q2); end if;
          if (rec.CONTRACT_NR_Q3 is not null) then col13:=(col13+rec.CONTRACT_NR_Q3); end if;
          if (rec.INHOUSE_NR_Q3 is not null) then col14:=(col14+rec.INHOUSE_NR_Q3); end if;
          if (rec.CONTRACT_R_Q3 is not null) then col15:=(col15+rec.CONTRACT_R_Q3); end if;
          if (rec.INHOUSE_R_Q3 is not null) then col16:=(col16+rec.INHOUSE_R_Q3); end if;
          if (rec.CONTRACT_NR_Q4 is not null) then col17:=(col17+rec.CONTRACT_NR_Q4); end if;
          if (rec.INHOUSE_NR_Q4 is not null) then col18:=(col18+rec.INHOUSE_NR_Q4); end if;
          if (rec.CONTRACT_R_Q4 is not null) then col19:=(col19+rec.CONTRACT_R_Q4); end if;
          if (rec.INHOUSE_R_Q4 is not null) then col20:=(col20+rec.INHOUSE_R_Q4); end if;
          
          
          if (IN_CREATE='MainTable') then
            xlsDocument.rowOpen;
            xlsDocument.addCell(p_style=>'TextStyleBlueCell',p_data=>rec.PL_DISPLAY);
            xlsDocument.addCell(p_style=>'TextStyleBlueCell',p_data=>rec.ACTIVITY);
            xlsDocument.addCell(p_style=>'NumberStyleWhiteCell_L',p_data_type=>'Number',p_data=>rec.CONTRACT_NR_TOTAL);
            xlsDocument.addCell(p_style=>'NumberStyleWhiteCell',p_data_type=>'Number',p_data=>rec.INHOUSE_NR_TOTAL);
            xlsDocument.addCell(p_style=>'NumberStyleWhiteCell',p_data_type=>'Number',p_data=>rec.CONTRACT_R_TOTAL);
            xlsDocument.addCell(p_style=>'NumberStyleWhiteCell_R',p_data_type=>'Number',p_data=>rec.INHOUSE_R_TOTAL);
            xlsDocument.addCell(p_style=>'NumberStyleWhiteCell_L',p_data_type=>'Number',p_data=>rec.CONTRACT_NR_Q1);
            xlsDocument.addCell(p_style=>'NumberStyleWhiteCell',p_data_type=>'Number',p_data=>rec.INHOUSE_NR_Q1);
            xlsDocument.addCell(p_style=>'NumberStyleWhiteCell',p_data_type=>'Number',p_data=>rec.CONTRACT_R_Q1);
            xlsDocument.addCell(p_style=>'NumberStyleWhiteCell_R',p_data_type=>'Number',p_data=>rec.INHOUSE_R_Q1);
            xlsDocument.addCell(p_style=>'NumberStyleWhiteCell_L',p_data_type=>'Number',p_data=>rec.CONTRACT_NR_Q2);
            xlsDocument.addCell(p_style=>'NumberStyleWhiteCell',p_data_type=>'Number',p_data=>rec.INHOUSE_NR_Q2);
            xlsDocument.addCell(p_style=>'NumberStyleWhiteCell',p_data_type=>'Number',p_data=>rec.CONTRACT_R_Q2);
            xlsDocument.addCell(p_style=>'NumberStyleWhiteCell_R',p_data_type=>'Number',p_data=>rec.INHOUSE_R_Q2);
            xlsDocument.addCell(p_style=>'NumberStyleWhiteCell_L',p_data_type=>'Number',p_data=>rec.CONTRACT_NR_Q3);
            xlsDocument.addCell(p_style=>'NumberStyleWhiteCell',p_data_type=>'Number',p_data=>rec.INHOUSE_NR_Q3);
            xlsDocument.addCell(p_style=>'NumberStyleWhiteCell',p_data_type=>'Number',p_data=>rec.CONTRACT_R_Q3);
            xlsDocument.addCell(p_style=>'NumberStyleWhiteCell_R',p_data_type=>'Number',p_data=>rec.INHOUSE_R_Q3);
            xlsDocument.addCell(p_style=>'NumberStyleWhiteCell_L',p_data_type=>'Number',p_data=>rec.CONTRACT_NR_Q4);
            xlsDocument.addCell(p_style=>'NumberStyleWhiteCell',p_data_type=>'Number',p_data=>rec.INHOUSE_NR_Q4);
            xlsDocument.addCell(p_style=>'NumberStyleWhiteCell',p_data_type=>'Number',p_data=>rec.CONTRACT_R_Q4);
            xlsDocument.addCell(p_style=>'NumberStyleWhiteCell_R',p_data_type=>'Number',p_data=>rec.INHOUSE_R_Q4);
            xlsDocument.rowClose;
          end if;
        end loop;
        
        if (IN_CREATE!='RollupTable') then
          insertPLsumRow(IN_CREATE=>IN_CREATE, IN_PL=>lastPL, IN_PL_DISPLAY=>lastPLdisplay);
        else
        
          xlsDocument.rowOpen;
          xlsDocument.addCell(p_style=>'ColumnHeader',p_data=>' ');
          xlsDocument.addCell(p_style=>'ColumnHeader',p_data=>' ');
          
          xlsDocument.addCell(p_style=>'ColumnHeader',p_data=>'Active',p_custom_attr=>'ss:MergeAcross="1"');
          xlsDocument.addCell(p_style=>'ColumnHeader',p_data=>'Reserve',p_custom_attr=>'ss:MergeAcross="1"');
          
          xlsDocument.addCell(p_style=>'ColumnHeader',p_data=>' ');
          xlsDocument.addCell(p_style=>'ColumnHeader',p_data=>' ');
          xlsDocument.addCell(p_style=>'ColumnHeader',p_data=>' ');
          xlsDocument.addCell(p_style=>'ColumnHeader',p_data=>' ');
          
          xlsDocument.addCell(p_style=>'ColumnHeader',p_data=>' ');
          xlsDocument.addCell(p_style=>'ColumnHeader',p_data=>' ');
          xlsDocument.addCell(p_style=>'ColumnHeader',p_data=>' ');
          xlsDocument.addCell(p_style=>'ColumnHeader',p_data=>' ');
          
          xlsDocument.addCell(p_style=>'ColumnHeader',p_data=>' ');
          xlsDocument.addCell(p_style=>'ColumnHeader',p_data=>' ');
          xlsDocument.addCell(p_style=>'ColumnHeader',p_data=>' ');
          xlsDocument.addCell(p_style=>'ColumnHeader',p_data=>' ');
          
          xlsDocument.addCell(p_style=>'ColumnHeader',p_data=>' ');
          xlsDocument.addCell(p_style=>'ColumnHeader',p_data=>' ');
          xlsDocument.addCell(p_style=>'ColumnHeader',p_data=>' ');
          xlsDocument.addCell(p_style=>'ColumnHeader',p_data=>' ');
          xlsDocument.rowClose;
          
          xlsDocument.rowOpen;
          xlsDocument.addCell(p_style=>'ColumnHeader',p_data=>' ');
          xlsDocument.addCell(p_style=>'SumTableRowHeader',p_data=>'Total Direct Cite + Project Mgmt:');
          xlsDocument.addCell(p_style=>'NumberSumTable_tlrb',p_data_type=>'Number',p_data=>(col01+col02),p_custom_attr=>'ss:MergeAcross="1"');
          xlsDocument.addCell(p_style=>'NumberSumTable_tlrb',p_data_type=>'Number',p_data=>(col03+col04),p_custom_attr=>'ss:MergeAcross="1"');
          xlsDocument.addCell(p_style=>'ColumnHeader',p_data=>' ');
          xlsDocument.addCell(p_style=>'ColumnHeader',p_data=>' ');
          xlsDocument.addCell(p_style=>'ColumnHeader',p_data=>' ');
          xlsDocument.addCell(p_style=>'ColumnHeader',p_data=>' ');
          xlsDocument.addCell(p_style=>'ColumnHeader',p_data=>' ');
          xlsDocument.addCell(p_style=>'ColumnHeader',p_data=>' ');
          xlsDocument.addCell(p_style=>'ColumnHeader',p_data=>' ');
          xlsDocument.addCell(p_style=>'ColumnHeader',p_data=>' ');
          xlsDocument.addCell(p_style=>'ColumnHeader',p_data=>' ');
          xlsDocument.addCell(p_style=>'ColumnHeader',p_data=>' ');
          xlsDocument.addCell(p_style=>'ColumnHeader',p_data=>' ');
          xlsDocument.addCell(p_style=>'ColumnHeader',p_data=>' ');
          xlsDocument.addCell(p_style=>'ColumnHeader',p_data=>' ');
          xlsDocument.addCell(p_style=>'ColumnHeader',p_data=>' ');
          xlsDocument.addCell(p_style=>'ColumnHeader',p_data=>' ');
          xlsDocument.addCell(p_style=>'ColumnHeader',p_data=>' ');
          xlsDocument.rowClose;
          
          l_totalProgramOversight:=getProgramOversight(in_plid=>0,in_qtr=>0);
          --l_totalProgramOversight:=l_totalProgramOversight+1;
          
          xlsDocument.rowOpen;
          xlsDocument.addCell(p_style=>'ColumnHeader',p_data=>' ');
          xlsDocument.addCell(p_style=>'SumTableRowHeader',p_data=>'Specialized Program Oversight:');
          xlsDocument.addCell(p_style=>'NumberSumTable_tlrb',p_data_type=>'Number',p_data=>l_totalProgramOversight,p_custom_attr=>'ss:MergeAcross="1"');
          xlsDocument.addCell(p_style=>'NumberSumTable_tlrb',p_data=>' ',p_custom_attr=>'ss:MergeAcross="1"');
          xlsDocument.addCell(p_style=>'ColumnHeader',p_data=>' ');
          xlsDocument.addCell(p_style=>'ColumnHeader',p_data=>' ');
          xlsDocument.addCell(p_style=>'ColumnHeader',p_data=>' ');
          xlsDocument.addCell(p_style=>'ColumnHeader',p_data=>' ');
          xlsDocument.addCell(p_style=>'ColumnHeader',p_data=>' ');
          xlsDocument.addCell(p_style=>'ColumnHeader',p_data=>' ');
          xlsDocument.addCell(p_style=>'ColumnHeader',p_data=>' ');
          xlsDocument.addCell(p_style=>'ColumnHeader',p_data=>' ');
          xlsDocument.addCell(p_style=>'ColumnHeader',p_data=>' ');
          xlsDocument.addCell(p_style=>'ColumnHeader',p_data=>' ');
          xlsDocument.addCell(p_style=>'ColumnHeader',p_data=>' ');
          xlsDocument.addCell(p_style=>'ColumnHeader',p_data=>' ');
          xlsDocument.addCell(p_style=>'ColumnHeader',p_data=>' ');
          xlsDocument.addCell(p_style=>'ColumnHeader',p_data=>' ');
          xlsDocument.addCell(p_style=>'ColumnHeader',p_data=>' ');
          xlsDocument.addCell(p_style=>'ColumnHeader',p_data=>' ');
          xlsDocument.rowClose;
          
          xlsDocument.rowOpen;
          xlsDocument.addCell(p_style=>'ColumnHeader',p_data=>' ');
          xlsDocument.addCell(p_style=>'SumTableRowHeader',p_data=>'Total:');
          xlsDocument.addCell(p_style=>'NumberSumTable_tlrb',p_data_type=>'Number',p_data=>(col01+col02+l_totalProgramOversight),p_custom_attr=>'ss:MergeAcross="1"');
          xlsDocument.addCell(p_style=>'NumberSumTable_tlrb',p_data_type=>'Number',p_data=>(col03+col04),p_custom_attr=>'ss:MergeAcross="1"');
          xlsDocument.addCell(p_style=>'ColumnHeader',p_data=>' ');
          xlsDocument.addCell(p_style=>'ColumnHeader',p_data=>' ');
          xlsDocument.addCell(p_style=>'ColumnHeader',p_data=>' ');
          xlsDocument.addCell(p_style=>'ColumnHeader',p_data=>' ');
          xlsDocument.addCell(p_style=>'ColumnHeader',p_data=>' ');
          xlsDocument.addCell(p_style=>'ColumnHeader',p_data=>' ');
          xlsDocument.addCell(p_style=>'ColumnHeader',p_data=>' ');
          xlsDocument.addCell(p_style=>'ColumnHeader',p_data=>' ');
          xlsDocument.addCell(p_style=>'ColumnHeader',p_data=>' ');
          xlsDocument.addCell(p_style=>'ColumnHeader',p_data=>' ');
          xlsDocument.addCell(p_style=>'ColumnHeader',p_data=>' ');
          xlsDocument.addCell(p_style=>'ColumnHeader',p_data=>' ');
          xlsDocument.addCell(p_style=>'ColumnHeader',p_data=>' ');
          xlsDocument.addCell(p_style=>'ColumnHeader',p_data=>' ');
          xlsDocument.addCell(p_style=>'ColumnHeader',p_data=>' ');
          xlsDocument.addCell(p_style=>'ColumnHeader',p_data=>' ');
          xlsDocument.rowClose;
            
            
        end if;
      end processCursor;
    
    
    
  BEGIN
    
    select basefy,PLANTITLE into l_fy, l_plantitle from spd_t_list_set_plans where setplanid = IN_nSETPLANID;
    l_fy:='FY'||substr(l_fy,3);
    
    xlsDocument := ExcelDocumentType();
    
    -- Open the document
    xlsDocument.documentOpen;
    
    -- Define Styles
    
    xlsDocument.stylesOpen;
    
    -- Include Default Style
    xlsDocument.defaultStyle;
    
    -- Add Custom Styles
    
    /* Style for Column Header Row */
    
    xlsDocument.createStyle(
       p_style_id =>'SumTableRowHeader'
      ,p_font     =>'Times New Roman'
      ,p_ffamily  =>'Roman'
      ,p_fsize    =>'11'
      ,p_bold     =>'Y'
      ,p_align_horizontal=>'Right'
      ,p_align_vertical=>'Bottom'
    );
    xlsDocument.createStyle(
       p_style_id =>'SumTableRowHeader_R'
      ,p_font     =>'Times New Roman'
      ,p_ffamily  =>'Roman'
      ,p_fsize    =>'11'
      ,p_bold     =>'Y'
      ,p_align_horizontal=>'Right'
      ,p_align_vertical=>'Bottom'
      ,p_custom_xml =>getBoarderAttr('R')
    );
    xlsDocument.createStyle(
       p_style_id =>'SumTableRowHeader_B'
      ,p_font     =>'Times New Roman'
      ,p_ffamily  =>'Roman'
      ,p_fsize    =>'11'
      ,p_bold     =>'Y'
      ,p_align_horizontal=>'Right'
      ,p_align_vertical=>'Bottom'
      ,p_custom_xml =>getBoarderAttr('B')
    );
    
    
    xlsDocument.createStyle(p_style_id =>'ColumnHeader',
    p_font     =>'Times New Roman',
    p_ffamily  =>'Roman',
    p_fsize    =>'11',
    p_bold     =>'Y',
    --p_underline =>'Single',
    p_align_horizontal=>'Center',
    p_align_vertical=>'Bottom');
    
    xlsDocument.createStyle
    (
       p_style_id =>'ColumnHeader_TLR'
      ,p_font     =>'Times New Roman'
      ,p_ffamily  =>'Roman'
      ,p_fsize    =>'11'
      ,p_bold     =>'Y'
      --,p_underline =>'Single'
      ,p_align_horizontal=>'Center'
      ,p_align_vertical=>'Bottom'
      ,p_custom_xml =>getBoarderAttr('TLR')
    );
    xlsDocument.createStyle
    (
       p_style_id =>'ColumnHeader_T'
      ,p_font     =>'Times New Roman'
      ,p_ffamily  =>'Roman'
      ,p_fsize    =>'11'
      ,p_bold     =>'Y'
      --,p_underline =>'Single'
      ,p_align_horizontal=>'Center'
      ,p_align_vertical=>'Bottom'
      ,p_custom_xml =>getBoarderAttr('T')
    );
    xlsDocument.createStyle
    (
       p_style_id =>'ColumnHeader_L'
      ,p_font     =>'Times New Roman'
      ,p_ffamily  =>'Roman'
      ,p_fsize    =>'11'
      ,p_bold     =>'Y'
      --,p_underline =>'Single'
      ,p_align_horizontal=>'Center'
      ,p_align_vertical=>'Bottom'
      ,p_custom_xml =>getBoarderAttr('L')
    );

    xlsDocument.createStyle
    (
       p_style_id =>'ColumnHeader_R'
      ,p_font     =>'Times New Roman'
      ,p_ffamily  =>'Roman'
      ,p_fsize    =>'11'
      ,p_bold     =>'Y'
      --,p_underline =>'Single'
      ,p_align_horizontal=>'Center'
      ,p_align_vertical=>'Bottom'
      ,p_custom_xml =>getBoarderAttr('R')
    );
    xlsDocument.createStyle
    (
       p_style_id =>'ColumnHeader_LB'
      ,p_font     =>'Times New Roman'
      ,p_ffamily  =>'Roman'
      ,p_fsize    =>'11'
      ,p_bold     =>'Y'
      --,p_underline =>'Single'
      ,p_align_horizontal=>'Center'
      ,p_align_vertical=>'Bottom'
      ,p_custom_xml =>getBoarderAttr('LB')
    );
    xlsDocument.createStyle
    (
       p_style_id =>'ColumnHeader_RB'
      ,p_font     =>'Times New Roman'
      ,p_ffamily  =>'Roman'
      ,p_fsize    =>'11'
      ,p_bold     =>'Y'
      --,p_underline =>'Single'
      ,p_align_horizontal=>'Center'
      ,p_align_vertical=>'Bottom'
      ,p_custom_xml =>getBoarderAttr('RB')
    );
    xlsDocument.createStyle
    (
       p_style_id =>'ColumnHeader_B'
      ,p_font     =>'Times New Roman'
      ,p_ffamily  =>'Roman'
      ,p_fsize    =>'11'
      ,p_bold     =>'Y'
      --,p_underline =>'Single'
      ,p_align_horizontal=>'Center'
      ,p_align_vertical=>'Bottom'
      ,p_custom_xml =>getBoarderAttr('B')
    );
    
    
    /* Styles for alternating row colors.
    p_cell_pattern =>'Solid',  */
    xlsDocument.createStyle
    (
       p_style_id=>'NumberStyleWhiteCell'
      ,p_cell_color=>'White'
      ,p_number_format => '###,###,###'
      ,p_align_horizontal => 'Right'
      ,p_fsize    =>'10'
    );
    
    xlsDocument.createStyle
    (
       p_style_id=>'NumberStyleWhiteCell_L'
      ,p_cell_color=>'White'
      ,p_number_format => '###,###,###'
      ,p_align_horizontal => 'Right'
      ,p_fsize    =>'10'
      ,p_custom_xml =>getBoarderAttr('L')
    );
    
    xlsDocument.createStyle
    (
       p_style_id=>'NumberStyleWhiteCell_R'
      ,p_cell_color=>'White'
      ,p_number_format => '###,###,###'
      ,p_align_horizontal => 'Right'
      ,p_fsize    =>'10'
      ,p_custom_xml =>getBoarderAttr('R')
    );
    
    xlsDocument.createStyle
    (
       p_style_id=>'NumberSubtotal'
      ,p_cell_color=>'White'
      ,p_bold     =>'Y'
      ,p_number_format => '###,###,###'
      ,p_align_horizontal => 'Right'
      ,p_fsize    =>'11'
    );
    xlsDocument.createStyle
    (
       p_style_id=>'NumberSubtotal_B'
      ,p_cell_color=>'White'
      ,p_bold     =>'Y'
      ,p_number_format => '###,###,###'
      ,p_align_horizontal => 'Right'
      ,p_fsize    =>'11'
      ,p_custom_xml =>getBoarderAttr('B')
    );
    xlsDocument.createStyle
    (
       p_style_id=>'NumberSubtotal_LB'
      ,p_cell_color=>'White'
      ,p_bold     =>'Y'
      ,p_number_format => '###,###,###'
      ,p_align_horizontal => 'Right'
      ,p_fsize    =>'11'
      ,p_custom_xml =>getBoarderAttr('LB')
    );
    xlsDocument.createStyle
    (
       p_style_id=>'NumberSubtotal_RB'
      ,p_cell_color=>'White'
      ,p_bold     =>'Y'
      ,p_number_format => '###,###,###'
      ,p_align_horizontal => 'Right'
      ,p_fsize    =>'11'
      ,p_custom_xml =>getBoarderAttr('RB')
    );
    xlsDocument.createStyle
    (
       p_style_id=>'NumberSumTable'
      ,p_cell_color=>'White'
      ,p_bold     =>'Y'
      ,p_number_format => '$###,###,##0k'
      ,p_align_horizontal => 'Center'
      ,p_fsize    =>'11'
    );
    xlsDocument.createStyle
    (
       p_style_id=>'NumberSumTable_tlrb'
      ,p_cell_color=>'White'
      ,p_bold     =>'Y'
      ,p_number_format => '$###,###,##0k'
      ,p_align_horizontal => 'Center'
      ,p_fsize    =>'11'
      ,p_custom_xml =>getBoarderAttr('tlrb')
    );
    xlsDocument.createStyle
    (
       p_style_id=>'NumberSumTable_tlRb'
      ,p_cell_color=>'White'
      ,p_bold     =>'Y'
      ,p_number_format => '$###,###,##0k'
      ,p_align_horizontal => 'Center'
      ,p_fsize    =>'11'
      ,p_custom_xml =>getBoarderAttr('tlRb')
    );
    
    xlsDocument.createStyle( p_style_id=>'TextStyleBlueCell',
      p_cell_color=>'Cyan');
    
    
    -- Close Styles
    xlsDocument.stylesClose;
    
    -- Open Worksheet
    xlsDocument.worksheetOpen(l_fy||' Exec Plan');
    

    --CONDITIONAL FORMATTING HERE - IF WE WANT TO ADD IT LATER
    
    --Define Columns
    xlsDocument.defineColumn(p_index=>'1',p_width=>10);  --PROGRAM
    xlsDocument.defineColumn(p_index=>'2',p_width=>40);  --ACTIVITY
    
    xlsDocument.defineColumn(p_index=>'3',p_width=>10);  --CONTRACT_NR_TOTAL
    xlsDocument.defineColumn(p_index=>'4',p_width=>10);  --INHOUSE_NR_TOTAL
    xlsDocument.defineColumn(p_index=>'5',p_width=>10);  --CONTRACT_R_TOTAL
    xlsDocument.defineColumn(p_index=>'6',p_width=>10);  --INHOUSE_R_TOTAL
    
    xlsDocument.defineColumn(p_index=>'7',p_width=>10);  --CONTRACT_NR_Q1
    xlsDocument.defineColumn(p_index=>'8',p_width=>10);  --INHOUSE_NR_Q1
    xlsDocument.defineColumn(p_index=>'9',p_width=>10);  --CONTRACT_R_Q1
    xlsDocument.defineColumn(p_index=>'10',p_width=>10);  --INHOUSE_R_Q1
    
    xlsDocument.defineColumn(p_index=>'11',p_width=>10);  --CONTRACT_NR_Q2
    xlsDocument.defineColumn(p_index=>'12',p_width=>10);  --INHOUSE_NR_Q2
    xlsDocument.defineColumn(p_index=>'13',p_width=>10);  --CONTRACT_R_Q2
    xlsDocument.defineColumn(p_index=>'14',p_width=>10);  --INHOUSE_R_Q2
    
    xlsDocument.defineColumn(p_index=>'15',p_width=>10);  --CONTRACT_NR_Q3
    xlsDocument.defineColumn(p_index=>'16',p_width=>10);  --INHOUSE_NR_Q3
    xlsDocument.defineColumn(p_index=>'17',p_width=>10);  --CONTRACT_R_Q3
    xlsDocument.defineColumn(p_index=>'18',p_width=>10);  --INHOUSE_R_Q3
    
    xlsDocument.defineColumn(p_index=>'19',p_width=>10);  --CONTRACT_NR_Q4
    xlsDocument.defineColumn(p_index=>'20',p_width=>10);  --INHOUSE_NR_Q4
    xlsDocument.defineColumn(p_index=>'21',p_width=>10);  --CONTRACT_R_Q4
    xlsDocument.defineColumn(p_index=>'22',p_width=>10);  --INHOUSE_R_Q4
    
    
    insertBlankRow;
    
    --BEGIN ROLLUP TABLE
    processCursor(IN_CREATE=>'RollupTable');
    
    insertBlankRow;
    insertBlankRow;
    
    --BEGIN PRODUCTLINE SUMMARY
    xlsDocument.rowOpen;
    xlsDocument.addCell(p_style=>'ColumnHeader',p_data=>' ');
    xlsDocument.addCell(p_style=>'SumTableRowHeader',p_data=>upper('PROGRAM OVERSIGHT:'));
    
    xlsDocument.addCell(p_style=>'NumberSumTable_tlrb',p_data=>' ');
    xlsDocument.addCell(p_style=>'NumberSumTable_tlrb',p_data_type=>'Number',p_data=>getProgramOversight(in_qtr=>0));
    xlsDocument.addCell(p_style=>'NumberSumTable_tlrb',p_data=>' ');
    xlsDocument.addCell(p_style=>'NumberSumTable_tlRb',p_data=>' ');
    
    xlsDocument.addCell(p_style=>'NumberSumTable_tlrb',p_data=>' ');
    xlsDocument.addCell(p_style=>'NumberSumTable_tlrb',p_data_type=>'Number',p_data=>getProgramOversight(in_qtr=>1));
    xlsDocument.addCell(p_style=>'NumberSumTable_tlrb',p_data=>' ');
    xlsDocument.addCell(p_style=>'NumberSumTable_tlRb',p_data=>' ');
    
    xlsDocument.addCell(p_style=>'NumberSumTable_tlrb',p_data=>' ');
    xlsDocument.addCell(p_style=>'NumberSumTable_tlrb',p_data_type=>'Number',p_data=>getProgramOversight(in_qtr=>2));
    xlsDocument.addCell(p_style=>'NumberSumTable_tlrb',p_data=>' ');
    xlsDocument.addCell(p_style=>'NumberSumTable_tlRb',p_data=>' ');
    
    xlsDocument.addCell(p_style=>'NumberSumTable_tlrb',p_data=>' ');
    xlsDocument.addCell(p_style=>'NumberSumTable_tlrb',p_data_type=>'Number',p_data=>getProgramOversight(in_qtr=>3));
    xlsDocument.addCell(p_style=>'NumberSumTable_tlrb',p_data=>' ');
    xlsDocument.addCell(p_style=>'NumberSumTable_tlRb',p_data=>' ');
    
    xlsDocument.addCell(p_style=>'NumberSumTable_tlrb',p_data=>' ');
    xlsDocument.addCell(p_style=>'NumberSumTable_tlrb',p_data_type=>'Number',p_data=>getProgramOversight(in_qtr=>4));
    xlsDocument.addCell(p_style=>'NumberSumTable_tlrb',p_data=>' ');
    xlsDocument.addCell(p_style=>'NumberSumTable_tlrb',p_data=>' ');
    xlsDocument.rowClose;  
    
    processCursor(IN_CREATE=>'SummaryTable');
    
    insertBlankRow;
    insertBlankRow;
    
    
    
    --BEGIN MAIN REPORT------------------------------
    xlsDocument.rowOpen;
      
    xlsDocument.addCell(p_style=>'ColumnHeader',p_data=>' ');
    xlsDocument.addCell(p_style=>'ColumnHeader',p_data=>' ');
    
    xlsDocument.addCell(p_style=>'ColumnHeader_TLR',p_data=>'Total '||l_fy,p_custom_attr=>'ss:MergeAcross="3"');

    xlsDocument.addCell(p_style=>'ColumnHeader_TLR',p_data=>'First Quarter',p_custom_attr=>'ss:MergeAcross="3"');
    xlsDocument.addCell(p_style=>'ColumnHeader_TLR',p_data=>'Second Quarter',p_custom_attr=>'ss:MergeAcross="3"');
    xlsDocument.addCell(p_style=>'ColumnHeader_TLR',p_data=>'Third Quarter',p_custom_attr=>'ss:MergeAcross="3"');
    xlsDocument.addCell(p_style=>'ColumnHeader_TLR',p_data=>'Fourth Quarter',p_custom_attr=>'ss:MergeAcross="3"');
    
    xlsDocument.rowClose;




    xlsDocument.rowOpen;
      
    xlsDocument.addCell(p_style=>'ColumnHeader_T',p_data=>'Program');
    xlsDocument.addCell(p_style=>'ColumnHeader_T',p_data=>'Activity');
    
    xlsDocument.addCell(p_style=>'ColumnHeader_L',p_data=>'O&MN',p_custom_attr=>'ss:MergeAcross="1"');
    --xlsDocument.addCell(p_style=>'ColumnHeader_B',p_data=>'O&MN');
    xlsDocument.addCell(p_style=>'ColumnHeader_R',p_data=>'O&MNR',p_custom_attr=>'ss:MergeAcross="1"');
    --xlsDocument.addCell(p_style=>'ColumnHeader_RB',p_data=>'O&MR');
    
    xlsDocument.addCell(p_style=>'ColumnHeader_L',p_data=>'O&MN',p_custom_attr=>'ss:MergeAcross="1"');
    --xlsDocument.addCell(p_style=>'ColumnHeader_B',p_data=>'O&MN');
    xlsDocument.addCell(p_style=>'ColumnHeader_R',p_data=>'O&MNR',p_custom_attr=>'ss:MergeAcross="1"');
    --xlsDocument.addCell(p_style=>'ColumnHeader_RB',p_data=>'O&MR');
    
    xlsDocument.addCell(p_style=>'ColumnHeader_L',p_data=>'O&MN',p_custom_attr=>'ss:MergeAcross="1"');
    --xlsDocument.addCell(p_style=>'ColumnHeader_B',p_data=>'O&MN');
    xlsDocument.addCell(p_style=>'ColumnHeader_R',p_data=>'O&MNR',p_custom_attr=>'ss:MergeAcross="1"');
    --xlsDocument.addCell(p_style=>'ColumnHeader_RB',p_data=>'O&MR');
    
    xlsDocument.addCell(p_style=>'ColumnHeader_L',p_data=>'O&MN',p_custom_attr=>'ss:MergeAcross="1"');
    --xlsDocument.addCell(p_style=>'ColumnHeader_B',p_data=>'O&MN');
    xlsDocument.addCell(p_style=>'ColumnHeader_R',p_data=>'O&MNR',p_custom_attr=>'ss:MergeAcross="1"');
    --xlsDocument.addCell(p_style=>'ColumnHeader_RB',p_data=>'O&MR');
    
    xlsDocument.addCell(p_style=>'ColumnHeader_L',p_data=>'O&MN',p_custom_attr=>'ss:MergeAcross="1"');
    --xlsDocument.addCell(p_style=>'ColumnHeader_B',p_data=>'O&MN');
    xlsDocument.addCell(p_style=>'ColumnHeader_R',p_data=>'O&MNR',p_custom_attr=>'ss:MergeAcross="1"');
    --xlsDocument.addCell(p_style=>'ColumnHeader_RB',p_data=>'O&MR');
    
    xlsDocument.rowClose;
    
    
    
    xlsDocument.rowOpen;
      
    xlsDocument.addCell(p_style=>'ColumnHeader_B',p_data=>' ');
    xlsDocument.addCell(p_style=>'ColumnHeader_B',p_data=>' ');
    
    xlsDocument.addCell(p_style=>'ColumnHeader_LB',p_data=>'Contract');
    xlsDocument.addCell(p_style=>'ColumnHeader_B',p_data=>'In House');
    xlsDocument.addCell(p_style=>'ColumnHeader_B',p_data=>'Contract');
    xlsDocument.addCell(p_style=>'ColumnHeader_RB',p_data=>'In House');
    
    xlsDocument.addCell(p_style=>'ColumnHeader_LB',p_data=>'Contract');
    xlsDocument.addCell(p_style=>'ColumnHeader_B',p_data=>'In House');
    xlsDocument.addCell(p_style=>'ColumnHeader_B',p_data=>'Contract');
    xlsDocument.addCell(p_style=>'ColumnHeader_RB',p_data=>'In House');
    
    xlsDocument.addCell(p_style=>'ColumnHeader_LB',p_data=>'Contract');
    xlsDocument.addCell(p_style=>'ColumnHeader_B',p_data=>'In House');
    xlsDocument.addCell(p_style=>'ColumnHeader_B',p_data=>'Contract');
    xlsDocument.addCell(p_style=>'ColumnHeader_RB',p_data=>'In House');
    
    xlsDocument.addCell(p_style=>'ColumnHeader_LB',p_data=>'Contract');
    xlsDocument.addCell(p_style=>'ColumnHeader_B',p_data=>'In House');
    xlsDocument.addCell(p_style=>'ColumnHeader_B',p_data=>'Contract');
    xlsDocument.addCell(p_style=>'ColumnHeader_RB',p_data=>'In House');
    
    xlsDocument.addCell(p_style=>'ColumnHeader_LB',p_data=>'Contract');
    xlsDocument.addCell(p_style=>'ColumnHeader_B',p_data=>'In House');
    xlsDocument.addCell(p_style=>'ColumnHeader_B',p_data=>'Contract');
    xlsDocument.addCell(p_style=>'ColumnHeader_RB',p_data=>'In House');
    
    xlsDocument.rowClose;
    
    
    

      
    processCursor;
    /*
    lastPL:='none';
    
    for rec in c1 loop
      
      if (rec.PRODUCTLINE!=lastPL) then
        if (lastPL!='none') then
          insertPLsumRow;
          resetColSums;
        end if;
        lastPL:=rec.PRODUCTLINE;
      end if;
      
      if (rec.CONTRACT_NR_TOTAL is not null) then col01:=(col01+rec.CONTRACT_NR_TOTAL); end if;
      if (rec.INHOUSE_NR_TOTAL is not null) then col02:=(col02+rec.INHOUSE_NR_TOTAL); end if;
      if (rec.CONTRACT_R_TOTAL is not null) then col03:=(col03+rec.CONTRACT_R_TOTAL); end if;
      if (rec.INHOUSE_R_TOTAL is not null) then col04:=(col04+rec.INHOUSE_R_TOTAL); end if;
      if (rec.CONTRACT_NR_Q1 is not null) then col05:=(col05+rec.CONTRACT_NR_Q1); end if;
      if (rec.INHOUSE_NR_Q1 is not null) then col06:=(col06+rec.INHOUSE_NR_Q1); end if;
      if (rec.CONTRACT_R_Q1 is not null) then col07:=(col07+rec.CONTRACT_R_Q1); end if;
      if (rec.INHOUSE_R_Q1 is not null) then col08:=(col08+rec.INHOUSE_R_Q1); end if;
      if (rec.CONTRACT_NR_Q2 is not null) then col09:=(col09+rec.CONTRACT_NR_Q2); end if;
      if (rec.INHOUSE_NR_Q2 is not null) then col10:=(col10+rec.INHOUSE_NR_Q2); end if;
      if (rec.CONTRACT_R_Q2 is not null) then col11:=(col11+rec.CONTRACT_R_Q2); end if;
      if (rec.INHOUSE_R_Q2 is not null) then col12:=(col12+rec.INHOUSE_R_Q2); end if;
      if (rec.CONTRACT_NR_Q3 is not null) then col13:=(col13+rec.CONTRACT_NR_Q3); end if;
      if (rec.INHOUSE_NR_Q3 is not null) then col14:=(col14+rec.INHOUSE_NR_Q3); end if;
      if (rec.CONTRACT_R_Q3 is not null) then col15:=(col15+rec.CONTRACT_R_Q3); end if;
      if (rec.INHOUSE_R_Q3 is not null) then col16:=(col16+rec.INHOUSE_R_Q3); end if;
      if (rec.CONTRACT_NR_Q4 is not null) then col17:=(col17+rec.CONTRACT_NR_Q4); end if;
      if (rec.INHOUSE_NR_Q4 is not null) then col18:=(col18+rec.INHOUSE_NR_Q4); end if;
      if (rec.CONTRACT_R_Q4 is not null) then col19:=(col19+rec.CONTRACT_R_Q4); end if;
      if (rec.INHOUSE_R_Q4 is not null) then col20:=(col20+rec.INHOUSE_R_Q4); end if;
      
      
      xlsDocument.rowOpen;
      xlsDocument.addCell(p_style=>'TextStyleBlueCell',p_data=>rec.PRODUCTLINE);
      xlsDocument.addCell(p_style=>'TextStyleBlueCell',p_data=>rec.ACTIVITY);
      xlsDocument.addCell(p_style=>'NumberStyleWhiteCell_L',p_data_type=>'Number',p_data=>rec.CONTRACT_NR_TOTAL);
      xlsDocument.addCell(p_style=>'NumberStyleWhiteCell',p_data_type=>'Number',p_data=>rec.INHOUSE_NR_TOTAL);
      xlsDocument.addCell(p_style=>'NumberStyleWhiteCell',p_data_type=>'Number',p_data=>rec.CONTRACT_R_TOTAL);
      xlsDocument.addCell(p_style=>'NumberStyleWhiteCell_R',p_data_type=>'Number',p_data=>rec.INHOUSE_R_TOTAL);
      xlsDocument.addCell(p_style=>'NumberStyleWhiteCell_L',p_data_type=>'Number',p_data=>rec.CONTRACT_NR_Q1);
      xlsDocument.addCell(p_style=>'NumberStyleWhiteCell',p_data_type=>'Number',p_data=>rec.INHOUSE_NR_Q1);
      xlsDocument.addCell(p_style=>'NumberStyleWhiteCell',p_data_type=>'Number',p_data=>rec.CONTRACT_R_Q1);
      xlsDocument.addCell(p_style=>'NumberStyleWhiteCell_R',p_data_type=>'Number',p_data=>rec.INHOUSE_R_Q1);
      xlsDocument.addCell(p_style=>'NumberStyleWhiteCell_L',p_data_type=>'Number',p_data=>rec.CONTRACT_NR_Q2);
      xlsDocument.addCell(p_style=>'NumberStyleWhiteCell',p_data_type=>'Number',p_data=>rec.INHOUSE_NR_Q2);
      xlsDocument.addCell(p_style=>'NumberStyleWhiteCell',p_data_type=>'Number',p_data=>rec.CONTRACT_R_Q2);
      xlsDocument.addCell(p_style=>'NumberStyleWhiteCell_R',p_data_type=>'Number',p_data=>rec.INHOUSE_R_Q2);
      xlsDocument.addCell(p_style=>'NumberStyleWhiteCell_L',p_data_type=>'Number',p_data=>rec.CONTRACT_NR_Q3);
      xlsDocument.addCell(p_style=>'NumberStyleWhiteCell',p_data_type=>'Number',p_data=>rec.INHOUSE_NR_Q3);
      xlsDocument.addCell(p_style=>'NumberStyleWhiteCell',p_data_type=>'Number',p_data=>rec.CONTRACT_R_Q3);
      xlsDocument.addCell(p_style=>'NumberStyleWhiteCell_R',p_data_type=>'Number',p_data=>rec.INHOUSE_R_Q3);
      xlsDocument.addCell(p_style=>'NumberStyleWhiteCell_L',p_data_type=>'Number',p_data=>rec.CONTRACT_NR_Q4);
      xlsDocument.addCell(p_style=>'NumberStyleWhiteCell',p_data_type=>'Number',p_data=>rec.INHOUSE_NR_Q4);
      xlsDocument.addCell(p_style=>'NumberStyleWhiteCell',p_data_type=>'Number',p_data=>rec.CONTRACT_R_Q4);
      xlsDocument.addCell(p_style=>'NumberStyleWhiteCell_R',p_data_type=>'Number',p_data=>rec.INHOUSE_R_Q4);
      xlsDocument.rowClose;
    end loop;
    
    insertPLsumRow;
    */
        
    xlsDocument.worksheetClose;
    
    xlsDocument.documentClose;
    
    --htp.p(xlsDocument.getDocument);
    theBlob := clob_to_blob(xlsDocument.getDocument);
    
    --theBlob:=xlsDocument.getDocumentBlob;
    
    
--mr update to add the file to the xmlfiles table

    lc_nXMLFILEID:=0;
    
    for rec in (select * from spd_t_list_set_plans where setplanid = in_nSetPlanId and exportfile is not null)
    loop
      lc_nXMLFILEID := rec.exportfile;
    end loop;
    
    if lc_nXMLFILEID = 0 then
--this has not had a file created, make a new one
      select SPD_S_XML_FILES.nextval into lc_nXMLFILEID from dual;
      
      
      insert into 
        spd_t_xml_files
        (
          xmlfileid,
          apexname,
          filename,
          MIME_TYPE,
          blob_content
        )
      values
        (
          lc_nXMLFILEID,
          l_plantitle||'.xls',
          l_plantitle||'.xls',
          'text/plain',
          theBlob
        );
      commit;
      
      update
        spd_t_list_set_plans
      set
        exportfile = lc_nXMLFILEID
      where
        setplanid = IN_nSETPLANID;
    else
--we update the blob_content only
      update
        spd_t_xml_files
      set
        blob_content = theBlob
      where
        xmlfileid = lc_nXMLFILEID;
      commit;
        
    end if;
    
    
    tobrowser.message('success',to_char(lc_nXMLFILEID));
  END get_funding_request;
END REPORTPROVIDER;

/
