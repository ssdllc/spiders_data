--------------------------------------------------------
--  File created - Monday-August-06-2018   
--------------------------------------------------------
--------------------------------------------------------
--  DDL for Package Body PKG_READ_DATA_TABLE
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE PACKAGE BODY "SPIDERS_DATA"."PKG_READ_DATA_TABLE" as

/*note, public functions are underscores, local functions are camel case*/

procedure p_log(in_vString varchar2) as 

begin
  dbms_output.put_line(in_vString);
end;

procedure p_log_htp(in_vString varchar2) as 

begin
  htp.p(in_vString);
end;

function f_test_for_error (in_vReturned varchar2) return varchar2 as 
  /* template variables */
  lc_vFunctionName varchar2(255) := 'f_test_for_error';
  lc_vReturn varchar2(32000) := '{"error":"default"}'; --defaul the return to e for error
  lc_nContinue number :=0; --default to 0 to dissalow continuing
  
  /* function specific variables */
  lc_vTest varchar2(32000) := in_vReturned;
begin

  if substr(lc_vTest, 1, 9) = '{"error":' then
    lc_vReturn := '0';
  else
    lc_vReturn := '1';
  end if;
  
  return lc_vReturn;

exception     
  when others then
      execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' dml error: ' || sqlerrm || ''')';
      return '{"error":"' || sqlerrm || '"}';
end; 

function f_parseGroupFilterData(in_jJson json, in_vTableName varchar2) return varchar2 as
  /* template variables */
  lc_vFunctionName varchar2(255) := 'f_parseGroupFilterData';

  json_error EXCEPTION;
  spiders_error EXCEPTION;
  lc_vErrorDescription varchar2(255) := 'default';

  lc_vReturn varchar2(32000) := '{"error":"default"}'; --defaul the return to e for error
  lc_nContinue number :=0; --default to 0 to dissalow continuing

  
  /* function specific variables */
  lc_jlData json_list;
  lc_jvRec json_value;
  lc_jValue json;
  lc_vGroupName varchar2(255);
  lc_vInClause long;
  lc_jvValue json_value;
  lc_jlDataValue json_list;
  lc_vValue varchar2(255);
  lc_vGroupWhere varchar(255);
  lc_nGroupCount number := 1;
begin

  if(in_jJson.exist('groupFilterData')) then
      if(in_jJson.get('groupFilterData').is_array) then
          lc_jlData := json_list(in_jJson.get('groupFilterData'));
          if(lc_jlData.count>0) then
            for rec in 1..lc_jlData.count loop
              lc_jvRec := lc_jlData.get(rec);
              lc_jValue := json(lc_jvRec);
              if (lc_jValue.exist('group_name')) then
                  lc_jvValue := lc_jValue.get('group_name');
                  lc_vGroupName := getVal(lc_jvValue); 
                  if (lc_jValue.exist('data')) then
                    lc_jlDataValue := json_list(lc_jValue.get('data'));
                    lc_vInClause := '' || lc_vGroupName || ' in (';
                    for rec2 in 1..lc_jlDataValue.count loop
                      lc_jvValue := lc_jlDataValue.get(rec2);                  
                      lc_vValue := getVal(lc_jvValue,1);   
                      if(rec2 = 1) then
                        lc_vInClause := lc_vInClause || lc_vValue;
                      else
                        lc_vInClause := lc_vInClause || ',' || lc_vValue;
                      end if;  
                    end loop;
                    lc_vInClause := lc_vInClause || ')';
                    if(lc_nGroupCount=1) then
                      lc_vGroupWhere := lc_vGroupWhere || lc_vInClause;
                    else
                      lc_vGroupWhere := lc_vGroupWhere || ' AND ' || lc_vInClause;
                    end if;
                    lc_nGroupCount := lc_nGroupCount + 1;
                  else
                    lc_vErrorDescription := 'data does not exist';
                  end if; 
              else
                lc_vErrorDescription := 'group_name does not exist';
              end if;
            end loop;
          else
            lc_vGroupWhere := '';
          end if;
      else
        lc_vErrorDescription := 'groupFilterData does not an array';
      end if;
  else
    lc_vErrorDescription := 'groupFilterData does not exist';
    --raise spiders_error;
    lc_vGroupWhere := '';
  end if;
  
  lc_vReturn := lc_vGroupWhere;
  return lc_vReturn;

exception   
  when spiders_error then
      execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' SPIDERS error ' || lc_vErrorDescription || ''')';
      return '{"error":"' || lc_vErrorDescription || '"}';
  when json_error then
      execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' JSON error'')';
      return '{"error":"json"}';
  when others then
      execute immediate 'insert into debug (functionanme, message) values (''' || lc_vFunctionName || ''','' dml error: ' || sqlerrm || ''')';
      return '{"error":"' || sqlerrm || '"}';
end; 

function f_parseAoDataSearch(in_jJson json) return varchar2 as
  /* template variables */
  lc_vFunctionName varchar2(255) := 'f_parseAoDataSearch';

  json_error EXCEPTION;
  spiders_error EXCEPTION;
  lc_vErrorDescription varchar2(255) := 'default';

  lc_vReturn varchar2(32000) := '{"error":"default"}'; --defaul the return to e for error
  lc_nContinue number :=0; --default to 0 to dissalow continuing

  
  /* function specific variables */
  lc_jlAoData json_list;
  lc_jvSearch json_value;
  lc_jValue json;
  lc_jValue2 json;
  lc_jvValue json_value;
  lc_jvValue2 json_value;
  lc_vSearch varchar2(255);
  lc_vValue varchar2(255);
begin

  
  if(in_jJson.exist('aoData')) then
    --htp.p('DID IT GET HERE');
    --lc_jvSearch := in_jJson.get('aoData');
    --htp.p('DID IT GET HERE2');
    --lc_jlAoData := json_list(lc_jvSearch); --THIS IS THE PROBLEM
     --htp.p('DID IT GET HERE3');
    lc_jlAoData := json_list(in_jJson.get('aoData'));
     --htp.p('DID IT GET HERE');
    lc_jvSearch := lc_jlAoData.get(6);
    --htp.p('DID IT GET HERE');
    lc_jValue := json(lc_jvSearch);
    --htp.p('DID IT GET HERE');
    lc_jvValue := lc_jValue.get('value');
    lc_jValue2 := json(lc_jvValue);
   -- htp.p('DID IT GET HERE');
    lc_jvValue2 := lc_jValue2.get('value');
    
    lc_vSearch := getVal(lc_jvValue2);
    lc_vSearch := lower(lc_vSearch);
    lc_vReturn := lc_vSearch;
  else
    lc_vErrorDescription := 'aoData does not exist';
    raise spiders_error;
  end if;

  return lc_vReturn;

exception   
  when spiders_error then
      execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' SPIDERS error ' || lc_vErrorDescription || ''')';
      return '{"error":"' || lc_vErrorDescription || '"}';
  when json_error then
      execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' JSON error'')';
      return '{"error":"json"}';
  when others then
      execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' dml error: ' || sqlerrm || ''')';
      return '{"error":"' || sqlerrm || '"}';
end; 

function f_parseAoDataStart(in_jJson json) return varchar2 as
  /* template variables */
  lc_vFunctionName varchar2(255) := 'f_parseAoDataStart';

  json_error EXCEPTION;
  spiders_error EXCEPTION;
  lc_vErrorDescription varchar2(255) := 'default';

  lc_vReturn varchar2(32000) := '{"error":"default"}'; --defaul the return to e for error
  lc_nContinue number :=0; --default to 0 to dissalow continuing

  
  /* function specific variables */
  lc_jlAoData json_list;
  lc_jvStart json_value;
  lc_jValue json;
  lc_jvValue json_value;
  lc_nStart number;
begin

  if(in_jJson.exist('aoData')) then
    lc_jlAoData := json_list(in_jJson.get('aoData'));
    lc_jvStart := lc_jlAoData.get(4);
    lc_jValue := json(lc_jvStart);
    lc_jvValue := lc_jValue.get('value');
    lc_nStart := getVal(lc_jvValue);
    lc_nStart := lc_nStart + 1;
    lc_vReturn := lc_nStart;
  else
    lc_vErrorDescription := 'aoData does not exist';
    raise spiders_error;
  end if;

  return lc_vReturn;

exception   
  when spiders_error then
      execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' SPIDERS error ' || lc_vErrorDescription || ''')';
      return '{"error":"' || lc_vErrorDescription || '"}';
  when json_error then
      execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' JSON error'')';
      return '{"error":"json"}';
  when others then
      execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' dml error: ' || sqlerrm || ''')';
      return '{"error":"' || sqlerrm || '"}';
end; 

function f_parseAoDataRecPerPage(in_jJson json) return varchar2 as
  /* template variables */
  lc_vFunctionName varchar2(255) := 'f_parseAoDataRecPerPage';

  json_error EXCEPTION;
  spiders_error EXCEPTION;
  lc_vErrorDescription varchar2(255) := 'default';

  lc_vReturn varchar2(32000) := '{"error":"default"}'; --defaul the return to e for error
  lc_nContinue number :=0; --default to 0 to dissalow continuing

  
  /* function specific variables */
  lc_jlAoData json_list;
  lc_jvRecordsPerPage json_value;
  lc_jValue json;
  lc_jvValue json_value;
  lc_nRecordsPerPage number;
begin

  if(in_jJson.exist('aoData')) then
    lc_jlAoData := json_list(in_jJson.get('aoData'));
    lc_jvRecordsPerPage := lc_jlAoData.get(5);
    lc_jValue := json(lc_jvRecordsPerPage);
    lc_jvValue := lc_jValue.get('value');
    lc_nRecordsPerPage := getVal(lc_jvValue);
    lc_vReturn := lc_nRecordsPerPage;
  else
    lc_vErrorDescription := 'aoData does not exist';
    raise spiders_error;
  end if;

  return lc_vReturn;

exception   
  when spiders_error then
      execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' SPIDERS error ' || lc_vErrorDescription || ''')';
      return '{"error":"' || lc_vErrorDescription || '"}';
  when json_error then
      execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' JSON error'')';
      return '{"error":"json"}';
  when others then
      execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' dml error: ' || sqlerrm || ''')';
      return '{"error":"' || sqlerrm || '"}';
end; 

function getAoDataOrderByColumnName (in_jJson json, in_nColumn number) return varchar2 as
  /* template variables */
  lc_vFunctionName varchar2(255) := 'getAoDataOrderByColumnName';

  json_error EXCEPTION;
  spiders_error EXCEPTION;
  lc_vErrorDescription varchar2(255) := 'default';

  lc_vReturn varchar2(32000) := '{"error":"default"}'; --defaul the return to e for error
  lc_nContinue number :=0; --default to 0 to dissalow continuing

  
  /* function specific variables */
  lc_jlAoData json_list;
  lc_jvColumns json_value;
  lc_jValue json;
  lc_jvValue json_value;
  lc_jlOrderByColumns json_list;
  lc_jValue2 json;
  lc_jvOrderByColumn json_value;
  lc_jColumn json;
  lc_jvColumnIndex json_value;  
  lc_jDirection json;
  lc_jvDirection json_value;  
  lc_vDirection varchar2(255);
  lc_vOrderBy varchar2(255);
  lc_nColumnIndex number;
  lc_jvColumnData json_value;
  lc_vColumnData varchar2(255);
begin

  if(in_jJson.exist('aoData')) then
    lc_jlAoData := json_list(in_jJson.get('aoData'));
    lc_jvColumns := lc_jlAoData.get(2);
    lc_jValue := json(lc_jvColumns);
    
    lc_jvValue := lc_jValue.get('value');
    
    lc_jlOrderByColumns := json_list(lc_jvValue);

    --set the column index
    lc_jvOrderByColumn := lc_jlOrderByColumns.get(in_nColumn);

  
    --NEED TO GET THE COLUMN NAME FROM THE INDEX
    lc_jColumn := json(lc_jvOrderByColumn);
    lc_jvColumnData := lc_jColumn.get('data');
    lc_vColumnData := getVal(lc_jvColumnData);
    
    lc_vReturn := lc_vColumnData;
 
  else
    lc_vErrorDescription := 'aoData does not exist';
    raise spiders_error;
  end if;

  return lc_vReturn;

exception   
  when spiders_error then
      execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' SPIDERS error ' || lc_vErrorDescription || ''')';
      return '{"error":"' || lc_vErrorDescription || '"}';
  when json_error then
      execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' JSON error'')';
      return '{"error":"json"}';
  when others then
      execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' dml error: ' || sqlerrm || ''')';
      return '{"error":"' || sqlerrm || '"}';
end; 

function f_wrapOrderByColumnName(in_colName varchar2) return varchar2 as
  lc_return varchar2(256);
  lc_count number;
begin
  select count(column_name) into lc_count
  from user_tab_cols
  where column_name = in_colName 
    and (data_type = 'VARCHAR2' or data_type = 'CLOB' or data_type = 'CHAR');
    
  if lc_count = 0 then
    return in_colName;
  else 
    return 'lower(' || in_colName || ')';
  end if;
  
end f_wrapOrderByColumnName;

function f_parseAoDataOrderBy(in_jJson json) return varchar2 as
  /* template variables */
  lc_vFunctionName varchar2(255) := 'f_parseAoDataOrderBy';

  json_error EXCEPTION;
  spiders_error EXCEPTION;
  lc_vErrorDescription varchar2(255) := 'default';

  lc_vReturn varchar2(32000) := '{"error":"default"}'; --defaul the return to e for error
  lc_nContinue number :=0; --default to 0 to dissalow continuing

  
  /* function specific variables */
  lc_jlAoData json_list;
  lc_jvOrder json_value;
  lc_jValue json;
  lc_jvValue json_value;
  lc_jlOrderByColumns json_list;
  lc_jValue2 json;
  lc_jvOrderByColumn json_value;
  lc_jColumnIndex json;
  lc_jvColumnIndex json_value;  
  lc_jDirection json;
  lc_jvDirection json_value;  
  lc_vDirection varchar2(255);
  lc_vOrderBy varchar2(255);
  lc_nColumnIndex number;
  lc_vColumnName varchar2(255);
begin

  if(in_jJson.exist('aoData')) then
    lc_jlAoData := json_list(in_jJson.get('aoData'));
    lc_jvOrder := lc_jlAoData.get(3);
    lc_jValue := json(lc_jvOrder);
    
    lc_jvValue := lc_jValue.get('value');
    
    lc_jlOrderByColumns := json_list(lc_jvValue);
    for rec in 1..lc_jlOrderByColumns.count loop
      --set the column index
      lc_jvOrderByColumn := lc_jlOrderByColumns.get(rec);
      
      
      --NEED TO GET THE COLUMN NAME FROM THE INDEX
      lc_jColumnIndex := json(lc_jvOrderByColumn);
      lc_jvColumnIndex := lc_jColumnIndex.get('column');
      lc_nColumnIndex := getVal(lc_jvColumnIndex);
      lc_nColumnIndex := lc_nColumnIndex + 1;
      lc_vColumnName := getAoDataOrderByColumnName (in_jJson,lc_nColumnIndex);
      
      --set the order by direction
      lc_jvDirection := lc_jColumnIndex.get('dir');
      lc_vDirection := getVal(lc_jvDirection);
      
      
      if rec = 1 then
        lc_vOrderBy := f_wrapOrderByColumnName(lc_vColumnName) || ' ' || lc_vDirection;
        --lc_vOrderBy := 'lower(' || lc_vColumnName || ') ' || lc_vDirection;
      else
        lc_vOrderBy := lc_vOrderBy || ', '|| f_wrapOrderByColumnName(lc_vColumnName) || ' ' || lc_vDirection;
        --lc_vOrderBy := lc_vOrderBy || ', lower(' || lc_vColumnName || ') ' || lc_vDirection;
      end if;
    end loop;
    
    lc_vReturn := lc_vOrderBy;
 
  else
    lc_vErrorDescription := 'aoData does not exist';
    raise spiders_error;
  end if;

  return lc_vReturn;

exception   
  when spiders_error then
      execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' SPIDERS error ' || lc_vErrorDescription || ''')';
      return '{"error":"' || lc_vErrorDescription || '"}';
  when json_error then
      execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' JSON error'')';
      return '{"error":"json"}';
  when others then
      execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' dml error: ' || sqlerrm || ''')';
      return '{"error":"' || sqlerrm || '"}';
end; 

function f_getDynamicWhere(in_vSearch varchar2, in_vTableName varchar2) return varchar2 as
  /* template variables */
  lc_vFunctionName varchar2(255) := 'f_getDynamicWhere';

  json_error EXCEPTION;
  spiders_error EXCEPTION;
  lc_vErrorDescription varchar2(255) := 'default';

  lc_vReturn varchar2(32000) := '{"error":"default"}'; --defaul the return to e for error
  lc_nContinue number :=0; --default to 0 to dissalow continuing

  
  /* function specific variables */
  lc_vWhereSegment long;
  lc_vTemp varchar2(255);
  lc_vPrefix varchar2(255) :='SPD_V_DT_';
  lc_nCount number;
  lc_vColumnWhere long;
begin

--note: probably want to check if the object exists first.
  lc_nCount := 1;
  
  for rec in (
    select 
      column_name, 
      data_type, 
      rownum 
    from 
      user_tab_cols 
    where 
      table_name = lc_vPrefix || in_vTableName 
      --and lower(column_name) not like '%id' this doesnt work with FACILITY_ID for an NFA number
      and lower(column_name) not like '%count%'
      and data_type = 'VARCHAR2'
    order by 
      column_id)
  loop
    lc_vTemp := rec.column_name;
    if lc_nCount = 1 then
      lc_vWhereSegment := '(upper(' || lc_vTemp|| ') like ''%' || upper(in_vSearch) || '%''';
    else
      lc_vColumnWhere := 'upper(' || lc_vTemp|| ') like ''%' || upper(in_vSearch) || '%''';
      lc_vWhereSegment := lc_vWhereSegment || ' or ' || lc_vColumnWhere;
    end if;
    lc_nCount := lc_nCount + 1;
  end loop;
  
  if length(lc_vWhereSegment)>0 then
    lc_vWhereSegment := lc_vWhereSegment || ')';
  end if;
  
  lc_vReturn := lc_vWhereSegment;
  return lc_vReturn;

exception   
  when spiders_error then
      execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' SPIDERS error ' || lc_vErrorDescription || ''')';
      return '{"error":"' || lc_vErrorDescription || '"}';
  when json_error then
      execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' JSON error'')';
      return '{"error":"json"}';
  when others then
      execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' dml error: ' || sqlerrm || ''')';
      return '{"error":"' || sqlerrm || '"}';
end; 

function f_buildFullWhere(in_vWhereClause varchar2, in_vDynamicWhere varchar2, in_vGroupWhere varchar2 ) return varchar2 as
  /* template variables */
  lc_vFunctionName varchar2(255) := 'f_buildFullWhere';

  json_error EXCEPTION;
  spiders_error EXCEPTION;
  lc_vErrorDescription varchar2(255) := 'default';

  lc_vReturn varchar2(32000) := '{"error":"default"}'; --defaul the return to e for error
  lc_nContinue number :=0; --default to 0 to dissalow continuing

  
  /* function specific variables */
  lc_vWhereSegment long;
begin

  lc_vWhereSegment := in_vWhereClause;
  if length(in_vWhereClause) > 0 then
    lc_vWhereSegment := in_vWhereClause;
  
    if length(in_vDynamicWhere) > 0 then
      if length(in_vGroupWhere) > 0 then
        lc_vWhereSegment := lc_vWhereSegment || ' AND ' || in_vDynamicWhere || ' AND ' || in_vGroupWhere ; /*WE MAY NEED TO WRAP GROUP IN A PARENTHESIS*/
      else
        lc_vWhereSegment := lc_vWhereSegment || ' AND ' || in_vDynamicWhere;
      end if;
    else
      null; -- NO OTHER WHERE STUFF
    end if;
  else
    if length(in_vDynamicWhere) > 0 then
      if length(in_vGroupWhere) > 0 then
        lc_vWhereSegment := in_vDynamicWhere || ' AND ' || in_vGroupWhere ; /*WE MAY NEED TO WRAP GROUP IN A PARENTHESIS*/
      else
        lc_vWhereSegment := in_vDynamicWhere;
      end if;
    else
      if length(in_vGroupWhere) > 0 then
        lc_vWhereSegment := in_vGroupWhere;
      else
        lc_vWhereSegment := '1=1';
      end if;
    end if;
  end if;
  
  
  lc_vReturn := lc_vWhereSegment;
  return lc_vReturn;

exception   
  when spiders_error then
      execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' SPIDERS error ' || lc_vErrorDescription || ''')';
      return '{"error":"' || lc_vErrorDescription || '"}';
  when json_error then
      execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' JSON error'')';
      return '{"error":"json"}';
  when others then
      execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' dml error: ' || sqlerrm || ''')';
      return '{"error":"' || sqlerrm || '"}';
end; 

function f_buildGroupFilter (in_vTableName varchar2, in_vWhere varchar2, in_vModule varchar2) return json as
  /* template variables */
  lc_vFunctionName varchar2(255) := 'f_buildGroupFilter';

  json_error EXCEPTION;
  spiders_error EXCEPTION;
  lc_vErrorDescription varchar2(255) := 'default';

  lc_vReturn varchar2(32000) := '{"error":"default"}'; --defaul the return to e for error
  lc_nContinue number :=0; --default to 0 to dissalow continuing

  
  /* function specific variables */
  lc_vSql long;
  lc_vWhere long;
  lc_vPrefix varchar2(25) := 'SPD_V_';
  lc_jJson json;
begin


  for rec in ( select group_filter, rownum rn from SPD_T_COLLECTION_GRP_FLTRS where module = in_vModule and collection_name = in_vTableName order by visual_order) loop
    if rec.rn = 1 then
      lc_vSql := ' select * from (select * from (select * from ' || lc_vPrefix || in_vTableName || '_' || rec.GROUP_FILTER || in_vWhere  || ') where r_n=1 order by record_count desc) ';
    else
      lc_vSql := lc_vSql || ' union select * from (select * from (select * from ' || lc_vPrefix || in_vTableName || '_' || rec.GROUP_FILTER || in_vWhere  || ') where r_n=1 order by record_count desc) ';
    end if;
    
  end loop;

  
  lc_jJson := json_dyn.executeObject(lc_vSql);
  return lc_jJson;

exception   
  when spiders_error then
      execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' SPIDERS error ' || lc_vErrorDescription || ''')';
      return json('{"error":"' || lc_vErrorDescription || '"}');
  when json_error then
      execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' JSON error'')';
      return json('{"error":"json"}');
  when others then
      execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' dml error: ' || sqlerrm || ''')';
      return json('{"error":"' || sqlerrm || '"}');
end; 

function f_buildFullSql (in_vTableName varchar2, in_vWhere varchar2, in_vOrderBy varchar2, in_vStart varchar2, in_nRecordsPerPage number) return json as
  /* template variables */
  lc_vFunctionName varchar2(255) := 'f_buildFullSql';

  json_error EXCEPTION;
  spiders_error EXCEPTION;
  lc_vErrorDescription varchar2(255) := 'default';

  lc_vReturn varchar2(32000) := '{"error":"default"}'; --defaul the return to e for error
  lc_nContinue number :=0; --default to 0 to dissalow continuing

  
  /* function specific variables */
  lc_vSql long;
  lc_vWhere long;
  lc_vEditWhere long;
  lc_jJson json;
begin

  lc_vEditWhere := REPLACE(in_vWhere, '^', '''');
  
/* 
  execute immediate 'insert into debug (input, message) values (''a1'',''' || in_vTableName || ''')';
  execute immediate 'insert into debug (input, message) values (''a2'',''' || lc_vEditWhere || ''')';
  execute immediate 'insert into debug (input, message) values (''a3'',''' || in_vOrderBy || ''')';
  execute immediate 'insert into debug (input, message) values (''a4'',''' || in_vStart || ''')';
  execute immediate 'insert into debug (input, misc) values (''a6'',''' || in_nRecordsPerPage || ''')';
*/

  
  lc_vSql := 'SELECT  
    *
  FROM    
    (
      SELECT  
        t.*, 
        ROWNUM AS rn
      FROM
        (
          select 
            *
          from 
            spd_v_dt_' || in_vTableName || '
          where
            ' || lc_vEditWhere || '
          order by 
            ' || in_vOrderBy || '
        ) t
    )
  WHERE   
    rn >= ' || in_vStart || '
    AND rownum <= ' || in_nRecordsPerPage;
    
 -- execute immediate 'insert into debug (message) values (''' || lc_vSql || ''')';

  lc_jJson := json_dyn.executeObject(lc_vSql);
  return lc_jJson;

exception   
  when spiders_error then
      execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' SPIDERS error ' || lc_vErrorDescription || ''')';
      return json('{"error":"' || lc_vErrorDescription || '"}');
  when json_error then
      execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' JSON error'')';
      return json('{"error":"json"}');
  when others then
      execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' dml error: ' || sqlerrm || ''')';
      return json('{"error":"' || sqlerrm || '"}');
end; 

function f_buildFullSqlString (in_vTableName varchar2, in_vWhere varchar2, in_vOrderBy varchar2) return varchar2 as
  /* template variables */
  lc_vFunctionName varchar2(255) := 'f_buildFullSqlString';

  json_error EXCEPTION;
  spiders_error EXCEPTION;
  lc_vErrorDescription varchar2(255) := 'default';

  lc_vReturn varchar2(32000) := '{"error":"default"}'; --defaul the return to e for error
  lc_nContinue number :=0; --default to 0 to dissalow continuing

  
  /* function specific variables */
  lc_vSql long;
  lc_vWhere long;
  lc_jJson json;
begin

  lc_vSql := 'SELECT  
        t.*, 
        ROWNUM AS rn
      FROM
        (
          select 
            *
          from 
            spd_v_dt_' || in_vTableName || '
          where
            ' || in_vWhere || '
          order by 
            ' || in_vOrderBy || '
        ) t';

  lc_vReturn := lc_vSql;
  return lc_vReturn;

exception   
  when spiders_error then
      execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' SPIDERS error ' || lc_vErrorDescription || ''')';
      return '{"error":"' || lc_vErrorDescription || '"}';
  when json_error then
      execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' JSON error'')';
      return '{"error":"json"}';
  when others then
      execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' dml error: ' || sqlerrm || ''')';
      return '{"error":"' || sqlerrm || '"}';
end; 

function f_getRecordCount (in_vTableName varchar2, in_vWhere varchar2, in_vOrderBy varchar2, in_vStart varchar2, in_nRecordsPerPage number) return json as
  /* template variables */
  lc_vFunctionName varchar2(255) := 'f_getRecordCount';

  json_error EXCEPTION;
  spiders_error EXCEPTION;
  lc_vErrorDescription varchar2(255) := 'default';

  lc_vReturn varchar2(32000) := '{"error":"default"}'; --defaul the return to e for error
  lc_nContinue number :=0; --default to 0 to dissalow continuing

  
  /* function specific variables */
  lc_vSql long;
  lc_vWhere long;
  lc_jJson json;
begin

  lc_vSql := '
    select 
      count(*) recordCount
    from 
      spd_v_dt_' || in_vTableName || '
    where
      ' || in_vWhere;

  lc_jJson := json_dyn.executeObject(lc_vSql);
  return lc_jJson;

exception   
  when spiders_error then
      execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' SPIDERS error ' || lc_vErrorDescription || ''')';
      return json('{"error":"' || lc_vErrorDescription || '"}');
  when json_error then
      execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' JSON error'')';
      return json('{"error":"json"}');
  when others then
      execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' dml error: ' || sqlerrm || ''')';
      return json('{"error":"' || sqlerrm || '"}');
end; 

function f_parseWhere(in_jJson json) return varchar2 as
 /* template variables */
  lc_vFunctionName varchar2(255) := 'f_parseWhere';

  json_error EXCEPTION;
  spiders_error EXCEPTION;
  lc_vErrorDescription varchar2(255) := 'default';

  lc_vReturn varchar2(32000) := '{"error":"default"}'; --defaul the return to e for error
  lc_nContinue number :=0; --default to 0 to dissalow continuing

  
  /* function specific variables */
  lc_jlWhereClause json_list;
  lc_jlWhereItem json_list;
  lc_vColumnName varchar2(255);
  lc_vWhereValue varchar2(255);
  lc_vWhereClauseSegment long;
  
  
  lc_vOperator varchar2(3) := 'and';
  lc_vComparatorStr varchar2(3) := '=';
  lc_vComparator varchar2(3) := '=';
  
begin


  if(in_jJson.exist('whereClause')) then
    if(in_jJson.get('whereClause').is_array) then
      lc_jlWhereClause := json_list(in_jJson.get('whereClause'));
      for rec in 1..lc_jlWhereClause.count loop
      
        lc_jlWhereItem := json_list(lc_jlWhereClause.get(rec));
        lc_vColumnName := getVal(lc_jlWhereItem.get(1));
        lc_vWhereValue := getVal(lc_jlWhereItem.get(2));
        
        if(lc_jlWhereItem.count>2)then
          lc_vOperator  := getVal(lc_jlWhereItem.get(3));
          --htp.p('lc_vOperator = '||lc_vOperator);
        end if;
        if(lc_jlWhereItem.count>3)then
          --get the comparator if it is included
          lc_vComparatorStr  := getVal(lc_jlWhereItem.get(4));
          --htp.p('COMPARATOR = '||lc_vComparatorStr);
          if(lc_vComparatorStr = 'gt')then
            lc_vComparator := '>';
          elsif(lc_vComparatorStr = 'lt')then
            lc_vComparator := '<';
          end if;
        end if;
        
          
        if rec = 1 then
          lc_vWhereClauseSegment := '(' || lc_vColumnName || lc_vComparator || '''' || lc_vWhereValue  || '''';
        else
          lc_vWhereClauseSegment := lc_vWhereClauseSegment || ' '||lc_vOperator||' ' || lc_vColumnName || lc_vComparator || '''' || lc_vWhereValue  || '''';
        end if;
        
        if rec = lc_jlWhereClause.count then
          lc_vWhereClauseSegment := lc_vWhereClauseSegment || ')';
        end if;
      end loop;
    else
      lc_vErrorDescription := 'whereClause is not an array';
      lc_vWhereClauseSegment := '';
      --raise spiders_error;
    end if;
  else
    lc_vErrorDescription := 'whereClause does not exist';
    lc_vWhereClauseSegment := '';
    --raise spiders_error;
  end if;

  

  lc_vReturn := lc_vWhereClauseSegment;
  return lc_vReturn;
  
  
  
  
  
 /*
  if(in_jJson.exist('whereClause')) then
    if(in_jJson.get('whereClause').is_array) then
      lc_jlWhereClause := json_list(in_jJson.get('whereClause'));
      for rec in 1..lc_jlWhereClause.count loop
      
        lc_jlWhereItem := json_list(lc_jlWhereClause.get(rec));
        lc_vColumnName := getVal(lc_jlWhereItem.get(1));
        lc_vWhereValue := getVal(lc_jlWhereItem.get(2));
          
          
          
       
        if(lc_jlWhereItem.count>2)then
          --this might need to be reset to and, but for now i'm leaving it as is
          --lc_vOperator := 'or'; --tc did this 20171012
          lc_vOperator  := getVal(lc_jlWhereItem.get(3));
          --htp.p('lc_vOperator = '||lc_vOperator);
        end if;
        
        
        if(lc_jlWhereItem.count>3)then
          --get the comparator if it is included
          lc_vComparatorStr  := getVal(lc_jlWhereItem.get(4));
          --htp.p('COMPARATOR = '||lc_vComparatorStr);
          if(lc_vComparatorStr = 'gt')then
            lc_vComparator := '=';
          elsif(lc_vComparatorStr = 'lt')then
            lc_vComparator := '=';
          end if;
        end if;
       
          
        if rec = 1 then
          --lc_vWhereClauseSegment := ' WHERE ' || lc_vColumnName || '=''' || lc_vWhereValue  || '''';
          lc_vWhereClauseSegment := ' WHERE ' || lc_vColumnName || ' ' || lc_vComparator || ' ''' || lc_vWhereValue  || '''';
        else
          lc_vWhereClauseSegment := lc_vWhereClauseSegment || ' ' || lc_vOperator || ' ' || lc_vColumnName || ' ' || lc_vComparator || ' ''' || lc_vWhereValue  || '''';
        end if;
        
        
         
          
          

        
  
      end loop;
      
    else
      lc_vErrorDescription := 'whereClause is not an array';
      lc_vWhereClauseSegment := '';
      --raise spiders_error;
    end if;
  else
    lc_vErrorDescription := 'whereClause does not exist';
    lc_vWhereClauseSegment := '';
    --raise spiders_error;
  end if;

  

  lc_vReturn := lc_vWhereClauseSegment;
  --htp.p(lc_vReturn);
  return lc_vReturn;
 */ 
exception   
  when spiders_error then
      execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' SPIDERS error ' || lc_vErrorDescription || ''')';
      return '{"error":"' || lc_vErrorDescription || '"}';
  when json_error then
      execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' JSON error'')';
      return '{"error":"json"}';
  when others then
      execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' dml error: ' || sqlerrm || ''')';
      return '{"error":"' || sqlerrm || '"}';
end; 

function f_parseWhereORIG(in_jJson json) return varchar2 as
 /* template variables */
  lc_vFunctionName varchar2(255) := 'f_parseWhere';

  json_error EXCEPTION;
  spiders_error EXCEPTION;
  lc_vErrorDescription varchar2(255) := 'default';

  lc_vReturn varchar2(32000) := '{"error":"default"}'; --defaul the return to e for error
  lc_nContinue number :=0; --default to 0 to dissalow continuing

  
  /* function specific variables */
  lc_jlWhereClause json_list;
  lc_jlWhereItem json_list;
  lc_vColumnName varchar2(255);
  lc_vWhereValue varchar2(255);
  lc_vWhereClauseSegment long;
  
  
begin

  if(in_jJson.exist('whereClause')) then
    if(in_jJson.get('whereClause').is_array) then
      lc_jlWhereClause := json_list(in_jJson.get('whereClause'));
      for rec in 1..lc_jlWhereClause.count loop
      
        lc_jlWhereItem := json_list(lc_jlWhereClause.get(rec));
        lc_vColumnName := getVal(lc_jlWhereItem.get(1));
        lc_vWhereValue := getVal(lc_jlWhereItem.get(2));
          
        if rec = 1 then
          lc_vWhereClauseSegment := '(' || lc_vColumnName || '=''' || lc_vWhereValue  || '''';
        else
          lc_vWhereClauseSegment := lc_vWhereClauseSegment || ' and ' || lc_vColumnName || '=''' || lc_vWhereValue  || '''';
        end if;
        
        if rec = lc_jlWhereClause.count then
          lc_vWhereClauseSegment := lc_vWhereClauseSegment || ')';
        end if;
      end loop;
    else
      lc_vErrorDescription := 'whereClause is not an array';
      lc_vWhereClauseSegment := '';
      --raise spiders_error;
    end if;
  else
    lc_vErrorDescription := 'whereClause does not exist';
    lc_vWhereClauseSegment := '';
    --raise spiders_error;
  end if;

  

  lc_vReturn := lc_vWhereClauseSegment;
  return lc_vReturn;

exception   
  when spiders_error then
      execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' SPIDERS error ' || lc_vErrorDescription || ''')';
      return '{"error":"' || lc_vErrorDescription || '"}';
  when json_error then
      execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' JSON error'')';
      return '{"error":"json"}';
  when others then
      execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' dml error: ' || sqlerrm || ''')';
      return '{"error":"' || sqlerrm || '"}';
end; 

function f_buildGroupFilterSql (in_vTableName varchar2, in_vWhere varchar2, in_vModule varchar2) return json as
  /* template variables */
  lc_vFunctionName varchar2(255) := 'f_buildGroupFilterSql';

  json_error EXCEPTION;
  spiders_error EXCEPTION;
  lc_vErrorDescription varchar2(255) := 'default';

  lc_vReturn varchar2(32000) := '{"error":"default"}'; --defaul the return to e for error
  lc_nContinue number :=0; --default to 0 to dissalow continuing

  
  /* function specific variables */
  lc_vPrefix varchar2(255) :='SPD_V_';
  lc_vSql long;
  lc_vWhere long;
  lc_jJson json;
  lc_vViewName varchar2(255);
begin

  
  
  for rec in ( select group_filter, rownum rn from  SPD_T_COLLECTION_GRP_FLTRS where module = in_vModule and collection_name = in_vTableName order by visual_order) loop
    lc_vViewName := lc_vPrefix || in_vTableName || '_' || rec.group_filter;
    if rec.rn = 1 then
      lc_vSql := ' (select * from (select * from (select * from ' || lc_vViewName || ' where ' ||  in_vWhere  || ') where rn=1 order by record_count desc) ';
    else
      lc_vSql := lc_vSql || ' union (select * from (select * from (select * from ' || lc_vViewName || ' where ' ||  in_vWhere  || ') where rn=1 order by record_count desc) ';
    end if;
  end loop;
  
  htp.p(lc_vSql);
  
  lc_jJson := json_dyn.executeObject(lc_vSql);
  lc_jJson.htp;
  
  return lc_jJson;

exception   
  when spiders_error then
      execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' SPIDERS error ' || lc_vErrorDescription || ''')';
      return json('{"error":"' || lc_vErrorDescription || '"}');
  when json_error then
      execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' JSON error'')';
      return json('{"error":"json"}');
  when others then
      execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' dml error: ' || sqlerrm || ''')';
      return json('{"error":"' || sqlerrm || '"}');
end; 

function f_read_data_table(in_jJson json, in_vTableName varchar2, in_vModule varchar2) return json as
  /* template variables */
  json_error EXCEPTION;
  spiders_error EXCEPTION;
  lc_vErrorDescription varchar2(255) := 'default';
  lc_vFunctionName varchar2(255) := 'f_read_data_table';
  lc_jReturn json; --defaul the return to e for error
  lc_nContinue number :=0; --default to 0 to dissalow continuing

  
  /* function specific variables */
  lc_vSql long;
  lc_jJson json;
  lc_vWhere long;
  lc_vSearch long := '';
  lc_nStart number :=1;
  lc_vGroupWhere long :='';
  lc_nRecordsPerPage number := 10;
  lc_vOrderBy long :='';
  lc_vDynamicWhere long := '';
  lc_jGroupFilter json;
  lc_jDataTable json;
  lc_jRecordCount json;
  lc_vWhereClause long;
  
begin
  lc_jReturn := json('{"error":"default"}');
  lc_jGroupFilter := json();
  lc_jDataTable := json();
  
  lc_vSearch :='';
  lc_nStart :=1;
  lc_nRecordsPerPage :=10;
  lc_vOrderBy :=' REPORTID asc '; --need a default sort by
  
  lc_vGroupWhere := f_parseGroupFilterData(in_jJson, in_vTableName);
  lc_nContinue := f_test_for_error(lc_vGroupWhere);
  
  p_log(lc_vGroupWhere);
  
  if lc_nContinue = 1 then
    lc_vSearch := f_parseAoDataSearch (in_jJson);
    lc_nContinue := f_test_for_error(lc_vSearch);
    if length(lc_vSearch) is not null then
      if lc_nContinue = 1 then
        lc_vDynamicWhere := f_getDynamicWhere (lc_vSearch, in_vTableName);
        lc_nContinue := f_test_for_error(lc_vDynamicWhere);
      else
        lc_vErrorDescription := 'something went wrong in aoData search parsing';
        raise spiders_error;
      end if;
    end if;
  else
    lc_vErrorDescription := 'something went wrong in group filters parsing';
    raise spiders_error;
  end if;

  if lc_nContinue = 1 then
    lc_vWhereClause := f_parseWhere (in_jJson);
    lc_nContinue := f_test_for_error(lc_vWhereClause);
  else
    lc_vErrorDescription := 'something went wrong in group filters parsing';
    raise spiders_error;
  end if;
  


  if lc_nContinue = 1 then
    lc_nStart := f_parseAoDataStart (in_jJson);
    lc_nContinue := f_test_for_error(lc_vSearch);
  else
    lc_vErrorDescription := 'something went wrong in whereClause parsing';
    raise spiders_error;
  end if;
  
  if lc_nContinue = 1 then
    lc_nRecordsPerPage := f_parseAoDataRecPerPage (in_jJson);
    lc_nContinue := f_test_for_error(lc_nRecordsPerPage);
  else
    lc_vErrorDescription := 'something went wrong in aoData start parsing';
    raise spiders_error;
  end if;

  if lc_nContinue = 1 then
    lc_vOrderBy := f_parseAoDataOrderBy (in_jJson);
    lc_nContinue := f_test_for_error(lc_vOrderBy);
  else
    lc_vErrorDescription := 'something went wrong in aoData records per page parsing';
    raise spiders_error;
  end if;

  if lc_nContinue = 1 then
    lc_vWhere := f_buildFullWhere (lc_vWhereClause, lc_vDynamicWhere, lc_vGroupWhere);
    lc_nContinue := f_test_for_error(lc_vWhere);
  else
    lc_vErrorDescription := 'something went wrong getting the dynamic where';
    raise spiders_error;
  end if;
  p_log(lc_vWhere);

  if lc_nContinue = 1 then
    lc_jGroupFilter := json(); -- ADD THIS IN A MINUTE f_buildGroupFilterSql (in_vTableName, lc_vWhere, in_vModule);
    --lc_nContinue := f_test_for_error(lc_jGroupFilter); !!may need to overload the function to make a test for error json
  else
    lc_vErrorDescription := 'something went wrong getting the dynamic where';
    raise spiders_error;
  end if;

  if lc_nContinue = 1 then
    lc_jDataTable := f_buildFullSql (in_vTableName, lc_vWhere, lc_vOrderBy, lc_nStart, lc_nRecordsPerPage);
    --lc_nContinue := f_test_for_error(lc_jDataTable); !!may need to overload the function to make a test for error json
  else
    lc_vErrorDescription := 'something went wrong getting the group filter sql where';
    raise spiders_error;
  end if;
  
  if lc_nContinue = 1 then
    lc_jRecordCount := f_getRecordCount (in_vTableName, lc_vWhere, lc_vOrderBy, lc_nStart, lc_nRecordsPerPage);
    --lc_nContinue := f_test_for_error(lc_nRecordCount); --!!may need to overload the function to make a test for error json
  else
    lc_vErrorDescription := 'something went wrong building the whole sql statement';
    raise spiders_error;
  end if;
  
  if lc_nContinue = 1 then
    lc_jReturn := json();
    lc_jReturn.put('groupFilter',lc_jGroupFilter);
    lc_jReturn.put('recordCount',lc_jRecordCount);
    lc_jReturn.put('dataTable',lc_jDataTable);

  else
    lc_vErrorDescription := 'something went wrong getting the record count';
    raise spiders_error;
  end if;
  
  return lc_jReturn;

exception   
  when spiders_error then
      execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' SPIDERS error ' || lc_vErrorDescription || ''')';
      return json('{"error":"' || lc_vErrorDescription || '"}');
  when json_error then
      execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' JSON error'')';
      return json('{"error":"json"}');
  when others then
      execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' dml error: ' || sqlerrm || ''')';
      return json('{"error":"' || sqlerrm || '"}');
end; 



function f_makeSpreadsheetFile (in_vSql long, in_vFolder varchar2, in_vDatatableName varchar2, in_vAO_DATA varchar2) return json as
  /* template variables */
  lc_vFunctionName varchar2(255) := 'f_makeSpreadsheetFile';

  json_error EXCEPTION;
  spiders_error EXCEPTION;
  lc_vErrorDescription varchar2(255) := 'default';

  lc_vReturn varchar2(32000) := '{"error":"default"}'; --defaul the return to e for error
  lc_nContinue number :=0; --default to 0 to dissalow continuing

  
  /* function specific variables */
  lc_vFolderid varchar2(32000) := 'SPD_1234';
  lc_nSequence number;
  lc_vFilename varchar2(32000);
  lc_vSql long;
  lc_jlDML json_list;
  lc_jResults json;
  lc_vDMLResult varchar2(255);
  lc_jMessages json;
  
  lc_sql varchar2(32000);
begin

lc_jlDML := json_list();
lc_jResults := json();
lc_jMessages := json();     
      
lc_vFilename := in_vDatatableName || '_' || v('APP_USER') || '_' || to_char(sysdate,'YYYYDDMM_HH24MI') || '.xlsx';



--lc_sql := REPLACE(in_vSql,CHR(9),' ');
--lc_sql := REPLACE(lc_sql,CHR(10),' ');
--lc_sql := REPLACE(lc_sql,CHR(11),' ');

--insert into debug (functionname, input) values ( 'f_makeSpreadsheetFile',in_vSql);

as_xlsx.query2sheet(in_vSql);
--insert into debug (functionname, input) values ( 'f_makeSpreadsheetFile','1');
as_xlsx.save(in_vFolder,lc_vFilename);
--insert into debug (functionname, input) values ( 'f_makeSpreadsheetFile','2');

--Add the file to the table
select spd_s_dt_downloads.nextval into lc_nSequence from dual;

--insert into debug (functionname, input) values ( 'f_makeSpreadsheetFile','3');
--Insert record into table
lc_vSql := '
  insert into 
    spd_t_dt_downloads (
      DTDOWNLOADID,
      DATATABLENAME,
      AODATA,
      the_bfile
    )
  
    values (
      ''' || lc_nSequence || ''',
      ''' || in_vDatatableName || ''',
      ''' || in_vAO_DATA || ''',
      BFILENAME(''' || lc_vFolderid || ''',''' || lc_vFILENAME || ''')
    )
';
--insert into debug (functionname, input) values ( 'f_makeSpreadsheetFile','4');
lc_jlDML.append(lc_vSql);
lc_jResults.put('results',lc_jlDML);
--insert into debug (functionname, input) values ( 'f_makeSpreadsheetFile','5');
lc_vDMLResult := BB_F_DML(lc_jResults, lc_jMessages);
--insert into debug (functionname, input) values ( 'f_makeSpreadsheetFile','6');
lc_jMessages.put('id',lc_nSequence);


  
  return lc_jMessages;

exception   
  when spiders_error then
      execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' SPIDERS error ' || lc_vErrorDescription || ''')';
      return json('{"error":"' || lc_vErrorDescription || '"}');
  when json_error then
      execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' JSON error'')';
      return json('{"error":"json"}');
  when others then
      execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' dml error: ' || sqlerrm || ''')';
      return json('{"error":"' || sqlerrm || '"}');
end; 
   
    
    
function f_download(in_jJson json, in_vTableName varchar2, in_vModule varchar2) return json as
  /* template variables */
  json_error EXCEPTION;
  spiders_error EXCEPTION;
  lc_vErrorDescription varchar2(255) := 'default';
  lc_vFunctionName varchar2(255) := 'f_read_data_table';
  lc_jReturn json; --defaul the return to e for error
  lc_nContinue number :=0; --default to 0 to dissalow continuing

  
  /* function specific variables */
  lc_vSql long;
  lc_jJson json;
  lc_vWhere long;
  lc_vSearch long := '';
  lc_nStart number :=1;
  lc_vGroupWhere long :='';
  lc_nRecordsPerPage number := 10;
  lc_vOrderBy long :='';
  lc_vDynamicWhere long := '';
  lc_jGroupFilter json;
  lc_jDataTable json;
  lc_jRecordCount json;
  lc_vWhereClause long;
  
begin
  lc_jReturn := json('{"error":"default"}');
  lc_jGroupFilter := json();
  lc_jDataTable := json();
  
  lc_vSearch :='';
  lc_nStart :=1;
  lc_nRecordsPerPage :=10;
  lc_vOrderBy :='1';-- REPORTID asc '; --need a default sort by
  
  lc_vGroupWhere := f_parseGroupFilterData(in_jJson, in_vTableName);
  lc_nContinue := f_test_for_error(lc_vGroupWhere);
  
  p_log(lc_vGroupWhere);
  
  --htp.p('group where '||lc_vGroupWhere);
  -- htp.p('lc_nContinue '||lc_nContinue);
  -- htp.p('in_vTableName '||in_vTableName);
   
  if lc_nContinue = 1 then
    lc_vSearch := f_parseAoDataSearch (in_jJson);
    lc_nContinue := f_test_for_error(lc_vSearch);
    if length(lc_vSearch) is not null then
      if lc_nContinue = 1 then
        lc_vDynamicWhere := f_getDynamicWhere (lc_vSearch, in_vTableName);
        lc_nContinue := f_test_for_error(lc_vDynamicWhere);
      else
        lc_vErrorDescription := 'something went wrong in aoData search parsing';
        raise spiders_error;
      end if;
    end if;
  else
    lc_vErrorDescription := 'something went wrong in group filters parsing';
    raise spiders_error;
  end if;

  if lc_nContinue = 1 then
    lc_vWhereClause := f_parseWhere (in_jJson);
    lc_nContinue := f_test_for_error(lc_vWhereClause);
  else
    lc_vErrorDescription := 'something went wrong in group filters parsing';
    raise spiders_error;
  end if;


  if lc_nContinue = 1 then
    lc_vWhere := f_buildFullWhere (lc_vWhereClause, lc_vDynamicWhere, lc_vGroupWhere);
    lc_nContinue := f_test_for_error(lc_vWhere);
  else
    lc_vErrorDescription := 'something went wrong getting the dynamic where';
    raise spiders_error;
  end if;
  p_log(lc_vWhere);

  if lc_nContinue = 1 then
    lc_jGroupFilter := json(); -- ADD THIS IN A MINUTE f_buildGroupFilterSql (in_vTableName, lc_vWhere, in_vModule);
    --lc_nContinue := f_test_for_error(lc_jGroupFilter); !!may need to overload the function to make a test for error json
  else
    lc_vErrorDescription := 'something went wrong getting the dynamic where';
    raise spiders_error;
  end if;

  if lc_nContinue = 1 then
    lc_vSql := f_buildFullSqlString (in_vTableName, lc_vWhere, lc_vOrderBy);
    --lc_nContinue := f_test_for_error(lc_jDataTable); !!may need to overload the function to make a test for error json
  else
    lc_vErrorDescription := 'something went wrong getting the group filter sql where';
    raise spiders_error;
  end if;
  
  if lc_nContinue = 1 then
    lc_jRecordCount := f_getRecordCount (in_vTableName, lc_vWhere, lc_vOrderBy, lc_nStart, lc_nRecordsPerPage);
    --lc_nContinue := f_test_for_error(lc_nRecordCount); --!!may need to overload the function to make a test for error json
  else
    lc_vErrorDescription := 'something went wrong building the whole sql statement';
    raise spiders_error;
  end if;
  
  if lc_nContinue = 1 then
    lc_jReturn := json();
    lc_jReturn.put('groupFilter',lc_jGroupFilter);
    lc_jReturn.put('recordCount',lc_jRecordCount);
    lc_jReturn.put('dataTable',lc_jDataTable);
    
--call the xlsx function
  --htp.p('sql = '||lc_vSql);
  lc_jReturn := f_makeSpreadsheetFile (lc_vSql, 'SPD_1234', in_vTableName, lc_vSearch);

  else
    lc_vErrorDescription := 'something went wrong getting the record count';
    raise spiders_error;
  end if;
  
  return lc_jReturn;

exception   
  when spiders_error then
      execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' SPIDERS error ' || lc_vErrorDescription || ''')';
      return json('{"error":"' || lc_vErrorDescription || '"}');
  when json_error then
      execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' JSON error'')';
      return json('{"error":"json"}');
  when others then
      execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' dml error: ' || sqlerrm || ''')';
      return json('{"error":"' || sqlerrm || '"}');
end; 

end;



/*template function
function f_read_data_table(in_jJson json, in_vTableName varchar2) return varchar2 as
  /* template variables * /
  lc_vFunctionName varchar2(255) := 'f_read_data_table';

  json_error EXCEPTION;
  spiders_error EXCEPTION;
  lc_vErrorDescription varchar2(255) := 'default';

  lc_vReturn varchar2(32000) := '{"error":"default"}'; --defaul the return to e for error
  lc_nContinue number :=0; --default to 0 to dissalow continuing

  
  /* function specific variables * /
  

begin

  if lc_nContinue = 1 then
    lc_vPrimaryKey := getPrimaryKeyColumn (in_jTable);
    lc_nContinue := f_test_for_error(lc_vPrimaryKey);
  else
    lc_vErrorDescription := 'name attribute not found in json';
    raise spiders_error;
  end if;
  
  return lc_vReturn;

exception   
  when spiders_error then
      execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' SPIDERS error ' || lc_vErrorDescription || ''')';
      return '{"error":"' || lc_vErrorDescription || '"}';
  when json_error then
      execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' JSON error'')';
      return '{"error":"json"}';
  when others then
      execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' dml error: ' || sqlerrm || ''')';
      return '{"error":"' || sqlerrm || '"}';
end; 
*/

/
