--------------------------------------------------------
--  DDL for Package Body PKG_SAVE_PWS_TEMPLATE
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE PACKAGE BODY "SPIDERS_DATA"."PKG_SAVE_PWS_TEMPLATE" as

function f_test_for_error (in_vReturned varchar2) return varchar2 as
  /* template variables */
  lc_vFunctionName varchar2(255) := 'f_test_for_error';
  lc_vReturn varchar2(32000) := '{"error":"default"}'; --defaul the return to e for error
  lc_nContinue number :=0; --default to 0 to dissalow continuing

  /* function specific variables */
  lc_vTest varchar2(32000) := in_vReturned;
begin

  if substr(lc_vTest, 1, 9) = '{"error":' then
    lc_vReturn := '0';
  else
    lc_vReturn := '1';
  end if;

  return lc_vReturn;

exception
  when others then
      execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' dml error: ' || sqlerrm || ''')';
      return '{"error":"' || sqlerrm || '"}';
end;


function f_save_pws_template(in_vFlowsFilesName in varchar2, o_nFileValid out number) return varchar2 as
  /* template variables */
  spiders_error EXCEPTION;
  lc_vErrorDescription varchar2(255) := 'default';
  lc_vFunctionName varchar2(255) := 'f_save_pws_template';
  lc_vReturn varchar2(32000) := '{"error":"default"}'; --defaul the return to e for error
  lc_nContinue number :=0; --default to 0 to dissalow continuing

  /* function specific variables */
  lc_vFilename varchar2(2550);
  lc_bBlob BLOB;
  lc_nStart NUMBER := 1;
  lc_nBytelen NUMBER := 32000;
  lc_nLen NUMBER;
  lc_rRaw RAW(32000);
  x NUMBER;

  l_output utl_file.file_type;
  lc_nXMLFileID number :=0;
  lc_nPWSTEMPLATEID number :=0;
  lc_vMimeType varchar2(2000);
  lc_vFileType varchar2(2000);
  
  lc_vResult varchar2(2550);

begin

select SPD_S_XML_FILES.nextval into lc_nXMLFileID from dual; 


select
    wwv_flow_files.FILENAME,
    wwv_flow_files.BLOB_CONTENT,
    dbms_lob.getlength(wwv_flow_files.BLOB_CONTENT),
    wwv_flow_files.MIME_TYPE,
    wwv_flow_files.FILE_TYPE
  into
    lc_vFilename,
    lc_bBlob,
    lc_nLen,
    lc_vMimeType,
    lc_vFileType
from
    wwv_flow_files
where
    name = in_vFlowsFilesName;
    
  
insert into spd_t_xml_files (XMLFILEID, FILENAME, BLOB_CONTENT, DOC_SIZE, MIME_TYPE, FILE_TYPE) values (lc_nXMLFILEID, lc_vFilename, lc_bBlob, lc_nLen, lc_vMimeType, lc_vFileType);
commit;



--select SPD_S_PWS_TEMPLATES.nextval into lc_nSequence from dual;
lc_nPWSTEMPLATEID := v('SPD_PWSTEMPLATEID');
update SPD_T_PWS_TEMPLATES set  XMLFILEID = lc_nXMLFILEID where PWSTEMPLATEID = lc_nPWSTEMPLATEID;
commit;



--if instr(lower(lc_vFilename),'.doc') > 0 then
--  lc_nContinue :=1;
--end if;
lc_nContinue :=1;


if lc_nContinue = 1 then
    lc_vResult := 1; 
else 
    lc_vErrorDescription := 'The document is not a docx';
    lc_vResult := 0; 
    raise spiders_error;
end if;

return lc_vResult;

exception
  when spiders_error then
      execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' SPIDERS error ' || lc_vErrorDescription || ''')';
      return '{"error":"' || lc_vErrorDescription || '"}';
  when others then
      execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' dml error: ' || sqlerrm || ''')';
      return '{"error":"' || sqlerrm || '"}';

end;

end;

/
