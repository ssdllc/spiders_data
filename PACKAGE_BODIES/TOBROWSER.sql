--------------------------------------------------------
--  DDL for Package Body TOBROWSER
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE PACKAGE BODY "SPIDERS_DATA"."TOBROWSER" AS

procedure message(IN_vStatus varchar2, IN_vMessage varchar2) AS

  obj json;

BEGIN

  obj := json(); --an empty structure
  obj.put('status', IN_vStatus);
  obj.put('message', IN_vMessage);
  obj.htp;


END message;


procedure data(IN_vSql varchar2, in_vJsonPFromRemote varchar2 default null) AS

  obj json;

BEGIN

  obj := json_dyn.executeObject(IN_vSql);
  obj.htp;


END data;


procedure data(in_jJson json, in_vJsonPFromRemote varchar2 default null) AS

  obj json;

BEGIN

  obj := in_jJson;
  obj.htp;

END data;


END tobrowser;

/
