--------------------------------------------------------
--  DDL for Package Body TESTPACKAGE
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE PACKAGE BODY "SPIDERS_DATA"."TESTPACKAGE" as 




  
procedure username

is

lc_vUsername varchar2(200);
begin

lc_vUsername := APEX_UTIL.GET_SESSION_STATE('SESSION');--APEX_UTIL.GET_SESSION_STATE('APP_USER');

--tobrowser.message('no action', lc_vUsername);
htp.p('hey there' || lc_vUsername || ' does this work');


BEGIN
    FOR c1 IN (SELECT user_name FROM wwv_flow_users) loop
        IF APEX_UTIL.GET_ACCOUNT_LOCKED_STATUS(p_user_name => c1.user_name) THEN
            HTP.P('User Account:'||c1.user_name||' is locked.'); 
        END IF;   
    END LOOP;
END;


end;

end;

/
