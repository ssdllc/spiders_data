--------------------------------------------------------
--  DDL for Package Body PKG_PROCESS_BFILE
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE PACKAGE BODY "SPIDERS_DATA"."PKG_PROCESS_BFILE" as 


function f_test_for_error (in_vReturned varchar2) return varchar2 as 
  /* template variables */
  lc_vFunctionName varchar2(255) := 'f_test_for_error';
  lc_vReturn varchar2(32000) := '{"error":"default"}'; --defaul the return to e for error
  lc_nContinue number :=0; --default to 0 to dissalow continuing
  
  /* function specific variables */
  lc_vTest varchar2(32000) := in_vReturned;
  
  
  /*EDITED 20170902 MR CREATED*/
begin

  if substr(lc_vTest, 1, 9) = '{"error":' then
    lc_vReturn := '0';
  else
    lc_vReturn := '1';
  end if;
  
  return lc_vReturn;

exception     
  when others then
      execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' dml error: ' || sqlerrm || ''')';
      return '{"error":"' || sqlerrm || '"}';
end; 


function getAttributeValue(in_vAttribute varchar2, in_jJSON json) return varchar2 as 
  /* template variables */
  json_error EXCEPTION;
  spiders_error EXCEPTION;
  lc_vErrorDescription varchar2(255) := 'default';
  lc_vFunctionName varchar2(255) := 'getAttributeValue';
  lc_vReturn varchar2(32000) := '{"error":"default"}'; --defaul the return to e for error
  lc_nContinue number :=0; --default to 0 to dissalow continuing
  
  /* function specific variables */
  lc_jvValue json_value;
  
  /*EDITED 20170902 MR CREATED*/
begin
  
  if(in_jJSON.exist(in_vAttribute)) then       
    lc_jvValue := in_jJSON.get(in_vAttribute);
    lc_vReturn := getVal(lc_jvValue);
  else
    lc_vErrorDescription := in_vAttribute || ' attribute not found in json';
    raise spiders_error;
  end if;
  
  /* get name of object */
  return lc_vReturn;

exception     
  when spiders_error then
      execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' SPIDERS error ' || lc_vErrorDescription || ''')';
      return '{"error":"' || lc_vErrorDescription || '"}';
  when json_error then
      execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' JSON error'')';
      return '{"error":"json"}';
  when others then
      execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' dml error: ' || sqlerrm || ''')';
      return '{"error":"' || sqlerrm || '"}';
end; 


function f_getJsonDataID(in_jJSON json) return number as 
  /* template variables */
  json_error EXCEPTION;
  spiders_error EXCEPTION;
  lc_vErrorDescription varchar2(255) := 'default';
  lc_vFunctionName varchar2(255) := 'f_getJsonDataRecordCount';
  lc_nReturn number := 0;--'{"error":"default"}'; --defaul the return to e for error
  lc_nContinue number :=0; --default to 0 to dissalow continuing
  
  /* function specific variables */
  lc_jvValue  json_value;
  lc_jlData   json_list;
  lc_jvRecVal json_value;
  lc_jValue   json;
begin
  
  if(in_jJson.exist('data')) then
    if(in_jJson.get('data').is_array) then
      lc_jlData := json_list(in_jJson.get('data'));        
          lc_jvRecVal := lc_jlData.get(1); --return null on outofbounds
          if (lc_jvRecVal is not null) then
            lc_jValue := json(lc_jvRecVal); 
            if(lc_jValue.exist('id')) then       
              lc_nReturn := getVal(lc_jValue.get('id'));
              lc_nContinue := 1;
            else
              lc_vErrorDescription := 'id' || ' attribute not found in json';
              raise spiders_error;
            end if;
          else 
            lc_vErrorDescription := 'Rec val is null';
            raise spiders_error;
          end if;
    else
      lc_vErrorDescription := 'Data is not an array';
      raise spiders_error;
    end if;
  else
    lc_vErrorDescription := 'Data attribute not found in json';
    raise spiders_error; 
  end if;
  
  return lc_nReturn;

exception     
  when spiders_error then
      execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' SPIDERS error ' || lc_vErrorDescription || ''')';
      return '{"error":"' || lc_vErrorDescription || '"}';
  when json_error then
      execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' JSON error'')';
      return '{"error":"json"}';
  when others then
      execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' dml error: ' || sqlerrm || ''')';
      return '{"error":"' || sqlerrm || '"}';
end; 



function f_create_directory(in_vFolderPath varchar2) return number as
  /* template variables */
  lc_vFunctionName varchar2(255) := 'f_create_directory';
  lc_nReturn number := 0; --defaul the return to e for error
  lc_nContinue number :=0; --default to 0 to dissalow continuing
  
  /* function specific variables */
  lc_nDirectoryID number := 0;
  
  /*EDITED 20170902 MR CREATED*/
begin

  select spd_s_folders.nextval into lc_nDirectoryID from dual;
  
  execute immediate 'create or replace directory ASPD_' || lc_nDirectoryID || ' AS ''' ||in_vFolderPath || '''';
  
  return lc_nDirectoryID;
  

exception     
  when others then
      execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' dml error: ' || sqlerrm || ''')';
      return '{"error":"' || sqlerrm || '"}';
end; 

function f_make_bfile(in_vFileName varchar2, in_nFolderID number, in_vFolderPath varchar2) return varchar2 as 
  /* template variables */
  lc_vFunctionName varchar2(255) := 'f_make_bfile';
  lc_vReturn varchar2(32000) := '{"error":"default"}'; --defaul the return to e for error
  lc_nContinue number :=0; --default to 0 to dissalow continuing
  
  /* function specific variables */
  --lc_vTest varchar2(32000) := in_vReturned;
  
  
  /*EDITED 20170902 MR CREATED*/
begin


  execute immediate 'insert into spd_t_bfiles (the_bfile, folderpath, filename, folderid) values (BFILENAME( ''ASPD_' || in_nFolderID || ''',''' ||  upper(in_vFileName) || '''),''' || in_vFolderPath || ''',''' || upper(in_vFileName) || ''',''' || in_nFolderID || ''')';
  
  lc_vReturn := '{"success":"TRUE"}';
  return lc_vReturn;

exception     
  when others then
      --execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' dml error: ' || sqlerrm || ''')';
      return '{"error":"' || sqlerrm || '"}';
end;  


function f_process_bfile(in_vFolderPath varchar2, in_vFileName varchar2) return varchar2 as
  /* template variables */
  lc_vFunctionName varchar2(255) := 'f_process_bfile';
  lc_vReturn varchar2(32000) := '{"error":"default"}'; --defaul the return to e for error
  lc_nContinue number :=0; --default to 0 to dissalow continuing
  
  /* function specific variables */
  lc_nDirectoryID number := 0;
  
  /*EDITED 20170902 MR CREATED*/
begin


  for rec in(
            select 
              folderid 
            from 
              spd_t_bfiles 
            where 
              folderpath = in_vFolderPath )
  loop
    --this should be only one record
    lc_nDirectoryID := rec.folderid;
  end loop;
  
  if lc_nDirectoryID = 0 then
    lc_nDirectoryID := f_create_directory(in_vFolderPath);
  end if;
  
  --make the bfile in the spd_t_bfiles table
  if lc_nDirectoryID > 0 then
    lc_vReturn := f_make_bfile(in_vFileName, lc_nDirectoryID, in_vFolderPath);
  end if;
  
  return lc_vReturn;

exception     
  when others then
      execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' dml error: ' || sqlerrm || ''')';
      return '{"error":"' || sqlerrm || '"}';
end; 

function f_process_drawings_bfiles(in_nID number) return varchar2 as
  /* template variables */
  lc_vFunctionName varchar2(255) := 'f_process_drawings_bfiles';
  lc_vReturn varchar2(32000) := '{"success":"no files to process"}'; --defaul the return to e for error
  lc_nContinue number :=0; --default to 0 to dissalow continuing
  
  /* function specific variables */
  lc_nDirectoryID number := 0;
  
  /*EDITED 20170902 MR CREATED*/
begin

/*
if there are no files we need to retun that to the calling procedure
*/

  for rec in(
              select
                xml_facility_match.dofid                                DELIVERYORDERFACILITYID,
                f_smoke_get_filename(filename)                          FILE_NAME,
                fac_book_drawing.facilityid                             FACILITYID,
                fac_book_drawing.facdrawingid                           OTHERID,
                f_smoke_get_file_path(filename)                         FILE_PATH
              from 
                spd_t_xml_facility_drawings   fac_book_drawing,
                spd_t_xml_facilities          xml_facility,
                spd_t_xml_facilities_match    xml_facility_match
              where
                xml_facility.facilityid = fac_book_drawing.facilityid and
                xml_facility.nfaid = xml_facility_match.xml_nfaid and 
                (select count(*) from all_directories where directory_path = f_smoke_get_file_path(fac_book_drawing.filename)) = 0 and
                xml_facility_match.dofid = in_nID 
              )
  loop
    lc_vReturn := f_process_bfile(rec.FILE_PATH, rec.FILE_NAME);
  end loop;
  
  return lc_vReturn;
  
exception     
  when others then
      execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' dml error: ' || sqlerrm || ''')';
      return '{"error":"' || sqlerrm || '"}';
end; 

function f_process_supfiles_bfiles(in_nID number) return varchar2 as
  /* template variables */
  lc_vFunctionName varchar2(255) := 'f_process_supfiles_bfiles';
  lc_vReturn varchar2(32000) := '{"success":"no files to process"}'; --defaul the return to e for error
  lc_nContinue number :=0; --default to 0 to dissalow continuing
  
  /* function specific variables */
  lc_nDirectoryID number := 0;
  
  /*EDITED 20170902 MR CREATED*/
begin

  for rec in(
              select
                xml_facility_match.dofid                          DELIVERYORDERFACILITYID,
                f_smoke_get_filename(filename)                    FILE_NAME,
                fac_book_sup_files.facilityid                     FACILITYID,
                fac_book_sup_files.supportingfileid               OTHERID,
                f_smoke_get_file_path(filename)                   FILE_PATH
              from 
                spd_t_xml_fac_sup_files       fac_book_sup_files,
                spd_t_xml_facilities          xml_facility,
                spd_t_xml_facilities_match    xml_facility_match
              where
                xml_facility.facilityid = fac_book_sup_files.facilityid and
                xml_facility.nfaid = xml_facility_match.xml_nfaid and 
                (select count(*) from all_directories where directory_path = f_smoke_get_file_path(fac_book_sup_files.filename)) = 0 and
                xml_facility_match.dofid = in_nID)
  loop
    lc_vReturn := f_process_bfile(rec.FILE_PATH, rec.FILE_NAME);
  end loop;
  
  return lc_vReturn;
  
exception     
  when others then
      execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' dml error: ' || sqlerrm || ''')';
      return '{"error":"' || sqlerrm || '"}';
end; 

function f_process_photos_bfiles(in_nID number) return varchar2 as
  /* template variables */
  lc_vFunctionName varchar2(255) := 'f_process_photos_bfiles';
  lc_vReturn varchar2(32000) := '{"success":"no files to process"}'; --defaul the return to e for error
  lc_nContinue number :=0; --default to 0 to dissalow continuing
  
  /* function specific variables */
  lc_nDirectoryID number := 0;
  
  /*EDITED 20170902 MR CREATED*/
begin

/*
if there are no files we need to retun that to the calling procedure
*/

  for rec in(
              select
                xml_facility_match.dofid                               DELIVERYORDERFACILITYID,
                f_smoke_get_filename(imagefilename)                    FILE_NAME,
                fac_book_photos.facilityid                             FACILITYID,
                fac_book_photos.photoid                                OTHERID,
                f_smoke_get_file_path(imagefilename)                   FILE_PATH
              from 
                spd_t_xml_facility_photos     fac_book_photos,
                spd_t_xml_facilities          xml_facility,
                spd_t_xml_facilities_match    xml_facility_match
              where
                xml_facility.facilityid = fac_book_photos.facilityid and
                xml_facility.nfaid = xml_facility_match.xml_nfaid and 
                (select count(*) from all_directories where directory_path = f_smoke_get_file_path(fac_book_photos.imagefilename)) = 0 and
                xml_facility_match.dofid = in_nID 
              )
  loop
    lc_vReturn := f_process_bfile(rec.FILE_PATH, rec.FILE_NAME);
  end loop;
  
  return lc_vReturn;
  
exception     
  when others then
      execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' dml error: ' || sqlerrm || ''')';
      return '{"error":"' || sqlerrm || '"}';
end; 

function f_process_1391_bfile(in_nID number) return varchar2 as
  /* template variables */
  lc_vFunctionName varchar2(255) := 'f_process_single_bfiles';
  lc_vReturn varchar2(32000) := '{"success":"no files to process"}'; --defaul the return to e for error
  lc_nContinue number :=0; --default to 0 to dissalow continuing
  
  /* function specific variables */
  lc_nDirectoryID number := 0;
  
  /*EDITED 20170902 MR CREATED*/
begin

/*
if there are no files we need to retun that to the calling procedure
*/

  for rec in(
              select
                xml_facility_match.dofid                               DELIVERYORDERFACILITYID,
                f_smoke_get_filename(DD1391FILENAME)                   FILE_NAME,
                xml_facility.facilityid                                FACILITYID,
                f_smoke_get_file_path(DD1391FILENAME)                  FILE_PATH
              from 
                spd_t_xml_facilities          xml_facility,
                spd_t_xml_facilities_match    xml_facility_match
              where
                xml_facility.nfaid = xml_facility_match.xml_nfaid and 
                (select count(*) from all_directories where directory_path = f_smoke_get_file_path(xml_facility.DD1391FILENAME)) = 0 and
                xml_facility_match.dofid = in_nID 
              )
  loop
    lc_vReturn := f_process_bfile(rec.FILE_PATH, rec.FILE_NAME);
  end loop;
  
  return lc_vReturn;
  
exception     
  when others then
      execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' dml error: ' || sqlerrm || ''')';
      return '{"error":"' || sqlerrm || '"}';
end; 

function f_process_ce_bfile(in_nID number) return varchar2 as
  /* template variables */
  lc_vFunctionName varchar2(255) := 'f_process_ce_bfile';
  lc_vReturn varchar2(32000) := '{"success":"no files to process"}'; --defaul the return to e for error
  lc_nContinue number :=0; --default to 0 to dissalow continuing
  
  /* function specific variables */
  lc_nDirectoryID number := 0;
  
  /*EDITED 20170902 MR CREATED*/
begin

/*
if there are no files we need to retun that to the calling procedure
*/

  for rec in(
              select
                xml_facility_match.dofid                               DELIVERYORDERFACILITYID,
                f_smoke_get_filename(COSTESTIMATEFILENAME)             FILE_NAME,
                xml_facility.facilityid                                FACILITYID,
                f_smoke_get_file_path(COSTESTIMATEFILENAME)            FILE_PATH
              from 
                spd_t_xml_facilities          xml_facility,
                spd_t_xml_facilities_match    xml_facility_match
              where
                xml_facility.nfaid = xml_facility_match.xml_nfaid and 
                (select count(*) from all_directories where directory_path = f_smoke_get_file_path(xml_facility.COSTESTIMATEFILENAME)) = 0 and
                xml_facility_match.dofid = in_nID 
              )
  loop
    lc_vReturn := f_process_bfile(rec.FILE_PATH, rec.FILE_NAME);
  end loop;
  
  return lc_vReturn;
  
exception     
  when others then
      execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' dml error: ' || sqlerrm || ''')';
      return '{"error":"' || sqlerrm || '"}';
end; 

function f_process_fs_bfile(in_nID number) return varchar2 as
  /* template variables */
  lc_vFunctionName varchar2(255) := 'f_process_ce_bfile';
  lc_vReturn varchar2(32000) := '{"success":"no files to process"}'; --defaul the return to e for error
  lc_nContinue number :=0; --default to 0 to dissalow continuing
  
  /* function specific variables */
  lc_nDirectoryID number := 0;
  
  /*EDITED 20170902 MR CREATED*/
begin

/*
if there are no files we need to retun that to the calling procedure
*/

  for rec in(
              select
                xml_facility_match.dofid                     DELIVERYORDERFACILITYID,
                f_smoke_get_filename(FSFILENAME)             FILE_NAME,
                xml_facility.facilityid                      FACILITYID,
                f_smoke_get_file_path(FSFILENAME)            FILE_PATH
              from 
                spd_t_xml_facilities          xml_facility,
                spd_t_xml_facilities_match    xml_facility_match
              where
                xml_facility.nfaid = xml_facility_match.xml_nfaid and 
                (select count(*) from all_directories where directory_path = f_smoke_get_file_path(xml_facility.FSFILENAME)) = 0 and
                xml_facility_match.dofid = in_nID 
              )
  loop
    lc_vReturn := f_process_bfile(rec.FILE_PATH, rec.FILE_NAME);
  end loop;
  
  return lc_vReturn;
  
exception     
  when others then
      execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' dml error: ' || sqlerrm || ''')';
      return '{"error":"' || sqlerrm || '"}';
end; 

function f_process_drawings(in_cJSON clob) return json as 
  /* template variables */
  json_error EXCEPTION;
  spiders_error EXCEPTION;
  lc_vErrorDescription varchar2(255) := 'default';
  lc_vFunctionName varchar2(255) := 'f_process_drawings';
  lc_vRest varchar2(255) := 'f_delete_af_temp_data';
  lc_jReturn json; 
  lc_nContinue number :=0; --default to 0 to dissalow continuing
  
  /* function specific variables */
  lc_vModule varchar2(255);
  lc_jParameterJson json;
  lc_nID number;
  lc_nRecordCount number := 0;
  lc_jlDML json_list;
  lc_jResults json;
  lc_jMessages json;
  lc_vDMLResult varchar2(255);
  lc_vSql long;
  
  lc_jJson json;
  
  lc_jIndividual json;
  lc_jvValue json_value;
  lc_jlData json_list;
  lc_jvRecVal json_value;
  lc_jValue json;
  
  /*EDITED 20170902 MR CREATED*/
begin
  
  lc_jJson := json(in_cJson);
  lc_nID := f_getJsonDataID(lc_jJson);
  lc_nContinue := 1;--hardcoded for now
  
  if lc_nContinue = 1 then
    lc_jReturn := json(f_process_drawings_bfiles(lc_nID));
  end if;
  
  commit;
    
  return lc_jReturn;
  

exception     
  when spiders_error then
      execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' SPIDERS error ' || lc_vErrorDescription || ''')';
      return json('{"error":"' || lc_vFunctionName || ' SPIDERS error ' || lc_vErrorDescription || '"}');
  when json_error then
      execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' JSON error'')';
      return json('{"error":"' || lc_vFunctionName || ' JSON error ' || lc_vErrorDescription || '"}');
  when others then
      execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' dml error: ' || sqlerrm || ''')';
     return json('{"error":"' || lc_vFunctionName || ' dml error ' || sqlerrm || '"}');
end; 






function f_process_photos(in_cJSON clob) return json as 
  /* template variables */
  json_error EXCEPTION;
  spiders_error EXCEPTION;
  lc_vErrorDescription varchar2(255) := 'default';
  lc_vFunctionName varchar2(255) := 'f_process_drawings';
  lc_vRest varchar2(255) := 'f_delete_af_temp_data';
  lc_jReturn json; 
  lc_nContinue number :=0; --default to 0 to dissalow continuing
  
  /* function specific variables */
  lc_vModule varchar2(255);
  lc_jParameterJson json;
  lc_nID number;
  lc_jlDML json_list;
  lc_jResults json;
  lc_jMessages json;
  lc_vDMLResult varchar2(255);
  lc_vSql long;
  
  lc_jJson json;
  
  lc_jvValue json_value;
  lc_jlData json_list;
  lc_jvRecVal json_value;
  lc_jValue json;
  
  /*EDITED 20170902 MR CREATED*/
begin
  lc_jJson := json(in_cJson);
  lc_nID := f_getJsonDataID(lc_jJson);
  lc_nContinue := 1;--hardcoded for now
  
  if lc_nContinue = 1 then
    lc_jReturn := json(f_process_photos_bfiles(lc_nID));
  end if;
  
  commit;
    
  return lc_jReturn;
  

exception     
  when spiders_error then
      execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' SPIDERS error ' || lc_vErrorDescription || ''')';
      return json('{"error":"' || lc_vFunctionName || ' SPIDERS error ' || lc_vErrorDescription || '"}');
  when json_error then
      execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' JSON error'')';
      return json('{"error":"' || lc_vFunctionName || ' JSON error ' || lc_vErrorDescription || '"}');
  when others then
      execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' dml error: ' || sqlerrm || ''')';
     return json('{"error":"' || lc_vFunctionName || ' dml error ' || sqlerrm || '"}');
end; 


--f_process_supporting_files suggest this change
function f_process_supfiles(in_cJSON clob) return json as 
  /* template variables */
  json_error EXCEPTION;
  spiders_error EXCEPTION;
  lc_vErrorDescription varchar2(255) := 'default';
  lc_vFunctionName varchar2(255) := 'f_process_drawings';
  lc_vRest varchar2(255) := 'f_delete_af_temp_data';
  lc_jReturn json; 
  lc_nContinue number :=0; --default to 0 to dissalow continuing
  
  /* function specific variables */
  lc_vModule varchar2(255);
  lc_jParameterJson json;
  lc_nID number;
  lc_jlDML json_list;
  lc_jResults json;
  lc_jMessages json;
  lc_vDMLResult varchar2(255);
  lc_vSql long;
  
  lc_jJson json;
  
  lc_jvValue json_value;
  lc_jlData json_list;
  lc_jvRecVal json_value;
  lc_jValue json;
  
  /*EDITED 20170902 MR CREATED*/
begin

  lc_jJson := json(in_cJson);
  lc_nID := f_getJsonDataID(lc_jJson);
  lc_nContinue := 1;--hardcoded for now
  
  if lc_nContinue = 1 then
    lc_jReturn := json(f_process_supfiles_bfiles(lc_nID));
  end if;
  
  commit;
  
  
  return lc_jReturn;
  

exception     
  when spiders_error then
      execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' SPIDERS error ' || lc_vErrorDescription || ''')';
      return json('{"error":"' || lc_vFunctionName || ' SPIDERS error ' || lc_vErrorDescription || '"}');
  when json_error then
      execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' JSON error'')';
      return json('{"error":"' || lc_vFunctionName || ' JSON error ' || lc_vErrorDescription || '"}');
  when others then
      execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' dml error: ' || sqlerrm || ''')';
     return json('{"error":"' || lc_vFunctionName || ' dml error ' || sqlerrm || '"}');
end; 

function f_process_1391(in_cJSON clob) return json as 
  /* template variables */
  json_error EXCEPTION;
  spiders_error EXCEPTION;
  lc_vErrorDescription varchar2(255) := 'default';
  lc_vFunctionName varchar2(255) := 'f_process_1391';
  lc_vRest varchar2(255) := 'f_delete_af_temp_data';
  lc_jReturn json; 
  lc_nContinue number :=0; --default to 0 to dissalow continuing
  
  /* function specific variables */
  lc_vModule varchar2(255);
  lc_jParameterJson json;
  lc_nID number;
  lc_jlDML json_list;
  lc_jResults json;
  lc_jMessages json;
  lc_vDMLResult varchar2(255);
  lc_vSql long;
  
  lc_jJson json;
  
  lc_jvValue json_value;
  lc_jlData json_list;
  lc_jvRecVal json_value;
  lc_jValue json;
  
  /*EDITED 20170902 MR CREATED*/
begin
  lc_jJson := json(in_cJson);
  lc_nID := f_getJsonDataID(lc_jJson);
  lc_nContinue := 1;--hardcoded for now
  
  if lc_nContinue = 1 then
    lc_jReturn := json(f_process_1391_bfile(lc_nID));
  end if;
  
  commit;
    
  return lc_jReturn;
  

exception     
  when spiders_error then
      execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' SPIDERS error ' || lc_vErrorDescription || ''')';
      return json('{"error":"' || lc_vFunctionName || ' SPIDERS error ' || lc_vErrorDescription || '"}');
  when json_error then
      execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' JSON error'')';
      return json('{"error":"' || lc_vFunctionName || ' JSON error ' || lc_vErrorDescription || '"}');
  when others then
      execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' dml error: ' || sqlerrm || ''')';
     return json('{"error":"' || lc_vFunctionName || ' dml error ' || sqlerrm || '"}');
end;

function f_process_ce(in_cJSON clob) return json as 
  /* template variables */
  json_error EXCEPTION;
  spiders_error EXCEPTION;
  lc_vErrorDescription varchar2(255) := 'default';
  lc_vFunctionName varchar2(255) := 'f_process_ce';
  lc_vRest varchar2(255) := 'f_delete_af_temp_data';
  lc_jReturn json; 
  lc_nContinue number :=0; --default to 0 to dissalow continuing
  
  /* function specific variables */
  lc_vModule varchar2(255);
  lc_jParameterJson json;
  lc_nID number;
  lc_jlDML json_list;
  lc_jResults json;
  lc_jMessages json;
  lc_vDMLResult varchar2(255);
  lc_vSql long;
  
  lc_jJson json;
  
  lc_jvValue json_value;
  lc_jlData json_list;
  lc_jvRecVal json_value;
  lc_jValue json;
  
  /*EDITED 20170902 MR CREATED*/
begin
  lc_jJson := json(in_cJson);
  lc_nID := f_getJsonDataID(lc_jJson);
  lc_nContinue := 1;--hardcoded for now
  
  if lc_nContinue = 1 then
    lc_jReturn := json(f_process_ce_bfile(lc_nID));
  end if;
  
  commit;
    
  return lc_jReturn;
  

exception     
  when spiders_error then
      execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' SPIDERS error ' || lc_vErrorDescription || ''')';
      return json('{"error":"' || lc_vFunctionName || ' SPIDERS error ' || lc_vErrorDescription || '"}');
  when json_error then
      execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' JSON error'')';
      return json('{"error":"' || lc_vFunctionName || ' JSON error ' || lc_vErrorDescription || '"}');
  when others then
      execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' dml error: ' || sqlerrm || ''')';
     return json('{"error":"' || lc_vFunctionName || ' dml error ' || sqlerrm || '"}');
end;

function f_process_fs(in_cJSON clob) return json as 
  /* template variables */
  json_error EXCEPTION;
  spiders_error EXCEPTION;
  lc_vErrorDescription varchar2(255) := 'default';
  lc_vFunctionName varchar2(255) := 'f_process_fs';
  lc_vRest varchar2(255) := 'f_delete_af_temp_data';
  lc_jReturn json; 
  lc_nContinue number :=0; --default to 0 to dissalow continuing
  
  /* function specific variables */
  lc_vModule varchar2(255);
  lc_jParameterJson json;
  lc_nID number;
  lc_jlDML json_list;
  lc_jResults json;
  lc_jMessages json;
  lc_vDMLResult varchar2(255);
  lc_vSql long;
  
  lc_jJson json;
  
  lc_jvValue json_value;
  lc_jlData json_list;
  lc_jvRecVal json_value;
  lc_jValue json;
  
  /*EDITED 20170902 MR CREATED*/
begin
  lc_jJson := json(in_cJson);
  lc_nID := f_getJsonDataID(lc_jJson);
  lc_nContinue := 1;--hardcoded for now
  
  if lc_nContinue = 1 then
    lc_jReturn := json(f_process_fs_bfile(lc_nID));
  end if;
  
  commit;
    
  return lc_jReturn;
  

exception     
  when spiders_error then
      execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' SPIDERS error ' || lc_vErrorDescription || ''')';
      return json('{"error":"' || lc_vFunctionName || ' SPIDERS error ' || lc_vErrorDescription || '"}');
  when json_error then
      execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' JSON error'')';
      return json('{"error":"' || lc_vFunctionName || ' JSON error ' || lc_vErrorDescription || '"}');
  when others then
      execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' dml error: ' || sqlerrm || ''')';
     return json('{"error":"' || lc_vFunctionName || ' dml error ' || sqlerrm || '"}');
end;
/*
function f_process_supporting_files(in_cJson clob) return json;
function f_process_photos(in_cJson clob) return json;
function f_process_dd_1391(in_cJson clob) return json;
function f_process_cost_estimate(in_cJson clob) return json;
*/

end;

/
