--------------------------------------------------------
--  DDL for Package Body UT_GENERATOR
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE PACKAGE BODY "SPIDERS_DATA"."UT_GENERATOR" AS


	-- This PLSQL bloc is going to generate testing code to test
   PROCEDURE generateTests IS
		lc_vExecuteImmediateSql long := '';
	pkg_name                varchar2(225) := '';
	proc_name               varchar2(225) := '';
	lc_vLocalName           long := '';
	lc_vDeclareVariables    long := '';
	lc_vBeginVariables      long := '';
	lc_vDeclareBegin        long := '';
	lc_vParameterSection    long := '';
	lc_vExecuteImmediateIn  long := '';
	lc_vUsing               long := '';
	lc_vSetup               clob;
	lc_vTest                long := '';
	lc_vExecuteImmediate    long := '';
	lc_vCustomSetup         long := '';
	lc_vCustomTeardown      long := '';

BEGIN

	htp.p('delete from SPD_T_UNIT_TEST_RESULTS;');
	htp.p('-- Reset Pass Fail values');
	htp.p('ut_common.resetTest();');

	-- Looping UNIT_TESTS
	for rec in (
	select
	  unit_test.UNITTESTID                            UNITTESTID,
	  unit_test.PACKAGE_NAME                          PACKAGE_NAME,
	  unit_test.PROCEDURE_NAME                        PROCEDURE_NAME,
	  unit_test.TEST_NAME                             TEST_NAME,
	  unit_test.TEST_DESCRIPTION                      TEST_DESCRIPTION,
	  replace(replace(unit_test.CUSTOM_SETUP, chr(9), ''), chr(10), '')      CUSTOM_SETUP,
	  replace(replace(unit_test.CUSTOM_TEARDOWN, chr(9), ''), chr(10), '')      CUSTOM_TEARDOWN,
	  unit_test.RETURN_TYPE                           RETURN_TYPE,
	  unit_test.ASSERT_VALUE_TYPE                     ASSERT_VALUE_TYPE,
	  unit_test.ASSERT_TEST                           ASSERT_TEST,
	  unit_test.ASSERT_VALUE_VARCHAR                  ASSERT_VALUE_VARCHAR,
	  unit_test.ASSERT_VALUE_NUMBER                   ASSERT_VALUE_NUMBER,
	  unit_test.ASSERT_VALUE_DATE                     ASSERT_VALUE_DATE,
	  unit_test.ASSERT_VALUE_JSON                     ASSERT_VALUE_JSON,
	  unit_test.ASSERT_VALUE_JSON_LIST                ASSERT_VALUE_JSON_LIST,
	  unit_test.ASSERT_VALUE_JSON_VALUE               ASSERT_VALUE_JSON_VALUE,
	  unit_test.ASSERT_VALUE_CLOB                     ASSERT_VALUE_CLOB,
	  unit_test.ASSERT_VALUE_BLOB                     ASSERT_VALUE_BLOB,
	  unit_test.ASSERT_VALUE_XML                      ASSERT_VALUE_XML
	from
	  spd_t_unit_tests            unit_test
	where
	  run_me = 1
	order by
	  unit_test.package_name,
	  unit_test.procedure_name
	) loop


			pkg_name := rec.package_name;
			proc_name := rec.procedure_name;
			lc_vLocalName := '';
			lc_vDeclareVariables := 'DECLARE '||chr(13);
			lc_vBeginVariables := '';
			lc_vDeclareBegin := '';
			lc_vParameterSection := '';
			lc_vExecuteImmediateIn := '';
			lc_vUsing := '';
			lc_vSetup := '';
			lc_vExecuteImmediateSql := '';
			lc_vExecuteImmediate := '';
	  

			htp.p('------------------------------------------------------------------------------------------------');
			htp.p(chr(9)||chr(9)||chr(9)||chr(9)||chr(9)||chr(9)|| chr(9)||chr(9)||chr(9)||chr(9)||chr(9)||chr(9)||chr(9)|| '-- ' ||pkg_name || '.' ||proc_name || ' --');
			htp.p('------------------------------------------------------------------------------------------------');

			-- variable declaration and instantiation for tests
		
			case
			  when rec.RETURN_TYPE = 'VARCHAR2' then
				lc_vDeclareVariables := lc_vDeclareVariables || chr(9)||'lc_vSql      long;'||chr(13)
				||chr(9)||'lc_Return    long;'||chr(13)
				||chr(9)||'lc_vAssert   long;';

				lc_vBeginVariables := 'begin'||chr(13)
				  --setup
				||chr(9)||'lc_vAssert := '''';';
			  else
				lc_vDeclareVariables := lc_vDeclareVariables
				||chr(9)||'lc_vSql long;'||chr(13)
				||chr(9)||'lc_Return ' || rec.return_type || ';'||chr(13)
				||chr(9)||'lc_vAssert   long;'||chr(13)
				||chr(9)||'lc_nAssert   number;'||chr(13)
				||chr(9)||'lc_dAssert   date;'||chr(13)
				||chr(9)||'lc_jAssert   json;'||chr(13)
				||chr(9)||'lc_jlAssert  json_list;'||chr(13)
				||chr(9)||'lc_jvAssert  json_value;'||chr(13)
				||chr(9)||'lc_cAssert   clob;'||chr(13);

				lc_vBeginVariables := 'BEGIN'||chr(13)
				||chr(9)||'lc_vAssert := '''';'||chr(13)
				||chr(9)||'lc_nAssert := -1;'||chr(13)
				||chr(9)||'lc_dAssert :=to_date(''01-JAN-0001'');'||chr(13)
				||chr(9)||'lc_jAssert := json();'||chr(13)
				||chr(9)||'lc_jlAssert:= json_list();'||chr(13)
				||chr(9)||'lc_jvAssert:= json_value();'||chr(13)
				||chr(9)||'lc_cAssert := '''';'||chr(13);
			end case;
	
			-- CUSTOM SETUP
			if(rec.custom_setup is not null) then
				lc_vDeclareVariables :=  lc_vDeclareVariables || chr(9) || 'lc_jlSetupDML json_list;'||chr(13);
	
				lc_vCustomSetup := '-- Custom Setup' || chr(13);
				lc_vCustomSetup := lc_vCustomSetup || chr(9)||'lc_jlSetupDML := json_list('''|| '[' || rec.custom_setup|| ']' ||''');'||chr(13)||'executeSQL(lc_jlSetupDML);'||chr(13)||chr(13); 
      else
        lc_vCustomSetup := '';
			end if;

	
			-- CUSTOM TEARDOWN 
      
			if(rec.custom_teardown is not null) then
				lc_vDeclareVariables :=  lc_vDeclareVariables || chr(9) || 'lc_jlTearDownDML json_list;'||chr(13);
	
				lc_vCustomTeardown := '-- Custom Tear Down' || chr(13);
				lc_vCustomTeardown := lc_vCustomTeardown || chr(9)||'lc_jlTearDownDML := json_list('''|| '[' || rec.custom_teardown|| ']' ||''');'||chr(13)|| chr(9) ||'executeSQL(lc_jlTearDownDML);'||chr(13)||chr(13);
      else
        lc_vCustomTeardown := '';
			end if; 
		

			-- Looping UNIT_TEST_PARAMETERS per UNIT_TESTS
			for rec2 in (
			select
			  unit_test_parameter.PARAMETER_SET               PARAMETER_SET,
			  unit_test_parameter.PARAMETER_ORDER             PARAMETER_ORDER,
			  unit_test_parameter.PARAMETER_NAME              PARAMETER_NAME,
			  unit_test_parameter.PARAMETER_TYPE              PARAMETER_TYPE,
			  unit_test_parameter.PARAMETER_VALUE_VARCHAR     PARAMETER_VALUE_VARCHAR,
			  unit_test_parameter.PARAMETER_VALUE_NUMBER      PARAMETER_VALUE_NUMBER,
			  unit_test_parameter.PARAMETER_VALUE_DATE        PARAMETER_VALUE_DATE,
			  replace(replace(unit_test_parameter.PARAMETER_VALUE_JSON, chr(9), ''), chr(13), '')      PARAMETER_VALUE_JSON,
			  unit_test_parameter.PARAMETER_VALUE_JSON_LIST   PARAMETER_VALUE_JSON_LIST,
			  unit_test_parameter.PARAMETER_VALUE_JSON_VALUE  PARAMETER_VALUE_JSON_VALUE,
			  unit_test_parameter.PARAMETER_VALUE_CLOB        PARAMETER_VALUE_CLOB,
			  unit_test_parameter.PARAMETER_VALUE_BLOB        PARAMETER_VALUE_BLOB,
			  unit_test_parameter.PARAMETER_VALUE_XML         PARAMETER_VALUE_XML
			from
			  spd_t_unit_test_parameters  unit_test_parameter
			where
			  unit_test_parameter.unittestid = rec.UNITTESTID
			  and parameter_name is not null
			order by
			  unit_test_parameter.parameter_set
			)
			loop


				lc_vLocalName := replace(lower(rec2.PARAMETER_NAME),'in_','lc_');
				case
				  when rec2.PARAMETER_TYPE = 'VARCHAR2' then
					lc_vDeclareVariables := lc_vDeclareVariables  || chr(9) || lc_vLocalName || ' ' || rec2.PARAMETER_TYPE || '(' || lengthb(rec2.PARAMETER_VALUE_VARCHAR)|| ')' ||';' ||chr(13);
				  else
					lc_vDeclareVariables := lc_vDeclareVariables  || chr(9) || lc_vLocalName || ' ' || rec2.PARAMETER_TYPE || ';' ||chr(13) ; -- add each parameter name to the list of declared variables
				end case;
	
				lc_vDeclareBegin := CONCAT(lc_vDeclareVariables || chr(13), lc_vBeginVariables);
				lc_vUsing := lc_vUsing || ':' || lc_vLocalName || ',';
				lc_vParameterSection := lc_vParameterSection || rec2.PARAMETER_NAME || ' => ' || ':' || lc_vLocalName || ',';
				lc_vExecuteImmediateIn := lc_vExecuteImmediateIn || lc_vLocalName || ',';

				case rec2.PARAMETER_TYPE
				   WHEN 'VARCHAR2'    THEN lc_vSetup := lc_vSetup || chr(9) || lc_vLocalName || ':= ''' || rec2.PARAMETER_VALUE_VARCHAR || ''';' || chr(13);
				   WHEN 'NUMBER'      THEN lc_vSetup := lc_vSetup || chr(9) || lc_vLocalName || ':= ' || rec2.PARAMETER_VALUE_NUMBER ||';' || chr(13);
				   WHEN 'DATE'        THEN lc_vSetup := lc_vSetup || chr(9) || lc_vLocalName || ':= to_date(''' || rec2.PARAMETER_VALUE_DATE ||''',''DD-MON-YYYY'');' || chr(13);
				   WHEN 'JSON'        THEN lc_vSetup := lc_vSetup || chr(9) || lc_vLocalName || ':= json(''' || rec2.PARAMETER_VALUE_JSON  ||''');' || chr(13);
				   WHEN 'JSON_LIST'   THEN lc_vSetup := lc_vSetup || chr(9) || lc_vLocalName || ':= json_list(''' || rec2.PARAMETER_VALUE_JSON_LIST ||''');' || chr(13);
				   WHEN 'JSON_VALUE'  THEN lc_vSetup := lc_vSetup || chr(9) || lc_vLocalName || ':= json_value(''' || rec2.PARAMETER_VALUE_JSON_VALUE ||''');' || chr(13);
				   WHEN 'CLOB'        THEN lc_vSetup := lc_vSetup || chr(9) || lc_vLocalName || ':= ''' || rec2.PARAMETER_VALUE_CLOB ||''';' || chr(13);
				   /*!!!NOTE: WHEN BLOB OR XML, FOR NOW MUST USE CUSTOM SETUP*/
				   --WHEN 'BLOB'        THEN lc_vSetup := lc_vSetup || lc_vLocalName || ':= ' empty_blob();--util_raw.cast_to_raw('changeme'); || chr(13);
				   --WHEN 'XMLTYPE'     THEN lc_vSetup := lc_vSetup || lc_vLocalName || ':= xmltype(''' || rec2.PARAMETER_VALUE_XML || ''');' || chr(13);
				   else
					htp.p('The UNIT_TEST_PARAMETERS parameter type is unknown');
				end case;

			end loop;

			htp.p(lc_vDeclareBegin);
			htp.p('-- Set Up');
			htp.p(lc_vSetup);
			htp.p(lc_vCustomSetup);



			lc_vParameterSection := SUBSTR(lc_vParameterSection,1,length(lc_vParameterSection)-1);
			lc_vUsing := SUBSTR(lc_vUsing,1,length(lc_vUsing)-1);
			lc_vExecuteImmediateIn := SUBSTR(lc_vExecuteImmediateIn,1,length(lc_vExecuteImmediateIn)-1);

			lc_vExecuteImmediateSql := 'lc_vSql := ''begin :x := ' || rec.package_name || '.' || rec.procedure_name || '(' || lc_vParameterSection || '); end;'''||';';
			--lc_vExecuteImmediateSql := CONCAT(lc_vExecuteImmediateSql, ';');
			htp.p('-- Execute function / procedure');
			htp.p(chr(9)||lc_vExecuteImmediateSql || chr(13));
			lc_vExecuteImmediate := 'execute immediate lc_vSql using out lc_Return, ' || lc_vExecuteImmediateIn || ';';
			htp.p(chr(9)||lc_vExecuteImmediate || chr(13) || chr(13));
			htp.p('-- Run TEST');



			case rec.RETURN_TYPE
			   WHEN 'VARCHAR2'    THEN lc_vTest := 'lc_vAssert :=''' || rec.assert_value_varchar || ''';' || chr(13) || chr(9)|| 'ut_common.' || rec.ASSERT_TEST || '(lc_Return,lc_vAssert);';
			   WHEN 'NUMBER'      THEN lc_vTest := 'lc_nAssert :=' || rec.assert_value_number || ';' || chr(13) || chr(9)||'ut_common.' || rec.ASSERT_TEST || '(lc_Return,lc_nAssert);';
			   WHEN 'DATE'        THEN lc_vTest := 'lc_dAssert :=' || rec.assert_value_date || ';' || chr(13) || chr(9)||'ut_common.' || rec.ASSERT_TEST || '(lc_Return,lc_dAssert);';
			   WHEN 'JSON'        THEN lc_vTest := 'lc_jAssert := json(''' || rec.assert_value_json || ''');' || chr(13) || chr(9)||'ut_common.' || rec.ASSERT_TEST || '(lc_Return,lc_jAssert);';
			   WHEN 'JSON_LIST'   THEN lc_vTest := 'lc_jlAssert := json_list(''' || rec.assert_value_json_list || ''');' || chr(13) || chr(9)||'ut_common.' || rec.ASSERT_TEST || '(lc_Return,lc_jlAssert);';
			   WHEN 'JSON_VALUE'  THEN lc_vTest := 'lc_jvAssert := json_value(''' || rec.assert_value_json_value || ''');' || chr(13) || chr(9)||'ut_common.' || rec.ASSERT_TEST || '(lc_Return,lc_jvAssert);';
			   WHEN 'CLOB'        THEN lc_vTest := 'lc_cAssert :=''' || rec.assert_value_clob || ''';' || chr(13) || chr(9)|| 'ut_common.' || rec.ASSERT_TEST || '(lc_Return,lc_cAssert);';
			   /*!!!NOTE: WHEN BLOB OR XML, FOR NOW MUST USE CUSTOM SETUP*/
			   --WHEN 'BLOB'        THEN htp.p('lc_bAssert :=' || rec.assert_value_blob || ';' || chr(13) || rec.ASSERT_TEST || '(lc_Return,lc_bAssert);');
			   --WHEN 'XMLTYPE'     THEN htp.p('lc_xAssert :=' || rec.assert_value_xml || ';' || chr(13) || rec.ASSERT_TEST || '(lc_Return,lc_xAssert);');
			   else
				htp.p('The UNIT_TEST return type is unknown');
			end case;

			htp.p(chr(9)||lc_vTest|| chr(13)||chr(13));
			htp.p(lc_vCustomTeardown);
			htp.p(chr(9)||'ut_common.pass(' || rec.UNITTESTID || ',' || '''' || rec.PACKAGE_NAME || ''''|| ',' || '''' || rec.PROCEDURE_NAME || '''' || ',' || '''' || rec.TEST_NAME || '''' || ');');
			htp.p(chr(9)||'exception when others then '||chr(13)||chr(9)||'ut_common.fail(' || rec.UNITTESTID || ',' || '''' || rec.PACKAGE_NAME || ''''|| ',' || '''' || rec.PROCEDURE_NAME || '''' || ',' || '''' || rec.TEST_NAME || '''' || ');'||chr(13)||lc_vCustomTeardown);
			htp.p('END;');
			htp.p('/'|| chr(13) || chr(13));

		end loop;

		htp.p('BEGIN');
		htp.p('ut_common.postResults();');
		htp.p('END;');

		exception when others then htp.p('sqlerrm:' || sqlerrm);
	END generateTests;
	
	
	
	

	
	
	
	PROCEDURE runTests AS
	
	
	lc_vExecuteImmediateSql long := '';
	pkg_name                varchar2(225) := '';
	proc_name               varchar2(225) := '';
	lc_vLocalName           long := '';
	lc_vDeclareVariables    long := '';
	lc_vBeginVariables      long := '';
	lc_vDeclareBegin        long := '';
	lc_vParameterSection    long := '';
	lc_vExecuteImmediateIn  long := '';
	lc_vUsing               long := '';
	lc_vSetup               clob;
	lc_vTest                long := '';
	lc_vExecuteImmediate    long := '';
	lc_vCustomSetup         long := '';
	lc_vCustomTeardown      long := '';
	lc_lTestBloc            long := '';
	lc_lfirstBloc           long := '';
	lc_llastBloc            long := '';

BEGIN

	makePackageFunctionPublic();

	lc_lfirstBloc := lc_lfirstBloc || 'begin' || chr(13) || 'delete from SPD_T_UNIT_TEST_RESULTS;' || chr(13);
	lc_lfirstBloc := lc_lfirstBloc || 'ut_common.resetTest();' || chr(13) || 'end;' || chr(13) || chr(13);
	execute immediate lc_lfirstBloc;

	-- Looping UNIT_TESTS
	for rec in (
	select
	  unit_test.UNITTESTID                            UNITTESTID,
	  unit_test.PACKAGE_NAME                          PACKAGE_NAME,
	  unit_test.PROCEDURE_NAME                        PROCEDURE_NAME,
	  unit_test.TEST_NAME                             TEST_NAME,
	  unit_test.TEST_DESCRIPTION                      TEST_DESCRIPTION,
	  replace(replace(unit_test.CUSTOM_SETUP, chr(9), ''), chr(10), '')      CUSTOM_SETUP,
	  replace(replace(unit_test.CUSTOM_TEARDOWN, chr(9), ''), chr(10), '')      CUSTOM_TEARDOWN,
	  unit_test.RETURN_TYPE                           RETURN_TYPE,
	  unit_test.ASSERT_VALUE_TYPE                     ASSERT_VALUE_TYPE,
	  unit_test.ASSERT_TEST                           ASSERT_TEST,
	  unit_test.ASSERT_VALUE_VARCHAR                  ASSERT_VALUE_VARCHAR,
	  unit_test.ASSERT_VALUE_NUMBER                   ASSERT_VALUE_NUMBER,
	  unit_test.ASSERT_VALUE_DATE                     ASSERT_VALUE_DATE,
	  unit_test.ASSERT_VALUE_JSON                     ASSERT_VALUE_JSON,
	  unit_test.ASSERT_VALUE_JSON_LIST                ASSERT_VALUE_JSON_LIST,
	  unit_test.ASSERT_VALUE_JSON_VALUE               ASSERT_VALUE_JSON_VALUE,
	  unit_test.ASSERT_VALUE_CLOB                     ASSERT_VALUE_CLOB,
	  unit_test.ASSERT_VALUE_BLOB                     ASSERT_VALUE_BLOB,
	  unit_test.ASSERT_VALUE_XML                      ASSERT_VALUE_XML
	from
	  spd_t_unit_tests            unit_test
	where
	  run_me = 1
	order by
	  unit_test.package_name,
	  unit_test.procedure_name
	) loop


			pkg_name := rec.package_name;
			proc_name := rec.procedure_name;
			lc_vLocalName := '';
			lc_vDeclareVariables := 'DECLARE '||chr(13);
			lc_vBeginVariables := '';
			lc_vDeclareBegin := '';
			lc_vParameterSection := '';
			lc_vExecuteImmediateIn := '';
			lc_vUsing := '';
			lc_vSetup := '';
			lc_vExecuteImmediateSql := '';
			lc_vExecuteImmediate := '';

		
			case
			  when rec.RETURN_TYPE = 'VARCHAR2' then
				lc_vDeclareVariables := lc_vDeclareVariables || chr(9)||'lc_vSql      long;'||chr(13)
				||chr(9)||'lc_Return    long;'||chr(13)
				||chr(9)||'lc_vAssert   long;';

				lc_vBeginVariables := 'begin'||chr(13)
				  --setup
				||chr(9)||'lc_vAssert := '''';';
			  else
				lc_vDeclareVariables := lc_vDeclareVariables
				||chr(9)||'lc_vSql long;'||chr(13)
				||chr(9)||'lc_Return ' || rec.return_type || ';'||chr(13)
				||chr(9)||'lc_vAssert   long;'||chr(13)
				||chr(9)||'lc_nAssert   number;'||chr(13)
				||chr(9)||'lc_dAssert   date;'||chr(13)
				||chr(9)||'lc_jAssert   json;'||chr(13)
				||chr(9)||'lc_jlAssert  json_list;'||chr(13)
				||chr(9)||'lc_jvAssert  json_value;'||chr(13)
				||chr(9)||'lc_cAssert   clob;'||chr(13);

				lc_vBeginVariables := 'BEGIN'||chr(13)
				||chr(9)||'lc_vAssert := '''';'||chr(13)
				||chr(9)||'lc_nAssert := -1;'||chr(13)
				||chr(9)||'lc_dAssert :=to_date(''01-JAN-0001'');'||chr(13)
				||chr(9)||'lc_jAssert := json();'||chr(13)
				||chr(9)||'lc_jlAssert:= json_list();'||chr(13)
				||chr(9)||'lc_jvAssert:= json_value();'||chr(13)
				||chr(9)||'lc_cAssert := '''';'||chr(13);
			end case;
	
      
			-- CUSTOM SETUP
    
			if(rec.custom_setup is not null) then
				lc_vDeclareVariables :=  lc_vDeclareVariables || chr(9) || 'lc_jlSetupDML json_list;'||chr(13);
				lc_vCustomSetup := lc_vCustomSetup || chr(9)||'lc_jlSetupDML := json_list('''|| '[' || rec.custom_setup|| ']' ||''');'||chr(13)||'executeSQL(lc_jlSetupDML);'||chr(13)|| 'commit;' || chr(13); 
      else
        lc_vCustomSetup := '';
			end if; 

      
			-- CUSTOM TEARDOWN
			if(rec.custom_teardown is not null) then
				lc_vDeclareVariables :=  lc_vDeclareVariables || chr(9) || 'lc_jlTearDownDML json_list;'||chr(13);
				lc_vCustomTeardown := lc_vCustomTeardown || chr(9)||'lc_jlTearDownDML := json_list('''|| '[' || rec.custom_teardown|| ']' ||''');'||chr(13)|| chr(9) ||'executeSQL(lc_jlTearDownDML);'||chr(13)|| 'commit;' || chr(13);    
			else
        lc_vCustomTeardown := '';
      end if; 
		

			-- Looping UNIT_TEST_PARAMETERS per UNIT_TESTS
			for rec2 in (
			select
			  unit_test_parameter.PARAMETER_SET               PARAMETER_SET,
			  unit_test_parameter.PARAMETER_ORDER             PARAMETER_ORDER,
			  unit_test_parameter.PARAMETER_NAME              PARAMETER_NAME,
			  unit_test_parameter.PARAMETER_TYPE              PARAMETER_TYPE,
			  unit_test_parameter.PARAMETER_VALUE_VARCHAR     PARAMETER_VALUE_VARCHAR,
			  unit_test_parameter.PARAMETER_VALUE_NUMBER      PARAMETER_VALUE_NUMBER,
			  unit_test_parameter.PARAMETER_VALUE_DATE        PARAMETER_VALUE_DATE,
			  replace(replace(unit_test_parameter.PARAMETER_VALUE_JSON, chr(9), ''), chr(13), '')      PARAMETER_VALUE_JSON,
			  unit_test_parameter.PARAMETER_VALUE_JSON_LIST   PARAMETER_VALUE_JSON_LIST,
			  unit_test_parameter.PARAMETER_VALUE_JSON_VALUE  PARAMETER_VALUE_JSON_VALUE,
			  unit_test_parameter.PARAMETER_VALUE_CLOB        PARAMETER_VALUE_CLOB,
			  unit_test_parameter.PARAMETER_VALUE_BLOB        PARAMETER_VALUE_BLOB,
			  unit_test_parameter.PARAMETER_VALUE_XML         PARAMETER_VALUE_XML
			from
			  spd_t_unit_test_parameters  unit_test_parameter
			where
			  unit_test_parameter.unittestid = rec.UNITTESTID
			  and parameter_name is not null
			order by
			  unit_test_parameter.parameter_set
			)
			loop


				lc_vLocalName := replace(lower(rec2.PARAMETER_NAME),'in_','lc_');
				case
				  when rec2.PARAMETER_TYPE = 'VARCHAR2' then
					lc_vDeclareVariables := lc_vDeclareVariables  || chr(9) || lc_vLocalName || ' ' || rec2.PARAMETER_TYPE || '(' || lengthb(rec2.PARAMETER_VALUE_VARCHAR)|| ')' ||';' ||chr(13);
				  else
					lc_vDeclareVariables := lc_vDeclareVariables  || chr(9) || lc_vLocalName || ' ' || rec2.PARAMETER_TYPE || ';' ||chr(13) ; -- add each parameter name to the list of declared variables
				end case;
	
				lc_vDeclareBegin := CONCAT(lc_vDeclareVariables || chr(13), lc_vBeginVariables);
				lc_vUsing := lc_vUsing || ':' || lc_vLocalName || ',';
				lc_vParameterSection := lc_vParameterSection || rec2.PARAMETER_NAME || ' => ' || ':' || lc_vLocalName || ',';
				lc_vExecuteImmediateIn := lc_vExecuteImmediateIn || lc_vLocalName || ',';

				case rec2.PARAMETER_TYPE
				   WHEN 'VARCHAR2'    THEN lc_vSetup := lc_vSetup || chr(9) || lc_vLocalName || ':= ''' || rec2.PARAMETER_VALUE_VARCHAR || ''';' || chr(13);
				   WHEN 'NUMBER'      THEN lc_vSetup := lc_vSetup || chr(9) || lc_vLocalName || ':= ' || rec2.PARAMETER_VALUE_NUMBER ||';' || chr(13);
				   WHEN 'DATE'        THEN lc_vSetup := lc_vSetup || chr(9) || lc_vLocalName || ':= to_date(''' || rec2.PARAMETER_VALUE_DATE ||''',''DD-MON-YYYY'');' || chr(13);
				   WHEN 'JSON'        THEN lc_vSetup := lc_vSetup || chr(9) || lc_vLocalName || ':= json(''' || rec2.PARAMETER_VALUE_JSON  ||''');' || chr(13);
				   WHEN 'JSON_LIST'   THEN lc_vSetup := lc_vSetup || chr(9) || lc_vLocalName || ':= json_list(''' || rec2.PARAMETER_VALUE_JSON_LIST ||''');' || chr(13);
				   WHEN 'JSON_VALUE'  THEN lc_vSetup := lc_vSetup || chr(9) || lc_vLocalName || ':= json_value(''' || rec2.PARAMETER_VALUE_JSON_VALUE ||''');' || chr(13);
				   WHEN 'CLOB'        THEN lc_vSetup := lc_vSetup || chr(9) || lc_vLocalName || ':= ''' || rec2.PARAMETER_VALUE_CLOB ||''';' || chr(13);
				   /*!!!NOTE: WHEN BLOB OR XML, FOR NOW MUST USE CUSTOM SETUP*/
				   --WHEN 'BLOB'        THEN lc_vSetup := lc_vSetup || lc_vLocalName || ':= ' empty_blob();--util_raw.cast_to_raw('changeme'); || chr(13);
				   --WHEN 'XMLTYPE'     THEN lc_vSetup := lc_vSetup || lc_vLocalName || ':= xmltype(''' || rec2.PARAMETER_VALUE_XML || ''');' || chr(13);
				   else
					htp.p('The UNIT_TEST_PARAMETERS parameter type is unknown');
				end case;

			end loop;

			lc_lTestBloc := lc_vDeclareBegin;
			lc_lTestBloc := lc_lTestBloc || lc_vSetup;
			lc_lTestBloc := lc_lTestBloc ||lc_vCustomSetup;



			lc_vParameterSection := SUBSTR(lc_vParameterSection,1,length(lc_vParameterSection)-1);
			lc_vUsing := SUBSTR(lc_vUsing,1,length(lc_vUsing)-1);
			lc_vExecuteImmediateIn := SUBSTR(lc_vExecuteImmediateIn,1,length(lc_vExecuteImmediateIn)-1);

			lc_vExecuteImmediateSql := 'lc_vSql := ''begin :x := ' || rec.package_name || '.' || rec.procedure_name || '(' || lc_vParameterSection || '); end;'''||';';
			lc_lTestBloc := lc_lTestBloc || chr(9)||lc_vExecuteImmediateSql || chr(13);
			lc_vExecuteImmediate := 'execute immediate lc_vSql using out lc_Return, ' || lc_vExecuteImmediateIn || ';';
			lc_lTestBloc := lc_lTestBloc || chr(9)||lc_vExecuteImmediate || chr(13) || chr(13);



			case rec.RETURN_TYPE
			   WHEN 'VARCHAR2'    THEN lc_vTest := 'lc_vAssert :=''' || rec.assert_value_varchar || ''';' || chr(13) || chr(9)|| 'ut_common.' || rec.ASSERT_TEST || '(lc_Return,lc_vAssert);';
			   WHEN 'NUMBER'      THEN lc_vTest := 'lc_nAssert :=' || rec.assert_value_number || ';' || chr(13) || chr(9)||'ut_common.' || rec.ASSERT_TEST || '(lc_Return,lc_nAssert);';
			   WHEN 'DATE'        THEN lc_vTest := 'lc_dAssert :=' || rec.assert_value_date || ';' || chr(13) || chr(9)||'ut_common.' || rec.ASSERT_TEST || '(lc_Return,lc_dAssert);';
			   WHEN 'JSON'        THEN lc_vTest := 'lc_jAssert := json(''' || rec.assert_value_json || ''');' || chr(13) || chr(9)||'ut_common.' || rec.ASSERT_TEST || '(lc_Return,lc_jAssert);';
			   WHEN 'JSON_LIST'   THEN lc_vTest := 'lc_jlAssert := json_list(''' || rec.assert_value_json_list || ''');' || chr(13) || chr(9)||'ut_common.' || rec.ASSERT_TEST || '(lc_Return,lc_jlAssert);';
			   WHEN 'JSON_VALUE'  THEN lc_vTest := 'lc_jvAssert := json_value(''' || rec.assert_value_json_value || ''');' || chr(13) || chr(9)||'ut_common.' || rec.ASSERT_TEST || '(lc_Return,lc_jvAssert);';
			   WHEN 'CLOB'        THEN lc_vTest := 'lc_cAssert :=''' || rec.assert_value_clob || ''';' || chr(13) || chr(9)|| 'ut_common.' || rec.ASSERT_TEST || '(lc_Return,lc_cAssert);';
			   /*!!!NOTE: WHEN BLOB OR XML, FOR NOW MUST USE CUSTOM SETUP*/
			   --WHEN 'BLOB'        THEN htp.p('lc_bAssert :=' || rec.assert_value_blob || ';' || chr(13) || rec.ASSERT_TEST || '(lc_Return,lc_bAssert);');
			   --WHEN 'XMLTYPE'     THEN htp.p('lc_xAssert :=' || rec.assert_value_xml || ';' || chr(13) || rec.ASSERT_TEST || '(lc_Return,lc_xAssert);');
			   else
				htp.p('The UNIT_TEST return type is unknown');
			end case;

			lc_lTestBloc := lc_lTestBloc ||chr(9)||lc_vTest|| chr(13)||chr(13);
			lc_lTestBloc := lc_lTestBloc || lc_vCustomTeardown;
			lc_lTestBloc := lc_lTestBloc || chr(9)||'ut_common.pass(' || rec.UNITTESTID || ',' || '''' || rec.PACKAGE_NAME || ''''|| ',' || '''' || rec.PROCEDURE_NAME || '''' || ',' || '''' || rec.TEST_NAME || '''' || ');'||chr(13);
      lc_lTestBloc := lc_lTestBloc ||chr(9)||'exception when others then '||chr(13)||chr(9)||'ut_common.fail(' || rec.UNITTESTID || ',' || '''' || rec.PACKAGE_NAME || ''''|| ',' || '''' || rec.PROCEDURE_NAME || '''' || ',' || '''' || rec.TEST_NAME || '''' || ');'||chr(13)||lc_vCustomTeardown;      lc_lTestBloc := lc_lTestBloc || 'END;';
      
      begin
        execute immediate lc_lTestBloc;
        exception when others then 
        htp.p('------------------------------');
        htp.p('there was a problem executing the test: ' || rec.TEST_NAME);
        htp.p('sqlerrm:' || sqlerrm);
        htp.p('------------------------------');
      end;
      
      lc_lTestBloc := '';

		end loop;

		lc_llastBloc := 'BEGIN'||chr(13);
		lc_llastBloc := lc_llastBloc ||'ut_common.postResults();'|| chr(13);
		lc_llastBloc := lc_llastBloc ||'END;';
		execute immediate lc_llastBloc;


		exception when
      others then 
        htp.p('sqlerrm:' || sqlerrm);
        
    
	
	END runTests;
	
	
	
	
	
   PROCEDURE makePackageFunctionPublic AS
   lc_vFlag varchar2(250);
   BEGIN
   lc_vFlag:= 'alter session set plsql_ccflags='''||'dev_env_test:true'||'''';
    execute immediate lc_vFlag;

		execute immediate 'alter package PKG_COST_ESTIMATE compile';
		execute immediate 'alter package PKG_COST_ESTIMATE_XML compile';
		execute immediate 'alter package PKG_DATA_TABLE_CUSTOM compile';
		execute immediate 'alter package PKG_DATA_TABLES compile';
		execute immediate 'alter package PKG_DELIVERY_ORDER_FACILITY compile';
		execute immediate 'alter package PKG_DIRECT_COSTS compile';
		execute immediate 'alter package PKG_FACILITY compile';
		execute immediate 'alter package PKG_JOB_TITLES compile';
		execute immediate 'alter package PKG_PURGE_MAXIMO_AF_DATA compile';
		execute immediate 'alter package PKG_QUERY_COLLECTION_CUSTOM compile';
		execute immediate 'alter package PKG_QUERY_COLLECTIONS compile';
		execute immediate 'alter package PKG_READ_DATA_TABLE compile';
		execute immediate 'alter package PKG_SAVE_PWS_TEMPLATE compile';
		execute immediate 'alter package PKG_SAVEBLOBASFILESYSTEMFILE compile';
    execute immediate 'alter package PKG_XML_FLATTEN compile';
    execute immediate 'alter package PKG_XML_PARSE compile';
   END makePackageFunctionPublic;
   
   
   PROCEDURE makePackageFunctionPrivate AS
  lc_vFlag varchar2(250);
   BEGIN
   lc_vFlag:= 'alter session set plsql_ccflags='''||'dev_env_test:false'||'''';
    execute immediate lc_vFlag;
    
		execute immediate  'alter package PKG_COST_ESTIMATE compile';
		execute immediate  'alter package PKG_COST_ESTIMATE_XML compile';
		execute immediate  'alter package PKG_DATA_TABLE_CUSTOM compile';
		execute immediate  'alter package PKG_DATA_TABLES compile';
		execute immediate  'alter package PKG_DELIVERY_ORDER_FACILITY compile';
		execute immediate  'alter package PKG_DIRECT_COSTS compile';
		execute immediate  'alter package PKG_FACILITY compile';
		execute immediate  'alter package PKG_JOB_TITLES compile';
		execute immediate  'alter package PKG_PURGE_MAXIMO_AF_DATA compile';
		execute immediate  'alter package PKG_QUERY_COLLECTION_CUSTOM compile';
		execute immediate  'alter package PKG_QUERY_COLLECTIONS compile';
		execute immediate  'alter package PKG_READ_DATA_TABLE compile';
		execute immediate  'alter package PKG_SAVE_PWS_TEMPLATE compile';
		execute immediate  'alter package PKG_SAVEBLOBASFILESYSTEMFILE compile';
    execute immediate  'alter package PKG_XML_FLATTEN compile';
    execute immediate  'alter package PKG_XML_PARSE compile';
   END makePackageFunctionPrivate;
   
   
   
   
END UT_GENERATOR;

/
