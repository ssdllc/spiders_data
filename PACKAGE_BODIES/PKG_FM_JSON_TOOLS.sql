--------------------------------------------------------
--  DDL for Package Body PKG_FM_JSON_TOOLS
--------------------------------------------------------

CREATE OR REPLACE PACKAGE BODY PKG_FM_JSON_TOOLS AS

    FUNCTION getCorrespondingJsonColumnVal(in_jJson JSON, in_vColumn VARCHAR2) RETURN VARCHAR2 AS
    /* template variables */
    json_error EXCEPTION;
    spiders_error EXCEPTION;
    lc_vErrorDescription VARCHAR2(255) := 'default';
    lc_vFunctionName VARCHAR2(255) := 'getCorrespondingJsonColumnVal';
    lc_vReturn VARCHAR2(32000) := '{"error":"default"}'; -- default the return to e for error
    lc_nContinue NUMBER :=0; -- default to 0 to disallow continuing
    
    /* function specific variables */
    lc_vName varchar2(2000);
    lc_vTemp varchar2(2000);
    lc_vComment varchar2(2000);
    lc_jvValue json_value;
    lc_jlData json_list;
    lc_jvRecVal json_value;
    lc_jValue json;
  begin
  
    /* i could not convince myself that we needed multiple functions here, since we still need to check if attributes exist in the sub functions*/
    if(in_jJson.exist('data')) then
        if(in_jJson.get('data').is_array) then
            lc_jlData := json_list(in_jJson.get('data'));        
            lc_jvRecVal := lc_jlData.get(1); --return null on outofbounds
            if (lc_jvRecVal is not null) then
              lc_jValue := json(lc_jvRecVal);            
              if(lc_jValue.exist(in_vColumn)) then       
                lc_jvValue := lc_jValue.get(in_vColumn);
                lc_vReturn := getVal(lc_jvValue);
              else
                lc_vErrorDescription := in_vColumn || ' attribute not found in json';
                raise spiders_error;
              end if;
            else 
              lc_vErrorDescription := 'Rec val is null';
              raise spiders_error;
            end if;
        else
          lc_vErrorDescription := 'Data is not an array';
          raise spiders_error;
        end if;
    else
      lc_vErrorDescription := 'Data attribute not found in json';
      raise spiders_error;
    end if;
              
    return lc_vReturn;
  
  exception     
    when spiders_error then
        execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' SPIDERS error ' || lc_vErrorDescription || ''')';
        return '{"error":"' || lc_vErrorDescription || '"}';
    when json_error then
        execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' JSON error'')';
        return '{"error":"json"}';
    when others then
        execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' dml error: ' || sqlerrm || ''')';
        return '{"error":"' || sqlerrm || '"}';
  END getCorrespondingJsonColumnVal;
  
  FUNCTION runDML(in_vInsertStatement VARCHAR2, in_jMessages OUT JSON) RETURN VARCHAR2 AS
      lc_jlDML JSON_LIST;
      lc_jResults JSON;
      --lc_jMessages JSON;
      lc_vDMLResult VARCHAR2(255);
      lc_vReturn VARCHAR2(32000) := '{"error":"default"}'; -- default the return to e for error
      BEGIN
      lc_jlDML := json_list();
      lc_jlDML.append(in_vInsertStatement);
      --lc_jMessages := JSON();
      lc_jResults := json();
      lc_jResults.put('results',lc_jlDML);
      lc_vDMLResult := BB_F_DML(lc_jResults, in_jMessages);
      lc_vReturn := lc_vDMLResult;
      return lc_vDMLResult;
    END runDML;
  
  FUNCTION buildFMTableSingleInsert(in_FMJSON JSON, in_TableName VARCHAR2, in_startingColumns VARCHAR2, in_startingValues VARCHAR2, in_columnNamePrefix VARCHAR2 default '', in_columnNameSuffix VARCHAR2 default '') RETURN VARCHAR2 AS
  
    lc_JSONVal json_value;
    lc_vExtractedValue varchar2(4000);
    lc_vInsertColumnNames varchar2(4000);
    lc_vInsertColumnValues varchar2(4000);
    lc_vInsertStatement varchar2(8000);
    lc_nMakeInsert number := 0;
    
    BEGIN
      
      lc_vInsertColumnNames := in_startingColumns;
      lc_vInsertColumnValues := in_startingValues;
      FOR rec IN (SELECT *
                  FROM all_tab_cols
                  WHERE table_name = in_TableName)
      LOOP
        --dbms_output.put_line(in_columnNamePrefix || rec.COLUMN_NAME || in_columnNameSuffix);
        IF (in_FMJSON.exist(in_columnNamePrefix || rec.COLUMN_NAME || in_columnNameSuffix)) THEN
          dbms_output.put_line('looping: ' || in_columnNamePrefix || rec.COLUMN_NAME || in_columnNameSuffix);
          lc_JSONVal := in_FMJSON.get(in_columnNamePrefix || rec.COLUMN_NAME || in_columnNameSuffix);
          lc_vExtractedValue := getVal(lc_JSONVal);
          IF (LENGTH(lc_vExtractedValue) > 0) THEN
            lc_nMakeInsert := 1;
          END IF;
          IF (rec.DATA_TYPE <> 'NUMBER') THEN
            -- Replacing any single quotes inside the value
            lc_vExtractedValue := replace(lc_vExtractedValue, '''', '''''');
            -- Wrapping the value in single quotes
            lc_vExtractedValue := '''' || lc_vExtractedValue || '''';
          END IF;
          lc_vInsertColumnNames := lc_vInsertColumnNames || ',' || rec.COLUMN_NAME;
          lc_vInsertColumnValues := lc_vInsertColumnValues || ',' || lc_vExtractedValue;
        END IF;
      END LOOP;
      dbms_output.put_line('end single insert loop');
      lc_vInsertStatement := 'INSERT INTO ' || in_TableName ||
                              '(' || lc_vInsertColumnNames || ')
                              VALUES (' || lc_vInsertColumnValues || ')';
      IF (lc_nMakeInsert <> 0) THEN
        RETURN lc_vInsertStatement;
      ELSE 
        RETURN 'No JSON Found';
      END IF;
  END buildFMTableSingleInsert;
  
  PROCEDURE buildFMTableMultiInsert(in_FMJSON JSON, in_TableName VARCHAR2, in_startingColumns VARCHAR2, in_startingValues VARCHAR2, in_jMessages IN OUT JSON) AS
    lc_vInsertStatement varchar2(8000);
    lc_vColumnNamePrefix varchar2(255);
    lc_vDMLResult varchar2(255);
    BEGIN
      lc_vColumnNamePrefix := SUBSTR(in_TableName, 10) || '_';
      FOR rec in 1..25
      LOOP
        lc_vInsertStatement := buildFMTableSingleInsert(in_FMJSON, in_TableName, in_startingColumns, in_startingValues, lc_vColumnNamePrefix, '_Row_' || rec);
        dbms_output.put_line(lc_vInsertStatement);
        IF (lc_vInsertStatement <> 'No JSON Found') THEN
          lc_vDMLResult := runDML(lc_vInsertStatement, in_jMessages);
        END IF;
      END LOOP;
  END buildFMTableMultiInsert;
  
  PROCEDURE cleanupFMTables(in_FMACTIONID number, in_FMMOORINGID number) AS
    lc_FMACTIONMOORINGID number;
    BEGIN
        FOR rec IN
          (SELECT FMACTIONMOORINGID
          FROM SPD_T_FM_ACTION_MOORINGS
          WHERE FMACTIONID = in_FMACTIONID AND FMMOORINGID = in_FMMOORINGID)
          LOOP
            dbms_output.put_line('found FMACTIONMOORINGID' || lc_FMACTIONMOORINGID);
            DELETE FROM SPD_T_FM_BUOY_TOPSIDE WHERE FMACTIONMOORINGID = rec.FMACTIONMOORINGID;
            dbms_output.put_line('deleting from  SPD_T_FM_BUOY_TOPSIDE');
            DELETE FROM SPD_T_FM_BUOY_TOP_JEWELRY WHERE FMACTIONMOORINGID = rec.FMACTIONMOORINGID;
            dbms_output.put_line('deleting from  SPD_T_FM_BUOY_TOP_JEWELRY');
            DELETE FROM SPD_T_FM_INSP_COMMENTS WHERE FMACTIONMOORINGID = rec.FMACTIONMOORINGID;
            dbms_output.put_line('deleting from  SPD_T_FM_INSP_COMMENTS');
            DELETE FROM SPD_T_FM_INSP_MEASUREMENTS WHERE FMACTIONMOORINGID = rec.FMACTIONMOORINGID;
            dbms_output.put_line('deleting from  SPD_T_FM_INSP_MEASUREMENTS');
            DELETE FROM SPD_T_FM_ACTION_MOORINGS WHERE FMACTIONMOORINGID = rec.FMACTIONMOORINGID;
        END LOOP;
  END cleanupFMTables;

  FUNCTION parseFleetMooringJSON(in_cJSON CLOB) RETURN JSON AS
    lc_jMessages JSON;
    lc_stubJSON JSON;
    
    lc_jJSON JSON;
    lc_jlFMJSON JSON_LIST;
    lc_jFMJSON JSON;
    
    --lc_stubJSONVal json_value;
    lc_stubFMACTIONID  number;
    lc_stubFMMOORINGID number;
    lc_FMACTIONMOORINGID number;
    --lc_vExtractedValue varchar2(255);
    lc_vInsertColumnNames varchar2(4000);
    lc_vInsertColumnValues varchar2(4000);
    lc_vInsertStatement varchar2(8000);
    lc_vDMLResult varchar2(255);
  
    BEGIN
      lc_jMessages := json();
    
      lc_jJSON := JSON(in_cJSON);
      lc_jlFMJSON := json_list(lc_jJSON.get('data'));
      lc_jFMJSON := json(lc_jlFMJSON.get(1));
      
    -- convert this stub data to a JSON object
    -- query SPD_T_FM_MOORING_ACTION and get all column names
    -- for each column name in JSON look for attribute
      --lc_stubFMACTIONID := 660000000000018;
      --lc_stubFMMOORINGID := 650000000000063;
      
      cleanupFMTables(lc_stubFMACTIONID, lc_stubFMMOORINGID);
      
      SELECT SPD_S_FM_ACTION_MOORINGS.nextval INTO lc_FMACTIONMOORINGID FROM dual;
      lc_vInsertColumnNames := 'FMACTIONMOORINGID';
      lc_vInsertColumnValues := lc_FMACTIONMOORINGID;
      lc_vInsertStatement := buildFMTableSingleInsert(lc_jFMJSON, 'SPD_T_FM_ACTION_MOORINGS', lc_vInsertColumnNames, lc_vInsertColumnValues);
      
      lc_vDMLResult := runDML(lc_vInsertStatement, lc_jMessages);
      
      dbms_output.put_line(lc_vDMLResult);
      
      IF (lc_vDMLResult = 'y') THEN
        lc_vInsertColumnNames := 'FMACTIONMOORINGID';
        lc_vInsertColumnValues := lc_FMACTIONMOORINGID;
        buildFMTableMultiInsert(lc_jFMJSON, 'SPD_T_FM_BUOY_TOPSIDE', lc_vInsertColumnNames, lc_vInsertColumnValues, lc_jMessages);
        dbms_output.put_line('building SPD_T_FM_BUOY_TOPSIDE');
        buildFMTableMultiInsert(lc_jFMJSON, 'SPD_T_FM_BUOY_TOP_JEWELRY', lc_vInsertColumnNames, lc_vInsertColumnValues, lc_jMessages);
        dbms_output.put_line('building SPD_T_FM_BUOY_TOP_JEWELRY');
        buildFMTableMultiInsert(lc_jFMJSON, 'SPD_T_FM_INSP_COMMENTS', lc_vInsertColumnNames, lc_vInsertColumnValues, lc_jMessages);
        dbms_output.put_line('building SPD_T_FM_INSP_COMMENTS');
        buildFMTableMultiInsert(lc_jFMJSON, 'SPD_T_FM_INSP_MEASUREMENTS', lc_vInsertColumnNames, lc_vInsertColumnValues, lc_jMessages);
        dbms_output.put_line('building SPD_T_FM_INSP_MEASUREMENTS');
      END IF;
      -- return lc_vReturn;

      return lc_jMessages;
    END parseFleetMooringJSON;
    
END PKG_FM_JSON_TOOLS;

