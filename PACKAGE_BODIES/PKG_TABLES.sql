--------------------------------------------------------
--  DDL for Package Body PKG_TABLES
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE PACKAGE BODY "SPIDERS_DATA"."PKG_TABLES" as

function f_test_for_error (in_vReturned varchar2) return varchar2;
function f_parse_json_table_name (in_jJson json) return varchar2;

function f_object_exists (in_vObjectName varchar2) return varchar2 as
  /* template variables */
  lc_vFunctionName varchar2(255) := 'f_object_exists';
  lc_vReturn varchar2(32000) := '{"error":"default"}'; --defaul the return to e for error
  lc_nCount number :=0;

  /* function specific variables */
begin


  select count(object_name) into lc_nCount from user_objects where object_name = in_vObjectName;
  return lc_nCount;

exception
  when others then
    execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' dml error: ' || sqlerrm || ''')';
    return '{"error":"' || sqlerrm || '"}';
end;


function f_drop_sequence (in_vName varchar2) return varchar2 as
  /* template variables */
  lc_vFunctionName varchar2(255) := 'f_drop_sequence';
  lc_vReturn varchar2(32000) := '{"error":"default"}'; --defaul the return to e for error
  lc_vPrefix varchar2(20) := 'SPD_S_'; --all sequences have this prefix before the name
  lc_vSql varchar2(2000) := '';
  lc_vSqlError varchar2(2000) := '';
  lc_nContinue number := 0;

  /* function specific variables */
  lc_vName varchar2(2000);
begin

  lc_vName := lc_vPrefix || in_vName;
  lc_nContinue := f_object_exists(lc_vName);

  if lc_nContinue > 0 then
    lc_vSql := 'drop sequence ' || lc_vName;
    execute immediate 'insert into debug (message) values (''' || lc_vSql || ''')';
    execute immediate lc_vSql;
  end if;

  return '1';

exception
  when others then
    execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' dml error: ' || sqlerrm || ''')';
    return '{"error":"' || sqlerrm || '"}';
end;

function f_drop_insert_trigger (in_vName varchar2) return varchar2 as
  /* template variables */
  lc_vFunctionName varchar2(255) := 'f_drop_insert_trigger';
  lc_vReturn varchar2(32000) := '{"error":"default"}'; --defaul the return to e for error
  lc_vPrefix varchar2(20) := 'SPD_I_'; --all triggers have this prefix before the name
  lc_vSql varchar2(2000) := '';
  lc_vSqlError varchar2(2000) := '';
  lc_nContinue number := 0;

  /* function specific variables */
  lc_vName varchar2(2000);
begin

  lc_vName := lc_vPrefix || in_vName;
  lc_nContinue := f_object_exists(lc_vName);

  if lc_nContinue > 0 then
    lc_vSql := 'drop trigger ' || lc_vName;
    execute immediate 'insert into debug (message) values (''' || lc_vSql || ''')';
    execute immediate lc_vSql;
  end if;

  return '1';

exception
  when others then
    execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' dml error: ' || sqlerrm || ''')';
    return '{"error":"' || sqlerrm || '"}';
end;

function f_drop_update_trigger (in_vName varchar2) return varchar2 as
  /* template variables */
  lc_vFunctionName varchar2(255) := 'f_drop_update_trigger';
  lc_vReturn varchar2(32000) := '{"error":"default"}'; --defaul the return to e for error
  lc_vPrefix varchar2(20) := 'SPD_U_'; --all triggers have this prefix before the name
  lc_vSql varchar2(2000) := '';
  lc_vSqlError varchar2(2000) := '';
  lc_nContinue number := 0;

  /* function specific variables */
  lc_vName varchar2(2000);
begin

  lc_vName := lc_vPrefix || in_vName;
  lc_nContinue := f_object_exists(lc_vName);

  if lc_nContinue > 0 then
    lc_vSql := 'drop trigger ' || lc_vName;
    execute immediate 'insert into debug (message) values (''' || lc_vSql || ''')';
    execute immediate lc_vSql;
  end if;

  return '1';

exception
  when others then
    execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' dml error: ' || sqlerrm || ''')';
    return '{"error":"' || sqlerrm || '"}';
end;

function f_drop_table (in_vName varchar2) return varchar2 as
  /* template variables */
  lc_vFunctionName varchar2(255) := 'f_drop_table';
  lc_vReturn varchar2(32000) := '{"error":"default"}'; --defaul the return to e for error
  lc_vPrefix varchar2(20) := 'SPD_T_'; --all tables have this prefix before the name
  lc_vSql varchar2(2000) := '';
  lc_vSqlError varchar2(2000) := '';
  lc_nContinue number := 0;

  /* function specific variables */
  lc_vName varchar2(2000);
begin

  lc_vName := lc_vPrefix || in_vName;
  lc_nContinue := f_object_exists(lc_vName);

  if lc_nContinue > 0 then
    lc_vSql := 'drop table ' || lc_vName;
    execute immediate 'insert into debug (message) values (''' || lc_vSql || ''')';
    execute immediate lc_vSql;
  end if;

  return '1';

exception
  when others then
    execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' dml error: ' || sqlerrm || ''')';
    return '{"error":"' || sqlerrm || '"}';
end;

function getNextSequence return varchar2 as
  /* template variables */
  lc_vFunctionName varchar2(255) := 'getNextSequence';
  lc_vReturn varchar2(32000) := '840000000'; --defaul the return to 840000000
  lc_vSql varchar2(2000) := '';

  /* function specific variables */
begin

  select
    (substr(max(last_number),1,2) + 1) * RPAD(1,length(max(last_number))-1,0) into lc_vReturn
  from
    user_sequences;

  return lc_vReturn;

exception
  when others then
    execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' dml error: ' || sqlerrm || ''')';
    return '{"error":"' || sqlerrm || '"}';
end;

function getValidObjectCount(in_vName varchar2) return varchar2 as
  /* template variables */
  lc_vFunctionName varchar2(255) := 'getValidObjectCount';
  lc_vReturn varchar2(32000) := '{"error":"default"}'; --defaul the return to 840000000
  lc_vSql varchar2(2000) := '';

  /* function specific variables */
begin

  select
    count(object_name) into lc_vReturn
  from
    user_objects
  where
    object_name like '%' || in_vName
    and status = 'VALID';
  /*EVENTUALLY WE NEED TO ADD THIS TO THE DEBUG TABLE*/
  return lc_vReturn;

exception
  when others then
    execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' dml error: ' || sqlerrm || ''')';
    return '{"error":"' || sqlerrm || '"}';
end;

function f_create_sequence (in_vName varchar2) return varchar2 as
  /* template variables */
  lc_vFunctionName varchar2(255) := 'f_create_sequence';
  lc_vReturn varchar2(32000) := '{"error":"default"}'; --defaul the return to e for error
  lc_vPrefix varchar2(20) := 'SPD_S_'; --all tables have this prefix before the name
  lc_vSql varchar2(2000) := '';
  lc_vSqlError varchar2(2000) := '';
  lc_nContinue number := 0;

  /* function specific variables */
  lc_vName varchar2(2000);
  lc_nSequenceStart number := 840000000;
begin

  lc_nSequenceStart := getNextSequence;
  lc_vName := lc_vPrefix || in_vName;
  lc_nContinue := f_object_exists(lc_vName);


  if lc_nContinue = 0 then
    /*this sequence does not exist, create it*/
    lc_vSql := 'create sequence ' || lc_vName || ' start with ' || lc_nSequenceStart;
    execute immediate 'insert into debug (message) values (''' || lc_vSql || ''')';
    execute immediate lc_vSql;
  end if;

  return '1';

exception
  when others then
    execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' dml error: ' || sqlerrm || ''')';
    return '{"error":"' || sqlerrm || '"}';
end;

function f_create_alter_update_trigger (in_vName varchar2) return varchar2 as
  /* template variables */
  lc_vFunctionName varchar2(255) := 'f_create_alter_update_trigger';
  lc_vReturn varchar2(32000) := '{"error":"default"}'; --defaul the return to e for error
  lc_vPrefix varchar2(20) := 'SPD_U_'; --all tables have this prefix before the name
  lc_vSql varchar2(2000) := '';
  lc_vSqlError varchar2(2000) := '';
  lc_nContinue number := 0;

  /* function specific variables */
begin

  lc_vSql := 'ALTER TRIGGER ' || lc_vPrefix || in_vName || ' ENABLE';

  execute immediate 'insert into debug (message) values (''' || lc_vSql || ''')';
  execute immediate lc_vSql;

  return '1';

exception
  when others then
    execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' dml error: ' || sqlerrm || ''')';
    return '{"error":"' || sqlerrm || '"}';
end;

function f_create_alter_insert_trigger (in_vName varchar2) return varchar2 as
  /* template variables */
  lc_vFunctionName varchar2(255) := 'f_create_alter_trigger';
  lc_vReturn varchar2(32000) := '{"error":"default"}'; --defaul the return to e for error
  lc_vPrefix varchar2(20) := 'SPD_I_'; --all tables have this prefix before the name
  lc_vSql varchar2(2000) := '';
  lc_vSqlError varchar2(2000) := '';
  lc_nContinue number := 0;

  /* function specific variables */
begin

  lc_vSql := 'ALTER TRIGGER ' || lc_vPrefix || in_vName || ' ENABLE';

  execute immediate 'insert into debug (message) values (''' || lc_vSql || ''')';
  execute immediate lc_vSql;

  return '1';

exception
  when others then
    execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' dml error: ' || sqlerrm || ''')';
    return '{"error":"' || sqlerrm || '"}';
end;

function f_create_update_trigger (in_vName varchar2) return varchar2 as
  /* template variables */
  lc_vFunctionName varchar2(255) := 'f_create_update_trigger';
  lc_vReturn varchar2(32000) := '{"error":"default"}'; --defaul the return to e for error
  lc_vPrefix varchar2(20) := 'SPD_U_'; --all tables have this prefix before the name
  lc_vPrefixTable varchar2(20) := 'SPD_T_'; --all tables have this prefix before the name
  lc_vSql varchar2(2000) := '';
  lc_vSqlError varchar2(2000) := '';
  lc_nContinue number := 0;

  /* function specific variables */
  lc_vNameTrigger varchar2(2000);
  lc_vNameTable varchar2(2000);
  lc_vNameSequence varchar2(2000);
  lc_jlDML json_list;
  lc_jResults json;
  lc_jMessages json;
  lc_vDMLResult varchar2(255);
begin

  lc_jlDML := json_list();

  lc_vNameTrigger := lc_vPrefix || in_vName;
  lc_vNameTable := lc_vPrefixTable || in_vName;


  lc_vSql := 'CREATE OR REPLACE TRIGGER ' || lc_vNameTrigger || '
  BEFORE UPDATE ON ' || lc_vNameTable || ' FOR EACH ROW

  BEGIN
    :new.UPDATED_BY := nvl(v(''APP_USER''),''NONE'');
    :new.UPDATED_TIMESTAMP := SYSTIMESTAMP;
  END;';

  lc_jlDML.append(lc_vSql);


  lc_jResults := json();
  lc_jResults.put('results',lc_jlDML);
  lc_jMessages := json();
  lc_vDMLResult := BB_F_DML(lc_jResults, lc_jMessages);
  lc_vReturn := lc_vDMLResult;
  return lc_vReturn;


exception
  when others then
    execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' dml error: ' || sqlerrm || ''')';
    return '{"error":"' || sqlerrm || '"}';
end;

function f_create_insert_trigger (in_vName varchar2, in_vColumnName varchar2) return varchar2 as
  /* template variables */
  lc_vFunctionName varchar2(255) := 'f_create_insert_trigger';
  lc_vReturn varchar2(32000) := '{"error":"default"}'; --defaul the return to e for error
  lc_vPrefix varchar2(20) := 'SPD_I_'; --all tables have this prefix before the name
  lc_vPrefixTable varchar2(20) := 'SPD_T_'; --all tables have this prefix before the name
  lc_vPrefixSequence varchar2(20) := 'SPD_S_'; --all tables have this prefix before the name
  lc_vSql varchar2(2000) := '';
  lc_vSqlError varchar2(2000) := '';
  lc_nContinue number := 0;

  /* function specific variables */
  lc_vNameTrigger varchar2(2000);
  lc_vNameTable varchar2(2000);
  lc_vNameSequence varchar2(2000);
  lc_jlDML json_list;
  lc_jResults json;
  lc_jMessages json;
  lc_vDMLResult varchar2(255);
begin

  lc_jlDML := json_list();


  lc_vNameTrigger := lc_vPrefix || in_vName;
  lc_vNameTable := lc_vPrefixTable || in_vName;
  lc_vNameSequence := lc_vPrefixSequence || in_vName;


  lc_vSql := 'CREATE OR REPLACE TRIGGER ' || lc_vNameTrigger || '
   BEFORE INSERT ON ' || lc_vNameTable || ' FOR EACH ROW
   BEGIN
    IF :new.' || in_vColumnName || ' IS NULL THEN
      SELECT ' || lc_vNameSequence || '.nextval INTO :new.' || in_vColumnName || ' FROM dual;
    END IF;
  :new.CREATED_BY := nvl(v(''APP_USER''),''NONE'');
  :new.CREATED_TIMESTAMP := SYSTIMESTAMP;
  END;';

  lc_jlDML.append(lc_vSql);


  lc_jResults := json();
  lc_jResults.put('results',lc_jlDML);
  lc_jMessages := json();
  lc_vDMLResult := BB_F_DML(lc_jResults, lc_jMessages);
  lc_vReturn := lc_vDMLResult;
  return lc_vReturn;


exception
  when others then
    execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' dml error: ' || sqlerrm || ''')';
    return '{"error":"' || sqlerrm || '"}';
end;


function f_create_table_comment (in_vName varchar2, in_vComment varchar2) return varchar2 as
  /* template variables */
  lc_vFunctionName varchar2(255) := 'f_create_table_comment';
  lc_vReturn varchar2(32000) := '{"error":"default"}'; --defaul the return to e for error
  lc_vPrefix varchar2(20) := 'SPD_T_'; --all tables have this prefix before the name
  lc_vSql varchar2(2000) := '';
  lc_vSqlError varchar2(2000) := '';
  lc_nContinue number := 0;

  /* function specific variables */
  lc_vName varchar2(2000);
begin

  lc_vName := lc_vPrefix || in_vName;
  lc_nContinue := f_object_exists(lc_vName);


  if lc_nContinue > 0 then
    /*this table does exist, create the comment*/
    lc_vSql := 'COMMENT ON TABLE ' || lc_vName || ' IS ''<' || lc_vName || '> ' || in_vComment || '''';
    execute immediate 'insert into debug (message) values (''' || replace(lc_vSql,'''','''''') || ''')';
    execute immediate lc_vSql;
  end if;

  return '1';

exception
  when others then
    execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' dml error: ' || sqlerrm || ''')';
    return '{"error":"' || sqlerrm || '"}';
end;

function getColumnLength (in_jJson json) return varchar2 as
  /* template variables */
  json_error EXCEPTION;
  spiders_error EXCEPTION;
  lc_vErrorDescription varchar2(255) := 'default';
  lc_vFunctionName varchar2(255) := 'f_parse_json_table_column_pk';
  lc_vReturn varchar2(32000) := '0'; --defaul the return to empty string
  lc_nContinue number :=0; --default to 0 to dissalow continuing

  /* function specific variables */
  lc_jJson json;
  lc_jvValue json_value;
  lc_jlColumns json_list;
  lc_nColumnsCount number := 0;

begin

  lc_jJson := in_jJson;

  if(lc_jJson.exist('columns')) then
    if(lc_jJson.get('columns').is_array) then
      lc_jlColumns := json_list(lc_jJson.get('columns'));
      lc_vReturn := lc_jlColumns.count;
    else
      lc_vErrorDescription := 'columns attribute is not an array';
      raise spiders_error;
    end if;
  else
    lc_vErrorDescription := 'columns attribute not found in json';
    raise spiders_error;
  end if;

  return lc_vReturn;

exception
  when spiders_error then
      execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' SPIDERS error ' || lc_vErrorDescription || ''')';
      return '{"error":"' || lc_vErrorDescription || '"}';
  when json_error then
      execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' JSON error'')';
      return '{"error":"json"}';
  when others then
      execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' dml error: ' || sqlerrm || ''')';
      return '{"error":"' || sqlerrm || '"}';
end;

function f_build_columns_sql_segment (in_jJson json, in_vColumnsCount varchar2) return varchar2 as
  /* template variables */
  json_error EXCEPTION;
  spiders_error EXCEPTION;
  lc_vErrorDescription varchar2(255) := 'default';
  lc_vFunctionName varchar2(255) := 'f_build_columns_sql_segment';
  lc_vPrefix varchar2(20) := 'SPD_T_'; --all tables have this prefix before the name
  lc_vReturn varchar2(32000) := ''; --defaul the return to empty string
  lc_nContinue number :=0; --default to 0 to dissalow continuing

  /* function specific variables */
  lc_vSqlSegment varchar2(32000) := '';
  lc_vName varchar2(255);
  lc_vType varchar2(255);
begin


  for rec in 1 .. in_vColumnsCount loop
    lc_vName := json_ext.get_string(in_jJson, 'columns[' || rec || '].name');
    lc_vType := json_ext.get_string(in_jJson, 'columns[' || rec || '].type');
    if rec = 1 then
      lc_vSqlSegment := lc_vName || ' ' || lc_vType || ',';
    else
      lc_vSqlSegment := lc_vSqlSegment || lc_vName || ' ' || lc_vType || ',';
      if rec = in_vColumnsCount then
        lc_vSqlSegment := lc_vSqlSegment || ' CREATED_BY VARCHAR2(255),';
        lc_vSqlSegment := lc_vSqlSegment || ' CREATED_TIMESTAMP DATE,';
        lc_vSqlSegment := lc_vSqlSegment || ' UPDATED_BY VARCHAR2(255),';
        lc_vSqlSegment := lc_vSqlSegment || ' UPDATED_TIMESTAMP DATE,';
      end if;
    end if;
  end loop;

  lc_vReturn := lc_vSqlSegment;
  return lc_vReturn;

exception
  when spiders_error then
      execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' SPIDERS error ' || lc_vErrorDescription || ''')';
      return '{"error":"' || lc_vErrorDescription || '"}';
  when json_error then
      execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' JSON error'')';
      return '{"error":"json"}';
  when others then
      execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' dml error: ' || sqlerrm || ''')';
      return '{"error":"' || sqlerrm || '"}';
end;


function f_build_col_names_sql_segment (in_jJson json, in_vColumnsCount varchar2) return varchar2 as
  /* template variables */
  json_error EXCEPTION;
  spiders_error EXCEPTION;
  lc_vErrorDescription varchar2(255) := 'default';
  lc_vFunctionName varchar2(255) := 'f_build_col_names_sql_segment';
  lc_vPrefix varchar2(20) := 'SPD_T_'; --all tables have this prefix before the name
  lc_vReturn varchar2(32000) := ''; --defaul the return to empty string
  lc_nContinue number :=0; --default to 0 to dissalow continuing

  /* function specific variables */
  lc_vSqlSegment varchar2(32000) := '';
  lc_vName varchar2(255);
  lc_vType varchar2(255);
begin


  for rec in 1 .. in_vColumnsCount loop
    lc_vName := json_ext.get_string(in_jJson, 'columns[' || rec || '].name');
    lc_vType := json_ext.get_string(in_jJson, 'columns[' || rec || '].type');
    if rec = 1 then
      lc_vSqlSegment := lc_vName || ',';
    else
      if rec = in_vColumnsCount then
        lc_vSqlSegment := lc_vSqlSegment || lc_vName;
      else
        lc_vSqlSegment := lc_vSqlSegment || lc_vName || ',';
      end if;
    end if;
  end loop;

  lc_vReturn := lc_vSqlSegment;
  return lc_vReturn;

exception
  when spiders_error then
      execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' SPIDERS error ' || lc_vErrorDescription || ''')';
      return '{"error":"' || lc_vErrorDescription || '"}';
  when json_error then
      execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' JSON error'')';
      return '{"error":"json"}';
  when others then
      execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' dml error: ' || sqlerrm || ''')';
      return '{"error":"' || sqlerrm || '"}';
end;

function f_build_pk_sql_segment (in_vTable varchar2, in_vColumnName varchar2) return varchar2 as
  /* template variables */
  json_error EXCEPTION;
  spiders_error EXCEPTION;
  lc_vErrorDescription varchar2(255) := 'default';
  lc_vFunctionName varchar2(255) := 'f_build_pk_sql_segment';
  lc_vPrefix varchar2(20) := 'SPD_T_'; --all tables have this prefix before the name
  lc_vReturn varchar2(32000) := ''; --defaul the return to empty string
  lc_nContinue number :=0; --default to 0 to dissalow continuing

  /* function specific variables */

begin

  lc_vReturn := ' CONSTRAINT ' ||  lc_vPrefix || in_vTable || '_PK PRIMARY KEY
  (
    ' ||  in_vColumnName || '
  )
  ENABLE';

  return lc_vReturn;

exception
  when spiders_error then
      execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' SPIDERS error ' || lc_vErrorDescription || ''')';
      return '{"error":"' || lc_vErrorDescription || '"}';
  when json_error then
      execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' JSON error'')';
      return '{"error":"json"}';
  when others then
      execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' dml error: ' || sqlerrm || ''')';
      return '{"error":"' || sqlerrm || '"}';
end;

function getPrimaryKeyColumn (in_jJson json) return varchar2 as
  /* template variables */
  json_error EXCEPTION;
  spiders_error EXCEPTION;
  lc_vErrorDescription varchar2(255) := 'default';
  lc_vFunctionName varchar2(255) := 'getPrimaryKeyColumn';
  lc_vReturn varchar2(32000) := ''; --defaul the return to empty string
  lc_nContinue number :=0; --default to 0 to dissalow continuing


  /* function specific variables */
  lc_jlColumns json_list;
  lc_vColumnsCount varchar2(255);
  lc_vName varchar2(31) := '';
  lc_vPrimaryKey varchar2(31) :='';
  lc_jJson json;
  lc_jvValue json_value;
begin

  lc_jJson := in_jJson;
  lc_vColumnsCount := getColumnLength(lc_jJson);
  lc_nContinue := f_test_for_error(lc_vColumnsCount);

  if lc_nContinue > 0 then
    /*for now we only expect a single column primary key*/
    for rec in 1 .. lc_vColumnsCount loop
      lc_vName := json_ext.get_string(lc_jJson, 'columns[' || rec || '].name');
      lc_vPrimaryKey := json_ext.get_string(lc_jJson, 'columns[' || rec || '].primaryKey');
      EXIT WHEN length(lc_vPrimaryKey) > 0;
    end loop;

    lc_vReturn := lc_vName;
  else
    null;
  end if;

  return lc_vReturn;

exception
  when spiders_error then
      execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' SPIDERS error ' || lc_vErrorDescription || ''')';
      return '{"error":"' || lc_vErrorDescription || '"}';
  when json_error then
      execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' JSON error'')';
      return '{"error":"json"}';
  when others then
      execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' dml error: ' || sqlerrm || ''')';
      return '{"error":"' || sqlerrm || '"}';
end;

function f_parse_json_table_column_pk (in_jJson json) return varchar2 as
  /* template variables */
  json_error EXCEPTION;
  spiders_error EXCEPTION;
  lc_vErrorDescription varchar2(255) := 'default';
  lc_vFunctionName varchar2(255) := 'f_parse_json_table_column_pk';
  lc_vReturn varchar2(32000) := ''; --defaul the return to empty string
  lc_nContinue number :=0; --default to 0 to dissalow continuing


  /* function specific variables */
  lc_vName varchar2(2000);
  lc_vSqlSegment varchar2(2000);
  lc_vColumnsCount varchar2(255);
  lc_jJson json;
  lc_jvValue json_value;

begin

  lc_jJson := in_jJson;
  lc_vColumnsCount := getColumnLength(lc_jJson);
  lc_nContinue := f_test_for_error(lc_vColumnsCount);

  if lc_nContinue > 0 then
    lc_vName := getPrimaryKeyColumn(lc_jJson);
    lc_nContinue := f_test_for_error(lc_vColumnsCount);
  end if;

  if lc_nContinue >0 then
    lc_vSqlSegment := f_build_pk_sql_segment(f_parse_json_table_name(lc_jJson),lc_vName);
    lc_nContinue := f_test_for_error(lc_vColumnsCount);
  end if;

  if lc_nContinue > 0 then
    lc_vReturn := lc_vSqlSegment;
  end if;

  return lc_vReturn;

exception
  when spiders_error then
      execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' SPIDERS error ' || lc_vErrorDescription || ''')';
      return '{"error":"' || lc_vErrorDescription || '"}';
  when json_error then
      execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' JSON error'')';
      return '{"error":"json"}';
  when others then
      execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' dml error: ' || sqlerrm || ''')';
      return '{"error":"' || sqlerrm || '"}';
end;


function f_create_table (in_vSql varchar2) return varchar2 as
  /* template variables */
  json_error EXCEPTION;
  spiders_error EXCEPTION;
  lc_vErrorDescription varchar2(255) := 'default';
  lc_vFunctionName varchar2(255) := 'f_create_table';
  lc_vReturn varchar2(32000) := '1'; --default the return to 1, assume a clean run of this dml code
  lc_nContinue number :=0; --default to 0 to dissalow continuing
  lc_jvValue json_value;

  /* function specific variables */
  lc_vSqlSegment varchar2(32000) := '';

begin

  execute immediate 'insert into debug (message) values (''' || in_vSql || ''')';
  execute immediate in_vSql;

  return lc_vReturn;

exception
  when spiders_error then
      execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' SPIDERS error ' || lc_vErrorDescription || ''')';
      return '{"error":"' || lc_vErrorDescription || '"}';
  when json_error then
      execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' JSON error'')';
      return '{"error":"json"}';
  when others then
      execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' dml error: ' || sqlerrm || ''')';
      return '{"error":"' || sqlerrm || '"}';
end;

function f_build_create_table_sql (in_vTableName varchar2, in_vSqlSegmentCol varchar2, in_vSqlSegmentPk varchar2) return varchar2 as
  /* template variables */
  json_error EXCEPTION;
  spiders_error EXCEPTION;
  lc_vErrorDescription varchar2(255) := 'default';
  lc_vPrefix varchar2(20) := 'SPD_T_'; --all tables have this prefix before the name
  lc_vFunctionName varchar2(255) := 'f_build_create_table_sql';
  lc_vReturn varchar2(32000) := '{"error":"default"}'; --defaul the return to e for error
  lc_nContinue number :=0; --default to 0 to dissalow continuing
  lc_jvValue json_value;

  /* function specific variables */
  lc_vSqlSegment varchar2(32000) := '';

begin

  lc_vSqlSegment := 'CREATE TABLE ' || lc_vPrefix || in_vTableName || '
    (
      ' || in_vSqlSegmentCol || in_vSqlSegmentPk || '
    )';

  lc_vReturn := f_create_table(lc_vSqlSegment);

  return lc_vReturn;

exception
  when spiders_error then
      execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' SPIDERS error ' || lc_vErrorDescription || ''')';
      return '{"error":"' || lc_vErrorDescription || '"}';
  when json_error then
      execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' JSON error'')';
      return '{"error":"json"}';
  when others then
      execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' dml error: ' || sqlerrm || ''')';
      return '{"error":"' || sqlerrm || '"}';
end;


function f_parse_json_table_columns (in_jJson json) return varchar2 as
  /* template variables */
  json_error EXCEPTION;
  spiders_error EXCEPTION;
  lc_vErrorDescription varchar2(255) := 'default';
  lc_vFunctionName varchar2(255) := 'f_parse_json_table_columns';
  lc_vReturn varchar2(32000) := '{"error":"default"}'; --defaul the return to e for error
  lc_nContinue number :=0; --default to 0 to dissalow continuing


  /* function specific variables */
  lc_vSqlSegmentCol varchar2(32000) := '';
  lc_vSqlSegmentPk varchar2(32000) := '';
  lc_vSqlSegment varchar2(32000) := '';
  lc_vColumnsCount varchar2(255);
  lc_jJson json;

begin

  lc_jJson := in_jJson;
  lc_vColumnsCount := getColumnLength(lc_jJson);
  lc_nContinue := f_test_for_error(lc_vColumnsCount);

  if lc_nContinue > 0 then
    lc_vSqlSegmentCol := f_build_columns_sql_segment(lc_jJson, lc_vColumnsCount);
    lc_nContinue := f_test_for_error(lc_vSqlSegment);
  end if;

  if lc_nContinue > 0 then
    lc_vSqlSegmentPk := f_parse_json_table_column_pk(lc_jJson);
    lc_nContinue := f_test_for_error(lc_vSqlSegment);
  end if;

  if lc_nContinue > 0 then
    lc_vSqlSegment := f_build_create_table_sql(f_parse_json_table_name(lc_jJson), lc_vSqlSegmentCol, lc_vSqlSegmentPk);
    lc_nContinue := f_test_for_error(lc_vSqlSegment);
  end if;

  lc_vReturn := lc_vSqlSegment;


  return lc_vReturn;

exception
  when spiders_error then
      execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' SPIDERS error ' || lc_vErrorDescription || ''')';
      return '{"error":"' || lc_vErrorDescription || '"}';
  when json_error then
      execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' JSON error'')';
      return '{"error":"json"}';
  when others then
      execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' dml error: ' || sqlerrm || ''')';
      return '{"error":"' || sqlerrm || '"}';
end;



function f_parse_json_table_comment (in_jJson json) return varchar2 as
  /* template variables */
  json_error EXCEPTION;
  spiders_error EXCEPTION;
  lc_vErrorDescription varchar2(255) := 'default';
  lc_vFunctionName varchar2(255) := 'f_parse_json_table_comment';
  lc_vReturn varchar2(32000) := '{"error":"default"}'; --defaul the return to e for error
  lc_nContinue number :=0; --default to 0 to dissalow continuing
  lc_jvValue json_value;

  /* function specific variables */
begin

  if(in_jJson.exist('comment')) then
    lc_jvValue := in_jJson.get('comment');
    lc_vReturn := getVal(lc_jvValue);
  else
    lc_vErrorDescription := 'comment attribute not found in json';
    raise spiders_error;
  end if;

  return lc_vReturn;

exception
  when spiders_error then
      execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' SPIDERS error ' || lc_vErrorDescription || ''')';
      return '{"error":"' || lc_vErrorDescription || '"}';
  when json_error then
      execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' JSON error'')';
      return '{"error":"json"}';
  when others then
      execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' dml error: ' || sqlerrm || ''')';
      return '{"error":"' || sqlerrm || '"}';
end;

function f_parse_json_table_name (in_jJson json) return varchar2 as
  /* template variables */
  json_error EXCEPTION;
  spiders_error EXCEPTION;
  lc_vErrorDescription varchar2(255) := 'default';
  lc_vFunctionName varchar2(255) := 'f_parse_json_table_name';
  lc_vReturn varchar2(32000) := '{"error":"default"}'; --defaul the return to e for error
  lc_nContinue number :=0; --default to 0 to dissalow continuing
  lc_jvValue json_value;

  /* function specific variables */
begin

  if(in_jJson.exist('collectionName')) then
    lc_jvValue := in_jJson.get('collectionName');
    lc_vReturn := getVal(lc_jvValue);
  else
    lc_vErrorDescription := 'collectionName attribute not found in json';
    raise spiders_error;
  end if;

  return lc_vReturn;

exception
  when spiders_error then
      execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' SPIDERS error ' || lc_vErrorDescription || ''')';
      return '{"error":"' || lc_vErrorDescription || '"}';
  when json_error then
      execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' JSON error'')';
      return '{"error":"json"}';
  when others then
      execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' dml error: ' || sqlerrm || ''')';
      return '{"error":"' || sqlerrm || '"}';
end;


function f_test_for_error (in_vReturned varchar2) return varchar2 as
  /* template variables */
  lc_vFunctionName varchar2(255) := 'f_test_for_error';
  lc_vReturn varchar2(32000) := '{"error":"default"}'; --defaul the return to e for error
  lc_nContinue number :=0; --default to 0 to dissalow continuing

  /* function specific variables */
  lc_vTest varchar2(32000) := in_vReturned;
begin

  if substr(lc_vTest, 1, 9) = '{"error":' then
    lc_vReturn := '0';
  else
    lc_vReturn := '1';
  end if;

  return lc_vReturn;

exception
  when others then
      execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' dml error: ' || sqlerrm || ''')';
      return '{"error":"' || sqlerrm || '"}';
end;


function f_build_model_view_sql (in_vName varchar2, in_vPrimaryKey varchar2, in_vColumns varchar2) return varchar2 as
  /* template variables */
  lc_vFunctionName varchar2(255) := 'f_build_model_view_sql';
  lc_vViewPrefix varchar2(255) := 'SPD_V_M_';
  lc_vTablePrefix varchar2(255) := 'SPD_T_';
  lc_vReturn varchar2(32000) := '{"error":"default"}'; --defaul the return to e for error
  lc_nContinue number :=0; --default to 0 to dissalow continuing

  /* function specific variables */
  lc_vSql varchar2(32000);
  lc_vCreate varchar2(2000);
  lc_vSelect long;
  lc_vFrom varchar2(2000);
begin

   lc_vSql := 'CREATE OR REPLACE FORCE VIEW
    ' || lc_vViewPrefix || in_vName || '
      (
       ' || in_vColumns || '
      )
  AS
    select
      ' || in_vColumns || '
    from
    ' || lc_vTablePrefix || in_vName;

  lc_vReturn := lc_vSql;

  return lc_vReturn;

exception
  when others then
      execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' dml error: ' || sqlerrm || ''')';
      return '{"error":"' || sqlerrm || '"}';
end;



function f_create_model_view (in_jTable json) return varchar2 as
  /* template variables */
  json_error EXCEPTION;
  spiders_error EXCEPTION;
  lc_vErrorDescription varchar2(255) := 'default';
  lc_vFunctionName varchar2(255) := 'getPrimaryKeyColumn';
  lc_vReturn varchar2(32000) := ''; --defaul the return to empty string
  lc_nContinue number :=0; --default to 0 to dissalow continuing


  /* function specific variables */
  lc_jlColumns json_list;
  lc_vColumnsCount varchar2(255);
  lc_vName varchar2(31) := '';
  lc_vPrimaryKey varchar2(31) :='';
  lc_vColumns long;
  lc_vSql long;
  lc_jJson json;
  lc_jvValue json_value;
begin

  lc_vName := f_parse_json_table_name(in_jTable);
  lc_nContinue := f_test_for_error(lc_vName);

  if lc_nContinue = 1 then
    lc_vPrimaryKey := getPrimaryKeyColumn (in_jTable);
    lc_nContinue := f_test_for_error(lc_vPrimaryKey);
  else
    lc_vErrorDescription := 'name attribute not found in json';
    raise spiders_error;
  end if;

  if lc_nContinue = 1 then
    lc_vColumnsCount := getColumnLength (in_jTable);
    lc_nContinue := f_test_for_error(lc_vColumnsCount);
  else
    lc_vErrorDescription := 'primary key attribute not found in json';
    raise spiders_error;
  end if;

  if lc_nContinue = 1 then
    lc_vColumns := f_build_col_names_sql_segment (in_jTable, lc_vColumnsCount);
    lc_nContinue := f_test_for_error(lc_vColumns);
  else
    lc_vErrorDescription := 'error getting column count';
    raise spiders_error;
  end if;

  if lc_nContinue = 1 then
    lc_vSql := f_build_model_view_sql (lc_vName, lc_vPrimaryKey, lc_vColumns);
    lc_nContinue := f_test_for_error(lc_vColumns);
  else
    lc_vErrorDescription := 'error getting column names segment';
    raise spiders_error;
  end if;

  if lc_nContinue = 1 then
    execute immediate lc_vSql;
  else
    lc_vErrorDescription := 'error making the sql';
    raise spiders_error;
  end if;

  return lc_vReturn;

exception
  when spiders_error then
      execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' SPIDERS error ' || lc_vErrorDescription || ''')';
      return '{"error":"' || lc_vErrorDescription || '"}';
  when json_error then
      execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' JSON error'')';
      return '{"error":"json"}';
  when others then
      execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' dml error: ' || sqlerrm || ''')';
      return '{"error":"' || sqlerrm || '"}';
end;

function f_build_data_table_view_sql (in_vName varchar2) return varchar2 as
  /* template variables */
  lc_vFunctionName varchar2(255) := 'f_build_data_table_view_sql';
  lc_vViewPrefix varchar2(255) := 'SPD_V_DT_';
  lc_vTablePrefix varchar2(255) := 'SPD_T_';
  lc_vReturn varchar2(32000) := '{"error":"default"}'; --defaul the return to e for error
  lc_nContinue number :=0; --default to 0 to dissalow continuing

  /* function specific variables */
  lc_vSql varchar2(32000);
  lc_vCreate varchar2(2000);
  lc_vSelect long;
  lc_vFrom varchar2(2000);
begin

   lc_vSql := 'CREATE OR REPLACE FORCE VIEW
    ' || lc_vViewPrefix || in_vName || '
      (
       "changeMe"
      )
  AS
    select
      ''changeMe''
    from
    ' || lc_vTablePrefix || in_vName;

  lc_vReturn := lc_vSql;

  return lc_vReturn;

exception
  when others then
      execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' dml error: ' || sqlerrm || ''')';
      return '{"error":"' || sqlerrm || '"}';
end;

function f_create_data_table_view (in_vName varchar2) return varchar2 as
  /* template variables */
  json_error EXCEPTION;
  spiders_error EXCEPTION;
  lc_vErrorDescription varchar2(255) := 'default';
  lc_vFunctionName varchar2(255) := 'f_create_data_table_view';
  lc_vReturn varchar2(32000) := ''; --defaul the return to empty string
  lc_nContinue number :=0; --default to 0 to dissalow continuing


  /* function specific variables */
  lc_jlColumns json_list;
  lc_vColumnsCount varchar2(255);
  lc_vName varchar2(31) := '';
  lc_vPrimaryKey varchar2(31) :='';
  lc_vColumns long;
  lc_vSql long;
  lc_jJson json;
  lc_jvValue json_value;
begin

  lc_vSql := f_build_data_table_view_sql (in_vName);
  lc_nContinue := f_test_for_error(lc_vSql);

  if lc_nContinue = 1 then
    execute immediate lc_vSql;
  else
    lc_vErrorDescription := 'error making the sql';
    raise spiders_error;
  end if;

  return lc_vReturn;

exception
  when spiders_error then
      execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' SPIDERS error ' || lc_vErrorDescription || ''')';
      return '{"error":"' || lc_vErrorDescription || '"}';
  when json_error then
      execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' JSON error'')';
      return '{"error":"json"}';
  when others then
      execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' dml error: ' || sqlerrm || ''')';
      return '{"error":"' || sqlerrm || '"}';
end;



function makeOrderByRecords (in_vTableName varchar2, in_jJson json) return varchar2 as
  /* template variables */
  json_error EXCEPTION;
  spiders_error EXCEPTION;
  lc_vErrorDescription varchar2(255) := 'default';
  lc_vFunctionName varchar2(255) := 'makeOrderByRecords';
  lc_vReturn varchar2(32000) := '{"error":"default"}'; --defaul the return to e for error
  lc_nContinue number :=0; --default to 0 to dissalow continuing
  lc_jvValue json_value;

  /* function specific variables */
  lc_jlData json_list;
  lc_jlOrderItem json_list;
  lc_vColumnName varchar2(255);
  lc_vSortDirection varchar2(255);
  lc_vSequence varchar2(255);
  lc_vSql long;
  lc_jlDML json_list;
  lc_jResults json;
  lc_jMessages json;
  lc_vDMLResult varchar2(255);
begin

  lc_jlDML := json_list();
  lc_vSql := 'delete from SPD_T_COLLECTION_ORDERS where object_name = ''' || in_vTableName || '''';
  lc_jlDML.append(lc_vSql);

  if(in_jJson.exist('order')) then
      if(in_jJson.get('order').is_array) then
          lc_jlData := json_list(in_jJson.get('order'));
          for rec in 1..lc_jlData.count loop
            lc_jlOrderItem := json_list(lc_jlData.get(rec));
            lc_vColumnName := getVal(lc_jlOrderItem.get(1));
            lc_vSortDirection := getVal(lc_jlOrderItem.get(2));
            lc_vSequence := rec;
            lc_vSql := 'INSERT INTO SPD_T_COLLECTION_ORDERS (OBJECT_NAME,ORDER_BY_COLUMN,DIRECTION,SEQUENCE) values (''' || in_vTableName || ''',''' || lc_vColumnName || ''',''' || lc_vSortDirection || ''',''' || lc_vSequence || ''')';
            lc_jlDML.append(lc_vSql);
          end loop;
      end if;
  end if;


  lc_jResults := json();
  lc_jResults.put('results',lc_jlDML);
  lc_jMessages := json();
  lc_vDMLResult := BB_F_DML(lc_jResults, lc_jMessages);
  lc_vReturn := lc_vDMLResult;
  return lc_vReturn;

exception
  when spiders_error then
      execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' SPIDERS error ' || lc_vErrorDescription || ''')';
      return '{"error":"' || lc_vErrorDescription || '"}';
  when json_error then
      execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' JSON error'')';
      return '{"error":"json"}';
  when others then
      execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' dml error: ' || sqlerrm || ''')';
      return '{"error":"' || sqlerrm || '"}';
end;


function f_parse_json_module (in_jJson json) return varchar2 as
  /* template variables */
  json_error EXCEPTION;
  spiders_error EXCEPTION;
  lc_vErrorDescription varchar2(255) := 'default';
  lc_vFunctionName varchar2(255) := 'f_parse_json_module';
  lc_vReturn varchar2(32000) := '{"error":"default"}'; --defaul the return to e for error
  lc_nContinue number :=0; --default to 0 to dissalow continuing
  lc_jvValue json_value;

  /* function specific variables */
begin

  if(in_jJson.exist('module')) then
    lc_jvValue := in_jJson.get('module');
    lc_vReturn := getVal(lc_jvValue);
  else
    lc_vErrorDescription := 'module attribute not found in json';
    raise spiders_error;
  end if;

  return lc_vReturn;

exception
  when spiders_error then
      execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' SPIDERS error ' || lc_vErrorDescription || ''')';
      return '{"error":"' || lc_vErrorDescription || '"}';
  when json_error then
      execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' JSON error'')';
      return '{"error":"json"}';
  when others then
      execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' dml error: ' || sqlerrm || ''')';
      return '{"error":"' || sqlerrm || '"}';
end;


function f_parse_json_model_name (in_jJson json) return varchar2 as
  /* template variables */
  json_error EXCEPTION;
  spiders_error EXCEPTION;
  lc_vErrorDescription varchar2(255) := 'default';
  lc_vFunctionName varchar2(255) := 'f_parse_json_model_name';
  lc_vReturn varchar2(32000) := '{"error":"default"}'; --defaul the return to e for error
  lc_nContinue number :=0; --default to 0 to dissalow continuing
  lc_jvValue json_value;

  /* function specific variables */
begin

  if(in_jJson.exist('modelName')) then
    lc_jvValue := in_jJson.get('modelName');
    lc_vReturn := getVal(lc_jvValue);
  else
    lc_vErrorDescription := 'modelName attribute not found in json';
    raise spiders_error;
  end if;

  return lc_vReturn;

exception
  when spiders_error then
      execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' SPIDERS error ' || lc_vErrorDescription || ''')';
      return '{"error":"' || lc_vErrorDescription || '"}';
  when json_error then
      execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' JSON error'')';
      return '{"error":"json"}';
  when others then
      execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' dml error: ' || sqlerrm || ''')';
      return '{"error":"' || sqlerrm || '"}';
end;

function f_create_rest_table_maps (in_jJson json, in_vTableName varchar2) return varchar2 as
  /* template variables */
  json_error EXCEPTION;
  spiders_error EXCEPTION;
  lc_vErrorDescription varchar2(255) := 'default';
  lc_vFunctionName varchar2(255) := 'f_create_rest_table_maps';
  lc_vReturn varchar2(32000) := '{"error":"default"}'; --defaul the return to e for error
  lc_nContinue number :=0; --default to 0 to dissalow continuing


  /* function specific variables */
  lc_jvValue json_value;
  lc_vSql long;
  lc_vModule varchar2(255);
  lc_vModelName varchar2(255);
  lc_jResults json;
  lc_jMessages json;
  lc_vDMLResult varchar2(255);
  lc_jlDML json_list;
begin

  lc_jlDML := json_list();

  lc_vModule := f_parse_json_module(in_jJson);
  lc_nContinue := f_test_for_error(lc_vModule);

  if lc_nContinue = 1 then
    lc_vModelName := f_parse_json_model_name(in_jJson);
    lc_nContinue := f_test_for_error(lc_vModelName);
  else
    lc_vErrorDescription := 'collectionName attribute not found in json';
    raise spiders_error;
  end if;

/*
if lc_nContinue = 1 then
    if lc_vModule != 'private' then
      lc_vSql := 'DELETE FROM SPD_T_REST_TO_TABLE_MAPS WHERE REST_ENDPOINT=''CREATEMODEL'', MODULE=''' || lc_vModule || ''', TARGET=''' || lc_vModelName || ''', TABLE_NAME=''' || in_vTableName || '''';
      lc_jlDML.append(lc_vSql);
      lc_vSql := 'DELETE FROM SPD_T_REST_TO_TABLE_MAPS WHERE REST_ENDPOINT=''DELETEMODEL'', MODULE=''' || lc_vModule || ''', TARGET=''' || lc_vModelName || ''', TABLE_NAME=''' || in_vTableName || '''';
      lc_jlDML.append(lc_vSql);
      lc_vSql := 'DELETE FROM SPD_T_REST_TO_TABLE_MAPS WHERE REST_ENDPOINT=''READDATATABLE'', MODULE=''' || lc_vModule || ''', TARGET=''' || lc_vModelName || ''', TABLE_NAME=''' || in_vTableName || '''';
      lc_jlDML.append(lc_vSql);
      lc_vSql := 'DELETE FROM SPD_T_REST_TO_TABLE_MAPS WHERE REST_ENDPOINT=''READMODEL'', MODULE=''' || lc_vModule || ''', TARGET=''' || lc_vModelName || ''', TABLE_NAME=''' || in_vTableName || '''';
      lc_jlDML.append(lc_vSql);
      lc_vSql := 'DELETE FROM SPD_T_REST_TO_TABLE_MAPS WHERE REST_ENDPOINT=''READCOLLECTION'', MODULE=''' || lc_vModule || ''', TARGET=''' || lc_vModelName || ''', TABLE_NAME=''' || in_vTableName || '''';
      lc_jlDML.append(lc_vSql);
      lc_vSql := 'DELETE FROM SPD_T_REST_TO_TABLE_MAPS WHERE REST_ENDPOINT=''UPDATEMODEL'', MODULE=''' || lc_vModule || ''', TARGET=''' || lc_vModelName || ''', TABLE_NAME=''' || in_vTableName || '''';
      lc_jlDML.append(lc_vSql);
    end if;
  else
    lc_vErrorDescription := 'modelName attribute not found in json';
    raise spiders_error;
  end if;
*/

  if lc_nContinue = 1 then
    if lc_vModule != 'private' then
      lc_vSql := 'INSERT INTO SPD_T_REST_TO_TABLE_MAPS (REST_ENDPOINT, MODULE, TARGET, TABLE_NAME) VALUES (''CREATEMODEL'',''' || lc_vModule || ''',''' || lc_vModelName || ''',''' || in_vTableName || ''')';
      lc_jlDML.append(lc_vSql);
      lc_vSql := 'INSERT INTO SPD_T_REST_TO_TABLE_MAPS (REST_ENDPOINT, MODULE, TARGET, TABLE_NAME) VALUES (''DELETEMODEL'',''' || lc_vModule || ''',''' || lc_vModelName || ''',''' || in_vTableName || ''')';
      lc_jlDML.append(lc_vSql);
      lc_vSql := 'INSERT INTO SPD_T_REST_TO_TABLE_MAPS (REST_ENDPOINT, MODULE, TARGET, TABLE_NAME) VALUES (''READDATATABLE'',''' || lc_vModule || ''',''' || in_vTableName || ''',''' || in_vTableName || ''')';
      lc_jlDML.append(lc_vSql);
      lc_vSql := 'INSERT INTO SPD_T_REST_TO_TABLE_MAPS (REST_ENDPOINT, MODULE, TARGET, TABLE_NAME) VALUES (''READMODEL'',''' || lc_vModule || ''',''' || lc_vModelName || ''',''' || in_vTableName || ''')';
      lc_jlDML.append(lc_vSql);
      lc_vSql := 'INSERT INTO SPD_T_REST_TO_TABLE_MAPS (REST_ENDPOINT, MODULE, TARGET, TABLE_NAME) VALUES (''READCOLLECTION'',''' || lc_vModule || ''',''' || in_vTableName || ''',''' || in_vTableName || ''')';
      lc_jlDML.append(lc_vSql);
      lc_vSql := 'INSERT INTO SPD_T_REST_TO_TABLE_MAPS (REST_ENDPOINT, MODULE, TARGET, TABLE_NAME) VALUES (''UPDATEMODEL'',''' || lc_vModule || ''',''' || lc_vModelName || ''',''' || in_vTableName || ''')';
      lc_jlDML.append(lc_vSql);
    end if;
  else
    lc_vErrorDescription := 'modelName attribute not found in json';
    raise spiders_error;
  end if;


  lc_jResults := json();
  lc_jResults.put('results',lc_jlDML);
  lc_jMessages := json();
  lc_vDMLResult := BB_F_DML(lc_jResults, lc_jMessages);
  lc_vReturn := lc_vDMLResult;

  return lc_vReturn;

exception
  when spiders_error then
      execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' SPIDERS error ' || lc_vErrorDescription || ''')';
      return '{"error":"' || lc_vErrorDescription || '"}';
  when json_error then
      execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' JSON error'')';
      return '{"error":"json"}';
  when others then
      execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' dml error: ' || sqlerrm || ''')';
      return '{"error":"' || sqlerrm || '"}';
end;


function f_make_all_table_objects( in_jTable json) return varchar2 as
  /* template variables */
  json_error EXCEPTION;
  spiders_error EXCEPTION;
  lc_vErrorDescription varchar2(255) := 'default';
  lc_vFunctionName varchar2(255) := 'f_make_all_table_objects';
  lc_vReturn varchar2(32000) := '{"error":"default"}'; --defaul the return to e for error
  lc_nContinue number :=0; --default to 0 to dissalow continuing

  /* function specific variables */
  lc_vName varchar2(2000);
  lc_vTemp varchar2(2000);
  lc_vComment varchar2(2000);
begin

  /* get name of object */
  lc_vName := f_parse_json_table_name(in_jTable);
  lc_nContinue := f_test_for_error(lc_vName);

  if lc_nContinue = 1 then
    /* drop sequence */
    lc_vTemp := f_drop_sequence(lc_vName);
    lc_nContinue := f_test_for_error(lc_vTemp);
  else
    lc_vErrorDescription := 'name attribute not found in json';
    raise spiders_error;
  end if;

  if lc_nContinue = 1 then
    /* drop trigger */
    lc_vTemp := f_drop_insert_trigger(lc_vName);
    lc_nContinue := f_test_for_error(lc_vTemp);
  else
    lc_vErrorDescription := 'error during drop sequence operation';
    raise spiders_error;
  end if;

  if lc_nContinue = 1 then
    /* drop trigger */
    lc_vTemp := f_drop_update_trigger(lc_vName);
    lc_nContinue := f_test_for_error(lc_vTemp);
  else
    lc_vErrorDescription := 'error during drop insert trigger operation';
    raise spiders_error;
  end if;

  if lc_nContinue = 1 then
    /* drop table */
    lc_vTemp := f_drop_table(lc_vName);
    lc_nContinue := f_test_for_error(lc_vTemp);
  else
    lc_vErrorDescription := 'error during drop update trigger operation';
    raise spiders_error;
  end if;

  if lc_nContinue = 1 then
    /* create sequence */
    lc_vTemp := f_create_sequence(lc_vName);
    lc_nContinue := f_test_for_error(lc_vTemp);
  else
    lc_vErrorDescription := 'error during drop table operation';
    raise spiders_error;
  end if;

  if lc_nContinue = 1 then
    /* create table */
    lc_vTemp := f_parse_json_table_columns(in_jTable);
    lc_nContinue := f_test_for_error(lc_vTemp);
  else
    lc_vErrorDescription := 'error during create sequence operation';
    raise spiders_error;
  end if;

  /*get the table comment */
  lc_vComment:= f_parse_json_table_comment(in_jTable);
  lc_nContinue := f_test_for_error(lc_vComment);

  if lc_nContinue = 1 then
    /* create table comment */
    lc_vTemp := f_create_table_comment(lc_vName, lc_vComment);
    lc_nContinue := f_test_for_error(lc_vTemp);
  else
    lc_vErrorDescription := 'error during create table operation';
    raise spiders_error;
  end if;

  if lc_nContinue = 1 then
    /* create insert trigger */
    lc_vTemp := f_create_insert_trigger(lc_vName, getPrimaryKeyColumn(in_jTable));
    lc_nContinue := f_test_for_error(lc_vTemp);
  else
    lc_vErrorDescription := 'error during create table comment operation';
    raise spiders_error;
  end if;

  if lc_nContinue = 1 then
    /* alter trigger */
    lc_vTemp := f_create_alter_insert_trigger(lc_vName);
    lc_nContinue := f_test_for_error(lc_vTemp);
  else
    lc_vErrorDescription := 'error during create insert trigger operation';
    raise spiders_error;
  end if;

  if lc_nContinue = 1 then
    /* create update trigger */
    lc_vTemp := f_create_update_trigger(lc_vName);
    lc_nContinue := f_test_for_error(lc_vTemp);
  else
    lc_vErrorDescription := 'error during create alter insert trigger operation';
    raise spiders_error;
  end if;

  if lc_nContinue = 1 then
    /* alter trigger */
    lc_vTemp := f_create_alter_update_trigger(lc_vName);
    lc_nContinue := f_test_for_error(lc_vTemp);
  else
    lc_vErrorDescription := 'error during create update trigger operation';
    raise spiders_error;
  end if;

  if lc_nContinue = 1 then
    /* check valid objects */
    lc_vTemp := f_create_model_view(in_jTable);
    htp.p(lc_vTemp);
    lc_nContinue := f_test_for_error(lc_vTemp);
  else
    lc_vErrorDescription := 'error during alter update trigger operation';
    raise spiders_error;
  end if;

  if lc_nContinue = 1 then
    /* check valid objects */
    lc_vTemp := f_create_data_table_view(lc_vName);
    lc_nContinue := f_test_for_error(lc_vTemp);
  else
    lc_vErrorDescription := 'error during create model view operation';
    raise spiders_error;
  end if;

  if lc_nContinue = 1 then
    lc_vTemp := f_create_rest_table_maps(in_jTable, lc_vName);
    lc_nContinue := f_test_for_error(lc_vTemp);
  else
    lc_vErrorDescription := 'error during create data table view operation';
    raise spiders_error;
  end if;

  if lc_nContinue = 1 then
    /* check valid objects */
    lc_vTemp := makeOrderByRecords(lc_vName,in_jTable);
    lc_nContinue := f_test_for_error(lc_vTemp);
  else
    lc_vErrorDescription := 'error during create trigger operation';
    raise spiders_error;
  end if;

  if lc_nContinue = 1 then
    /* check valid objects */
    lc_vTemp := getValidObjectCount(lc_vName);
    lc_nContinue := f_test_for_error(lc_vTemp);
  else
    lc_vErrorDescription := 'error making order by records';
    raise spiders_error;
  end if;

  lc_vReturn := lc_vName || ':' || lc_vTemp || ' valid objects total';
  return lc_vReturn;

exception
  when spiders_error then
      execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' SPIDERS error ' || lc_vErrorDescription || ''')';
      return '{"error":"' || lc_vErrorDescription || '"}';
  when json_error then
      execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' JSON error'')';
      return '{"error":"json"}';
  when others then
      execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' dml error: ' || sqlerrm || ''')';
      return '{"error":"' || sqlerrm || '"}';
end;


------------------------
-- TESTING PROCEDURES --
------------------------

-- COMMON TESTING FUNCTIONS --
-- Should be in a different package
-- In a common one


  function greatherThan(n1 number,n2 number) return number as
  begin
  	if n1 > n2 then
  		return 1;
  	else
  		return 0;
  	end if;
  end;

 function equalTo(v1 varchar2, v2 varchar2) return number as
  begin
  	if v1 = v2 then
  		return 1;
  	else
  		return 0;
  	end if;
  end;



  -- TESTING FUNCTIONS --

function t_object_exists_s return number as
  lc_oTableName varchar2(31);
  lc_nResult number;

  begin
	select OBJECT_NAME into lc_oTableName from user_objects where OBJECT_TYPE = 'TABLE' AND rownum = 1;
	lc_nResult := f_object_exists(lc_oTableName);
	if greatherThan(lc_nResult,0) = 1 then
		return 1;
	else
		return 0;
	end if;
end;


function t_object_exists_f return number as
  lc_oTableName varchar2(31);
  lc_nResult number;

  begin
	lc_nResult := f_object_exists('BS');
	if greatherThan(lc_nResult,0) = 1 then
		return 0;
	else
		return 1;
	end if;
end;


function t_parse_json_table_name_s return number as
	lc_jJson json;
	lc_vResult varchar2(6);
begin
	lc_jJson := json('{"collectionName":"JGTEST"}');
	lc_vResult := f_parse_json_table_name(lc_jJson);
	if equalTo(lc_vResult,'JGTEST') = 1 then
		return 1;
	else
		return 0;
	end if;
end;






-- PUBLIC TEST FUNCTION --
-- This function will launch all the tests on this package
procedure testAll as
  lc_nSuccessCount number;
  lc_nFailureCount number;

  -- This procedure will capture the success or the failure of a test
  procedure checking(n number) as
	begin
		if n = 1 then
			lc_nSuccessCount := lc_nSuccessCount + 1;
		else
			lc_nFailureCount := lc_nFailureCount + 1;
		end if;
	end;

    begin

    -- Initialization
    lc_nSuccessCount := 0;
    lc_nFailureCount := 0;

	-- Testing !!
    checking(t_object_exists_s());
    checking(t_object_exists_f());
    checking(t_parse_json_table_name_s());

	-- Printing out the results of our tests
    htp.p('Success : '||lc_nSuccessCount);
    htp.p('Failure : '||lc_nFailureCount);
	htp.p('Percentage of Success : '|| 100*(lc_nSuccessCount)/(lc_nSuccessCount+lc_nFailureCount)||' %');

    end;

end;

/
