--------------------------------------------------------
--  DDL for Package Body PKG_COMMIT_MAXIMO_AF_DATA
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE PACKAGE BODY "SPIDERS_DATA"."PKG_COMMIT_MAXIMO_AF_DATA" as 

function f_test_for_error (in_vReturned varchar2) return varchar2 as 
  /* template variables */
  lc_vFunctionName varchar2(255) := 'f_test_for_error';
  lc_vReturn varchar2(32000) := '{"error":"default"}'; --defaul the return to e for error
  lc_nContinue number :=0; --default to 0 to dissalow continuing
  
  /* function specific variables */
  lc_vTest varchar2(32000) := in_vReturned;
begin

  if substr(lc_vTest, 1, 9) = '{"error":' then
    lc_vReturn := '0';
  else
    lc_vReturn := '1';
  end if;
  
  return lc_vReturn;

exception     
  when others then
      execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' dml error: ' || sqlerrm || ''')';
      return '{"error":"' || sqlerrm || '"}';
end; 


function getAttributeValue(in_vAttribute varchar2, in_jJSON json) return varchar2 as 
  /* template variables */
  json_error EXCEPTION;
  spiders_error EXCEPTION;
  lc_vErrorDescription varchar2(255) := 'default';
  lc_vFunctionName varchar2(255) := 'getAttributeValue';
  lc_vReturn varchar2(32000) := '{"error":"default"}'; --defaul the return to e for error
  lc_nContinue number :=0; --default to 0 to dissalow continuing
  
  /* function specific variables */
  lc_jvValue json_value;
begin
  
  if(in_jJSON.exist(in_vAttribute)) then       
    lc_jvValue := in_jJSON.get(in_vAttribute);
    lc_vReturn := getVal(lc_jvValue);
  else
    lc_vErrorDescription := in_vAttribute || ' attribute not found in json';
    raise spiders_error;
  end if;
  
  /* get name of object */
  return lc_vReturn;

exception     
  when spiders_error then
      execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' SPIDERS error ' || lc_vErrorDescription || ''')';
      return '{"error":"' || lc_vErrorDescription || '"}';
  when json_error then
      execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' JSON error'')';
      return '{"error":"json"}';
  when others then
      execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' dml error: ' || sqlerrm || ''')';
      return '{"error":"' || sqlerrm || '"}';
end; 

function f_commit(in_cJSON clob) return json as 
  /* template variables */
  json_error EXCEPTION;
  spiders_error EXCEPTION;
  lc_vErrorDescription varchar2(255) := 'default';
  lc_vFunctionName varchar2(255) := 'f_commit';
  lc_vRest varchar2(255) := 'f_commit';
  lc_jReturn json; 
  lc_nContinue number :=0; --default to 0 to dissalow continuing
  
  /* function specific variables */
  lc_vModule varchar2(255);
  lc_jParameterJson json;
  lc_jlDML json_list;
  lc_jResults json;
  lc_jMessages json;
  lc_vDMLResult varchar2(255);
  lc_vSql long;
  lc_dNow date;
begin

  lc_jlDML := json_list();
  lc_dNow := systimestamp;
  
  lc_vSql := '
  insert into spd_t_maximo_af_assets ( 
    NETWORK_ID,
    BRANCH_NAME,
    BRANCH_USE,
    BRANCH_ID,
    SECTION_ID,
    SURFACE_TYPE,
    TRUE_AREA,
    LAST_INSPECTION_DATE,
    PCI,
    ASSET_NUMBER,
    UNIFORMAT_SUBSYSTEM,
    CHANGE_DESCRIPTION,
    LEGACY_ASSET_NUMBER,
    SITE_CODE,
    FACILITY_ID,
    CAT_CODE_NUMBER,
    REPORT_NUMBER,
    REPORT_YEAR,
    REPORT_LOCATION,
    CREATED_BY,
    CREATED_TIMESTAMP,
    UPDATED_BY,
    UPDATED_TIMESTAMP)
  select
    NETWORK_ID,
    BRANCH_NAME,
    BRANCH_USE,
    BRANCH_ID,
    SECTION_ID,
    SURFACE_TYPE,
    TRUE_AREA,
    LAST_INSPECTION_DATE,
    PCI,
    ASSET_NUMBER,
    UNIFORMAT_SUBSYSTEM,
    CHANGE_DESCRIPTION,
    LEGACY_ASSET_NUMBER,
    SITE_CODE,
    FACILITY_ID,
    CAT_CODE_NUMBER,
    REPORT_NUMBER,
    REPORT_YEAR,
    REPORT_LOCATION,
    CREATED_BY,
    to_date(''' || to_char(lc_dNow,'YYYYMMDDHH24MISS')  || ''',''YYYYMMDDHH24MISS'') CREATED_TIMESTAMP,
    UPDATED_BY,
    UPDATED_TIMESTAMP
  from
    spd_t_maximo_af_assets_tmp
  where 
    created_by = v(''APP_USER'')';
  lc_jlDML.append(lc_vSql);

  lc_vSql := '
  insert into spd_t_maximo_af_srs (
    BRANCH_ID, 
    SECTION_ID, 
    MAXIMO_ASSET_NUMBER, 
    PAVEMENT_OBSERVATION, 
    PLANNED_FISCAL_YEAR, 
    TYPE_OF_M_AND_R, 
    ACTION_REQUIRED, 
    PREACTION_PCI, 
    POSTACTION_PCI, 
    COST, 
    REPORT_NUMBER,
    REPORT_YEAR,
    REPORT_LOCATION,
    CREATED_BY, 
    CREATED_TIMESTAMP, 
    UPDATED_BY, 
    UPDATED_TIMESTAMP)
  select 
    BRANCH_ID, 
    SECTION_ID,                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                     
    MAXIMO_ASSET_NUMBER, 
    PAVEMENT_OBSERVATION, 
    PLANNED_FISCAL_YEAR, 
    TYPE_OF_M_AND_R, 
    ACTION_REQUIRED, 
    PREACTION_PCI, 
    POSTACTION_PCI, 
    COST, 
    REPORT_NUMBER,
    REPORT_YEAR,
    REPORT_LOCATION,
    CREATED_BY, 
    to_date(''' || to_char(lc_dNow,'YYYYMMDDHH24MISS')  || ''',''YYYYMMDDHH24MISS'') CREATED_TIMESTAMP, 
    UPDATED_BY, 
    UPDATED_TIMESTAMP
  from
    spd_t_maximo_af_srs_tmp
  where 
    created_by = v(''APP_USER'')';
  lc_jlDML.append(lc_vSql);

  lc_jResults := json();
  lc_jResults.put('results',lc_jlDML);
  lc_jMessages := json();
  lc_vDMLResult := BB_F_DML(lc_jResults, lc_jMessages);
  lc_jReturn := lc_jMessages;
  return lc_jReturn;
  

exception     
  when spiders_error then
      execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' SPIDERS error ' || lc_vErrorDescription || ''')';
      return json('{"error":"' || lc_vFunctionName || ' SPIDERS error ' || lc_vErrorDescription || '"}');
  when json_error then
      execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' JSON error'')';
      return json('{"error":"' || lc_vFunctionName || ' JSON error ' || lc_vErrorDescription || '"}');
  when others then
      execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' dml error: ' || sqlerrm || ''')';
     return json('{"error":"' || lc_vFunctionName || ' dml error ' || sqlerrm || '"}');
end; 

end;

/
