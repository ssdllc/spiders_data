--------------------------------------------------------
--  DDL for Package Body PKG_CALL_ANY_PROC
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE PACKAGE BODY "SPIDERS_DATA"."PKG_CALL_ANY_PROC" as

function f_checkPermission(in_procName varchar2, lc_err out varchar2) return BOOLEAN as
  nRun number :=1;
begin
  insert into spiders_data.debug (input) values (substr(in_procName,0,7));
  for rec in (select * from spiders_data.ssd_security_objects where upper(object_name) = upper(in_procName)) loop
    nRun := 0;
    for rec2 in (select * from spiders_data.ssd_security_users where securitygroupid = rec.securitygroupid and employeeid = v('APP_USER')) loop
    nRun := 1;
    end loop;
  end loop;

  if nRun != 1 then
    lc_err:= 'SPIDERS_DATA.CallAnyProc error - Procedure ('||in_procName||') is not available for User Procedure ('||v('APP_USER')||') ';
    return false;
  else
    return true;
  end if;
end;

function f_procedureExists (in_procName varchar2, lc_err out varchar2) return BOOLEAN as
  lc_nCount number :=0;
begin
  select count(*) into lc_nCount
  from ssd_security_objects
  where upper(object_name) = upper(in_procName);

  if lc_nCount < 1 then
    lc_err:= 'SPIDERS_DATA.CallAnyProc error - Procedure ('||in_procName||') is missing from security objects';
    return false;
  else
    return true;
  end if;
end;

function f_testSchemaPrefix(in_procName varchar2, lc_err out varchar2) return BOOLEAN as
begin
  if substr(in_procName,0,7)!='SPIDERS' then
		--lc_cont:=false;
		lc_err:= 'SPIDERS_DATA.CallAnyProc error - incorrect name for procedure ('||in_procName||')';
    return false;
	else
    return true;
  end if;
end;

function f_checkProcedure(in_procName varchar2, lc_err out varchar2) return BOOLEAN as 
  lc_count number;
begin

  SELECT 
    count(object_name) into lc_count
  FROM 
    ALL_ARGUMENTS
  where
    upper(owner)||'.'||upper(package_name)||'.'||upper(object_name) = upper(in_procName)
    or
    upper(owner)||'.'||upper(object_name) = upper(in_procName);
    
  if lc_count = 1 then
    return true;
  else
    lc_err:= 'SPIDERS_DATA.CallAnyProc error - invalid procedure or param count ('||in_procName||')';
    return false;
  end if;
end;

function sanitizeClob (str2 clob)
  RETURN clob
IS 
  str clob;
BEGIN 
  str := str2;
  str := TRIM(str);
  str := REPLACE(str,'&','');
  str := REPLACE(str,'"','');
  str := REPLACE(str,'''','');
  
  RETURN str;
END;

function f_checkArguments(in_procName varchar2, in_args varchar2,lc_param out clob, lc_err out varchar2) return BOOLEAN as 
  lc_count number;
begin

  SELECT 
    count(object_name) into lc_count
  FROM 
    ALL_ARGUMENTS
  where
    data_type = 'CLOB' AND
    upper(owner)||'.'||upper(package_name)||'.'||upper(object_name) = upper(in_procName)
    or
    upper(owner)||'.'||upper(object_name) = upper(in_procName);
    
  if lc_count = 1 then
    lc_param := sanitizeClob(lc_param);
    return true;
  else
    lc_err:= 'SPIDERS_DATA.CallAnyProc error - invalid param type ('||in_procName||')';
    return false;
  end if;
end;

function f_call_any_proc(in_procName_unsanitized varchar2, in_args_unsanitized varchar2) return varchar2 as
--function f_call_any_proc(in_procName varchar2, in_args varchar2) return varchar2 as

  in_procName  varchar2(4000);
  in_args varchar2(32000);
  nSecurityGroupId number;

  lc_cont BOOLEAN := true;
  lc_err varchar2(256) := 'none';
  lc_param clob;

begin

	--LOG THIS REQUEST
  insert into debug (message) values ('new f_call_any_proc beginning');
	insert into debug (input, message) values (in_procName, in_args);

	--TEST SCHEMA PREFIX
  --lc_cont := f_testSchemaPrefix(in_procName,lc_err);

	--CHECK THAT PROCEDURE NAME EXISTS IN SECURITY OBJECTS
  --if lc_cont then
  --  lc_cont := f_procedureExists(in_procName,lc_err);
  --end if;

	--CHECK THAT USER HAS PERMISSION TO RUN PROCEDURE
  --if lc_cont then
  --  lc_cont := f_checkPermission(in_procName,lc_err);
  --end if;

  --CHECK THAT PROCEDURE EXISTS IN ALL_ARGUMENTS TABLE
  if lc_cont then
    lc_cont := f_checkProcedure(in_procName,lc_err);
  end if;
  
  --CHECK THAT THE PROCEDURE HAS ONE CLOB ARGUMENT AND SET THE BIND PARAM
  if lc_cont then
    lc_cont := f_checkArguments(in_procName,in_args,lc_param,lc_err);
  end if;
  
	--RUN PROCEDURE
	if lc_cont then
		--execute immediate ('BEGIN '||in_procName||' ('||in_args||'); END;');
    insert into debug (message) values ('running '||in_procName||' with param '||lc_param);
		execute immediate 'BEGIN '||in_procName||' (:param); END;' using lc_param;
	end if;

	--LOG AND RETURN ERROR MESSAGE
	if not lc_cont then
		htp.p(lc_err);
		insert into spiders_data.debug (input) values(lc_err);
		return 'OK';
	else
		return 'Not OK';
	end if;

	exception when others then
		htp.p('SPIDERS_DATA.CallAnyProc Error: ' || SQLERRM);
		execute immediate 'insert into spiders_data.debug (input) values(''SPIDERS_DATA.CallAnyProc Exception: ''' || SQLERRM || ''')';
	end;

end;

/
