--------------------------------------------------------
--  DDL for Package Body PKG_PROCESS_MAXIMO
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE PACKAGE BODY "SPIDERS_DATA"."PKG_PROCESS_MAXIMO" AS

  function f_test_for_error (in_vReturned varchar2) return varchar2 AS
      /* template variables */
    lc_vFunctionName varchar2(255) := 'f_test_for_error';
    lc_vReturn varchar2(32000) := '{"error":"default"}'; --defaul the return to e for error
    lc_nContinue number :=0; --default to 0 to dissalow continuing
    
    /* function specific variables */
    lc_vTest varchar2(32000) := in_vReturned;
  BEGIN
      if substr(lc_vTest, 1, 9) = '{"error":' then
      lc_vReturn := '0';
    else
      lc_vReturn := '1';
    end if;
    
    return lc_vReturn;
  
  EXCEPTION    
    when others then
        execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' dml error: ' || sqlerrm || ''')';
        return '{"error":"' || sqlerrm || '"}';
  END f_test_for_error;
  
  function getAttributeValue(in_vAttribute varchar2, in_jJSON json) return varchar2 as 
    /* template variables */
    json_error EXCEPTION;
    spiders_error EXCEPTION;
    lc_vErrorDescription varchar2(255) := 'default';
    lc_vFunctionName varchar2(255) := 'getAttributeValue';
    lc_vReturn varchar2(32000) := '{"error":"default"}'; --defaul the return to e for error
    lc_nContinue number :=0; --default to 0 to dissalow continuing
    
    /* function specific variables */
    lc_jlData json_list;
    lc_jvValue json_value;
    lc_jvRecVal json_value;
    lc_jValue json;
    
    /*EDITED 20170902 MR CLEANUP DEBUG STATEMENTS*/
  begin
    --execute immediate 'insert into debug (message) values (''' || in_vAttribute || ''')';
    lc_jlData := json_list(in_jJson.get('data'));        
    lc_jvRecVal := lc_jlData.get(1); --return null on outofbounds
    lc_jValue := json(lc_jvRecVal);            
    if(lc_jValue.exist(in_vAttribute)) then 
      lc_jvValue := lc_jValue.get(in_vAttribute);
      lc_vReturn := getVal(lc_jvValue);
    else
      lc_vErrorDescription := in_vAttribute || ' attribute not found in json';
      return '{"error":"'||lc_vErrorDescription||'"}';
    end if;
    
    /* get name of object */
    return lc_vReturn;
  
  exception     
    when spiders_error then
        execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' SPIDERS error ' || lc_vErrorDescription || ''')';
        return '{"error":"' || lc_vErrorDescription || '"}';
    when json_error then
        execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' JSON error'')';
        return '{"error":"json"}';
    when others then
        execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' dml error: ' || sqlerrm || ''')';
        return '{"error":"' || sqlerrm || '"}';
  end getAttributeValue; 

  function generate_fac_assets_sql (in_vDeliveryOrderId varchar2) return varchar2 AS
      /* template variables */
    json_error EXCEPTION;
    spiders_error EXCEPTION;
    lc_vErrorDescription varchar2(255) := 'default';
    lc_vFunctionName varchar2(255) := 'generate_fac_assets_sql';
    --lc_cReturn clob; --defaul the return to e for error
    --lc_nContinue number :=0; --default to 0 to dissalow continuing
    
    /* function specific variables */
    lc_vSql long;
    lc_vDelete long;
    lc_jlSQL json_list := json_list();
    lc_jlDelete json_list := json_list();
    lc_cTest clob;
    --l_xXML  xmltype;
  BEGIN
  
    lc_vDelete := 'delete from spd_t_spiders_fac_assets where DELIVERYORDERID = ' || to_number(in_vDeliveryOrderId);
    lc_jlDelete.append(lc_vDelete);
    executeSql(lc_jlDelete);
  
  
    lc_vSql := '
      insert into spd_t_spiders_fac_assets
      (
        SPIDERSID,
        DELIVERYORDERID,
        MAXIMOASSET,
        ASSETNUM,
        CHANGEDATE,
        DESCRIPTION,
        LONG_DESC,
        MASTERSYSTEM,
        SYSTEM,
        SUBSYSTEM,
        LOCATION,
        SITEID,
        WORKCENTER,
        ASSETTYPE,
        QUANTITY,
        INVENTORY,
        PURCHASEPRICE,
        BUDGETCOST,
        REPLACEMENTCOST,
        METERGROUP,
        ORGID
      )
      select unique
        CONCAT(to_char(fac.facilityid), inv.UNIFORMATIICODE) as SPIDERSID, ' ||
        to_number(in_vDeliveryOrderId) || ' as DELIVERYORDERID,
        '''' as MAXIMOASSET,
        CONCAT(loc_nfa.location, SUBSTR(inv.UNIFORMATIICODE, 0, 7)) as ASSETNUM,
        null        as CHANGEDATE,
        assets_loc.description       as DESCRIPTION,
        ''''        as LONG_DESC,
        assets_loc.mastersystem      as MASTERSYSTEM,
        assets_loc.system            as SYSTEM,
        assets_loc.subsystem         as SUBSYSTEM,
        loc_nfa.location          as LOCATION,
        loc_nfa.loc_site            as SITEID,
        ''''       as WORKCENTER,
        ''''       as ASSETTPYE,
        inv.quantityofunits   as QUANTITY,
        '''' as INVENTORY,
        null     as PURCHASEPRICE,
        null       as BUDGETCOST,
        null       as REPLACEMENTCOST,
        ''''       as METERGROUP,
        null            as ORGID
      from
        spd_t_sip_locations_by_nfa loc_nfa,
        SPD_T_SIP_ASSETS_BY_LOC assets_loc,
        spd_t_do_facilities dofac,
        spd_t_xml_actions act,
        spd_t_xml_facilities fac,
        spd_t_xml_asset_summary sum,
        spd_t_xml_asset_inventory inv
      where
        loc_nfa.location = assets_loc.location and
        loc_nfa.facilityid = fac.nfaid and
        SUBSTR(replace(inv.UNIFORMATIICODE,''.'',''''), 0, 7) = SUBSTR(replace(assets_loc.subsystem,''.'',''''),0,7) and
        dofac.adoid = act.adoid and
        fac.XMLACTIONID = act.XMLACTIONID and
        fac.facilityid = sum.facilityid and
        sum.lineitemid = inv.lineitemid and
        act.adoid = ' || to_number(in_vDeliveryOrderId) || '';


        --htp.p('insert into debug (message) values (''' || lc_vsql || ''')');
        
        
    lc_jlSQL.append(lc_vSql);
    executeSql(lc_jlSQL);
    
    RETURN null;
  EXCEPTION    
    when spiders_error then
        execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' SPIDERS error ' || lc_vErrorDescription || ''')';
        return '{"error":"' || lc_vErrorDescription || '"}';
    when json_error then
        execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' JSON error'')';
        return '{"error":"json"}';
    when others then
        execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' dml error: ' || sqlerrm || ''')';
        return '{"error":"' || sqlerrm || '"}';
  END generate_fac_assets_sql;

  function generate_service_request_sql (in_vDeliveryOrderId varchar2) return varchar2 AS
    lc_vSql long;
    lc_vDelete long;
    lc_vRequirement varchar(225);
    lc_vH3 varchar(225);
    lc_vPw varchar(225);
    lc_v3 varchar(225);
    lc_vCustomerRef varchar(225);
    lc_jlSQL json_list := json_list();
    lc_jlDelete json_list := json_list();
  BEGIN
  
    lc_vRequirement := 'REQUIREMENT ';
    lc_vH3 := 'H3';
    lc_vPw := 'PW ';
    lc_v3  := '3 ';
    lc_vCustomerRef := 'SPIDERS ';
    
    lc_vDelete := 'delete from spd_t_spiders_maximo_sr where DELIVERYORDERID = ' || to_number(in_vDeliveryOrderId);
    lc_jlDelete.append(lc_vDelete);
    executeSql(lc_jlDelete);
    
    
    lc_vSql := 'insert into spd_t_spiders_maximo_sr
        (
          COMMENTS,
          DELIVERYORDERID,
          SITEID,
          SUMMARY,
          LONG_DESC,
          INFADS,
          FACILITYNAME,
          LOCATION,
          ADDLLOCINFO,
          WORKTYPE,
          SUBWORKTYPE,
          BUSINESSLINE,
          PSDELIVERABLE,
          WORKCATEGORY,
          REPORTEDBY,
          CUSTOMERID,
          CUSTOMERNAME,
          CUSTOMERPHONE,
          CUSTOMEREMAIL,
          CUSTOMERREFNUM,
          PRIORITYSCORE,
          PRIORITYYEAR,
          SPIDERSUFII,
          ASSETMAXIMO,
          ASSET,
          EXPECTEDCOST,
          GLACCOUNT,
          SVCPROVREFCODE,
          CLASSIFICATION,
          INTERNAL
        )
        select unique
          '''' as COMMENTS, ' ||
          to_number(in_vDeliveryOrderId)|| ' as DELIVERYORDERID,
          loc_nfa.loc_site as SITEID,
          rep.repairdescription as SUMMARY,
          rep.repairdescription as LONG_DESC,
          fac.nfaid as INFADS,
          '''' as FACILITYNAME,
          loc_nfa.location as LOCATION,
          null as ADDLLOCINFO, 
          ''REQUIREMENT'' as WORKTYPE,
          ''REQUIREMENT'' as SUBWORKTYPE,
          ''PW'' as BUSINESSLINE, 
          ''H3'' as PSDELIVERABLE,
          ''3'' as WORKCATEGORY,
          act.engineerofrecord as REPORTEDBY,
          null as CUSTOMERID,
          '''' as CUSTOMERNAME,
          null as CUSTOMERPHONE,
          '''' as CUSTOMEREMAIL,
          ''SPIDERS'' as CUSTOMERREFNUM,
          '''' as PRIORITYSCORE,
          '''' as PRIORITYYEAR,
          inv.UNIFORMATIICODE as SPIDERSUFII,
          '''' as ASSETMAXIMO,
          CONCAT(loc_nfa.location, SUBSTR(replace(inv.UNIFORMATIICODE,''.'',''''), 0, 7)) as ASSET,
          sum.approximaterepaircosts as EXPECTEDCOST,
          '''' as GLACCOUNT,
          '''' as SVCPROVREFCODE,
          '''' as CLASSIFICATION,
          '''' as INTERNAL
        from
          spd_t_sip_locations_by_nfa loc_nfa,
          SPD_T_SIP_ASSETS_BY_LOC assets_loc,
          spd_t_do_facilities dofac,
          spd_t_xml_actions act,
          spd_t_xml_facilities fac,
          spd_t_xml_asset_summary sum,
          spd_t_xml_asset_inventory inv,
          spd_t_xml_facility_repairs rep
        where
          loc_nfa.facilityid = fac.nfaid and
          SUBSTR(replace(inv.UNIFORMATIICODE,''.'',''''), 0, 7) = SUBSTR(replace(assets_loc.subsystem,''.'',''''),0,7) and
          rep.facilityid = fac.facilityid and
          rep.uniformatiicode = inv.uniformatiicode and
          dofac.adoid = act.adoid and
          fac.XMLACTIONID = act.XMLACTIONID and
          fac.facilityid = sum.facilityid and
          sum.lineitemid = inv.lineitemid and 
          act.adoid = ' || to_number(in_vDeliveryOrderId) || '';
          
          --htp.p(lc_vSql);
          
          lc_jlSQL.append(lc_vSql);
          executeSql(lc_jlSQL);
    RETURN null;
  END generate_service_request_sql;

  function generate_condition_index_sql (in_vDeliveryOrderId varchar2) return varchar2 AS
    lc_vSql long;
    lc_vDelete long;
    lc_jlSQL json_list := json_list();
    lc_jlDelete json_list := json_list();
  BEGIN
  
    lc_vDelete := 'delete from spd_t_spiders_facility_ci where DELIVERYORDERID = ' || to_number(in_vDeliveryOrderId);
    lc_jlDelete.append(lc_vDelete);
    executeSql(lc_jlDelete);
    
    lc_vSql := '
      insert into spd_t_spiders_facility_ci
      (
        FACILITYID,
        DELIVERYORDERID,
        DELIVERYORDERFACILITYID,
        CI,
        CIDATE,
        REPORTNUM,
        LOCATION,
        FACILITYNAME,
        FACILITYNO,
        INSPECTION_TYPE
      )
      select unique
        fac.infads_facility_id          as FACILITYID, 
        do.adoid                        as DELIVERYORDERID,
        dofac.deliveryorderfacilityid   as DELIVERYORDERFACILITYID,
        xml_exec_table.cirating         as CI,
        do.onsite_start_date            as CIDATE,
        do.report_number                as REPORTNUM,
        maxfac.location                 as LOCATION,
        fac.spd_fac_name                as FACILITYNAME,
        fac.spd_fac_no                  as FACILITYNO,
        xml_exec_table.inspectype       as INSPECTION_TYPE
      from 
        spd_t_facilities            fac,
        spd_t_delivery_order_facs   dofac,
        spd_t_delivery_orders       do,
        spd_t_xml_exec_tables       xml_exec_table,
        spd_t_sip_locations_by_nfa  maxfac
      where 
        fac.facid = dofac.facid and
        fac.infads_facility_id = xml_exec_table.nfaid and
        maxfac.facilityid = fac.infads_facility_id and
        dofac.deliveryorderid = do.deliveryorderid and
        do.adoid =' || to_number(in_vDeliveryOrderId) || '';
        
        
        --htp.p(lc_vSql);
        
    lc_jlSQL.append(lc_vSql);
    executeSql(lc_jlSQL);
        
    RETURN null;
  END generate_condition_index_sql;

  function process_maximo (in_cJSON clob) return json AS
    spiders_error EXCEPTION;
    json_error EXCEPTION;
    lc_vErrorDescription varchar2(255) := 'default';
    lc_vFunctionName varchar2(255) := 'process_maximo';
    lc_jReturn json; 
    lc_jParameterJson json;
    lc_vADOID varchar2(255);
    lc_vAssetSql varchar2(255);
    lc_vSvcReqSql varchar2(255);
    lc_vCISql varchar2(255);
    lc_nContinue number :=0;
  BEGIN
    execute immediate 'insert into debug (MISC) values (''' || in_cJSON || ' json sent toeprocess_maximo function'')';
    lc_jReturn := json('{"testKey":"testval"}');
    
    lc_jParameterJson := json(in_cJSON);
    
    lc_vADOID := getAttributeValue('ADOID', lc_jParameterJson);
    execute immediate 'insert into debug (message) values (''' || lc_vADOID || ''')';
    lc_nContinue := f_test_for_error(lc_vADOID);
    
    if lc_nContinue = 1 then
      execute immediate 'insert into debug (message) values ('' executed fac_assets_sql '')';
      lc_vAssetSql := generate_fac_assets_sql(lc_vADOID);
      
      lc_vCISql := generate_condition_index_sql(lc_vADOID);
      execute immediate 'insert into debug (message) values ('' executed condition_index_sql '')';
      
      execute immediate 'insert into debug (message) values ('' executed service_request_sql '')';
      lc_vSvcReqSql := generate_service_request_sql(lc_vADOID);
    else
      lc_vErrorDescription := 'problem getting ADOID';
      raise spiders_error; 
    end if;
    
    RETURN lc_jReturn;
  EXCEPTION     
    when spiders_error then
        execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' SPIDERS error ' || lc_vErrorDescription || ''')';
        return json('{"error":"' || lc_vFunctionName || ' SPIDERS error ' || lc_vErrorDescription || '"}');
    when json_error then
        execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' JSON error'')';
        return json('{"error":"' || lc_vFunctionName || ' JSON error ' || lc_vErrorDescription || '"}');
    when others then
        execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' dml error: ' || sqlerrm || ''')';
       return json('{"error":"' || lc_vFunctionName || ' dml error ' || sqlerrm || '"}');
  END process_maximo;

END PKG_PROCESS_MAXIMO;

/
