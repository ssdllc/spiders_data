create or replace PACKAGE BODY PKG_ROWIFY as 

/*
function f_getPrimaryKeyValue (in_vXPath varchar2, in_vNode varchar2) return number 
as 

begin

XML_ACTION -- PK = /Action[1]
XML_ACTION_DRAWING -- PK = /Action[1]
XML_ACTION_PHOTO -- PK = /Action[1]
XML_ACTSUPPORT_FILE -- PK = /Action[1]
XML_AI_FITTING -- PL = /Action[1]/Facilities[1]/Facility[1]/AssetSummary[1]/AssetSummaryLineItem[101]/AssetInventory[1]/AssetInventoryLineItem[1]/
XML_AI_METADATA -- PL = /Action[1]/Facilities[1]/Facility[1]/AssetSummary[1]/AssetSummaryLineItem[101]/AssetInventory[1]/AssetInventoryLineItem[1]/
XML_ASSET_INVENTORY -- PL = /Action[1]/Facilities[1]/Facility[1]/AssetSummary[1]/AssetSummaryLineItem[101]/
XML_ASSET_SUMMARY -- PL = /Action[1]/Facilities[1]/Facility[1]/
XML_ATTENDEE -- PK = /Action[1]/ExitBrief[1]
XML_DEFECT -- PK = /Action[1]/Facilities[1]/Facility[1]/AssetSummary[1]/AssetSummaryLineItem[47]/AssetInventory[1]/AssetInventoryLineItem[1]/Defects[1]
XML_DEFECT_METADATA -- PK = /Action[1]/Facilities[1]/Facility[1]/AssetSummary[1]/AssetSummaryLineItem[47]/AssetInventory[1]/AssetInventoryLineItem[1]/Defects[1]
XML_DEFECT_VALIDATION -- PK = /Action[1]
XML_ES_PARAGRAPH -- PK = /Action[1]/ExecutiveSummary[1]
XML_ET_METADATA -- PK = /Action[1]/Facilities[1]/Facility[1]/ExecutiveTable[1]/
XML_EXEC_SUMMARY -- PK = /Action[1]
XML_EXEC_TABLE -- PK = /Action[1]/Facilities[1]/Facility[1]
XML_EXIT_BRIEF -- PK = /Action[1]
XML_FACILITY -- PK = /Action[1]
XML_FACILITY_DRAWING -- PK = /Action[1]/Facilities[1]/Facility[1]
XML_FACILITY_PHOTO -- PK = /Action[1]/Facilities[1]/Facility[1]
XML_FACILITY_REPAIR -- PK = /Action[1]/Facilities[1]/Facility[1]
XML_FAC_SUP_FILE -- PK = /Action[1]/Facilities[1]/Facility[1]
XML_FINDREC -- PK = /Action[1]/Facilities[1]/Facility[1]
XML_REPAIR_PERCENTAGE -- PK = /Action[1]/Facilities[1]/Facility[1]
XML_SENTENCE -- PK = /Action[1]/ExecutiveSummary[1]/Paragraph[1]


--XML_PHOTO_ASSET_TYPE

end;
*/

function f_XML_ACTION (in_nID number, in_nPK number, in_nADOID number, in_vCreatedBy varchar2) return XML_ACTIONS pipelined
as
  lc_vFunctionName varchar2(255) := 'f_XML_ACTION';
  l_row   xml_action := new XML_ACTION(null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);

-----
vAuthor   varchar2(200) := 'mrussalesi';
vCreated  varchar2(200) := '20180528';
vUpdate   varchar2(200) := '20180528';
----- 
begin

  for rec in (
                select 
                  (select xmlflattenid from spd_t_xml_flatten where fpath = '/Action' and adoid = in_nADOID) xmlactionid, 
                  spd_t_xml_flatten.* 
                from 
                  spd_t_xml_flatten 
                where 
                  parentid in (
                    select 
                      node_position 
                    from 
                      spd_t_xml_flatten 
                    where 
                      fpath = '/Action' 
                      and adoid = in_nADOID) 
                order by node_position
  ) 
  loop
  
    l_row.XMLACTIONID := rec.xmlactionid;
    l_row.ADOID       := in_nADOID;
    l_row.CREATED_BY  := in_vCreatedBy;

    if rec.node_name = 'ESFile' then
      begin
        l_row.ESFile := rec.node_value;
      exception when others then
        l_row.ESFile := null;
        ----htp.p('err: ' || sqlerrm || ',' || rec.xpath); --MAKE THIS A DEBUG FUNCTION FROM HERE SO WE CAN EVENTUALLY LOG ALL ERRORS
      end;
    end if;
    
    if lower(rec.node_name) = 'actionid' then
      begin
        l_row.ACTIONID := rec.node_value;
      exception when others then
        l_row.ACTIONID := -9999;
        ----htp.p('err: ' || sqlerrm || ',' || rec.xpath); --MAKE THIS A DEBUG FUNCTION FROM HERE SO WE CAN EVENTUALLY LOG ALL ERRORS
      end;
    end if;
    
    if rec.node_name = 'DataReqRevision' then
      begin
        l_row.DataReqRevision := rec.node_value;
      exception when others then
        l_row.DataReqRevision := null;
        ----htp.p('err: ' || sqlerrm || ',' || rec.xpath); --MAKE THIS A DEBUG FUNCTION FROM HERE SO WE CAN EVENTUALLY LOG ALL ERRORS
      end;
    end if;
    
    if rec.node_name = 'ContractNumber' then
      begin
        l_row.ContractNumber := rec.node_value;
      exception when others then
        l_row.ContractNumber := null;
        ----htp.p('err: ' || sqlerrm || ',' || rec.xpath); --MAKE THIS A DEBUG FUNCTION FROM HERE SO WE CAN EVENTUALLY LOG ALL ERRORS
      end;
    end if;
    
    if rec.node_name = 'DeliveryOrderNumber' then
      begin
        l_row.DeliveryOrderNumber := rec.node_value;
      exception when others then
        l_row.DeliveryOrderNumber := null;
        ----htp.p('err: ' || sqlerrm || ',' || rec.xpath); --MAKE THIS A DEBUG FUNCTION FROM HERE SO WE CAN EVENTUALLY LOG ALL ERRORS
      end;
    end if;
    
    if rec.node_name = 'FundingSource' then
      begin
        l_row.FundingSource := rec.node_value;
      exception when others then
        l_row.FundingSource := null;
        ----htp.p('err: ' || sqlerrm || ',' || rec.xpath); --MAKE THIS A DEBUG FUNCTION FROM HERE SO WE CAN EVENTUALLY LOG ALL ERRORS
      end;
    end if;
    
    if rec.node_name = 'ReportNumber' then
      begin
        l_row.ReportNumber := rec.node_value;
      exception when others then
        l_row.ReportNumber := null;
        ----htp.p('err: ' || sqlerrm || ',' || rec.xpath); --MAKE THIS A DEBUG FUNCTION FROM HERE SO WE CAN EVENTUALLY LOG ALL ERRORS
      end;
    end if;
    
    if rec.node_name = 'ContractorCompany' then
      begin
        l_row.ContractorCompany := rec.node_value;
      exception when others then
        l_row.ContractorCompany := null;
        ----htp.p('err: ' || sqlerrm || ',' || rec.xpath); --MAKE THIS A DEBUG FUNCTION FROM HERE SO WE CAN EVENTUALLY LOG ALL ERRORS
      end;
    end if;
    
    if rec.node_name = 'Address1' then
      begin
        l_row.Address1 := rec.node_value;
      exception when others then
        l_row.Address1 := null;
        ----htp.p('err: ' || sqlerrm || ',' || rec.xpath); --MAKE THIS A DEBUG FUNCTION FROM HERE SO WE CAN EVENTUALLY LOG ALL ERRORS
      end;
    end if;
    
    if rec.node_name = 'Address2' then
      begin
        l_row.Address2 := rec.node_value;
      exception when others then
        l_row.Address2 := null;
        ----htp.p('err: ' || sqlerrm || ',' || rec.xpath); --MAKE THIS A DEBUG FUNCTION FROM HERE SO WE CAN EVENTUALLY LOG ALL ERRORS
      end;
    end if;
    
    if rec.node_name = 'City' then
      begin
        l_row.City := rec.node_value;
      exception when others then
        l_row.City := null;
        ----htp.p('err: ' || sqlerrm || ',' || rec.xpath); --MAKE THIS A DEBUG FUNCTION FROM HERE SO WE CAN EVENTUALLY LOG ALL ERRORS
      end;
    end if;
    
    if rec.node_name = 'State' then --THIS ONE MIGHT NOT MATCH EXACTLY -- MR
      begin
        l_row.ADDRESSSTATE := rec.node_value;
      exception when others then
        l_row.ADDRESSSTATE := null;
        ----htp.p('err: ' || sqlerrm || ',' || rec.xpath); --MAKE THIS A DEBUG FUNCTION FROM HERE SO WE CAN EVENTUALLY LOG ALL ERRORS
      end;
    end if;
    
    if rec.node_name = 'ZipCode' then
      begin
        l_row.ZipCode := rec.node_value;
      exception when others then
        l_row.ZipCode := null;
        ----htp.p('err: ' || sqlerrm || ',' || rec.xpath); --MAKE THIS A DEBUG FUNCTION FROM HERE SO WE CAN EVENTUALLY LOG ALL ERRORS
      end;
    end if;
    
    if rec.node_name = 'EngineerOfRecord' then
      begin
        l_row.EngineerOfRecord := rec.node_value;
      exception when others then
        l_row.EngineerOfRecord := null;
        ----htp.p('err: ' || sqlerrm || ',' || rec.xpath); --MAKE THIS A DEBUG FUNCTION FROM HERE SO WE CAN EVENTUALLY LOG ALL ERRORS
      end;
    end if;
    
    if rec.node_name = 'EORContactEmail' then
      begin
        l_row.EORContactEmail := rec.node_value;
      exception when others then
        l_row.EORContactEmail := null;
        ----htp.p('err: ' || sqlerrm || ',' || rec.xpath); --MAKE THIS A DEBUG FUNCTION FROM HERE SO WE CAN EVENTUALLY LOG ALL ERRORS
      end;
    end if;
    
    if rec.node_name = 'EORPhoneNumber' then
      begin
        l_row.EORPhoneNumber := rec.node_value;
      exception when others then
        l_row.EORPhoneNumber := null;
        ----htp.p('err: ' || sqlerrm || ',' || rec.xpath); --MAKE THIS A DEBUG FUNCTION FROM HERE SO WE CAN EVENTUALLY LOG ALL ERRORS
      end;
    end if;
    
    if rec.node_name = 'OnsiteStartDate' then
      begin
        l_row.OnsiteStartDate := rec.node_value;
      exception when others then
        l_row.OnsiteStartDate := null;
        ----htp.p('err: ' || sqlerrm || ',' || rec.xpath); --MAKE THIS A DEBUG FUNCTION FROM HERE SO WE CAN EVENTUALLY LOG ALL ERRORS
      end;
    end if;
    
    if rec.node_name = 'OnsiteEndDate' then
      begin
        l_row.OnsiteEndDate := rec.node_value;
      exception when others then
        l_row.OnsiteEndDate := null;
        ----htp.p('err: ' || sqlerrm || ',' || rec.xpath); --MAKE THIS A DEBUG FUNCTION FROM HERE SO WE CAN EVENTUALLY LOG ALL ERRORS
      end;
    end if;
    
    if rec.node_name = 'SubmittalDate' then
      begin
        l_row.SubmittalDate := rec.node_value;
      exception when others then
        l_row.SubmittalDate := null;
        ----htp.p('err: ' || sqlerrm || ',' || rec.xpath); --MAKE THIS A DEBUG FUNCTION FROM HERE SO WE CAN EVENTUALLY LOG ALL ERRORS
      end;
    end if;
    
    if rec.node_name = 'ActivityName' then
      begin
        l_row.ActivityName := rec.node_value;
      exception when others then
        l_row.ActivityName := null;
        ----htp.p('err: ' || sqlerrm || ',' || rec.xpath); --MAKE THIS A DEBUG FUNCTION FROM HERE SO WE CAN EVENTUALLY LOG ALL ERRORS
      end;
    end if;
    
    if rec.node_name = 'ActCity' then
      begin
        l_row.ActCity := rec.node_value;
      exception when others then
        l_row.ActCity := null;
        ----htp.p('err: ' || sqlerrm || ',' || rec.xpath); --MAKE THIS A DEBUG FUNCTION FROM HERE SO WE CAN EVENTUALLY LOG ALL ERRORS
      end;
    end if;
    
    if rec.node_name = 'ActState' then
      begin
        l_row.ActState := rec.node_value;
      exception when others then
        l_row.ActState := null;
        ----htp.p('err: ' || sqlerrm || ',' || rec.xpath); --MAKE THIS A DEBUG FUNCTION FROM HERE SO WE CAN EVENTUALLY LOG ALL ERRORS
      end;
    end if;
    
    if rec.node_name = 'ActZipCode' then
      begin
        l_row.ActZipCode := rec.node_value;
      exception when others then
        l_row.ActZipCode := null;
        ----htp.p('err: ' || sqlerrm || ',' || rec.xpath); --MAKE THIS A DEBUG FUNCTION FROM HERE SO WE CAN EVENTUALLY LOG ALL ERRORS
      end;
    end if;
    
    if rec.node_name = 'ActivityUIC' then
      begin
        l_row.ActivityUIC := rec.node_value;
      exception when others then
        l_row.ActivityUIC := null;
        ----htp.p('err: ' || sqlerrm || ',' || rec.xpath); --MAKE THIS A DEBUG FUNCTION FROM HERE SO WE CAN EVENTUALLY LOG ALL ERRORS
      end;
    end if;
    
  end loop;
  
  pipe row(l_row);
  
EXCEPTION WHEN OTHERS THEN
  null; --htp.p into debug (FUNCTIONNAME, INPUT, MESSAGE) values (''' ||  lg_vPackageName ||lc_vFunctionName || ''','''|| 'An error occured while extracting the data from the SPD_T_XML_FLATTEN' ||''',''' || sqlerrm || ''')');
  
end f_XML_ACTION;

function f_XML_ACTION_DRAWING (in_nID number, in_nPK number, in_nADOID number, in_vCreatedBy varchar2) return XML_ACTION_DRAWINGS pipelined
as
  lc_vFunctionName varchar2(255) := 'f_XML_ACTION_DRAWING';
  l_row   XML_ACTION_DRAWING := new XML_ACTION_DRAWING(null, null, null, null, null, null, null);

/* -- REFERENCE TYPE
  XMLACTIONID             NUMBER,
	NAVFACDRAWINGNUMBER     NUMBER,
	DESCRIPTION             VARCHAR2(4000),
	DRAWINGTYPE             VARCHAR2(4000),
	FILENAME                VARCHAR2(4000),
	ACT_DRAWINGID           VARCHAR2(4000),
	CREATED_BY              VARCHAR2(255)
*/
  
-----
vAuthor   varchar2(200) := 'mrussalesi';
vCreated  varchar2(200) := '20180528';
vUpdate   varchar2(200) := '20180528';
----- 
begin

--!!!NOTE THERE IS NO DATA IN THE SAMPLE FILES FOR THIS...USURE OF THE XML NODE NAMES, using upper() to force, may take extra time to do this.

for rec1 in (select * from spd_t_xml_flatten where fpath = '/Action/Drawings/Drawing' and adoid = in_nADOID) 
loop

  for rec in (select (select xmlflattenid from spd_t_xml_flatten where fpath = '/Action' and adoid = in_nADOID) xmlactionid, spd_t_xml_flatten.* from spd_t_xml_flatten where parentid = rec1.node_position order by node_position) 
  loop
  
    --i think we may have to get the PK, using the level - 1, and the parentid, and maybe something else....this may be the tricky part
    --its also possible that we just specifically find the data
  
    l_row.XMLACTIONID := rec.xmlactionid;
    l_row.CREATED_BY  := in_vCreatedBy;

    if upper(rec.node_name) = 'NAVFACDRAWINGNUMBER' then
      begin
        l_row.NAVFACDRAWINGNUMBER := rec.node_value;
      exception when others then
        l_row.NAVFACDRAWINGNUMBER := -9999;
        ----htp.p('err: ' || sqlerrm || ',' || rec.xpath); --MAKE THIS A DEBUG FUNCTION FROM HERE SO WE CAN EVENTUALLY LOG ALL ERRORS
      end;
    end if;
    
    if upper(rec.node_name) = 'DESCRIPTION' then
      begin
        l_row.DESCRIPTION := rec.node_value;
      exception when others then
        l_row.DESCRIPTION := -9999;
        ----htp.p('err: ' || sqlerrm || ',' || rec.xpath); --MAKE THIS A DEBUG FUNCTION FROM HERE SO WE CAN EVENTUALLY LOG ALL ERRORS
      end;
    end if;
    
    if upper(rec.node_name) = 'DRAWINGTYPE' then
      begin
        l_row.DRAWINGTYPE := rec.node_value;
      exception when others then
        l_row.DRAWINGTYPE := -9999;
        ----htp.p('err: ' || sqlerrm || ',' || rec.xpath); --MAKE THIS A DEBUG FUNCTION FROM HERE SO WE CAN EVENTUALLY LOG ALL ERRORS
      end;
    end if;
    
    if upper(rec.node_name) = 'FILENAME' then
      begin
        l_row.FILENAME := rec.node_value;
      exception when others then
        l_row.FILENAME := -9999;
        ----htp.p('err: ' || sqlerrm || ',' || rec.xpath); --MAKE THIS A DEBUG FUNCTION FROM HERE SO WE CAN EVENTUALLY LOG ALL ERRORS
      end;
    end if;
    
    if upper(rec.node_name) = 'ACT_DRAWINGID' then
      begin
        l_row.ACT_DRAWINGID := rec.node_value;
      exception when others then
        l_row.ACT_DRAWINGID := -9999;
        ----htp.p('err: ' || sqlerrm || ',' || rec.xpath); --MAKE THIS A DEBUG FUNCTION FROM HERE SO WE CAN EVENTUALLY LOG ALL ERRORS
      end;
    end if;
    
  end loop;
  
  pipe row(l_row);

end loop;
  
EXCEPTION WHEN OTHERS THEN
  null; --htp.p into debug (FUNCTIONNAME, INPUT, MESSAGE) values (''' ||  lg_vPackageName ||lc_vFunctionName || ''','''|| 'An error occured while extracting the data from the SPD_T_XML_FLATTEN' ||''',''' || sqlerrm || ''')');
  
end f_XML_ACTION_DRAWING;

function f_XML_ACTION_PHOTO (in_nID number, in_nPK number, in_nADOID number, in_vCreatedBy varchar2) return XML_ACTION_PHOTOS pipelined
as
  lc_vFunctionName varchar2(255) := 'f_XML_ACTION_PHOTOS';
  l_row   XML_ACTION_PHOTO := new XML_ACTION_PHOTO(null, null, null, null, null, null, null);

/* -- REFERENCE TYPE
  PHOTOID           NUMBER,
	XMLACTIONID       NUMBER,
	UNIFORMATIICODE   VARCHAR2(4000),
	IMAGEFILENAME     VARCHAR2(4000),
	CAPTION           VARCHAR2(4000),
	PHOTOTYPE         VARCHAR2(4000),
	CREATED_BY        VARCHAR2(255)
*/
  
-----
vAuthor   varchar2(200) := 'mrussalesi';
vCreated  varchar2(200) := '20180528';
vUpdate   varchar2(200) := '20180528';
----- 
begin

--!!!NOTE THERE IS NO DATA IN THE SAMPLE FILES FOR THIS...USURE OF THE XML NODE NAMES, using upper() to force, may take extra time to do this.

for rec1 in (select * from spd_t_xml_flatten where fpath = '/Action/Photos/Photo' and adoid = in_nADOID) 
loop

  for rec in (select (select xmlflattenid from spd_t_xml_flatten where fpath = '/Action' and adoid = in_nADOID) xmlactionid, spd_t_xml_flatten.* from spd_t_xml_flatten where parentid = rec1.node_position order by node_position)  
  loop
  
    --i think we may have to get the PK, using the level - 1, and the parentid, and maybe something else....this may be the tricky part
    --its also possible that we just specifically find the data
  
    l_row.XMLACTIONID := rec.xmlactionid;
    l_row.CREATED_BY  := in_vCreatedBy;

    if upper(rec.node_name) = 'PHOTOID' then
      begin
        l_row.PHOTOID := rec.node_value;
      exception when others then
        l_row.PHOTOID := -9999;
        ----htp.p('err: ' || sqlerrm || ',' || rec.xpath); --MAKE THIS A DEBUG FUNCTION FROM HERE SO WE CAN EVENTUALLY LOG ALL ERRORS
      end;
    end if;
    
    if upper(rec.node_name) = 'UNIFORMATIICODE' then
      begin
        l_row.UNIFORMATIICODE := rec.node_value;
      exception when others then
        l_row.UNIFORMATIICODE := -9999;
        ----htp.p('err: ' || sqlerrm || ',' || rec.xpath); --MAKE THIS A DEBUG FUNCTION FROM HERE SO WE CAN EVENTUALLY LOG ALL ERRORS
      end;
    end if;
    
    if upper(rec.node_name) = 'IMAGEFILENAME' then
      begin
        l_row.IMAGEFILENAME := rec.node_value;
      exception when others then
        l_row.IMAGEFILENAME := -9999;
        ----htp.p('err: ' || sqlerrm || ',' || rec.xpath); --MAKE THIS A DEBUG FUNCTION FROM HERE SO WE CAN EVENTUALLY LOG ALL ERRORS
      end;
    end if;
    
    if upper(rec.node_name) = 'CAPTION' then
      begin
        l_row.CAPTION := rec.node_value;
      exception when others then
        l_row.CAPTION := -9999;
        ----htp.p('err: ' || sqlerrm || ',' || rec.xpath); --MAKE THIS A DEBUG FUNCTION FROM HERE SO WE CAN EVENTUALLY LOG ALL ERRORS
      end;
    end if;
    
    if upper(rec.node_name) = 'PHOTOTYPE' then
      begin
        l_row.PHOTOTYPE := rec.node_value;
      exception when others then
        l_row.PHOTOTYPE := -9999;
        ----htp.p('err: ' || sqlerrm || ',' || rec.xpath); --MAKE THIS A DEBUG FUNCTION FROM HERE SO WE CAN EVENTUALLY LOG ALL ERRORS
      end;
    end if;
    
  end loop;
  
  pipe row(l_row);

end loop;
  
EXCEPTION WHEN OTHERS THEN
  null; --htp.p into debug (FUNCTIONNAME, INPUT, MESSAGE) values (''' ||  lg_vPackageName ||lc_vFunctionName || ''','''|| 'An error occured while extracting the data from the SPD_T_XML_FLATTEN' ||''',''' || sqlerrm || ''')');
  
end f_XML_ACTION_PHOTO;
	
function f_XML_ACTSUPPORT_FILE (in_nID number, in_nPK number, in_nADOID number, in_vCreatedBy varchar2) return XML_ACTSUPPORT_FILES pipelined
as
  lc_vFunctionName varchar2(255) := 'f_XML_ACTSUPPORT_FILE';
  l_row   XML_ACTSUPPORT_FILE := new XML_ACTSUPPORT_FILE(null, null, null, null, null, null);

/* -- REFERENCE TYPE
  FILEID      NUMBER,
	XMLACTIONID      NUMBER,
	SUPPORTINGFILEID      VARCHAR2(4000),
	FILENAME      VARCHAR2(4000),
	DESCRIPTION      VARCHAR2(4000),
	CREATED_BY      VARCHAR2(255)
*/
  
-----
vAuthor   varchar2(200) := 'mrussalesi';
vCreated  varchar2(200) := '20180528';
vUpdate   varchar2(200) := '20180528';
----- 
begin

--!!!NOTE THERE IS NO DATA IN THE SAMPLE FILES FOR THIS...USURE OF THE XML NODE NAMES, using upper() to force, may take extra time to do this.
for rec1 in (select * from spd_t_xml_flatten where fpath = '/Action/SupportingFiles/SupportingFile' and adoid = in_nADOID) 
loop

  for rec in (select (select xmlflattenid from spd_t_xml_flatten where fpath = '/Action' and adoid = in_nADOID) xmlactionid, spd_t_xml_flatten.* from spd_t_xml_flatten where parentid = rec1.node_position order by node_position)  
  loop
  
    --i think we may have to get the PK, using the level - 1, and the parentid, and maybe something else....this may be the tricky part
    --its also possible that we just specifically find the data
  
    l_row.XMLACTIONID := rec.xmlactionid;
    l_row.CREATED_BY  := in_vCreatedBy;

    if upper(rec.node_name) = 'FILEID' then
      begin
        l_row.FILEID := rec.node_value;
      exception when others then
        l_row.FILEID := -9999;
        ----htp.p('err: ' || sqlerrm || ',' || rec.xpath); --MAKE THIS A DEBUG FUNCTION FROM HERE SO WE CAN EVENTUALLY LOG ALL ERRORS
      end;
    end if;
    
    if upper(rec.node_name) = 'SUPPORTINGFILEID' then
      begin
        l_row.SUPPORTINGFILEID := rec.node_value;
      exception when others then
        l_row.SUPPORTINGFILEID := -9999;
        ----htp.p('err: ' || sqlerrm || ',' || rec.xpath); --MAKE THIS A DEBUG FUNCTION FROM HERE SO WE CAN EVENTUALLY LOG ALL ERRORS
      end;
    end if;
    
    if upper(rec.node_name) = 'FILENAME' then
      begin
        l_row.FILENAME := rec.node_value;
      exception when others then
        l_row.FILENAME := -9999;
        ----htp.p('err: ' || sqlerrm || ',' || rec.xpath); --MAKE THIS A DEBUG FUNCTION FROM HERE SO WE CAN EVENTUALLY LOG ALL ERRORS
      end;
    end if;
    
    if upper(rec.node_name) = 'DESCRIPTION' then
      begin
        l_row.DESCRIPTION := rec.node_value;
      exception when others then
        l_row.DESCRIPTION := -9999;
        ----htp.p('err: ' || sqlerrm || ',' || rec.xpath); --MAKE THIS A DEBUG FUNCTION FROM HERE SO WE CAN EVENTUALLY LOG ALL ERRORS
      end;
    end if;
    
  end loop;
  
  pipe row(l_row);
  
end loop;
  
EXCEPTION WHEN OTHERS THEN
  null; --htp.p into debug (FUNCTIONNAME, INPUT, MESSAGE) values (''' ||  lg_vPackageName ||lc_vFunctionName || ''','''|| 'An error occured while extracting the data from the SPD_T_XML_FLATTEN' ||''',''' || sqlerrm || ''')');
  
end f_XML_ACTSUPPORT_FILE;


function f_XML_FACILITY (in_nID number, in_nPK number, in_nADOID number, in_vCreatedBy varchar2) return XML_FACILITIES pipelined
as
  lc_vFunctionName varchar2(255) := 'f_XML_FACILITY';
  l_row   XML_FACILITY := new XML_FACILITY(null, null, null, null, null, null, null, null, null, null, null, null, null, null);

/* -- REFERENCE TYPE
  FACILITYID                NUMBER,
	XMLACTIONID               NUMBER,
	FSFILENAME                VARCHAR2(4000),
	ONSITESTARTDATE           VARCHAR2(4000),
	ONSITEENDDATE             VARCHAR2(4000),
	SUBMITTALDATE             VARCHAR2(4000),
	FACILITYNAME              VARCHAR2(4000),
	FACILITYNO                NUMBER,
	NFAID                     VARCHAR2(4000),
	SPIDERS_FACILITYID        NUMBER,
	COSTESTIMATEFILENAME      VARCHAR2(4000),
	DD1391FILENAME            VARCHAR2(4000),
	DELIVERYORDERFACILITYID   NUMBER,
	CREATED_BY
*/
  
-----
vAuthor   varchar2(200) := 'mrussalesi';
vCreated  varchar2(200) := '20180528';
vUpdate   varchar2(200) := '20180528';
----- 
begin

--!!!NOTE THERE IS NO DATA IN THE SAMPLE FILES FOR THIS...USURE OF THE XML NODE NAMES, using upper() to force, may take extra time to do this.

for rec1 in (select * from spd_t_xml_flatten where fpath = '/Action/Facilities/Facility' and adoid = in_nADOID) 
loop

  for rec in (select (select xmlflattenid from spd_t_xml_flatten where fpath = '/Action' and adoid = in_nADOID) xmlactionid, spd_t_xml_flatten.* from spd_t_xml_flatten where parentid = rec1.node_position order by node_position) 
  loop
  
    --i think we may have to get the PK, using the level - 1, and the parentid, and maybe something else....this may be the tricky part
    --its also possible that we just specifically find the data
    l_row.FACILITYID := rec1.xmlflattenid;
    l_row.XMLACTIONID := rec.xmlactionid;
    l_row.CREATED_BY  := in_vCreatedBy;

    if upper(rec.node_name) = 'FSFILENAME' then
      begin
        l_row.FSFILENAME := rec.node_value;
      exception when others then
        l_row.FSFILENAME := -9999;
        ----htp.p('err: ' || sqlerrm || ',' || rec.xpath); --MAKE THIS A DEBUG FUNCTION FROM HERE SO WE CAN EVENTUALLY LOG ALL ERRORS
      end;
    end if;
    
    if upper(rec.node_name) = 'ONSITESTARTDATE' then
      begin
        l_row.ONSITESTARTDATE := rec.node_value;
      exception when others then
        l_row.ONSITESTARTDATE := -9999;
        ----htp.p('err: ' || sqlerrm || ',' || rec.xpath); --MAKE THIS A DEBUG FUNCTION FROM HERE SO WE CAN EVENTUALLY LOG ALL ERRORS
      end;
    end if;
    
    if upper(rec.node_name) = 'ONSITEENDDATE' then
      begin
        l_row.ONSITEENDDATE := rec.node_value;
      exception when others then
        l_row.ONSITEENDDATE := -9999;
        ----htp.p('err: ' || sqlerrm || ',' || rec.xpath); --MAKE THIS A DEBUG FUNCTION FROM HERE SO WE CAN EVENTUALLY LOG ALL ERRORS
      end;
    end if;
    
    if upper(rec.node_name) = 'SUBMITTALDATE' then
      begin
        l_row.SUBMITTALDATE := rec.node_value;
      exception when others then
        l_row.SUBMITTALDATE := -9999;
        ----htp.p('err: ' || sqlerrm || ',' || rec.xpath); --MAKE THIS A DEBUG FUNCTION FROM HERE SO WE CAN EVENTUALLY LOG ALL ERRORS
      end;
    end if;
    
    if upper(rec.node_name) = 'FACILITYNAME' then
      begin
        l_row.FACILITYNAME := rec.node_value;
      exception when others then
        l_row.FACILITYNAME := -9999;
        ----htp.p('err: ' || sqlerrm || ',' || rec.xpath); --MAKE THIS A DEBUG FUNCTION FROM HERE SO WE CAN EVENTUALLY LOG ALL ERRORS
      end;
    end if;
    
    if upper(rec.node_name) = 'FACILITYNO' then
      begin
        l_row.FACILITYNO := rec.node_value;
      exception when others then
        l_row.FACILITYNO := -9999;
        ----htp.p('err: ' || sqlerrm || ',' || rec.xpath); --MAKE THIS A DEBUG FUNCTION FROM HERE SO WE CAN EVENTUALLY LOG ALL ERRORS
      end;
    end if;
    
    if upper(rec.node_name) = 'NFAID' then
      begin
        l_row.NFAID := rec.node_value;
      exception when others then
        l_row.NFAID := -9999;
        ----htp.p('err: ' || sqlerrm || ',' || rec.xpath); --MAKE THIS A DEBUG FUNCTION FROM HERE SO WE CAN EVENTUALLY LOG ALL ERRORS
      end;
    end if;
    
    if upper(rec.node_name) = 'SPIDERS_FACILITYID' then
      begin
        l_row.SPIDERS_FACILITYID := rec.node_value;
      exception when others then
        l_row.SPIDERS_FACILITYID := -9999;
        ----htp.p('err: ' || sqlerrm || ',' || rec.xpath); --MAKE THIS A DEBUG FUNCTION FROM HERE SO WE CAN EVENTUALLY LOG ALL ERRORS
      end;
    end if;
    
    if upper(rec.node_name) = 'COSTESTIMATEFILENAME' then
      begin
        l_row.COSTESTIMATEFILENAME := rec.node_value;
      exception when others then
        l_row.COSTESTIMATEFILENAME := -9999;
        ----htp.p('err: ' || sqlerrm || ',' || rec.xpath); --MAKE THIS A DEBUG FUNCTION FROM HERE SO WE CAN EVENTUALLY LOG ALL ERRORS
      end;
    end if;
    
    if upper(rec.node_name) = 'DD1391FILENAME' then
      begin
        l_row.DD1391FILENAME := rec.node_value;
      exception when others then
        l_row.DD1391FILENAME := -9999;
        ----htp.p('err: ' || sqlerrm || ',' || rec.xpath); --MAKE THIS A DEBUG FUNCTION FROM HERE SO WE CAN EVENTUALLY LOG ALL ERRORS
      end;
    end if;
    
    if upper(rec.node_name) = 'DELIVERYORDERFACILITYID' then
      begin
        l_row.DELIVERYORDERFACILITYID := rec.node_value;
      exception when others then
        l_row.DELIVERYORDERFACILITYID := -9999;
        ----htp.p('err: ' || sqlerrm || ',' || rec.xpath); --MAKE THIS A DEBUG FUNCTION FROM HERE SO WE CAN EVENTUALLY LOG ALL ERRORS
      end;
    end if;
    
  end loop;
  
  pipe row(l_row);

end loop;
  
EXCEPTION WHEN OTHERS THEN
  null; --htp.p into debug (FUNCTIONNAME, INPUT, MESSAGE) values (''' ||  lg_vPackageName ||lc_vFunctionName || ''','''|| 'An error occured while extracting the data from the SPD_T_XML_FLATTEN' ||''',''' || sqlerrm || ''')');
  
end f_XML_FACILITY;


function f_XML_EXIT_BRIEF (in_nID number, in_nPK number, in_nADOID number, in_vCreatedBy varchar2) return XML_EXIT_BRIEFS pipelined
as
  lc_vFunctionName varchar2(255) := 'f_XML_EXIT_BRIEF';
  l_row   XML_EXIT_BRIEF := new XML_EXIT_BRIEF(null, null, null, null, null, null);

/* -- REFERENCE TYPE
  EXITBRIEFID      NUMBER,
	XMLACTIONID      NUMBER,
	EXITBRIEFTIME    VARCHAR2(4000),
	EXITBRIEFLOC     VARCHAR2(4000),
	EXITBRIEFDATE    VARCHAR2(4000),
	CREATED_BY       VARCHAR2(255)
*/
  
-----
vAuthor   varchar2(200) := 'mrussalesi';
vCreated  varchar2(200) := '20180528';
vUpdate   varchar2(200) := '20180528';
----- 
begin

--!!!NOTE THERE IS NO DATA IN THE SAMPLE FILES FOR THIS...USURE OF THE XML NODE NAMES, using upper() to force, may take extra time to do this.
for rec1 in (select * from spd_t_xml_flatten where fpath like '/Action/ExitBrief' and adoid = in_nADOID) 
loop

  for rec in (select (select xmlflattenid from spd_t_xml_flatten where fpath = '/Action' and adoid = in_nADOID) xmlactionid, spd_t_xml_flatten.* from spd_t_xml_flatten where parentid = rec1.node_position order by node_position)  
  loop
  
    --i think we may have to get the PK, using the level - 1, and the parentid, and maybe something else....this may be the tricky part
    --its also possible that we just specifically find the data
    l_row.EXITBRIEFID := rec1.xmlflattenid;
    l_row.XMLACTIONID := rec.xmlactionid;
    l_row.CREATED_BY  := in_vCreatedBy;

    if upper(rec.node_name) = 'EXITBRIEFTIME' then
      begin
        l_row.EXITBRIEFTIME := rec.node_value;
      exception when others then
        l_row.EXITBRIEFTIME := -9999;
        ----htp.p('err: ' || sqlerrm || ',' || rec.xpath); --MAKE THIS A DEBUG FUNCTION FROM HERE SO WE CAN EVENTUALLY LOG ALL ERRORS
      end;
    end if;
    
    if upper(rec.node_name) = 'EXITBRIEFLOC' then
      begin
        l_row.EXITBRIEFLOC := rec.node_value;
      exception when others then
        l_row.EXITBRIEFLOC := -9999;
        ----htp.p('err: ' || sqlerrm || ',' || rec.xpath); --MAKE THIS A DEBUG FUNCTION FROM HERE SO WE CAN EVENTUALLY LOG ALL ERRORS
      end;
    end if;
    
    if upper(rec.node_name) = 'EXITBRIEFDATE' then
      begin
        l_row.EXITBRIEFDATE := rec.node_value;
      exception when others then
        l_row.EXITBRIEFDATE := -9999;
        ----htp.p('err: ' || sqlerrm || ',' || rec.xpath); --MAKE THIS A DEBUG FUNCTION FROM HERE SO WE CAN EVENTUALLY LOG ALL ERRORS
      end;
    end if;
    
  end loop;
  
  pipe row(l_row);
  
end loop;
  
EXCEPTION WHEN OTHERS THEN
  null; --htp.p into debug (FUNCTIONNAME, INPUT, MESSAGE) values (''' ||  lg_vPackageName ||lc_vFunctionName || ''','''|| 'An error occured while extracting the data from the SPD_T_XML_FLATTEN' ||''',''' || sqlerrm || ''')');
  
end f_XML_EXIT_BRIEF;


function f_XML_EXEC_TABLE (in_nID number, in_nPK number, in_nADOID number, in_vCreatedBy varchar2) return XML_EXEC_TABLES pipelined
as
  lc_vFunctionName varchar2(255) := 'f_XML_EXEC_TABLE';
  l_row   XML_EXEC_TABLE := new XML_EXEC_TABLE(null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);

/* -- REFERENCE TYPE
  EXECTABLEID      NUMBER,
	FACILITYID      NUMBER,
	FACNAME      VARCHAR2(4000),
	FACID      NUMBER,
	NFAID      VARCHAR2(4000),
	PRN      NUMBER,
	INSPECTYPE      VARCHAR2(4000),
	INSPECTED      NUMBER,
	ENGRATING      VARCHAR2(4000),
	CIRATING      NUMBER,
	FIVEYEARCI      NUMBER,
	TENYEARCI      NUMBER,
	OPRATING      VARCHAR2(4000),
	RESTRICTIONS      VARCHAR2(4000),
	LOADING      VARCHAR2(4000),
	MOORING      VARCHAR2(4000),
	BERTHING      VARCHAR2(4000),
	REPAIRREC      VARCHAR2(4000),
	USAGEDESC      VARCHAR2(4000),
	CREATED_BY
*/

/*
xml node names:
InspectionType
YearPreviouslyInspected
OverallEngAssmentRatingASCE
OverallCIRating
Overall5YearProjectedCI
Overall10YearProjectedCI
CurrentOperationalRating
OperationalRestrictions
DeckLoading
VesselMooring
VesselBerthing
RepairRecommendations
CurrentFacilityUsageDescription
*/
  
-----
vAuthor   varchar2(200) := 'mrussalesi';
vCreated  varchar2(200) := '20180528';
vUpdate   varchar2(200) := '20180528';
----- 
begin

--!!!NOTE THERE IS NO DATA IN THE SAMPLE FILES FOR THIS...USURE OF THE XML NODE NAMES, using upper() to force, may take extra time to do this.

for rec1 in (select (select max(xmlflattenid) from spd_t_xml_flatten where node_position = flatten.parentid and adoid = in_nADOID) xmlfacilityid, flatten.* from spd_t_xml_flatten flatten where fpath = '/Action/Facilities/Facility/ExecutiveTable' and adoid = in_nADOID) 
loop

  for rec in (select * from spd_t_xml_flatten where parentid = rec1.node_position and adoid = in_nADOID order by node_position) 
  loop
  
    --i think we may have to get the PK, using the level - 1, and the parentid, and maybe something else....this may be the tricky part
    --its also possible that we just specifically find the data
    l_row.EXECTABLEID := rec1.xmlflattenid;
    l_row.FACILITYID := rec1.xmlfacilityid;
    l_row.CREATED_BY  := in_vCreatedBy;

    if upper(rec.node_name) = 'FACNAME' then
      begin
        l_row.FACNAME := rec.node_value;
      exception when others then
        l_row.FACNAME := -9999;
        ----htp.p('err: ' || sqlerrm || ',' || rec.xpath); --MAKE THIS A DEBUG FUNCTION FROM HERE SO WE CAN EVENTUALLY LOG ALL ERRORS
      end;
    end if;
    
    if upper(rec.node_name) = 'FACID' then
      begin
        l_row.FACID := rec.node_value;
      exception when others then
        l_row.FACID := -9999;
        ----htp.p('err: ' || sqlerrm || ',' || rec.xpath); --MAKE THIS A DEBUG FUNCTION FROM HERE SO WE CAN EVENTUALLY LOG ALL ERRORS
      end;
    end if;
    
    if upper(rec.node_name) = 'NFAID' then
      begin
        l_row.NFAID := rec.node_value;
      exception when others then
        l_row.NFAID := -9999;
        ----htp.p('err: ' || sqlerrm || ',' || rec.xpath); --MAKE THIS A DEBUG FUNCTION FROM HERE SO WE CAN EVENTUALLY LOG ALL ERRORS
      end;
    end if;
    
    if upper(rec.node_name) = 'PRN' then
      begin
        l_row.PRN := rec.node_value;
      exception when others then
        l_row.PRN := -9999;
        ----htp.p('err: ' || sqlerrm || ',' || rec.xpath); --MAKE THIS A DEBUG FUNCTION FROM HERE SO WE CAN EVENTUALLY LOG ALL ERRORS
      end;
    end if;
    
    if upper(rec.node_name) = upper('InspectionType') then
      begin
        l_row.INSPECTYPE := rec.node_value;
      exception when others then
        l_row.INSPECTYPE := -9999;
        ----htp.p('err: ' || sqlerrm || ',' || rec.xpath); --MAKE THIS A DEBUG FUNCTION FROM HERE SO WE CAN EVENTUALLY LOG ALL ERRORS
      end;
    end if;
    
    if upper(rec.node_name) = upper('YearPreviouslyInspected') then
      begin
        l_row.INSPECTED := rec.node_value;
      exception when others then
        l_row.INSPECTED := -9999;
        ----htp.p('err: ' || sqlerrm || ',' || rec.xpath); --MAKE THIS A DEBUG FUNCTION FROM HERE SO WE CAN EVENTUALLY LOG ALL ERRORS
      end;
    end if;
    
    if upper(rec.node_name) = upper('OverallCIRating') then
      begin
        l_row.CIRATING := rec.node_value;
      exception when others then
        l_row.CIRATING := -9999;
        ----htp.p('err: ' || sqlerrm || ',' || rec.xpath); --MAKE THIS A DEBUG FUNCTION FROM HERE SO WE CAN EVENTUALLY LOG ALL ERRORS
      end;
    end if;
    
    if upper(rec.node_name) = upper('OverallEngAssmentRatingASCE') then
      begin
        l_row.ENGRATING := rec.node_value;
      exception when others then
        l_row.ENGRATING := -9999;
        ----htp.p('err: ' || sqlerrm || ',' || rec.xpath); --MAKE THIS A DEBUG FUNCTION FROM HERE SO WE CAN EVENTUALLY LOG ALL ERRORS
      end;
    end if;
    
    if upper(rec.node_name) = upper('Overall5YearProjectedCI') then
      begin
        l_row.FIVEYEARCI := rec.node_value;
      exception when others then
        l_row.FIVEYEARCI := -9999;
        ----htp.p('err: ' || sqlerrm || ',' || rec.xpath); --MAKE THIS A DEBUG FUNCTION FROM HERE SO WE CAN EVENTUALLY LOG ALL ERRORS
      end;
    end if;
    
    if upper(rec.node_name) = upper('Overall10YearProjectedCI') then
      begin
        l_row.TENYEARCI := rec.node_value;
      exception when others then
        l_row.TENYEARCI := -9999;
        ----htp.p('err: ' || sqlerrm || ',' || rec.xpath); --MAKE THIS A DEBUG FUNCTION FROM HERE SO WE CAN EVENTUALLY LOG ALL ERRORS
      end;
    end if;
    
    if upper(rec.node_name) = upper('CurrentOperationalRating') then
      begin
        l_row.OPRATING := rec.node_value;
      exception when others then
        l_row.OPRATING := -9999;
        ----htp.p('err: ' || sqlerrm || ',' || rec.xpath); --MAKE THIS A DEBUG FUNCTION FROM HERE SO WE CAN EVENTUALLY LOG ALL ERRORS
      end;
    end if;
    
    if upper(rec.node_name) = 'RESTRICTIONS' then
      begin
        l_row.RESTRICTIONS := rec.node_value;
      exception when others then
        l_row.RESTRICTIONS := -9999;
        ----htp.p('err: ' || sqlerrm || ',' || rec.xpath); --MAKE THIS A DEBUG FUNCTION FROM HERE SO WE CAN EVENTUALLY LOG ALL ERRORS
      end;
    end if;
    
    if upper(rec.node_name) = upper('DeckLoading') then
      begin
        l_row.LOADING := rec.node_value;
      exception when others then
        l_row.LOADING := -9999;
        ----htp.p('err: ' || sqlerrm || ',' || rec.xpath); --MAKE THIS A DEBUG FUNCTION FROM HERE SO WE CAN EVENTUALLY LOG ALL ERRORS
      end;
    end if;
    
    if upper(rec.node_name) = upper('VesselMooring') then
      begin
        l_row.MOORING := rec.node_value;
      exception when others then
        l_row.MOORING := -9999;
        ----htp.p('err: ' || sqlerrm || ',' || rec.xpath); --MAKE THIS A DEBUG FUNCTION FROM HERE SO WE CAN EVENTUALLY LOG ALL ERRORS
      end;
    end if;
    
    if upper(rec.node_name) = upper('VesselBerthing') then
      begin
        l_row.BERTHING := rec.node_value;
      exception when others then
        l_row.BERTHING := -9999;
        ----htp.p('err: ' || sqlerrm || ',' || rec.xpath); --MAKE THIS A DEBUG FUNCTION FROM HERE SO WE CAN EVENTUALLY LOG ALL ERRORS
      end;
    end if;
    
    if upper(rec.node_name) = upper('RepairRecommendations') then
      begin
        l_row.REPAIRREC := rec.node_value;
      exception when others then
        l_row.REPAIRREC := -9999;
        ----htp.p('err: ' || sqlerrm || ',' || rec.xpath); --MAKE THIS A DEBUG FUNCTION FROM HERE SO WE CAN EVENTUALLY LOG ALL ERRORS
      end;
    end if;
    
    if upper(rec.node_name) = upper('CurrentFacilityUsageDescription') then
      begin
        l_row.USAGEDESC := rec.node_value;
      exception when others then
        l_row.USAGEDESC := -9999;
        ----htp.p('err: ' || sqlerrm || ',' || rec.xpath); --MAKE THIS A DEBUG FUNCTION FROM HERE SO WE CAN EVENTUALLY LOG ALL ERRORS
      end;
    end if;
    
  end loop;
  
  pipe row(l_row);

end loop;
  
EXCEPTION WHEN OTHERS THEN
  null; --htp.p into debug (FUNCTIONNAME, INPUT, MESSAGE) values (''' ||  lg_vPackageName ||lc_vFunctionName || ''','''|| 'An error occured while extracting the data from the SPD_T_XML_FLATTEN' ||''',''' || sqlerrm || ''')');
  
end f_XML_EXEC_TABLE;


function f_XML_ASSET_SUMMARY (in_nID number, in_nPK number, in_nADOID number, in_vCreatedBy varchar2) return XML_ASSET_SUMMARIES pipelined
as
  lc_vFunctionName varchar2(255) := 'f_XML_ASSET_SUMMARY';
  l_row   XML_ASSET_SUMMARY := new XML_ASSET_SUMMARY(null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);

/* -- REFERENCE TYPE
  LINEITEMID                NUMBER,
	FACILITYID                NUMBER,
	UNIFORMATIICODE           VARCHAR2(4000),
	DESCRIPTION               VARCHAR2(4000),
	ASSETTYPE                 VARCHAR2(4000),
	FACILITYSECTION           VARCHAR2(4000),
	CRITICALITYPERCENTAGE     VARCHAR2(4000),
	NOOFASSETSCOUNT           NUMBER,
	QUANTITYOFUNITS           NUMBER,
	UNITTYPE                  VARCHAR2(4000),
	CURRENTCI                 NUMBER,
	APPROXIMATEREPAIRCOSTS    VARCHAR2(4000),
	FIVEYEARCIIFREPAIRED      NUMBER,
	TENYEARCIIFREPAIRED       NUMBER,
	FIVEYEARCIIFNOTREPAIRED   NUMBER,
	TENYEARCIIFNOTREPAIRED    NUMBER,
	CREATED_BY                VARCHAR2(255)
*/

/*
xml node names:
*/

/*
usage:
SELECT * FROM TABLE(pkg_rowify.f_XML_ASSET_SUMMARY (in_nID =>0, in_nPK =>12345, in_nADOID =>12346, in_vCreatedBy =>'mike'));
select count(*) from (SELECT * FROM TABLE(pkg_rowify.f_XML_ASSET_SUMMARY (in_nID =>0, in_nPK =>12345, in_nADOID =>12346, in_vCreatedBy =>'mike')));
*/
  
-----
vAuthor   varchar2(200) := 'mrussalesi';
vCreated  varchar2(200) := '20180528';
vUpdate   varchar2(200) := '20180528';
----- 
begin

for rec in (
select 
  parentid,
  adoid,
  max((select xmlflattenid from spd_t_xml_flatten 
          where node_position = flatten.parentid 
            and adoid = in_nADOID))                                                         currentrecordid,
  max((select xmlflattenid from spd_t_xml_flatten 
        where adoid = in_nADOID 
          and node_position = (select parentid from spd_t_xml_flatten 
            where adoid = in_nADOID 
              and node_position = (select parentid from spd_t_xml_flatten 
                where node_position = flatten.parentid 
                  and adoid = in_nADOID))))                                                 parent_xmlflattenid,
  max( case upper(node_name) when 'UNIFORMATIICODE' then node_value else NULL end)          UNIFORMATIICODE, 
  max( case upper(node_name) when 'DESCRIPTION' then node_value else NULL end)              DESCRIPTION,
  max( case upper(node_name) when 'ASSETTYPE' then node_value else NULL end)                ASSETTYPE,
  max( case upper(node_name) when 'FACILITYSECTION' then node_value else NULL end)          FACILITYSECTION, 
  max( case upper(node_name) when 'CRITICALITYPERCENTAGE' then node_value else NULL end)    CRITICALITYPERCENTAGE,
  max( case upper(node_name) when 'NOOFASSETSCOUNT' then node_value else NULL end)          NOOFASSETSCOUNT,
  max( case upper(node_name) when 'QUANTITYOFUNITS' then node_value else NULL end)          QUANTITYOFUNITS, 
  max( case upper(node_name) when 'UNITTYPE' then node_value else NULL end)                 UNITTYPE,
  max( case upper(node_name) when 'CURRENTCI' then node_value else NULL end)                CURRENTCI,
  max( case upper(node_name) when 'APPROXIMATEREPAIRCOSTS' then node_value else NULL end)   APPROXIMATEREPAIRCOSTS, 
  max( case upper(node_name) when 'FIVEYEARCIIFREPAIRED' then node_value else NULL end)     FIVEYEARCIIFREPAIRED,
  max( case upper(node_name) when 'FIVEYEARCIIFNOTREPAIRED' then node_value else NULL end)  FIVEYEARCIIFNOTREPAIRED,
  max( case upper(node_name) when 'TENYEARCIIFREPAIRED' then node_value else NULL end)      TENYEARCIIFREPAIRED,
  max( case upper(node_name) when 'TENYEARCIIFNOTREPAIRED' then node_value else NULL end)   TENYEARCIIFNOTREPAIRED
from
  spd_t_xml_flatten flatten
where
  parentid in (select flatten.node_position from spd_t_xml_flatten flatten where fpath = '/Action/Facilities/Facility/AssetSummary/AssetSummaryLineItem' and adoid = in_nADOID)
  and adoid = in_nADOID
group by 
  parentid,
  adoid
)
loop

  
  l_row := new XML_ASSET_SUMMARY(null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
  
  l_row.LINEITEMID := rec.currentrecordid;
  l_row.FACILITYID := rec.parent_xmlflattenid;
  l_row.CREATED_BY  := in_vCreatedBy;
  
  begin
    l_row.UNIFORMATIICODE := rec.UNIFORMATIICODE;
  exception when others then
    l_row.UNIFORMATIICODE := -9999;
  end;
  
  begin
    l_row.DESCRIPTION := rec.DESCRIPTION;
  exception when others then
    l_row.DESCRIPTION := -9999;
  end;
  
  begin
    l_row.ASSETTYPE := rec.ASSETTYPE;
  exception when others then
    l_row.ASSETTYPE := -9999;
  end;
  
  begin
    l_row.FACILITYSECTION := rec.FACILITYSECTION;
  exception when others then
    l_row.FACILITYSECTION := -9999;
  end;
  
  begin
    l_row.CRITICALITYPERCENTAGE := rec.CRITICALITYPERCENTAGE;
  exception when others then
    l_row.CRITICALITYPERCENTAGE := -9999;
  end;
  
  begin
    l_row.NOOFASSETSCOUNT := rec.NOOFASSETSCOUNT;
  exception when others then
    l_row.NOOFASSETSCOUNT := -9999;
  end;
  
  begin
    l_row.QUANTITYOFUNITS := rec.QUANTITYOFUNITS;
  exception when others then
    l_row.QUANTITYOFUNITS := -9999;
  end;
  
  begin
    l_row.UNITTYPE := rec.UNITTYPE;
  exception when others then
    l_row.UNITTYPE := -9999;
  end;
  
  begin
    l_row.CURRENTCI := rec.CURRENTCI;
  exception when others then
    l_row.CURRENTCI := -9999;
  end;
  
  begin
    l_row.APPROXIMATEREPAIRCOSTS := rec.APPROXIMATEREPAIRCOSTS;
  exception when others then
    l_row.APPROXIMATEREPAIRCOSTS := -9999;
  end;
  
  begin
    l_row.FIVEYEARCIIFREPAIRED := rec.FIVEYEARCIIFREPAIRED;
  exception when others then
    l_row.FIVEYEARCIIFREPAIRED := -9999;
  end;
  
  begin
    l_row.TENYEARCIIFREPAIRED := rec.TENYEARCIIFREPAIRED;
  exception when others then
    l_row.TENYEARCIIFREPAIRED := -9999;
  end;
  
  begin
    l_row.FIVEYEARCIIFNOTREPAIRED := rec.FIVEYEARCIIFNOTREPAIRED;
  exception when others then
    l_row.FIVEYEARCIIFNOTREPAIRED := -9999;
  end;
  
  begin
    l_row.TENYEARCIIFNOTREPAIRED := rec.TENYEARCIIFNOTREPAIRED;
  exception when others then
    l_row.TENYEARCIIFNOTREPAIRED := -9999;
  end; 

  pipe row(l_row);

end loop;

/*
for rec1 in (select (select max(xmlflattenid) from spd_t_xml_flatten where node_position = (select max(parentid) from spd_t_xml_flatten where node_position = flatten.parentid and adoid = in_nADOID)) xmlfacilityid, flatten.* from spd_t_xml_flatten flatten where fpath = '/Action/Facilities/Facility/AssetSummary/AssetSummaryLineItem' and adoid = in_nADOID) 
loop

  for rec in (select * from spd_t_xml_flatten where parentid = rec1.node_position and adoid = in_nADOID order by node_position) 
  loop
  
    --i think we may have to get the PK, using the level - 1, and the parentid, and maybe something else....this may be the tricky part
    --its also possible that we just specifically find the data
    l_row.LINEITEMID := rec1.xmlflattenid;
    l_row.FACILITYID := rec1.xmlfacilityid;
    l_row.CREATED_BY  := in_vCreatedBy;

    if upper(rec.node_name) = upper('UNIFORMATIICODE') then
      begin
        l_row.UNIFORMATIICODE := rec.node_value;
      exception when others then
        l_row.UNIFORMATIICODE := -9999;
        ----htp.p('err: ' || sqlerrm || ',' || rec.xpath); --MAKE THIS A DEBUG FUNCTION FROM HERE SO WE CAN EVENTUALLY LOG ALL ERRORS
      end;
    end if;
    
    if upper(rec.node_name) = upper('DESCRIPTION') then
      begin
        l_row.DESCRIPTION := rec.node_value;
      exception when others then
        l_row.DESCRIPTION := -9999;
        ----htp.p('err: ' || sqlerrm || ',' || rec.xpath); --MAKE THIS A DEBUG FUNCTION FROM HERE SO WE CAN EVENTUALLY LOG ALL ERRORS
      end;
    end if;
    
    if upper(rec.node_name) = upper('ASSETTYPE') then
      begin
        l_row.ASSETTYPE := rec.node_value;
      exception when others then
        l_row.ASSETTYPE := -9999;
        ----htp.p('err: ' || sqlerrm || ',' || rec.xpath); --MAKE THIS A DEBUG FUNCTION FROM HERE SO WE CAN EVENTUALLY LOG ALL ERRORS
      end;
    end if;
    
    if upper(rec.node_name) = upper('FACILITYSECTION') then
      begin
        l_row.FACILITYSECTION := rec.node_value;
      exception when others then
        l_row.FACILITYSECTION := -9999;
        ----htp.p('err: ' || sqlerrm || ',' || rec.xpath); --MAKE THIS A DEBUG FUNCTION FROM HERE SO WE CAN EVENTUALLY LOG ALL ERRORS
      end;
    end if;
    
    if upper(rec.node_name) = upper('CRITICALITYPERCENTAGE') then
      begin
        l_row.CRITICALITYPERCENTAGE := rec.node_value;
      exception when others then
        l_row.CRITICALITYPERCENTAGE := -9999;
        ----htp.p('err: ' || sqlerrm || ',' || rec.xpath); --MAKE THIS A DEBUG FUNCTION FROM HERE SO WE CAN EVENTUALLY LOG ALL ERRORS
      end;
    end if;
    
    if upper(rec.node_name) = upper('NOOFASSETSCOUNT') then
      begin
        l_row.NOOFASSETSCOUNT := rec.node_value;
      exception when others then
        l_row.NOOFASSETSCOUNT := -9999;
        ----htp.p('err: ' || sqlerrm || ',' || rec.xpath); --MAKE THIS A DEBUG FUNCTION FROM HERE SO WE CAN EVENTUALLY LOG ALL ERRORS
      end;
    end if;
    
    if upper(rec.node_name) = upper('QUANTITYOFUNITS') then
      begin
        l_row.QUANTITYOFUNITS := rec.node_value;
      exception when others then
        l_row.QUANTITYOFUNITS := -9999;
        ----htp.p('err: ' || sqlerrm || ',' || rec.xpath); --MAKE THIS A DEBUG FUNCTION FROM HERE SO WE CAN EVENTUALLY LOG ALL ERRORS
      end;
    end if;
    
    if upper(rec.node_name) = upper('UNITTYPE') then
      begin
        l_row.UNITTYPE := rec.node_value;
      exception when others then
        l_row.UNITTYPE := -9999;
        ----htp.p('err: ' || sqlerrm || ',' || rec.xpath); --MAKE THIS A DEBUG FUNCTION FROM HERE SO WE CAN EVENTUALLY LOG ALL ERRORS
      end;
    end if;
    
    if upper(rec.node_name) = upper('CURRENTCI') then
      begin
        l_row.CURRENTCI := rec.node_value;
      exception when others then
        l_row.CURRENTCI := -9999;
        ----htp.p('err: ' || sqlerrm || ',' || rec.xpath); --MAKE THIS A DEBUG FUNCTION FROM HERE SO WE CAN EVENTUALLY LOG ALL ERRORS
      end;
    end if;
    
    if upper(rec.node_name) = upper('APPROXIMATEREPAIRCOSTS') then
      begin
        l_row.APPROXIMATEREPAIRCOSTS := rec.node_value;
      exception when others then
        l_row.APPROXIMATEREPAIRCOSTS := -9999;
        ----htp.p('err: ' || sqlerrm || ',' || rec.xpath); --MAKE THIS A DEBUG FUNCTION FROM HERE SO WE CAN EVENTUALLY LOG ALL ERRORS
      end;
    end if;
    
    if upper(rec.node_name) = upper('FIVEYEARCIIFREPAIRED') then
      begin
        l_row.FIVEYEARCIIFREPAIRED := rec.node_value;
      exception when others then
        l_row.FIVEYEARCIIFREPAIRED := -9999;
        ----htp.p('err: ' || sqlerrm || ',' || rec.xpath); --MAKE THIS A DEBUG FUNCTION FROM HERE SO WE CAN EVENTUALLY LOG ALL ERRORS
      end;
    end if;
    
    if upper(rec.node_name) = upper('TENYEARCIIFNOTREPAIRED') then
      begin
        l_row.TENYEARCIIFNOTREPAIRED := rec.node_value;
      exception when others then
        l_row.TENYEARCIIFNOTREPAIRED := -9999;
        ----htp.p('err: ' || sqlerrm || ',' || rec.xpath); --MAKE THIS A DEBUG FUNCTION FROM HERE SO WE CAN EVENTUALLY LOG ALL ERRORS
      end;
    end if;
    
  end loop;
  
  pipe row(l_row);

end loop;
*/


return;
  
EXCEPTION WHEN OTHERS THEN
  null; --htp.p into debug (FUNCTIONNAME, INPUT, MESSAGE) values (''' ||  lg_vPackageName ||lc_vFunctionName || ''','''|| 'An error occured while extracting the data from the SPD_T_XML_FLATTEN' ||''',''' || sqlerrm || ''')');
  
end f_XML_ASSET_SUMMARY;



function f_XML_ASSET_INVENTORY (in_nID number, in_nPK number, in_nADOID number, in_vCreatedBy varchar2) return XML_ASSET_INVENTORIES pipelined
as
  lc_vFunctionName varchar2(255) := 'f_XML_ASSET_INVENTORY';
  l_row   XML_ASSET_INVENTORY := new XML_ASSET_INVENTORY(null, null, null, null, null, null, null, null, null, null, null, null, null, null);

/* -- REFERENCE TYPE
  INVENTORYID           NUMBER,
	LINEITEMID            NUMBER,
	UNIFORMATIICODE       VARCHAR2(4000),
	UNIFORMATIINAME       VARCHAR2(4000),
	ASSETTYPE             VARCHAR2(4000),
	ASSETTYPEDESCRIPTION  VARCHAR2(4000),
	FACILITYSECTION       VARCHAR2(4000),
	CRITICALITYFACTOR     VARCHAR2(4000),
	NOOFASSETSCOUNT       NUMBER,
	QUANTITYOFUNITS       NUMBER,
	UNITTYPE              VARCHAR2(4000),
	MATERIAL              VARCHAR2(4000),
	MATERAILDESCRIPTION   VARCHAR2(4000),
	CREATED_BY            VARCHAR2(255)
*/

/*
xml node names:
UniFormatIICode
UniFormatIIName
AssetType
AssetTypeDescription
FacilitySection
CriticalityFactor
NoofAssetsCount
QuantityofUnits
UnitType
Material
MaterialDescription
Metadata
*/
  
/*
usage:
SELECT * FROM TABLE(pkg_rowify.f_XML_ASSET_INVENTORY (in_nID =>0, in_nPK =>12345, in_nADOID =>12346, in_vCreatedBy =>'mike'));
select count(*) from (SELECT * FROM TABLE(pkg_rowify.f_XML_ASSET_INVENTORY (in_nID =>0, in_nPK =>12345, in_nADOID =>12346, in_vCreatedBy =>'mike')));
*/
-----
vAuthor   varchar2(200) := 'mrussalesi';
vCreated  varchar2(200) := '20180528';
vUpdate   varchar2(200) := '20180528';
----- 
begin


for rec in (
select 
  parentid,
  adoid,
  max((select xmlflattenid from spd_t_xml_flatten 
          where node_position = flatten.parentid 
            and adoid = in_nADOID))                                                     currentrecordid,
  max((select xmlflattenid from spd_t_xml_flatten 
        where adoid = in_nADOID 
          and node_position = (select parentid from spd_t_xml_flatten 
            where adoid = in_nADOID 
              and node_position = (select parentid from spd_t_xml_flatten 
                where node_position = flatten.parentid 
                  and adoid = in_nADOID))))                                             parent_xmlflattenid,  
  max( case upper(node_name) when 'UNIFORMATIICODE' then node_value else NULL end)      UNIFORMATIICODE,
  max( case upper(node_name) when 'UNIFORMATIINAME' then node_value else NULL end)      UNIFORMATIINAME,
  max( case upper(node_name) when 'ASSETTYPE' then node_value else NULL end)            ASSETTYPE,
  max( case upper(node_name) when 'ASSETTYPEDESCRIPTION' then node_value else NULL end) ASSETTYPEDESCRIPTION,
  max( case upper(node_name) when 'FACILITYSECTION' then node_value else NULL end)      FACILITYSECTION,
  max( case upper(node_name) when 'CRITICALITYFACTOR' then node_value else NULL end)    CRITICALITYFACTOR,
  max( case upper(node_name) when 'NOOFASSETSCOUNT' then node_value else NULL end)      NOOFASSETSCOUNT,
  max( case upper(node_name) when 'QUANTITYOFUNITS' then node_value else NULL end)      QUANTITYOFUNITS,
  max( case upper(node_name) when 'UNITTYPE' then node_value else NULL end)             UNITTYPE,
  max( case upper(node_name) when 'MATERIAL' then node_value else NULL end)             MATERIAL,
  max( case upper(node_name) when 'MATERAILDESCRIPTION' then node_value else NULL end)  MATERAILDESCRIPTION
from
  spd_t_xml_flatten flatten
where
  parentid in (select flatten.node_position from spd_t_xml_flatten flatten where fpath = '/Action/Facilities/Facility/AssetSummary/AssetSummaryLineItem/AssetInventory/AssetInventoryLineItem' and adoid = in_nADOID)
  and adoid = in_nADOID
group by 
  parentid,
  adoid
)
loop

  
  l_row := new XML_ASSET_INVENTORY(null, null, null, null, null, null, null, null, null, null, null, null, null, null);
  
  l_row.LINEITEMID := rec.parent_xmlflattenid;
  l_row.INVENTORYID := rec.currentrecordid;
  l_row.CREATED_BY  := in_vCreatedBy;

  begin
    l_row.UNIFORMATIICODE := rec.UNIFORMATIICODE;
  exception when others then
    l_row.UNIFORMATIICODE := -9999;
  end; 

  begin
    l_row.UNIFORMATIINAME := rec.UNIFORMATIINAME;
  exception when others then
    l_row.UNIFORMATIINAME := -9999;
  end; 

  begin
    l_row.ASSETTYPE := rec.ASSETTYPE;
  exception when others then
    l_row.ASSETTYPE := -9999;
  end; 

  begin
    l_row.ASSETTYPEDESCRIPTION := rec.ASSETTYPEDESCRIPTION;
  exception when others then
    l_row.ASSETTYPEDESCRIPTION := -9999;
  end; 

  begin
    l_row.FACILITYSECTION := rec.FACILITYSECTION;
  exception when others then
    l_row.FACILITYSECTION := -9999;
  end; 

  begin
    l_row.CRITICALITYFACTOR := rec.CRITICALITYFACTOR;
  exception when others then
    l_row.CRITICALITYFACTOR := -9999;
  end; 

  begin
    l_row.NOOFASSETSCOUNT := rec.NOOFASSETSCOUNT;
  exception when others then
    l_row.NOOFASSETSCOUNT := -9999;
  end; 

  begin
    l_row.QUANTITYOFUNITS := rec.QUANTITYOFUNITS;
  exception when others then
    l_row.QUANTITYOFUNITS := -9999;
  end; 

  begin
    l_row.UNITTYPE := rec.UNITTYPE;
  exception when others then
    l_row.UNITTYPE := -9999;
  end; 

  begin
    l_row.MATERIAL := rec.MATERIAL;
  exception when others then
    l_row.MATERIAL := -9999;
  end; 

  begin
    l_row.MATERAILDESCRIPTION := rec.MATERAILDESCRIPTION;
  exception when others then
    l_row.MATERAILDESCRIPTION := -9999;
  end; 

  pipe row(l_row);

end loop;

/*
for rec1 in (select 
  (select max(xmlflattenid) from spd_t_xml_flatten 
      where node_position = (select max(parentid) from spd_t_xml_flatten 
        where node_position = flatten.parentid and adoid = in_nADOID)) assetsummaryid, 
  flatten.* from spd_t_xml_flatten flatten where fpath = '/Action/Facilities/Facility/AssetSummary/AssetSummaryLineItem/AssetInventory/AssetInventoryLineItem' and adoid = in_nADOID
  ) 
loop

  for rec in (select * from spd_t_xml_flatten where parentid = rec1.node_position and adoid = in_nADOID order by node_position) 
  loop
  
    --i think we may have to get the PK, using the level - 1, and the parentid, and maybe something else....this may be the tricky part
    --its also possible that we just specifically find the data
    l_row.LINEITEMID := rec1.assetsummaryid;
    l_row.INVENTORYID := rec1.xmlflattenid;
    l_row.CREATED_BY  := in_vCreatedBy;

    if upper(rec.node_name) = upper('UniFormatIICode') then
      begin
        l_row.UNIFORMATIICODE := rec.node_value;
      exception when others then
        l_row.UNIFORMATIICODE := -9999;
        ----htp.p('err: ' || sqlerrm || ',' || rec.xpath); --MAKE THIS A DEBUG FUNCTION FROM HERE SO WE CAN EVENTUALLY LOG ALL ERRORS
      end;
    end if;
    
    if upper(rec.node_name) = upper('UniFormatIIName') then
      begin
        l_row.UNIFORMATIINAME := rec.node_value;
      exception when others then
        l_row.UNIFORMATIINAME := -9999;
        ----htp.p('err: ' || sqlerrm || ',' || rec.xpath); --MAKE THIS A DEBUG FUNCTION FROM HERE SO WE CAN EVENTUALLY LOG ALL ERRORS
      end;
    end if;
    
    if upper(rec.node_name) = upper('AssetType') then
      begin
        l_row.ASSETTYPE := rec.node_value;
      exception when others then
        l_row.ASSETTYPE := -9999;
        ----htp.p('err: ' || sqlerrm || ',' || rec.xpath); --MAKE THIS A DEBUG FUNCTION FROM HERE SO WE CAN EVENTUALLY LOG ALL ERRORS
      end;
    end if;
    
    if upper(rec.node_name) = upper('AssetTypeDescription') then
      begin
        l_row.ASSETTYPEDESCRIPTION := rec.node_value;
      exception when others then
        l_row.ASSETTYPEDESCRIPTION := -9999;
        ----htp.p('err: ' || sqlerrm || ',' || rec.xpath); --MAKE THIS A DEBUG FUNCTION FROM HERE SO WE CAN EVENTUALLY LOG ALL ERRORS
      end;
    end if;
    
    if upper(rec.node_name) = upper('FacilitySection') then
      begin
        l_row.FACILITYSECTION := rec.node_value;
      exception when others then
        l_row.FACILITYSECTION := -9999;
        ----htp.p('err: ' || sqlerrm || ',' || rec.xpath); --MAKE THIS A DEBUG FUNCTION FROM HERE SO WE CAN EVENTUALLY LOG ALL ERRORS
      end;
    end if;
    
    if upper(rec.node_name) = upper('CriticalityFactor') then
      begin
        l_row.CRITICALITYFACTOR := rec.node_value;
      exception when others then
        l_row.CRITICALITYFACTOR := -9999;
        ----htp.p('err: ' || sqlerrm || ',' || rec.xpath); --MAKE THIS A DEBUG FUNCTION FROM HERE SO WE CAN EVENTUALLY LOG ALL ERRORS
      end;
    end if;
    
    if upper(rec.node_name) = upper('NoofAssetsCount') then
      begin
        l_row.NOOFASSETSCOUNT := rec.node_value;
      exception when others then
        l_row.NOOFASSETSCOUNT := -9999;
        ----htp.p('err: ' || sqlerrm || ',' || rec.xpath); --MAKE THIS A DEBUG FUNCTION FROM HERE SO WE CAN EVENTUALLY LOG ALL ERRORS
      end;
    end if;
    
    if upper(rec.node_name) = upper('QuantityofUnits') then
      begin
        l_row.QUANTITYOFUNITS := rec.node_value;
      exception when others then
        l_row.QUANTITYOFUNITS := -9999;
        ----htp.p('err: ' || sqlerrm || ',' || rec.xpath); --MAKE THIS A DEBUG FUNCTION FROM HERE SO WE CAN EVENTUALLY LOG ALL ERRORS
      end;
    end if;
    
    if upper(rec.node_name) = upper('UnitType') then
      begin
        l_row.UNITTYPE := rec.node_value;
      exception when others then
        l_row.UNITTYPE := -9999;
        ----htp.p('err: ' || sqlerrm || ',' || rec.xpath); --MAKE THIS A DEBUG FUNCTION FROM HERE SO WE CAN EVENTUALLY LOG ALL ERRORS
      end;
    end if;
    
    if upper(rec.node_name) = upper('Material') then
      begin
        l_row.MATERIAL := rec.node_value;
      exception when others then
        l_row.MATERIAL := -9999;
        ----htp.p('err: ' || sqlerrm || ',' || rec.xpath); --MAKE THIS A DEBUG FUNCTION FROM HERE SO WE CAN EVENTUALLY LOG ALL ERRORS
      end;
    end if;
    
    if upper(rec.node_name) = upper('MaterialDescription') then
      begin
        l_row.MATERAILDESCRIPTION := rec.node_value;
      exception when others then
        l_row.MATERAILDESCRIPTION := -9999;
        ----htp.p('err: ' || sqlerrm || ',' || rec.xpath); --MAKE THIS A DEBUG FUNCTION FROM HERE SO WE CAN EVENTUALLY LOG ALL ERRORS
      end;
    end if;
    
  end loop;
  
  pipe row(l_row);

end loop;
*/


EXCEPTION WHEN OTHERS THEN
  null; --htp.p into debug (FUNCTIONNAME, INPUT, MESSAGE) values (''' ||  lg_vPackageName ||lc_vFunctionName || ''','''|| 'An error occured while extracting the data from the SPD_T_XML_FLATTEN' ||''',''' || sqlerrm || ''')');
  
end f_XML_ASSET_INVENTORY;



function f_XML_DEFECT (in_nID number, in_nPK number, in_nADOID number, in_vCreatedBy varchar2) return XML_DEFECTS pipelined
as
  lc_vFunctionName varchar2(255) := 'f_XML_DEFECT';
  l_row   XML_DEFECT := new XML_DEFECT(null, null, null, null, null, null, null, null, null);

/* -- REFERENCE TYPE
  DEFECTID          NUMBER,
	INVENTORYID       NUMBER,
	DEFECTIDNO        VARCHAR2(4000),
	DEFECTLOCATION    VARCHAR2(4000),
	DEFECTCENTER      VARCHAR2(4000),
	CRITICALITYFACTOR VARCHAR2(4000),
	DEFECTTYPE        VARCHAR2(4000),
	SRMCLASSIFICATION VARCHAR2(4000),
	CREATED_BY        VARCHAR2(255)
*/

/*
xml node names:

*/

/*
usage:
SELECT * FROM TABLE(pkg_rowify.f_XML_DEFECT (in_nID =>0, in_nPK =>12345, in_nADOID =>12346, in_vCreatedBy =>'mike'));
select count(*) from (SELECT * FROM TABLE(pkg_rowify.f_XML_DEFECT (in_nID =>0, in_nPK =>12345, in_nADOID =>12346, in_vCreatedBy =>'mike')));

*/
  
-----
vAuthor   varchar2(200) := 'mrussalesi';
vCreated  varchar2(200) := '20180528';
vUpdate   varchar2(200) := '20180528';
----- 
begin


for rec in (
select 
  parentid,
  adoid,
  max((select xmlflattenid from spd_t_xml_flatten 
          where node_position = flatten.parentid 
            and adoid = in_nADOID))                                                   currentrecordid,
  max((select xmlflattenid from spd_t_xml_flatten 
        where adoid = in_nADOID 
          and node_position = (select parentid from spd_t_xml_flatten 
            where adoid = in_nADOID 
              and node_position = (select parentid from spd_t_xml_flatten 
                where node_position = flatten.parentid 
                  and adoid = in_nADOID))))                                           parent_xmlflattenid,
  max( case upper(node_name) when 'DEFECTIDNO' then node_value else NULL end)         DEFECTIDNO,
  max( case upper(node_name) when 'DEFECTLOCATION' then node_value else NULL end)     DEFECTLOCATION,
  max( case upper(node_name) when 'DEFECTCENTER' then node_value else NULL end)       DEFECTCENTER,
  max( case upper(node_name) when 'CRITICALITYFACTOR' then node_value else NULL end)  CRITICALITYFACTOR,
  max( case upper(node_name) when 'DEFECTTYPE' then node_value else NULL end)         DEFECTTYPE,
  max( case upper(node_name) when 'SRMCLASSIFICATION' then node_value else NULL end)  SRMCLASSIFICATION
from
  spd_t_xml_flatten flatten
where
  parentid in (select flatten.node_position from spd_t_xml_flatten flatten where fpath = '/Action/Facilities/Facility/AssetSummary/AssetSummaryLineItem/AssetInventory/AssetInventoryLineItem/Defects/Defect' and adoid = in_nADOID)
  and adoid = in_nADOID
group by 
  parentid,
  adoid
)
loop

  
  l_row := new XML_DEFECT(null, null, null, null, null, null, null, null, null);
  
  l_row.INVENTORYID := rec.parent_xmlflattenid;
  l_row.DEFECTID := rec.currentrecordid;
  l_row.CREATED_BY  := in_vCreatedBy;

  begin
    l_row.DEFECTIDNO := rec.DEFECTIDNO;
  exception when others then
    l_row.DEFECTIDNO := -9999;
  end; 

  begin
    l_row.DEFECTLOCATION := rec.DEFECTLOCATION;
  exception when others then
    l_row.DEFECTLOCATION := -9999;
  end; 

  begin
    l_row.DEFECTCENTER := rec.DEFECTCENTER;
  exception when others then
    l_row.DEFECTCENTER := -9999;
  end; 

  begin
    l_row.CRITICALITYFACTOR := rec.CRITICALITYFACTOR;
  exception when others then
    l_row.CRITICALITYFACTOR := -9999;
  end; 

  begin
    l_row.DEFECTTYPE := rec.DEFECTTYPE;
  exception when others then
    l_row.DEFECTTYPE := -9999;
  end; 

  begin
    l_row.SRMCLASSIFICATION := rec.SRMCLASSIFICATION;
  exception when others then
    l_row.SRMCLASSIFICATION := -9999;
  end; 

  pipe row(l_row);

end loop;

  
EXCEPTION WHEN OTHERS THEN
  null; --htp.p into debug (FUNCTIONNAME, INPUT, MESSAGE) values (''' ||  lg_vPackageName ||lc_vFunctionName || ''','''|| 'An error occured while extracting the data from the SPD_T_XML_FLATTEN' ||''',''' || sqlerrm || ''')');
  
end f_XML_DEFECT;

function getID(in_nADOID number, in_nLevel number, in_nParentid number) return number as 

  lc_nReturn number :=0;
begin

if in_nLevel = 1 then
  begin
    select xmlflattenid into lc_nReturn from spd_t_xml_flatten 
      where node_position = in_nParentid 
        and adoid = in_nADOID;
  exception when others then
    lc_nReturn := -99999;
  end;
end if;

if in_nLevel = 2 then
  begin
    select xmlflattenid into lc_nReturn from spd_t_xml_flatten 
        where adoid = in_nADOID 
          and node_position = (select parentid from spd_t_xml_flatten 
            where adoid = in_nADOID 
              and node_position = (select parentid from spd_t_xml_flatten 
                where node_position = in_nParentid 
                  and adoid = in_nADOID));
  exception when others then
    lc_nReturn := -99999;
  end;
end if;
    
end getID;


function f_XML_DEFECT_METADATA (in_nID number, in_nPK number, in_nADOID number, in_vCreatedBy varchar2) return XML_DEFECT_METADATAS pipelined
as
  lc_vFunctionName varchar2(255) := 'f_XML_DEFECT_METADATA';
  l_row   XML_DEFECT_METADATA := new XML_DEFECT_METADATA(null, null, null, null, null, null);

/* -- REFERENCE TYPE
  METADATAID      NUMBER,
	DEFECTID      NUMBER,
	DISPLAY      VARCHAR2(4000),
	VALUE      VARCHAR2(4000),
	VISUALORDER      VARCHAR2(4000),
	CREATED_BY      VARCHAR2(255)
*/

/*
xml node names:

*/
  
-----
vAuthor   varchar2(200) := 'mrussalesi';
vCreated  varchar2(200) := '20180528';
vUpdate   varchar2(200) := '20180528';
----- 
begin

--!!!NOTE THERE IS NO DATA IN THE SAMPLE FILES FOR THIS...USURE OF THE XML NODE NAMES, using upper() to force, may take extra time to do this.
/*
i tried to get the parent id's as functions, but i got this PLS-00231: function GETID may not be used in SQL. may look into it later - MR 20180531
*/

for rec in (
select 
  parentid,
  adoid,
  max((select xmlflattenid from spd_t_xml_flatten 
          where node_position = flatten.parentid 
            and adoid = in_nADOID))                                     metadataid,
  max((select xmlflattenid from spd_t_xml_flatten 
        where adoid = in_nADOID 
          and node_position = (select parentid from spd_t_xml_flatten 
            where adoid = in_nADOID 
              and node_position = (select parentid from spd_t_xml_flatten 
                where node_position = flatten.parentid 
                  and adoid = in_nADOID))))                             defectid,
  max( case node_name when 'display' then node_value else NULL end)     display, 
  max( case node_name when 'value' then node_value else NULL end)       value,
  max( case node_name when 'visualorder' then node_value else NULL end) visualorder
from
  spd_t_xml_flatten flatten
where
  parentid in (select flatten.node_position from spd_t_xml_flatten flatten where fpath = '/Action/Facilities/Facility/AssetSummary/AssetSummaryLineItem/AssetInventory/AssetInventoryLineItem/Defects/Defect/metadata/LineItem' and adoid = in_nADOID)
  and adoid = in_nADOID
group by 
  parentid,
  adoid
)
loop

  l_row := new XML_DEFECT_METADATA(null, null, null, null, null, null);
  
  l_row.DEFECTID := rec.defectid;
  l_row.METADATAID := rec.metadataid;
  l_row.CREATED_BY  := in_vCreatedBy;
  
  begin
    l_row.DISPLAY := rec.DISPLAY;
  exception when others then
    l_row.DISPLAY := -9999;
  end;
  
  begin
    l_row.VALUE := rec.VALUE;
  exception when others then
    l_row.VALUE := -9999;
  end;
  
  begin
    l_row.visualorder := rec.visualorder;
  exception when others then
    l_row.visualorder := -9999;
  end;


  pipe row(l_row);

end loop;
  
EXCEPTION WHEN OTHERS THEN
  null; --htp.p into debug (FUNCTIONNAME, INPUT, MESSAGE) values (''' ||  lg_vPackageName ||lc_vFunctionName || ''','''|| 'An error occured while extracting the data from the SPD_T_XML_FLATTEN' ||''',''' || sqlerrm || ''')');
  
end f_XML_DEFECT_METADATA;

function f_XML_EXEC_SUMMARY (in_nID number, in_nPK number, in_nADOID number, in_vCreatedBy varchar2) return XML_EXEC_SUMMARIES pipelined
as
  lc_vFunctionName varchar2(255) := 'f_XML_EXEC_SUMMARY';
  l_row   XML_EXEC_SUMMARY := new XML_EXEC_SUMMARY(null, null, null);

/* -- REFERENCE TYPE
  EXECSUMMARYID   NUMBER,
	XMLACTIONID     NUMBER,
	CREATED_BY      VARCHAR2(255
*/
  
-----
vAuthor   varchar2(200) := 'mrussalesi';
vCreated  varchar2(200) := '20180528';
vUpdate   varchar2(200) := '20180528';
----- 
begin

--!!!NOTE THERE IS NO DATA IN THE SAMPLE FILES FOR THIS...USURE OF THE XML NODE NAMES, using upper() to force, may take extra time to do this.

for rec1 in (select * from spd_t_xml_flatten where fpath = '/Action/ExecutiveSummary' and adoid = in_nADOID) 
loop

  for rec in (select (select xmlflattenid from spd_t_xml_flatten where fpath = '/Action' and adoid = in_nADOID) xmlactionid, spd_t_xml_flatten.* from spd_t_xml_flatten where parentid = rec1.node_position order by node_position) 
  loop
  
    --i think we may have to get the PK, using the level - 1, and the parentid, and maybe something else....this may be the tricky part
    --its also possible that we just specifically find the data
  
    l_row.XMLACTIONID := rec.xmlactionid;
    l_row.EXECSUMMARYID := rec1.xmlflattenid;
    l_row.CREATED_BY  := in_vCreatedBy;
    
  end loop;
  
  pipe row(l_row);

end loop;
  
EXCEPTION WHEN OTHERS THEN
  null; --htp.p into debug (FUNCTIONNAME, INPUT, MESSAGE) values (''' ||  lg_vPackageName ||lc_vFunctionName || ''','''|| 'An error occured while extracting the data from the SPD_T_XML_FLATTEN' ||''',''' || sqlerrm || ''')');
  
end f_XML_EXEC_SUMMARY;


function f_XML_ES_PARAGRAPH (in_nID number, in_nPK number, in_nADOID number, in_vCreatedBy varchar2) return XML_ES_PARAGRAPHS pipelined
as
  lc_vFunctionName varchar2(255) := 'f_XML_ES_PARAGRAPH';
  l_row   XML_ES_PARAGRAPH := new XML_ES_PARAGRAPH(null, null, null);

/* -- REFERENCE TYPE
  PARAGRAPHID     NUMBER,
	EXECSUMMARYID   NUMBER,
	CREATED_BY      VARCHAR2(255
*/
  
-----
vAuthor   varchar2(200) := 'mrussalesi';
vCreated  varchar2(200) := '20180528';
vUpdate   varchar2(200) := '20180528';
----- 
begin

--!!!NOTE THERE IS NO DATA IN THE SAMPLE FILES FOR THIS...USURE OF THE XML NODE NAMES, using upper() to force, may take extra time to do this.

  for rec1 in (select (select max(xmlflattenid) from spd_t_xml_flatten where node_position = flatten.parentid and adoid = in_nADOID) execsummaryid, flatten.* from spd_t_xml_flatten flatten where fpath = '/Action/ExecutiveSummary/Paragraph' and adoid = in_nADOID) 
  loop

  --(select xmlflattenid from spd_t_xml_flatten node_position = flatten.parentid) paragraphid,
    --i think we may have to get the PK, using the level - 1, and the parentid, and maybe something else....this may be the tricky part
    --its also possible that we just specifically find the data
    
    l_row.EXECSUMMARYID := rec1.execsummaryid;
    l_row.PARAGRAPHID := rec1.xmlflattenid;
    l_row.CREATED_BY  := in_vCreatedBy;
    
    pipe row(l_row);

  end loop;
  
EXCEPTION WHEN OTHERS THEN
  null; --htp.p into debug (FUNCTIONNAME, INPUT, MESSAGE) values (''' ||  lg_vPackageName ||lc_vFunctionName || ''','''|| 'An error occured while extracting the data from the SPD_T_XML_FLATTEN' ||''',''' || sqlerrm || ''')');
  
end f_XML_ES_PARAGRAPH;


function f_XML_SENTENCE (in_nID number, in_nPK number, in_nADOID number, in_vCreatedBy varchar2) return XML_SENTENCES pipelined
as
  lc_vFunctionName varchar2(255) := 'f_XML_SENTENCE';
  l_row   XML_SENTENCE := new XML_SENTENCE(null, null, null, null);

/* -- REFERENCE TYPE
  SENTENCEID    NUMBER,
	PARAGRAPHID   NUMBER,
	SENTENCE      VARCHAR2(4000),
	CREATED_BY    VARCHAR2(255)
*/

/*
usage:
SELECT * FROM TABLE(pkg_rowify.f_XML_SENTENCE (in_nID =>0, in_nPK =>12345, in_nADOID =>12346, in_vCreatedBy =>'mike'));
select count(*) from (SELECT * FROM TABLE(pkg_rowify.f_XML_SENTENCE (in_nID =>0, in_nPK =>12345, in_nADOID =>12346, in_vCreatedBy =>'mike')));
*/
  
-----
vAuthor   varchar2(200) := 'mrussalesi';
vCreated  varchar2(200) := '20180528';
vUpdate   varchar2(200) := '20180528';
----- 
begin

for rec in (
select 
  (select xmlflattenid from spd_t_xml_flatten 
          where node_position = flatten.parentid 
            and adoid = in_nADOID)                                                parent_xmlflattenid,
  adoid                                                                       adoid,
  xmlflattenid                                                                currentrecordid,                                          
  (case upper(node_name) when 'SENTENCE' then node_value else NULL end)       SENTENCE
from
  spd_t_xml_flatten flatten
where
  parentid in (select flatten.node_position from spd_t_xml_flatten flatten where fpath = '/Action/ExecutiveSummary/Paragraph' and adoid = in_nADOID)
  and adoid = in_nADOID
order by xmlflattenid asc
)
loop

  l_row := new XML_SENTENCE(null, null, null, null);
  
  l_row.PARAGRAPHID := rec.parent_xmlflattenid;
  l_row.SENTENCEID := rec.currentrecordid;
  l_row.CREATED_BY  := in_vCreatedBy;

  begin
    l_row.SENTENCE := rec.SENTENCE;
  exception when others then
    l_row.SENTENCE := -9999;
  end; 

  pipe row(l_row);

end loop;

/*
for rec1 in (select * from spd_t_xml_flatten where fpath = '/Action/ExecutiveSummary/Paragraph' and adoid = in_nADOID) 
loop

  for rec in (select flatten.* from spd_t_xml_flatten flatten where parentid = rec1.node_position order by node_position) 
  loop
  
    --i think we may have to get the PK, using the level - 1, and the parentid, and maybe something else....this may be the tricky part
    --its also possible that we just specifically find the data
    
    l_row.PARAGRAPHID := rec1.xmlflattenid;
    l_row.SENTENCEID := rec.xmlflattenid;
    l_row.CREATED_BY  := in_vCreatedBy;
    
    if upper(rec.node_name) = upper('SENTENCE') then
      begin
        l_row.SENTENCE := rec.node_value;
      exception when others then
        l_row.SENTENCE := -9999;
        ----htp.p('err: ' || sqlerrm || ',' || rec.xpath); --MAKE THIS A DEBUG FUNCTION FROM HERE SO WE CAN EVENTUALLY LOG ALL ERRORS
      end;
    end if;
    
    pipe row(l_row);

  end loop;
  
end loop;
*/


EXCEPTION WHEN OTHERS THEN
  null; --htp.p into debug (FUNCTIONNAME, INPUT, MESSAGE) values (''' ||  lg_vPackageName ||lc_vFunctionName || ''','''|| 'An error occured while extracting the data from the SPD_T_XML_FLATTEN' ||''',''' || sqlerrm || ''')');
  
end f_XML_SENTENCE;


function f_XML_AI_FITTING (in_nID number, in_nPK number, in_nADOID number, in_vCreatedBy varchar2) return XML_AI_FITTINGS pipelined
as
  lc_vFunctionName varchar2(255) := 'f_XML_AI_FITTING';
  l_row   XML_AI_FITTING := new XML_AI_FITTING(null, null, null, null, null, null, null, null, null, null, null, null);

/* -- REFERENCE TYPE
  FITTINGID      NUMBER,
	INVENTORYID      NUMBER,
	ASSETTYPE      VARCHAR2(4000),
	FITTINGIDNUMBER      VARCHAR2(4000),
	TYPE      VARCHAR2(4000),
	DESIGNCAPACITY      VARCHAR2(4000),
	RATINGCAPACITY      VARCHAR2(4000),
	CONDITIONFITTING      NUMBER,
	CONDITIONBASE      NUMBER,
	CONDITIONCONNECTIONHARDWARE      NUMBER,
	PHOTOFILENAME      VARCHAR2(4000),
	CREATED_BY      VARCHAR2(255)
*/

/*
usage:
SELECT * FROM TABLE(pkg_rowify.f_XML_AI_FITTING (in_nID =>0, in_nPK =>12345, in_nADOID =>12346, in_vCreatedBy =>'mike'));
select count(*) from (SELECT * FROM TABLE(pkg_rowify.f_XML_AI_FITTING (in_nID =>0, in_nPK =>12345, in_nADOID =>12346, in_vCreatedBy =>'mike')));
*/
  
-----
vAuthor   varchar2(200) := 'mrussalesi';
vCreated  varchar2(200) := '20180528';
vUpdate   varchar2(200) := '20180528';
----- 
begin



for rec in (
select 
  parentid,
  adoid,
  max((select xmlflattenid from spd_t_xml_flatten 
          where node_position = flatten.parentid 
            and adoid = in_nADOID))                                                   currentrecordid,
  max((select xmlflattenid from spd_t_xml_flatten 
        where adoid = in_nADOID 
          and node_position = (select parentid from spd_t_xml_flatten 
            where adoid = in_nADOID 
              and node_position = (select parentid from spd_t_xml_flatten 
                where node_position = flatten.parentid 
                  and adoid = in_nADOID))))                                           parent_xmlflattenid,
  max( case upper(node_name) when 'ASSETTYPE' then node_value else NULL end)                    ASSETTYPE,
  max( case upper(node_name) when 'FITTINGIDNUMBER' then node_value else NULL end)              FITTINGIDNUMBER,
  max( case upper(node_name) when 'TYPE' then node_value else NULL end)                         TYPE,
  max( case upper(node_name) when 'DESIGNCAPACITY' then node_value else NULL end)               DESIGNCAPACITY,
  max( case upper(node_name) when 'RATINGCAPACITY' then node_value else NULL end)               RATINGCAPACITY,
  max( case upper(node_name) when 'CONDITIONFITTING' then node_value else NULL end)             CONDITIONFITTING,
  max( case upper(node_name) when 'CONDITIONBASE' then node_value else NULL end)                CONDITIONBASE,
  max( case upper(node_name) when 'CONDITIONCONNECTIONHARDWARE' then node_value else NULL end)  CONDITIONCONNECTIONHARDWARE,
  max( case upper(node_name) when 'PHOTOFILENAME' then node_value else NULL end)                PHOTOFILENAME
from
  spd_t_xml_flatten flatten
where
  parentid in (select flatten.node_position from spd_t_xml_flatten flatten where fpath = '/Action/Facilities/Facility/AssetSummary/AssetSummaryLineItem/AssetInventory/AssetInventoryLineItem/MooringFittings/MooringFitting' and adoid = in_nADOID)
  and adoid = in_nADOID
group by 
  parentid,
  adoid
)
loop

  l_row := new XML_AI_FITTING(null, null, null, null, null, null, null, null, null, null, null, null);
  
  l_row.INVENTORYID := rec.parent_xmlflattenid;
  l_row.FITTINGID := rec.currentrecordid;
  l_row.CREATED_BY  := in_vCreatedBy;

  begin
    l_row.ASSETTYPE := rec.ASSETTYPE;
  exception when others then
    l_row.ASSETTYPE := -9999;
  end; 

  begin
    l_row.FITTINGIDNUMBER := rec.FITTINGIDNUMBER;
  exception when others then
    l_row.FITTINGIDNUMBER := -9999;
  end; 

  begin
    l_row.TYPE := rec.TYPE;
  exception when others then
    l_row.TYPE := -9999;
  end; 

  begin
    l_row.DESIGNCAPACITY := rec.DESIGNCAPACITY;
  exception when others then
    l_row.DESIGNCAPACITY := -9999;
  end; 

  begin
    l_row.RATINGCAPACITY := rec.RATINGCAPACITY;
  exception when others then
    l_row.RATINGCAPACITY := -9999;
  end; 

  begin
    l_row.CONDITIONFITTING := rec.CONDITIONFITTING;
  exception when others then
    l_row.CONDITIONFITTING := -9999;
  end; 

  begin
    l_row.CONDITIONBASE := rec.CONDITIONBASE;
  exception when others then
    l_row.CONDITIONBASE := -9999;
  end; 

  begin
    l_row.CONDITIONCONNECTIONHARDWARE := rec.CONDITIONCONNECTIONHARDWARE;
  exception when others then
    l_row.CONDITIONCONNECTIONHARDWARE := -9999;
  end; 

  begin
    l_row.PHOTOFILENAME := rec.PHOTOFILENAME;
  exception when others then
    l_row.PHOTOFILENAME := -9999;
  end; 


  pipe row(l_row);

end loop;


/*
for rec1 in (
select 
  (select max(xmlflattenid) from spd_t_xml_flatten 
      where node_position = (select max(parentid) from spd_t_xml_flatten 
        where node_position = flatten.parentid and adoid = in_nADOID)) assetinventoryid, 
  flatten.* from spd_t_xml_flatten flatten where fpath = '/Action/Facilities/Facility/AssetSummary/AssetSummaryLineItem/AssetInventory/AssetInventoryLineItem/MooringFittings/MooringFitting' and adoid = in_nADOID
 ) 
loop

  for rec in (select * from spd_t_xml_flatten where parentid = rec1.node_position and adoid = in_nADOID order by node_position) 
  loop
  
    --i think we may have to get the PK, using the level - 1, and the parentid, and maybe something else....this may be the tricky part
    --its also possible that we just specifically find the data
  
    l_row.INVENTORYID := rec1.assetinventoryid;
    l_row.FITTINGID := rec.xmlflattenid;
    l_row.CREATED_BY  := in_vCreatedBy;

    if upper(rec.node_name) = 'FITTINGID' then
      begin
        l_row.FITTINGID := rec.node_value;
      exception when others then
        l_row.FITTINGID := -9999;
        ----htp.p('err: ' || sqlerrm || ',' || rec.xpath); --MAKE THIS A DEBUG FUNCTION FROM HERE SO WE CAN EVENTUALLY LOG ALL ERRORS
      end;
    end if;
    
    if upper(rec.node_name) = 'ASSETTYPE' then
      begin
        l_row.ASSETTYPE := rec.node_value;
      exception when others then
        l_row.ASSETTYPE := -9999;
        ----htp.p('err: ' || sqlerrm || ',' || rec.xpath); --MAKE THIS A DEBUG FUNCTION FROM HERE SO WE CAN EVENTUALLY LOG ALL ERRORS
      end;
    end if;
    
    if upper(rec.node_name) = 'FITTINGIDNUMBER' then
      begin
        l_row.FITTINGIDNUMBER := rec.node_value;
      exception when others then
        l_row.FITTINGIDNUMBER := -9999;
        ----htp.p('err: ' || sqlerrm || ',' || rec.xpath); --MAKE THIS A DEBUG FUNCTION FROM HERE SO WE CAN EVENTUALLY LOG ALL ERRORS
      end;
    end if;
    
    if upper(rec.node_name) = 'TYPE' then
      begin
        l_row.TYPE := rec.node_value;
      exception when others then
        l_row.TYPE := -9999;
        ----htp.p('err: ' || sqlerrm || ',' || rec.xpath); --MAKE THIS A DEBUG FUNCTION FROM HERE SO WE CAN EVENTUALLY LOG ALL ERRORS
      end;
    end if;
    
    if upper(rec.node_name) = 'DESIGNCAPACITY' then
      begin
        l_row.DESIGNCAPACITY := rec.node_value;
      exception when others then
        l_row.DESIGNCAPACITY := -9999;
        ----htp.p('err: ' || sqlerrm || ',' || rec.xpath); --MAKE THIS A DEBUG FUNCTION FROM HERE SO WE CAN EVENTUALLY LOG ALL ERRORS
      end;
    end if;
    
    if upper(rec.node_name) = 'RATINGCAPACITY' then
      begin
        l_row.RATINGCAPACITY := rec.node_value;
      exception when others then
        l_row.RATINGCAPACITY := -9999;
        ----htp.p('err: ' || sqlerrm || ',' || rec.xpath); --MAKE THIS A DEBUG FUNCTION FROM HERE SO WE CAN EVENTUALLY LOG ALL ERRORS
      end;
    end if;
    
    if upper(rec.node_name) = 'CONDITIONFITTING' then
      begin
        l_row.CONDITIONFITTING := rec.node_value;
      exception when others then
        l_row.CONDITIONFITTING := -9999;
        ----htp.p('err: ' || sqlerrm || ',' || rec.xpath); --MAKE THIS A DEBUG FUNCTION FROM HERE SO WE CAN EVENTUALLY LOG ALL ERRORS
      end;
    end if;
    
    if upper(rec.node_name) = 'CONDITIONBASE' then
      begin
        l_row.CONDITIONBASE := rec.node_value;
      exception when others then
        l_row.CONDITIONBASE := -9999;
        ----htp.p('err: ' || sqlerrm || ',' || rec.xpath); --MAKE THIS A DEBUG FUNCTION FROM HERE SO WE CAN EVENTUALLY LOG ALL ERRORS
      end;
    end if;
    
    if upper(rec.node_name) = 'CONDITIONCONNECTIONHARDWARE' then
      begin
        l_row.CONDITIONCONNECTIONHARDWARE := rec.node_value;
      exception when others then
        l_row.CONDITIONCONNECTIONHARDWARE := -9999;
        ----htp.p('err: ' || sqlerrm || ',' || rec.xpath); --MAKE THIS A DEBUG FUNCTION FROM HERE SO WE CAN EVENTUALLY LOG ALL ERRORS
      end;
    end if;
    
    if upper(rec.node_name) = 'PHOTOFILENAME' then
      begin
        l_row.PHOTOFILENAME := rec.node_value;
      exception when others then
        l_row.PHOTOFILENAME := -9999;
        ----htp.p('err: ' || sqlerrm || ',' || rec.xpath); --MAKE THIS A DEBUG FUNCTION FROM HERE SO WE CAN EVENTUALLY LOG ALL ERRORS
      end;
    end if;
    
  end loop;
  
  pipe row(l_row);
  
end loop;

*/
  
EXCEPTION WHEN OTHERS THEN
  null; --htp.p into debug (FUNCTIONNAME, INPUT, MESSAGE) values (''' ||  lg_vPackageName ||lc_vFunctionName || ''','''|| 'An error occured while extracting the data from the SPD_T_XML_FLATTEN' ||''',''' || sqlerrm || ''')');
  
end f_XML_AI_FITTING;

function f_XML_AI_METADATA (in_nID number, in_nPK number, in_nADOID number, in_vCreatedBy varchar2) return XML_AI_METADATAS pipelined
as
  lc_vFunctionName varchar2(255) := 'f_XML_AI_METADATA';
  l_row   XML_AI_METADATA := new XML_AI_METADATA(null, null, null, null, null, null);

/* -- REFERENCE TYPE
  METADATAID    NUMBER,
	INVENTORYID   NUMBER,
	DISPLAY       VARCHAR2(4000),
	VALUE         VARCHAR2(4000),
	VISUALORDER   VARCHAR2(4000),
	CREATED_BY    VARCHAR2(255)
*/

/*
usage:
SELECT * FROM TABLE(pkg_rowify.f_XML_AI_METADATA (in_nID =>0, in_nPK =>12345, in_nADOID =>12346, in_vCreatedBy =>'mike'));--maybe an issue
select count(*) from (SELECT * FROM TABLE(pkg_rowify.f_XML_AI_METADATA (in_nID =>0, in_nPK =>12345, in_nADOID =>12346, in_vCreatedBy =>'mike')));
*/
  
-----
vAuthor   varchar2(200) := 'mrussalesi';
vCreated  varchar2(200) := '20180528';
vUpdate   varchar2(200) := '20180528';
----- 
begin


for rec in (
select 
  parentid,
  adoid,
  max((select xmlflattenid from spd_t_xml_flatten 
          where node_position = flatten.parentid 
            and adoid = in_nADOID))                                                   currentrecordid,
  max((select xmlflattenid from spd_t_xml_flatten 
        where adoid = in_nADOID 
          and node_position = (select parentid from spd_t_xml_flatten 
            where adoid = in_nADOID 
              and node_position = (select parentid from spd_t_xml_flatten 
                where node_position = flatten.parentid 
                  and adoid = in_nADOID))))                                           parent_xmlflattenid,
  max( case upper(node_name) when 'DISPLAY' then node_value else NULL end)          DISPLAY,
  max( case upper(node_name) when 'VALUE' then node_value else NULL end)            VALUE,
  max( case upper(node_name) when 'VISUALORDER' then node_value else NULL end)      VISUALORDER
from
  spd_t_xml_flatten flatten
where
  parentid in (select flatten.node_position from spd_t_xml_flatten flatten where fpath = '/Action/Facilities/Facility/AssetSummary/AssetSummaryLineItem/AssetInventory/AssetInventoryLineItem/Metadata/LineItem' and adoid = in_nADOID)
  and adoid = in_nADOID
group by 
  parentid,
  adoid
)
loop

  l_row := new XML_AI_METADATA(null, null, null, null, null, null);
  
  l_row.INVENTORYID := rec.parent_xmlflattenid;
  l_row.METADATAID := rec.currentrecordid;
  l_row.CREATED_BY  := in_vCreatedBy;

  begin
    l_row.DISPLAY := rec.DISPLAY;
  exception when others then
    l_row.DISPLAY := -9999;
  end; 

  begin
    l_row.VALUE := rec.VALUE;
  exception when others then
    l_row.VALUE := -9999;
  end; 

  begin
    l_row.VISUALORDER := rec.VISUALORDER;
  exception when others then
    l_row.VISUALORDER := -9999;
  end; 

  pipe row(l_row);

end loop;

/*
for rec1 in (
select 
  (select max(xmlflattenid) from spd_t_xml_flatten 
      where node_position = (select max(parentid) from spd_t_xml_flatten 
        where node_position = flatten.parentid and adoid = in_nADOID)) assetinventoryid, 
  flatten.* from spd_t_xml_flatten flatten where fpath = '/Action/Facilities/Facility/AssetSummary/AssetSummaryLineItem/AssetInventory/AssetInventoryLineItem/Metadata/LineItem' and adoid = in_nADOID
 ) 
loop

  for rec in (select * from spd_t_xml_flatten where parentid = rec1.node_position and adoid = in_nADOID order by node_position) 
  loop
  
    --i think we may have to get the PK, using the level - 1, and the parentid, and maybe something else....this may be the tricky part
    --its also possible that we just specifically find the data
  
    l_row.INVENTORYID := rec1.assetinventoryid;
    l_row.METADATAID := rec.xmlflattenid;
    l_row.CREATED_BY  := in_vCreatedBy;

    if upper(rec.node_name) = 'DISPLAY' then
      begin
        l_row.DISPLAY := rec.node_value;
      exception when others then
        l_row.DISPLAY := -9999;
        ----htp.p('err: ' || sqlerrm || ',' || rec.xpath); --MAKE THIS A DEBUG FUNCTION FROM HERE SO WE CAN EVENTUALLY LOG ALL ERRORS
      end;
    end if;
    
    if upper(rec.node_name) = 'VALUE' then
      begin
        l_row.VALUE := rec.node_value;
      exception when others then
        l_row.VALUE := -9999;
        ----htp.p('err: ' || sqlerrm || ',' || rec.xpath); --MAKE THIS A DEBUG FUNCTION FROM HERE SO WE CAN EVENTUALLY LOG ALL ERRORS
      end;
    end if;
    
  end loop;
  
  pipe row(l_row);
  
end loop;
*/
  
EXCEPTION WHEN OTHERS THEN
  null; --htp.p into debug (FUNCTIONNAME, INPUT, MESSAGE) values (''' ||  lg_vPackageName ||lc_vFunctionName || ''','''|| 'An error occured while extracting the data from the SPD_T_XML_FLATTEN' ||''',''' || sqlerrm || ''')');
  
end f_XML_AI_METADATA;


function f_XML_FACILITY_DRAWING (in_nID number, in_nPK number, in_nADOID number, in_vCreatedBy varchar2) return XML_FACILITY_DRAWINGS pipelined
as
  lc_vFunctionName varchar2(255) := 'f_XML_FACILITY_DRAWING';
  l_row   XML_FACILITY_DRAWING := new XML_FACILITY_DRAWING(null, null, null, null, null, null, null, null);

/* -- REFERENCE TYPE
  FACDRAWINGID          NUMBER,
	FACILITYID            NUMBER,
	DRAWINGID             VARCHAR2(4000),
	NAVFACDRAWINGNUMBER   NUMBER,
	DESCRIPTION           VARCHAR2(4000),
	DRAWINGTYPE           VARCHAR2(4000),
	FILENAME              VARCHAR2(4000),
	CREATED_BY            VARCHAR2(255)
*/

/*
xml node names:
*/

/*
usage:
SELECT * FROM TABLE(pkg_rowify.f_XML_FACILITY_DRAWING (in_nID =>0, in_nPK =>12345, in_nADOID =>12346, in_vCreatedBy =>'mike'));
select count(*) from (SELECT * FROM TABLE(pkg_rowify.f_XML_FACILITY_DRAWING (in_nID =>0, in_nPK =>12345, in_nADOID =>12346, in_vCreatedBy =>'mike')));
*/
  
-----
vAuthor   varchar2(200) := 'mrussalesi';
vCreated  varchar2(200) := '20180528';
vUpdate   varchar2(200) := '20180528';
----- 
begin



for rec in (
select 
  parentid,
  adoid,
  max((select xmlflattenid from spd_t_xml_flatten 
          where node_position = flatten.parentid 
            and adoid = in_nADOID))                                     currentrecordid,
  max((select xmlflattenid from spd_t_xml_flatten 
        where adoid = in_nADOID 
          and node_position = (select parentid from spd_t_xml_flatten 
            where adoid = in_nADOID 
              and node_position = (select parentid from spd_t_xml_flatten 
                where node_position = flatten.parentid 
                  and adoid = in_nADOID))))                             parent_xmlflattenid,
  max( case upper(node_name) when 'DRAWINGID' then node_value else NULL end)            DRAWINGID,
  max( case upper(node_name) when 'NAVFACDRAWINGNUMBER' then node_value else NULL end)  NAVFACDRAWINGNUMBER,
  max( case upper(node_name) when 'DESCRIPTION' then node_value else NULL end)          DESCRIPTION,
  max( case upper(node_name) when 'DRAWINGTYPE' then node_value else NULL end)          DRAWINGTYPE,
  max( case upper(node_name) when 'FILENAME' then node_value else NULL end)             FILENAME
  
from
  spd_t_xml_flatten flatten
where
  parentid in (select flatten.node_position from spd_t_xml_flatten flatten where fpath = '/Action/Facilities/Facility/FacilityDrawings/FacilityDrawing' and adoid = in_nADOID)
  and adoid = in_nADOID
group by 
  parentid,
  adoid
)
loop

  l_row := new XML_FACILITY_DRAWING(null, null, null, null, null, null, null, null);
  
  l_row.FACILITYID := rec.parent_xmlflattenid;
  l_row.FACDRAWINGID := rec.currentrecordid;
  l_row.CREATED_BY  := in_vCreatedBy;

  begin
    l_row.DRAWINGID := rec.DRAWINGID;
  exception when others then
    l_row.DRAWINGID := -9999;
  end;

  begin
    l_row.NAVFACDRAWINGNUMBER := rec.NAVFACDRAWINGNUMBER;
  exception when others then
    l_row.NAVFACDRAWINGNUMBER := -9999;
  end;

  begin
    l_row.DESCRIPTION := rec.DESCRIPTION;
  exception when others then
    l_row.DESCRIPTION := -9999;
  end;

  begin
    l_row.DRAWINGTYPE := rec.DRAWINGTYPE;
  exception when others then
    l_row.DRAWINGTYPE := -9999;
  end;

  begin
    l_row.FILENAME := rec.FILENAME;
  exception when others then
    l_row.FILENAME := -9999;
  end;

  pipe row(l_row);

end loop;


/*
for rec1 in (
select 
  (select max(xmlflattenid) from spd_t_xml_flatten where node_position = 
    (select max(parentid) from spd_t_xml_flatten where node_position = flatten.parentid and adoid = in_nADOID)) xmlfacilityid, 
  flatten.* from spd_t_xml_flatten flatten where fpath = '/Action/Facilities/Facility/FacilityDrawings/FacilityDrawing' and adoid = in_nADOID) 
loop

  for rec in (select * from spd_t_xml_flatten where parentid = rec1.node_position and adoid = in_nADOID order by node_position) 
  loop
  
    --i think we may have to get the PK, using the level - 1, and the parentid, and maybe something else....this may be the tricky part
    --its also possible that we just specifically find the data
    l_row.FACDRAWINGID := rec.xmlflattenid;
    l_row.FACILITYID := rec1.xmlfacilityid;
    l_row.CREATED_BY  := in_vCreatedBy;

    if upper(rec.node_name) = upper('DRAWINGID') then
      begin
        l_row.DRAWINGID := rec.node_value;
      exception when others then
        l_row.DRAWINGID := -9999;
        ----htp.p('err: ' || sqlerrm || ',' || rec.xpath); --MAKE THIS A DEBUG FUNCTION FROM HERE SO WE CAN EVENTUALLY LOG ALL ERRORS
      end;
    end if;
    
    if upper(rec.node_name) = upper('NAVFACDRAWINGNUMBER') then
      begin
        l_row.NAVFACDRAWINGNUMBER := rec.node_value;
      exception when others then
        l_row.NAVFACDRAWINGNUMBER := -9999;
        ----htp.p('err: ' || sqlerrm || ',' || rec.xpath); --MAKE THIS A DEBUG FUNCTION FROM HERE SO WE CAN EVENTUALLY LOG ALL ERRORS
      end;
    end if;
    
    if upper(rec.node_name) = upper('DESCRIPTION') then
      begin
        l_row.DESCRIPTION := rec.node_value;
      exception when others then
        l_row.DESCRIPTION := -9999;
        ----htp.p('err: ' || sqlerrm || ',' || rec.xpath); --MAKE THIS A DEBUG FUNCTION FROM HERE SO WE CAN EVENTUALLY LOG ALL ERRORS
      end;
    end if;
    
    if upper(rec.node_name) = upper('DRAWINGTYPE') then
      begin
        l_row.DRAWINGTYPE := rec.node_value;
      exception when others then
        l_row.DRAWINGTYPE := -9999;
        ----htp.p('err: ' || sqlerrm || ',' || rec.xpath); --MAKE THIS A DEBUG FUNCTION FROM HERE SO WE CAN EVENTUALLY LOG ALL ERRORS
      end;
    end if;
    
    if upper(rec.node_name) = upper('FILENAME') then
      begin
        l_row.FILENAME := rec.node_value;
      exception when others then
        l_row.FILENAME := -9999;
        ----htp.p('err: ' || sqlerrm || ',' || rec.xpath); --MAKE THIS A DEBUG FUNCTION FROM HERE SO WE CAN EVENTUALLY LOG ALL ERRORS
      end;
    end if;
    
  end loop;

  pipe row(l_row);

end loop;
*/


EXCEPTION WHEN OTHERS THEN
  null; --htp.p into debug (FUNCTIONNAME, INPUT, MESSAGE) values (''' ||  lg_vPackageName ||lc_vFunctionName || ''','''|| 'An error occured while extracting the data from the SPD_T_XML_FLATTEN' ||''',''' || sqlerrm || ''')');
  
end f_XML_FACILITY_DRAWING;


function f_XML_FACILITY_PHOTO (in_nID number, in_nPK number, in_nADOID number, in_vCreatedBy varchar2) return XML_FACILITY_PHOTOS pipelined
as
  lc_vFunctionName varchar2(255) := 'f_XML_FACILITY_PHOTO';
  l_row   XML_FACILITY_PHOTO := new XML_FACILITY_PHOTO(null, null, null, null, null, null, null);

/* -- REFERENCE TYPE
  PHOTOID         NUMBER,
	FACILITYID      NUMBER,
	UNIFORMATIICODE VARCHAR2(4000),
	IMAGEFILENAME   VARCHAR2(4000),
	CAPTION         VARCHAR2(4000),
	PHOTOTYPE       VARCHAR2(4000),
	CREATED_BY 
*/

/*
xml node names:
PhotoID
UniFormat-IICode
AssetTypes
ImageFileName
Caption
PhotoType
*/

/*
usage:
SELECT * FROM TABLE(pkg_rowify.f_XML_FACILITY_PHOTO (in_nID =>0, in_nPK =>12345, in_nADOID =>12346, in_vCreatedBy =>'mike'));
select count(*) from (SELECT * FROM TABLE(pkg_rowify.f_XML_FACILITY_PHOTO (in_nID =>0, in_nPK =>12345, in_nADOID =>12346, in_vCreatedBy =>'mike'));
*/
  
-----
vAuthor   varchar2(200) := 'mrussalesi';
vCreated  varchar2(200) := '20180528';
vUpdate   varchar2(200) := '20180528';
----- 
begin


for rec in (
select 
  parentid,
  adoid,
  max((select xmlflattenid from spd_t_xml_flatten 
          where node_position = flatten.parentid 
            and adoid = in_nADOID))                                                   currentrecordid,
  max((select xmlflattenid from spd_t_xml_flatten 
        where adoid = in_nADOID 
          and node_position = (select parentid from spd_t_xml_flatten 
            where adoid = in_nADOID 
              and node_position = (select parentid from spd_t_xml_flatten 
                where node_position = flatten.parentid 
                  and adoid = in_nADOID))))                                           parent_xmlflattenid,
  max( case upper(node_name) when 'UNIFORMAT-IICODE' then node_value else NULL end) UNIFORMATIICODE,
  max( case upper(node_name) when 'IMAGEFILENAME' then node_value else NULL end)    IMAGEFILENAME,
  max( case upper(node_name) when 'CAPTION' then node_value else NULL end)          CAPTION,
  max( case upper(node_name) when 'PHOTOTYPE' then node_value else NULL end)        PHOTOTYPE
from
  spd_t_xml_flatten flatten
where
  parentid in (select flatten.node_position from spd_t_xml_flatten flatten where fpath = '/Action/Facilities/Facility/Photos/Photo' and adoid = in_nADOID)
  and adoid = in_nADOID
group by 
  parentid,
  adoid
)
loop

  l_row := new XML_FACILITY_PHOTO(null, null, null, null, null, null, null);
  
  l_row.FACILITYID := rec.parent_xmlflattenid;
  l_row.PHOTOID := rec.currentrecordid;
  l_row.CREATED_BY  := in_vCreatedBy;

  begin
    l_row.UNIFORMATIICODE := rec.UNIFORMATIICODE;
  exception when others then
    l_row.UNIFORMATIICODE := -9999;
  end; 

  begin
    l_row.IMAGEFILENAME := rec.IMAGEFILENAME;
  exception when others then
    l_row.IMAGEFILENAME := -9999;
  end; 

  begin
    l_row.CAPTION := rec.CAPTION;
  exception when others then
    l_row.CAPTION := -9999;
  end; 

  begin
    l_row.PHOTOTYPE := rec.PHOTOTYPE;
  exception when others then
    l_row.PHOTOTYPE := -9999;
  end; 


  pipe row(l_row);

end loop;
/*
for rec1 in (
select 
  (select xmlflattenid from spd_t_xml_flatten where adoid = in_nADOID and node_position = 
    (select parentid from spd_t_xml_flatten where node_position = flatten.parentid and adoid = in_nADOID)) xmlfacilityid, 
  flatten.* from spd_t_xml_flatten flatten where fpath = '/Action/Facilities/Facility/Photos/Photo' and adoid = in_nADOID) 
loop

  for rec in (select * from spd_t_xml_flatten where parentid = rec1.node_position and adoid = in_nADOID order by node_position) 
  loop
  
    --i think we may have to get the PK, using the level - 1, and the parentid, and maybe something else....this may be the tricky part
    --its also possible that we just specifically find the data
    l_row.PHOTOID := rec1.xmlflattenid;
    l_row.FACILITYID := rec1.xmlfacilityid;
    l_row.CREATED_BY  := in_vCreatedBy;

    if upper(rec.node_name) = upper('UniFormat-IICode') then
      begin
        l_row.UNIFORMATIICODE := rec.node_value;
      exception when others then
        l_row.UNIFORMATIICODE := -9999;
        ----htp.p('err: ' || sqlerrm || ',' || rec.xpath); --MAKE THIS A DEBUG FUNCTION FROM HERE SO WE CAN EVENTUALLY LOG ALL ERRORS
      end;
    end if;
    
    if upper(rec.node_name) = upper('IMAGEFILENAME') then
      begin
        l_row.IMAGEFILENAME := rec.node_value;
      exception when others then
        l_row.IMAGEFILENAME := -9999;
        ----htp.p('err: ' || sqlerrm || ',' || rec.xpath); --MAKE THIS A DEBUG FUNCTION FROM HERE SO WE CAN EVENTUALLY LOG ALL ERRORS
      end;
    end if;
    
    if upper(rec.node_name) = upper('CAPTION') then
      begin
        l_row.CAPTION := rec.node_value;
      exception when others then
        l_row.CAPTION := -9999;
        ----htp.p('err: ' || sqlerrm || ',' || rec.xpath); --MAKE THIS A DEBUG FUNCTION FROM HERE SO WE CAN EVENTUALLY LOG ALL ERRORS
      end;
    end if;
    
    if upper(rec.node_name) = upper('PHOTOTYPE') then
      begin
        l_row.PHOTOTYPE := rec.node_value;
      exception when others then
        l_row.PHOTOTYPE := -9999;
        ----htp.p('err: ' || sqlerrm || ',' || rec.xpath); --MAKE THIS A DEBUG FUNCTION FROM HERE SO WE CAN EVENTUALLY LOG ALL ERRORS
      end;
    end if;
    
  end loop;
  
  pipe row(l_row);
  
  --reset row
  l_row.PHOTOID := null;
	l_row.FACILITYID := null;
	l_row.UNIFORMATIICODE := null;
	l_row.IMAGEFILENAME := null;
	l_row.CAPTION := null;
	l_row.PHOTOTYPE := null;
	l_row.CREATED_BY := null;

end loop;

return;
  
*/
EXCEPTION WHEN OTHERS THEN
  null; --htp.p into debug (FUNCTIONNAME, INPUT, MESSAGE) values (''' ||  lg_vPackageName ||lc_vFunctionName || ''','''|| 'An error occured while extracting the data from the SPD_T_XML_FLATTEN' ||''',''' || sqlerrm || ''')');
  
end f_XML_FACILITY_PHOTO;



function f_XML_FACILITY_REPAIR (in_nID number, in_nPK number, in_nADOID number, in_vCreatedBy varchar2) return XML_FACILITY_REPAIRS pipelined
as
  lc_vFunctionName varchar2(255) := 'f_XML_FACILITY_REPAIR';
  l_row   XML_FACILITY_REPAIR := new XML_FACILITY_REPAIR(null, null, null, null, null, null, null, null, null, null);

/* -- REFERENCE TYPE
  REPAIRID            NUMBER,
	FACILITYID          NUMBER,
	UNIFORMATIICODE     VARCHAR2(4000),
	REPAIRDESCRIPTION   VARCHAR2(4000),
	QUANTITY            NUMBER,
	UNITS               VARCHAR2(4000),
	MATERIALUNITCOST    NUMBER,
	LABORUNITCOST       NUMBER,
	ASSETTYPE           VARCHAR2(4000),
	CREATED_BY          VARCHAR2(255)
*/

/*
xml node names:
*/

/*
usage:
SELECT * FROM TABLE(pkg_rowify.f_XML_FACILITY_REPAIR (in_nID =>0, in_nPK =>12345, in_nADOID =>12346, in_vCreatedBy =>'mike'));
select count(*) from (SELECT * FROM TABLE(pkg_rowify.f_XML_FACILITY_REPAIR (in_nID =>0, in_nPK =>12345, in_nADOID =>12346, in_vCreatedBy =>'mike')));
*/
  
-----
vAuthor   varchar2(200) := 'mrussalesi';
vCreated  varchar2(200) := '20180528';
vUpdate   varchar2(200) := '20180528';
----- 
begin


for rec in (
select 
  parentid,
  adoid,
  max((select xmlflattenid from spd_t_xml_flatten 
          where node_position = flatten.parentid 
            and adoid = in_nADOID))                                     currentrecordid,
  max((select xmlflattenid from spd_t_xml_flatten 
        where adoid = in_nADOID 
          and node_position = (select parentid from spd_t_xml_flatten 
            where adoid = in_nADOID 
              and node_position = (select parentid from spd_t_xml_flatten 
                where node_position = flatten.parentid 
                  and adoid = in_nADOID))))                             parent_xmlflattenid,
  max( case upper(node_name) when 'UNIFORMATIICODE' then node_value else NULL end)     UNIFORMATIICODE,
  max( case upper(node_name) when 'REPAIRDESCRIPTION' then node_value else NULL end)   REPAIRDESCRIPTION,
  max( case upper(node_name) when 'QUANTITY' then node_value else NULL end)            QUANTITY,
  max( case upper(node_name) when 'UNITS' then node_value else NULL end)               UNITS,
  max( case upper(node_name) when 'MATERIALUNITCOST' then node_value else NULL end)    MATERIALUNITCOST,
  max( case upper(node_name) when 'LABORUNITCOST' then node_value else NULL end)       LABORUNITCOST,
  max( case upper(node_name) when 'ASSETTYPE' then node_value else NULL end)           ASSETTYPE
from
  spd_t_xml_flatten flatten
where
  parentid in (select flatten.node_position from spd_t_xml_flatten flatten where fpath = '/Action/Facilities/Facility/FacilityRepairCosts/Repair' and adoid = in_nADOID)
  and adoid = in_nADOID
group by 
  parentid,
  adoid
)
loop

  l_row := new XML_FACILITY_REPAIR(null, null, null, null, null, null, null, null, null, null);
  
  l_row.FACILITYID := rec.parent_xmlflattenid;
  l_row.REPAIRID := rec.currentrecordid;
  l_row.CREATED_BY  := in_vCreatedBy;
  
  begin
    l_row.UNIFORMATIICODE := rec.UNIFORMATIICODE;
  exception when others then
    l_row.UNIFORMATIICODE := -9999;
  end;

  begin
    l_row.REPAIRDESCRIPTION := rec.REPAIRDESCRIPTION;
  exception when others then
    l_row.REPAIRDESCRIPTION := -9999;
  end;

  begin
    l_row.QUANTITY := rec.QUANTITY;
  exception when others then
    l_row.QUANTITY := -9999;
  end;

  begin
    l_row.UNITS := rec.UNITS;
  exception when others then
    l_row.UNITS := -9999;
  end;

  begin
    l_row.MATERIALUNITCOST := rec.MATERIALUNITCOST;
  exception when others then
    l_row.MATERIALUNITCOST := -9999;
  end;

  begin
    l_row.LABORUNITCOST := rec.LABORUNITCOST;
  exception when others then
    l_row.LABORUNITCOST := -9999;
  end;

  begin
    l_row.ASSETTYPE := rec.ASSETTYPE;
  exception when others then
    l_row.ASSETTYPE := -9999;
  end;


  pipe row(l_row);

end loop;

/*
for rec1 in (
select 
  (select max(xmlflattenid) from spd_t_xml_flatten where node_position = 
    (select max(parentid) from spd_t_xml_flatten where node_position = flatten.parentid and adoid = in_nADOID)) xmlfacilityid, 
  flatten.* from spd_t_xml_flatten flatten where fpath = '/Action/Facilities/Facility/FacilityRepairCosts/Repair' and adoid = in_nADOID) 
loop

  for rec in (select * from spd_t_xml_flatten where parentid = rec1.node_position and adoid = in_nADOID order by node_position) 
  loop
  
    --i think we may have to get the PK, using the level - 1, and the parentid, and maybe something else....this may be the tricky part
    --its also possible that we just specifically find the data
    l_row.REPAIRID := rec.xmlflattenid;
    l_row.FACILITYID := rec1.xmlfacilityid;
    l_row.CREATED_BY  := in_vCreatedBy;

    if upper(rec.node_name) = upper('UNIFORMATIICODE') then
      begin
        l_row.UNIFORMATIICODE := rec.node_value;
      exception when others then
        l_row.UNIFORMATIICODE := -9999;
        ----htp.p('err: ' || sqlerrm || ',' || rec.xpath); --MAKE THIS A DEBUG FUNCTION FROM HERE SO WE CAN EVENTUALLY LOG ALL ERRORS
      end;
    end if;
    
    if upper(rec.node_name) = upper('REPAIRDESCRIPTION') then
      begin
        l_row.REPAIRDESCRIPTION := rec.node_value;
      exception when others then
        l_row.REPAIRDESCRIPTION := -9999;
        ----htp.p('err: ' || sqlerrm || ',' || rec.xpath); --MAKE THIS A DEBUG FUNCTION FROM HERE SO WE CAN EVENTUALLY LOG ALL ERRORS
      end;
    end if;
    
    if upper(rec.node_name) = upper('QUANTITY') then
      begin
        l_row.QUANTITY := rec.node_value;
      exception when others then
        l_row.QUANTITY := -9999;
        ----htp.p('err: ' || sqlerrm || ',' || rec.xpath); --MAKE THIS A DEBUG FUNCTION FROM HERE SO WE CAN EVENTUALLY LOG ALL ERRORS
      end;
    end if;
    
    if upper(rec.node_name) = upper('UNITS') then
      begin
        l_row.UNITS := rec.node_value;
      exception when others then
        l_row.UNITS := -9999;
        ----htp.p('err: ' || sqlerrm || ',' || rec.xpath); --MAKE THIS A DEBUG FUNCTION FROM HERE SO WE CAN EVENTUALLY LOG ALL ERRORS
      end;
    end if;
  
    if upper(rec.node_name) = upper('LABORUNITCOST') then
      begin
        l_row.LABORUNITCOST := rec.node_value;
      exception when others then
        l_row.LABORUNITCOST := -9999;
        ----htp.p('err: ' || sqlerrm || ',' || rec.xpath); --MAKE THIS A DEBUG FUNCTION FROM HERE SO WE CAN EVENTUALLY LOG ALL ERRORS
      end;
    end if;
    
    if upper(rec.node_name) = upper('ASSETTYPE') then
      begin
        l_row.ASSETTYPE := rec.node_value;
      exception when others then
        l_row.ASSETTYPE := -9999;
        ----htp.p('err: ' || sqlerrm || ',' || rec.xpath); --MAKE THIS A DEBUG FUNCTION FROM HERE SO WE CAN EVENTUALLY LOG ALL ERRORS
      end;
    end if;
    
  end loop;
  
  pipe row(l_row);

end loop;
*/


EXCEPTION WHEN OTHERS THEN
  null; --htp.p into debug (FUNCTIONNAME, INPUT, MESSAGE) values (''' ||  lg_vPackageName ||lc_vFunctionName || ''','''|| 'An error occured while extracting the data from the SPD_T_XML_FLATTEN' ||''',''' || sqlerrm || ''')');
  
end f_XML_FACILITY_REPAIR;

function f_XML_FAC_SUP_FILE (in_nID number, in_nPK number, in_nADOID number, in_vCreatedBy varchar2) return XML_FAC_SUP_FILES pipelined
as
  lc_vFunctionName varchar2(255) := 'f_XML_FAC_SUP_FILE';
  l_row   XML_FAC_SUP_FILE := new XML_FAC_SUP_FILE(null, null, null, null, null, null);

/* -- REFERENCE TYPE
  FILEID            NUMBER,
	FACILITYID        NUMBER,
	SUPPORTINGFILEID  VARCHAR2(4000),
	FILENAME          VARCHAR2(4000),
	DESCRIPTION       VARCHAR2(4000),
	CREATED_BY        VARCHAR2(255)
*/

/*
xml node names:
*/
  
-----
vAuthor   varchar2(200) := 'mrussalesi';
vCreated  varchar2(200) := '20180528';
vUpdate   varchar2(200) := '20180528';
----- 
begin

--!!!NOTE THERE IS NO DATA IN THE SAMPLE FILES FOR THIS...USURE OF THE XML NODE NAMES, using upper() to force, may take extra time to do this.

for rec1 in (
select 
  (select max(xmlflattenid) from spd_t_xml_flatten where node_position = 
    (select max(parentid) from spd_t_xml_flatten where node_position = flatten.parentid and adoid = in_nADOID)) xmlfacilityid, 
  flatten.* from spd_t_xml_flatten flatten where fpath = '/Action/Facilities/Facility/FacilitySupportingFiles/SupportingFile' and adoid = in_nADOID) 
loop

  for rec in (select * from spd_t_xml_flatten where parentid = rec1.node_position and adoid = in_nADOID order by node_position) 
  loop
  
    --i think we may have to get the PK, using the level - 1, and the parentid, and maybe something else....this may be the tricky part
    --its also possible that we just specifically find the data
    l_row.FILEID := rec.xmlflattenid;
    l_row.FACILITYID := rec1.xmlfacilityid;
    l_row.CREATED_BY  := in_vCreatedBy;

    if upper(rec.node_name) = upper('SUPPORTINGFILEID') then
      begin
        l_row.SUPPORTINGFILEID := rec.node_value;
      exception when others then
        l_row.SUPPORTINGFILEID := -9999;
        ----htp.p('err: ' || sqlerrm || ',' || rec.xpath); --MAKE THIS A DEBUG FUNCTION FROM HERE SO WE CAN EVENTUALLY LOG ALL ERRORS
      end;
    end if;
    
    if upper(rec.node_name) = upper('FILENAME') then
      begin
        l_row.FILENAME := rec.node_value;
      exception when others then
        l_row.FILENAME := -9999;
        ----htp.p('err: ' || sqlerrm || ',' || rec.xpath); --MAKE THIS A DEBUG FUNCTION FROM HERE SO WE CAN EVENTUALLY LOG ALL ERRORS
      end;
    end if;
    
    if upper(rec.node_name) = upper('DESCRIPTION') then
      begin
        l_row.DESCRIPTION := rec.node_value;
      exception when others then
        l_row.DESCRIPTION := -9999;
        ----htp.p('err: ' || sqlerrm || ',' || rec.xpath); --MAKE THIS A DEBUG FUNCTION FROM HERE SO WE CAN EVENTUALLY LOG ALL ERRORS
      end;
    end if;
    
  end loop;
  
  pipe row(l_row);

end loop;

return;
  
EXCEPTION WHEN OTHERS THEN
  null; --htp.p into debug (FUNCTIONNAME, INPUT, MESSAGE) values (''' ||  lg_vPackageName ||lc_vFunctionName || ''','''|| 'An error occured while extracting the data from the SPD_T_XML_FLATTEN' ||''',''' || sqlerrm || ''')');
  
end f_XML_FAC_SUP_FILE;


function f_XML_FINDREC (in_nID number, in_nPK number, in_nADOID number, in_vCreatedBy varchar2) return XML_FINDRECS pipelined
as
  lc_vFunctionName varchar2(255) := 'f_XML_FINDREC';
  l_row   XML_FINDREC := new XML_FINDREC(null, null, null, null, null, null, null);

/* -- REFERENCE TYPE
  RECID             NUMBER,
	FACILITYID        NUMBER,
	UNIFORMATIICODE   VARCHAR2(4000),
	DESCRIPTION       VARCHAR2(4000),
	FINDINGS          VARCHAR2(4000),
	RECOMMENDATIONS   VARCHAR2(4000),
	CREATED_BY        VARCHAR2(255)
*/

/*
xml node names:
*/

/*
usage:
SELECT * FROM TABLE(pkg_rowify.f_XML_FINDREC (in_nID =>0, in_nPK =>12345, in_nADOID =>12346, in_vCreatedBy =>'mike'));
select count(*) from (SELECT * FROM TABLE(pkg_rowify.f_XML_FINDREC (in_nID =>0, in_nPK =>12345, in_nADOID =>12346, in_vCreatedBy =>'mike')));
*/
  
-----
vAuthor   varchar2(200) := 'mrussalesi';
vCreated  varchar2(200) := '20180528';
vUpdate   varchar2(200) := '20180528';
----- 
begin


for rec in (
select 
  parentid,
  adoid,
  max((select xmlflattenid from spd_t_xml_flatten 
          where node_position = flatten.parentid 
            and adoid = in_nADOID))                                     currentrecordid,
  max((select xmlflattenid from spd_t_xml_flatten 
        where adoid = in_nADOID 
          and node_position = (select parentid from spd_t_xml_flatten 
            where adoid = in_nADOID 
              and node_position = (select parentid from spd_t_xml_flatten 
                where node_position = flatten.parentid 
                  and adoid = in_nADOID))))                             parent_xmlflattenid,
  max( case upper(node_name) when 'UNIFORMATIICODE' then node_value else NULL end)  UNIFORMATIICODE,
  max( case upper(node_name) when 'DESCRIPTION' then node_value else NULL end)      DESCRIPTION,
  max( case upper(node_name) when 'FINDINGS' then node_value else NULL end)         FINDINGS,
  max( case upper(node_name) when 'RECOMMENDATIONS' then node_value else NULL end)  RECOMMENDATIONS
from
  spd_t_xml_flatten flatten
where
  parentid in (select flatten.node_position from spd_t_xml_flatten flatten where fpath = '/Action/Facilities/Facility/FindingsRecommendations/FindingRecommendation' and adoid = in_nADOID)
  and adoid = in_nADOID
group by 
  parentid,
  adoid
)
loop

  l_row := new XML_FINDREC(null, null, null, null, null, null, null);
  
  l_row.FACILITYID := rec.parent_xmlflattenid;
  l_row.RECID := rec.currentrecordid;
  l_row.CREATED_BY  := in_vCreatedBy;
  
  begin
    l_row.UNIFORMATIICODE := rec.UNIFORMATIICODE;
  exception when others then
    l_row.UNIFORMATIICODE := -9999;
  end;

  begin
    l_row.DESCRIPTION := rec.DESCRIPTION;
  exception when others then
    l_row.DESCRIPTION := -9999;
  end;

  begin
    l_row.FINDINGS := rec.FINDINGS;
  exception when others then
    l_row.FINDINGS := -9999;
  end;

  begin
    l_row.RECOMMENDATIONS := rec.RECOMMENDATIONS;
  exception when others then
    l_row.RECOMMENDATIONS := -9999;
  end;

  pipe row(l_row);

end loop;

/*
for rec1 in (
select 
  (select max(xmlflattenid) from spd_t_xml_flatten where node_position = 
    (select max(parentid) from spd_t_xml_flatten where node_position = flatten.parentid and adoid = in_nADOID)) xmlfacilityid, 
  flatten.* from spd_t_xml_flatten flatten where fpath = '/Action/Facilities/Facility/FindingsRecommendations/FindingRecommendation' and adoid = in_nADOID) 
loop

  for rec in (select * from spd_t_xml_flatten where parentid = rec1.node_position and adoid = in_nADOID order by node_position) 
  loop
  
    --i think we may have to get the PK, using the level - 1, and the parentid, and maybe something else....this may be the tricky part
    --its also possible that we just specifically find the data
    l_row.RECID := rec.xmlflattenid;
    l_row.FACILITYID := rec1.xmlfacilityid;
    l_row.CREATED_BY  := in_vCreatedBy;

    if upper(rec.node_name) = upper('UNIFORMATIICODE') then
      begin
        l_row.UNIFORMATIICODE := rec.node_value;
      exception when others then
        l_row.UNIFORMATIICODE := -9999;
        ----htp.p('err: ' || sqlerrm || ',' || rec.xpath); --MAKE THIS A DEBUG FUNCTION FROM HERE SO WE CAN EVENTUALLY LOG ALL ERRORS
      end;
    end if;
    
    if upper(rec.node_name) = upper('DESCRIPTION') then
      begin
        l_row.DESCRIPTION := rec.node_value;
      exception when others then
        l_row.DESCRIPTION := -9999;
        ----htp.p('err: ' || sqlerrm || ',' || rec.xpath); --MAKE THIS A DEBUG FUNCTION FROM HERE SO WE CAN EVENTUALLY LOG ALL ERRORS
      end;
    end if;
    
    if upper(rec.node_name) = upper('FINDINGS') then
      begin
        l_row.FINDINGS := rec.node_value;
      exception when others then
        l_row.FINDINGS := -9999;
        ----htp.p('err: ' || sqlerrm || ',' || rec.xpath); --MAKE THIS A DEBUG FUNCTION FROM HERE SO WE CAN EVENTUALLY LOG ALL ERRORS
      end;
    end if;
    
    if upper(rec.node_name) = upper('RECOMMENDATIONS') then
      begin
        l_row.RECOMMENDATIONS := rec.node_value;
      exception when others then
        l_row.RECOMMENDATIONS := -9999;
        ----htp.p('err: ' || sqlerrm || ',' || rec.xpath); --MAKE THIS A DEBUG FUNCTION FROM HERE SO WE CAN EVENTUALLY LOG ALL ERRORS
      end;
    end if;
    
  end loop;
  
  pipe row(l_row);

end loop;
*/
  
EXCEPTION WHEN OTHERS THEN
  null; --htp.p into debug (FUNCTIONNAME, INPUT, MESSAGE) values (''' ||  lg_vPackageName ||lc_vFunctionName || ''','''|| 'An error occured while extracting the data from the SPD_T_XML_FLATTEN' ||''',''' || sqlerrm || ''')');
  
end f_XML_FINDREC;


function f_XML_REPAIR_PERCENTAGE (in_nID number, in_nPK number, in_nADOID number, in_vCreatedBy varchar2) return XML_REPAIR_PERCENTAGES pipelined
as
  lc_vFunctionName varchar2(255) := 'f_XML_REPAIR_PERCENTAGE';
  l_row   XML_REPAIR_PERCENTAGE := new XML_REPAIR_PERCENTAGE(null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);

/* -- REFERENCE TYPE
  PERCENTID       NUMBER,
	FACILITYID      NUMBER,
	Z1010           VARCHAR2(4000),
	Z1020           NUMBER,
	Z1030           VARCHAR2(4000),
	Z1040           VARCHAR2(4000),
	Z2010           VARCHAR2(4000),
	Z2020           VARCHAR2(4000),
	Z2030           VARCHAR2(4000),
	Z2040           VARCHAR2(4000),
	Z2050           VARCHAR2(4000),
	Z3010           VARCHAR2(4000),
	IMPACT          VARCHAR2(4000),
	ENGSTUDY        VARCHAR2(4000),
	CREATED_BY      VARCHAR2(255)
*/

/*
xml node names:
*/
  
-----
vAuthor   varchar2(200) := 'mrussalesi';
vCreated  varchar2(200) := '20180528';
vUpdate   varchar2(200) := '20180528';
----- 
begin


for rec in (
select 
  parentid,
  adoid,
  max((select xmlflattenid from spd_t_xml_flatten 
          where node_position = flatten.parentid 
            and adoid = in_nADOID))                                     currentrecordid,
  max((select xmlflattenid from spd_t_xml_flatten 
        where adoid = in_nADOID 
          and node_position = (select parentid from spd_t_xml_flatten 
            where adoid = in_nADOID 
              and node_position = (select parentid from spd_t_xml_flatten 
                where node_position = flatten.parentid 
                  and adoid = in_nADOID))))                             parent_xmlflattenid,
  max( case upper(node_name) when 'Z1010' then node_value else NULL end)          Z1010,
  max( case upper(node_name) when 'Z1020' then node_value else NULL end)          Z1020,
  max( case upper(node_name) when 'Z1030' then node_value else NULL end)          Z1030,
  max( case upper(node_name) when 'Z1040' then node_value else NULL end)          Z1040,
  max( case upper(node_name) when 'Z2010' then node_value else NULL end)          Z2010,
  max( case upper(node_name) when 'Z2020' then node_value else NULL end)          Z2020,
  max( case upper(node_name) when 'Z2030' then node_value else NULL end)          Z2030,
  max( case upper(node_name) when 'Z2040' then node_value else NULL end)          Z2040,
  max( case upper(node_name) when 'Z2050' then node_value else NULL end)          Z2050,
  max( case upper(node_name) when 'Z3010' then node_value else NULL end)          Z3010,
  max( case upper(node_name) when 'IMPACT' then node_value else NULL end)         IMPACT,
  max( case upper(node_name) when 'ENGSTUDY' then node_value else NULL end)       ENGSTUDY
from
  spd_t_xml_flatten flatten
where
  parentid in (select flatten.node_position from spd_t_xml_flatten flatten where fpath = '/Action/Facilities/Facility/FacilityRepairCosts/RepairCostPercentages' and adoid = in_nADOID)
  and adoid = in_nADOID
group by 
  parentid,
  adoid
)
loop

  l_row := new XML_REPAIR_PERCENTAGE(null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
  
  l_row.FACILITYID := rec.parent_xmlflattenid;
  l_row.PERCENTID := rec.currentrecordid;
  l_row.CREATED_BY  := in_vCreatedBy;
  
  begin
      l_row.Z1010 := rec.Z1010;
    exception when others then
      l_row.Z1010 := -9999;
    end;
  
  begin
      l_row.Z1020 := rec.Z1020;
    exception when others then
      l_row.Z1020 := -9999;
    end;
  
  begin
      l_row.Z1030 := rec.Z1030;
    exception when others then
      l_row.Z1030 := -9999;
    end;
  
  begin
      l_row.Z1040 := rec.Z1040;
    exception when others then
      l_row.Z1040 := -9999;
    end;
  
  begin
      l_row.Z2010 := rec.Z2010;
    exception when others then
      l_row.Z2010 := -9999;
    end;
  
  begin
      l_row.Z2020 := rec.Z2020;
    exception when others then
      l_row.Z2020 := -9999;
    end;
  
  begin
      l_row.Z2030 := rec.Z2030;
    exception when others then
      l_row.Z2030 := -9999;
    end;
  
  begin
      l_row.Z2040 := rec.Z2040;
    exception when others then
      l_row.Z2040 := -9999;
    end;
  
  begin
      l_row.Z2050 := rec.Z2050;
    exception when others then
      l_row.Z2050 := -9999;
    end;
  
  begin
      l_row.Z3010 := rec.Z3010;
    exception when others then
      l_row.Z3010 := -9999;
    end;
  
  begin
      l_row.IMPACT := rec.IMPACT;
    exception when others then
      l_row.IMPACT := -9999;
    end;
  
  begin
      l_row.ENGSTUDY := rec.ENGSTUDY;
    exception when others then
      l_row.ENGSTUDY := -9999;
    end;

  pipe row(l_row);

end loop;

/*
for rec1 in (
select 
  (select max(xmlflattenid) from spd_t_xml_flatten where node_position = 
    (select max(parentid) from spd_t_xml_flatten where node_position = flatten.parentid and adoid = in_nADOID)) xmlfacilityid, 
  flatten.* from spd_t_xml_flatten flatten where fpath = '/Action/Facilities/Facility/FacilityRepairCosts/RepairCostPercentages' and adoid = in_nADOID) 
loop

  for rec in (select * from spd_t_xml_flatten where parentid = rec1.node_position and adoid = in_nADOID order by node_position) 
  loop
  
    --i think we may have to get the PK, using the level - 1, and the parentid, and maybe something else....this may be the tricky part
    --its also possible that we just specifically find the data
    l_row.PERCENTID := rec.xmlflattenid;
    l_row.FACILITYID := rec1.xmlfacilityid;
    l_row.CREATED_BY  := in_vCreatedBy;

    if upper(rec.node_name) = upper('Z1010') then
      begin
        l_row.Z1010 := rec.node_value;
      exception when others then
        l_row.Z1010 := -9999;
        ----htp.p('err: ' || sqlerrm || ',' || rec.xpath); --MAKE THIS A DEBUG FUNCTION FROM HERE SO WE CAN EVENTUALLY LOG ALL ERRORS
      end;
    end if;
    
    if upper(rec.node_name) = upper('Z1020') then
      begin
        l_row.Z1020 := rec.node_value;
      exception when others then
        l_row.Z1020 := -9999;
        ----htp.p('err: ' || sqlerrm || ',' || rec.xpath); --MAKE THIS A DEBUG FUNCTION FROM HERE SO WE CAN EVENTUALLY LOG ALL ERRORS
      end;
    end if;
    
    if upper(rec.node_name) = upper('Z1030') then
      begin
        l_row.Z1030 := rec.node_value;
      exception when others then
        l_row.Z1030 := -9999;
        ----htp.p('err: ' || sqlerrm || ',' || rec.xpath); --MAKE THIS A DEBUG FUNCTION FROM HERE SO WE CAN EVENTUALLY LOG ALL ERRORS
      end;
    end if;
    
    if upper(rec.node_name) = upper('Z1040') then
      begin
        l_row.Z1040 := rec.node_value;
      exception when others then
        l_row.Z1040 := -9999;
        ----htp.p('err: ' || sqlerrm || ',' || rec.xpath); --MAKE THIS A DEBUG FUNCTION FROM HERE SO WE CAN EVENTUALLY LOG ALL ERRORS
      end;
    end if;
    
    if upper(rec.node_name) = upper('Z2010') then
      begin
        l_row.Z2010 := rec.node_value;
      exception when others then
        l_row.Z2010 := -9999;
        ----htp.p('err: ' || sqlerrm || ',' || rec.xpath); --MAKE THIS A DEBUG FUNCTION FROM HERE SO WE CAN EVENTUALLY LOG ALL ERRORS
      end;
    end if;
    
    if upper(rec.node_name) = upper('Z2020') then
      begin
        l_row.Z2020 := rec.node_value;
      exception when others then
        l_row.Z2020 := -9999;
        ----htp.p('err: ' || sqlerrm || ',' || rec.xpath); --MAKE THIS A DEBUG FUNCTION FROM HERE SO WE CAN EVENTUALLY LOG ALL ERRORS
      end;
    end if;
    
    if upper(rec.node_name) = upper('Z2030') then
      begin
        l_row.Z2030 := rec.node_value;
      exception when others then
        l_row.Z2030 := -9999;
        ----htp.p('err: ' || sqlerrm || ',' || rec.xpath); --MAKE THIS A DEBUG FUNCTION FROM HERE SO WE CAN EVENTUALLY LOG ALL ERRORS
      end;
    end if;
    
    if upper(rec.node_name) = upper('Z2040') then
      begin
        l_row.Z2040 := rec.node_value;
      exception when others then
        l_row.Z2040 := -9999;
        ----htp.p('err: ' || sqlerrm || ',' || rec.xpath); --MAKE THIS A DEBUG FUNCTION FROM HERE SO WE CAN EVENTUALLY LOG ALL ERRORS
      end;
    end if;
    
    if upper(rec.node_name) = upper('Z2050') then
      begin
        l_row.Z2050 := rec.node_value;
      exception when others then
        l_row.Z2050 := -9999;
        ----htp.p('err: ' || sqlerrm || ',' || rec.xpath); --MAKE THIS A DEBUG FUNCTION FROM HERE SO WE CAN EVENTUALLY LOG ALL ERRORS
      end;
    end if;
    
    if upper(rec.node_name) = upper('Z3010') then
      begin
        l_row.Z3010 := rec.node_value;
      exception when others then
        l_row.Z3010 := -9999;
        ----htp.p('err: ' || sqlerrm || ',' || rec.xpath); --MAKE THIS A DEBUG FUNCTION FROM HERE SO WE CAN EVENTUALLY LOG ALL ERRORS
      end;
    end if;
    
    if upper(rec.node_name) = upper('IMPACT') then
      begin
        l_row.IMPACT := rec.node_value;
      exception when others then
        l_row.IMPACT := -9999;
        ----htp.p('err: ' || sqlerrm || ',' || rec.xpath); --MAKE THIS A DEBUG FUNCTION FROM HERE SO WE CAN EVENTUALLY LOG ALL ERRORS
      end;
    end if;
    
    if upper(rec.node_name) = upper('ENGSTUDY') then
      begin
        l_row.ENGSTUDY := rec.node_value;
      exception when others then
        l_row.ENGSTUDY := -9999;
        ----htp.p('err: ' || sqlerrm || ',' || rec.xpath); --MAKE THIS A DEBUG FUNCTION FROM HERE SO WE CAN EVENTUALLY LOG ALL ERRORS
      end;
    end if;
    
  end loop;
  
  pipe row(l_row);

end loop;
*/

EXCEPTION WHEN OTHERS THEN
  null; --htp.p into debug (FUNCTIONNAME, INPUT, MESSAGE) values (''' ||  lg_vPackageName ||lc_vFunctionName || ''','''|| 'An error occured while extracting the data from the SPD_T_XML_FLATTEN' ||''',''' || sqlerrm || ''')');
  
end f_XML_REPAIR_PERCENTAGE;


function f_XML_ET_METADATA (in_nID number, in_nPK number, in_nADOID number, in_vCreatedBy varchar2) return XML_ET_METADATAS pipelined
as
  lc_vFunctionName varchar2(255) := 'f_XML_ET_METADATA';
  l_row   XML_ET_METADATA := new XML_ET_METADATA(null, null, null, null, null, null);

/* -- REFERENCE TYPE
  METADATAID      NUMBER,
	EXECTABLEID      NUMBER,
	DISPLAY      VARCHAR2(4000),
	VALUE      VARCHAR2(4000),
	VISUALORDER      VARCHAR2(4000),
	CREATED_BY      VARCHAR2(255)
*/

/*
xml node names:

*/

/*
usage:
SELECT * FROM TABLE(pkg_rowify.f_XML_ET_METADATA (in_nID =>0, in_nPK =>12345, in_nADOID =>12346, in_vCreatedBy =>'mike'));
select count(*) from (SELECT * FROM TABLE(pkg_rowify.f_XML_ET_METADATA (in_nID =>0, in_nPK =>12345, in_nADOID =>12346, in_vCreatedBy =>'mike')));
*/
  
-----
vAuthor   varchar2(200) := 'mrussalesi';
vCreated  varchar2(200) := '20180528';
vUpdate   varchar2(200) := '20180528';
----- 
begin



for rec in (
select 
  parentid,
  adoid,
  max((select xmlflattenid from spd_t_xml_flatten 
          where node_position = flatten.parentid 
            and adoid = in_nADOID))                                     currentrecordid,
  max((select xmlflattenid from spd_t_xml_flatten 
        where adoid = in_nADOID 
          and node_position = (select parentid from spd_t_xml_flatten 
            where adoid = in_nADOID 
              and node_position = (select parentid from spd_t_xml_flatten 
                where node_position = flatten.parentid 
                  and adoid = in_nADOID))))                             parent_xmlflattenid,
  max( case upper(node_name) when 'DISPLAY' then node_value else NULL end)      DISPLAY,
  max( case upper(node_name) when 'VALUE' then node_value else NULL end)        VALUE,
  max( case upper(node_name) when 'VISUALORDER' then node_value else NULL end)  VISUALORDER
from
  spd_t_xml_flatten flatten
where
  parentid in (select flatten.node_position from spd_t_xml_flatten flatten where fpath = '/Action/Facilities/Facility/ExecutiveTable/metadata/LineItem' and adoid = in_nADOID)
  and adoid = in_nADOID
group by 
  parentid,
  adoid
)
loop

  l_row := new XML_ET_METADATA(null, null, null, null, null, null);
  
  l_row.EXECTABLEID := rec.parent_xmlflattenid;
  l_row.METADATAID := rec.currentrecordid;
  l_row.CREATED_BY  := in_vCreatedBy;
  
  begin
    l_row.DISPLAY := rec.DISPLAY;
  exception when others then
    l_row.DISPLAY := -9999;
  end;

  begin
    l_row.VALUE := rec.VALUE;
  exception when others then
    l_row.VALUE := -9999;
  end;

  begin
    l_row.VISUALORDER := rec.VISUALORDER;
  exception when others then
    l_row.VISUALORDER := -9999;
  end;

  pipe row(l_row);

end loop;


/*
for rec1 in (
select 
  (select max(xmlflattenid) from spd_t_xml_flatten 
      where node_position = (select max(parentid) from spd_t_xml_flatten 
        where node_position = flatten.parentid and adoid = in_nADOID)) EXECTABLEID, 
  flatten.* from spd_t_xml_flatten flatten where fpath = '/Action/Facilities/Facility/ExecutiveTable/metadata/LineItem' and adoid = in_nADOID
 ) 
loop

  for rec in (select * from spd_t_xml_flatten where parentid = rec1.node_position and adoid = in_nADOID order by node_position) 
  loop
  
    --i think we may have to get the PK, using the level - 1, and the parentid, and maybe something else....this may be the tricky part
    --its also possible that we just specifically find the data
    l_row.EXECTABLEID := rec1.EXECTABLEID;
    l_row.METADATAID := rec1.xmlflattenid;
    l_row.CREATED_BY  := in_vCreatedBy;

    if upper(rec.node_name) = upper('DISPLAY') then
      begin
        l_row.DISPLAY := rec.node_value;
      exception when others then
        l_row.DISPLAY := -9999;
        ----htp.p('err: ' || sqlerrm || ',' || rec.xpath); --MAKE THIS A DEBUG FUNCTION FROM HERE SO WE CAN EVENTUALLY LOG ALL ERRORS
      end;
    end if;
    
    if upper(rec.node_name) = upper('VALUE') then
      begin
        l_row.VALUE := rec.node_value;
      exception when others then
        l_row.VALUE := -9999;
        ----htp.p('err: ' || sqlerrm || ',' || rec.xpath); --MAKE THIS A DEBUG FUNCTION FROM HERE SO WE CAN EVENTUALLY LOG ALL ERRORS
      end;
    end if;
    
    if upper(rec.node_name) = upper('VISUALORDER') then
      begin
        l_row.VISUALORDER := rec.node_value;
      exception when others then
        l_row.VISUALORDER := -9999;
        ----htp.p('err: ' || sqlerrm || ',' || rec.xpath); --MAKE THIS A DEBUG FUNCTION FROM HERE SO WE CAN EVENTUALLY LOG ALL ERRORS
      end;
    end if;
       
  end loop;
  
  pipe row(l_row);

end loop;
*/

EXCEPTION WHEN OTHERS THEN
  null; --htp.p into debug (FUNCTIONNAME, INPUT, MESSAGE) values (''' ||  lg_vPackageName ||lc_vFunctionName || ''','''|| 'An error occured while extracting the data from the SPD_T_XML_FLATTEN' ||''',''' || sqlerrm || ''')');
  
end f_XML_ET_METADATA;


function f_XML_ATTENDEE (in_nID number, in_nPK number, in_nADOID number, in_vCreatedBy varchar2) return XML_ATTENDEES pipelined
as
  lc_vFunctionName varchar2(255) := 'f_XML_ATTENDEE';
  l_row   XML_ATTENDEE := new XML_ATTENDEE(null, null, null, null, null, null, null);

/* -- REFERENCE TYPE
  ATTENDEEID      NUMBER,
	EXITBRIEFID      NUMBER,
	ATTENDEE_NAME      VARCHAR2(4000),
	ATTENDEE_ORG      VARCHAR2(4000),
	PHONENUMBER      VARCHAR2(4000),
	EMAIL      VARCHAR2(4000),
	CREATED_BY      VARCHAR2(255)
*/

/*
xml node names:

*/
  
-----
vAuthor   varchar2(200) := 'mrussalesi';
vCreated  varchar2(200) := '20180528';
vUpdate   varchar2(200) := '20180528';
----- 
begin

--!!!NOTE THERE IS NO DATA IN THE SAMPLE FILES FOR THIS...USURE OF THE XML NODE NAMES, using upper() to force, may take extra time to do this.

for rec1 in (
select 
  (select max(xmlflattenid) from spd_t_xml_flatten 
        where node_position = flatten.parentid and adoid = in_nADOID) exitbriefid, 
  flatten.* from spd_t_xml_flatten flatten where fpath = '/Action/ExitBrief/Attendee' and adoid = in_nADOID
 ) 
loop

  for rec in (select * from spd_t_xml_flatten where parentid = rec1.node_position and adoid = in_nADOID order by node_position) 
  loop
  
    --i think we may have to get the PK, using the level - 1, and the parentid, and maybe something else....this may be the tricky part
    --its also possible that we just specifically find the data
    l_row.EXITBRIEFID := rec1.exitbriefid;
    l_row.ATTENDEEID := rec1.xmlflattenid;
    l_row.CREATED_BY  := in_vCreatedBy;

    if upper(rec.node_name) = upper('NAME') then
      begin
        l_row.ATTENDEE_NAME := rec.node_value;
      exception when others then
        l_row.ATTENDEE_NAME := -9999;
        ----htp.p('err: ' || sqlerrm || ',' || rec.xpath); --MAKE THIS A DEBUG FUNCTION FROM HERE SO WE CAN EVENTUALLY LOG ALL ERRORS
      end;
    end if;
    
    if upper(rec.node_name) = upper('ORGANIZATION') then
      begin
        l_row.ATTENDEE_ORG := rec.node_value;
      exception when others then
        l_row.ATTENDEE_ORG := -9999;
        ----htp.p('err: ' || sqlerrm || ',' || rec.xpath); --MAKE THIS A DEBUG FUNCTION FROM HERE SO WE CAN EVENTUALLY LOG ALL ERRORS
      end;
    end if;
    
    if upper(rec.node_name) = upper('PHONENUMBER') then
      begin
        l_row.PHONENUMBER := rec.node_value;
      exception when others then
        l_row.PHONENUMBER := -9999;
        ----htp.p('err: ' || sqlerrm || ',' || rec.xpath); --MAKE THIS A DEBUG FUNCTION FROM HERE SO WE CAN EVENTUALLY LOG ALL ERRORS
      end;
    end if;
    
    if upper(rec.node_name) = upper('EMAIL') then
      begin
        l_row.EMAIL := rec.node_value;
      exception when others then
        l_row.EMAIL := -9999;
        ----htp.p('err: ' || sqlerrm || ',' || rec.xpath); --MAKE THIS A DEBUG FUNCTION FROM HERE SO WE CAN EVENTUALLY LOG ALL ERRORS
      end;
    end if;
    
  end loop;
  
  pipe row(l_row);

end loop;
  
EXCEPTION WHEN OTHERS THEN
  null; --htp.p into debug (FUNCTIONNAME, INPUT, MESSAGE) values (''' ||  lg_vPackageName ||lc_vFunctionName || ''','''|| 'An error occured while extracting the data from the SPD_T_XML_FLATTEN' ||''',''' || sqlerrm || ''')');
  
end f_XML_ATTENDEE;


end PKG_ROWIFY;



/*
SELECT * FROM TABLE(pkg_rowify.f_XML_EXEC_TABLE(0,12345,12345,'mike'));

SELECT * FROM TABLE(pkg_rowify.f_XML_ACTION (in_nID =>0, in_nPK =>12345, in_nADOID =>12346, in_vCreatedBy =>'mike'));
SELECT * FROM TABLE(pkg_rowify.f_XML_ACTION_DRAWING (in_nID =>0, in_nPK =>12345, in_nADOID =>12346, in_vCreatedBy =>'mike'));
SELECT * FROM TABLE(pkg_rowify.f_XML_ACTION_PHOTO (in_nID =>0, in_nPK =>12345, in_nADOID =>12346, in_vCreatedBy =>'mike'));
SELECT * FROM TABLE(pkg_rowify.f_XML_ACTSUPPORT_FILE (in_nID =>0, in_nPK =>12345, in_nADOID =>12346, in_vCreatedBy =>'mike'));
SELECT * FROM TABLE(pkg_rowify.f_XML_AI_FITTING (in_nID =>0, in_nPK =>12345, in_nADOID =>12346, in_vCreatedBy =>'mike'));
SELECT * FROM TABLE(pkg_rowify.f_XML_FACILITY (in_nID =>0, in_nPK =>12345, in_nADOID =>12346, in_vCreatedBy =>'mike'));
SELECT * FROM TABLE(pkg_rowify.f_XML_EXIT_BRIEF (in_nID =>0, in_nPK =>12345, in_nADOID =>12346, in_vCreatedBy =>'mike'));
SELECT * FROM TABLE(pkg_rowify.f_XML_EXEC_TABLE (in_nID =>0, in_nPK =>12345, in_nADOID =>12346, in_vCreatedBy =>'mike'));
SELECT * FROM TABLE(pkg_rowify.f_XML_ASSET_SUMMARY (in_nID =>0, in_nPK =>12345, in_nADOID =>12346, in_vCreatedBy =>'mike'));
SELECT * FROM TABLE(pkg_rowify.f_XML_ASSET_INVENTORY (in_nID =>0, in_nPK =>12345, in_nADOID =>12346, in_vCreatedBy =>'mike'));
SELECT * FROM TABLE(pkg_rowify.f_XML_DEFECT (in_nID =>0, in_nPK =>12345, in_nADOID =>12346, in_vCreatedBy =>'mike'));
SELECT * FROM TABLE(pkg_rowify.f_XML_DEFECT_METADATA (in_nID =>0, in_nPK =>12345, in_nADOID =>12346, in_vCreatedBy =>'mike'));
SELECT * FROM TABLE(pkg_rowify.f_XML_EXEC_SUMMARY (in_nID =>0, in_nPK =>12345, in_nADOID =>12346, in_vCreatedBy =>'mike'));
SELECT * FROM TABLE(pkg_rowify.f_XML_ES_PARAGRAPH (in_nID =>0, in_nPK =>12345, in_nADOID =>12346, in_vCreatedBy =>'mike'));
SELECT * FROM TABLE(pkg_rowify.f_XML_SENTENCE (in_nID =>0, in_nPK =>12345, in_nADOID =>12346, in_vCreatedBy =>'mike'));
SELECT * FROM TABLE(pkg_rowify.f_XML_AI_METADATA (in_nID =>0, in_nPK =>12345, in_nADOID =>12346, in_vCreatedBy =>'mike'));--maybe an issue
SELECT * FROM TABLE(pkg_rowify.f_XML_FACILITY_DRAWING (in_nID =>0, in_nPK =>12345, in_nADOID =>12346, in_vCreatedBy =>'mike'));
SELECT * FROM TABLE(pkg_rowify.f_XML_FACILITY_PHOTO (in_nID =>0, in_nPK =>12345, in_nADOID =>12346, in_vCreatedBy =>'mike')) where photoid is not null;
SELECT * FROM TABLE(pkg_rowify.f_XML_FACILITY_REPAIR (in_nID =>0, in_nPK =>12345, in_nADOID =>12346, in_vCreatedBy =>'mike'));
SELECT * FROM TABLE(pkg_rowify.f_XML_FAC_SUP_FILE (in_nID =>0, in_nPK =>12345, in_nADOID =>12346, in_vCreatedBy =>'mike'));
SELECT * FROM TABLE(pkg_rowify.f_XML_FINDREC (in_nID =>0, in_nPK =>12345, in_nADOID =>12346, in_vCreatedBy =>'mike'));
SELECT * FROM TABLE(pkg_rowify.f_XML_REPAIR_PERCENTAGE (in_nID =>0, in_nPK =>12345, in_nADOID =>12346, in_vCreatedBy =>'mike'));
SELECT * FROM TABLE(pkg_rowify.f_XML_ET_METADATA (in_nID =>0, in_nPK =>12345, in_nADOID =>12346, in_vCreatedBy =>'mike'));
SELECT * FROM TABLE(pkg_rowify.f_XML_ATTENDEE(in_nID =>0, in_nPK =>12345, in_nADOID =>12346, in_vCreatedBy =>'mike'));
*/