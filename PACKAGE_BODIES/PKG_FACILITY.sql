--------------------------------------------------------
--  DDL for Package Body PKG_FACILITY
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE PACKAGE BODY "SPIDERS_DATA"."PKG_FACILITY" as 


function f_test_for_error (in_vReturned varchar2) return varchar2 as 
  /* template variables */
  lc_vFunctionName varchar2(255) := 'f_test_for_error';
  lc_vReturn varchar2(32000) := '{"error":"default"}'; --defaul the return to e for error
  lc_nContinue number :=0; --default to 0 to dissalow continuing
  
  /* function specific variables */
  lc_vTest varchar2(32000) := in_vReturned;
begin

  if substr(lc_vTest, 1, 9) = '{"error":' then
    lc_vReturn := '0';
  else
    lc_vReturn := '1';
  end if;
  
  return lc_vReturn;

exception     
  when others then
      execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' dml error: ' || sqlerrm || ''')';
      return '{"error":"' || sqlerrm || '"}';
end; 


function getCorrespondingJsonColumnVal(in_jJson json, in_vColumn varchar2) return varchar2 as 
  /* template variables */
  json_error EXCEPTION;
  spiders_error EXCEPTION;
  lc_vErrorDescription varchar2(255) := 'default';
  lc_vFunctionName varchar2(255) := 'getCorrespondingJsonColumnVal';
  lc_vReturn varchar2(32000) := '{"error":"default"}'; --defaul the return to e for error
  lc_nContinue number :=0; --default to 0 to dissalow continuing
  
  /* function specific variables */
  lc_vName varchar2(2000);
  lc_vTemp varchar2(2000);
  lc_vComment varchar2(2000);
  lc_jvValue json_value;
  lc_jlData json_list;
  lc_jvRecVal json_value;
  lc_jValue json;
begin


  if(in_jJson.exist('data')) then
      if(in_jJson.get('data').is_array) then
          lc_jlData := json_list(in_jJson.get('data'));        
          lc_jvRecVal := lc_jlData.get(1); --return null on outofbounds
          if (lc_jvRecVal is not null) then
            lc_jValue := json(lc_jvRecVal);            
            if(lc_jValue.exist(in_vColumn)) then       
              lc_jvValue := lc_jValue.get(in_vColumn);
              lc_vReturn := getVal(lc_jvValue);
            else
              lc_vErrorDescription := in_vColumn || ' attribute not found in json';
              raise spiders_error;
            end if;
          else 
            lc_vErrorDescription := 'Rec val is null';
            raise spiders_error;
          end if;
      else
        lc_vErrorDescription := 'Data is not an array';
        raise spiders_error;
      end if;
  else
    lc_vErrorDescription := 'Data attribute not found in json';
    raise spiders_error;
  end if;

  return lc_vReturn;

exception     
  when spiders_error then
      execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' SPIDERS error ' || lc_vErrorDescription || ''')';
      return '{"error":"' || lc_vErrorDescription || '"}';
  when json_error then
      execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' JSON error'')';
      return '{"error":"json"}';
  when others then
      execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' dml error: ' || sqlerrm || ''')';
      return '{"error":"' || sqlerrm || '"}';
end; 

function getAttributeValue(in_vAttribute varchar2, in_jJSON json) return varchar2 as
  /* template variables */
  json_error EXCEPTION;
  spiders_error EXCEPTION;
  lc_vErrorDescription varchar2(255) := 'default';
  lc_vFunctionName varchar2(255) := 'getAttributeValue';
  lc_vReturn varchar2(32000) := '{"error":"default"}'; --defaul the return to e for error
  lc_nContinue number :=0; --default to 0 to dissalow continuing

  /* function specific variables */
  lc_jvValue json_value;
begin

  if(in_jJSON.exist(in_vAttribute)) then
    lc_jvValue := in_jJSON.get(in_vAttribute);
    lc_vReturn := getVal(lc_jvValue);
  else
    lc_vErrorDescription := in_vAttribute || ' attribute not found in json';
    raise spiders_error;
  end if;

  /* get name of object */
  return lc_vReturn;

exception
  when spiders_error then
      execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' SPIDERS error ' || lc_vErrorDescription || ''')';
      return '{"error":"' || lc_vErrorDescription || '"}';
  when json_error then
      execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' JSON error'')';
      return '{"error":"json"}';
  when others then
      execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' dml error: ' || sqlerrm || ''')';
      return '{"error":"' || sqlerrm || '"}';
end;

function getFacilityId(in_vProductlineListID varchar2,in_vFacilityName varchar2,in_vUIC varchar2,in_vFacilityNO varchar2) return varchar2 as
  /* template variables */
  json_error EXCEPTION;
  spiders_error EXCEPTION;
  lc_vErrorDescription varchar2(255) := 'default';
  lc_vFunctionName varchar2(255) := 'getFacilityId';
  lc_vReturn varchar2(32000) := '{"error":"default"}'; --defaul the return to e for error
  lc_nContinue number :=0; --default to 0 to dissalow continuing

  /* function specific variables */
  lc_nSequence number;
  lc_vSql long;
begin

  select spd_s_facilities.nextval into lc_nSequence from dual;
  
  
  for rec in (select facid from spd_t_facilities where product_line_listid = in_vProductlineListID and spd_fac_name = in_vFacilityName and spd_uic = in_vUIC)
  loop
    lc_nSequence := rec.facid;
    lc_nContinue := 1;
  end loop;

insert into debug (message) values ('sequence: ' || lc_nSequence);

  if lc_nContinue < 1 then
    /*I'M DOING THIS THE LAZY WAY FOR NOW, NEED TO MAKE A FULL FUNCTION FOR THIS, SHAME ON ME*/
    
    Insert into spd_t_facilities (facid, product_line_listid, spd_fac_name, spd_uic, spd_fac_no) values (lc_nSequence, in_vProductlineListID,in_vFacilityName,in_vUIC,in_vFacilityNo);
    commit;
  end if;

insert into debug (message) values ('insert complete: ' || lc_nSequence);
  /* get name of object */
  return lc_nSequence;

exception
  when spiders_error then
      execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' SPIDERS error ' || lc_vErrorDescription || ''')';
      return '{"error":"' || lc_vErrorDescription || '"}';
  when json_error then
      execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' JSON error'')';
      return '{"error":"json"}';
  when others then
      execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' dml error: ' || sqlerrm || ''')';
      return '{"error":"' || sqlerrm || '"}';
end;


function addByName(in_cJson clob) return json as
  /* template variables */
  json_error EXCEPTION;
  spiders_error EXCEPTION;
  lc_vErrorDescription varchar2(255) := 'default';
  lc_vFunctionName varchar2(255) := 'getAttributeValue';
  lc_jReturn json;-- := '{"error":"default"}'; --defaul the return to e for error
  lc_nContinue number :=0; --default to 0 to dissalow continuing

  /* function specific variables */
  lc_vProductlineListID varchar2(255);
  lc_vFacilityName varchar2(255);
  lc_vUIC varchar2(255);
  lc_vFacilityNO varchar2(255);
  lc_vModule varchar2(255);
  lc_vTarget varchar2(255);
  lc_jParameterJson json;
  lc_vIsAuthorized varchar2(255);
  lc_nSequence number;
  
begin
  lc_jReturn := json();
  
  /*!! I LEFT OFF just at the end of the createModel function, have not tested*/
  lc_jParameterJson := json(in_cJson);
  lc_vModule := getAttributeValue('module', lc_jParameterJson);
  lc_nContinue := f_test_for_error(lc_vModule);
  
  if lc_nContinue = 1 then
    lc_vTarget := getAttributeValue('target', lc_jParameterJson);
    lc_nContinue := f_test_for_error(lc_vTarget);
  else
    lc_vErrorDescription := 'Module attribute not found';
    raise spiders_error;
  end if;
  if lc_nContinue = 1 then
    lc_vProductlineListID := getCorrespondingJsonColumnVal(lc_jParameterJson,'PRODUCTLINE_LISTID');
    lc_nContinue := f_test_for_error(lc_vProductlineListID);
  else
    lc_vErrorDescription := 'target attribute not found';
    raise spiders_error;
  end if;
  if lc_nContinue = 1 then
    lc_vFacilityName := getCorrespondingJsonColumnVal(lc_jParameterJson,'FACILITY_NAME');
    lc_nContinue := f_test_for_error(lc_vFacilityName);
  else
    lc_vErrorDescription := 'PRODUCTLINE_LISTID attribute not found';
    raise spiders_error;
  end if;

  if lc_nContinue = 1 then
    lc_vUIC := getCorrespondingJsonColumnVal(lc_jParameterJson,'UIC');
    lc_nContinue := f_test_for_error(lc_vUIC);
  else
    lc_vErrorDescription := 'FACILITY_NAME attribute not found';
    raise spiders_error;
  end if;
  if lc_nContinue = 1 then
    lc_vFacilityNO := getCorrespondingJsonColumnVal(lc_jParameterJson,'FACILITY_NO');
    lc_nContinue := f_test_for_error(lc_vFacilityNO);
  else
    lc_vErrorDescription := 'UIC attribute not found';
    raise spiders_error;
  end if;
/*
  if lc_nContinue = 1 then
    lc_vIsAuthorized := BB_F_AUTHORIZATION('BB_REST.create_',lc_vModule);
  else
    lc_vErrorDescription := 'target attribute not found';
    raise spiders_error; 
  end if; 
*/

  lc_vIsAuthorized := 'y'; --!!REMOVE AFTER TESTING
  insert into debug (message) values ('prior lc_vProductlineListID = '||lc_vProductlineListID);
  insert into debug (message) values ('prior lc_vFacilityName = '||lc_vFacilityName);
  insert into debug (message) values ('prior lc_vUIC = '||lc_vUIC);
  insert into debug (message) values ('prior lc_vFacilityNO = '||lc_vFacilityNO);
  
  if lc_vIsAuthorized = 'y' then
    lc_nSequence := getFacilityID(lc_vProductlineListID,lc_vFacilityName,lc_vUIC,lc_vFacilityNO);
    insert into debug (message) values ('after lc_nSequence = '||lc_nSequence);
    lc_nContinue := f_test_for_error(lc_nSequence);
  else
    lc_vErrorDescription := 'NOT AUTHORIZED';
    raise spiders_error; 
  end if;  
  insert into debug (message) values ('after last test lc_nSequence='||lc_nSequence);
  
  if lc_nContinue = 1 then
    lc_jReturn.put('facid',lc_nSequence);
  else
    lc_vErrorDescription := 'NOT AUTHORIZED';
    raise spiders_error; 
  end if;    
  

  return lc_jReturn;


exception     
  when spiders_error then
      execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' SPIDERS error ' || lc_vErrorDescription || ''')';
      htp.p('{"error":"' || lc_vFunctionName || ' SPIDERS error ' || lc_vErrorDescription || '"}');
  when json_error then
      execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' JSON error'')';
      htp.p('{"error":"' || lc_vFunctionName || ' JSON error ' || lc_vErrorDescription || '"}');
  when others then
      execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' dml error: ' || sqlerrm || ''')';
     htp.p('{"error":"' || lc_vFunctionName || ' dml error ' || sqlerrm || '"}');
end; 


end;

/
