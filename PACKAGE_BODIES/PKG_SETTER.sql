--------------------------------------------------------
--  DDL for Package Body PKG_SETTER
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE PACKAGE BODY "SPIDERS_DATA"."PKG_SETTER" as

procedure set_filtersetid(in_vValue varchar2) as  
  /* template variables */
  lc_vFunctionName varchar2(255) := 'set_filtersetid';
  
  /* function specific variables */
begin

filtersetid := in_vValue;
htp.p(filtersetid);

exception     
  when others then
      execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' error: ' || sqlerrm || ''')';
      htp.p('{"error":"' || sqlerrm || '"}');
end; 

function get_filtersetid return number as  
  /* template variables */
  lc_vFunctionName varchar2(255) := 'get_filtersetid';
  
  /* function specific variables */
begin

return filtersetid;

exception     
  when others then
      execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' error: ' || sqlerrm || ''')';
      return -1;
end; 


end;

/
