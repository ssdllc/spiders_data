--------------------------------------------------------
--  DDL for Index APEX$_ACL_IDX1
--------------------------------------------------------

  CREATE INDEX "SPIDERS_DATA"."APEX$_ACL_IDX1" ON "SPIDERS_DATA"."APEX$_ACL" ("WS_APP_ID") 
  PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  TABLESPACE "SPIDERS_DATA_TABLES" ;
