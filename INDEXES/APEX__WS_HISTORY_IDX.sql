--------------------------------------------------------
--  DDL for Index APEX$_WS_HISTORY_IDX
--------------------------------------------------------

  CREATE INDEX "SPIDERS_DATA"."APEX$_WS_HISTORY_IDX" ON "SPIDERS_DATA"."APEX$_WS_HISTORY" ("WS_APP_ID", "DATA_GRID_ID", "ROW_ID") 
  PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  TABLESPACE "SPIDERS_DATA_TABLES" ;
