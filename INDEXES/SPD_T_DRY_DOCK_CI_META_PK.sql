--------------------------------------------------------
--  DDL for Index SPD_T_DRY_DOCK_CI_META_PK
--------------------------------------------------------

  CREATE UNIQUE INDEX "SPIDERS_DATA"."SPD_T_DRY_DOCK_CI_META_PK" ON "SPIDERS_DATA"."SPD_T_DRY_DOCK_CI_META" ("DRYDOCKCIMETAID") 
  PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  TABLESPACE "SPIDERS_DATA_TABLES" ;
