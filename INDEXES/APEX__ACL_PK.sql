--------------------------------------------------------
--  DDL for Index APEX$_ACL_PK
--------------------------------------------------------

  CREATE UNIQUE INDEX "SPIDERS_DATA"."APEX$_ACL_PK" ON "SPIDERS_DATA"."APEX$_ACL" ("ID") 
  PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  TABLESPACE "SPIDERS_DATA_TABLES" ;
