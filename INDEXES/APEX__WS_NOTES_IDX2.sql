--------------------------------------------------------
--  DDL for Index APEX$_WS_NOTES_IDX2
--------------------------------------------------------

  CREATE INDEX "SPIDERS_DATA"."APEX$_WS_NOTES_IDX2" ON "SPIDERS_DATA"."APEX$_WS_NOTES" ("WS_APP_ID", "WEBPAGE_ID") 
  PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  TABLESPACE "SPIDERS_DATA_TABLES" ;
