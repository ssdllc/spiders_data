--------------------------------------------------------
--  DDL for Index SPD_T_XML_FAC_SUP_FILES_PK
--------------------------------------------------------

  CREATE UNIQUE INDEX "SPIDERS_DATA"."SPD_T_XML_FAC_SUP_FILES_PK" ON "SPIDERS_DATA"."SPD_T_XML_FAC_SUP_FILES" ("FILEID") 
  PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  TABLESPACE "SPIDERS_DATA_TABLES" ;
