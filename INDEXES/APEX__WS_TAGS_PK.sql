--------------------------------------------------------
--  DDL for Index APEX$_WS_TAGS_PK
--------------------------------------------------------

  CREATE UNIQUE INDEX "SPIDERS_DATA"."APEX$_WS_TAGS_PK" ON "SPIDERS_DATA"."APEX$_WS_TAGS" ("ID") 
  PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  TABLESPACE "SPIDERS_DATA_TABLES" ;
