--------------------------------------------------------
--  DDL for Index SPD_T_XML_IMPORT_TYPES_PK
--------------------------------------------------------

  CREATE UNIQUE INDEX "SPIDERS_DATA"."SPD_T_XML_IMPORT_TYPES_PK" ON "SPIDERS_DATA"."SPD_T_XML_IMPORT_TYPES" ("IMPORTID") 
  PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  TABLESPACE "SPIDERS_DATA_TABLES" ;
