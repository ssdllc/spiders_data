--------------------------------------------------------
--  DDL for Index SPD_T_3D_SCENE_MEMBERS_PK
--------------------------------------------------------

  CREATE UNIQUE INDEX "SPIDERS_DATA"."SPD_T_3D_SCENE_MEMBERS_PK" ON "SPIDERS_DATA"."SPD_T_3D_SCENE_MEMBERS" ("SESSIONID") 
  PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  TABLESPACE "SPIDERS_DATA_TABLES" ;
