--------------------------------------------------------
--  DDL for Index APEX$_WS_FILES_IDX1
--------------------------------------------------------

  CREATE INDEX "SPIDERS_DATA"."APEX$_WS_FILES_IDX1" ON "SPIDERS_DATA"."APEX$_WS_FILES" ("WS_APP_ID", "DATA_GRID_ID", "ROW_ID") 
  PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  TABLESPACE "SPIDERS_DATA_TABLES" ;
