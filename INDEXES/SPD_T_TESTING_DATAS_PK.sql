--------------------------------------------------------
--  DDL for Index SPD_T_TESTING_DATAS_PK
--------------------------------------------------------

  CREATE UNIQUE INDEX "SPIDERS_DATA"."SPD_T_TESTING_DATAS_PK" ON "SPIDERS_DATA"."SPD_T_TESTING_DATAS" ("QUERYDEFINITIONID") 
  PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  TABLESPACE "SPIDERS_DATA_TABLES" ;
