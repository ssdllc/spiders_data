--------------------------------------------------------
--  DDL for Index SPD_T_XML_ACTION_PHOTOS_PK
--------------------------------------------------------

  CREATE UNIQUE INDEX "SPIDERS_DATA"."SPD_T_XML_ACTION_PHOTOS_PK" ON "SPIDERS_DATA"."SPD_T_XML_ACTION_PHOTOS" ("PHOTOID") 
  PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  TABLESPACE "SPIDERS_DATA_TABLES" ;
