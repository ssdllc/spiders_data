--------------------------------------------------------
--  DDL for Index APEX$_WS_NOTES_PK
--------------------------------------------------------

  CREATE UNIQUE INDEX "SPIDERS_DATA"."APEX$_WS_NOTES_PK" ON "SPIDERS_DATA"."APEX$_WS_NOTES" ("ID") 
  PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  TABLESPACE "SPIDERS_DATA_TABLES" ;
