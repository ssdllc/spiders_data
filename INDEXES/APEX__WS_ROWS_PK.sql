--------------------------------------------------------
--  DDL for Index APEX$_WS_ROWS_PK
--------------------------------------------------------

  CREATE UNIQUE INDEX "SPIDERS_DATA"."APEX$_WS_ROWS_PK" ON "SPIDERS_DATA"."APEX$_WS_ROWS" ("ID") 
  PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  TABLESPACE "SPIDERS_DATA_TABLES" ;
