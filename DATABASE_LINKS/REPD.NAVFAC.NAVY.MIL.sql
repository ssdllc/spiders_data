--------------------------------------------------------
--  DDL for DB Link REPD.NAVFAC.NAVY.MIL
--------------------------------------------------------

  CREATE DATABASE LINK "REPD.NAVFAC.NAVY.MIL"
   CONNECT TO "SPIDERS_LINK" IDENTIFIED BY VALUES ':1'
   USING 'REPD';
