SET DEFINE OFF;

ALTER TABLE SPIDERS_DATA.SPD_T_ACTIONS
  ADD SPECIAL_AREA_CODE varchar2(15);
/
  
UPDATE SPIDERS_DATA.SPD_T_ACTIONS
  SET SPECIAL_AREA_CODE = 'host';
/
commit;