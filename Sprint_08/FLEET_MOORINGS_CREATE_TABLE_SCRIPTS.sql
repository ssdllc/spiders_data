ALTER TABLE SPD_T_FM_INSP_COMMENTS
DROP CONSTRAINT FK_FM_IC_ACTIONMOORINGID;
  
ALTER TABLE SPD_T_FM_INSP_MEASUREMENTS
DROP CONSTRAINT FK_FM_IM_ACTIONMOORINGID;

ALTER TABLE SPD_T_FM_BUOY_TOP_JEWELRY
DROP CONSTRAINT FK_FM_BTJ_ACTIONMOORINGID;
  
ALTER TABLE SPD_T_FM_BUOY_TOPSIDE
DROP CONSTRAINT FK_FM_BT_ACTIONMOORINGID;
  
ALTER TABLE SPD_T_FM_ACTION_MOORINGS
DROP CONSTRAINT FK_FM_AM_ACTIONID;
  
ALTER TABLE SPD_T_FM_ACTION_MOORINGS
DROP CONSTRAINT FK_FM_AM_MOORINGID;
  
ALTER TABLE SPD_T_FM_ACTIONS
DROP CONSTRAINT FK_FM_ACTIONS_ACTIVITIESID;
  
ALTER TABLE SPD_T_FM_MOORINGS
DROP CONSTRAINT FK_FM_MOORINGS_ACTIVITIESID;

DELETE FROM SPD_T_REST_TO_TABLE_MAPS
WHERE module = 'fleetMoorings';

declare
	lc_jJson json;
begin
	lc_jJson := json('{
	"collectionName":"FM_MOORINGS",
	"modelName":"FM_MOORING",
	"module":"fleetMoorings",
	"path":[],
	"comment":"This table is used to store all fleet moorings.",
	"columns":[
	{"name":"FMMOORINGID","type":"NUMBER","primaryKey":"yes"},
	{"name":"ACTIVITIESID","type":"NUMBER","primaryKey":""},
	{"name":"DECOMISSIONED","type":"NUMBER(1)","primaryKey":""},
	{"name":"MOORING_NAME","type":"VARCHAR2(64)","primaryKey":""},
	{"name":"SPECIAL_AREA_CODE","type":"VARCHAR2(64)","primaryKey":""}
	],
	"order":[["MOORING_NAME","DESC"]],
	"dataTable":{}
	}');

	htp.p(pkg_tables.f_make_all_table_objects(lc_jJson));
end;
/

declare
	lc_jJson json;
begin
	lc_jJson := json('{
	"collectionName":"FM_ACTIONS",
	"modelName":"FM_ACTION",
	"module":"fleetMoorings",
	"path":[],
	"comment":"This table is used to store all fleet mooring actions.",
	"columns":[
	{"name":"FMACTIONID","type":"NUMBER","primaryKey":"yes"},
	{"name":"ACTIVITIESID","type":"NUMBER","primaryKey":""},
	{"name":"SPECIAL_AREA_CODE","type":"VARCHAR2(64)","primaryKey":""},
	{"name":"ACTION_TYPE","type":"VARCHAR2(64)","primaryKey":""},
	{"name":"REPORT_NUMBER","type":"VARCHAR2(255)","primaryKey":""},
  {"name":"REPORT_DATE","type":"DATE","primaryKey":""}
	],
	"order":[["REPORT_DATE","DESC"]],
	"dataTable":{}
	}');

	htp.p(pkg_tables.f_make_all_table_objects(lc_jJson));
end;
/

declare
	lc_jJson json;
begin
	lc_jJson := json('{
	"collectionName":"FM_ACTION_MOORINGS",
	"modelName":"FM_ACTION_MOORING",
	"module":"fleetMoorings",
	"path":[],
	"comment":"This table is used to store all fleet mooring action moorings.",
	"columns":[
  {"name":"FMACTIONMOORINGID","type":"NUMBER","primaryKey":"yes"},
	{"name":"FMACTIONID","type":"NUMBER","primaryKey":""},
	{"name":"FMMOORINGID","type":"NUMBER","primaryKey":""},
	{"name":"DESIGN_CLASS","type":"VARCHAR2(4)","primaryKey":""},
	{"name":"DESIGN_RATED_CAPACITY","type":"NUMBER","primaryKey":""},
	{"name":"BUOY_TYPE","type":"VARCHAR(64)","primaryKey":""},
  {"name":"BUOY_DIAMETER","type":"NUMBER","primaryKey":""},
  {"name":"MOORING_TYPE","type":"VARCHAR(64)","primaryKey":""},
  {"name":"QUANTITY_OF_GROUND_LEGS","type":"NUMBER","primaryKey":""},
  {"name":"ANCHOR_TYPE","type":"VARCHAR(64)","primaryKey":""},
  {"name":"ANCHOR_SIZE","type":"VARCHAR(64)","primaryKey":""},
  {"name":"RISER_SIZE","type":"NUMBER","primaryKey":""},
  {"name":"GROUND_LEG_SIZE","type":"NUMBER","primaryKey":""},
  {"name":"MOORING_CONDITION_READINESS","type":"VARCHAR(64)","primaryKey":""},
  {"name":"INSPECTION_DATE","type":"DATE","primaryKey":""},
  {"name":"MOORING_RATED_CONDITION","type":"NUMBER","primaryKey":""},
  {"name":"REQUIRED_REPAIRS","type":"VARCHAR(255)","primaryKey":""},
  {"name":"CURRENT_CLASS","type":"VARCHAR(64)","primaryKey":""},
  {"name":"CURRENT_RATED_CAPACITY","type":"NUMBER","primaryKey":""},
  {"name":"DIVERS","type":"VARCHAR(255)","primaryKey":""},
  {"name":"ENGINEER_IN_CHARGE","type":"VARCHAR(70)","primaryKey":""},
  {"name":"BUOY_FREEBOARD","type":"NUMBER","primaryKey":""},
  {"name":"WATER_VISIBILITY","type":"VARCHAR2(64)","primaryKey":""},
  {"name":"WATER_DEPTH","type":"NUMBER","primaryKey":""},
  {"name":"BOTTOM_CONDITION","type":"VARCHAR(64)","primaryKey":""},
  {"name":"LAT_LONG_DATUM","type":"VARCHAR(64)","primaryKey":""},
  {"name":"LATITUDE","type":"VARCHAR2(64)","primaryKey":""},
  {"name":"LONGITUDE","type":"VARCHAR2(64)","primaryKey":""},
  {"name":"NORTHING_EASTING_DATUM","type":"VARCHAR(64)","primaryKey":""},
  {"name":"NORTHING","type":"NUMBER","primaryKey":""},
  {"name":"EASTING","type":"NUMBER","primaryKey":""},
  {"name":"ANODES_DEPLETED","type":"NUMBER","primaryKey":""},
  {"name":"ANODES_REPLACED","type":"VARCHAR(1)","primaryKey":""},
  {"name":"REPLACEMENT_ANODE_SIZE","type":"NUMBER","primaryKey":""},
  {"name":"QUANTITY_ANODES_REPLACED","type":"NUMBER","primaryKey":""}
	],
	"order":[["FMACTIONMOORINGID","DESC"]],
	"dataTable":{}
	}');

	htp.p(pkg_tables.f_make_all_table_objects(lc_jJson));
end;
/

declare
	lc_jJson json;
begin
	lc_jJson := json('{
	"collectionName":"FM_BUOY_TOPSIDE",
	"modelName":"FM_BUOY_TOPSIDE",
	"module":"fleetMoorings",
	"path":[],
	"comment":"This table is used to store all fleet mooring buoy topside inspection assessments.",
	"columns":[
	{"name":"FMBUOYTOPSIDEID","type":"NUMBER","primaryKey":"yes"},
	{"name":"FMACTIONMOORINGID","type":"NUMBER","primaryKey":""},
	{"name":"COMPONENT","type":"VARCHAR2(64)","primaryKey":""},
	{"name":"TYPE_QTY","type":"VARCHAR2(64)","primaryKey":""},
	{"name":"DAMAGE_WEAR","type":"VARCHAR2(64)","primaryKey":""},
  {"name":"CORROSION_DISCOLORATION","type":"VARCHAR2(64)","primaryKey":""},
  {"name":"OVERALL_CONDITION","type":"VARCHAR2(64)","primaryKey":""}
	],
	"order":[["FMBUOYTOPSIDEID","DESC"]],
	"dataTable":{}
	}');

	htp.p(pkg_tables.f_make_all_table_objects(lc_jJson));
end;
/

declare
	lc_jJson json;
begin
	lc_jJson := json('{
	"collectionName":"FM_BUOY_TOP_JEWELRY",
	"modelName":"FM_BUOY_TOP_JEWELRY",
	"module":"fleetMoorings",
	"path":[],
	"comment":"This table is used to store all fleet mooring buoy top jewelry inspection measurements.",
	"columns":[
	{"name":"FMBUOYTOPJEWELRYID","type":"NUMBER","primaryKey":"yes"},
	{"name":"FMACTIONMOORINGID","type":"NUMBER","primaryKey":""},
	{"name":"ITEM","type":"VARCHAR2(64)","primaryKey":""},
	{"name":"INSPECTION_POINT","type":"VARCHAR2(64)","primaryKey":""},
	{"name":"CATALOG_VALUE","type":"VARCHAR2(64)","primaryKey":""},
  {"name":"CALIPER_RESULTS","type":"NUMBER","primaryKey":""},
  {"name":"CONDITION","type":"NUMBER","primaryKey":""}
	],
	"order":[["FMBUOYTOPJEWELRYID","DESC"]],
	"dataTable":{}
	}');

	htp.p(pkg_tables.f_make_all_table_objects(lc_jJson));
end;
/

declare
	lc_jJson json;
begin
	lc_jJson := json('{
	"collectionName":"FM_INSP_MEASUREMENTS",
	"modelName":"FM_INSP_MEASUREMENT",
	"module":"fleetMoorings",
	"path":[],
	"comment":"This table is used to store all fleet mooring inspection measurement results.",
	"columns":[
	{"name":"FMINSPMEASUREMENTID","type":"NUMBER","primaryKey":"yes"},
  {"name":"FMACTIONMOORINGID","type":"NUMBER", "primaryKey":""},
	{"name":"ITEM","type":"NUMBER","primaryKey":""},
	{"name":"CRITERIA","type":"VARCHAR2(64)","primaryKey":""},
	{"name":"INSPECTION_POINT","type":"VARCHAR2(64)","primaryKey":""},
	{"name":"UNIT_OF_MEASURE","type":"VARCHAR2(64)","primaryKey":""},
  {"name":"REFERENCE_POINT","type":"VARCHAR2(64)","primaryKey":""},
  {"name":"FM_SIZE","type":"NUMBER","primaryKey":""},
  {"name":"CONDITION","type":"VARCHAR2(64)","primaryKey":""}
	],
	"order":[["FMINSPMEASUREMENTID","DESC"]],
	"dataTable":{}
	}');

	htp.p(pkg_tables.f_make_all_table_objects(lc_jJson));
end;
/

declare
	lc_jJson json;
begin
	lc_jJson := json('{
	"collectionName":"FM_INSP_COMMENTS",
	"modelName":"FM_INSP_COMMENT",
	"module":"fleetMoorings",
	"path":[],
	"comment":"This table is used to store all fleet mooring inspection comment results.",
	"columns":[
	{"name":"FMINSPCOMMENTID","type":"NUMBER","primaryKey":"yes"},
  {"name":"FMACTIONMOORINGID","type":"NUMBER", "primaryKey":""},
	{"name":"COMMENT_TYPE","type":"VARCHAR2(64)","primaryKey":""},
	{"name":"FM_COMMENT","type":"VARCHAR2(255)","primaryKey":""}
	],
	"order":[["FMINSPCOMMENTID","DESC"]],
	"dataTable":{}
	}');

	htp.p(pkg_tables.f_make_all_table_objects(lc_jJson));
end;
/

ALTER TABLE SPD_T_FM_INSP_COMMENTS
ADD CONSTRAINT FK_FM_IC_ACTIONMOORINGID
  FOREIGN KEY (FMACTIONMOORINGID)
  REFERENCES SPD_T_FM_ACTION_MOORINGS(FMACTIONMOORINGID);
  
ALTER TABLE SPD_T_FM_INSP_MEASUREMENTS
ADD CONSTRAINT FK_FM_IM_ACTIONMOORINGID
  FOREIGN KEY (FMACTIONMOORINGID)
  REFERENCES SPD_T_FM_ACTION_MOORINGS(FMACTIONMOORINGID);

ALTER TABLE SPD_T_FM_BUOY_TOP_JEWELRY
ADD CONSTRAINT FK_FM_BTJ_ACTIONMOORINGID
  FOREIGN KEY (FMACTIONMOORINGID)
  REFERENCES SPD_T_FM_ACTION_MOORINGS(FMACTIONMOORINGID);
  
ALTER TABLE SPD_T_FM_BUOY_TOPSIDE
ADD CONSTRAINT FK_FM_BT_ACTIONMOORINGID
  FOREIGN KEY (FMACTIONMOORINGID)
  REFERENCES SPD_T_FM_ACTION_MOORINGS(FMACTIONMOORINGID);
  
ALTER TABLE SPD_T_FM_ACTION_MOORINGS
ADD CONSTRAINT FK_FM_AM_ACTIONID
  FOREIGN KEY (FMACTIONID)
  REFERENCES SPD_T_FM_ACTIONS(FMACTIONID);
  
ALTER TABLE SPD_T_FM_ACTION_MOORINGS
ADD CONSTRAINT FK_FM_AM_MOORINGID
  FOREIGN KEY (FMMOORINGID)
  REFERENCES SPD_T_FM_MOORINGS(FMMOORINGID);
  
ALTER TABLE SPD_T_FM_ACTIONS
ADD CONSTRAINT FK_FM_ACTIONS_ACTIVITIESID
  FOREIGN KEY (ACTIVITIESID)
  REFERENCES SPD_T_ACTIVITIES(ACTIVITIESID);
  
ALTER TABLE SPD_T_FM_MOORINGS
ADD CONSTRAINT FK_FM_MOORINGS_ACTIVITIESID
  FOREIGN KEY (ACTIVITIESID)
  REFERENCES SPD_T_ACTIVITIES(ACTIVITIESID);

commit;