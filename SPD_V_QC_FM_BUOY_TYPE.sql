--------------------------------------------------------
--  DDL for View SPD_V_QC_FM_BUOY_TYPE
--------------------------------------------------------

  CREATE OR REPLACE VIEW "SPIDERS_DATA"."SPD_V_QC_FM_BUOY_TYPE" AS 
  SELECT DISTINCT
        BUOY_TYPE
	FROM 
        SPD_T_FM_ACTION_MOORINGS
  WHERE
        BUOY_TYPE IS NOT NULL;