--------------------------------------------------------
--  DDL for Function SPD_F_MAKE_ASYNC_JOB_XML_PARSE
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE FUNCTION "SPIDERS_DATA"."SPD_F_MAKE_ASYNC_JOB_XML_PARSE" (in_cJSON clob) return json AS 

lc_jReturn json;
lc_nRandom number;
lc_vAppUser long;

BEGIN
  
    lc_vAppUser := v('APP_USER');
  
    lc_nRandom := round(dbms_random.value(1,10000),0);
    lc_jReturn := json('{"status":"success"}');

    insert into debug (message) values ('SPD_F_MAKE_ASYNC_JOB_XML_PARSE');
    dbms_scheduler.create_job(
        job_name        => 'async_job_xml_parse_' || lc_nRandom,
        program_name    => 'SPD_ASYNC_JOB_RUN_XML_PARSE',
        start_date      => systimestamp,
        end_date        => null
    );

    dbms_scheduler.set_job_argument_value(
        job_name        => 'async_job_xml_parse_' || lc_nRandom,
        argument_position => 1,
        argument_value    => in_cJSON
    );
    
    dbms_scheduler.set_job_argument_value(
        job_name        => 'async_job_xml_parse_' || lc_nRandom,
        argument_position => 2,
        argument_value    => lc_vAppUser
    );

    dbms_scheduler.enable (name => 'async_job_xml_parse_' || lc_nRandom);
    insert into debug (message) values ('SPD_F_MAKE_ASYNC_JOB_XML_PARSE END');
    
    return lc_jReturn;
  
exception
  when others then
    execute immediate 'insert into debug (message) values (''error in SPD_P_MAKE_ASYNC_JOB_XML_PARSE'')';
  
END SPD_F_MAKE_ASYNC_JOB_XML_PARSE;
 

/
