--------------------------------------------------------
--  DDL for Function BB_F_READ_REPORT_LIBRARY
--------------------------------------------------------

 create or replace FUNCTION                "BB_F_READ_REPORT_LIBRARY" (in_jJson json) RETURN json AS

  lc_jJson json;
  lc_data json_list;
  lc_rec_val json_value;
  lc_rec json;
  lc_adoid_val json_value; --
  lc_nAdoid number;

  lc_ret_str varchar(512);
  lc_ret json;

  thesql varchar2(2000) := '';

  --error stuff
  lc_jError json;
  lc_jlMessages json_list;
  json_error EXCEPTION;
  
  lc_jlAoData json_list;
  lc_vSearch varchar2(2000);
  lc_jvSearch json_value;
  lc_jValue json;
  lc_jvValue json_value;
  
  lc_jValue2 json;
  lc_jvValue2 json_value;
  lc_jvValue3 json_value;
  lc_jvValue4 json_value;
  
  lc_jValue3 json;
  
  lc_jlValue2 json_list;
  lc_jlValue4 json_list;


  lc_vValue varchar2(2000);
  lc_vWhere varchar2(2000);
  lc_nStart number := 1;
  lc_nEnd number := 10;
  
  lc_nOrderByCol number;
  lc_vOrderByCol varchar2(2000);
  lc_vOrderBySort varchar2(2000);
  lc_vOrderBy varchar2(2000) := ' reportid asc ';
  
  lc_nGetValuesFromAodata number;
  

BEGIN
/*
stub data
{
    "module": "reportLibraryManager",
    "target": "reportLibrary",
    "aoData": [],
    "groupFilterData": [
        {
            "group_name": "PRODUCTLINE",
            "data": [
                100085,
                100086
            ]
        },
        {
            "group_name": "YEAR",
            "data": [
                2014,
                2013,
                2012
            ]
        }
    ],
    "data": []
}
*/
--DECODE(to_char(eststartdate,'YYYY'),null,'000',to_char(eststartdate,'YYYY')) in ('2015')
lc_vWhere := parseGroupFilterData(in_jJson);
--lc_vWhere := replace(lc_vWhere, 'YEAR', 'to_char(eststartdate,''YYYY'')');
lc_vWhere := replace(lc_vWhere, 'YEAR', 'eststartdate');
--lc_vWhere := replace(lc_vWhere, 'YEAR', 'DECODE(to_char(eststartdate,''YYYY''),null,''000'',to_char(eststartdate,''YYYY''))');
lc_vWhere := replace(lc_vWhere, 'PRODUCTLINE', 'PRODUCTLINE_LISTID');
lc_vWhere := replace(lc_vWhere, 'REPORT_AUTHOR', 'REPORTAUTHOR');

--lc_vWhere := replace(lc_vWhere, 'year', 'to_char(eststartdate,''YYYY'')');
--lc_vWhere := replace(lc_vWhere, 'year', 'DECODE(to_char(eststartdate,''YYYY''),null,''000'',to_char(eststartdate,''YYYY''))');
lc_vWhere := replace(lc_vWhere, 'year', 'eststartdate');
lc_vWhere := replace(lc_vWhere, 'productline', 'PRODUCTLINE_LISTID');
lc_vWhere := replace(lc_vWhere, 'report_author', 'REPORTAUTHOR');


lc_nGetValuesFromAodata := parseAoData(in_jJson, lc_vSearch, lc_nStart, lc_nEnd, lc_vOrderBy);


if(lc_nGetValuesFromAodata = 0) then
  lc_vSearch :='';
  lc_nStart :=1;
  lc_nEnd :=10;
  lc_vOrderBy :=' REPORTID asc ';
end if;

if length(lc_vSearch) is null then
  lc_vWhere := lc_vWhere;
else
  if length(lc_vWhere) is null then
    lc_vWhere := ' Where upper(REPORTID || 
    REPORTTITLE     || 
    REPORTAUTHOR ||     
    ESTSTARTDATE ||     
    CONTRACTOR ||     
    CONTRACTNUMBER ||     
    CREATEDDATE || 
    LOCATION ||
    REPORTNUMBER)
    like ''%' || upper(lc_vSearch) || '%''';
  else
    lc_vWhere := lc_vWhere || ' AND  
    upper(REPORTID || 
    REPORTTITLE     || 
    REPORTAUTHOR ||     
    ESTSTARTDATE ||     
    CONTRACTOR ||     
    CONTRACTNUMBER ||     
    CREATEDDATE ||     
    REPORTNUMBER)
    like ''%' || upper(lc_vSearch) || '%''';
  end if;
end if;




--to_char(ESTSTARTDATE,''YYYY'') ESTSTARTDATE,

  thesql := 
'select 
    * 
  from (
    select x.*,
    row_number() over (order by ' || lc_vOrderBy || ') rn,
    count(*) over (partition by (select 1 from dual)) countOfRecords
    from (
      select 
        REPORTID,
        REPORTTITLE,
        REPORTAUTHOR,
        DECODE(to_char(eststartdate,''YYYY''),null,'''',to_char(eststartdate,''YYYY'')) ESTSTARTDATE,
        CONTRACTOR,
        CONTRACTNUMBER,
        CREATEDDATE,
        REPORTNUMBER,
        ACTIONSID,
        (select spd_activity_name from spd_t_activities actv, spd_t_action_planner ap where actv.activitiesid = ap.planned_activitiesid and ap.apid = rep.ACTIONSID) ACTIVITY_NAME,
        (select spd_uic from spd_t_activities actv, spd_t_action_planner ap where actv.activitiesid = ap.planned_activitiesid and ap.apid = rep.ACTIONSID) ACTIVITY_UIC,
        CEACTIONSID,
        CITY,
        STATE,
        COUNTRY,
        LOCATION_CUSTOM,
        NVL2(LOCATION_CUSTOM, LOCATION_CUSTOM, COUNTRY || '' - '' || STATE) LOCATION,   
        NOTESKEY,
        GOVPROJENGINEER,
        PRODUCTLINE_LISTID,
        (select PRODUCTLINE_DISPLAY from SPD_V_QC_PRODUCTLINES where PRODUCTLINE_LISTID = rep.productline_listid) productline_display,
        (select count(*) from spd_t_report_files rf where rf.reportid = rep.reportid and BFILEEXISTS(the_bfile) = 1) countOfFiles,
        FOLDERID
      from 
        spd_t_reports rep   
) x
  ' || lc_vWhere || '
  order by
  ' || lc_vOrderBy || ' )
where rn between ' || lc_nStart || ' and ' || lc_nEnd;


--htp.p(thesql);


  lc_jJson := json_dyn.executeObject(thesql);

  return lc_jJson;

exception
    when json_error then
        --htp.p('raised json_error in update func');
        lc_jError := json();
        lc_jError.put('function','BB_F_READ_REPORT_LIBRARY');
        lc_jError.put('input',in_jJson.to_char);
        lc_jError.put('messages',lc_jlMessages);
        BB_P_ERROR(lc_jError);
        --htp.p('returning n');
        return json('e');
    when others then
        lc_jlMessages := json_list();
        lc_jError := json();
        lc_jError.put('function','BB_F_READ_REPORT_LIBRARY');
        lc_jError.put('input',in_jJson.to_char);
        lc_jlMessages.append(json_list('[{error:''RDRL.000X ' || replace(SQLERRM,chr(10),'') || '''}]'));
        lc_jError.put('messages',lc_jlMessages);
        BB_P_ERROR(lc_jError);
        return json('e');


END BB_F_READ_REPORT_LIBRARY;
 

/
