--------------------------------------------------------
--  DDL for Function BB_F_UPDATE_XML_FACILITIES
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE FUNCTION "SPIDERS_DATA"."BB_F_UPDATE_XML_FACILITIES" (in_jJson json, o_jResults out json, o_jMessages out json ) RETURN VARCHAR2 AS

  lc_jJson json;
  lc_data json_list;
  lc_rec_val json_value;
  lc_rec json;
  lc_DOFID_val json_value; --
  lc_nDOFID number;
  lc_xmlFacilityId_val json_value;
  lc_nXmlFacilityId number;

  lc_ret_str varchar(512);
  lc_ret json;

  lc_jResults json;
  lc_jlDML json_list;

  lc_jMessages json;
  lc_jlMessages json_list;
  lc_bReturn varchar2(1);

  --error stuff
  lc_jError json;
  json_error EXCEPTION;

BEGIN
/*
stub data
{"module":"xmlImportManager","target":"matchFacilities","data":[{"DOFID":1, "XMLFACILITYMATCHID" : 1 }]}
*/


    lc_jResults := json('{}');
    lc_jlDML := json_list('[]');

    lc_jMessages := json('{}');
    lc_jlMessages := json_list('[]');

    if(in_jJson.exist('data')) then
        if(in_jJson.get('data').is_array) then
            lc_data := json_list(in_jJson.get('data'));
            dbms_output.put_line(lc_data.count);
            lc_rec_val := lc_data.get(1); --return null on outofbounds
            if (lc_rec_val is not null) then
                lc_rec := json(lc_rec_val);
                if (lc_rec.exist('DOFID')) then
                    lc_DOFID_val := lc_rec.get('DOFID');
                    lc_nDOFID := lc_DOFID_val.get_number;
                else
                    lc_jlMessages.append(json_list('[{error:''Error UXFM-0001 There has been an error processing the request''}]'));
                end if;
                if (lc_rec.exist('XMLFACILITYMATCHID')) then
                    lc_xmlFacilityId_val := lc_rec.get('XMLFACILITYMATCHID');
                    lc_nXmlFacilityId := lc_xmlFacilityId_val.get_number;
                else
                    lc_jlMessages.append(json_list('[{error:''Error UXFM-0002 There has been an error processing the request''}]'));
                end if;
            else
                lc_jlMessages.append(json_list('[{error:''Error UXFM-0003 There has been an error processing the request''}]'));
            end if;
        else
            lc_jlMessages.append(json_list('[{error:''Error UXFM-0004 There has been an error processing the request''}]'));
        end if;
    else
        lc_jlMessages.append(json_list('[{error:''Error UXFM-0005 There has been an error processing the request''}]'));
    end if;





    lc_jMessages.put('messages',lc_jlMessages);
    


    --if there are any messages this is not valid return false
    if (lc_jlMessages.count > 0) then
        --htp.p('RETURN n');
        lc_bReturn := 'n';
        lc_jMessages.put('status','error');
        
        raise json_error;
    else

        --this is a valid object create the dml
        lc_jlDML.append('update spd_t_xml_facilities_match set DOFID = ' || lc_nDOFID || ' where XMLFACILITYMATCHID = ' || lc_nXmlFacilityId );
        lc_jResults.put('results',lc_jlDML);

        --htp.p('RETURN y');
        lc_bReturn := 'y';
        lc_jMessages.put('status','pending dml');
    end if;

    o_jMessages := lc_jMessages;
    o_jResults := lc_jResults;

    return lc_bReturn;

  exception
    when json_error then
        --htp.p('raised json_error in update func');
        lc_jError := json();
        lc_jError.put('function','BB_F_UPDATE_XML_FACILITIES');
        lc_jError.put('input',in_jJson.to_char);
        lc_jError.put('messages',lc_jlMessages);
        BB_P_ERROR(lc_jError);
        --htp.p('returning n');
        return 'e';
    when others then
        lc_jError := json();
        lc_jError.put('function','BB_F_UPDATE_XML_FACILITIES');
        lc_jError.put('input',in_jJson.to_char);
        lc_jlMessages.append(json_list('[{error:''UXFM.000X ' || SQLERRM || '''}]'));
        lc_jError.put('messages',lc_jlMessages);
        BB_P_ERROR(lc_jError);
        return 'e';

END BB_F_UPDATE_XML_FACILITIES;
 

/
