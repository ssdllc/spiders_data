--------------------------------------------------------
--  DDL for Function PURGEDDCSVFILE
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE FUNCTION "SPIDERS_DATA"."PURGEDDCSVFILE" (in_vFlowsFilesName in varchar2, in_vFileType varchar2) return number IS

lc_bBlob BLOB;
lc_cClob clob;
lc_nStart NUMBER := 1;
lc_nBytelen NUMBER := 32000;
lc_nLen NUMBER;
lc_rRaw RAW(32000);
lc_nContinue number :=0;
lc_nXmlfileid number :=0;
x NUMBER;

l_output utl_file.file_type;

lc_jError json;
lc_jlMessages json_list;

thesql varchar2(2000);

lc_jlDML json_list;
lc_jResults json;
lc_jMessages json;
lc_vDMLResult varchar2(255);
lc_ret_str varchar(512);
lc_ret json;

lc_vFilename varchar2(2000);
lc_nLoopCounter number :=0;
lc_vWWVFlowName varchar2(2000);
lc_nSequence number :=0;
lc_vMimeType varchar2(4000);

/*EDITED 20170623 MR DEBUG: [SPD_BUG_FIX][03] Dry Dock Upload*/
/*EDITED 20170913 MR COMMENT DEBUG: [SPD_BUG_FIX][03] Dry Dock Upload*/


BEGIN
      

/*debugging*/
--insert into debug (functionname, input, message) values ('PURGEDDCSVFILE','[SPD_BUG_FIX][03] Dry Dock Upload','1');
/*end debugging*/

  lc_jResults := json();
  lc_jlDML := json_list();
 
  
  /*!! Delete the data from the dd_ci table and the xml_files table for a match for this exact filename*/
  for rec in (select xmlfileid from spd_t_xml_files where filename = in_vFlowsFilesName) 
  loop


/*debugging*/
--insert into debug (functionname, input, message) values ('PURGEDDCSVFILE','[SPD_BUG_FIX][03] Dry Dock Upload','2-' || rec.xmlfileid);
/*end debugging*/ 

    lc_nXmlfileid := rec.xmlfileid;  
    
    thesql := 'delete from spd_t_xml_files where xmlfileid = ''' || lc_nXmlfileid || '''';
    lc_jlDML.append(thesql);
    
    thesql := 'delete from spd_t_dry_dock_ci_tmp'; --file id is not fully implemented here, front end or in next func
    lc_jlDML.append(thesql);
    
    thesql := 'delete from spd_t_dry_dock_ci where xmlfileid = ''' || lc_nXmlfileid || '''';
    lc_jlDML.append(thesql);
  end loop;
  
  
  /* there are issues if the same filename is loaded into the wwv_flows_files table, make a loop to grab the most recent uploaded version and use that id specifically */
  for rec in ( select name from wwv_flow_files where FILENAME = in_vFlowsFilesName order by created_on desc) loop
    if lc_nLoopCounter = 0 then
      lc_vWWVFlowName := rec.name;
      lc_nLoopCounter := 1;      
    end if;
  end loop;
  
  
  /*!! Add the csv file to the table spd_t_xml_files !!*/
  
  
  select SPIDERS_DATA.SPD_S_XML_FILES.nextval into lc_nSequence from dual;  
  thesql := 'insert into spd_t_xml_files
    (
        XMLFILEID,
        ADOID,
        APEXNAME,
        FILENAME,
        TITLE,
        MIME_TYPE,
        DOC_SIZE,
        DAD_CHARSET,
        CREATED_BY,
        CREATED_ON,
        UPDATED_BY,
        UPDATED_ON,
        LAST_UPDATED,
        CONTENT_TYPE,
        BLOB_CONTENT,
        LANGUAGE,
        DESCRIPTION,
        FILE_TYPE,
        FILE_CHARSET
    )
        select
        ' || lc_nSequence || ',
        1,
        wwv_flow_files.NAME,
        wwv_flow_files.FILENAME,
        wwv_flow_files.TITLE,
        wwv_flow_files.MIME_TYPE,
        wwv_flow_files.DOC_SIZE,
        wwv_flow_files.DAD_CHARSET,
        wwv_flow_files.CREATED_BY,
        wwv_flow_files.CREATED_ON,
        wwv_flow_files.UPDATED_BY,
        wwv_flow_files.UPDATED_ON,
        wwv_flow_files.LAST_UPDATED,
        wwv_flow_files.CONTENT_TYPE,
        wwv_flow_files.BLOB_CONTENT,
        wwv_flow_files.LANGUAGE,
        wwv_flow_files.DESCRIPTION,
        ''' || in_vFileType || ''',
        wwv_flow_files.FILE_CHARSET
    from
        wwv_flow_files
    where
        name = ''' ||  lc_vWWVFlowName || '''';  
        
    
    ----insert into debug (message) values (thesql);
/*debugging*/
--insert into debug (functionname, input, message) values ('PURGEDDCSVFILE','[SPD_BUG_FIX][03] Dry Dock Upload','3');
/*end debugging*/ 

/*debugging*/
  --for rec in (select * from wwv_flow_files) loop
--insert into debug (functionname, input, message) values ('PURGEDDCSVFILE','[SPD_BUG_FIX][03] Dry Dock Upload','4');
  --end loop;
/*end debugging*/

    lc_jlDML.append(thesql);
    
    theSql := 'delete from wwv_flow_files';-- where filename = ''' ||  in_vFlowsFilesName || '''';  
    lc_jlDML.append(thesql);
    lc_jResults.put('results',lc_jlDML);
    
/*debugging*/
--insert into debug (functionname, input, message) values ('PURGEDDCSVFILE','[SPD_BUG_FIX][03] Dry Dock Upload','5');
/*end debugging*/ 
    
    
    lc_vDMLResult := BB_F_DML(lc_jResults, lc_jMessages);
    
/*debugging*/
--insert into debug (functionname, input, message) values ('PURGEDDCSVFILE','[SPD_BUG_FIX][03] Dry Dock Upload','6');
/*end debugging*/ 
    lc_jMessages.put('id',lc_nSequence);
    
/*debugging*/
--insert into debug (functionname, input, message) values ('PURGEDDCSVFILE','[SPD_BUG_FIX][03] Dry Dock Upload','7');
/*end debugging*/ 
    
    /*
    for rec in (select * from wwv_flow_files) loop
      --insert into debug (message) values ('2: ' || rec.FILENAME);
    end loop;
    */
  
/*debugging*/
--insert into debug (functionname, input, message) values ('PURGEDDCSVFILE','[SPD_BUG_FIX][03] Dry Dock Upload','8');
/*end debugging*/ 

  if lc_vDMLResult = 'y' then
/*debugging*/
--insert into debug (functionname, input, message) values ('PURGEDDCSVFILE','[SPD_BUG_FIX][03] Dry Dock Upload','9');
/*end debugging*/ 
    return lc_nSequence;
  else
/*debugging*/
--insert into debug (functionname, input, message) values ('PURGEDDCSVFILE','[SPD_BUG_FIX][03] Dry Dock Upload','10');
/*end debugging*/ 
    return 0;
  end if;

exception 
  when others then

/*debugging*/
insert into debug (functionname, input, message) values ('PURGEDDCSVFILE','[SPD_BUG_FIX][03] Dry Dock Upload','e1');
/*end debugging*/

      /*
      lc_jError := json();
      lc_jlMessages := json_list();
      lc_jError := json();
      lc_jError.put('function','PURGEDDCSVFILE');
      lc_jError.put('input','');

      lc_jlMessages.append(json_list('[{error:''PDDCSVF.000X blah''}]'));
      lc_jError.put('messages',lc_jlMessages);
      BB_P_ERROR(lc_jError);
      --htp.p('e');
      */

end PURGEDDCSVFILE;

/
