--------------------------------------------------------
--  DDL for Function BB_F_READ_USERS
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE FUNCTION "SPIDERS_DATA"."BB_F_READ_USERS" (in_jJson json) RETURN json AS

    lc_jJson json;
    thesql varchar2(2000) := '';


    --error stuff
    lc_jError json;
    lc_jlMessages json_list;
    json_error EXCEPTION;


BEGIN
/*
stub data
{"target":"users","data":[]}
*/

thesql := 'select
    employeeid
from
    ssd_security_users
group by
    employeeid
order by
    employeeid
';

    
    lc_jJson := json_dyn.executeObject(thesql);

    
    return lc_jJson;

exception
    when json_error then
        --htp.p('raised json_error in update func');
        lc_jError := json();
        lc_jError.put('function','BB_F_READ_USERS');
        lc_jError.put('input',in_jJson.to_char);
        lc_jError.put('messages',lc_jlMessages);
        BB_P_ERROR(lc_jError);
        --htp.p('returning n');
        return json('e');
    when others then
        lc_jError := json();
        lc_jError.put('function','BB_F_READ_USERS');
        lc_jError.put('input',in_jJson.to_char);
        lc_jlMessages.append(json_list('[{error:''RU.000X ' || SQLERRM || '''}]'));
        lc_jError.put('messages',lc_jlMessages);
        BB_P_ERROR(lc_jError);
        return json('e');

END BB_F_READ_USERS;
 

/
