--------------------------------------------------------
--  DDL for Function BB_F_VAL_CREATE_USER_ROLES
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE FUNCTION "SPIDERS_DATA"."BB_F_VAL_CREATE_USER_ROLES" (in_jJson json, o_jResults out json, o_jMessages out json ) RETURN VARCHAR2 AS

  lc_jJson json;
  lc_data json_list;
  lc_rec_val json_value;
  lc_rec json;
  lc_employeeid_val json_value; --
  lc_employeeid varchar(256);
  lc_cnt number;

  lc_isMember_val json_value;
  lc_isMember number;

  lc_securitygroupid_val json_value;
  lc_securitygroupid number; -- varchar2(256);

  lc_jResults json;
  lc_jlDML json_list;

  lc_jMessages json;
  lc_jlMessages json_list;
  lc_bReturn varchar2(1);

  --error stuff
  lc_jError json;
  json_error EXCEPTION;

BEGIN

    lc_jResults := json('{}');
    lc_jlDML := json_list('[]');

    lc_jMessages := json('{}');
    lc_jlMessages := json_list('[]');

  --lc_jlMessages.append(json_list('[{error:''t-1''}]'));
  --POPLUTATE data ARRAY AND GET EMPLOYEEID OF FIRST ITEM
  if(in_jJson.exist('data')) then
    if(in_jJson.get('data').is_array) then
      lc_data := json_list(in_jJson.get('data'));
      lc_rec_val := lc_data.get(1); --return null on outofbounds
      if (lc_rec_val is not null) then
        lc_rec := json(lc_rec_val);
        --Check if EMPLOYEEID attribute exists
        if (lc_rec.exist('EMPLOYEEID')) then
          lc_employeeid_val := lc_rec.get('EMPLOYEEID');
          lc_employeeid := lc_employeeid_val.get_string;
          lc_employeeid := upper(lc_employeeid);
          --CHECK IF EMPLOYEEID ALREADY EXISTS
            select count(SECURITYUSERID) into lc_cnt from SSD_SECURITY_USERS where EMPLOYEEID = lc_employeeid;
 
            if (lc_cnt > 0) then
              --EMPLOYEEID ALREADY EXISTS - RETURN AN ERROR
              lc_jlMessages.append(json_list('[{error:''Error CUR-0005 There has been an error processing the request''}]'));
            else
              --LOOP ON THESE AND ADD RECORDS WHEN isMember = 1
              if (lc_employeeid is not null) then
                for i in 1..lc_data.count loop
                  lc_rec_val := lc_data.get(i);
                  if (lc_rec_val is not null) then
                    lc_rec := json(lc_rec_val);
                    if (lc_rec.exist('ISMEMBER')) then
                      lc_isMember_val := lc_rec.get('ISMEMBER');
                      lc_isMember := lc_isMember_val.get_number;
                      if(lc_isMember=1) then
                        if (lc_rec.exist('SECURITYGROUPID')) then
                          lc_securitygroupid_val := lc_rec.get('SECURITYGROUPID');
                          lc_securitygroupid := lc_securitygroupid_val.get_number; 
                          --lc_securitygroupid := lc_securitygroupid_val.get_string;

                          --this is a valid object create the dml
                          lc_jlDML.append('insert into SSD_SECURITY_USERS (EMPLOYEEID,SECURITYGROUPID) values (''' || lc_employeeid || ''',' || lc_securitygroupid || ')' );



                        end if;
                      else
                        null; --this is not an error
                        --lc_jlMessages.append(json_list('[{row:' || i || '},{error:''Error CUR-0007 There has been an error processing the request''}]'));
                      end if;
                    else
                        lc_jlMessages.append(json_list('[{row:' || i || '},{error:''Error CUR-0008 There has been an error processing the request''}]'));
                    end if;
                  else
                    lc_jlMessages.append(json_list('[{row:' || i || '},{error:''Error CUR-0009 There has been an error processing the request''}]'));
                  end if;
                end loop;
              else
                lc_jlMessages.append(json_list('[{error:''Error CUR-0006 There has been an error processing the request''}]'));
              end if;
            end if;
        else
            lc_jlMessages.append(json_list('[{error:''Error CUR-0001 There has been an error processing the request''}]'));
        end if;
      else
        lc_jlMessages.append(json_list('[{error:''Error CUR-0002 There has been an error processing the request''}]'));
      end if;
    else
        lc_jlMessages.append(json_list('[{error:''Error CUR-0003 There has been an error processing the request''}]'));
    end if;
  else
    lc_jlMessages.append(json_list('[{error:''Error CUR-0004 There has been an error processing the request''}]'));
  end if;



    lc_jResults.put('results',lc_jlDML);
    lc_jMessages.put('messages',lc_jlMessages);

    --if there are any messages this is not valid return false
    if (lc_jlMessages.count > 0) then
        lc_bReturn := 'n';
        lc_jMessages.put('status','error');
        raise json_error;
    else
        lc_bReturn := 'y';
        lc_jMessages.put('status','pending dml');
    end if;

    o_jMessages := lc_jMessages;
    o_jResults := lc_jResults;


    return lc_bReturn;

  exception
    when json_error then
         
        lc_jError := json();
        lc_jError.put('function','bb_f_val_create_user_roles');
        lc_jError.put('input',in_jJson.to_char);
        
        lc_jError.put('messages',lc_jlMessages);
        o_jMessages := lc_jError;
        o_jResults := lc_jError;
        BB_P_ERROR(lc_jError);
        
        return 'e';
        
    when others then
        lc_jError := json();
        lc_jlMessages := json_list();
        lc_jError.put('function','bb_f_val_create_user_roles');
        lc_jError.put('input',in_jJson.to_char);
        lc_jlMessages.append(json_list('[{error:''CUR.000X ' || SQLERRM || '''}]'));
        lc_jError.put('messages',lc_jlMessages);
        o_jMessages := lc_jError;
        o_jResults := lc_jError;
        BB_P_ERROR(lc_jError);
        return 'e';
END;


/*
DECLARE
  IN_JJSON SPIDERS_DATA.JSON;
  
  lc_vReturn varchar2(1);
  lc_jMessages SPIDERS_DATA.JSON;
  lc_jResults SPIDERS_DATA.JSON;
BEGIN
  -- Modify the code to initialize the variable
 delete from ssd_security_users where employeeid = 'larry@email.com';
 commit;

  --CUR.0005 is produced when a duplicate name is found
  --in_jJson := json('{"target":"userRoles","data":{}}'); --produces CUR.0003
  --in_jJson := json('{"target":"userRoles","data":[]}'); --produces CUR.0002
  --in_jJson := json('{"target":"userRoles","datas":[{"EMPLOYEEID":"larry@email.com","SECURITYGROUPID":"590000001","GROUP_NAME":"Administrators1","isMember":1}]}'); --produces CUR.0004
  --in_jJson := json('{"target":"userRoless","data":[{"EMPLOsYEEID":"larry@email.com","SECURITYGROUPID":"590000001","GROUP_NAME":"Administrators1","isMember":1}]}'); --produces CUR.001
  --in_jJson := json('{"target":"userRoles","data":[{"EMPLOYEEID":"larry@email.com","SECURITYGROUPID":"590000001","GROUP_NAME":"Administrators1","isMember":1}]}');
 --in_jJson := json('{"target":"userRoles","data":[
   --                                             {"EMPLOYEEID":"larry@email.com","XXXSECURITYGROUPID":"590000001","GROUP_NAME":"Administrators1","isMember":1},
     --                                         {"EMPLOYEEID":"larry@email.com","SECURITYGROUPID":"590000002","GROUP_NAME":"Administrators1","isMember":0},
       --                                     {"EMPLOYEEID":"larry@email.com","SECURITYGROUPID":"590000003","GROUP_NAME":"Administrators1","XXXisMember":1},
         --                                 {"EMPLOYEEID":"larry@email.com","SECURITYGROUPID":"590000004","GROUP_NAME":"Administrators1","isMember":1},
           --                             {"EMPLOYEEID":"larry@email.com","SECURITYGROUPID":"590000005","GROUP_NAME":"Administrators1","isMember":1}                                   
             --                       ]}');
 --in_jJson := json('{"target":"userRoles","data":[
   --                                               {"EMPLOYEEID":"larry@email.com","SECURITYGROUPID":"590000001","GROUP_NAME":"Administrators1","isMember":1},
     --                                             {"EMPLOYEEID":"larry@email.com","SECURITYGROUPID":"590000002","GROUP_NAME":"Administrators1","isMember":1},
       --                                           {"EMPLOYEEID":"larry@email.com","SECURITYGROUPID":"590000003","GROUP_NAME":"Administrators1","isMember":1},
         --                                         {"EMPLOYEEID":"larry@email.com","SECURITYGROUPID":"590000004","GROUP_NAME":"Administrators1","isMember":1},
           --                                       {"EMPLOYEEID":"larry@email.com","SECURITYGROUPID":"590000005","GROUP_NAME":"Administrators1","isMember":1}                                   
             --                                 ]}');
  lc_vReturn := bb_f_val_create_user_roles(
    IN_JJSON => IN_JJSON,
    o_jMessages => lc_jMessages,
    o_jResults => lc_jResults
  );

  lc_jMessages.print;
  lc_jResults.print;
  dbms_output.put_line(lc_vReturn);
  
  exception
    when json_error then
        --htp.p('raised json_error in update func');
        lc_jError := json();
        lc_jError.put('function','bb_f_val_create_user_roles');
        lc_jError.put('input',in_jJson.to_char);
        lc_jError.put('messages',lc_jlMessages);
        BB_P_ERROR(lc_jError);
        --htp.p('returning n');
        return 'e';
    when others then
        lc_jError := json();
        lc_jError.put('function','bb_f_val_create_user_roles');
        lc_jError.put('input',in_jJson.to_char);
        lc_jlMessages.append(json_list('[{error:''CUR.000X ' || SQLERRM || '''}]'));
        lc_jError.put('messages',lc_jlMessages);
        BB_P_ERROR(lc_jError);
        return 'e';

END bb_f_val_create_user_roles;
*/
 

/
