--------------------------------------------------------
--  DDL for Function GETVAL
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE FUNCTION "SPIDERS_DATA"."GETVAL" (in_jvValue json_value, in_nQuotes number default 0) return varchar2 IS 

lc_vReturn varchar2(32000);
BEGIN 

  
  CASE in_jvValue.get_type
     WHEN 
        'string' 
     THEN 
        if(in_nQuotes>0) then
            lc_vReturn := '''' || in_jvValue.get_string || '''';
        else
            lc_vReturn := in_jvValue.get_string;
        end if;
     WHEN 'number' THEN 
      lc_vReturn := in_jvValue.get_number;   
     ELSE lc_vReturn :='';
  END CASE;
  
  return lc_vReturn;
  
/* todo: exception handler */

END; -- calls proc2
 

/
