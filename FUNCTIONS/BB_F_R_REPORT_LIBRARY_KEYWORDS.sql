--------------------------------------------------------
--  DDL for Function BB_F_R_REPORT_LIBRARY_KEYWORDS
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE FUNCTION "SPIDERS_DATA"."BB_F_R_REPORT_LIBRARY_KEYWORDS" (in_jJson json) RETURN json AS

  lc_jJson json;
  lc_data json_list;
  lc_rec_val json_value;
  lc_rec json;
 
  lc_jValue json;
  lc_jvValue json_value;
  
  lc_nREPORTFILEID number;

  thesql varchar2(2000) := '';

  --error stuff
  lc_jError json;
  lc_jlMessages json_list;
  json_error EXCEPTION;

BEGIN
/*
stub data
{"module":"reportLibrary","target":"reportFileKeywords","data":[{"REPORTFILEID":11042}]}
*/


if(in_jJson.exist('data')) then
    if(in_jJson.get('data').is_array) then
        lc_data := json_list(in_jJson.get('data'));        
        lc_rec_val := lc_data.get(1); --return null on outofbounds

        if (lc_rec_val is not null) then
        
            lc_jValue := json(lc_rec_val);
            
            if (lc_jValue.exist('REPORTFILEID')) then
                lc_jvValue := lc_jValue.get('REPORTFILEID');
                lc_nREPORTFILEID := getVal(lc_jvValue);
            end if;
            
        end if;
    end if;
end if;

thesql := '

SELECT 
  * 
from 
  spd_t_report_keywords 
where 
  reportfileid = ' || lc_nREPORTFILEID;

  lc_jJson := json_dyn.executeObject(thesql);

  return lc_jJson;

exception
    when json_error then
        --htp.p('raised json_error in update func');
        lc_jlMessages := json_list();
        lc_jError := json();
        lc_jError.put('function','BB_F_R_REPORT_LIBRARY_KEYWORDS');
        lc_jError.put('input',in_jJson.to_char);
        lc_jError.put('messages',lc_jlMessages);
        BB_P_ERROR(lc_jError);
        --htp.p('returning n');
        return json('e');
    when others then
        lc_jError := json();
        lc_jError.put('function','BB_F_R_REPORT_LIBRARY_KEYWORDS');
        lc_jError.put('input',in_jJson.to_char);
        lc_jlMessages.append(json_list('[{error:''RDRLK.000X ' || SQLERRM || '''}]'));
        lc_jError.put('messages',lc_jlMessages);
        BB_P_ERROR(lc_jError);
        return json('e');


END BB_F_R_REPORT_LIBRARY_KEYWORDS;
 

/
