--------------------------------------------------------
--  DDL for Function BB_F_READ_GF_AUTHOR
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE FUNCTION "SPIDERS_DATA"."BB_F_READ_GF_AUTHOR" (in_jJson json) return json as
  lc_jJson json;
  lc_data json_list;
  lc_rec_val json_value;
  lc_rec json;
  lc_adoid_val json_value; --
  lc_nAdoid number;

  lc_ret_str varchar(512);
  lc_ret json;

  thesql varchar2(2000) := '';

  --error stuff
  lc_jError json;
  lc_jlMessages json_list;
  json_error EXCEPTION;
  
  
lc_jlData json_list;
lc_jvRec json_value;
lc_jlValue json_list;

lc_jvRec2 json_value;
lc_jValue json;

lc_jvValue json_value;
lc_vValue varchar2(2000) := '';
in_vGroup varchar2(2000) := 'PRODUCTLINE';
lc_vStatus varchar2(2000);


lc_jlGroupNames json_list;
lc_vGroupName varchar2(2000);
lc_jlDataValue json_list;
lc_vInClause varchar2(2000);

  lc_vWhere varchar2(2000);
  lc_vSearch varchar2(2000);
  lc_nStart number := 1;
  lc_nEnd number := 10;
  
  lc_nOrderByCol number;
  lc_vOrderByCol varchar2(2000);
  lc_vOrderBySort varchar2(2000);
  lc_vOrderBy varchar2(2000) := ' time asc ';
  
  lc_nGetValuesFromAodata number;


begin
/*
stubb data
'{"groupFilterData" : [
{"group_name" : "PRODUCTLINE" , "data" : [ 100085, 100086]},
{"group_name" : "YEAR" , "data" : [ 2014,2013,2012 ]},
{"group_name" : "STATE" , "data" : [ "HI","VA"]}
]}'
*/


lc_vWhere := parseGroupFilterData(in_jJson);

lc_vWhere := replace(lc_vWhere, 'YEAR', 'to_char(eststartdate,''YYYY'')');
lc_vWhere := replace(lc_vWhere, 'PRODUCTLINE', 'PRODUCTLINE_LISTID');
lc_vWhere := replace(lc_vWhere, 'REPORT_AUTHOR', 'REPORTAUTHOR');

lc_vWhere := replace(lc_vWhere, 'year', 'to_char(eststartdate,''YYYY'')');
lc_vWhere := replace(lc_vWhere, 'productline', 'PRODUCTLINE_LISTID');
lc_vWhere := replace(lc_vWhere, 'report_author', 'REPORTAUTHOR');

lc_nGetValuesFromAodata := parseAoData(in_jJson, lc_vSearch, lc_nStart, lc_nEnd, lc_vOrderBy);

if length(lc_vSearch) is null then
  lc_vWhere := lc_vWhere;
else
  if length(lc_vWhere) is null then
    lc_vWhere := ' Where upper(REPORTID || 
    REPORTTITLE     || 
    REPORTAUTHOR ||     
    ESTSTARTDATE ||     
    CONTRACTOR ||     
    CONTRACTNUMBER ||     
    CREATEDDATE ||     
    REPORTNUMBER)
    like ''%' || upper(lc_vSearch) || '%''';
  else
    lc_vWhere := lc_vWhere || ' AND  
    upper(REPORTID || 
    REPORTTITLE     || 
    REPORTAUTHOR ||     
    ESTSTARTDATE ||     
    CONTRACTOR ||     
    CONTRACTNUMBER ||     
    CREATEDDATE ||     
    REPORTNUMBER)
    like ''%' || upper(lc_vSearch) || '%''';
  end if;
end if;


thesql := '

select * from (

select
  ''report_author'' group_name,
  nvl(trim(reportauthor),''No Author'') display_value,
  nvl(reportauthor,''No Author'') return_value,
  count(reportid) record_count
from 
  spd_t_reports

' || lc_vWhere || '
group by reportauthor
)
order by record_count desc';



  lc_jJson := json_dyn.executeObject(thesql);

  return lc_jJson;
  
exception
    when json_error then
        --htp.p('raised json_error in update func');
        lc_jlMessages := json_list();
        lc_jError := json();
        lc_jError.put('function','BB_F_READ_GF_AUTHOR');
        lc_jError.put('input',in_jJson.to_char);
        --lc_jError.put('messages',lc_jlMessages);
        BB_P_ERROR(lc_jError);
        --htp.p('returning n');
        return json('e');
    when others then
        lc_jlMessages := json_list();
        lc_jError := json();
        lc_jError.put('function','BB_F_READ_GF_AUTHOR');
        lc_jError.put('input',in_jJson.to_char);
        lc_jlMessages.append(json_list('[{error:''RSBGFA.000X ' || replace(SQLERRM,chr(10),'') || '''}]'));
        lc_jError.put('messages',lc_jlMessages);
        BB_P_ERROR(lc_jError);
        return json('e');
end BB_F_READ_GF_AUTHOR;
 

/
