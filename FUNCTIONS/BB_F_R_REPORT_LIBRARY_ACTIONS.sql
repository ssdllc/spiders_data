--------------------------------------------------------
--  DDL for Function BB_F_R_REPORT_LIBRARY_ACTIONS
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE FUNCTION "SPIDERS_DATA"."BB_F_R_REPORT_LIBRARY_ACTIONS" (in_jJson json, thesql out varchar2) RETURN json AS

  lc_jJson json;
  lc_data json_list;
  lc_rec_val json_value;
  lc_rec json;
  
  lc_YEAR_val json_value; --
  lc_nYEAR number;


  lc_PRODUCTLINE_val json_value; --
  lc_nPRODUCTLINE number;
  
  
  lc_STATE_val json_value; --
  lc_vSTATE varchar2(200);
  
  
  lc_ret_str varchar(512);
  lc_ret json;

  --thesql varchar2(2000) := '';

  --error stuff
  lc_jError json;
  lc_jlMessages json_list;
  json_error EXCEPTION;
  
  lc_vSearch varchar2(2000);
  lc_vWhere varchar2(2000);
  lc_nStart number := 1;
  lc_nEnd number := 10;
  
  lc_nOrderByCol number;
  lc_vOrderByCol varchar2(2000);
  lc_vOrderBySort varchar2(2000);
  lc_vOrderBy varchar2(2000) := ' reportid asc ';
  
  lc_nGetValuesFromAodata number;


BEGIN
/*
stub data
{"module":"reportLibrary","target":"reportFiles","data":[{"REPORTID":11042}]}
*/


if(in_jJson.exist('data')) then
    if(in_jJson.get('data').is_array) then
        lc_data := json_list(in_jJson.get('data'));
        
        
        
        
        lc_rec_val := lc_data.get(1); --return null on outofbounds
        if (lc_rec_val is not null) then
            lc_rec := json(lc_rec_val);
            
            if (lc_rec.exist('YEAR')) then
                lc_YEAR_val := lc_rec.get('YEAR');
                lc_nYEAR := lc_YEAR_val.get_number;
            else
                lc_jlMessages.append(json_list('[{error:''Error RDRLF-0001 There has been an error processing the request''}]'));
            end if;
            

            if (lc_rec.exist('PRODUCTLINE')) then
                lc_PRODUCTLINE_val := lc_rec.get('PRODUCTLINE');
                lc_nPRODUCTLINE := lc_PRODUCTLINE_val.get_number;
            else
                lc_jlMessages.append(json_list('[{error:''Error RDRLF-0001 There has been an error processing the request''}]'));
            end if;  
            
            
            if (lc_rec.exist('STATE')) then
                lc_STATE_val := lc_rec.get('STATE');
                lc_vSTATE := lc_STATE_val.get_string;
            else
                lc_jlMessages.append(json_list('[{error:''Error RDRLF-0001 There has been an error processing the request''}]'));
            end if;
            
            
        else
            lc_jlMessages.append(json_list('[{error:''Error RDRLF-0002 There has been an error processing the request''}]'));
        end if;
    else
        lc_jlMessages.append(json_list('[{error:''Error RDRLF-0003 There has been an error processing the request''}]'));
    end if;
else
    lc_jlMessages.append(json_list('[{error:''Error RDRLF-0004 There has been an error processing the request''}]'));
end if;


lc_nGetValuesFromAodata := parseAoData(in_jJson, lc_vSearch, lc_nStart, lc_nEnd, lc_vOrderBy);

if(lc_nGetValuesFromAodata = 0) then
  lc_vSearch :='';
  lc_nStart :=1;
  lc_nEnd :=10;
  lc_vOrderBy :=' act.spd_activity_name asc ';
end if;


lc_vSearch := upper(lc_vSearch);

lc_vOrderBy := replace(lc_vOrderBy,'ACTIVITY_NAME','act.spd_activity_name');
lc_vOrderBy := replace(lc_vOrderBy,'ACTIVITY_UIC', 'act.spd_uic');


thesql := '

select * from (
select
  apid,
  act.spd_activity_name ACTIVITY_NAME,
  act.spd_uic ACTIVITY_UIC,
  
  
  decode(
    act.STATE, ''NON-US'',
    (select distinct country from spd_mv_inf_cur_activities ca where ca.uic = act.spd_uic),
    act.STATE
  ) STATE,
  
  
  
  planned_qtr,
  planned_year,
  planned_fund_source_listid,
  productline_listid,
  (select displayvalue from spd_t_lists where listid = ap.productline_listid) productline_display,
  row_number() over (order by ' || lc_vOrderBy || ') rn,
  count(*) over (partition by (select 1 from dual)) countOfRecords
  
from
  spd_t_action_planner ap,
  spd_t_activities act
  
where
  ap.planned_activitiesid = act.activitiesid and

  decode(
  act.STATE, ''NON-US'',
  (select distinct country from spd_mv_inf_cur_activities ca where ca.uic = act.spd_uic),
  act.STATE
  ) like ''%' || lc_vSTATE || '%''
  
  and
  
  planned_year like ''%' || lc_nYear || '%''
  
  and
  
  productline_listid like ''%' || lc_nProductline || '%''
  
  and
  
  upper(act.spd_activity_name || act.spd_uic) like ''%' || lc_vSearch || '%''
  
order by
    ' || lc_vOrderBy || '
)
where rn between ' || lc_nStart || ' and ' || lc_nEnd;

--htp.p('thesql = '||thesql);
  lc_jJson := json_dyn.executeObject(thesql);

  return lc_jJson;

exception
    when json_error then
        --htp.p('raised json_error in update func');
        lc_jlMessages := json_list();
        lc_jError := json();
        lc_jError.put('function','BB_F_R_REPORT_LIBRARY_ACTIONS');
        lc_jError.put('input',in_jJson.to_char);
        lc_jError.put('messages',lc_jlMessages);
        BB_P_ERROR(lc_jError);
        --htp.p('returning n');
        return json('e');
    when others then
        lc_jError := json();
        lc_jError.put('function','BB_F_R_REPORT_LIBRARY_ACTIONS');
        lc_jError.put('input',in_jJson.to_char);
        lc_jlMessages.append(json_list('[{error:''RDRLA.000X ' || SQLERRM || '''}]'));
        lc_jError.put('messages',lc_jlMessages);
        BB_P_ERROR(lc_jError);
        return json('e');


END BB_F_R_REPORT_LIBRARY_ACTIONS;

/
