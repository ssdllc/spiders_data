--------------------------------------------------------
--  DDL for Function BB_F_CU_REPORT_LIBRARY_REC
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE FUNCTION "SPIDERS_DATA"."BB_F_CU_REPORT_LIBRARY_REC" (in_jJson json, o_jMessages out json) return varchar2 as 

    lc_jJson json;
    lc_data json_list;
    lc_rec_val json_value;
    lc_rec json;
    lc_employeeid_val json_value; --
    lc_employeeid varchar(256);
    lc_cnt number;
    lc_nAdoid number :=0;
    lc_adoid_val json_value;
    lc_nCountXMLFac number :=0;
    lc_nCountSpidersFac number :=0;
    
    lc_jValue json;
    lc_jvValue json_value;
    lc_nREPORTID number :=0;
    lc_vREPORTTITLE varchar(256) :='';
    lc_vREPORTAUTHOR varchar(256) :='';
    lc_vESTSTARTDATE_temp varchar(222) :='';
    lc_nESTSTARTDATE number := to_char(sysdate,'YYYY');
    lc_vCONTRACTOR varchar(256) :='';
    lc_vCONTRACTNUMBER varchar(256) :='';
    lc_vREPORTNUMBER varchar(256) :='';
    lc_vACTIONSID varchar(256) :='';
    lc_vCITY varchar(256) :='';
    lc_vSTATE varchar(256) :='';
    lc_vLOCATION_CUSTOM varchar(256) := '';
    lc_vCOUNTRY varchar(256) :='';
    lc_nPRODUCTLINE number;
    lc_vGOVPROJENGINEER varchar2(255);
    
    lc_nSequence number;
    lc_nContinue number :=0;
    
    
    lc_jlDML json_list;
    lc_jResults json;
    lc_jMessages json;
    lc_vDMLResult varchar2(255);
    lc_ret_str varchar(512);
    lc_ret json;

    thesql varchar2(2000) := '';

    lc_vWhere varchar2(2000);

    --error stuff
    lc_jError json;
    lc_jlMessages json_list;
    json_error EXCEPTION;


BEGIN
/*
stub data
{"module":"xmlImportManager","target":"itDoesntMatterNotBeingCalledFromTheBrowser","data":["ADOID":12345]}
*/

lc_jResults := json();
lc_jlDML := json_list();

if(in_jJson.exist('data')) then
    if(in_jJson.get('data').is_array) then
        lc_data := json_list(in_jJson.get('data'));        
        lc_rec_val := lc_data.get(1); --return null on outofbounds

        if (lc_rec_val is not null) then
        
            lc_jValue := json(lc_rec_val);
            
            if (lc_jValue.exist('REPORTID')) then
                lc_jvValue := lc_jValue.get('REPORTID');
                lc_nREPORTID := getVal(lc_jvValue);
            end if;

            if (lc_jValue.exist('REPORTTITLE')) then
                lc_jvValue := lc_jValue.get('REPORTTITLE');
                lc_vREPORTTITLE := getVal(lc_jvValue);
            end if;
            if (lc_jValue.exist('REPORTAUTHOR')) then
                lc_jvValue := lc_jValue.get('REPORTAUTHOR');
                lc_vREPORTAUTHOR := getVal(lc_jvValue);
            end if;
            
            if (lc_jValue.exist('ESTSTARTDATE')) then
                lc_jvValue := lc_jValue.get('ESTSTARTDATE');
                lc_nESTSTARTDATE := getVal(lc_jvValue);
            end if;
            
            if (lc_jValue.exist('CONTRACTOR')) then
                lc_jvValue := lc_jValue.get('CONTRACTOR');
                lc_vCONTRACTOR := getVal(lc_jvValue);
            end if;
            
            if (lc_jValue.exist('CONTRACTNUMBER')) then
                lc_jvValue := lc_jValue.get('CONTRACTNUMBER');
                lc_vCONTRACTNUMBER := getVal(lc_jvValue);
            end if;
            
            if (lc_jValue.exist('REPORTNUMBER')) then
                lc_jvValue := lc_jValue.get('REPORTNUMBER');
                lc_vREPORTNUMBER := getVal(lc_jvValue);
            end if;
            
            if (lc_jValue.exist('ACTIONSID')) then
                lc_jvValue := lc_jValue.get('ACTIONSID');
                lc_vACTIONSID := getVal(lc_jvValue);
                
                if lc_vACTIONSID = '0' then
                  lc_vACTIONSID := 'null';
                end if;
            end if;
            
            if (lc_jValue.exist('CITY')) then
                lc_jvValue := lc_jValue.get('CITY');
                lc_vCITY := getVal(lc_jvValue);
            end if;
            
            if (lc_jValue.exist('STATE')) then
                lc_jvValue := lc_jValue.get('STATE');
                lc_vSTATE := getVal(lc_jvValue);
            end if;
            
            if (lc_jValue.exist('LOCATION_CUSTOM')) then
                lc_jvValue := lc_jValue.get('LOCATION_CUSTOM');
                lc_vLOCATION_CUSTOM := getVal(lc_jvValue);
            end if;
            
            if (lc_jValue.exist('COUNTRY')) then
                lc_jvValue := lc_jValue.get('COUNTRY');
                lc_vCOUNTRY := getVal(lc_jvValue);
            end if;
            
            if (lc_jValue.exist('PRODUCTLINE')) then
                lc_jvValue := lc_jValue.get('PRODUCTLINE');
                lc_nPRODUCTLINE := getVal(lc_jvValue);
            end if;
            
            if (lc_jValue.exist('GOVPROJENGINEER')) then
                lc_jvValue := lc_jValue.get('GOVPROJENGINEER');
                lc_vGOVPROJENGINEER := getVal(lc_jvValue);
            end if;
            
            lc_nContinue := 1;
            
        end if;
    end if;
end if;


if lc_nContinue = 1 then

  if lc_nREPORTID > 0 then
      --Update the table
      thesql := '
        update 
          spd_t_reports 
        set 
          REPORTTITLE = ''' || lc_vREPORTTITLE || ''',
          REPORTAUTHOR = ''' || lc_vREPORTAUTHOR || ''',
          ESTSTARTDATE = ''' || to_date('01-OCT-' || lc_nESTSTARTDATE || '','DD-MON-YYYY') || ''',
          CONTRACTOR = ''' || lc_vCONTRACTOR || ''',
          CONTRACTNUMBER = ''' || lc_vCONTRACTNUMBER || ''',
          REPORTNUMBER = ''' || lc_vREPORTNUMBER || ''',
          ACTIONSID = ' || lc_vACTIONSID || ',
          CITY = ''' || lc_vCITY || ''',
          STATE = ''' || lc_vSTATE || ''',
          LOCATION_CUSTOM = ''' || lc_vLOCATION_CUSTOM || ''',
          COUNTRY = ''' || lc_vCOUNTRY || ''',
          PRODUCTLINE_LISTID = ''' || lc_nPRODUCTLINE || ''',
          GOVPROJENGINEER = ''' || lc_vGOVPROJENGINEER || '''
        where
          reportid = ' || lc_nREPORTID;
  
        lc_jlDML.append(thesql);
        lc_jResults.put('results',lc_jlDML);
        lc_vDMLResult := BB_F_DML(lc_jResults, lc_jMessages);
        o_jMessages := lc_jMessages;
  else
  
      select spd_s_reports.nextval into lc_nSequence from dual;
      --Insert record into table
      thesql := '
        insert into 
          spd_t_reports (
            reportid,
            REPORTTITLE,
            REPORTAUTHOR,
            ESTSTARTDATE,
            CONTRACTOR,
            CONTRACTNUMBER,
            REPORTNUMBER,
            ACTIONSID,
            CITY,
            STATE,
            LOCATION_CUSTOM,
            COUNTRY,
            PRODUCTLINE_LISTID,
            GOVPROJENGINEER
          )
        
          values (
            ''' || lc_nSequence || ''',
            ''' || lc_vREPORTTITLE || ''',
            ''' || lc_vREPORTAUTHOR || ''',
            to_date(''01-OCT-' || lc_nESTSTARTDATE || ''',''DD-MON-YYYY''),
            ''' || lc_vCONTRACTOR || ''',
            ''' || lc_vCONTRACTNUMBER || ''',
            ''' || lc_vREPORTNUMBER || ''',
            ' || lc_vACTIONSID || ',
            ''' || lc_vCITY || ''',
            ''' || lc_vSTATE || ''',
            ''' || lc_vLOCATION_CUSTOM || ''',
            ''' || lc_vCOUNTRY || ''',
            ''' || lc_nPRODUCTLINE || ''',
            ''' || lc_vGOVPROJENGINEER || '''
          )
      ';
  


      lc_jlDML.append(thesql);
      lc_jResults.put('results',lc_jlDML);
      lc_vDMLResult := BB_F_DML(lc_jResults, lc_jMessages);
      lc_jMessages.put('id',lc_nSequence);
      o_jMessages := lc_jMessages;
  end if;
  
  
  return lc_vDMLResult;
else 
  raise json_error;
end if;


exception
when json_error then
    --htp.p('raised json_error in update func');
    lc_jError := json();
    lc_jError.put('function','BB_P_CREATE_REPORT_LIBRARY_REC');
    lc_jError.put('input',in_jJson.to_char);
    lc_jError.put('messages',lc_jlMessages);
    BB_P_ERROR(lc_jError);
    return 'e';
    --htp.p('returning n');
when others then
    lc_jError := json();
    lc_jlMessages := json_list();
    lc_jError.put('function','BB_P_CREATE_REPORT_LIBRARY_REC');
    lc_jError.put('input','');
    lc_jlMessages.append(json_list('[{error:''CRLR.000X ' || SQLERRM || '''}]'));
    lc_jError.put('messages',lc_jlMessages);
    BB_P_ERROR(lc_jError);
    return 'e';


END BB_F_CU_REPORT_LIBRARY_REC;
 

/
