--------------------------------------------------------
--  DDL for Function BB_F_XML_IMPORT_RESET
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE FUNCTION "SPIDERS_DATA"."BB_F_XML_IMPORT_RESET" (in_jJson json) RETURN json AS

  lc_jJson json;
  lc_data json_list;
  lc_rec_val json_value;
  lc_rec json;
  lc_adoid_val json_value; --
  lc_nAdoid number :=0;
  lc_cDML clob;

  lc_ret_str varchar(512);
  lc_ret json;

  thesql varchar2(2000) := '';

  --error stuff
  lc_jError json;
  lc_jlMessages json_list;
  json_error EXCEPTION;

BEGIN
/*
stub data
{"module":"xmlImportManager","target":"spidersDeliveryOrderFacs","data":[{"ADOID":330001385}]}
*/


lc_jlMessages := json_list();

if(in_jJson.exist('data')) then
    if(in_jJson.get('data').is_array) then
        lc_data := json_list(in_jJson.get('data'));
        lc_rec_val := lc_data.get(1); --return null on outofbounds
        if (lc_rec_val is not null) then
            lc_rec := json(lc_rec_val);
            if (lc_rec.exist('ADOID')) then
                lc_adoid_val := lc_rec.get('ADOID');
                lc_nAdoid := lc_adoid_val.get_number;
            else
                lc_jlMessages.append(json_list('[{error:''Error RDOXIE-0001 There has been an error processing the request''}]'));
            end if;
        else
            lc_jlMessages.append(json_list('[{error:''Error RDOXIE-0002 There has been an error processing the request''}]'));
        end if;
    else
        lc_jlMessages.append(json_list('[{error:''Error RDOXIE-0003 There has been an error processing the request''}]'));
    end if;
else
    lc_jlMessages.append(json_list('[{error:''Error RDOXIE-0004 There has been an error processing the request''}]'));
end if;



lc_cDML := 'delete from spd_t_thumbnails where thumbnailid in
(
  select thumbnailid from spd_t_do_fac_mooring_fittings where aiid in (select aiid from spd_t_do_fac_asset_inventory where dofid in (select dofid from spd_t_do_facilities where adoid = ' || lc_nADOID || ')) and thumbnailid is not null
  union
  select thumbnailid from spd_t_do_supporting_files where adoid = ' || lc_nADOID || ' and thumbnailid is not null
  union
  select thumbnailid from spd_t_ai_supporting_files where aiid in (select aiid from spd_t_do_fac_asset_inventory where dofid in (select dofid from spd_t_do_facilities where adoid = ' || lc_nADOID || ')) and thumbnailid is not null
  union
  select thumbnailid from spd_t_do_fac_supporting_files where dofid in (select dofid from spd_t_do_facilities where adoid = ' || lc_nADOID || ') and thumbnailid is not null
)';
execute immediate lc_cDML;

lc_cDML := 'delete from spd_t_do_supporting_files where adoid = ' || lc_nADOID ;
execute immediate lc_cDML;

lc_cDML := 'delete from spd_t_do_fac_mooring_fittings where aiid in (select aiid from spd_t_do_fac_asset_inventory where dofid in (select dofid from spd_t_do_facilities where adoid = ' || lc_nADOID || '))';
execute immediate lc_cDML;

lc_cDML := 'delete from spd_t_ai_defect_meta_data where aidefectid in (select aidefectid from spd_t_ai_defects where aiid in (select aiid from spd_t_do_fac_asset_inventory where dofid  in (select dofid from spd_t_do_facilities where adoid = ' || lc_nADOID || ')))';
execute immediate lc_cDML;

lc_cDML := 'delete from spd_t_ai_defects where aiid in (select aiid from spd_t_do_fac_asset_inventory where dofid in (select dofid from spd_t_do_facilities where adoid = ' || lc_nADOID || '))';
execute immediate lc_cDML;

lc_cDML := 'delete from spd_t_ai_find_rec where aiid in (select aiid from spd_t_do_fac_asset_inventory where dofid in (select dofid from spd_t_do_facilities where adoid = ' || lc_nADOID || '))';
execute immediate lc_cDML;

lc_cDML := 'delete from spd_t_ai_meta_data where aiid in (select aiid from spd_t_do_fac_asset_inventory where dofid in (select dofid from spd_t_do_facilities where adoid = ' || lc_nADOID || '))';
execute immediate lc_cDML;

lc_cDML := 'delete from spd_t_ai_supporting_files where aiid in (select aiid from spd_t_do_fac_asset_inventory where dofid in (select dofid from spd_t_do_facilities where adoid = ' || lc_nADOID || '))';
execute immediate lc_cDML;

lc_cDML := 'delete from spd_t_do_fac_asset_inventory where aiid in (select aiid from spd_t_do_fac_asset_inventory where dofid in (select dofid from spd_t_do_facilities where adoid = ' || lc_nADOID || '))';
execute immediate lc_cDML;

lc_cDML := 'delete from spd_t_do_facility_meta_data where dofid in (select dofid from spd_t_do_facilities where adoid = ' || lc_nADOID || ')';
execute immediate lc_cDML;

lc_cDML := 'delete from spd_t_do_fac_supporting_files where dofid in (select dofid from spd_t_do_facilities where adoid = ' || lc_nADOID || ')';
execute immediate lc_cDML;

lc_cDML := 'delete from spd_t_import_log where adoid = ' || lc_nADOID;
execute immediate lc_cDML;

lc_cDML := 'delete from spd_t_xml_files where adoid = ' || lc_nADOID;
execute immediate lc_cDML;

lc_cDML := 'delete from spd_t_xml_facilities_match where adoid = ' || lc_nADOID;
execute immediate lc_cDML;

lc_cDML := 'delete from debug where ref_id = ' || lc_nADOID;
execute immediate lc_cDML;

lc_cDML := 'delete from spd_t_xml_actions where adoid = ' || lc_nADOID;
execute immediate lc_cDML;


commit;

DBMS_MVIEW.REFRESH('SPD_MV_DATA_ANALYSIS', 'C', '', TRUE, FALSE, 0,0,0, FALSE, FALSE);



  lc_jJson := json('{"messages":[],"status":"success"}');

  return lc_jJson;

exception
    when json_error then
        --htp.p('raised json_error in update func');
        lc_jError := json();
        lc_jError.put('function','BB_F_XML_IMPORT_RESET');
        lc_jError.put('input',in_jJson.to_char);
        lc_jError.put('messages',lc_jlMessages);
        BB_P_ERROR(lc_jError);
        --htp.p('returning n');
        return json('{"error":"e"}');
    when others then
        lc_jError := json();
        lc_jError.put('function','BB_F_XML_IMPORT_RESET');
        lc_jError.put('input',in_jJson.to_char);
        lc_jlMessages.append(json_list('[{error:''RDOXIE.000X ' || SQLERRM || '''}]'));
        lc_jError.put('messages',lc_jlMessages);
        BB_P_ERROR(lc_jError);
        return json('{"error":"e"}');


END BB_F_XML_IMPORT_RESET;
 

/
