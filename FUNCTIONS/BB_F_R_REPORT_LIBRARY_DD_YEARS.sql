--------------------------------------------------------
--  DDL for Function BB_F_R_REPORT_LIBRARY_DD_YEARS
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE FUNCTION "SPIDERS_DATA"."BB_F_R_REPORT_LIBRARY_DD_YEARS" (in_jJson json) RETURN json AS

  lc_jJson json;

  thesql varchar2(2000) := '';

  --error stuff
  lc_jError json;
  lc_jlMessages json_list;
  json_error EXCEPTION;

BEGIN
/*
stub data
{"module":"reportLibrary","target":"reportFiles","data":[{"REPORTID":11042}]}
*/


thesql := '
select 
  ''YEARS'' listname,
  YEARS displayvalue,
  YEARS returnvalue,
  YEARS visualorder
from
(
  SELECT EXTRACT(YEAR FROM SYSDATE) + 3 - LEVEL AS YEARS
  FROM DUAL
  CONNECT BY LEVEL <= 30
  ORDER BY YEARS
)

';

  lc_jJson := json_dyn.executeObject(thesql);

  return lc_jJson;

exception
    when json_error then
        --htp.p('raised json_error in update func');
        lc_jlMessages := json_list();
        lc_jError := json();
        lc_jError.put('function','BB_F_R_REPORT_LIBRARY_DD_YEARS');
        lc_jError.put('input',in_jJson.to_char);
        lc_jError.put('messages',lc_jlMessages);
        BB_P_ERROR(lc_jError);
        --htp.p('returning n');
        return json('e');
    when others then
        lc_jError := json();
        lc_jError.put('function','BB_F_R_REPORT_LIBRARY_DD_YEARS');
        lc_jError.put('input',in_jJson.to_char);
        lc_jlMessages.append(json_list('[{error:''RDRLA.000X ' || SQLERRM || '''}]'));
        lc_jError.put('messages',lc_jlMessages);
        BB_P_ERROR(lc_jError);
        return json('e');


END BB_F_R_REPORT_LIBRARY_DD_YEARS;
 

/
