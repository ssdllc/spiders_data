create or replace FUNCTION                "BB_F_ADD_DO_FACILITY" (in_jJson json, o_jResults out json, o_jMessages out json ) RETURN VARCHAR2 AS

--JSON stuff
lc_jJson json;
lc_data json_list;
lc_rec_val json_value;
lc_rec json;
lc_jValue json;
lc_jvValue json_value;
 

--procedure specific stuff
lc_nSequence number;
lc_nContinue number :=1;
lc_nCountOfRecords number :=0;
lc_nADOID number;
lc_vNFAID varchar2(2000);
lc_nPRODUCTLINE_LISTID number;
lc_vFACILITY_NAME varchar2(2000);
lc_vFACILITY_NO varchar2(2000);
lc_nDOFID number :=0;
lc_nACTIVITIESID number :=0;
lc_nDOWorkTypeListid number :=0;
lc_nFacid number :=0;
lc_nNewFacility number :=1;


--DML stuff
lc_jlDML json_list;
lc_jResults json;
lc_jResultsSubProgram json;
lc_jMessages json;
lc_jMessagesSubProgram json;
lc_vDMLResult varchar2(255);
lc_vResult varchar2(200);
lc_vResultSubProgram varchar2(200);
lc_ret_str varchar(512);
lc_ret json;
thesql varchar2(2000) := '';
lc_vWhere varchar2(2000);
lc_vReturn varchar2(200);

--error stuff
lc_vErrorText varchar2(2000);
lc_jError json;
lc_jlMessages json_list;
json_error EXCEPTION;


procedure p (in_vInput varchar2, in_vMessage varchar2 default '') as

begin

insert into debug (functionname, input, message) values ('BB_F_ADD_DO_FACILITY', in_vInput, in_vMessage);

end;

begin

lc_jResults := json();
lc_jlDML := json_list();
lc_jMessages := json();
lc_jlMessages := json_list();
o_jResults := json();
o_jMessages := json();


if(in_jJson.exist('NFAID')) then
  lc_jvValue := in_jJson.get('NFAID');
  lc_vNFAID:= getVal(lc_jvValue);
  p('lc_vNFAID',lc_vNFAID);

  
  if(in_jJson.exist('ADOID')) then
    lc_jvValue := in_jJson.get('ADOID');
    lc_nADOID := getVal(lc_jvValue);
    p('lc_nADOID',lc_nADOID);
  
    if(in_jJson.exist('PRODUCTLINE_LISTID')) then
      lc_jvValue := in_jJson.get('PRODUCTLINE_LISTID');
      lc_nPRODUCTLINE_LISTID := getVal(lc_jvValue);
      p('lc_nPRODUCTLINE_LISTID',lc_nPRODUCTLINE_LISTID);
    else
      lc_vErrorText := 'PRODUCTLINE_LISTID doesnt exist';
      lc_nContinue :=0;
      lc_jlMessages.append('error: ''Error XMLADOFAC-0001 ' || lc_vErrorText);
      p('lc_nPRODUCTLINE_LISTID error',lc_vErrorText);
    end if;
  else
    lc_vErrorText := 'ADOID doesnt exist';
    lc_nContinue :=0;
    lc_jlMessages.append('error: ''Error XMLADOFAC-0001 ' || lc_vErrorText);
    p('lc_nADOID error',lc_vErrorText);
  end if;
else
  lc_vErrorText := 'NFAID doesnt exist';
  lc_nContinue :=0;
  lc_jlMessages.append('error: ''Error XMLADOFAC-0001 ' || lc_vErrorText);
  p('lc_vNFAID error',lc_vErrorText);
end if;


p('BB_F_ADD_DO_FACILITY IN_JSON',json_printer.pretty_print(in_jJson));
p('lc_nContinue',lc_nContinue);


if lc_nContinue = 1 then 


  for rec in (select worktype_listid from spd_t_actn_dos where adoid = lc_nADOID) loop
    lc_nDOWorkTypeListid := rec.worktype_listid;
  end loop;
  
  p('lc_nDOWorkTypeListid',lc_nDOWorkTypeListid);

  lc_nDOWorkTypeListid := 1; --!!THIS IS NOT NEEDED ANYMORE WITH NEW DELIVERY ORDERS. KEEPING FULL IF IN HERE FOR TIME SAKE.
  if lc_nDOWorkTypeListid > 0 then
    --all is well
    --get a count of facilities from spd_t_facilities using this NFA, if there is more than one we have to throw an error
    /*
    select f.*, p.productline_display from 
    spd_t_facilities f,  
    spd_t_productlines p
    where
    f.product_line_listid = p.productlineid and
    p.productline_display = 'Waterfront' and
    f.infads_facility_id ='NFA100000773610'
*/
    
    --select count(*) into lc_nCountOfRecords from spd_t_facilities where infads_facility_id = lc_vNFAID;
    
    select count(f.FACID) into lc_nCountOfRecords from spd_t_facilities f, spd_t_productlines p where f.product_line_listid=p.productlineid and f.infads_facility_id = lc_vNFAID;
    
    --select f.* from spd_t_facilities f, spd_t_productlines p where f.product_line_listid=p.productlineid and f.infads_facility_id ='NFA100000773610'
    
    p('select count(f.FACID) into lc_nCountOfRecords from spd_t_facilities f, spd_t_productlines p where f.product_line_listid=p.productlineid and f.infads_facility_id = ' || lc_vNFAID,lc_nCountOfRecords);
    if lc_nCountOfRecords = 0 then
      --this is a new NFA number and not in our list, we need to add it to the spd_t_facilities
      --use the JSON from the in parameter to send to the sub program
      p('Running BB_F_ADD_FACILITY',1);
      lc_vResult := BB_F_ADD_FACILITY (in_jJson, lc_jResults, lc_jMessages);
      p('AFTER Running BB_F_ADD_FACILITY',lc_vResult);
      if lc_vResult = 'y' then
        --add facility to spd_t_facilities went well, it returns the FACID in lc_jResults
        if(lc_jResults.exist('FACID')) then
          lc_jvValue := lc_jResults.get('FACID');
          lc_nFacid := getVal(lc_jvValue);
          lc_nNewFacility := 1;          
          p('lc_nFacid from new',lc_nFacid);
          p('lc_nFacid',lc_nFacid);
          
          
          if(lc_jResults.exist('ACTIVITIESID')) then
            lc_jvValue := lc_jResults.get('ACTIVITIESID');
            lc_nACTIVITIESID := getVal(lc_jvValue);
            p('lc_nACTIVITIESID',lc_nACTIVITIESID);
          else
            lc_vErrorText := 'ACTIVITIESID doesnt exist';
            lc_nContinue :=0;
            lc_jlMessages.append('error: ''Error XMLADOFAC-0001 ' || lc_vErrorText);
          end if;
          p('lc_nActivitiesid from new',lc_nActivitiesid);
        else
          lc_vErrorText := 'FACID doesnt exist';
          lc_nContinue :=0;
          lc_jlMessages.append('error: ''Error XMLADOFAC-0001 ' || lc_vErrorText);
        end if;
      else
        lc_vErrorText := 'BB_F_ADD_FACILITY did not return y';
        lc_nContinue :=0;
        lc_jlMessages.append('error: ''Error XMLADOFAC-0001 ' || lc_vErrorText);
      end if;
      --no else here, we are doing multiple linear if statements
    end if;
    

    
    if lc_nCountOfRecords = 1 then
      --this facility exists in spd_t_facilities, we want to get the FACID from the table direct
      select facid into lc_nFacid from spd_t_facilities where infads_facility_id = lc_vNFAID;
      lc_jResults := json();
      lc_jResults.put('FACID',lc_nFacid);
      p('lc_nFacid from Exisiting',lc_nFacid);
      lc_nNewFacility := 0;
      --this could be a sub program call
      select count(*) into lc_nCountOfRecords from spd_t_activities where spd_uic in (select ACTIVITY_UIC from spd_mv_inf_facility where FACILITY_ID = lc_vNFAID);
      if lc_nCountOfRecords = 1 then
        select activitiesid into lc_nActivitiesid from spd_t_activities where spd_uic in (select ACTIVITY_UIC from spd_mv_inf_facility where FACILITY_ID = lc_vNFAID);
        lc_jResults.put('ACTIVITIESID',lc_nActivitiesid);
      else
        --make the activity
        lc_vResultSubProgram := BB_F_ADD_ACTIVITY(in_jJson, lc_jResultsSubProgram, lc_jMessagesSubProgram);
      
        p('lc_vResultSubProgram',lc_vResultSubProgram);
        
        if(lc_jResultsSubProgram.exist('ACTIVITIESID')) then
          lc_vReturn := 'y';
          lc_jvValue := lc_jResultsSubProgram.get('ACTIVITIESID');
          lc_nACTIVITIESID := getVal(lc_jvValue);
        else
          lc_vErrorText := 'ACTIVITIESID doesnt exist';
          lc_nACTIVITIESID := 0;
          lc_nContinue :=0;
          lc_jlMessages.append('error: ''Error XMLIFFM-0001 ' || lc_vErrorText);
        end if;
  
        
      
      --tc took these lines out 20180702
        --lc_vErrorText := 'ACTIVITIESID does not exist for this NFA';
        --lc_nContinue :=0;
        --lc_jlMessages.append('error: ''Error XMLADOFAC-0001 ' || lc_vErrorText);
      end if;
      
      p('ACTIVITIESID',lc_nActivitiesid);
      p('lc_nContinue',lc_nContinue);
      
    end if;
    
    p('lc_nCountOfRecords t',lc_nCountOfRecords);
    if lc_nCountOfRecords > 1 then
      --there are multiple NFA id's with this one, and it presents an issue, not sure how to resolve this just yet, 
      lc_nContinue :=0;
      lc_jResults := json();
      lc_jResults.put('FACID',0);
    end if;
    
    p('lc_nContinue t',lc_nContinue);
    if lc_nContinue = 1 then
      --now that we have a FACID we can look to see if we need to add this facility to the Delivery Order facilities
      for rec in 
      (
        select 
          * 
        from 
          spd_t_do_facilities 
        where 
          facid = lc_nFacid 
          and adoid = lc_nAdoid
      )
      loop
        lc_nDofid := rec.dofid;
      end loop;
      
      p('select * from spd_t_do_facilities where facid = ' || lc_nFacid || ' and adoid = ' || lc_nAdoid);
      
      if lc_nDofid = 0 then
      /* this facility is not already in the delivery order */
      
        lc_jJson := json();
        lc_jJson.put('FACID',lc_nFacid);
        
        p('Run BB_F_DELIVERY_ORDER_ADD_FAC',1); 
        lc_jJson.put('ADOID',lc_nAdoid);
        lc_jJson.put('PRODUCTLINE_LISTID',lc_nPRODUCTLINE_LISTID);
        
        lc_vResult := BB_F_DELIVERY_ORDER_ADD_FAC(lc_jJson, lc_jResults, lc_jMessages);
        p('AFter BB_F_DELIVERY_ORDER_ADD_FAC',lc_vResult); 
        if lc_vResult = 'y' then
        
          --set the dofid to return up to calling procedure
          if(lc_jResults.exist('DOFID')) then
            lc_jvValue := lc_jResults.get('DOFID');
            lc_nDOFID := getVal(lc_jvValue);
            lc_jJson.put('DOFID',lc_nDOFID);
            
            p('lc_nDOFID',lc_nDOFID);
          else
            lc_vErrorText := 'DOFID doesnt exist';
            lc_nContinue :=0;
            lc_jlMessages.append('error: ''Error XMLADOFAC-0001 ' || lc_vErrorText);
          end if;
          
          
          --add worktype_listid to the json, and run the add default special instructions
          --!!REMOVING SPECIAL INSTRUCTIONS AND COST ESTIMATE
          /*
          lc_jJson.put('WORKTYPE_LISTID',lc_nDOWorkTypeListid);
          lc_vResult := BB_F_DO_FAC_ADD_SPEC_INSTR(lc_jJson, lc_jResults, lc_jMessages);
          
          if lc_vResult = 'y' then
            --run the delivery order facility cost estimate direct costs, we have to look into why we arent running the labor stuff as well...i'm sure i had a good reason.
            lc_vResult := BB_F_DO_FAC_CE_DIRECT_COSTS(lc_jJson, lc_jResults, lc_jMessages);
            if lc_vResult = 'y' then
              lc_nContinue := 1;
            else
              lc_vErrorText := 'Delivery Order Facility Cost Estimates were not created';
              lc_nContinue :=0;
              lc_jlMessages.append('error: ''Error XMLADOFAC-0001 ' || lc_vErrorText);
            end if;
          
          else
              lc_vErrorText := 'Delivery Order Facility Special Instructions were not created';
              lc_nContinue :=0;
              lc_jlMessages.append('error: ''Error XMLADOFAC-0001 ' || lc_vErrorText);
          end if;
          */
        else
          lc_vErrorText := 'Facility was not added to the Delivery Order';
          lc_nContinue :=0;
          lc_jlMessages.append('error: ''Error XMLADOFAC-0001 ' || lc_vErrorText);
        end if;
      else
        --this facility is already in the delivery order facility list
        --the dofid is set from the query above
        lc_nContinue := 1;
      end if;

    else
      lc_vErrorText := 'Continue is false from first portion of the subprogram';
      lc_nContinue :=0;
      lc_jlMessages.append('error: ''Error XMLADOFAC-0001 ' || lc_vErrorText);
    end if;
    
  else
    lc_vErrorText := 'The ADOID and it didnt return a work type listid';
    lc_nContinue :=0;
    lc_jlMessages.append('error: ''Error XMLADOFAC-0001 ' || lc_vErrorText);
  end if;

else
  lc_vErrorText := 'The JSON did not have the required fields';
  lc_nContinue :=0;
  lc_jlMessages.append('error: ''Error XMLADOFAC-0001 ' || lc_vErrorText);
end if;


lc_jMessages.put('messages',lc_jlMessages);


--if there are any messages this is not valid return false
if (lc_jlMessages.count > 0) then
    lc_vReturn := 'n';
    lc_jMessages.put('status','error');
else
    lc_vReturn := 'y';
    lc_jMessages.put('status','success');
    o_jResults.put('FACID',lc_nFacid);
    o_jResults.put('DOFID',lc_nDOFID);
    o_jResults.put('ACTIVITIESID',lc_nACTIVITIESID);
    o_jResults.put('NEW_FACILITY',lc_nNewFacility);
    
    
end if;

o_jMessages := lc_jMessages;

return lc_vReturn;

  exception     
    when json_error then
        lc_jError := json();
        lc_jError.put('function','BB_F_ADD_DO_FACILITY');
        lc_jError.put('input',in_jJson.to_char);
        lc_jError.put('messages',lc_jlMessages);
        BB_P_ERROR(lc_jError);
        return 'e';
    when others then
        execute immediate 'insert into debug (functionname, input, message) values (''BB_F_ADD_DO_FACILITY'',''' || json_printer.pretty_print(in_jJson) || ''',''error: ' || sqlerrm || ''')';
        return 'e';

end;