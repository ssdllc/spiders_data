--------------------------------------------------------
--  DDL for Function GETFILENAMEFROMBFILE
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE FUNCTION "SPIDERS_DATA"."GETFILENAMEFROMBFILE" (in_bfFile BFILE) RETURN VARCHAR2 is
    dir_alias VARCHAR2(255);
    file_name VARCHAR2(255);
BEGIN
    IF in_bfFile is NULL THEN
      RETURN NULL;
    ELSE
      DBMS_LOB.FILEGETNAME (in_bfFile, dir_alias, file_name);
      RETURN file_name;
    END IF;
    
    --!!TODO EXCEPTIONS
END;
 

/
