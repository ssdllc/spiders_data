--------------------------------------------------------
--  DDL for Function BFILETOBLOB
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE FUNCTION "SPIDERS_DATA"."BFILETOBLOB" (in_vDir varchar2, in_vFile varchar2) RETURN BLOB IS
   bf       BFILE;
   Amount   INTEGER := 32767;
   Position INTEGER := 1;
   buffer   RAW(32767);
   bl       LONG RAW := '';
   bb       BLOB;
BEGIN

   bf := BFILENAME(in_vDir,in_vFile);
   --bf := BFILENAME('SPD_630000085','CR_NAVFAC_ESC_CIOFP_1213_10194.PDF');
   
   dbms_lob.open(bf, dbms_lob.lob_readonly);
   DBMS_LOB.CREATETEMPORARY(bb, TRUE, DBMS_LOB.SESSION);

   LOOP
      BEGIN
         dbms_lob.read(bf, Amount, Position, buffer);
      EXCEPTION 
         WHEN NO_DATA_FOUND THEN
            EXIT;
      END;
      dbms_lob.writeappend(bb, amount, buffer);
      Position := Position + Amount; 
   END LOOP;

   dbms_lob.close(bf);

   RETURN bb;
END;
 

/
