--------------------------------------------------------
--  DDL for Function BB_F_VAL_DELETE_USER_ROLES
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE FUNCTION "SPIDERS_DATA"."BB_F_VAL_DELETE_USER_ROLES" (in_jJson json, o_jResults out json, o_jMessages out json ) RETURN VARCHAR2 AS

    lc_jJson json;
    lc_data json_list;
    lc_rec_val json_value;
    lc_rec json;
    lc_employeeid_val json_value; --
    lc_employeeid varchar(256);
    lc_cnt number;

    lc_ret_str varchar(512);
    lc_ret json;

    lc_isMember_val json_value;
    lc_isMember number;

    lc_securitygroupid_val json_value;
    lc_securitygroupid varchar2(256);

    lc_jResults json;
    lc_jlDML json_list;

    lc_jMessages json;
    lc_jlMessages json_list;
    lc_bReturn varchar2(1);
    
    --error stuff
    lc_jError json;
    json_error EXCEPTION;

BEGIN

    lc_jResults := json('{}');
    lc_jlDML := json_list('[]');

    lc_jMessages := json('{}');
    lc_jlMessages := json_list('[]');



  --POPLUTATE data ARRAY AND GET EMPLOYEEID OF FIRST ITEM
  if(in_jJson.exist('data')) then
    if(in_jJson.get('data').is_array) then
      lc_data := json_list(in_jJson.get('data'));
      lc_rec_val := lc_data.get(1); --return null on outofbounds
      if (lc_rec_val is not null) then
        lc_rec := json(lc_rec_val);
        if (lc_rec.exist('EMPLOYEEID')) then
          lc_employeeid_val := lc_rec.get('EMPLOYEEID');
          lc_employeeid := lc_employeeid_val.get_string;


          --CHECK IF EMPLOYEEID ALREADY EXISTS
            select count(SECURITYUSERID) into lc_cnt from SSD_SECURITY_USERS where EMPLOYEEID = lc_employeeid;

            if (lc_cnt > 0) then
              --EMPLOYEEID  EXISTS - REMOVE ALL
              lc_jlDML.append('DELETE FROM SSD_SECURITY_USERS WHERE EMPLOYEEID = '''||lc_employeeid||'''');
              commit;

              --CONFIRM THE EMPLOYEEID WAS SUCCESFULLY DELETED (Sodano's idea....not mine)
              --select count(SECURITYUSERID) into lc_cnt from SSD_SECURITY_USERS where EMPLOYEEID = lc_employeeid;
            else
                lc_jlMessages.append(json_list('[{error:''Error DUR.0006 There has been an error processing the request.''}]'));
            end if;

        else
            lc_jlMessages.append(json_list('[{error:''Error DUR.0001 There has been an error processing the request.''}]'));
        end if;
      else
        lc_jlMessages.append(json_list('[{error:''Error DUR.0002 There has been an error processing the request.''}]'));
      end if;
    else
        lc_jlMessages.append(json_list('[{error:''Error DUR.0003 There has been an error processing the request.''}]'));
    end if;
  else
    lc_jlMessages.append(json_list('[{error:''Error DUR.0004 There has been an error processing the request.''}]'));
  end if;

    lc_jResults.put('results',lc_jlDML);
    lc_jMessages.put('messages',lc_jlMessages);

    --if there are any messages this is not valid return false
    if (lc_jlMessages.count > 0) then
        lc_bReturn := 'n';
        lc_jMessages.put('status','error');
        raise json_error;
    else
        lc_bReturn := 'y';
        lc_jMessages.put('status','pending dml');
    end if;

    o_jMessages := lc_jMessages;
    o_jResults := lc_jResults;

    return lc_bReturn;

  exception
    when json_error then
        lc_jError := json();
        lc_jError.put('function','bb_f_val_delete_user_roles');
        lc_jError.put('input',in_jJson.to_char);
        lc_jError.put('messages',lc_jlMessages);
        o_jMessages := lc_jError;
        o_jResults := lc_jError;
        BB_P_ERROR(lc_jError);
        return 'e';

    when others then
        lc_jError := json();
        lc_jError.put('function','bb_f_val_delete_user_roles');
        lc_jError.put('input',in_jJson.to_char);
        lc_jlMessages.append(json_list('[{error:''DUR.000X ' || SQLERRM || '''}]'));
        lc_jError.put('messages',lc_jlMessages);
        o_jMessages := lc_jError;
        o_jResults := lc_jError;
        BB_P_ERROR(lc_jError);
        return 'e';

end BB_F_VAL_DELETE_USER_ROLES;


/*
DECLARE
  IN_JJSON SPIDERS_DATA.JSON;
  o_jJSON SPIDERS_DATA.JSON;
  o_jMessages SPIDERS_DATA.JSON;
  v_Return varchar2(22222);
BEGIN
  -- Modify the code to initialize the variable
  
  --DUR.0006 is produced when lc_cnt is not > than 0, this is tough to reproduce, but could be an issue at some point
  --DUR.0005 is produced when the opposite of this happens (lc_data.count > 0 and lc_cnt = 0);
  --in_jJson := json('{"target":"userRoles","data":{}}'); --produces DUR.0003
  --in_jJson := json('{"target":"userRoles","data":[]}'); --produces DUR.0002
  --in_jJson := json('{"target":"userRoles","datas":[{"EMPLOYEEID":"larry@email.com","SECURITYGROUPID":"590000001","GROUP_NAME":"Administrators1","isMember":1}]}'); --produces DUR.0004
  --in_jJson := json('{"target":"userRoless","data":[{"EMPLOsYEEID":"larry@email.com","SECURITYGROUPID":"590000001","GROUP_NAME":"Administrators1","isMember":1}]}'); --produces DUR.0001
  --in_jJson := json('{"target":"userRoles","data":[{"EMPLOYEEID":"larry@email.com","SECURITYGROUPID":"590000001","GROUP_NAME":"Administrators1","isMember":1}]}');

  v_Return := BB_F_VAL_DELETE_USER_ROLES(
    IN_JJSON,
    o_jJSON,
    o_jMessages    
  );

  dbms_output.put_line(v_return);
  
  exception
    when others then
      null;
END;
 */
 

/
