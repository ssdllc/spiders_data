--------------------------------------------------------
--  DDL for Function BB_F_CSV_TO_SIA_DATA
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE FUNCTION "SPIDERS_DATA"."BB_F_CSV_TO_SIA_DATA" (in_cClob clob, in_nXmlfileid number) return varchar2 as 


    lc_jJson json;
    lc_data json_list;
    lc_rec_val json_value;
    lc_rec json;
    o_jMessages json;
   
    lc_jValue json;
    lc_jvValue json_value;
    
    
    lc_nSequence number;
    lc_nContinue number :=0;
    lc_nCountOfRecords number :=0;
    lc_nImportType number :=0;
    
    
    lc_jlDML json_list;
    lc_jResults json;
    lc_jMessages json;
    lc_vDMLResult varchar2(255) :='9';
    lc_ret_str varchar(512);
    lc_ret json;

    thesql varchar2(2000) := '';

    lc_vWhere varchar2(2000);

    --error stuff
    lc_jError json;
    lc_jlMessages json_list;
    json_error EXCEPTION;



    lc_cClob clob := in_cClob;
    lc_bBlob blob;
    lc_nScenemodelid number;
    lc_vCreatedBy varchar2(255) :='';
    lc_nCount number :=1;
    
    lc_vReturn varchar2(4000);

    lc_nFACID number;
    lc_nADOID number := TO_NUMBER(v('SPD_ADOID'));

    lc_nDELIVERYORDERFACILITYID number := 0;
    lc_nDELIVERYORDERID number := 0;

    lc_nSIASTRUCTUREID number := 0;

    -- TYPE myTable IS TABLE OF varchar2
    -- INDEX BY BINARY_INTEGER;

    -- lc_tCSV myTable;

    lc_nCSVCount number;
    lc_nQuestionID number := 0;
    --lc_nSIADataStructureIDCount number := 0;

    lc_vValidatedValue varchar2(255);
    
/*debugging*/
lc_nDebugCount number := 0;
lc_vError long;
/*end debugging*/

BEGIN

insert into debug (message, misc) values ('checking clob',lc_cClob);

--lc_tCSV := table(csv_util_pkg.clob_to_csv13(lc_cClob,',',1));
select count(c001) into lc_nCSVCount from table(csv_util_pkg.clob_to_csv13(lc_cClob,',',1));

insert into debug (input) values ('CSV Count: ' || lc_nCSVCount);

if lc_nCSVCount > 0 then
    /* Get FACID */
    select TO_NUMBER(c001) into lc_nFACID from table(csv_util_pkg.clob_to_csv13(lc_cClob,',',1)) where rownum = 1;
    /* For testing */
    lc_nCSVCount := lc_nFACID;
    insert into debug (input) values ('CSV Count - FACID: ' || lc_nCSVCount);
end if;

if lc_nCSVCount > 0 then
    /* Get DELIVERYORDERID from SPD_T_DELIVERY_ORDERS */
    for rec in (select DELIVERYORDERID
    from SPD_T_DELIVERY_ORDERS
    where ADOID = lc_nADOID)
    loop
        lc_nDELIVERYORDERID := rec.DELIVERYORDERID;
    end loop;
    insert into debug (input) values ('Delivery Order ID: ' || lc_nDELIVERYORDERID);
end if;

if lc_nDELIVERYORDERID > 0 then
    select count(DELIVERYORDERFACILITYID) into lc_nCSVCount
    from SPD_T_DELIVERY_ORDER_FACS
    where DELIVERYORDERID = lc_nDELIVERYORDERID
    and FACID = lc_nFACID;

    /* If there is no DELIVERY ORDER FAC record, create new record in SPD_T_DELIVERY_ORDER_FACS
    then get new DELIVERYORDERFACILITYID */
    if lc_nCSVCount = 0 then
        select SPD_S_DELIVERY_ORDER_FACS.nextval into lc_nDELIVERYORDERFACILITYID from dual;
        insert into SPD_T_DELIVERY_ORDER_FACS (DELIVERYORDERFACILITYID, DELIVERYORDERID, FACID)
            values (lc_nDELIVERYORDERFACILITYID, lc_nDELIVERYORDERID, lc_nFACID);
    else 
        select DELIVERYORDERFACILITYID into lc_nDELIVERYORDERFACILITYID
        from SPD_T_DELIVERY_ORDER_FACS
        where DELIVERYORDERID = lc_nDELIVERYORDERID
        and FACID = lc_nFACID;
    end if;
end if;

if lc_nDELIVERYORDERFACILITYID > 0 then
    select count(SIASTRUCTUREID) into lc_nSIASTRUCTUREID
    from SPD_T_SIA_STRUCTURES
    where DOFID = lc_nDELIVERYORDERFACILITYID;

    /* If there is no SIA STRUCTURE record, create new record in
    SPD_T_SIA_STRUCTURES then get new SIASTRUCTUREID */
    if lc_nSIASTRUCTUREID = 0 then
        select SPD_S_SIA_STRUCTURES.nextval into lc_nSIASTRUCTUREID from dual;
        insert into SPD_T_SIA_STRUCTURES (SIASTRUCTUREID, ADOID, DOFID)
            values (lc_nSIASTRUCTUREID, lc_nADOID, 
                lc_nDELIVERYORDERFACILITYID);
    /* else if the structure already exists */
    else 
        select SIASTRUCTUREID into lc_nSIASTRUCTUREID
        from SPD_T_SIA_STRUCTURES
        where DOFID = lc_nDELIVERYORDERFACILITYID;
    end if;
end if;

/* Check if STRUCTUREID is already in SIA_DATA */
if lc_nSIASTRUCTUREID > 0 then
    -- select count(SIASTRUCTUREID) into lc_nSIADataStructureIDCount
    -- from spd_t_sia_data
    -- where SIASTRUCTUREID = lc_nSIASTRUCTUREID

    /* If the StructureIDCount is not zero - do we want to delete these records from SIA_DATA? */
    -- if lc_nSIADataStructureIDCount = 0 then

    delete from spd_t_sia_data where SIASTRUCTUREID = lc_nSIASTRUCTUREID;

    lc_cClob := in_cClob;
    lc_jlDML := json_list();
    lc_jResults := json();
    lc_jMessages := json();
    lc_vCreatedBy := v('APP_USER');

    -- Skipping the first row in the CSV file
    for rec in (select * from table(csv_util_pkg.clob_to_csv13(lc_cClob,',',1)))
        loop
            -- Converting SIAQUESTIONID to number - stored in SIA_DATA as varchar2 for some reason
            if pkg_sia_tools.is_number(rec.c003) = 1 then
                lc_nQuestionID := TO_NUMBER(rec.c003);
            else 
                lc_nQuestionID := 0;
            end if;

            lc_vValidatedValue := pkg_sia_tools.validateSIAValue(rec.c009, lc_nQuestionID);

            --insert into debug (message) values ('validated value: ' || lc_vValidatedValue);

            if lc_vValidatedValue = 'Invalid' then
                exit;
            end if;

            --insert into debug (misc) values (in_cClob);
            select SPD_S_SIA_DATA.nextval into lc_nSequence from dual;
            --Insert record into table
            thesql := 'insert into spd_t_sia_data (
                    SIAID,
                    SIAQUESTIONID,
                    VALUE,
                    CREATED_BY,
                    SIASTRUCTUREID)

                    values (
                      ' || lc_nSequence || ',
                      ''' || rec.c003 || ''',
                      ''' || lc_vValidatedValue || ''',
                      ''' || lc_vCreatedBy || ''',
                     ' || lc_nSIASTRUCTUREID || '
                    )';
            insert into debug (message) values (thesql);

            lc_jlDML := json_list();
            lc_jResults := json();
            lc_jlDML.append(thesql);
            lc_jResults.put('results',lc_jlDML);
    
            lc_vDMLResult := BB_F_DML(lc_jResults, lc_jMessages);
            if lc_vDMLResult = 'n' then
                exit;
            end if;
        end loop;
    if (lc_vDMLResult = 'n') or (lc_vValidatedValue = 'Invalid') then
        delete from spd_t_sia_data where SIASTRUCTUREID = lc_nSIASTRUCTUREID;
        lc_vReturn := 'n';
    else 
        lc_vReturn := 'y';
    end if;
else 
    lc_vReturn := 'n';
end if;

/*debugging*/
--insert into debug (functionname, input, message) values ('BB_F_CSV_TO_DRY_DOCK_CI_TMP','[SPD_BUG_FIX][03] Dry Dock Upload','1');
/*end debugging*/ 
return lc_vReturn;

exception
when json_error then
/*debugging*/
insert into debug (functionname, input, message) values ('BB_F_CSV_TO_SIA_DATA','SIA Upload','e1');
/*end debugging*/
    /*
    --htp.p('raised json_error in update func');
    lc_jError := json();
    lc_jError.put('function','BB_F_CSV_TO_DRY_DOCK_CI_TMP');
    lc_jError.put('input','csv file ' || lc_nSceneid);
    lc_jError.put('messages',lc_jlMessages);
    BB_P_ERROR(lc_jError);
    return 'e';
    --htp.p('returning n');
    */
when others then
/*debugging*/
lc_vError := SQLERRM;
insert into debug (functionname, input, message) values ('BB_F_CSV_TO_SIA_DATA','SIA Upload','e2' || lc_vError);
/*end debugging*/
  /*
    --htp.p('this ' || SQLERRM);
    lc_jError := json();
    lc_jlMessages := json_list();
    lc_jError.put('function','BB_F_CSV_TO_DRY_DOCK_CI_TMP');
    lc_jError.put('input','');
    lc_jlMessages.append(json_list('[{error:''CSVTDDCIT.000X ' || SQLERRM || '''}]'));
    lc_jError.put('messages',lc_jlMessages);
    BB_P_ERROR(lc_jError);
    return 'e';
    */
end;

/
