--------------------------------------------------------
--  DDL for Function BB_F_READ_IS_ADMIN
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE FUNCTION "SPIDERS_DATA"."BB_F_READ_IS_ADMIN" (in_jJson json) RETURN json AS

    lc_jJson json;
    lc_data json_list;
    lc_rec_val json_value;
    lc_rec json;
    lc_employeeid_val json_value; --
    lc_employeeid varchar(256);
    lc_cnt number;

    lc_ret_str varchar(512);
    lc_ret json;

    thesql varchar2(2000) := '';

    --error stuff
    lc_jError json;
    lc_jlMessages json_list;
    json_error EXCEPTION;

BEGIN
/*
stub data
{"module":"userRoleManager","target":"isAdmin","data":[]}
*/



lc_employeeid := v('APP_USER');
--htp.p('USER IS '||lc_employeeid);
thesql := 'select
        count(*) isAdmin
    from
        ssd_security_groups g,
        ssd_security_users u
    where
        g.securitygroupid = u.securitygroupid and
        (g.group_name = ''Security View'' or g.group_name = ''Security Add'' or g.group_name = ''Security Update'' or g.group_name = ''Security Delete'') and
        upper(u.employeeid) = ''' || lc_employeeid || '''';


  lc_jJson := json_dyn.executeObject(thesql);

  return lc_jJson;

exception
    when json_error then
        --htp.p('raised json_error in update func');
        lc_jError := json();
        lc_jError.put('function','BB_F_READ_IS_ADMIN');
        lc_jError.put('input',in_jJson.to_char);
        lc_jError.put('messages',lc_jlMessages);
        BB_P_ERROR(lc_jError);
        --htp.p('returning n');
        return json('e');
    when others then
        lc_jError := json();
        lc_jError.put('function','BB_F_READ_IS_ADMIN');
        lc_jError.put('input',in_jJson.to_char);
        lc_jlMessages.append(json_list('[{error:''RUR.000X ' || SQLERRM || '''}]'));
        lc_jError.put('messages',lc_jlMessages);
        BB_P_ERROR(lc_jError);
        return json('e');


END BB_F_READ_IS_ADMIN;

/
