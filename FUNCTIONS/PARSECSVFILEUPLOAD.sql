--------------------------------------------------------
--  DDL for Function PARSECSVFILEUPLOAD
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE FUNCTION "SPIDERS_DATA"."PARSECSVFILEUPLOAD" (in_vFlowsFilesName in varchar2, in_vImportType varchar2) return varchar2 IS

lc_bBlob BLOB;
lc_cClob clob;
lc_nStart NUMBER := 1;
lc_nBytelen NUMBER := 32000;
lc_nLen NUMBER;
lc_rRaw RAW(32000);
lc_nContinue number :=0;
x NUMBER;

l_output utl_file.file_type;

lc_jError json;
lc_jlMessages json_list;

lc_jlDML json_list;
lc_jResults json;
lc_jMessages json;
lc_vDMLResult varchar2(255);

lc_vFilename varchar2(2000);
lc_nSequence number :=0;
lc_vMimeType varchar2(4000);

/*EDITED 20170623 MR DEBUG: [SPD_BUG_FIX][03] Dry Dock Upload*/
/*EDITED 20170913 MR COMMENTED DEBUG: [SPD_BUG_FIX][03] Dry Dock Upload*/

BEGIN


/*debugging*/
--insert into debug (functionname, input, message) values ('PARSECSVFILEUPLOAD','[SPD_BUG_FIX][03] Dry Dock Upload','1');
/*end debugging*/

  for rec in (
    select
      wwv_flow_files.MIME_TYPE mimeType,
      wwv_flow_files.FILENAME fileName,
      wwv_flow_files.BLOB_CONTENT blob_content,
      dbms_lob.getlength(wwv_flow_files.BLOB_CONTENT) filesize
    from
      wwv_flow_files
    where
      name = in_vFlowsFilesName) 
  loop 
      lc_vMimeType := rec.mimeType;
      lc_vFilename := rec.fileName;
      lc_bBlob := rec.blob_content;
      lc_nLen := rec.filesize;
  end loop;
  
/*debugging*/
--insert into debug (functionname, input, message) values ('PARSECSVFILEUPLOAD','[SPD_BUG_FIX][03] Dry Dock Upload','2');
/*end debugging*/  
  --insert into debug (input, message) values (lc_vFilename, lc_vMimeType);

  if upper(lc_vMimeType) = 'TEXT/CSV' then
    lc_nContinue :=1;
  else
    if upper(substr(lc_vFilename,-4))='.CSV' then
      lc_nContinue :=1;
    else
      lc_nContinue :=0;
    end if;
  end if;
      
/*debugging*/
--insert into debug (functionname, input, message) values ('PARSECSVFILEUPLOAD','[SPD_BUG_FIX][03] Dry Dock Upload','3');
/*end debugging*/ 

  if lc_nContinue = 1 then
 
 
/*debugging*/
--insert into debug (functionname, input, message) values ('PARSECSVFILEUPLOAD','[SPD_BUG_FIX][03] Dry Dock Upload','4');
/*end debugging*/

    lc_cClob := blob_to_clob(lc_bBlob);    
    
    case in_vImportType
      when 'SIACSV' then
        lc_nSequence := PURGEDDCSVFILE(lc_vFilename,upper(substr(lc_vFilename,-4)));
        if lc_nSequence > 0 then
          lc_vMimeType := BB_F_CSV_TO_SIA_DATA(lc_cClob,lc_nSequence);
        else
          null;
        end if;
      when 'CustomShapesCSV' then
        lc_vMimeType := BB_F_CSV_TO_CUSTOM_SHAPES_TMP(lc_cClob);
      when 'DryDockConditionCSV' then

/*debugging*/
--insert into debug (functionname, input, message) values ('PARSECSVFILEUPLOAD','[SPD_BUG_FIX][03] Dry Dock Upload','4a');
/*end debugging*/

        lc_nSequence := PURGEDDCSVFILE(lc_vFilename,upper(substr(lc_vFilename,-4)));
          --insert into debug (input) values (lc_nSequence);
          
/*debugging*/
--insert into debug (functionname, input, message) values ('PARSECSVFILEUPLOAD','[SPD_BUG_FIX][03] Dry Dock Upload','4b');
/*end debugging*/


        if lc_nSequence > 0 then

/*debugging*/
--insert into debug (functionname, input, message) values ('PARSECSVFILEUPLOAD','[SPD_BUG_FIX][03] Dry Dock Upload','4c');
/*end debugging*/

          lc_vMimeType := BB_F_CSV_TO_DRY_DOCK_CI_TMP(lc_cClob,lc_nSequence);
        else
          --!! If the purge dry dock csv file failed to return a sequence. this else will get called, dont know if we need to really worry about it yet !!
          null;
        end if;
      else
        null;
    end case;
  
  else

/*debugging*/
--insert into debug (functionname, input, message) values ('PARSECSVFILEUPLOAD','[SPD_BUG_FIX][03] Dry Dock Upload','5');
/*end debugging*/

    lc_vMimeType := 'file invalid';
  end if;
 
/*debugging*/
--insert into debug (functionname, input, message) values ('PARSECSVFILEUPLOAD','[SPD_BUG_FIX][03] Dry Dock Upload','6');
/*end debugging*/ 

return lc_vMimeType;

exception 
  when others then
      
/*debugging*/
insert into debug (functionname, input, message) values ('PARSECSVFILEUPLOAD','[SPD_BUG_FIX][03] Dry Dock Upload','7');
/*end debugging*/

      /*
      lc_jError := json();
      lc_jlMessages := json_list();
      lc_jError := json();
      lc_jError.put('function','PARSECSVFILEUPLOAD');
      lc_jError.put('input','');

      lc_jlMessages.append(json_list('[{error:''PCSVFU.000X blah''}]'));
      lc_jError.put('messages',lc_jlMessages);
      BB_P_ERROR(lc_jError);
      --htp.p('e');
      */
      

end PARSECSVFILEUPLOAD;

/
