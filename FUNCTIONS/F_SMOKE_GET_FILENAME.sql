--------------------------------------------------------
--  DDL for Function F_SMOKE_GET_FILENAME
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE FUNCTION "SPIDERS_DATA"."F_SMOKE_GET_FILENAME" 
   (p_path IN VARCHAR2)
   RETURN varchar2

IS
   v_file VARCHAR2(32000);
   lc_vPath long;

BEGIN

   lc_vPath := replace(p_path,'/','\');
   
   -- Parse string for UNIX system
   IF INSTR(lc_vPath,'/') > 0 THEN
   
      v_file := SUBSTR(lc_vPath,(INSTR(lc_vPath,'/',-1,1)+1),length(lc_vPath));

   -- Parse string for Windows system
   ELSIF INSTR(lc_vPath,'\') > 0 THEN
      v_file := SUBSTR(p_path,(INSTR(lc_vPath,'\',-1,1)+1),length(lc_vPath));

   -- If no slashes were found, return the original string
   ELSE
      v_file := lc_vPath;

   END IF;
   
   v_file := upper(v_file);

   RETURN v_file;

END;
 

/
