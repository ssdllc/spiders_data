--------------------------------------------------------
--  DDL for Function BB_F_CSV_TO_DRY_DOCK_CI_TMP
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE FUNCTION "SPIDERS_DATA"."BB_F_CSV_TO_DRY_DOCK_CI_TMP" (in_cClob clob, in_nXmlfileid number) return varchar2 as 


    lc_jJson json;
    lc_data json_list;
    lc_rec_val json_value;
    lc_rec json;
    o_jMessages json;
   
    lc_jValue json;
    lc_jvValue json_value;
    
    
    lc_nSequence number;
    lc_nContinue number :=0;
    lc_nCountOfRecords number :=0;
    lc_nImportType number :=0;
    
    
    lc_jlDML json_list;
    lc_jResults json;
    lc_jMessages json;
    lc_vDMLResult varchar2(255) :='9';
    lc_ret_str varchar(512);
    lc_ret json;

    thesql varchar2(2000) := '';

    lc_vWhere varchar2(2000);

    --error stuff
    lc_jError json;
    lc_jlMessages json_list;
    json_error EXCEPTION;



    lc_cClob clob;
    lc_bBlob blob;
    lc_nScenemodelid number;
    lc_vCreatedBy varchar2(255) :='';
    lc_nCount number :=1;
    lc_nSceneid number;
    
    lc_vReturn varchar2(4000) := 'sdfsdf';
    
/*debugging*/
lc_nDebugCount number := 0;
lc_vError long;
/*end debugging*/
/*EDITED 20170623 MR DEBUG: [SPD_BUG_FIX][03] Dry Dock Upload*/
/*EDITED 20170917 MR COMMENTED DEBUG: [SPD_BUG_FIX][03] Dry Dock Upload*/

BEGIN

/*debugging*/
--insert into debug (functionname, input, message) values ('BB_F_CSV_TO_DRY_DOCK_CI_TMP','[SPD_BUG_FIX][03] Dry Dock Upload','1');
/*end debugging*/

lc_cClob := in_cClob;
lc_jlDML := json_list();
lc_jResults := json();
lc_jMessages := json();

lc_vCreatedBy := v('APP_USER');



  for rec in (select * from table(csv_util_pkg.clob_to_csv(lc_cClob)))  
  loop



      select SPD_S_DRY_DOCK_CI_TMP.nextval into lc_nSequence from dual;
      --Insert record into table
      
/* 
Order	
UFII
Item_Inspected
Satifactory
Marginal
Unsatisfactory
Deficiency_Notes
CI
Cost_Repair
Date_Inspected
iNFADS
Visual
Operational
eFEM
Initial
*/


/*!!!!NEED TO GET THE FACID, USING 1 FOR NOW !!!!*/

      thesql := 'insert into spd_t_dry_dock_ci_tmp (
                    FACID,
                    UFII_CODE, 
                    ITEMS_INSPECTED, 
                    SATISFACTORY, 
                    MARGINAL, 
                    UNSATISFACTORY,
                    CONDITION_INDEX, 
                    EST_COST_TO_REPAIR, 
                    INSPECTION_DATE,  
                    REMARKS, 
                    VISUAL_INSPECTION, 
                    OPERATIONAL_INSPECTION, 
                    FEMS_NUMBER,
                    INITALS, 
                    CREATED_BY,
                    XMLFILEID
                  )
                    
                  values (
                    1,
                    ''' || replace(trim(rec.c002),CHR(39),'') || ''',
                    ''' || replace(trim(rec.c003),CHR(39),'') || ''',
                    ''' || replace(trim(rec.c006),CHR(39),'') || ''',
                    ''' || replace(trim(rec.c007),CHR(39),'') || ''',
                    ''' || replace(trim(rec.c008),CHR(39),'') || ''',
                    ''' || replace(trim(rec.c009),CHR(39),'') || ''',
                    ''' || replace(trim(rec.c013),CHR(39),'') || ''',
                    ''' || replace(trim(rec.c016),CHR(39),'') || ''', --back to 16 from 15 --was 16
                    ''' || replace(trim(rec.c010),CHR(39),'') || ''',
                    ''' || replace(trim(rec.c004),CHR(39),'') || ''',
                    ''' || replace(trim(rec.c005),CHR(39),'') || ''',
                    ''' || replace(trim(rec.c012),CHR(39),'') || ''',
                    ''' || replace(trim(rec.c015),CHR(39),'') || ''', --back to 15 from 14 --was 15
                    ''' || lc_vCreatedBy || ''',
                    ' || in_nXmlfileid || '
                  )';

      if substr(rec.c002,0,2) = 'H2' then


/*debugging*/
--insert into debug (functionname, input, message) values ('BB_F_CSV_TO_DRY_DOCK_CI_TMP','[SPD_BUG_FIX][03] Dry Dock Upload-sql',thesql);
lc_nDebugCount := lc_nDebugCount + 1;
/*end debugging*/

		lc_jlDML := json_list();
		lc_jResults := json();
      	lc_jlDML.append(thesql);
         

      lc_jResults.put('results',lc_jlDML);
/*debugging*/
--insert into debug (functionname, input, message) values ('BB_F_CSV_TO_DRY_DOCK_CI_TMP','[SPD_BUG_FIX][03] Dry Dock Upload','2a');
/*end debugging*/

      lc_vDMLResult := BB_F_DML(lc_jResults, lc_jMessages);
/*debugging*/
--insert into debug (functionname, input, message) values ('BB_F_CSV_TO_DRY_DOCK_CI_TMP','[SPD_BUG_FIX][03] Dry Dock Upload','2b');
/*end debugging*/

      --lc_jMessages.put('id',lc_nSequence);
/*debugging*/
----insert into debug (functionname, input, message) values ('BB_F_CSV_TO_DRY_DOCK_CI_TMP','[SPD_BUG_FIX][03] Dry Dock Upload','2c');
/*end debugging*/
      --o_jMessages := lc_jMessages;

/*debugging*/
----insert into debug (functionname, input, message) values ('BB_F_CSV_TO_DRY_DOCK_CI_TMP','[SPD_BUG_FIX][03] Dry Dock Upload','3' || lc_vDMLResult);
/*end debugging*/ 


end if; 

  end loop;


lc_vReturn := lc_vDMLResult;
return lc_vReturn;

exception
when json_error then
/*debugging*/
insert into debug (functionname, input, message) values ('BB_F_CSV_TO_DRY_DOCK_CI_TMP','[SPD_BUG_FIX][03] Dry Dock Upload','e1');
/*end debugging*/
    /*
    --htp.p('raised json_error in update func');
    lc_jError := json();
    lc_jError.put('function','BB_F_CSV_TO_DRY_DOCK_CI_TMP');
    lc_jError.put('input','csv file ' || lc_nSceneid);
    lc_jError.put('messages',lc_jlMessages);
    BB_P_ERROR(lc_jError);
    return 'e';
    --htp.p('returning n');
    */
when others then
/*debugging*/
lc_vError := SQLERRM;
insert into debug (functionname, input, message) values ('BB_F_CSV_TO_DRY_DOCK_CI_TMP','[SPD_BUG_FIX][03] Dry Dock Upload','e2' || lc_vError);
/*end debugging*/
  /*
    --htp.p('this ' || SQLERRM);
    lc_jError := json();
    lc_jlMessages := json_list();
    lc_jError.put('function','BB_F_CSV_TO_DRY_DOCK_CI_TMP');
    lc_jError.put('input','');
    lc_jlMessages.append(json_list('[{error:''CSVTDDCIT.000X ' || SQLERRM || '''}]'));
    lc_jError.put('messages',lc_jlMessages);
    BB_P_ERROR(lc_jError);
    return 'e';
    */
end;

/
