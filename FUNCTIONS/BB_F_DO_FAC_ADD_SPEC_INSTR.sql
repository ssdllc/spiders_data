--------------------------------------------------------
--  DDL for Function BB_F_DO_FAC_ADD_SPEC_INSTR
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE FUNCTION "SPIDERS_DATA"."BB_F_DO_FAC_ADD_SPEC_INSTR" (in_jJson json, o_jResults out json, o_jMessages out json ) RETURN VARCHAR2 AS



--JSON stuff
lc_jJson json;
lc_data json_list;
lc_rec_val json_value;
lc_rec json;
lc_jValue json;
lc_jvValue json_value;


--procedure specific stuff
lc_nSequence number;
lc_nContinue number :=0;
lc_nCountOfRecords number :=0;
lc_nADOID number;
lc_vNFAID varchar2(2000);
lc_nPRODUCTLINE_LISTID number;
lc_nWORKTYPE_LISTID number;
lc_vFACILITY_NAME varchar2(2000);
lc_vFACILITY_NO varchar2(2000);
lc_nDOFID number;

--DML stuff
lc_jlDML json_list;
lc_jResults json;
lc_jMessages json;
lc_vDMLResult varchar2(255);
lc_vResult varchar2(200);
lc_ret_str varchar(512);
lc_ret json;
thesql varchar2(2000) := '';
lc_vWhere varchar2(2000);
lc_vReturn varchar2(2000);

--error stuff
lc_vErrorText varchar2(2000);
lc_jError json;
lc_jlMessages json_list;
json_error EXCEPTION;


procedure p (in_vInput varchar2, in_vMessage varchar2 default '') as

begin

insert into debug (input, message) values (in_vInput, in_vMessage);

end;


BEGIN
/*
stub data
*/

lc_jResults := json();
lc_jlDML := json_list();
lc_jMessages := json();
lc_jlMessages := json_list();
o_jResults := json();
o_jMessages := json();

if(in_jJson.exist('PRODUCTLINE_LISTID')) then
  lc_jvValue := in_jJson.get('PRODUCTLINE_LISTID');
  lc_nPRODUCTLINE_LISTID := getVal(lc_jvValue);
else
  lc_vErrorText := 'PRODUCTLINE_LISTID doesnt exist';
  lc_nContinue :=0;
  lc_jlMessages.append('error: ''Error XMLIFFM-0001 ' || lc_vErrorText);
end if;

p('lc_nPRODUCTLINE_LISTID',lc_nPRODUCTLINE_LISTID);



if(in_jJson.exist('WORKTYPE_LISTID')) then
  lc_jvValue := in_jJson.get('WORKTYPE_LISTID');
  lc_nWORKTYPE_LISTID := getVal(lc_jvValue);
else
  lc_vErrorText := 'WORKTYPE_LISTID doesnt exist';
  lc_nContinue :=0;
  lc_jlMessages.append('error: ''Error XMLIFFM-0001 ' || lc_vErrorText);
end if;



p('lc_nWORKTYPE_LISTID',lc_nWORKTYPE_LISTID);



if(in_jJson.exist('DOFID')) then
  lc_jvValue := in_jJson.get('DOFID');
  lc_nDOFID := getVal(lc_jvValue);
else
  lc_vErrorText := 'DOFID doesnt exist';
  lc_nContinue :=0;
  lc_jlMessages.append('error: ''Error XMLIFFM-0001 ' || lc_vErrorText);
end if;


p('lc_nDOFID',lc_nDOFID);





select count(*) into lc_nCountOfRecords from spd_t_do_fac_spec_inst where dofid = lc_nDOFID;
if lc_nCountOfRecords = 0 then
  
  --Check if the productline listid is valid
  select count(*) into lc_nCountOfRecords from spd_t_lists where listname = 'list_product_lines' and listid = lc_nPRODUCTLINE_LISTID;
  p('lc_nCountOfRecords list_product_lines',lc_nCountOfRecords);
  if lc_nCountOfRecords = 1 then
    --check if the worktype listid is good
    select count(*) into lc_nCountOfRecords from spd_t_lists where listname = 'list_sow_types' and listid = lc_nWORKTYPE_LISTID;
    if lc_nCountOfRecords = 1 then
      lc_nContinue := 1;
    else
      lc_vErrorText := 'Worktype List Id Invalid';
      lc_nContinue :=0;
      lc_jlMessages.append('error: ''error XMLIFFM-0001 ' || lc_vErrorText);
    end if;
  else
      lc_vErrorText := 'Productline Id Invalid';
      lc_nContinue :=0;
      lc_jlMessages.append('error: ''error XMLIFFM-0001 ' || lc_vErrorText);
  end if;
  
  
  if lc_nContinue = 1 then
    --Add the records to the special insructions
    thesql := '
    insert into spd_t_do_fac_spec_inst (dofid, spec_instr_listid) 
    select 
      ' || lc_nDOFID || ', 
      listid 
    from 
      spd_t_lists where listid in 
        (
        select 
          listid 
        from 
          spd_t_lists
        where 
          productline_listid = ' || lc_nPRODUCTLINE_LISTID  || '
          and listname = ''list_facility_special_instructions''
          and grouping = (select displayvalue from spd_t_lists where listid = ' || lc_nWORKTYPE_LISTID || ')
        )';
    


    lc_jlDML.append(thesql);
    lc_jResults.put('results',lc_jlDML);
    lc_vDMLResult := BB_F_DML(lc_jResults, lc_jMessages);
    lc_jMessages.put('id',lc_nSequence);
    o_jMessages := lc_jMessages;
    
    end if;
    
    

else
  lc_vDMLResult := 'y';
  lc_vErrorText := 'Facility Special Instructions exists';
  lc_nContinue :=0;
  lc_jlMessages.append('warning: ''warning XMLIFFM-0001 ' || lc_vErrorText);
end if;

lc_jMessages.put('messages',lc_jlMessages);


--if there are any messages this is not valid return false
if (lc_jlMessages.count > 0) then
    lc_vReturn := 'n';
    lc_jMessages.put('status','error');
    --raise json_error;
else
    lc_vReturn := 'y';
    lc_jMessages.put('status','success');
end if;


    o_jResults := json();
    o_jMessages := lc_jMessages;

return lc_vReturn;

exception
when json_error then
      execute immediate 'insert into debug (functionname, input, message) values (''BB_F_DO_FAC_ADD_SPEC_INSTR'',''' || json_printer.pretty_print(in_jJson) || ''',''error: something '')';
      return 'e';
    lc_jError := json();
    lc_jError.put('function','BB_F_DO_FAC_ADD_SPEC_INSTR');
    lc_jError.put('input',json_printer.pretty_print(in_jJson));
    lc_jError.put('messages',lc_jlMessages);
    BB_P_ERROR(lc_jError);
    return 'n';
    --htp.p('returning n');
  when others then
      execute immediate 'insert into debug (functionname, input, message) values (''BB_F_DO_FAC_ADD_SPEC_INSTR'',''' || json_printer.pretty_print(in_jJson) || ''',''error: ' || sqlerrm || ''')';
      return 'e';


end;
 

/
