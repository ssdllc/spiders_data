--------------------------------------------------------
--  DDL for Function BB_F_COPY_FILES_BEGIN_PROC
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE FUNCTION "SPIDERS_DATA"."BB_F_COPY_FILES_BEGIN_PROC" (in_jJson json) return JSON as


    lc_vFolder varchar2(2000);
    lc_vFolderID varchar2(2000);
    lc_vFileID varchar2(2000);
    
    lc_data json_list;

    lc_nAdoid number :=0;
    lc_adoid_val json_value;
    
    lc_ProcessImages_val json_value;
    lc_vProcessImages varchar2(100);
    
    
    lc_vReturn varchar2(1000);
    lc_vMessage varchar2(2000);
    lc_vSuccess varchar2(2000);
    lc_nRandom number;

    lc_jJson json;
    lc_jlResults json_list;
    lc_rec_val json_value;
    lc_rec json;
    lc_cnt number;

    lc_jResults json;
    lc_jMessages json;
    lc_jlMessages json_list;
    lc_vReturn varchar2(1);
    lc_vDML varchar2(32000);
    lc_cClob clob;
    --error stuff
    lc_jError json;
    json_error EXCEPTION;
    
    

begin

    lc_jMessages := json('{"messages":[],"status":"success"}');
    lc_jlMessages := json_list();


    if(in_jJson.exist('data')) then
        if(in_jJson.get('data').is_array) then
            lc_data := json_list(in_jJson.get('data'));
            lc_rec_val := lc_data.get(1); --return null on outofbounds
            if (lc_rec_val is not null) then
                lc_rec := json(lc_rec_val);
                
                if (lc_rec.exist('ADOID')) then
                    lc_adoid_val := lc_rec.get('ADOID');
                    lc_nAdoid := lc_adoid_val.get_number;
                else
                    lc_nAdoid :=0;
                end if;
                
                if (lc_rec.exist('PROCESS_IMAGES')) then
                    lc_ProcessImages_val := lc_rec.get('PROCESS_IMAGES');
                    lc_vProcessImages := lc_ProcessImages_val.get_string;
                else
                    lc_vProcessImages :='N';
                end if;
            end if;
        end if;
    end if;

--get values from json object
    --Initiate the copy of the files
    
    lc_nRandom := round(dbms_random.value(1,10000),0);
    lc_vFolder := file_tools.get_FolderName(lc_nADOID);
    lc_vFolderID := file_tools.get_FolderID(lc_vFolder);
    lc_vFileID := 1;

    insert into debug (functionname, input) values ('BB_F_COPY_FILES_BEGIN_PROC','Process: ' || lc_vProcessImages || ', Random: ' || lc_nRandom);
    dbms_scheduler.create_job(
        job_name        => 'job_cp_files_load_loaded_' || lc_nRandom,
        program_name    => 'COPY_FILES_LOAD_LOADED',
        start_date      => systimestamp,
        end_date        => null
    );

    dbms_scheduler.set_job_argument_value(
        job_name          => 'job_cp_files_load_loaded_' || lc_nRandom,
        argument_position => 1,
        argument_value    => lc_vFolder
    );
    dbms_scheduler.set_job_argument_value(
        job_name          => 'job_cp_files_load_loaded_' || lc_nRandom,
        argument_position => 2,
        argument_value    => lc_vFolderID
    );
    dbms_scheduler.set_job_argument_value(
        job_name          => 'job_cp_files_load_loaded_' || lc_nRandom,
        argument_position => 3,
        argument_value    => lc_nAdoid
    );
    dbms_scheduler.set_job_argument_value(
        job_name          => 'job_cp_files_load_loaded_' || lc_nRandom,
        argument_position => 4,
        argument_value    => lc_vFileID
    );
    dbms_scheduler.set_job_argument_value(
        job_name          => 'job_cp_files_load_loaded_' || lc_nRandom,
        argument_position => 5,
        argument_value    => lc_vProcessImages
    );

    dbms_scheduler.enable (name => 'job_cp_files_load_loaded_' || lc_nRandom);
    insert into debug (functionname, input) values ('BB_F_COPY_FILES_BEGIN_PROC','scheduled job: job_cp_files_load_loaded_' || lc_nRandom);
    
    return lc_jMessages;
    
    exception
        when others then
          execute immediate 'insert into debug (functionname, message) values (''BB_F_COPY_FILES_BEGIN_PROC'','' error: ' || sqlerrm || ''')';
            /*
            lc_jError := json();
            lc_jError.put('function','copySpidersFilesAndProcessXML');
            lc_jError.put('module','file_upload');
            lc_jError.put('target','');
            lc_jError.put('ref_id','');
            lc_jError.put('input','');
            lc_jlMessages.append(json_list('[{error:''FU.000X ' || SQLERRM || '''}]'));
            lc_jError.put('messages',lc_jlMessages);
            BB_P_ERROR(lc_jError);
            */


end;
 

/
