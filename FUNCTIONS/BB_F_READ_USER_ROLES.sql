--------------------------------------------------------
--  DDL for Function BB_F_READ_USER_ROLES
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE FUNCTION "SPIDERS_DATA"."BB_F_READ_USER_ROLES" (in_jJson json) RETURN json AS 

    lc_jJson json;
    lc_data json_list;
    lc_rec_val json_value;
    lc_rec json;
    lc_employeeid_val json_value; --
    lc_employeeid varchar(256);
    lc_cnt number;

    lc_ret_str varchar(512);
    lc_ret json;

    thesql varchar2(2000) := '';

    --error stuff
    lc_jError json;
    lc_jlMessages json_list;
    json_error EXCEPTION;

BEGIN
/*
stub data
{"target":"userRoles","data":[{"EMPLOYEEID":"larry@email.com"}]}
*/
  
    if(in_jJson.exist('data')) then
        if(in_jJson.get('data').is_array) then
            lc_data := json_list(in_jJson.get('data'));
            dbms_output.put_line(lc_data.count);
            lc_rec_val := lc_data.get(1); --return null on outofbounds
            if (lc_rec_val is not null) then
                lc_rec := json(lc_rec_val);
                if (lc_rec.exist('EMPLOYEEID')) then
                    lc_employeeid_val := lc_rec.get('EMPLOYEEID');
                    lc_employeeid := lc_employeeid_val.get_string;
                    dbms_output.put_line(lc_employeeid);
                else
                    lc_jlMessages.append(json_list('[{error:''Error RUR-0001 There has been an error processing the request''}]'));
                end if;
            else
                lc_jlMessages.append(json_list('[{error:''Error RUR-0002 There has been an error processing the request''}]'));
            end if;
        else
            lc_jlMessages.append(json_list('[{error:''Error RUR-0003 There has been an error processing the request''}]'));
        end if;
    else
        lc_jlMessages.append(json_list('[{error:''Error RUR-0004 There has been an error processing the request''}]'));
    end if;
  

thesql := 'select 
  sgsu.securitygroupid,
  sgsu.group_name,
  ''' || lc_employeeid || ''' employeeid, 
  decode(sgsu.employeeid,null,0,1) as isMember
from
  (
    select 
      securitygroupid,
      group_name,
      (
        select
          employeeid
        from
          ssd_security_users su
        where 
          su.securitygroupid = sg.securitygroupid
          and employeeid = ''' || lc_employeeid || '''
      ) as employeeid
    from 
      ssd_security_groups sg
  ) sgsu
order by
  sgsu.group_name';
  
  lc_jJson := json_dyn.executeObject(thesql);

  return lc_jJson;

exception
    when json_error then
        --htp.p('raised json_error in update func');
        lc_jError := json();
        lc_jError.put('function','BB_F_READ_USER_ROLES');
        lc_jError.put('input',in_jJson.to_char);
        lc_jError.put('messages',lc_jlMessages);
        BB_P_ERROR(lc_jError);
        --htp.p('returning n');
        return json('e');
    when others then
        lc_jError := json();
        lc_jError.put('function','BB_F_READ_USER_ROLES');
        lc_jError.put('input',in_jJson.to_char);
        lc_jlMessages.append(json_list('[{error:''RUR.000X ' || SQLERRM || '''}]'));
        lc_jError.put('messages',lc_jlMessages);
        BB_P_ERROR(lc_jError);
        return json('e');


END BB_F_READ_USER_ROLES;
 

/
