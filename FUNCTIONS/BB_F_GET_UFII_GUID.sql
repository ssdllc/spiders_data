--------------------------------------------------------
--  DDL for Function BB_F_GET_UFII_GUID
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE FUNCTION "SPIDERS_DATA"."BB_F_GET_UFII_GUID" (in_vVal varchar2, in_vVal2 varchar2) return varchar2 as 

lc_vReturn varchar2(255) := 'NONE';
lc_vVal varchar2(255);
begin

lc_vVal := replace(in_vVal,'.','');


if lc_vVal= 'H10100101' and in_vVal2 = 'GDP' then lc_vReturn := 'a588cd03-c0e4-11e4-85cc-5b31063c31ef'; end if;
if lc_vVal= 'H10100101' and in_vVal2 = 'P' then lc_vReturn := 'a588cd01-c0e4-11e4-85cc-5b31063c31ef'; end if;
if lc_vVal= 'H10100101' and in_vVal2 = 'PB' then lc_vReturn := 'a588cd02-c0e4-11e4-85cc-5b31063c31ef'; end if;
if lc_vVal= 'H10100102' and in_vVal2 = 'SP' then lc_vReturn := 'a588f410-c0e4-11e4-85cc-5b31063c31ef'; end if;
if lc_vVal= 'H10100103' and in_vVal2 = 'C' then lc_vReturn := 'a588f411-c0e4-11e4-85cc-5b31063c31ef'; end if;
if lc_vVal= 'H10100104' and in_vVal2 = 'COF' then lc_vReturn := 'a588f412-c0e4-11e4-85cc-5b31063c31ef'; end if;
if lc_vVal= 'H10100105' and in_vVal2 = 'WRP' then lc_vReturn := 'a588f413-c0e4-11e4-85cc-5b31063c31ef'; end if;
if lc_vVal= 'H10100106' and in_vVal2 = 'ENC' then lc_vReturn := 'a588f414-c0e4-11e4-85cc-5b31063c31ef'; end if;
if lc_vVal= 'H10100107' and in_vVal2 = 'CTG' then lc_vReturn := 'a5891b20-c0e4-11e4-85cc-5b31063c31ef'; end if;
if lc_vVal= 'H10100108' and in_vVal2 = 'OPFE' then lc_vReturn := 'a5891b21-c0e4-11e4-85cc-5b31063c31ef'; end if;
if lc_vVal= 'H101002' and in_vVal2 = 'PC' then lc_vReturn := 'a5891b22-c0e4-11e4-85cc-5b31063c31ef'; end if;
if lc_vVal= 'H10100301' and in_vVal2 = 'SP' then lc_vReturn := 'a5894230-c0e4-11e4-85cc-5b31063c31ef'; end if;
if lc_vVal= 'H10100302' and in_vVal2 = 'TBA' then lc_vReturn := 'a5894231-c0e4-11e4-85cc-5b31063c31ef'; end if;
if lc_vVal= 'H10100303' and in_vVal2 = 'TR' then lc_vReturn := 'a5894232-c0e4-11e4-85cc-5b31063c31ef'; end if;
if lc_vVal= 'H10100304' and in_vVal2 = 'CGW' then lc_vReturn := 'a5894233-c0e4-11e4-85cc-5b31063c31ef'; end if;
if lc_vVal= 'H10100305' and in_vVal2 = 'BW' then lc_vReturn := 'a5894234-c0e4-11e4-85cc-5b31063c31ef'; end if;
if lc_vVal= 'H10100306' and in_vVal2 = 'C' then lc_vReturn := 'a5896940-c0e4-11e4-85cc-5b31063c31ef'; end if;
if lc_vVal= 'H10100307' and in_vVal2 = 'CRB' then lc_vReturn := 'a5896941-c0e4-11e4-85cc-5b31063c31ef'; end if;
if lc_vVal= 'H10100308' and in_vVal2 = 'OQE' then lc_vReturn := 'a5896942-c0e4-11e4-85cc-5b31063c31ef'; end if;
if lc_vVal= 'H10100401' and in_vVal2 = 'P' then lc_vReturn := 'a5896944-c0e4-11e4-85cc-5b31063c31ef'; end if;
if lc_vVal= 'H10100402' and in_vVal2 = 'PC' then lc_vReturn := 'a5899050-c0e4-11e4-85cc-5b31063c31ef'; end if;
if lc_vVal= 'H10100403' and in_vVal2 = 'DA' then lc_vReturn := 'a5899051-c0e4-11e4-85cc-5b31063c31ef'; end if;
if lc_vVal= 'H10100403' and in_vVal2 = 'DU' then lc_vReturn := 'a5899052-c0e4-11e4-85cc-5b31063c31ef'; end if;
if lc_vVal= 'H101005' and in_vVal2 = 'REV' then lc_vReturn := 'a589b760-c0e4-11e4-85cc-5b31063c31ef'; end if;
if lc_vVal= 'H101006' and in_vVal2 = 'SW' then lc_vReturn := 'a589b761-c0e4-11e4-85cc-5b31063c31ef'; end if;
if lc_vVal= 'H101007' and in_vVal2 = 'RMP' then lc_vReturn := 'a589b762-c0e4-11e4-85cc-5b31063c31ef'; end if;
if lc_vVal= 'H101008' and in_vVal2 = 'COW' then lc_vReturn := 'a589b763-c0e4-11e4-85cc-5b31063c31ef'; end if;
if lc_vVal= 'H10100901' and in_vVal2 = 'FW' then lc_vReturn := 'a589de70-c0e4-11e4-85cc-5b31063c31ef'; end if;
if lc_vVal= 'H10100902' and in_vVal2 = 'WAP' then lc_vReturn := 'a589de71-c0e4-11e4-85cc-5b31063c31ef'; end if;
if lc_vVal= 'H10100903' and in_vVal2 = 'OWP' then lc_vReturn := 'a589de72-c0e4-11e4-85cc-5b31063c31ef'; end if;
if lc_vVal= 'H10100904' and in_vVal2 = 'WWL' then lc_vReturn := 'a589de73-c0e4-11e4-85cc-5b31063c31ef'; end if;
if lc_vVal= 'H10100905' and in_vVal2 = 'WST' then lc_vReturn := 'a589de74-c0e4-11e4-85cc-5b31063c31ef'; end if;
if lc_vVal= 'H10100906' and in_vVal2 = 'P' then lc_vReturn := 'a58a0580-c0e4-11e4-85cc-5b31063c31ef'; end if;
if lc_vVal= 'H101010' and in_vVal2 = 'APR' then lc_vReturn := 'a58a0581-c0e4-11e4-85cc-5b31063c31ef'; end if;
if lc_vVal= 'H101010' and in_vVal2 = 'BKF' then lc_vReturn := 'a58a0582-c0e4-11e4-85cc-5b31063c31ef'; end if;
if lc_vVal= 'H101010' and in_vVal2 = 'PD' then lc_vReturn := 'a58a0583-c0e4-11e4-85cc-5b31063c31ef'; end if;
if lc_vVal= 'H102001' and in_vVal2 = 'B' then lc_vReturn := 'a58a2c90-c0e4-11e4-85cc-5b31063c31ef'; end if;
if lc_vVal= 'H102001' and in_vVal2 = 'BR' then lc_vReturn := 'a58a2c93-c0e4-11e4-85cc-5b31063c31ef'; end if;
if lc_vVal= 'H102001' and in_vVal2 = 'G' then lc_vReturn := 'a58a2c92-c0e4-11e4-85cc-5b31063c31ef'; end if;
if lc_vVal= 'H102001' and in_vVal2 = 'STR' then lc_vReturn := 'a58a2c91-c0e4-11e4-85cc-5b31063c31ef'; end if;
if lc_vVal= 'H102002' and in_vVal2 = 'COL' then lc_vReturn := 'a58a53a0-c0e4-11e4-85cc-5b31063c31ef'; end if;
if lc_vVal= 'H10200301' and in_vVal2 = 'UV' then lc_vReturn := 'a58a53a2-c0e4-11e4-85cc-5b31063c31ef'; end if;
if lc_vVal= 'H10200302' and in_vVal2 = 'UT' then lc_vReturn := 'a58a53a3-c0e4-11e4-85cc-5b31063c31ef'; end if;
if lc_vVal= 'H10200303' and in_vVal2 = 'ENCB' then lc_vReturn := 'a58aa1c2-c0e4-11e4-85cc-5b31063c31ef'; end if;
if lc_vVal= 'H10200303' and in_vVal2 = 'MH' then lc_vReturn := 'a58aa1c0-c0e4-11e4-85cc-5b31063c31ef'; end if;
if lc_vVal= 'H10200303' and in_vVal2 = 'STP' then lc_vReturn := 'a58a7ab2-c0e4-11e4-85cc-5b31063c31ef'; end if;
if lc_vVal= 'H10200303' and in_vVal2 = 'UPD' then lc_vReturn := 'a58a7ab3-c0e4-11e4-85cc-5b31063c31ef'; end if;
if lc_vVal= 'H10200303' and in_vVal2 = 'US' then lc_vReturn := 'a58aa1c1-c0e4-11e4-85cc-5b31063c31ef'; end if;
if lc_vVal= 'H10200303' and in_vVal2 = 'UTC' then lc_vReturn := 'a58a7ab1-c0e4-11e4-85cc-5b31063c31ef'; end if;
if lc_vVal= 'H10200303' and in_vVal2 = 'UVC' then lc_vReturn := 'a58a7ab0-c0e4-11e4-85cc-5b31063c31ef'; end if;
if lc_vVal= 'H102004' and in_vVal2 = 'AW' then lc_vReturn := 'a58aa1c3-c0e4-11e4-85cc-5b31063c31ef'; end if;
if lc_vVal= 'H102004' and in_vVal2 = 'FAC' then lc_vReturn := 'a58aa1c4-c0e4-11e4-85cc-5b31063c31ef'; end if;
if lc_vVal= 'H103001' and in_vVal2 = 'D' then lc_vReturn := 'a58ac8d1-c0e4-11e4-85cc-5b31063c31ef'; end if;
if lc_vVal= 'H103001' and in_vVal2 = 'DA' then lc_vReturn := 'a58ac8d2-c0e4-11e4-85cc-5b31063c31ef'; end if;
if lc_vVal= 'H103001' and in_vVal2 = 'DU' then lc_vReturn := 'a58ac8d3-c0e4-11e4-85cc-5b31063c31ef'; end if;
if lc_vVal= 'H103002' and in_vVal2 = 'OGD' then lc_vReturn := 'a58aefe0-c0e4-11e4-85cc-5b31063c31ef'; end if;
if lc_vVal= 'H103003' and in_vVal2 = 'DOV' then lc_vReturn := 'a58aefe1-c0e4-11e4-85cc-5b31063c31ef'; end if;
if lc_vVal= 'H103004' and in_vVal2 = 'BRL' then lc_vReturn := 'a58aefe3-c0e4-11e4-85cc-5b31063c31ef'; end if;
if lc_vVal= 'H103004' and in_vVal2 = 'CB' then lc_vReturn := 'a58aefe2-c0e4-11e4-85cc-5b31063c31ef'; end if;
if lc_vVal= 'H103005' and in_vVal2 = 'MFD' then lc_vReturn := 'a58aefe4-c0e4-11e4-85cc-5b31063c31ef'; end if;
if lc_vVal= 'H103006' and in_vVal2 = 'LFD' then lc_vReturn := 'a58b16f0-c0e4-11e4-85cc-5b31063c31ef'; end if;
if lc_vVal= 'H103007' and in_vVal2 = 'UM' then lc_vReturn := 'a58b16f1-c0e4-11e4-85cc-5b31063c31ef'; end if;
if lc_vVal= 'H103008' and in_vVal2 = 'EJ' then lc_vReturn := 'a58b16f2-c0e4-11e4-85cc-5b31063c31ef'; end if;
if lc_vVal= 'H103009' and in_vVal2 = 'GPT' then lc_vReturn := 'a58b3e00-c0e4-11e4-85cc-5b31063c31ef'; end if;
if lc_vVal= 'H103009' and in_vVal2 = 'GRL' then lc_vReturn := 'a58b16f3-c0e4-11e4-85cc-5b31063c31ef'; end if;
if lc_vVal= 'H103009' and in_vVal2 = 'HRL' then lc_vReturn := 'a58b3e01-c0e4-11e4-85cc-5b31063c31ef'; end if;
if lc_vVal= 'H10301001' and in_vVal2 = 'DST' then lc_vReturn := 'a58b3e03-c0e4-11e4-85cc-5b31063c31ef'; end if;
if lc_vVal= 'H10301002' and in_vVal2 = 'CBST' then lc_vReturn := 'a58b3e04-c0e4-11e4-85cc-5b31063c31ef'; end if;
if lc_vVal= 'H103011' and in_vVal2 = 'CRL' then lc_vReturn := 'a58b6510-c0e4-11e4-85cc-5b31063c31ef'; end if;
if lc_vVal= 'H103011' and in_vVal2 = 'CRS' then lc_vReturn := 'a58b6511-c0e4-11e4-85cc-5b31063c31ef'; end if;
if lc_vVal= 'H103011' and in_vVal2 = 'DRN' then lc_vReturn := 'a58b6512-c0e4-11e4-85cc-5b31063c31ef'; end if;
if lc_vVal= 'H103011' and in_vVal2 = 'GRT' then lc_vReturn := 'a58b8c20-c0e4-11e4-85cc-5b31063c31ef'; end if;
if lc_vVal= 'H103011' and in_vVal2 = 'TRL' then lc_vReturn := 'a58b6513-c0e4-11e4-85cc-5b31063c31ef'; end if;
if lc_vVal= 'H10400101' and in_vVal2 = 'FP' then lc_vReturn := 'a58b8c23-c0e4-11e4-85cc-5b31063c31ef'; end if;
if lc_vVal= 'H10400101' and in_vVal2 = 'FPC' then lc_vReturn := 'a58bb330-c0e4-11e4-85cc-5b31063c31ef'; end if;
if lc_vVal= 'H10400102' and in_vVal2 = 'FCH' then lc_vReturn := 'a58cc4a0-c0e4-11e4-85cc-5b31063c31ef'; end if;
if lc_vVal= 'H10400102' and in_vVal2 = 'FF' then lc_vReturn := 'a58c9d90-c0e4-11e4-85cc-5b31063c31ef'; end if;
if lc_vVal= 'H10400102' and in_vVal2 = 'FWL' then lc_vReturn := 'a58cc4a1-c0e4-11e4-85cc-5b31063c31ef'; end if;
if lc_vVal= 'H10400103' and in_vVal2 = 'EFND' then lc_vReturn := 'a58cc4a2-c0e4-11e4-85cc-5b31063c31ef'; end if;
if lc_vVal= 'H10400104' and in_vVal2 = 'MFND' then lc_vReturn := 'a58cc4a3-c0e4-11e4-85cc-5b31063c31ef'; end if;
if lc_vVal= 'H10400105' and in_vVal2 = 'FFF' then lc_vReturn := 'a58cc4a4-c0e4-11e4-85cc-5b31063c31ef'; end if;
if lc_vVal= 'H10400106' and in_vVal2 = 'HPF' then lc_vReturn := 'a58cebb0-c0e4-11e4-85cc-5b31063c31ef'; end if;
if lc_vVal= 'H10400107' and in_vVal2 = 'PF' then lc_vReturn := 'a58cebb1-c0e4-11e4-85cc-5b31063c31ef'; end if;
if lc_vVal= 'H10400108' and in_vVal2 = 'CML' then lc_vReturn := 'a58cebb2-c0e4-11e4-85cc-5b31063c31ef'; end if;
if lc_vVal= 'H10400109' and in_vVal2 = 'SEP' then lc_vReturn := 'a58cebb3-c0e4-11e4-85cc-5b31063c31ef'; end if;
if lc_vVal= 'H10400110' and in_vVal2 = 'CHN' then lc_vReturn := 'a58d12c0-c0e4-11e4-85cc-5b31063c31ef'; end if;
if lc_vVal= 'H10400110' and in_vVal2 = 'FPN' then lc_vReturn := 'a58d12c1-c0e4-11e4-85cc-5b31063c31ef'; end if;
if lc_vVal= 'H10400201' and in_vVal2 = 'FP' then lc_vReturn := 'a58d12c3-c0e4-11e4-85cc-5b31063c31ef'; end if;
if lc_vVal= 'H10400201' and in_vVal2 = 'FPC' then lc_vReturn := 'a58d12c4-c0e4-11e4-85cc-5b31063c31ef'; end if;
if lc_vVal= 'H10400202' and in_vVal2 = 'FCH' then lc_vReturn := 'a58d39d0-c0e4-11e4-85cc-5b31063c31ef'; end if;
if lc_vVal= 'H10400202' and in_vVal2 = 'FF' then lc_vReturn := 'a58d12c5-c0e4-11e4-85cc-5b31063c31ef'; end if;
if lc_vVal= 'H10400202' and in_vVal2 = 'FWL' then lc_vReturn := 'a58d39d1-c0e4-11e4-85cc-5b31063c31ef'; end if;
if lc_vVal= 'H10400203' and in_vVal2 = 'EFND' then lc_vReturn := 'a58d39d2-c0e4-11e4-85cc-5b31063c31ef'; end if;
if lc_vVal= 'H10400204' and in_vVal2 = 'MFND' then lc_vReturn := 'a58d60e0-c0e4-11e4-85cc-5b31063c31ef'; end if;
if lc_vVal= 'H10400205' and in_vVal2 = 'CML' then lc_vReturn := 'a58d60e1-c0e4-11e4-85cc-5b31063c31ef'; end if;
if lc_vVal= 'H10400301' and in_vVal2 = 'FP' then lc_vReturn := 'a58d60e3-c0e4-11e4-85cc-5b31063c31ef'; end if;
if lc_vVal= 'H10400302' and in_vVal2 = 'FCH' then lc_vReturn := 'a58d60e4-c0e4-11e4-85cc-5b31063c31ef'; end if;
if lc_vVal= 'H10400302' and in_vVal2 = 'FWL' then lc_vReturn := 'a58d87f0-c0e4-11e4-85cc-5b31063c31ef'; end if;
if lc_vVal= 'H10400303' and in_vVal2 = 'EFND' then lc_vReturn := 'a58d87f1-c0e4-11e4-85cc-5b31063c31ef'; end if;
if lc_vVal= 'H10400304' and in_vVal2 = 'MFND' then lc_vReturn := 'a58d87f2-c0e4-11e4-85cc-5b31063c31ef'; end if;
if lc_vVal= 'H10400401' and in_vVal2 = 'FP' then lc_vReturn := 'a58d87f4-c0e4-11e4-85cc-5b31063c31ef'; end if;
if lc_vVal= 'H10400402' and in_vVal2 = 'WRP' then lc_vReturn := 'a58daf00-c0e4-11e4-85cc-5b31063c31ef'; end if;
if lc_vVal= 'H10400501' and in_vVal2 = 'MF' then lc_vReturn := 'a58daf02-c0e4-11e4-85cc-5b31063c31ef'; end if;
if lc_vVal= 'H10400502' and in_vVal2 = 'MF' then lc_vReturn := 'a58daf03-c0e4-11e4-85cc-5b31063c31ef'; end if;
if lc_vVal= 'H10400503' and in_vVal2 = 'MF' then lc_vReturn := 'a58daf04-c0e4-11e4-85cc-5b31063c31ef'; end if;
if lc_vVal= 'H10400504' and in_vVal2 = 'MF' then lc_vReturn := 'a58dd610-c0e4-11e4-85cc-5b31063c31ef'; end if;
if lc_vVal= 'H10400505' and in_vVal2 = 'MF' then lc_vReturn := 'a58dd611-c0e4-11e4-85cc-5b31063c31ef'; end if;
if lc_vVal= 'H104006' and in_vVal2 = 'MF' then lc_vReturn := 'a58dd613-c0e4-11e4-85cc-5b31063c31ef'; end if;
if lc_vVal= 'H104006' and in_vVal2 = 'OMBC' then lc_vReturn := 'a58dd614-c0e4-11e4-85cc-5b31063c31ef'; end if;
if lc_vVal= 'H104006' and in_vVal2 = 'RC' then lc_vReturn := 'a58dd612-c0e4-11e4-85cc-5b31063c31ef'; end if;
if lc_vVal= 'H105001' and in_vVal2 = 'HRL' then lc_vReturn := 'a58dfd21-c0e4-11e4-85cc-5b31063c31ef'; end if;
if lc_vVal= 'H10500201' and in_vVal2 = 'GW' then lc_vReturn := 'a58dfd23-c0e4-11e4-85cc-5b31063c31ef'; end if;
if lc_vVal= 'H10500202' and in_vVal2 = 'WPLT' then lc_vReturn := 'a58dfd24-c0e4-11e4-85cc-5b31063c31ef'; end if;
if lc_vVal= 'H10500203' and in_vVal2 = 'GWP' then lc_vReturn := 'a58e2430-c0e4-11e4-85cc-5b31063c31ef'; end if;
if lc_vVal= 'H10500204' and in_vVal2 = 'RGD' then lc_vReturn := 'a58e2431-c0e4-11e4-85cc-5b31063c31ef'; end if;
if lc_vVal= 'H10500205' and in_vVal2 = 'GWR' then lc_vReturn := 'a58e2432-c0e4-11e4-85cc-5b31063c31ef'; end if;
if lc_vVal= 'H105003' and in_vVal2 = 'CBL' then lc_vReturn := 'a58e2433-c0e4-11e4-85cc-5b31063c31ef'; end if;
if lc_vVal= 'H105004' and in_vVal2 = 'FD' then lc_vReturn := 'a58e4b40-c0e4-11e4-85cc-5b31063c31ef'; end if;
if lc_vVal= 'H10500401' and in_vVal2 = 'FLT' then lc_vReturn := 'a58e4b41-c0e4-11e4-85cc-5b31063c31ef'; end if;
if lc_vVal= 'H10500402' and in_vVal2 = 'WL' then lc_vReturn := 'a58e4b42-c0e4-11e4-85cc-5b31063c31ef'; end if;
if lc_vVal= 'H10500403' and in_vVal2 = 'TR' then lc_vReturn := 'a58e4b43-c0e4-11e4-85cc-5b31063c31ef'; end if;
if lc_vVal= 'H10500404' and in_vVal2 = 'MF' then lc_vReturn := 'a58e7250-c0e4-11e4-85cc-5b31063c31ef'; end if;
if lc_vVal= 'H10500405' and in_vVal2 = 'FND' then lc_vReturn := 'a58e7251-c0e4-11e4-85cc-5b31063c31ef'; end if;
if lc_vVal= 'H10500406' and in_vVal2 = 'GPA' then lc_vReturn := 'a58e7252-c0e4-11e4-85cc-5b31063c31ef'; end if;
if lc_vVal= 'H10500407' and in_vVal2 = 'TPLT' then lc_vReturn := 'a58e7253-c0e4-11e4-85cc-5b31063c31ef'; end if;
if lc_vVal= 'H10500408' and in_vVal2 = 'CNH' then lc_vReturn := 'a58e9960-c0e4-11e4-85cc-5b31063c31ef'; end if;
if lc_vVal= 'H10500409' and in_vVal2 = 'As Needed' then lc_vReturn := 'a58e9961-c0e4-11e4-85cc-5b31063c31ef'; end if;
if lc_vVal= 'H105005' and in_vVal2 = 'LAD' then lc_vReturn := 'a58e9962-c0e4-11e4-85cc-5b31063c31ef'; end if;
if lc_vVal= 'H105006' and in_vVal2 = 'LRH' then lc_vReturn := 'a58e9963-c0e4-11e4-85cc-5b31063c31ef'; end if;
if lc_vVal= 'H105007' and in_vVal2 = 'OCB' then lc_vReturn := 'a58e9964-c0e4-11e4-85cc-5b31063c31ef'; end if;
if lc_vVal= 'H105008' and in_vVal2 = 'STS' then lc_vReturn := 'a58ec070-c0e4-11e4-85cc-5b31063c31ef'; end if;
if lc_vVal= 'H301001' and in_vVal2 = 'WPA' then lc_vReturn := 'a58ec073-c0e4-11e4-85cc-5b31063c31ef'; end if;
if lc_vVal= 'H301002' and in_vVal2 = 'BRW' then lc_vReturn := 'a58ec074-c0e4-11e4-85cc-5b31063c31ef'; end if;
if lc_vVal= 'H302001' and in_vVal2 = 'RR' then lc_vReturn := 'a58ee781-c0e4-11e4-85cc-5b31063c31ef'; end if;
if lc_vVal= 'H302002' and in_vVal2 = 'GFR' then lc_vReturn := 'a58ee782-c0e4-11e4-85cc-5b31063c31ef'; end if;
if lc_vVal= 'H302003' and in_vVal2 = 'CFR' then lc_vReturn := 'a58ee783-c0e4-11e4-85cc-5b31063c31ef'; end if;
if lc_vVal= 'H50100101' and in_vVal2 = 'DPPW' then lc_vReturn := 'a58f0e93-c0e4-11e4-85cc-5b31063c31ef'; end if;
if lc_vVal= 'H50100101' and in_vVal2 = 'LPW' then lc_vReturn := 'a58f0e92-c0e4-11e4-85cc-5b31063c31ef'; end if;
if lc_vVal= 'H50100102' and in_vVal2 = 'HPW' then lc_vReturn := 'a58f0e94-c0e4-11e4-85cc-5b31063c31ef'; end if;
if lc_vVal= 'H50100102' and in_vVal2 = 'TPW' then lc_vReturn := 'a58f0e95-c0e4-11e4-85cc-5b31063c31ef'; end if;
if lc_vVal= 'H50100103' and in_vVal2 = 'BVPW' then lc_vReturn := 'a58f35a1-c0e4-11e4-85cc-5b31063c31ef'; end if;
if lc_vVal= 'H50100103' and in_vVal2 = 'CVPW' then lc_vReturn := 'a58f35a3-c0e4-11e4-85cc-5b31063c31ef'; end if;
if lc_vVal= 'H50100103' and in_vVal2 = 'DNPW' then lc_vReturn := 'a58f5cb0-c0e4-11e4-85cc-5b31063c31ef'; end if;
if lc_vVal= 'H50100103' and in_vVal2 = 'GVPW' then lc_vReturn := 'a58f35a2-c0e4-11e4-85cc-5b31063c31ef'; end if;
if lc_vVal= 'H50100103' and in_vVal2 = 'IVPW' then lc_vReturn := 'a58f5cb1-c0e4-11e4-85cc-5b31063c31ef'; end if;
if lc_vVal= 'H50100103' and in_vVal2 = 'VPW' then lc_vReturn := 'a58f35a0-c0e4-11e4-85cc-5b31063c31ef'; end if;
if lc_vVal= 'H50100103' and in_vVal2 = 'VTPW' then lc_vReturn := 'a58f35a4-c0e4-11e4-85cc-5b31063c31ef'; end if;
if lc_vVal= 'H50100104' and in_vVal2 = 'RPPW' then lc_vReturn := 'a58f5cb3-c0e4-11e4-85cc-5b31063c31ef'; end if;
if lc_vVal= 'H50100104' and in_vVal2 = 'RPW' then lc_vReturn := 'a58f5cb2-c0e4-11e4-85cc-5b31063c31ef'; end if;
if lc_vVal= 'H50100105' and in_vVal2 = 'CNPW' then lc_vReturn := 'a58f5cb4-c0e4-11e4-85cc-5b31063c31ef'; end if;
if lc_vVal= 'H50100106' and in_vVal2 = 'IPW' then lc_vReturn := 'a58f5cb5-c0e4-11e4-85cc-5b31063c31ef'; end if;
if lc_vVal= 'H50100107' and in_vVal2 = 'MPW' then lc_vReturn := 'a58f83c0-c0e4-11e4-85cc-5b31063c31ef'; end if;
if lc_vVal= 'H50100108' and in_vVal2 = 'GPW' then lc_vReturn := 'a58f83c1-c0e4-11e4-85cc-5b31063c31ef'; end if;
if lc_vVal= 'H50100109' and in_vVal2 = 'ECWS' then lc_vReturn := 'a58f83c2-c0e4-11e4-85cc-5b31063c31ef'; end if;
if lc_vVal= 'H50100110' and in_vVal2 = 'BPPW' then lc_vReturn := 'a58f83c3-c0e4-11e4-85cc-5b31063c31ef'; end if;
if lc_vVal= 'H50100110' and in_vVal2 = 'CBPW' then lc_vReturn := 'a58faad2-c0e4-11e4-85cc-5b31063c31ef'; end if;
if lc_vVal= 'H50100110' and in_vVal2 = 'PCPW' then lc_vReturn := 'a58faad1-c0e4-11e4-85cc-5b31063c31ef'; end if;
if lc_vVal= 'H50100110' and in_vVal2 = 'USPW' then lc_vReturn := 'a58faad0-c0e4-11e4-85cc-5b31063c31ef'; end if;
if lc_vVal= 'H50100201' and in_vVal2 = 'LSW' then lc_vReturn := 'a58fd1e0-c0e4-11e4-85cc-5b31063c31ef'; end if;
if lc_vVal= 'H50100202' and in_vVal2 = 'HSW' then lc_vReturn := 'a58fd1e1-c0e4-11e4-85cc-5b31063c31ef'; end if;
if lc_vVal= 'H50100202' and in_vVal2 = 'TSW' then lc_vReturn := 'a58fd1e2-c0e4-11e4-85cc-5b31063c31ef'; end if;
if lc_vVal= 'H50100203' and in_vVal2 = 'BVSW' then lc_vReturn := 'a58fd1e4-c0e4-11e4-85cc-5b31063c31ef'; end if;
if lc_vVal= 'H50100203' and in_vVal2 = 'CVSW' then lc_vReturn := 'a58ff8f1-c0e4-11e4-85cc-5b31063c31ef'; end if;
if lc_vVal= 'H50100203' and in_vVal2 = 'GVSW' then lc_vReturn := 'a58ff8f0-c0e4-11e4-85cc-5b31063c31ef'; end if;
if lc_vVal= 'H50100203' and in_vVal2 = 'IVSW' then lc_vReturn := 'a5929100-c0e4-11e4-85cc-5b31063c31ef'; end if;
if lc_vVal= 'H50100203' and in_vVal2 = 'VSW' then lc_vReturn := 'a58fd1e3-c0e4-11e4-85cc-5b31063c31ef'; end if;
if lc_vVal= 'H50100203' and in_vVal2 = 'VTSW' then lc_vReturn := 'a58ff8f2-c0e4-11e4-85cc-5b31063c31ef'; end if;
if lc_vVal= 'H50100204' and in_vVal2 = 'RPSW' then lc_vReturn := 'a5929102-c0e4-11e4-85cc-5b31063c31ef'; end if;
if lc_vVal= 'H50100204' and in_vVal2 = 'RSW' then lc_vReturn := 'a5929101-c0e4-11e4-85cc-5b31063c31ef'; end if;
if lc_vVal= 'H50100205' and in_vVal2 = 'CNSW' then lc_vReturn := 'a5929103-c0e4-11e4-85cc-5b31063c31ef'; end if;
if lc_vVal= 'H50100206' and in_vVal2 = 'ISW' then lc_vReturn := 'a592b810-c0e4-11e4-85cc-5b31063c31ef'; end if;
if lc_vVal= 'H50100207' and in_vVal2 = 'MSW' then lc_vReturn := 'a592b811-c0e4-11e4-85cc-5b31063c31ef'; end if;
if lc_vVal= 'H50100208' and in_vVal2 = 'GSW' then lc_vReturn := 'a592b812-c0e4-11e4-85cc-5b31063c31ef'; end if;
if lc_vVal= 'H50100209' and in_vVal2 = 'PESW' then lc_vReturn := 'a592b813-c0e4-11e4-85cc-5b31063c31ef'; end if;
if lc_vVal= 'H50100210' and in_vVal2 = 'CBSW' then lc_vReturn := 'a592b816-c0e4-11e4-85cc-5b31063c31ef'; end if;
if lc_vVal= 'H50100210' and in_vVal2 = 'PCSW' then lc_vReturn := 'a592b815-c0e4-11e4-85cc-5b31063c31ef'; end if;
if lc_vVal= 'H50100210' and in_vVal2 = 'USSW' then lc_vReturn := 'a592b814-c0e4-11e4-85cc-5b31063c31ef'; end if;
if lc_vVal= 'H50100301' and in_vVal2 = 'LSS' then lc_vReturn := 'a592df21-c0e4-11e4-85cc-5b31063c31ef'; end if;
if lc_vVal= 'H50100302' and in_vVal2 = 'HSS' then lc_vReturn := 'a592df22-c0e4-11e4-85cc-5b31063c31ef'; end if;
if lc_vVal= 'H50100302' and in_vVal2 = 'TSS' then lc_vReturn := 'a592df23-c0e4-11e4-85cc-5b31063c31ef'; end if;
if lc_vVal= 'H50100303' and in_vVal2 = 'BVSS' then lc_vReturn := 'a592df25-c0e4-11e4-85cc-5b31063c31ef'; end if;
if lc_vVal= 'H50100303' and in_vVal2 = 'CVSS' then lc_vReturn := 'a5930631-c0e4-11e4-85cc-5b31063c31ef'; end if;
if lc_vVal= 'H50100303' and in_vVal2 = 'GVSS' then lc_vReturn := 'a5930630-c0e4-11e4-85cc-5b31063c31ef'; end if;
if lc_vVal= 'H50100303' and in_vVal2 = 'IVSS' then lc_vReturn := 'a5930632-c0e4-11e4-85cc-5b31063c31ef'; end if;
if lc_vVal= 'H50100303' and in_vVal2 = 'VSS' then lc_vReturn := 'a592df24-c0e4-11e4-85cc-5b31063c31ef'; end if;
if lc_vVal= 'H50100304' and in_vVal2 = 'RPSS' then lc_vReturn := 'a5930634-c0e4-11e4-85cc-5b31063c31ef'; end if;
if lc_vVal= 'H50100304' and in_vVal2 = 'RSS' then lc_vReturn := 'a5930633-c0e4-11e4-85cc-5b31063c31ef'; end if;
if lc_vVal= 'H50100305' and in_vVal2 = 'CNSS' then lc_vReturn := 'a5930635-c0e4-11e4-85cc-5b31063c31ef'; end if;
if lc_vVal= 'H50100306' and in_vVal2 = 'ISS' then lc_vReturn := 'a5930636-c0e4-11e4-85cc-5b31063c31ef'; end if;
if lc_vVal= 'H50100307' and in_vVal2 = 'MSS' then lc_vReturn := 'a5932d40-c0e4-11e4-85cc-5b31063c31ef'; end if;
if lc_vVal= 'H50100308' and in_vVal2 = 'PESS' then lc_vReturn := 'a5932d41-c0e4-11e4-85cc-5b31063c31ef'; end if;
if lc_vVal= 'H50100309' and in_vVal2 = 'CBSS' then lc_vReturn := 'a5932d44-c0e4-11e4-85cc-5b31063c31ef'; end if;
if lc_vVal= 'H50100309' and in_vVal2 = 'PCSS' then lc_vReturn := 'a5932d43-c0e4-11e4-85cc-5b31063c31ef'; end if;
if lc_vVal= 'H50100309' and in_vVal2 = 'USSS' then lc_vReturn := 'a5932d42-c0e4-11e4-85cc-5b31063c31ef'; end if;
if lc_vVal= 'H50100401' and in_vVal2 = 'LOW' then lc_vReturn := 'a5935450-c0e4-11e4-85cc-5b31063c31ef'; end if;
if lc_vVal= 'H50100402' and in_vVal2 = 'HOW' then lc_vReturn := 'a5935451-c0e4-11e4-85cc-5b31063c31ef'; end if;
if lc_vVal= 'H50100402' and in_vVal2 = 'TOW' then lc_vReturn := 'a5935452-c0e4-11e4-85cc-5b31063c31ef'; end if;
if lc_vVal= 'H50100403' and in_vVal2 = 'BVOW' then lc_vReturn := 'a5935454-c0e4-11e4-85cc-5b31063c31ef'; end if;
if lc_vVal= 'H50100403' and in_vVal2 = 'CVOW' then lc_vReturn := 'a5935456-c0e4-11e4-85cc-5b31063c31ef'; end if;
if lc_vVal= 'H50100403' and in_vVal2 = 'GVOW' then lc_vReturn := 'a5935455-c0e4-11e4-85cc-5b31063c31ef'; end if;
if lc_vVal= 'H50100403' and in_vVal2 = 'IVOW' then lc_vReturn := 'a5937b60-c0e4-11e4-85cc-5b31063c31ef'; end if;
if lc_vVal= 'H50100403' and in_vVal2 = 'VOW' then lc_vReturn := 'a5935453-c0e4-11e4-85cc-5b31063c31ef'; end if;
if lc_vVal= 'H50100404' and in_vVal2 = 'ROW' then lc_vReturn := 'a5937b61-c0e4-11e4-85cc-5b31063c31ef'; end if;
if lc_vVal= 'H50100404' and in_vVal2 = 'RPOW' then lc_vReturn := 'a5937b62-c0e4-11e4-85cc-5b31063c31ef'; end if;
if lc_vVal= 'H50100405' and in_vVal2 = 'CNOW' then lc_vReturn := 'a5937b63-c0e4-11e4-85cc-5b31063c31ef'; end if;
if lc_vVal= 'H50100406' and in_vVal2 = 'IOW' then lc_vReturn := 'a5937b64-c0e4-11e4-85cc-5b31063c31ef'; end if;
if lc_vVal= 'H50100407' and in_vVal2 = 'MOW' then lc_vReturn := 'a5937b65-c0e4-11e4-85cc-5b31063c31ef'; end if;
if lc_vVal= 'H50100408' and in_vVal2 = 'PEOW' then lc_vReturn := 'a5937b66-c0e4-11e4-85cc-5b31063c31ef'; end if;
if lc_vVal= 'H50100409' and in_vVal2 = 'CBOW' then lc_vReturn := 'a593a272-c0e4-11e4-85cc-5b31063c31ef'; end if;
if lc_vVal= 'H50100409' and in_vVal2 = 'PCOW' then lc_vReturn := 'a593a271-c0e4-11e4-85cc-5b31063c31ef'; end if;
if lc_vVal= 'H50100409' and in_vVal2 = 'USOW' then lc_vReturn := 'a593a270-c0e4-11e4-85cc-5b31063c31ef'; end if;
if lc_vVal= 'H50100501' and in_vVal2 = 'LST' then lc_vReturn := 'a593a274-c0e4-11e4-85cc-5b31063c31ef'; end if;
if lc_vVal= 'H50100502' and in_vVal2 = 'HST' then lc_vReturn := 'a593a275-c0e4-11e4-85cc-5b31063c31ef'; end if;
if lc_vVal= 'H50100502' and in_vVal2 = 'TST' then lc_vReturn := 'a593c980-c0e4-11e4-85cc-5b31063c31ef'; end if;
if lc_vVal= 'H50100503' and in_vVal2 = 'BFST' then lc_vReturn := 'a593c983-c0e4-11e4-85cc-5b31063c31ef'; end if;
if lc_vVal= 'H50100503' and in_vVal2 = 'BVST' then lc_vReturn := 'a593c982-c0e4-11e4-85cc-5b31063c31ef'; end if;
if lc_vVal= 'H50100503' and in_vVal2 = 'CVST' then lc_vReturn := 'a593c985-c0e4-11e4-85cc-5b31063c31ef'; end if;
if lc_vVal= 'H50100503' and in_vVal2 = 'GVST' then lc_vReturn := 'a593c984-c0e4-11e4-85cc-5b31063c31ef'; end if;
if lc_vVal= 'H50100503' and in_vVal2 = 'IVST' then lc_vReturn := 'a593f090-c0e4-11e4-85cc-5b31063c31ef'; end if;
if lc_vVal= 'H50100503' and in_vVal2 = 'VST' then lc_vReturn := 'a593c981-c0e4-11e4-85cc-5b31063c31ef'; end if;
if lc_vVal= 'H50100503' and in_vVal2 = 'VTST' then lc_vReturn := 'a593f091-c0e4-11e4-85cc-5b31063c31ef'; end if;
if lc_vVal= 'H50100504' and in_vVal2 = 'RPST' then lc_vReturn := 'a593f093-c0e4-11e4-85cc-5b31063c31ef'; end if;
if lc_vVal= 'H50100504' and in_vVal2 = 'RST' then lc_vReturn := 'a593f092-c0e4-11e4-85cc-5b31063c31ef'; end if;
if lc_vVal= 'H50100505' and in_vVal2 = 'CNST' then lc_vReturn := 'a593f094-c0e4-11e4-85cc-5b31063c31ef'; end if;
if lc_vVal= 'H50100506' and in_vVal2 = 'IST' then lc_vReturn := 'a593f095-c0e4-11e4-85cc-5b31063c31ef'; end if;
if lc_vVal= 'H50100507' and in_vVal2 = 'MST' then lc_vReturn := 'a593f096-c0e4-11e4-85cc-5b31063c31ef'; end if;
if lc_vVal= 'H50100508' and in_vVal2 = 'PCST' then lc_vReturn := 'a59417a1-c0e4-11e4-85cc-5b31063c31ef'; end if;
if lc_vVal= 'H50100508' and in_vVal2 = 'USST' then lc_vReturn := 'a59417a0-c0e4-11e4-85cc-5b31063c31ef'; end if;
if lc_vVal= 'H50100601' and in_vVal2 = 'LCA' then lc_vReturn := 'a59417a3-c0e4-11e4-85cc-5b31063c31ef'; end if;
if lc_vVal= 'H50100602' and in_vVal2 = 'HCA' then lc_vReturn := 'a59417a4-c0e4-11e4-85cc-5b31063c31ef'; end if;
if lc_vVal= 'H50100602' and in_vVal2 = 'TCA' then lc_vReturn := 'a59417a5-c0e4-11e4-85cc-5b31063c31ef'; end if;
if lc_vVal= 'H50100603' and in_vVal2 = 'BVCA' then lc_vReturn := 'a5943eb1-c0e4-11e4-85cc-5b31063c31ef'; end if;
if lc_vVal= 'H50100603' and in_vVal2 = 'CVCA' then lc_vReturn := 'a5943eb3-c0e4-11e4-85cc-5b31063c31ef'; end if;
if lc_vVal= 'H50100603' and in_vVal2 = 'GVCA' then lc_vReturn := 'a5943eb2-c0e4-11e4-85cc-5b31063c31ef'; end if;
if lc_vVal= 'H50100603' and in_vVal2 = 'IVCA' then lc_vReturn := 'a5943eb4-c0e4-11e4-85cc-5b31063c31ef'; end if;
if lc_vVal= 'H50100603' and in_vVal2 = 'VCA' then lc_vReturn := 'a5943eb0-c0e4-11e4-85cc-5b31063c31ef'; end if;
if lc_vVal= 'H50100603' and in_vVal2 = 'VTCA' then lc_vReturn := 'a5943eb5-c0e4-11e4-85cc-5b31063c31ef'; end if;
if lc_vVal= 'H50100604' and in_vVal2 = 'RCA' then lc_vReturn := 'a5943eb6-c0e4-11e4-85cc-5b31063c31ef'; end if;
if lc_vVal= 'H50100604' and in_vVal2 = 'RPCA' then lc_vReturn := 'a59465c0-c0e4-11e4-85cc-5b31063c31ef'; end if;
if lc_vVal= 'H50100605' and in_vVal2 = 'CNCA' then lc_vReturn := 'a59465c1-c0e4-11e4-85cc-5b31063c31ef'; end if;
if lc_vVal= 'H50100606' and in_vVal2 = 'MCA' then lc_vReturn := 'a59465c2-c0e4-11e4-85cc-5b31063c31ef'; end if;
if lc_vVal= 'H50100607' and in_vVal2 = 'PCCA' then lc_vReturn := 'a59465c4-c0e4-11e4-85cc-5b31063c31ef'; end if;
if lc_vVal= 'H50100607' and in_vVal2 = 'USCA' then lc_vReturn := 'a59465c3-c0e4-11e4-85cc-5b31063c31ef'; end if;
if lc_vVal= 'H50100701' and in_vVal2 = 'LDS' then lc_vReturn := 'a5948cd0-c0e4-11e4-85cc-5b31063c31ef'; end if;
if lc_vVal= 'H50100701' and in_vVal2 = 'LFL' then lc_vReturn := 'a59465c6-c0e4-11e4-85cc-5b31063c31ef'; end if;
if lc_vVal= 'H50100701' and in_vVal2 = 'LJP' then lc_vReturn := 'a5948cd1-c0e4-11e4-85cc-5b31063c31ef'; end if;
if lc_vVal= 'H50100701' and in_vVal2 = 'RDS' then lc_vReturn := 'a5948cd2-c0e4-11e4-85cc-5b31063c31ef'; end if;
if lc_vVal= 'H50100701' and in_vVal2 = 'RJP' then lc_vReturn := 'a5948cd3-c0e4-11e4-85cc-5b31063c31ef'; end if;
if lc_vVal= 'H50100701' and in_vVal2 = 'RPDS' then lc_vReturn := 'a5948cd4-c0e4-11e4-85cc-5b31063c31ef'; end if;
if lc_vVal= 'H50100701' and in_vVal2 = 'RPJP' then lc_vReturn := 'a5948cd5-c0e4-11e4-85cc-5b31063c31ef'; end if;
if lc_vVal= 'H50100702' and in_vVal2 = 'HDS' then lc_vReturn := 'a594b3e2-c0e4-11e4-85cc-5b31063c31ef'; end if;
if lc_vVal= 'H50100702' and in_vVal2 = 'HFL' then lc_vReturn := 'a594b3e0-c0e4-11e4-85cc-5b31063c31ef'; end if;
if lc_vVal= 'H50100702' and in_vVal2 = 'HJP' then lc_vReturn := 'a594b3e4-c0e4-11e4-85cc-5b31063c31ef'; end if;
if lc_vVal= 'H50100702' and in_vVal2 = 'TDS' then lc_vReturn := 'a594b3e3-c0e4-11e4-85cc-5b31063c31ef'; end if;
if lc_vVal= 'H50100702' and in_vVal2 = 'TFL' then lc_vReturn := 'a594b3e1-c0e4-11e4-85cc-5b31063c31ef'; end if;
if lc_vVal= 'H50100702' and in_vVal2 = 'TJP' then lc_vReturn := 'a594b3e5-c0e4-11e4-85cc-5b31063c31ef'; end if;
if lc_vVal= 'H50100703' and in_vVal2 = 'BVDS' then lc_vReturn := 'a594daf5-c0e4-11e4-85cc-5b31063c31ef'; end if;
if lc_vVal= 'H50100703' and in_vVal2 = 'BVFL' then lc_vReturn := 'a594daf0-c0e4-11e4-85cc-5b31063c31ef'; end if;
if lc_vVal= 'H50100703' and in_vVal2 = 'BVJP' then lc_vReturn := 'a5950204-c0e4-11e4-85cc-5b31063c31ef'; end if;
if lc_vVal= 'H50100703' and in_vVal2 = 'CVDS' then lc_vReturn := 'a5950201-c0e4-11e4-85cc-5b31063c31ef'; end if;
if lc_vVal= 'H50100703' and in_vVal2 = 'CVFL' then lc_vReturn := 'a594daf2-c0e4-11e4-85cc-5b31063c31ef'; end if;
if lc_vVal= 'H50100703' and in_vVal2 = 'CVJP' then lc_vReturn := 'a5950206-c0e4-11e4-85cc-5b31063c31ef'; end if;
if lc_vVal= 'H50100703' and in_vVal2 = 'GVDS' then lc_vReturn := 'a5950200-c0e4-11e4-85cc-5b31063c31ef'; end if;
if lc_vVal= 'H50100703' and in_vVal2 = 'GVFL' then lc_vReturn := 'a594daf1-c0e4-11e4-85cc-5b31063c31ef'; end if;
if lc_vVal= 'H50100703' and in_vVal2 = 'GVJP' then lc_vReturn := 'a5950205-c0e4-11e4-85cc-5b31063c31ef'; end if;
if lc_vVal= 'H50100703' and in_vVal2 = 'IVDS' then lc_vReturn := 'a5950202-c0e4-11e4-85cc-5b31063c31ef'; end if;
if lc_vVal= 'H50100703' and in_vVal2 = 'IVFL' then lc_vReturn := 'a594daf3-c0e4-11e4-85cc-5b31063c31ef'; end if;
if lc_vVal= 'H50100703' and in_vVal2 = 'IVJP' then lc_vReturn := 'a5952910-c0e4-11e4-85cc-5b31063c31ef'; end if;
if lc_vVal= 'H50100703' and in_vVal2 = 'VDS' then lc_vReturn := 'a594daf4-c0e4-11e4-85cc-5b31063c31ef'; end if;
if lc_vVal= 'H50100703' and in_vVal2 = 'VFL' then lc_vReturn := 'a594b3e6-c0e4-11e4-85cc-5b31063c31ef'; end if;
if lc_vVal= 'H50100703' and in_vVal2 = 'VJP' then lc_vReturn := 'a5950203-c0e4-11e4-85cc-5b31063c31ef'; end if;
if lc_vVal= 'H50100704' and in_vVal2 = 'FHDS' then lc_vReturn := 'a5952912-c0e4-11e4-85cc-5b31063c31ef'; end if;
if lc_vVal= 'H50100704' and in_vVal2 = 'FHFL' then lc_vReturn := 'a5952911-c0e4-11e4-85cc-5b31063c31ef'; end if;
if lc_vVal= 'H50100704' and in_vVal2 = 'FHJP' then lc_vReturn := 'a5952913-c0e4-11e4-85cc-5b31063c31ef'; end if;
if lc_vVal= 'H50100705' and in_vVal2 = 'LADS' then lc_vReturn := 'a5952915-c0e4-11e4-85cc-5b31063c31ef'; end if;
if lc_vVal= 'H50100705' and in_vVal2 = 'LAFL' then lc_vReturn := 'a5952914-c0e4-11e4-85cc-5b31063c31ef'; end if;
if lc_vVal= 'H50100705' and in_vVal2 = 'LAJP' then lc_vReturn := 'a5955020-c0e4-11e4-85cc-5b31063c31ef'; end if;
if lc_vVal= 'H50100706' and in_vVal2 = 'MDS' then lc_vReturn := 'a5955022-c0e4-11e4-85cc-5b31063c31ef'; end if;
if lc_vVal= 'H50100706' and in_vVal2 = 'MFL' then lc_vReturn := 'a5955021-c0e4-11e4-85cc-5b31063c31ef'; end if;
if lc_vVal= 'H50100706' and in_vVal2 = 'MJP' then lc_vReturn := 'a5955023-c0e4-11e4-85cc-5b31063c31ef'; end if;
if lc_vVal= 'H50100707' and in_vVal2 = 'OFC' then lc_vReturn := 'a5955024-c0e4-11e4-85cc-5b31063c31ef'; end if;
if lc_vVal= 'H50100806' and in_vVal2 = 'OMMC' then lc_vReturn := 'a5955026-c0e4-11e4-85cc-5b31063c31ef'; end if;
if lc_vVal= 'H501009' and in_vVal2 = 'OCMU' then lc_vReturn := 'a5957730-c0e4-11e4-85cc-5b31063c31ef'; end if;
if lc_vVal= 'H501009' and in_vVal2 = 'RUNK' then lc_vReturn := 'a5957731-c0e4-11e4-85cc-5b31063c31ef'; end if;
if lc_vVal= 'H50200101' and in_vVal2 = 'TFR' then lc_vReturn := 'a5957734-c0e4-11e4-85cc-5b31063c31ef'; end if;
if lc_vVal= 'H50200102' and in_vVal2 = 'CBK' then lc_vReturn := 'a5957735-c0e4-11e4-85cc-5b31063c31ef'; end if;
if lc_vVal= 'H50200103' and in_vVal2 = 'LPWR' then lc_vReturn := 'a5957736-c0e4-11e4-85cc-5b31063c31ef'; end if;
if lc_vVal= 'H50200104' and in_vVal2 = 'PDS' then lc_vReturn := 'a5959e41-c0e4-11e4-85cc-5b31063c31ef'; end if;
if lc_vVal= 'H50200104' and in_vVal2 = 'REC' then lc_vReturn := 'a5959e40-c0e4-11e4-85cc-5b31063c31ef'; end if;
if lc_vVal= 'H50200105' and in_vVal2 = 'AMP' then lc_vReturn := 'a5959e42-c0e4-11e4-85cc-5b31063c31ef'; end if;
if lc_vVal= 'H50200105' and in_vVal2 = 'EM' then lc_vReturn := 'a5959e43-c0e4-11e4-85cc-5b31063c31ef'; end if;
if lc_vVal= 'H50200105' and in_vVal2 = 'ESS' then lc_vReturn := 'a5959e44-c0e4-11e4-85cc-5b31063c31ef'; end if;
if lc_vVal= 'H50200105' and in_vVal2 = 'LS' then lc_vReturn := 'a5959e45-c0e4-11e4-85cc-5b31063c31ef'; end if;
if lc_vVal= 'H50200105' and in_vVal2 = 'PNL' then lc_vReturn := 'a5959e46-c0e4-11e4-85cc-5b31063c31ef'; end if;
if lc_vVal= 'H50200106' and in_vVal2 = 'HPWR' then lc_vReturn := 'a595c550-c0e4-11e4-85cc-5b31063c31ef'; end if;
if lc_vVal= 'H50200107' and in_vVal2 = 'TPWR' then lc_vReturn := 'a595c551-c0e4-11e4-85cc-5b31063c31ef'; end if;
if lc_vVal= 'H502002' and in_vVal2 = 'COM' then lc_vReturn := 'a595c553-c0e4-11e4-85cc-5b31063c31ef'; end if;
if lc_vVal= 'H50200201' and in_vVal2 = 'FOP' then lc_vReturn := 'a595c554-c0e4-11e4-85cc-5b31063c31ef'; end if;
if lc_vVal= 'H50200202' and in_vVal2 = 'LCOM' then lc_vReturn := 'a595c555-c0e4-11e4-85cc-5b31063c31ef'; end if;
if lc_vVal= 'H50200203' and in_vVal2 = 'N/A' then lc_vReturn := 'a595c556-c0e4-11e4-85cc-5b31063c31ef'; end if;
if lc_vVal= 'H50200204' and in_vVal2 = 'CTV' then lc_vReturn := 'a595ec60-c0e4-11e4-85cc-5b31063c31ef'; end if;
if lc_vVal= 'H50200204' and in_vVal2 = 'LCTV' then lc_vReturn := 'a595ec61-c0e4-11e4-85cc-5b31063c31ef'; end if;
if lc_vVal= 'H50200205' and in_vVal2 = 'AS' then lc_vReturn := 'a595ec62-c0e4-11e4-85cc-5b31063c31ef'; end if;
if lc_vVal= 'H50200206' and in_vVal2 = 'MPWR' then lc_vReturn := 'a595ec63-c0e4-11e4-85cc-5b31063c31ef'; end if;
if lc_vVal= 'H50200207' and in_vVal2 = 'HCOM' then lc_vReturn := 'a595ec64-c0e4-11e4-85cc-5b31063c31ef'; end if;
if lc_vVal= 'H50200208' and in_vVal2 = 'TCOM' then lc_vReturn := 'a595ec65-c0e4-11e4-85cc-5b31063c31ef'; end if;
if lc_vVal= 'H502003' and in_vVal2 = 'DLT' then lc_vReturn := 'a5961371-c0e4-11e4-85cc-5b31063c31ef'; end if;
if lc_vVal= 'H502003' and in_vVal2 = 'HML' then lc_vReturn := 'a5961370-c0e4-11e4-85cc-5b31063c31ef'; end if;
if lc_vVal= 'H502003' and in_vVal2 = 'LGT' then lc_vReturn := 'a595ec66-c0e4-11e4-85cc-5b31063c31ef'; end if;
if lc_vVal= 'H502004' and in_vVal2 = 'LTP' then lc_vReturn := 'a5961372-c0e4-11e4-85cc-5b31063c31ef'; end if;
if lc_vVal= 'H502005' and in_vVal2 = 'CBL' then lc_vReturn := 'a5961373-c0e4-11e4-85cc-5b31063c31ef'; end if;
if lc_vVal= 'H502006' and in_vVal2 = 'OEU' then lc_vReturn := 'a5961374-c0e4-11e4-85cc-5b31063c31ef'; end if;
if lc_vVal= 'H502006' and in_vVal2 = 'RUNK' then lc_vReturn := 'a5961375-c0e4-11e4-85cc-5b31063c31ef'; end if;
if lc_vVal= 'H50300101' and in_vVal2 = 'LFP' then lc_vReturn := 'a5963a82-c0e4-11e4-85cc-5b31063c31ef'; end if;
if lc_vVal= 'H50300102' and in_vVal2 = 'HFP' then lc_vReturn := 'a5963a83-c0e4-11e4-85cc-5b31063c31ef'; end if;
if lc_vVal= 'H50300102' and in_vVal2 = 'TFP' then lc_vReturn := 'a5963a84-c0e4-11e4-85cc-5b31063c31ef'; end if;
if lc_vVal= 'H50300103' and in_vVal2 = 'SPR' then lc_vReturn := 'a5963a85-c0e4-11e4-85cc-5b31063c31ef'; end if;
if lc_vVal= 'H50300104' and in_vVal2 = 'HYD' then lc_vReturn := 'a5963a86-c0e4-11e4-85cc-5b31063c31ef'; end if;
if lc_vVal= 'H50300105' and in_vVal2 = 'FH' then lc_vReturn := 'a5966190-c0e4-11e4-85cc-5b31063c31ef'; end if;
if lc_vVal= 'H50300106' and in_vVal2 = 'GVFP' then lc_vReturn := 'a5966191-c0e4-11e4-85cc-5b31063c31ef'; end if;
if lc_vVal= 'H50300107' and in_vVal2 = 'BPFP' then lc_vReturn := 'a5966192-c0e4-11e4-85cc-5b31063c31ef'; end if;
if lc_vVal= 'H503002' and in_vVal2 = 'FA' then lc_vReturn := 'a5966193-c0e4-11e4-85cc-5b31063c31ef'; end if;
if lc_vVal= 'H503003' and in_vVal2 = 'OFPS' then lc_vReturn := 'a5966194-c0e4-11e4-85cc-5b31063c31ef'; end if;
if lc_vVal= 'H503003' and in_vVal2 = 'RUNK' then lc_vReturn := 'a5966195-c0e4-11e4-85cc-5b31063c31ef'; end if;

return lc_vReturn;

exception
  when others then
      execute immediate 'insert into debug (functionname, input, message) values (''BB_F_GET_UFII_GUID'',''' || in_vVal || ''',''error: ' || sqlerrm || ''')';
      return 'e';

end;




/*

type = 'uniformatII'
[
{'id':'a588cd03-c0e4-11e4-85cc-5b31063c31ef','key':['H10100101','GDP','a588cd03-c0e4-11e4-85cc-5b31063c31ef'],'value':'GDP'},
{'id':'a588cd01-c0e4-11e4-85cc-5b31063c31ef','key':['H10100101','P','a588cd01-c0e4-11e4-85cc-5b31063c31ef'],'value':'P'},
{'id':'a588cd02-c0e4-11e4-85cc-5b31063c31ef','key':['H10100101','PB','a588cd02-c0e4-11e4-85cc-5b31063c31ef'],'value':'PB'},
{'id':'a588f410-c0e4-11e4-85cc-5b31063c31ef','key':['H10100102','SP','a588f410-c0e4-11e4-85cc-5b31063c31ef'],'value':'SP'},
{'id':'a588f411-c0e4-11e4-85cc-5b31063c31ef','key':['H10100103','C','a588f411-c0e4-11e4-85cc-5b31063c31ef'],'value':'C'},
{'id':'a588f412-c0e4-11e4-85cc-5b31063c31ef','key':['H10100104','COF','a588f412-c0e4-11e4-85cc-5b31063c31ef'],'value':'COF'},
{'id':'a588f413-c0e4-11e4-85cc-5b31063c31ef','key':['H10100105','WRP','a588f413-c0e4-11e4-85cc-5b31063c31ef'],'value':'WRP'},
{'id':'a588f414-c0e4-11e4-85cc-5b31063c31ef','key':['H10100106','ENC','a588f414-c0e4-11e4-85cc-5b31063c31ef'],'value':'ENC'},
{'id':'a5891b20-c0e4-11e4-85cc-5b31063c31ef','key':['H10100107','CTG','a5891b20-c0e4-11e4-85cc-5b31063c31ef'],'value':'CTG'},
{'id':'a5891b21-c0e4-11e4-85cc-5b31063c31ef','key':['H10100108','OPFE','a5891b21-c0e4-11e4-85cc-5b31063c31ef'],'value':'OPFE'},
{'id':'a5891b22-c0e4-11e4-85cc-5b31063c31ef','key':['H101002','PC','a5891b22-c0e4-11e4-85cc-5b31063c31ef'],'value':'PC'},
{'id':'a5894230-c0e4-11e4-85cc-5b31063c31ef','key':['H10100301','SP','a5894230-c0e4-11e4-85cc-5b31063c31ef'],'value':'SP'},
{'id':'a5894231-c0e4-11e4-85cc-5b31063c31ef','key':['H10100302','TBA','a5894231-c0e4-11e4-85cc-5b31063c31ef'],'value':'TBA'},
{'id':'a5894232-c0e4-11e4-85cc-5b31063c31ef','key':['H10100303','TR','a5894232-c0e4-11e4-85cc-5b31063c31ef'],'value':'TR'},
{'id':'a5894233-c0e4-11e4-85cc-5b31063c31ef','key':['H10100304','CGW','a5894233-c0e4-11e4-85cc-5b31063c31ef'],'value':'CGW'},
{'id':'a5894234-c0e4-11e4-85cc-5b31063c31ef','key':['H10100305','BW','a5894234-c0e4-11e4-85cc-5b31063c31ef'],'value':'BW'},
{'id':'a5896940-c0e4-11e4-85cc-5b31063c31ef','key':['H10100306','C','a5896940-c0e4-11e4-85cc-5b31063c31ef'],'value':'C'},
{'id':'a5896941-c0e4-11e4-85cc-5b31063c31ef','key':['H10100307','CRB','a5896941-c0e4-11e4-85cc-5b31063c31ef'],'value':'CRB'},
{'id':'a5896942-c0e4-11e4-85cc-5b31063c31ef','key':['H10100308','OQE','a5896942-c0e4-11e4-85cc-5b31063c31ef'],'value':'OQE'},
{'id':'a5896944-c0e4-11e4-85cc-5b31063c31ef','key':['H10100401','P','a5896944-c0e4-11e4-85cc-5b31063c31ef'],'value':'P'},
{'id':'a5899050-c0e4-11e4-85cc-5b31063c31ef','key':['H10100402','PC','a5899050-c0e4-11e4-85cc-5b31063c31ef'],'value':'PC'},
{'id':'a5899051-c0e4-11e4-85cc-5b31063c31ef','key':['H10100403','DA','a5899051-c0e4-11e4-85cc-5b31063c31ef'],'value':'DA'},
{'id':'a5899052-c0e4-11e4-85cc-5b31063c31ef','key':['H10100403','DU','a5899052-c0e4-11e4-85cc-5b31063c31ef'],'value':'DU'},
{'id':'a589b760-c0e4-11e4-85cc-5b31063c31ef','key':['H101005','REV','a589b760-c0e4-11e4-85cc-5b31063c31ef'],'value':'REV'},
{'id':'a589b761-c0e4-11e4-85cc-5b31063c31ef','key':['H101006','SW','a589b761-c0e4-11e4-85cc-5b31063c31ef'],'value':'SW'},
{'id':'a589b762-c0e4-11e4-85cc-5b31063c31ef','key':['H101007','RMP','a589b762-c0e4-11e4-85cc-5b31063c31ef'],'value':'RMP'},
{'id':'a589b763-c0e4-11e4-85cc-5b31063c31ef','key':['H101008','COW','a589b763-c0e4-11e4-85cc-5b31063c31ef'],'value':'COW'},
{'id':'a589de70-c0e4-11e4-85cc-5b31063c31ef','key':['H10100901','FW','a589de70-c0e4-11e4-85cc-5b31063c31ef'],'value':'FW'},
{'id':'a589de71-c0e4-11e4-85cc-5b31063c31ef','key':['H10100902','WAP','a589de71-c0e4-11e4-85cc-5b31063c31ef'],'value':'WAP'},
{'id':'a589de72-c0e4-11e4-85cc-5b31063c31ef','key':['H10100903','OWP','a589de72-c0e4-11e4-85cc-5b31063c31ef'],'value':'OWP'},
{'id':'a589de73-c0e4-11e4-85cc-5b31063c31ef','key':['H10100904','WWL','a589de73-c0e4-11e4-85cc-5b31063c31ef'],'value':'WWL'},
{'id':'a589de74-c0e4-11e4-85cc-5b31063c31ef','key':['H10100905','WST','a589de74-c0e4-11e4-85cc-5b31063c31ef'],'value':'WST'},
{'id':'a58a0580-c0e4-11e4-85cc-5b31063c31ef','key':['H10100906','P','a58a0580-c0e4-11e4-85cc-5b31063c31ef'],'value':'P'},
{'id':'a58a0581-c0e4-11e4-85cc-5b31063c31ef','key':['H101010','APR','a58a0581-c0e4-11e4-85cc-5b31063c31ef'],'value':'APR'},
{'id':'a58a0582-c0e4-11e4-85cc-5b31063c31ef','key':['H101010','BKF','a58a0582-c0e4-11e4-85cc-5b31063c31ef'],'value':'BKF'},
{'id':'a58a0583-c0e4-11e4-85cc-5b31063c31ef','key':['H101010','PD','a58a0583-c0e4-11e4-85cc-5b31063c31ef'],'value':'PD'},
{'id':'a58a2c90-c0e4-11e4-85cc-5b31063c31ef','key':['H102001','B','a58a2c90-c0e4-11e4-85cc-5b31063c31ef'],'value':'B'},
{'id':'a58a2c93-c0e4-11e4-85cc-5b31063c31ef','key':['H102001','BR','a58a2c93-c0e4-11e4-85cc-5b31063c31ef'],'value':'BR'},
{'id':'a58a2c92-c0e4-11e4-85cc-5b31063c31ef','key':['H102001','G','a58a2c92-c0e4-11e4-85cc-5b31063c31ef'],'value':'G'},
{'id':'a58a2c91-c0e4-11e4-85cc-5b31063c31ef','key':['H102001','STR','a58a2c91-c0e4-11e4-85cc-5b31063c31ef'],'value':'STR'},
{'id':'a58a53a0-c0e4-11e4-85cc-5b31063c31ef','key':['H102002','COL','a58a53a0-c0e4-11e4-85cc-5b31063c31ef'],'value':'COL'},
{'id':'a58a53a2-c0e4-11e4-85cc-5b31063c31ef','key':['H10200301','UV','a58a53a2-c0e4-11e4-85cc-5b31063c31ef'],'value':'UV'},
{'id':'a58a53a3-c0e4-11e4-85cc-5b31063c31ef','key':['H10200302','UT','a58a53a3-c0e4-11e4-85cc-5b31063c31ef'],'value':'UT'},
{'id':'a58aa1c2-c0e4-11e4-85cc-5b31063c31ef','key':['H10200303','ENCB','a58aa1c2-c0e4-11e4-85cc-5b31063c31ef'],'value':'ENCB'},
{'id':'a58aa1c0-c0e4-11e4-85cc-5b31063c31ef','key':['H10200303','MH','a58aa1c0-c0e4-11e4-85cc-5b31063c31ef'],'value':'MH'},
{'id':'a58a7ab2-c0e4-11e4-85cc-5b31063c31ef','key':['H10200303','STP','a58a7ab2-c0e4-11e4-85cc-5b31063c31ef'],'value':'STP'},
{'id':'a58a7ab3-c0e4-11e4-85cc-5b31063c31ef','key':['H10200303','UPD','a58a7ab3-c0e4-11e4-85cc-5b31063c31ef'],'value':'UPD'},
{'id':'a58aa1c1-c0e4-11e4-85cc-5b31063c31ef','key':['H10200303','US','a58aa1c1-c0e4-11e4-85cc-5b31063c31ef'],'value':'US'},
{'id':'a58a7ab1-c0e4-11e4-85cc-5b31063c31ef','key':['H10200303','UTC','a58a7ab1-c0e4-11e4-85cc-5b31063c31ef'],'value':'UTC'},
{'id':'a58a7ab0-c0e4-11e4-85cc-5b31063c31ef','key':['H10200303','UVC','a58a7ab0-c0e4-11e4-85cc-5b31063c31ef'],'value':'UVC'},
{'id':'a58aa1c3-c0e4-11e4-85cc-5b31063c31ef','key':['H102004','AW','a58aa1c3-c0e4-11e4-85cc-5b31063c31ef'],'value':'AW'},
{'id':'a58aa1c4-c0e4-11e4-85cc-5b31063c31ef','key':['H102004','FAC','a58aa1c4-c0e4-11e4-85cc-5b31063c31ef'],'value':'FAC'},
{'id':'a58ac8d1-c0e4-11e4-85cc-5b31063c31ef','key':['H103001','D','a58ac8d1-c0e4-11e4-85cc-5b31063c31ef'],'value':'D'},
{'id':'a58ac8d2-c0e4-11e4-85cc-5b31063c31ef','key':['H103001','DA','a58ac8d2-c0e4-11e4-85cc-5b31063c31ef'],'value':'DA'},
{'id':'a58ac8d3-c0e4-11e4-85cc-5b31063c31ef','key':['H103001','DU','a58ac8d3-c0e4-11e4-85cc-5b31063c31ef'],'value':'DU'},
{'id':'a58aefe0-c0e4-11e4-85cc-5b31063c31ef','key':['H103002','OGD','a58aefe0-c0e4-11e4-85cc-5b31063c31ef'],'value':'OGD'},
{'id':'a58aefe1-c0e4-11e4-85cc-5b31063c31ef','key':['H103003','DOV','a58aefe1-c0e4-11e4-85cc-5b31063c31ef'],'value':'DOV'},
{'id':'a58aefe3-c0e4-11e4-85cc-5b31063c31ef','key':['H103004','BRL','a58aefe3-c0e4-11e4-85cc-5b31063c31ef'],'value':'BRL'},
{'id':'a58aefe2-c0e4-11e4-85cc-5b31063c31ef','key':['H103004','CB','a58aefe2-c0e4-11e4-85cc-5b31063c31ef'],'value':'CB'},
{'id':'a58aefe4-c0e4-11e4-85cc-5b31063c31ef','key':['H103005','MFD','a58aefe4-c0e4-11e4-85cc-5b31063c31ef'],'value':'MFD'},
{'id':'a58b16f0-c0e4-11e4-85cc-5b31063c31ef','key':['H103006','LFD','a58b16f0-c0e4-11e4-85cc-5b31063c31ef'],'value':'LFD'},
{'id':'a58b16f1-c0e4-11e4-85cc-5b31063c31ef','key':['H103007','UM','a58b16f1-c0e4-11e4-85cc-5b31063c31ef'],'value':'UM'},
{'id':'a58b16f2-c0e4-11e4-85cc-5b31063c31ef','key':['H103008','EJ','a58b16f2-c0e4-11e4-85cc-5b31063c31ef'],'value':'EJ'},
{'id':'a58b3e00-c0e4-11e4-85cc-5b31063c31ef','key':['H103009','GPT','a58b3e00-c0e4-11e4-85cc-5b31063c31ef'],'value':'GPT'},
{'id':'a58b16f3-c0e4-11e4-85cc-5b31063c31ef','key':['H103009','GRL','a58b16f3-c0e4-11e4-85cc-5b31063c31ef'],'value':'GRL'},
{'id':'a58b3e01-c0e4-11e4-85cc-5b31063c31ef','key':['H103009','HRL','a58b3e01-c0e4-11e4-85cc-5b31063c31ef'],'value':'HRL'},
{'id':'a58b3e03-c0e4-11e4-85cc-5b31063c31ef','key':['H10301001','DST','a58b3e03-c0e4-11e4-85cc-5b31063c31ef'],'value':'DST'},
{'id':'a58b3e04-c0e4-11e4-85cc-5b31063c31ef','key':['H10301002','CBST','a58b3e04-c0e4-11e4-85cc-5b31063c31ef'],'value':'CBST'},
{'id':'a58b6510-c0e4-11e4-85cc-5b31063c31ef','key':['H103011','CRL','a58b6510-c0e4-11e4-85cc-5b31063c31ef'],'value':'CRL'},
{'id':'a58b6511-c0e4-11e4-85cc-5b31063c31ef','key':['H103011','CRS','a58b6511-c0e4-11e4-85cc-5b31063c31ef'],'value':'CRS'},
{'id':'a58b6512-c0e4-11e4-85cc-5b31063c31ef','key':['H103011','DRN','a58b6512-c0e4-11e4-85cc-5b31063c31ef'],'value':'DRN'},
{'id':'a58b8c20-c0e4-11e4-85cc-5b31063c31ef','key':['H103011','GRT','a58b8c20-c0e4-11e4-85cc-5b31063c31ef'],'value':'GRT'},
{'id':'a58b6513-c0e4-11e4-85cc-5b31063c31ef','key':['H103011','TRL','a58b6513-c0e4-11e4-85cc-5b31063c31ef'],'value':'TRL'},
{'id':'a58b8c23-c0e4-11e4-85cc-5b31063c31ef','key':['H10400101','FP','a58b8c23-c0e4-11e4-85cc-5b31063c31ef'],'value':'FP'},
{'id':'a58bb330-c0e4-11e4-85cc-5b31063c31ef','key':['H10400101','FPC','a58bb330-c0e4-11e4-85cc-5b31063c31ef'],'value':'FPC'},
{'id':'a58cc4a0-c0e4-11e4-85cc-5b31063c31ef','key':['H10400102','FCH','a58cc4a0-c0e4-11e4-85cc-5b31063c31ef'],'value':'FCH'},
{'id':'a58c9d90-c0e4-11e4-85cc-5b31063c31ef','key':['H10400102','FF','a58c9d90-c0e4-11e4-85cc-5b31063c31ef'],'value':'FF'},
{'id':'a58cc4a1-c0e4-11e4-85cc-5b31063c31ef','key':['H10400102','FWL','a58cc4a1-c0e4-11e4-85cc-5b31063c31ef'],'value':'FWL'},
{'id':'a58cc4a2-c0e4-11e4-85cc-5b31063c31ef','key':['H10400103','EFND','a58cc4a2-c0e4-11e4-85cc-5b31063c31ef'],'value':'EFND'},
{'id':'a58cc4a3-c0e4-11e4-85cc-5b31063c31ef','key':['H10400104','MFND','a58cc4a3-c0e4-11e4-85cc-5b31063c31ef'],'value':'MFND'},
{'id':'a58cc4a4-c0e4-11e4-85cc-5b31063c31ef','key':['H10400105','FFF','a58cc4a4-c0e4-11e4-85cc-5b31063c31ef'],'value':'FFF'},
{'id':'a58cebb0-c0e4-11e4-85cc-5b31063c31ef','key':['H10400106','HPF','a58cebb0-c0e4-11e4-85cc-5b31063c31ef'],'value':'HPF'},
{'id':'a58cebb1-c0e4-11e4-85cc-5b31063c31ef','key':['H10400107','PF','a58cebb1-c0e4-11e4-85cc-5b31063c31ef'],'value':'PF'},
{'id':'a58cebb2-c0e4-11e4-85cc-5b31063c31ef','key':['H10400108','CML','a58cebb2-c0e4-11e4-85cc-5b31063c31ef'],'value':'CML'},
{'id':'a58cebb3-c0e4-11e4-85cc-5b31063c31ef','key':['H10400109','SEP','a58cebb3-c0e4-11e4-85cc-5b31063c31ef'],'value':'SEP'},
{'id':'a58d12c0-c0e4-11e4-85cc-5b31063c31ef','key':['H10400110','CHN','a58d12c0-c0e4-11e4-85cc-5b31063c31ef'],'value':'CHN'},
{'id':'a58d12c1-c0e4-11e4-85cc-5b31063c31ef','key':['H10400110','FPN','a58d12c1-c0e4-11e4-85cc-5b31063c31ef'],'value':'FPN'},
{'id':'a58d12c3-c0e4-11e4-85cc-5b31063c31ef','key':['H10400201','FP','a58d12c3-c0e4-11e4-85cc-5b31063c31ef'],'value':'FP'},
{'id':'a58d12c4-c0e4-11e4-85cc-5b31063c31ef','key':['H10400201','FPC','a58d12c4-c0e4-11e4-85cc-5b31063c31ef'],'value':'FPC'},
{'id':'a58d39d0-c0e4-11e4-85cc-5b31063c31ef','key':['H10400202','FCH','a58d39d0-c0e4-11e4-85cc-5b31063c31ef'],'value':'FCH'},
{'id':'a58d12c5-c0e4-11e4-85cc-5b31063c31ef','key':['H10400202','FF','a58d12c5-c0e4-11e4-85cc-5b31063c31ef'],'value':'FF'},
{'id':'a58d39d1-c0e4-11e4-85cc-5b31063c31ef','key':['H10400202','FWL','a58d39d1-c0e4-11e4-85cc-5b31063c31ef'],'value':'FWL'},
{'id':'a58d39d2-c0e4-11e4-85cc-5b31063c31ef','key':['H10400203','EFND','a58d39d2-c0e4-11e4-85cc-5b31063c31ef'],'value':'EFND'},
{'id':'a58d60e0-c0e4-11e4-85cc-5b31063c31ef','key':['H10400204','MFND','a58d60e0-c0e4-11e4-85cc-5b31063c31ef'],'value':'MFND'},
{'id':'a58d60e1-c0e4-11e4-85cc-5b31063c31ef','key':['H10400205','CML','a58d60e1-c0e4-11e4-85cc-5b31063c31ef'],'value':'CML'},
{'id':'a58d60e3-c0e4-11e4-85cc-5b31063c31ef','key':['H10400301','FP','a58d60e3-c0e4-11e4-85cc-5b31063c31ef'],'value':'FP'},
{'id':'a58d60e4-c0e4-11e4-85cc-5b31063c31ef','key':['H10400302','FCH','a58d60e4-c0e4-11e4-85cc-5b31063c31ef'],'value':'FCH'},
{'id':'a58d87f0-c0e4-11e4-85cc-5b31063c31ef','key':['H10400302','FWL','a58d87f0-c0e4-11e4-85cc-5b31063c31ef'],'value':'FWL'},
{'id':'a58d87f1-c0e4-11e4-85cc-5b31063c31ef','key':['H10400303','EFND','a58d87f1-c0e4-11e4-85cc-5b31063c31ef'],'value':'EFND'},
{'id':'a58d87f2-c0e4-11e4-85cc-5b31063c31ef','key':['H10400304','MFND','a58d87f2-c0e4-11e4-85cc-5b31063c31ef'],'value':'MFND'},
{'id':'a58d87f4-c0e4-11e4-85cc-5b31063c31ef','key':['H10400401','FP','a58d87f4-c0e4-11e4-85cc-5b31063c31ef'],'value':'FP'},
{'id':'a58daf00-c0e4-11e4-85cc-5b31063c31ef','key':['H10400402','WRP','a58daf00-c0e4-11e4-85cc-5b31063c31ef'],'value':'WRP'},
{'id':'a58daf02-c0e4-11e4-85cc-5b31063c31ef','key':['H10400501','MF','a58daf02-c0e4-11e4-85cc-5b31063c31ef'],'value':'MF'},
{'id':'a58daf03-c0e4-11e4-85cc-5b31063c31ef','key':['H10400502','MF','a58daf03-c0e4-11e4-85cc-5b31063c31ef'],'value':'MF'},
{'id':'a58daf04-c0e4-11e4-85cc-5b31063c31ef','key':['H10400503','MF','a58daf04-c0e4-11e4-85cc-5b31063c31ef'],'value':'MF'},
{'id':'a58dd610-c0e4-11e4-85cc-5b31063c31ef','key':['H10400504','MF','a58dd610-c0e4-11e4-85cc-5b31063c31ef'],'value':'MF'},
{'id':'a58dd611-c0e4-11e4-85cc-5b31063c31ef','key':['H10400505','MF','a58dd611-c0e4-11e4-85cc-5b31063c31ef'],'value':'MF'},
{'id':'a58dd613-c0e4-11e4-85cc-5b31063c31ef','key':['H104006','MF','a58dd613-c0e4-11e4-85cc-5b31063c31ef'],'value':'MF'},
{'id':'a58dd614-c0e4-11e4-85cc-5b31063c31ef','key':['H104006','OMBC','a58dd614-c0e4-11e4-85cc-5b31063c31ef'],'value':'OMBC'},
{'id':'a58dd612-c0e4-11e4-85cc-5b31063c31ef','key':['H104006','RC','a58dd612-c0e4-11e4-85cc-5b31063c31ef'],'value':'RC'},
{'id':'a58dfd21-c0e4-11e4-85cc-5b31063c31ef','key':['H105001','HRL','a58dfd21-c0e4-11e4-85cc-5b31063c31ef'],'value':'HRL'},
{'id':'a58dfd23-c0e4-11e4-85cc-5b31063c31ef','key':['H10500201','GW','a58dfd23-c0e4-11e4-85cc-5b31063c31ef'],'value':'GW'},
{'id':'a58dfd24-c0e4-11e4-85cc-5b31063c31ef','key':['H10500202','WPLT','a58dfd24-c0e4-11e4-85cc-5b31063c31ef'],'value':'WPLT'},
{'id':'a58e2430-c0e4-11e4-85cc-5b31063c31ef','key':['H10500203','GWP','a58e2430-c0e4-11e4-85cc-5b31063c31ef'],'value':'GWP'},
{'id':'a58e2431-c0e4-11e4-85cc-5b31063c31ef','key':['H10500204','RGD','a58e2431-c0e4-11e4-85cc-5b31063c31ef'],'value':'RGD'},
{'id':'a58e2432-c0e4-11e4-85cc-5b31063c31ef','key':['H10500205','GWR','a58e2432-c0e4-11e4-85cc-5b31063c31ef'],'value':'GWR'},
{'id':'a58e2433-c0e4-11e4-85cc-5b31063c31ef','key':['H105003','CBL','a58e2433-c0e4-11e4-85cc-5b31063c31ef'],'value':'CBL'},
{'id':'a58e4b40-c0e4-11e4-85cc-5b31063c31ef','key':['H105004','FD','a58e4b40-c0e4-11e4-85cc-5b31063c31ef'],'value':'FD'},
{'id':'a58e4b41-c0e4-11e4-85cc-5b31063c31ef','key':['H10500401','FLT','a58e4b41-c0e4-11e4-85cc-5b31063c31ef'],'value':'FLT'},
{'id':'a58e4b42-c0e4-11e4-85cc-5b31063c31ef','key':['H10500402','WL','a58e4b42-c0e4-11e4-85cc-5b31063c31ef'],'value':'WL'},
{'id':'a58e4b43-c0e4-11e4-85cc-5b31063c31ef','key':['H10500403','TR','a58e4b43-c0e4-11e4-85cc-5b31063c31ef'],'value':'TR'},
{'id':'a58e7250-c0e4-11e4-85cc-5b31063c31ef','key':['H10500404','MF','a58e7250-c0e4-11e4-85cc-5b31063c31ef'],'value':'MF'},
{'id':'a58e7251-c0e4-11e4-85cc-5b31063c31ef','key':['H10500405','FND','a58e7251-c0e4-11e4-85cc-5b31063c31ef'],'value':'FND'},
{'id':'a58e7252-c0e4-11e4-85cc-5b31063c31ef','key':['H10500406','GPA','a58e7252-c0e4-11e4-85cc-5b31063c31ef'],'value':'GPA'},
{'id':'a58e7253-c0e4-11e4-85cc-5b31063c31ef','key':['H10500407','TPLT','a58e7253-c0e4-11e4-85cc-5b31063c31ef'],'value':'TPLT'},
{'id':'a58e9960-c0e4-11e4-85cc-5b31063c31ef','key':['H10500408','CNH','a58e9960-c0e4-11e4-85cc-5b31063c31ef'],'value':'CNH'},
{'id':'a58e9961-c0e4-11e4-85cc-5b31063c31ef','key':['H10500409','As Needed','a58e9961-c0e4-11e4-85cc-5b31063c31ef'],'value':'As Needed'},
{'id':'a58e9962-c0e4-11e4-85cc-5b31063c31ef','key':['H105005','LAD','a58e9962-c0e4-11e4-85cc-5b31063c31ef'],'value':'LAD'},
{'id':'a58e9963-c0e4-11e4-85cc-5b31063c31ef','key':['H105006','LRH','a58e9963-c0e4-11e4-85cc-5b31063c31ef'],'value':'LRH'},
{'id':'a58e9964-c0e4-11e4-85cc-5b31063c31ef','key':['H105007','OCB','a58e9964-c0e4-11e4-85cc-5b31063c31ef'],'value':'OCB'},
{'id':'a58ec070-c0e4-11e4-85cc-5b31063c31ef','key':['H105008','STS','a58ec070-c0e4-11e4-85cc-5b31063c31ef'],'value':'STS'},
{'id':'a58ec073-c0e4-11e4-85cc-5b31063c31ef','key':['H301001','WPA','a58ec073-c0e4-11e4-85cc-5b31063c31ef'],'value':'WPA'},
{'id':'a58ec074-c0e4-11e4-85cc-5b31063c31ef','key':['H301002','BRW','a58ec074-c0e4-11e4-85cc-5b31063c31ef'],'value':'BRW'},
{'id':'a58ee781-c0e4-11e4-85cc-5b31063c31ef','key':['H302001','RR','a58ee781-c0e4-11e4-85cc-5b31063c31ef'],'value':'RR'},
{'id':'a58ee782-c0e4-11e4-85cc-5b31063c31ef','key':['H302002','GFR','a58ee782-c0e4-11e4-85cc-5b31063c31ef'],'value':'GFR'},
{'id':'a58ee783-c0e4-11e4-85cc-5b31063c31ef','key':['H302003','CFR','a58ee783-c0e4-11e4-85cc-5b31063c31ef'],'value':'CFR'},
{'id':'a58f0e93-c0e4-11e4-85cc-5b31063c31ef','key':['H50100101','DPPW','a58f0e93-c0e4-11e4-85cc-5b31063c31ef'],'value':'DPPW'},
{'id':'a58f0e92-c0e4-11e4-85cc-5b31063c31ef','key':['H50100101','LPW','a58f0e92-c0e4-11e4-85cc-5b31063c31ef'],'value':'LPW'},
{'id':'a58f0e94-c0e4-11e4-85cc-5b31063c31ef','key':['H50100102','HPW','a58f0e94-c0e4-11e4-85cc-5b31063c31ef'],'value':'HPW'},
{'id':'a58f0e95-c0e4-11e4-85cc-5b31063c31ef','key':['H50100102','TPW','a58f0e95-c0e4-11e4-85cc-5b31063c31ef'],'value':'TPW'},
{'id':'a58f35a1-c0e4-11e4-85cc-5b31063c31ef','key':['H50100103','BVPW','a58f35a1-c0e4-11e4-85cc-5b31063c31ef'],'value':'BVPW'},
{'id':'a58f35a3-c0e4-11e4-85cc-5b31063c31ef','key':['H50100103','CVPW','a58f35a3-c0e4-11e4-85cc-5b31063c31ef'],'value':'CVPW'},
{'id':'a58f5cb0-c0e4-11e4-85cc-5b31063c31ef','key':['H50100103','DNPW','a58f5cb0-c0e4-11e4-85cc-5b31063c31ef'],'value':'DNPW'},
{'id':'a58f35a2-c0e4-11e4-85cc-5b31063c31ef','key':['H50100103','GVPW','a58f35a2-c0e4-11e4-85cc-5b31063c31ef'],'value':'GVPW'},
{'id':'a58f5cb1-c0e4-11e4-85cc-5b31063c31ef','key':['H50100103','IVPW','a58f5cb1-c0e4-11e4-85cc-5b31063c31ef'],'value':'IVPW'},
{'id':'a58f35a0-c0e4-11e4-85cc-5b31063c31ef','key':['H50100103','VPW','a58f35a0-c0e4-11e4-85cc-5b31063c31ef'],'value':'VPW'},
{'id':'a58f35a4-c0e4-11e4-85cc-5b31063c31ef','key':['H50100103','VTPW','a58f35a4-c0e4-11e4-85cc-5b31063c31ef'],'value':'VTPW'},
{'id':'a58f5cb3-c0e4-11e4-85cc-5b31063c31ef','key':['H50100104','RPPW','a58f5cb3-c0e4-11e4-85cc-5b31063c31ef'],'value':'RPPW'},
{'id':'a58f5cb2-c0e4-11e4-85cc-5b31063c31ef','key':['H50100104','RPW','a58f5cb2-c0e4-11e4-85cc-5b31063c31ef'],'value':'RPW'},
{'id':'a58f5cb4-c0e4-11e4-85cc-5b31063c31ef','key':['H50100105','CNPW','a58f5cb4-c0e4-11e4-85cc-5b31063c31ef'],'value':'CNPW'},
{'id':'a58f5cb5-c0e4-11e4-85cc-5b31063c31ef','key':['H50100106','IPW','a58f5cb5-c0e4-11e4-85cc-5b31063c31ef'],'value':'IPW'},
{'id':'a58f83c0-c0e4-11e4-85cc-5b31063c31ef','key':['H50100107','MPW','a58f83c0-c0e4-11e4-85cc-5b31063c31ef'],'value':'MPW'},
{'id':'a58f83c1-c0e4-11e4-85cc-5b31063c31ef','key':['H50100108','GPW','a58f83c1-c0e4-11e4-85cc-5b31063c31ef'],'value':'GPW'},
{'id':'a58f83c2-c0e4-11e4-85cc-5b31063c31ef','key':['H50100109','ECWS','a58f83c2-c0e4-11e4-85cc-5b31063c31ef'],'value':'ECWS'},
{'id':'a58f83c3-c0e4-11e4-85cc-5b31063c31ef','key':['H50100110','BPPW','a58f83c3-c0e4-11e4-85cc-5b31063c31ef'],'value':'BPPW'},
{'id':'a58faad2-c0e4-11e4-85cc-5b31063c31ef','key':['H50100110','CBPW','a58faad2-c0e4-11e4-85cc-5b31063c31ef'],'value':'CBPW'},
{'id':'a58faad1-c0e4-11e4-85cc-5b31063c31ef','key':['H50100110','PCPW','a58faad1-c0e4-11e4-85cc-5b31063c31ef'],'value':'PCPW'},
{'id':'a58faad0-c0e4-11e4-85cc-5b31063c31ef','key':['H50100110','USPW','a58faad0-c0e4-11e4-85cc-5b31063c31ef'],'value':'USPW'},
{'id':'a58fd1e0-c0e4-11e4-85cc-5b31063c31ef','key':['H50100201','LSW','a58fd1e0-c0e4-11e4-85cc-5b31063c31ef'],'value':'LSW'},
{'id':'a58fd1e1-c0e4-11e4-85cc-5b31063c31ef','key':['H50100202','HSW','a58fd1e1-c0e4-11e4-85cc-5b31063c31ef'],'value':'HSW'},
{'id':'a58fd1e2-c0e4-11e4-85cc-5b31063c31ef','key':['H50100202','TSW','a58fd1e2-c0e4-11e4-85cc-5b31063c31ef'],'value':'TSW'},
{'id':'a58fd1e4-c0e4-11e4-85cc-5b31063c31ef','key':['H50100203','BVSW','a58fd1e4-c0e4-11e4-85cc-5b31063c31ef'],'value':'BVSW'},
{'id':'a58ff8f1-c0e4-11e4-85cc-5b31063c31ef','key':['H50100203','CVSW','a58ff8f1-c0e4-11e4-85cc-5b31063c31ef'],'value':'CVSW'},
{'id':'a58ff8f0-c0e4-11e4-85cc-5b31063c31ef','key':['H50100203','GVSW','a58ff8f0-c0e4-11e4-85cc-5b31063c31ef'],'value':'GVSW'},
{'id':'a5929100-c0e4-11e4-85cc-5b31063c31ef','key':['H50100203','IVSW','a5929100-c0e4-11e4-85cc-5b31063c31ef'],'value':'IVSW'},
{'id':'a58fd1e3-c0e4-11e4-85cc-5b31063c31ef','key':['H50100203','VSW','a58fd1e3-c0e4-11e4-85cc-5b31063c31ef'],'value':'VSW'},
{'id':'a58ff8f2-c0e4-11e4-85cc-5b31063c31ef','key':['H50100203','VTSW','a58ff8f2-c0e4-11e4-85cc-5b31063c31ef'],'value':'VTSW'},
{'id':'a5929102-c0e4-11e4-85cc-5b31063c31ef','key':['H50100204','RPSW','a5929102-c0e4-11e4-85cc-5b31063c31ef'],'value':'RPSW'},
{'id':'a5929101-c0e4-11e4-85cc-5b31063c31ef','key':['H50100204','RSW','a5929101-c0e4-11e4-85cc-5b31063c31ef'],'value':'RSW'},
{'id':'a5929103-c0e4-11e4-85cc-5b31063c31ef','key':['H50100205','CNSW','a5929103-c0e4-11e4-85cc-5b31063c31ef'],'value':'CNSW'},
{'id':'a592b810-c0e4-11e4-85cc-5b31063c31ef','key':['H50100206','ISW','a592b810-c0e4-11e4-85cc-5b31063c31ef'],'value':'ISW'},
{'id':'a592b811-c0e4-11e4-85cc-5b31063c31ef','key':['H50100207','MSW','a592b811-c0e4-11e4-85cc-5b31063c31ef'],'value':'MSW'},
{'id':'a592b812-c0e4-11e4-85cc-5b31063c31ef','key':['H50100208','GSW','a592b812-c0e4-11e4-85cc-5b31063c31ef'],'value':'GSW'},
{'id':'a592b813-c0e4-11e4-85cc-5b31063c31ef','key':['H50100209','PESW','a592b813-c0e4-11e4-85cc-5b31063c31ef'],'value':'PESW'},
{'id':'a592b816-c0e4-11e4-85cc-5b31063c31ef','key':['H50100210','CBSW','a592b816-c0e4-11e4-85cc-5b31063c31ef'],'value':'CBSW'},
{'id':'a592b815-c0e4-11e4-85cc-5b31063c31ef','key':['H50100210','PCSW','a592b815-c0e4-11e4-85cc-5b31063c31ef'],'value':'PCSW'},
{'id':'a592b814-c0e4-11e4-85cc-5b31063c31ef','key':['H50100210','USSW','a592b814-c0e4-11e4-85cc-5b31063c31ef'],'value':'USSW'},
{'id':'a592df21-c0e4-11e4-85cc-5b31063c31ef','key':['H50100301','LSS','a592df21-c0e4-11e4-85cc-5b31063c31ef'],'value':'LSS'},
{'id':'a592df22-c0e4-11e4-85cc-5b31063c31ef','key':['H50100302','HSS','a592df22-c0e4-11e4-85cc-5b31063c31ef'],'value':'HSS'},
{'id':'a592df23-c0e4-11e4-85cc-5b31063c31ef','key':['H50100302','TSS','a592df23-c0e4-11e4-85cc-5b31063c31ef'],'value':'TSS'},
{'id':'a592df25-c0e4-11e4-85cc-5b31063c31ef','key':['H50100303','BVSS','a592df25-c0e4-11e4-85cc-5b31063c31ef'],'value':'BVSS'},
{'id':'a5930631-c0e4-11e4-85cc-5b31063c31ef','key':['H50100303','CVSS','a5930631-c0e4-11e4-85cc-5b31063c31ef'],'value':'CVSS'},
{'id':'a5930630-c0e4-11e4-85cc-5b31063c31ef','key':['H50100303','GVSS','a5930630-c0e4-11e4-85cc-5b31063c31ef'],'value':'GVSS'},
{'id':'a5930632-c0e4-11e4-85cc-5b31063c31ef','key':['H50100303','IVSS','a5930632-c0e4-11e4-85cc-5b31063c31ef'],'value':'IVSS'},
{'id':'a592df24-c0e4-11e4-85cc-5b31063c31ef','key':['H50100303','VSS','a592df24-c0e4-11e4-85cc-5b31063c31ef'],'value':'VSS'},
{'id':'a5930634-c0e4-11e4-85cc-5b31063c31ef','key':['H50100304','RPSS','a5930634-c0e4-11e4-85cc-5b31063c31ef'],'value':'RPSS'},
{'id':'a5930633-c0e4-11e4-85cc-5b31063c31ef','key':['H50100304','RSS','a5930633-c0e4-11e4-85cc-5b31063c31ef'],'value':'RSS'},
{'id':'a5930635-c0e4-11e4-85cc-5b31063c31ef','key':['H50100305','CNSS','a5930635-c0e4-11e4-85cc-5b31063c31ef'],'value':'CNSS'},
{'id':'a5930636-c0e4-11e4-85cc-5b31063c31ef','key':['H50100306','ISS','a5930636-c0e4-11e4-85cc-5b31063c31ef'],'value':'ISS'},
{'id':'a5932d40-c0e4-11e4-85cc-5b31063c31ef','key':['H50100307','MSS','a5932d40-c0e4-11e4-85cc-5b31063c31ef'],'value':'MSS'},
{'id':'a5932d41-c0e4-11e4-85cc-5b31063c31ef','key':['H50100308','PESS','a5932d41-c0e4-11e4-85cc-5b31063c31ef'],'value':'PESS'},
{'id':'a5932d44-c0e4-11e4-85cc-5b31063c31ef','key':['H50100309','CBSS','a5932d44-c0e4-11e4-85cc-5b31063c31ef'],'value':'CBSS'},
{'id':'a5932d43-c0e4-11e4-85cc-5b31063c31ef','key':['H50100309','PCSS','a5932d43-c0e4-11e4-85cc-5b31063c31ef'],'value':'PCSS'},
{'id':'a5932d42-c0e4-11e4-85cc-5b31063c31ef','key':['H50100309','USSS','a5932d42-c0e4-11e4-85cc-5b31063c31ef'],'value':'USSS'},
{'id':'a5935450-c0e4-11e4-85cc-5b31063c31ef','key':['H50100401','LOW','a5935450-c0e4-11e4-85cc-5b31063c31ef'],'value':'LOW'},
{'id':'a5935451-c0e4-11e4-85cc-5b31063c31ef','key':['H50100402','HOW','a5935451-c0e4-11e4-85cc-5b31063c31ef'],'value':'HOW'},
{'id':'a5935452-c0e4-11e4-85cc-5b31063c31ef','key':['H50100402','TOW','a5935452-c0e4-11e4-85cc-5b31063c31ef'],'value':'TOW'},
{'id':'a5935454-c0e4-11e4-85cc-5b31063c31ef','key':['H50100403','BVOW','a5935454-c0e4-11e4-85cc-5b31063c31ef'],'value':'BVOW'},
{'id':'a5935456-c0e4-11e4-85cc-5b31063c31ef','key':['H50100403','CVOW','a5935456-c0e4-11e4-85cc-5b31063c31ef'],'value':'CVOW'},
{'id':'a5935455-c0e4-11e4-85cc-5b31063c31ef','key':['H50100403','GVOW','a5935455-c0e4-11e4-85cc-5b31063c31ef'],'value':'GVOW'},
{'id':'a5937b60-c0e4-11e4-85cc-5b31063c31ef','key':['H50100403','IVOW','a5937b60-c0e4-11e4-85cc-5b31063c31ef'],'value':'IVOW'},
{'id':'a5935453-c0e4-11e4-85cc-5b31063c31ef','key':['H50100403','VOW','a5935453-c0e4-11e4-85cc-5b31063c31ef'],'value':'VOW'},
{'id':'a5937b61-c0e4-11e4-85cc-5b31063c31ef','key':['H50100404','ROW','a5937b61-c0e4-11e4-85cc-5b31063c31ef'],'value':'ROW'},
{'id':'a5937b62-c0e4-11e4-85cc-5b31063c31ef','key':['H50100404','RPOW','a5937b62-c0e4-11e4-85cc-5b31063c31ef'],'value':'RPOW'},
{'id':'a5937b63-c0e4-11e4-85cc-5b31063c31ef','key':['H50100405','CNOW','a5937b63-c0e4-11e4-85cc-5b31063c31ef'],'value':'CNOW'},
{'id':'a5937b64-c0e4-11e4-85cc-5b31063c31ef','key':['H50100406','IOW','a5937b64-c0e4-11e4-85cc-5b31063c31ef'],'value':'IOW'},
{'id':'a5937b65-c0e4-11e4-85cc-5b31063c31ef','key':['H50100407','MOW','a5937b65-c0e4-11e4-85cc-5b31063c31ef'],'value':'MOW'},
{'id':'a5937b66-c0e4-11e4-85cc-5b31063c31ef','key':['H50100408','PEOW','a5937b66-c0e4-11e4-85cc-5b31063c31ef'],'value':'PEOW'},
{'id':'a593a272-c0e4-11e4-85cc-5b31063c31ef','key':['H50100409','CBOW','a593a272-c0e4-11e4-85cc-5b31063c31ef'],'value':'CBOW'},
{'id':'a593a271-c0e4-11e4-85cc-5b31063c31ef','key':['H50100409','PCOW','a593a271-c0e4-11e4-85cc-5b31063c31ef'],'value':'PCOW'},
{'id':'a593a270-c0e4-11e4-85cc-5b31063c31ef','key':['H50100409','USOW','a593a270-c0e4-11e4-85cc-5b31063c31ef'],'value':'USOW'},
{'id':'a593a274-c0e4-11e4-85cc-5b31063c31ef','key':['H50100501','LST','a593a274-c0e4-11e4-85cc-5b31063c31ef'],'value':'LST'},
{'id':'a593a275-c0e4-11e4-85cc-5b31063c31ef','key':['H50100502','HST','a593a275-c0e4-11e4-85cc-5b31063c31ef'],'value':'HST'},
{'id':'a593c980-c0e4-11e4-85cc-5b31063c31ef','key':['H50100502','TST','a593c980-c0e4-11e4-85cc-5b31063c31ef'],'value':'TST'},
{'id':'a593c983-c0e4-11e4-85cc-5b31063c31ef','key':['H50100503','BFST','a593c983-c0e4-11e4-85cc-5b31063c31ef'],'value':'BFST'},
{'id':'a593c982-c0e4-11e4-85cc-5b31063c31ef','key':['H50100503','BVST','a593c982-c0e4-11e4-85cc-5b31063c31ef'],'value':'BVST'},
{'id':'a593c985-c0e4-11e4-85cc-5b31063c31ef','key':['H50100503','CVST','a593c985-c0e4-11e4-85cc-5b31063c31ef'],'value':'CVST'},
{'id':'a593c984-c0e4-11e4-85cc-5b31063c31ef','key':['H50100503','GVST','a593c984-c0e4-11e4-85cc-5b31063c31ef'],'value':'GVST'},
{'id':'a593f090-c0e4-11e4-85cc-5b31063c31ef','key':['H50100503','IVST','a593f090-c0e4-11e4-85cc-5b31063c31ef'],'value':'IVST'},
{'id':'a593c981-c0e4-11e4-85cc-5b31063c31ef','key':['H50100503','VST','a593c981-c0e4-11e4-85cc-5b31063c31ef'],'value':'VST'},
{'id':'a593f091-c0e4-11e4-85cc-5b31063c31ef','key':['H50100503','VTST','a593f091-c0e4-11e4-85cc-5b31063c31ef'],'value':'VTST'},
{'id':'a593f093-c0e4-11e4-85cc-5b31063c31ef','key':['H50100504','RPST','a593f093-c0e4-11e4-85cc-5b31063c31ef'],'value':'RPST'},
{'id':'a593f092-c0e4-11e4-85cc-5b31063c31ef','key':['H50100504','RST','a593f092-c0e4-11e4-85cc-5b31063c31ef'],'value':'RST'},
{'id':'a593f094-c0e4-11e4-85cc-5b31063c31ef','key':['H50100505','CNST','a593f094-c0e4-11e4-85cc-5b31063c31ef'],'value':'CNST'},
{'id':'a593f095-c0e4-11e4-85cc-5b31063c31ef','key':['H50100506','IST','a593f095-c0e4-11e4-85cc-5b31063c31ef'],'value':'IST'},
{'id':'a593f096-c0e4-11e4-85cc-5b31063c31ef','key':['H50100507','MST','a593f096-c0e4-11e4-85cc-5b31063c31ef'],'value':'MST'},
{'id':'a59417a1-c0e4-11e4-85cc-5b31063c31ef','key':['H50100508','PCST','a59417a1-c0e4-11e4-85cc-5b31063c31ef'],'value':'PCST'},
{'id':'a59417a0-c0e4-11e4-85cc-5b31063c31ef','key':['H50100508','USST','a59417a0-c0e4-11e4-85cc-5b31063c31ef'],'value':'USST'},
{'id':'a59417a3-c0e4-11e4-85cc-5b31063c31ef','key':['H50100601','LCA','a59417a3-c0e4-11e4-85cc-5b31063c31ef'],'value':'LCA'},
{'id':'a59417a4-c0e4-11e4-85cc-5b31063c31ef','key':['H50100602','HCA','a59417a4-c0e4-11e4-85cc-5b31063c31ef'],'value':'HCA'},
{'id':'a59417a5-c0e4-11e4-85cc-5b31063c31ef','key':['H50100602','TCA','a59417a5-c0e4-11e4-85cc-5b31063c31ef'],'value':'TCA'},
{'id':'a5943eb1-c0e4-11e4-85cc-5b31063c31ef','key':['H50100603','BVCA','a5943eb1-c0e4-11e4-85cc-5b31063c31ef'],'value':'BVCA'},
{'id':'a5943eb3-c0e4-11e4-85cc-5b31063c31ef','key':['H50100603','CVCA','a5943eb3-c0e4-11e4-85cc-5b31063c31ef'],'value':'CVCA'},
{'id':'a5943eb2-c0e4-11e4-85cc-5b31063c31ef','key':['H50100603','GVCA','a5943eb2-c0e4-11e4-85cc-5b31063c31ef'],'value':'GVCA'},
{'id':'a5943eb4-c0e4-11e4-85cc-5b31063c31ef','key':['H50100603','IVCA','a5943eb4-c0e4-11e4-85cc-5b31063c31ef'],'value':'IVCA'},
{'id':'a5943eb0-c0e4-11e4-85cc-5b31063c31ef','key':['H50100603','VCA','a5943eb0-c0e4-11e4-85cc-5b31063c31ef'],'value':'VCA'},
{'id':'a5943eb5-c0e4-11e4-85cc-5b31063c31ef','key':['H50100603','VTCA','a5943eb5-c0e4-11e4-85cc-5b31063c31ef'],'value':'VTCA'},
{'id':'a5943eb6-c0e4-11e4-85cc-5b31063c31ef','key':['H50100604','RCA','a5943eb6-c0e4-11e4-85cc-5b31063c31ef'],'value':'RCA'},
{'id':'a59465c0-c0e4-11e4-85cc-5b31063c31ef','key':['H50100604','RPCA','a59465c0-c0e4-11e4-85cc-5b31063c31ef'],'value':'RPCA'},
{'id':'a59465c1-c0e4-11e4-85cc-5b31063c31ef','key':['H50100605','CNCA','a59465c1-c0e4-11e4-85cc-5b31063c31ef'],'value':'CNCA'},
{'id':'a59465c2-c0e4-11e4-85cc-5b31063c31ef','key':['H50100606','MCA','a59465c2-c0e4-11e4-85cc-5b31063c31ef'],'value':'MCA'},
{'id':'a59465c4-c0e4-11e4-85cc-5b31063c31ef','key':['H50100607','PCCA','a59465c4-c0e4-11e4-85cc-5b31063c31ef'],'value':'PCCA'},
{'id':'a59465c3-c0e4-11e4-85cc-5b31063c31ef','key':['H50100607','USCA','a59465c3-c0e4-11e4-85cc-5b31063c31ef'],'value':'USCA'},
{'id':'a5948cd0-c0e4-11e4-85cc-5b31063c31ef','key':['H50100701','LDS','a5948cd0-c0e4-11e4-85cc-5b31063c31ef'],'value':'LDS'},
{'id':'a59465c6-c0e4-11e4-85cc-5b31063c31ef','key':['H50100701','LFL','a59465c6-c0e4-11e4-85cc-5b31063c31ef'],'value':'LFL'},
{'id':'a5948cd1-c0e4-11e4-85cc-5b31063c31ef','key':['H50100701','LJP','a5948cd1-c0e4-11e4-85cc-5b31063c31ef'],'value':'LJP'},
{'id':'a5948cd2-c0e4-11e4-85cc-5b31063c31ef','key':['H50100701','RDS','a5948cd2-c0e4-11e4-85cc-5b31063c31ef'],'value':'RDS'},
{'id':'a5948cd3-c0e4-11e4-85cc-5b31063c31ef','key':['H50100701','RJP','a5948cd3-c0e4-11e4-85cc-5b31063c31ef'],'value':'RJP'},
{'id':'a5948cd4-c0e4-11e4-85cc-5b31063c31ef','key':['H50100701','RPDS','a5948cd4-c0e4-11e4-85cc-5b31063c31ef'],'value':'RPDS'},
{'id':'a5948cd5-c0e4-11e4-85cc-5b31063c31ef','key':['H50100701','RPJP','a5948cd5-c0e4-11e4-85cc-5b31063c31ef'],'value':'RPJP'},
{'id':'a594b3e2-c0e4-11e4-85cc-5b31063c31ef','key':['H50100702','HDS','a594b3e2-c0e4-11e4-85cc-5b31063c31ef'],'value':'HDS'},
{'id':'a594b3e0-c0e4-11e4-85cc-5b31063c31ef','key':['H50100702','HFL','a594b3e0-c0e4-11e4-85cc-5b31063c31ef'],'value':'HFL'},
{'id':'a594b3e4-c0e4-11e4-85cc-5b31063c31ef','key':['H50100702','HJP','a594b3e4-c0e4-11e4-85cc-5b31063c31ef'],'value':'HJP'},
{'id':'a594b3e3-c0e4-11e4-85cc-5b31063c31ef','key':['H50100702','TDS','a594b3e3-c0e4-11e4-85cc-5b31063c31ef'],'value':'TDS'},
{'id':'a594b3e1-c0e4-11e4-85cc-5b31063c31ef','key':['H50100702','TFL','a594b3e1-c0e4-11e4-85cc-5b31063c31ef'],'value':'TFL'},
{'id':'a594b3e5-c0e4-11e4-85cc-5b31063c31ef','key':['H50100702','TJP','a594b3e5-c0e4-11e4-85cc-5b31063c31ef'],'value':'TJP'},
{'id':'a594daf5-c0e4-11e4-85cc-5b31063c31ef','key':['H50100703','BVDS','a594daf5-c0e4-11e4-85cc-5b31063c31ef'],'value':'BVDS'},
{'id':'a594daf0-c0e4-11e4-85cc-5b31063c31ef','key':['H50100703','BVFL','a594daf0-c0e4-11e4-85cc-5b31063c31ef'],'value':'BVFL'},
{'id':'a5950204-c0e4-11e4-85cc-5b31063c31ef','key':['H50100703','BVJP','a5950204-c0e4-11e4-85cc-5b31063c31ef'],'value':'BVJP'},
{'id':'a5950201-c0e4-11e4-85cc-5b31063c31ef','key':['H50100703','CVDS','a5950201-c0e4-11e4-85cc-5b31063c31ef'],'value':'CVDS'},
{'id':'a594daf2-c0e4-11e4-85cc-5b31063c31ef','key':['H50100703','CVFL','a594daf2-c0e4-11e4-85cc-5b31063c31ef'],'value':'CVFL'},
{'id':'a5950206-c0e4-11e4-85cc-5b31063c31ef','key':['H50100703','CVJP','a5950206-c0e4-11e4-85cc-5b31063c31ef'],'value':'CVJP'},
{'id':'a5950200-c0e4-11e4-85cc-5b31063c31ef','key':['H50100703','GVDS','a5950200-c0e4-11e4-85cc-5b31063c31ef'],'value':'GVDS'},
{'id':'a594daf1-c0e4-11e4-85cc-5b31063c31ef','key':['H50100703','GVFL','a594daf1-c0e4-11e4-85cc-5b31063c31ef'],'value':'GVFL'},
{'id':'a5950205-c0e4-11e4-85cc-5b31063c31ef','key':['H50100703','GVJP','a5950205-c0e4-11e4-85cc-5b31063c31ef'],'value':'GVJP'},
{'id':'a5950202-c0e4-11e4-85cc-5b31063c31ef','key':['H50100703','IVDS','a5950202-c0e4-11e4-85cc-5b31063c31ef'],'value':'IVDS'},
{'id':'a594daf3-c0e4-11e4-85cc-5b31063c31ef','key':['H50100703','IVFL','a594daf3-c0e4-11e4-85cc-5b31063c31ef'],'value':'IVFL'},
{'id':'a5952910-c0e4-11e4-85cc-5b31063c31ef','key':['H50100703','IVJP','a5952910-c0e4-11e4-85cc-5b31063c31ef'],'value':'IVJP'},
{'id':'a594daf4-c0e4-11e4-85cc-5b31063c31ef','key':['H50100703','VDS','a594daf4-c0e4-11e4-85cc-5b31063c31ef'],'value':'VDS'},
{'id':'a594b3e6-c0e4-11e4-85cc-5b31063c31ef','key':['H50100703','VFL','a594b3e6-c0e4-11e4-85cc-5b31063c31ef'],'value':'VFL'},
{'id':'a5950203-c0e4-11e4-85cc-5b31063c31ef','key':['H50100703','VJP','a5950203-c0e4-11e4-85cc-5b31063c31ef'],'value':'VJP'},
{'id':'a5952912-c0e4-11e4-85cc-5b31063c31ef','key':['H50100704','FHDS','a5952912-c0e4-11e4-85cc-5b31063c31ef'],'value':'FHDS'},
{'id':'a5952911-c0e4-11e4-85cc-5b31063c31ef','key':['H50100704','FHFL','a5952911-c0e4-11e4-85cc-5b31063c31ef'],'value':'FHFL'},
{'id':'a5952913-c0e4-11e4-85cc-5b31063c31ef','key':['H50100704','FHJP','a5952913-c0e4-11e4-85cc-5b31063c31ef'],'value':'FHJP'},
{'id':'a5952915-c0e4-11e4-85cc-5b31063c31ef','key':['H50100705','LADS','a5952915-c0e4-11e4-85cc-5b31063c31ef'],'value':'LADS'},
{'id':'a5952914-c0e4-11e4-85cc-5b31063c31ef','key':['H50100705','LAFL','a5952914-c0e4-11e4-85cc-5b31063c31ef'],'value':'LAFL'},
{'id':'a5955020-c0e4-11e4-85cc-5b31063c31ef','key':['H50100705','LAJP','a5955020-c0e4-11e4-85cc-5b31063c31ef'],'value':'LAJP'},
{'id':'a5955022-c0e4-11e4-85cc-5b31063c31ef','key':['H50100706','MDS','a5955022-c0e4-11e4-85cc-5b31063c31ef'],'value':'MDS'},
{'id':'a5955021-c0e4-11e4-85cc-5b31063c31ef','key':['H50100706','MFL','a5955021-c0e4-11e4-85cc-5b31063c31ef'],'value':'MFL'},
{'id':'a5955023-c0e4-11e4-85cc-5b31063c31ef','key':['H50100706','MJP','a5955023-c0e4-11e4-85cc-5b31063c31ef'],'value':'MJP'},
{'id':'a5955024-c0e4-11e4-85cc-5b31063c31ef','key':['H50100707','OFC','a5955024-c0e4-11e4-85cc-5b31063c31ef'],'value':'OFC'},
{'id':'a5955026-c0e4-11e4-85cc-5b31063c31ef','key':['H50100806','OMMC','a5955026-c0e4-11e4-85cc-5b31063c31ef'],'value':'OMMC'},
{'id':'a5957730-c0e4-11e4-85cc-5b31063c31ef','key':['H501009','OCMU','a5957730-c0e4-11e4-85cc-5b31063c31ef'],'value':'OCMU'},
{'id':'a5957731-c0e4-11e4-85cc-5b31063c31ef','key':['H501009','RUNK','a5957731-c0e4-11e4-85cc-5b31063c31ef'],'value':'RUNK'},
{'id':'a5957734-c0e4-11e4-85cc-5b31063c31ef','key':['H50200101','TFR','a5957734-c0e4-11e4-85cc-5b31063c31ef'],'value':'TFR'},
{'id':'a5957735-c0e4-11e4-85cc-5b31063c31ef','key':['H50200102','CBK','a5957735-c0e4-11e4-85cc-5b31063c31ef'],'value':'CBK'},
{'id':'a5957736-c0e4-11e4-85cc-5b31063c31ef','key':['H50200103','LPWR','a5957736-c0e4-11e4-85cc-5b31063c31ef'],'value':'LPWR'},
{'id':'a5959e41-c0e4-11e4-85cc-5b31063c31ef','key':['H50200104','PDS','a5959e41-c0e4-11e4-85cc-5b31063c31ef'],'value':'PDS'},
{'id':'a5959e40-c0e4-11e4-85cc-5b31063c31ef','key':['H50200104','REC','a5959e40-c0e4-11e4-85cc-5b31063c31ef'],'value':'REC'},
{'id':'a5959e42-c0e4-11e4-85cc-5b31063c31ef','key':['H50200105','AMP','a5959e42-c0e4-11e4-85cc-5b31063c31ef'],'value':'AMP'},
{'id':'a5959e43-c0e4-11e4-85cc-5b31063c31ef','key':['H50200105','EM','a5959e43-c0e4-11e4-85cc-5b31063c31ef'],'value':'EM'},
{'id':'a5959e44-c0e4-11e4-85cc-5b31063c31ef','key':['H50200105','ESS','a5959e44-c0e4-11e4-85cc-5b31063c31ef'],'value':'ESS'},
{'id':'a5959e45-c0e4-11e4-85cc-5b31063c31ef','key':['H50200105','LS','a5959e45-c0e4-11e4-85cc-5b31063c31ef'],'value':'LS'},
{'id':'a5959e46-c0e4-11e4-85cc-5b31063c31ef','key':['H50200105','PNL','a5959e46-c0e4-11e4-85cc-5b31063c31ef'],'value':'PNL'},
{'id':'a595c550-c0e4-11e4-85cc-5b31063c31ef','key':['H50200106','HPWR','a595c550-c0e4-11e4-85cc-5b31063c31ef'],'value':'HPWR'},
{'id':'a595c551-c0e4-11e4-85cc-5b31063c31ef','key':['H50200107','TPWR','a595c551-c0e4-11e4-85cc-5b31063c31ef'],'value':'TPWR'},
{'id':'a595c553-c0e4-11e4-85cc-5b31063c31ef','key':['H502002','COM','a595c553-c0e4-11e4-85cc-5b31063c31ef'],'value':'COM'},
{'id':'a595c554-c0e4-11e4-85cc-5b31063c31ef','key':['H50200201','FOP','a595c554-c0e4-11e4-85cc-5b31063c31ef'],'value':'FOP'},
{'id':'a595c555-c0e4-11e4-85cc-5b31063c31ef','key':['H50200202','LCOM','a595c555-c0e4-11e4-85cc-5b31063c31ef'],'value':'LCOM'},
{'id':'a595c556-c0e4-11e4-85cc-5b31063c31ef','key':['H50200203','N/A','a595c556-c0e4-11e4-85cc-5b31063c31ef'],'value':'N/A'},
{'id':'a595ec60-c0e4-11e4-85cc-5b31063c31ef','key':['H50200204','CTV','a595ec60-c0e4-11e4-85cc-5b31063c31ef'],'value':'CTV'},
{'id':'a595ec61-c0e4-11e4-85cc-5b31063c31ef','key':['H50200204','LCTV','a595ec61-c0e4-11e4-85cc-5b31063c31ef'],'value':'LCTV'},
{'id':'a595ec62-c0e4-11e4-85cc-5b31063c31ef','key':['H50200205','AS','a595ec62-c0e4-11e4-85cc-5b31063c31ef'],'value':'AS'},
{'id':'a595ec63-c0e4-11e4-85cc-5b31063c31ef','key':['H50200206','MPWR','a595ec63-c0e4-11e4-85cc-5b31063c31ef'],'value':'MPWR'},
{'id':'a595ec64-c0e4-11e4-85cc-5b31063c31ef','key':['H50200207','HCOM','a595ec64-c0e4-11e4-85cc-5b31063c31ef'],'value':'HCOM'},
{'id':'a595ec65-c0e4-11e4-85cc-5b31063c31ef','key':['H50200208','TCOM','a595ec65-c0e4-11e4-85cc-5b31063c31ef'],'value':'TCOM'},
{'id':'a5961371-c0e4-11e4-85cc-5b31063c31ef','key':['H502003','DLT','a5961371-c0e4-11e4-85cc-5b31063c31ef'],'value':'DLT'},
{'id':'a5961370-c0e4-11e4-85cc-5b31063c31ef','key':['H502003','HML','a5961370-c0e4-11e4-85cc-5b31063c31ef'],'value':'HML'},
{'id':'a595ec66-c0e4-11e4-85cc-5b31063c31ef','key':['H502003','LGT','a595ec66-c0e4-11e4-85cc-5b31063c31ef'],'value':'LGT'},
{'id':'a5961372-c0e4-11e4-85cc-5b31063c31ef','key':['H502004','LTP','a5961372-c0e4-11e4-85cc-5b31063c31ef'],'value':'LTP'},
{'id':'a5961373-c0e4-11e4-85cc-5b31063c31ef','key':['H502005','CBL','a5961373-c0e4-11e4-85cc-5b31063c31ef'],'value':'CBL'},
{'id':'a5961374-c0e4-11e4-85cc-5b31063c31ef','key':['H502006','OEU','a5961374-c0e4-11e4-85cc-5b31063c31ef'],'value':'OEU'},
{'id':'a5961375-c0e4-11e4-85cc-5b31063c31ef','key':['H502006','RUNK','a5961375-c0e4-11e4-85cc-5b31063c31ef'],'value':'RUNK'},
{'id':'a5963a82-c0e4-11e4-85cc-5b31063c31ef','key':['H50300101','LFP','a5963a82-c0e4-11e4-85cc-5b31063c31ef'],'value':'LFP'},
{'id':'a5963a83-c0e4-11e4-85cc-5b31063c31ef','key':['H50300102','HFP','a5963a83-c0e4-11e4-85cc-5b31063c31ef'],'value':'HFP'},
{'id':'a5963a84-c0e4-11e4-85cc-5b31063c31ef','key':['H50300102','TFP','a5963a84-c0e4-11e4-85cc-5b31063c31ef'],'value':'TFP'},
{'id':'a5963a85-c0e4-11e4-85cc-5b31063c31ef','key':['H50300103','SPR','a5963a85-c0e4-11e4-85cc-5b31063c31ef'],'value':'SPR'},
{'id':'a5963a86-c0e4-11e4-85cc-5b31063c31ef','key':['H50300104','HYD','a5963a86-c0e4-11e4-85cc-5b31063c31ef'],'value':'HYD'},
{'id':'a5966190-c0e4-11e4-85cc-5b31063c31ef','key':['H50300105','FH','a5966190-c0e4-11e4-85cc-5b31063c31ef'],'value':'FH'},
{'id':'a5966191-c0e4-11e4-85cc-5b31063c31ef','key':['H50300106','GVFP','a5966191-c0e4-11e4-85cc-5b31063c31ef'],'value':'GVFP'},
{'id':'a5966192-c0e4-11e4-85cc-5b31063c31ef','key':['H50300107','BPFP','a5966192-c0e4-11e4-85cc-5b31063c31ef'],'value':'BPFP'},
{'id':'a5966193-c0e4-11e4-85cc-5b31063c31ef','key':['H503002','FA','a5966193-c0e4-11e4-85cc-5b31063c31ef'],'value':'FA'},
{'id':'a5966194-c0e4-11e4-85cc-5b31063c31ef','key':['H503003','OFPS','a5966194-c0e4-11e4-85cc-5b31063c31ef'],'value':'OFPS'},
{'id':'a5966195-c0e4-11e4-85cc-5b31063c31ef','key':['H503003','RUNK','a5966195-c0e4-11e4-85cc-5b31063c31ef'],'value':'RUNK'}
]

*/
 

/
