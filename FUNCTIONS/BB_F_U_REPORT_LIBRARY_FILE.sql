--------------------------------------------------------
--  DDL for Function BB_F_U_REPORT_LIBRARY_FILE
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE FUNCTION "SPIDERS_DATA"."BB_F_U_REPORT_LIBRARY_FILE" (in_jJson json, o_jMessages out json) return varchar2 as 

    lc_jJson json;
    lc_data json_list;
    lc_rec_val json_value;
    lc_rec json;
   
    lc_jValue json;
    lc_jvValue json_value;
    
    lc_nREPORTFILEID number;
    lc_vFILENAME varchar2(255);
    lc_vREPORT_NAME varchar2(255);
    
    lc_nSequence number;
    lc_nContinue number :=0;
    lc_nCountOfRecords number :=0;
    
    
    lc_jlDML json_list;
    lc_jResults json;
    lc_jMessages json;
    lc_vDMLResult varchar2(255);
    lc_ret_str varchar(512);
    lc_ret json;

    thesql varchar2(2000) := '';

    lc_vWhere varchar2(2000);

    --error stuff
    lc_jError json;
    lc_jlMessages json_list;
    json_error EXCEPTION;


BEGIN
/*
stub data
{"module":"reportLibrary","target":"createReportLibraryFile","data":[{"REPORTID":12345,"FILENAME":"Preliminary_WBS_SSD_Critigen.pdf","REPORT_NAME":"test"}]}
*/
lc_jResults := json();
lc_jlDML := json_list();

if(in_jJson.exist('data')) then
    if(in_jJson.get('data').is_array) then
        lc_data := json_list(in_jJson.get('data'));        
        lc_rec_val := lc_data.get(1); --return null on outofbounds


        if (lc_rec_val is not null) then
        
            lc_jValue := json(lc_rec_val);
            
            
            if (lc_jValue.exist('REPORTFILEID')) then
                lc_jvValue := lc_jValue.get('REPORTFILEID');
                lc_nREPORTFILEID := getVal(lc_jvValue);
                --htp.p('lc_nREPORTID : ' || lc_nREPORTID);
            end if;
            
            if (lc_jValue.exist('REPORT_NAME')) then
                lc_jvValue := lc_jValue.get('REPORT_NAME');
                lc_vREPORT_NAME := getVal(lc_jvValue);
                --htp.p('lc_vREPORT_NAME : ' || lc_vREPORT_NAME);
            end if;
            
            lc_nContinue := 1;
            
        end if;
    end if;
end if;




if lc_nContinue = 1 then

  if lc_nREPORTFILEID > 0 then
  
    --update record 
    thesql := '
      update 
        spd_t_report_files 
      set 
        report_name = ''' || lc_vREPORT_NAME || '''
      where 
        reportfileid = ' || lc_nREPORTFILEID;

    lc_jlDML.append(thesql);
    lc_jResults.put('results',lc_jlDML);
    lc_vDMLResult := BB_F_DML(lc_jResults, lc_jMessages);
    o_jMessages := lc_jMessages;
    --not a dupe, make the record;
  else
    --dupe do nothing
    lc_jMessages := json();
    lc_jMessages.put('error','no reportfileid');
    lc_vDMLResult := 'n';
    o_jMessages := lc_jMessages;
  end if;
  
  
  
  return lc_vDMLResult;
else 
  raise json_error;
end if;


exception
when json_error then
    --htp.p('raised json_error in update func');
    lc_jError := json();
    lc_jError.put('function','BB_F_U_REPORT_LIBRARY_FILE');
    lc_jError.put('input',in_jJson.to_char);
    lc_jError.put('messages',lc_jlMessages);
    BB_P_ERROR(lc_jError);
    return 'e';
    --htp.p('returning n');
when others then
    --htp.p('this ' || SQLERRM);
    lc_jError := json();
    lc_jlMessages := json_list();
    lc_jError.put('function','BB_F_U_REPORT_LIBRARY_FILE');
    lc_jError.put('input','');
    lc_jlMessages.append(json_list('[{error:''URLFL.000X ' || SQLERRM || '''}]'));
    lc_jError.put('messages',lc_jlMessages);
    BB_P_ERROR(lc_jError);
    return 'e';


END BB_F_U_REPORT_LIBRARY_FILE;
 

/
