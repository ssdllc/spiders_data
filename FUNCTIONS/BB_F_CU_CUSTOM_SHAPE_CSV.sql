--------------------------------------------------------
--  DDL for Function BB_F_CU_CUSTOM_SHAPE_CSV
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE FUNCTION "SPIDERS_DATA"."BB_F_CU_CUSTOM_SHAPE_CSV" (in_jJson json, o_jMessages out json) return varchar2 as 

    lc_jJson json;
    lc_data json_list;
    lc_rec_val json_value;
    lc_rec json;
    lc_jValue json;
    lc_jvValue json_value;
    
--CUSTOMIZE FOR TABLE FIELDS
    --none
--END CUSTOMIZE FOR TABLE FIELDS
    
    
    lc_nSequence number;
    lc_nContinue number :=0;
    
    
    lc_jlDML json_list;
    lc_jResults json;
    lc_jMessages json;
    lc_vDMLResult varchar2(255);
    lc_ret_str varchar(512);
    lc_ret json;

    thesql varchar2(2000) := '';

    lc_vWhere varchar2(2000);

    --error stuff
    lc_jError json;
    lc_jlMessages json_list;
    json_error EXCEPTION;


BEGIN
/*
stub data
{module:"customShapes", target:"createCustomShape",data:[{{"ID":123,"CODE":"CL01","DESCRIPTION":"cleat","LENGTH":25,"WIDTH":3,"HEIGHT":5,"RED":0,"GREEN":255,"BLUE":0,"SCENEID":2222}]}
*/

lc_jResults := json();
lc_jlDML := json_list();

  
  thesql := '
    insert into 
      spd_t_tele_custom_shapes 
    ( SCENEMODELID,CODE,DESCRIPTION,LENGTH,WIDTH,HEIGHT,RED,GREEN,BLUE,TRANSPARENCY,RADIUS,RELATED_PLATFORM,CREATED_BY)
    
    select SCENEMODELID,CODE,DESCRIPTION,LENGTH,WIDTH,HEIGHT,RED,GREEN,BLUE,TRANSPARENCY,RADIUS,RELATED_PLATFORM,CREATED_BY
    from spd_t_tele_custom_shapes_tmp
    where
    created_by = ''' || v('APP_USER') || '''';
    

  
  lc_jlDML.append(thesql);
  lc_jResults.put('results',lc_jlDML);
  lc_vDMLResult := BB_F_DML(lc_jResults, lc_jMessages);
  o_jMessages := lc_jMessages;

  
  return lc_vDMLResult;



exception
when json_error then
    --htp.p('raised json_error in update func');
    lc_jlMessages:=json_list();
    lc_jError := json();
    lc_jError.put('function','BB_F_CU_CUSTOM_SHAPE_CSV');
    lc_jError.put('input',in_jJson.to_char);
    lc_jError.put('messages',lc_jlMessages);
    BB_P_ERROR(lc_jError);
    return 'e';
    --htp.p('returning n');
when others then
    lc_jError := json();
    lc_jlMessages := json_list();
    lc_jError.put('function','BB_F_CU_CUSTOM_SHAPE_CSV');
    lc_jError.put('input','');
    lc_jlMessages.append(json_list('[{error:''CCS.000X ' || SQLERRM || '''}]'));
    lc_jError.put('messages',lc_jlMessages);
    BB_P_ERROR(lc_jError);
    return 'e';


END BB_F_CU_CUSTOM_SHAPE_CSV;
 

/
