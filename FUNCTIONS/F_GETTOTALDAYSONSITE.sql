--------------------------------------------------------
--  DDL for Function F_GETTOTALDAYSONSITE
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE FUNCTION "SPIDERS_DATA"."F_GETTOTALDAYSONSITE" (in_nExectableID number) RETURN varchar2 AS 
	lc_vFunctionName varchar2(255) := 'f_getTotalDaysOnsite';
	lc_vReturn varchar2(200) := '-999'; --defaul the return to e for error
	
	BEGIN

    for rec in (select value from spd_t_xml_et_metadata where exectableid = in_nExectableID and display='Total Number Of Days On-site')
    loop
      lc_vReturn := rec.value;
    end loop;
		
	RETURN lc_vReturn;

	EXCEPTION WHEN OTHERS THEN
		execute immediate 'insert into debug (FUNCTIONNAME, INPUT, MESSAGE) values (''' || lc_vFunctionName || ''', '|| 'An error occured '|| ''','''|| sqlerrm ||''')';
		RETURN '-998';
END f_getTotalDaysOnsite;
 

/
