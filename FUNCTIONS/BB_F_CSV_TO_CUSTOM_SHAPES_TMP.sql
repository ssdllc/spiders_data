--------------------------------------------------------
--  DDL for Function BB_F_CSV_TO_CUSTOM_SHAPES_TMP
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE FUNCTION "SPIDERS_DATA"."BB_F_CSV_TO_CUSTOM_SHAPES_TMP" (in_cClob clob) return varchar2 as 


    lc_jJson json;
    lc_data json_list;
    lc_rec_val json_value;
    lc_rec json;
    o_jMessages json;
   
    lc_jValue json;
    lc_jvValue json_value;
    
    
    lc_nSequence number;
    lc_nContinue number :=0;
    lc_nCountOfRecords number :=0;
    
    
    lc_jlDML json_list;
    lc_jResults json;
    lc_jMessages json;
    lc_vDMLResult varchar2(255);
    lc_ret_str varchar(512);
    lc_ret json;

    thesql varchar2(2000) := '';

    lc_vWhere varchar2(2000);

    --error stuff
    lc_jError json;
    lc_jlMessages json_list;
    json_error EXCEPTION;



    lc_cClob clob;
    lc_bBlob blob;
    lc_nScenemodelid number;
    lc_vCreatedBy varchar2(255) :='';
    lc_nCount number :=1;
    lc_nSceneid number;
    
    lc_vReturn varchar2(4000) := 'sdfsdf';
    


BEGIN
lc_cClob := in_cClob;
lc_jlDML := json_list();
lc_jResults := json();
lc_jMessages := json();

lc_nSceneid := v('SCENE_ID');
lc_vCreatedBy := v('APP_USER'); 

thesql := 'delete from spd_t_tele_custom_shapes_tmp where created_by = ''' || lc_vCreatedBy || '''';
lc_jlDML.append(thesql);

--lc_vReturn := in_cClob;

  for rec in (select * from table(csv_util_pkg.clob_to_csv(lc_cClob)))  
  loop
    if lc_nCount > 1 then
    
      select SPD_S_TELE_CUSTOM_SHAPES_TMP.nextval into lc_nSequence from dual;
      --Insert record into table
      thesql := 'insert into spd_t_tele_custom_shapes_tmp (SCENEMODELID, CREATED_BY, CODE,	DESCRIPTION, LENGTH,	WIDTH,	HEIGHT,	RED, GREEN, BLUE,	TRANSPARENCY)
        values
        (
          ' || lc_nSceneid || ', 
          ''' || lc_vCreatedBy || ''', 
          ''' || rec.c001 || ''', 
          ''' || rec.c002 || ''', 
          ' || nvl(rec.c003,'null') || ', 
          ' || nvl(rec.c004,'null') || ', 
          ' || nvl(rec.c005,'null') || ', 
          ' || fGetRedValue(rec.c006) || ', 
          ' || fGetGreenValue(rec.c006) || ',
          ' || fGetBlueValue(rec.c006) || ', 
          ' || nvl(rec.c007,35) || ')';
  
      --insert into debug (input) values (theSql);
      
      if rec.c003 || rec.c004 || rec.c005 <> '  ' then
        lc_jlDML.append(thesql);
      end if;
      
    end if;
    lc_nCount := lc_nCount + 1;
  end loop;
  

      lc_jResults.put('results',lc_jlDML);
      lc_vDMLResult := BB_F_DML(lc_jResults, lc_jMessages);
      lc_jMessages.put('id',lc_nSequence);
      o_jMessages := lc_jMessages;
      
      
lc_vReturn := lc_vDMLResult;
return lc_vReturn;

exception
when json_error then
    --htp.p('raised json_error in update func');
    lc_jError := json();
    lc_jError.put('function','BB_F_CSV_TO_CUSTOM_SHAPES_TMP');
    lc_jError.put('input','csv file ' || lc_nSceneid);
    lc_jError.put('messages',lc_jlMessages);
    BB_P_ERROR(lc_jError);
    return 'e';
    --htp.p('returning n');
when others then
    --htp.p('this ' || SQLERRM);
    lc_jError := json();
    lc_jlMessages := json_list();
    lc_jError.put('function','BB_F_CSV_TO_CUSTOM_SHAPES_TMP');
    lc_jError.put('input','');
    lc_jlMessages.append(json_list('[{error:''CSVTCST.000X ' || SQLERRM || '''}]'));
    lc_jError.put('messages',lc_jlMessages);
    BB_P_ERROR(lc_jError);
    return 'e';
end;
 

/
