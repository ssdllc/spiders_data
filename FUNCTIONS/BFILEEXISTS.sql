--------------------------------------------------------
--  DDL for Function BFILEEXISTS
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE FUNCTION "SPIDERS_DATA"."BFILEEXISTS" (in_bBFILE bfile) return number

as 

lc_nReturn number := 0;

begin

  lc_nReturn := dbms_lob.fileexists(in_bBFILE);

  return lc_nReturn;

exception 
    when others then
      return 2;

end;
  
  
 

/
