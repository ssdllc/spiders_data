--------------------------------------------------------
--  DDL for Function BB_F_AUTHORIZATION
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE FUNCTION "SPIDERS_DATA"."BB_F_AUTHORIZATION" (in_vRestEndpoint varchar2, in_vModule varchar2) return VARCHAR2 as
  lc_nCount number :=0;
  --for errors
  lc_jError json;
  lc_jlMessages json_list;

begin
--htp.p(upper(in_vRestEndpoint || '.' || in_vModule));
  --insert into debug (input) values ('AUTH: ' || in_vRestEndpoint || ' : ' || in_vModule || ' : ' || v('APP_USER'));
  CASE upper(in_vModule)
    when 'REPORTLIBRARY' then
      lc_nCount :=1;
    when 'FACILITYBOOK' then
      lc_nCount :=1;
    when 'DRYDOCK' then
      lc_nCount :=1;
    else
      select
        count(*) into lc_nCount
      from
        ssd_security_users
      where
        securitygroupid = (select securitygroupid from ssd_security_objects where upper(object_name) = upper(in_vRestEndpoint || '.' || in_vModule))
        and upper(employeeid) = v('APP_USER');
  end case;


    --owa_util.get_cgi_env('REMOTE_USER'); --> FOR PORTAL      --v('APP_USER'); --> FOR FIDO
  return 'y';
  /**
  if(lc_nCount=1) then
    return 'y';
  else
    return 'n';
  end if;
  */


  exception
    when others then
        lc_jError := json();
        lc_jError.put('function','authorization');
        lc_jError.put('input',in_vRestEndpoint || ',' || in_vModule);
        lc_jlMessages := json_list();
        lc_jlMessages.append(json_list('[{error:''AUTH-0001 ' || SQLERRM || '''}]'));
        lc_jError.put('messages',lc_jlMessages);
        BB_P_ERROR(lc_jError);

end;

/
