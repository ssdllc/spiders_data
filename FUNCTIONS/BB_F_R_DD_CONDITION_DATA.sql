--------------------------------------------------------
--  DDL for Function BB_F_R_DD_CONDITION_DATA
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE FUNCTION "SPIDERS_DATA"."BB_F_R_DD_CONDITION_DATA" (in_jJson json) RETURN json AS

  lc_jJson json;
  lc_data json_list;
  lc_rec_val json_value;
  lc_jValue json;
  lc_rec json;
  lc_jvValue json_value; --
  lc_nXMLFILEID number;

  lc_ret_str varchar(512);
  lc_ret json;

  thesql varchar2(2000) := '';

  --error stuff
  lc_jError json;
  lc_jlMessages json_list;
  json_error EXCEPTION;
  
  lc_jKeywords json;
  
  lc_jlAoData json_list;
  lc_vSearch varchar2(2000);
  lc_jvSearch json_value;
  
  lc_jValue2 json;
  lc_jvValue2 json_value;
  lc_jvValue3 json_value;
  lc_jvValue4 json_value;
  
  lc_jValue3 json;
  
  lc_jlValue2 json_list;
  lc_jlValue4 json_list;


  lc_vValue varchar2(2000);
  lc_vWhere varchar2(2000);
  lc_nStart number := 1;
  lc_nEnd number := 10;
  
  lc_nOrderByCol number;
  lc_vOrderByCol varchar2(2000);
  lc_vOrderBySort varchar2(2000);
  lc_vOrderBy varchar2(2000) := ' DRYDOCKCIID asc ';
  
  lc_nGetValuesFromAodata number;

/*EDITED 20170913 MR COMMENT DEBUG: [SPD_BUG_FIX][03] Dry Dock Upload*/


BEGIN
/*
stub data
{"module":"dryDock", "target": "homeDatatable", data:[]}
*/
lc_jJson := json();

--insert into debug (input) values (json_printer.pretty_print(in_jJson));

/*!! Data is not needed at this moment !!*/
if(in_jJson.exist('data')) then
    if(in_jJson.get('data').is_array) then
        lc_data := json_list(in_jJson.get('data'));        
        lc_rec_val := lc_data.get(1); --return null on outofbounds

        if (lc_rec_val is not null) then
        
            lc_jValue := json(lc_rec_val);
            
            
            if (lc_jValue.exist('XMLFILEID')) then
                lc_jvValue := lc_jValue.get('XMLFILEID');
                lc_nXMLFILEID := getVal(lc_jvValue);
            end if;
            
        end if;
    end if;
end if;


lc_vWhere := ' ';



lc_nGetValuesFromAodata := parseAoData(in_jJson, lc_vSearch, lc_nStart, lc_nEnd, lc_vOrderBy);

if(lc_nGetValuesFromAodata = 0) then
  lc_vSearch :='';
  lc_nStart :=1;
  lc_nEnd :=10;
  lc_vOrderBy :=' DRYDOCKCIID asc ';
end if;

if length(lc_vSearch) is null then
  lc_vWhere := lc_vWhere;
else
  if length(lc_vWhere) is null then
    lc_vWhere := ' Where upper(CODE || 
    DESCRIPTION     ||     
    CREATED_BY)
    like ''%' || upper(lc_vSearch) || '%''';
  else
    lc_vWhere := lc_vWhere || ' AND  
     upper(CODE || 
    DESCRIPTION     ||     
    CREATED_BY)
    like ''%' || upper(lc_vSearch) || '%''';
  end if;
end if;


/*select 
    * 
  from (
  row_number() over (order by ' || lc_vOrderBy || ') rn,
  count(*) over (partition by (select 1 from dual)) countOfRecords
*/



thesql := '
  select
    DRYDOCKCIID,	
    FACID,
    UFII_CODE,	
    ITEMS_INSPECTED,	
    VISUAL_INSPECTION,
    OPERATIONAL_INSPECTION,	
    SATISFACTORY,
    MARGINAL,
    UNSATISFACTORY,	
    CONDITION_INDEX,	
    REMARKS,
    SPM_NUMBER,
    FEMS_NUMBER,
    EST_COST_TO_REPAIR,
    INITALS,
    INSPECTION_DATE,
    CREATED_BY,	
    XMLFILEID	
  from
    spd_t_dry_dock_ci
   where
    XMLFILEID = ' || lc_nXMLFILEID || '
  order by
  ' || lc_vOrderBy; --|| ';
--)
--where rn between ' || lc_nStart || ' and ' || lc_nEnd;

--insert into debug (input) values (thesql);
  
  lc_jJson := json_dyn.executeObject(thesql);

  return lc_jJson;

exception
    when json_error then
        --htp.p('raised json_error in update func');
        lc_jError := json();
        lc_jError.put('function','BB_F_R_DD_CONDITION_DATA');
        lc_jError.put('input',in_jJson.to_char);
        lc_jError.put('messages',lc_jlMessages);
        BB_P_ERROR(lc_jError);
        --htp.p('returning n');
        return json('e');
    when others then
        lc_jError := json();
        lc_jError.put('function','BB_F_R_DD_CONDITION_DATA');
        lc_jError.put('input',in_jJson.to_char);
        lc_jlMessages.append(json_list('[{error:''RDDCD.000X ' || replace(replace(replace(SQLERRM,chr(10),''),chr(13),''),chr(9),'') || '''}]'));
        lc_jError.put('messages',lc_jlMessages);
        BB_P_ERROR(lc_jError);
        return json('e');


END BB_F_R_DD_CONDITION_DATA;
 

/
