--------------------------------------------------------
--  DDL for Function BB_DOWNLOAD_FILE
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE FUNCTION "SPIDERS_DATA"."BB_DOWNLOAD_FILE" (in_jJson json) RETURN json AS

  lc_jJson json;
  lc_data json_list;
  lc_rec_val json_value;
  lc_rec json;
  lc_REPORTFILEID_val json_value; --
  lc_nREPORTFILEID number;

  lc_ret_str varchar(512);
  lc_ret json;
 
  thesql varchar2(2000) := '';

  --error stuff
  lc_jError json;
  lc_jlMessages json_list;
  json_error EXCEPTION;
  
  lc_jKeywords json;

BEGIN
/*
stub data
{"module":"reportLibrary","target":"reportFiles","data":[{"REPORTID":11042}]}
*/

if(in_jJson.exist('data')) then
    if(in_jJson.get('data').is_array) then
        lc_data := json_list(in_jJson.get('data'));
        lc_rec_val := lc_data.get(1); --return null on outofbounds
        if (lc_rec_val is not null) then
            lc_rec := json(lc_rec_val);
            if (lc_rec.exist('REPORTFILEID')) then
                lc_REPORTFILEID_val := lc_rec.get('REPORTFILEID');
                lc_nREPORTFILEID := lc_REPORTFILEID_val.get_number;
            else
                lc_jlMessages.append(json_list('[{error:''Error RDRLF-0001 There has been an error processing the request''}]'));
            end if;
        else
            lc_jlMessages.append(json_list('[{error:''Error RDRLF-0002 There has been an error processing the request''}]'));
        end if;
    else
        lc_jlMessages.append(json_list('[{error:''Error RDRLF-0003 There has been an error processing the request''}]'));
    end if;
else
    lc_jlMessages.append(json_list('[{error:''Error RDRLF-0004 There has been an error processing the request''}]'));
end if;



  file_tools.download_bfile(lc_nREPORTFILEID,'report');
  lc_jJson := json();
  lc_jJson.put('status','success');
  
  return lc_jJson;

exception
    when json_error then
        --htp.p('raised json_error in update func');
        lc_jError := json();
        lc_jError.put('function','BB_DOWNLOAD_FILE');
        lc_jError.put('input',in_jJson.to_char);
        lc_jError.put('messages',lc_jlMessages);
        BB_P_ERROR(lc_jError);
        --htp.p('returning n');
        return json('e');
    when others then
        lc_jError := json();
        lc_jError.put('function','BB_DOWNLOAD_FILE');
        lc_jError.put('input',in_jJson.to_char);
        lc_jlMessages.append(json_list('[{error:''DLF.000X ' || SQLERRM || '''}]'));
        lc_jError.put('messages',lc_jlMessages);
        BB_P_ERROR(lc_jError);
        return json('e');


END BB_DOWNLOAD_FILE;
 

/
