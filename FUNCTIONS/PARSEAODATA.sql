--------------------------------------------------------
--  DDL for Function PARSEAODATA
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE FUNCTION "SPIDERS_DATA"."PARSEAODATA" (in_jJson json, o_vSearch out varchar2, o_nStart out number, o_nEnd out number, o_vOrderBy out varchar2) return number IS 


lc_jlAoData json_list;
lc_jvSearch json_value;
lc_jValue json;
lc_jvValue json_value;
lc_jValue2 json;
lc_jvValue2 json_value;
lc_jValue3 json;
lc_jvValue3 json_value;
lc_vSearch varchar2(2000);

lc_jvStart json_value;
lc_nStart number;

lc_jvEnd json_value;
lc_nEnd number;

lc_jvOrder json_value;
lc_jlValue2 json_list;
lc_vOrderByCol varchar2(2000);
lc_nOrderByCol number;
lc_vOrderBySort varchar2(2000);
lc_vOrderBy varchar2(2000);

begin

/*
stubb data

{"module":"testModule","target":"xmlImportErrorsDataTable",
"aoData":[{"name":"draw","value":1},{"name":"columns","value":[{"data":0,"name":"message","searchable":true,"orderable":true,"search":{"value":"","regex":false}},{"data":1,"name":"target","searchable":true,"orderable":true,"search":{"value":"","regex":false}},{"data":2,"name":"rn","searchable":false,"orderable":false,"search":{"value":"","regex":false}}]},{"name":"order","value":[{"column":0,"dir":"asc"}]},{"name":"start","value":0},{"name":"length","value":10},{"name":"search","value":{"value":"","regex":false}}],
"data":[{"ADOID":330001385}]}

*/

/* get the data for search */
if(in_jJson.exist('aoData')) then
    lc_jlAoData := json_list(in_jJson.get('aoData'));
  
    lc_jvSearch := lc_jlAoData.get(6);
    lc_jValue := json(lc_jvSearch);
    lc_jvValue := lc_jValue.get('value');
  
    lc_jValue2 := json(lc_jvValue);
    lc_jvValue2 := lc_jValue2.get('value');
  
    lc_vSearch := getVal(lc_jvValue2);
    lc_vSearch := lower(lc_vSearch);
    o_vSearch := lc_vSearch; 
/* end get the search data */


/* get the start page and the records per page */
    lc_jvStart := lc_jlAoData.get(4);
    
    lc_jValue := json(lc_jvStart);
    lc_jvValue := lc_jValue.get('value');
    lc_nStart := getVal(lc_jvValue);
    lc_nStart := lc_nStart + 1;
    
    
    lc_jvEnd := lc_jlAoData.get(5);
    
    lc_jValue := json(lc_jvEnd);
    lc_jvValue := lc_jValue.get('value');
    lc_nEnd := getVal(lc_jvValue);
    lc_nEnd := lc_nEnd - 1;
    lc_nEnd := lc_nStart + lc_nEnd; 
    
    o_nStart := lc_nStart;
    o_nEnd := lc_nEnd;
/* end get the start page and records per page */


/* get the order by column and the sort order */
    
    lc_jvOrder := lc_jlAoData.get(3);
    lc_jValue := json(lc_jvOrder);
    lc_jvValue := lc_jValue.get('value');
    
    lc_jlValue2 := json_list(lc_jvValue);
    lc_jvValue2 := lc_jlValue2.get(1);
    
    lc_jValue3 := json(lc_jvValue2);
    lc_jvValue3 := lc_jValue3.get('column');
    lc_nOrderByCol := getVal(lc_jvValue3);
    lc_nOrderByCol := lc_nOrderByCol + 1;
    
    
    
    lc_jvValue3 := lc_jValue3.get('dir');
    lc_vOrderBySort := getVal(lc_jvValue3);
    
    
    
    lc_jvOrder := lc_jlAoData.get(2);
    
    lc_jValue := json(lc_jvOrder);
    lc_jvValue := lc_jValue.get('value');
    
    lc_jlValue2 := json_list(lc_jvValue);
    lc_jvValue2 := lc_jlValue2.get(lc_nOrderByCol);
    lc_jValue3 := json(lc_jvValue2);
    
    lc_jvValue3 := lc_jValue3.get('name');
    lc_vOrderByCol := getVal(lc_jvValue3);
    
    lc_vOrderBy := lc_vOrderByCol || ' ' || lc_vOrderBySort; 
    o_vOrderBy := lc_vOrderBy;
    return 1;
else
    o_vSearch := '';
    o_vOrderBy := '';
    o_nStart := 1;
    o_nEnd := 10;
    return 0;
end if;

/* end get the order by column and the sort order */
    
    
/* todo: better exception handler */
exception
    when others then
      return 0;

end;
 

/
