--------------------------------------------------------
--  DDL for Function GETTARGETTABLENAME
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE FUNCTION "SPIDERS_DATA"."GETTARGETTABLENAME" (in_vRest varchar2, in_vModule varchar2, in_vTarget varchar2) return varchar2 as 
  /* template variables */
  json_error EXCEPTION;
  spiders_error EXCEPTION;
  lc_vErrorDescription varchar2(255) := 'default';
  lc_vPrefix varchar2(255) := 'SPD_T_';
  lc_vFunctionName varchar2(255) := 'getTargetTableName';
  lc_vReturn varchar2(32000) := '{"error":"default"}'; --defaul the return to e for error
  lc_nContinue number :=0; --default to 0 to dissalow continuing
  
  /* function specific variables */
  lc_vName varchar2(2000);
  lc_vTemp varchar2(2000);
  lc_vComment varchar2(2000);
  
  lc_nUserPermission number(5,2);
  /*EDITED 20170915 MR REMOVE DEBUG: [SPD3D][45] Sync*/
begin
  
  select USER_PERMISSION into lc_nUserPermission from SPD_V_QC_APEX_USER_PERMISSION;
  /* get name of object */
  select table_name into lc_vReturn from spd_t_rest_to_table_maps where USER_PERMISSION <= lc_nUserPermission and upper(rest_endpoint) = upper(trim(in_vRest)) and upper(module) = upper(trim(in_vModule)) and upper(target) = upper(trim(in_vTarget));
  return lc_vReturn;

exception     
  when spiders_error then
      execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' SPIDERS error ' || lc_vErrorDescription || ''')';
      return '{"error":"' || lc_vErrorDescription || '"}';
  when json_error then
      execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' JSON error'')';
      return '{"error":"json"}';
  when others then
      execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' dml error: ' || sqlerrm || ''')';
      return '{"error":"' || sqlerrm || '"}';
end;
 

/
