--------------------------------------------------------
--  DDL for Function BB_F_R_REPORT_LIBRARY_REC
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE FUNCTION "SPIDERS_DATA"."BB_F_R_REPORT_LIBRARY_REC" (in_jJson json) RETURN json AS

  lc_jJson json;  
  lc_data json_list;
  lc_rec_val json_value;
  lc_jValue json;
  lc_jvValue json_value;
  lc_nREPORTID number;
  
  thesql varchar2(2000) := '';

  --error stuff
  lc_jError json;
  lc_jlMessages json_list;
  json_error EXCEPTION;

BEGIN
/*
stub data
{"module":"reportLibrary","target":"reportFiles","data":[{"REPORTID":11042}]}
*/

if(in_jJson.exist('data')) then
    if(in_jJson.get('data').is_array) then
        lc_data := json_list(in_jJson.get('data'));        
        lc_rec_val := lc_data.get(1); --return null on outofbounds

        if (lc_rec_val is not null) then
        
            lc_jValue := json(lc_rec_val);
            
            if (lc_jValue.exist('REPORTID')) then
                lc_jvValue := lc_jValue.get('REPORTID');
                lc_nREPORTID := getVal(lc_jvValue);
            end if;
        end if;
    end if;
end if;

thesql := '

SELECT 
  reportid,
  reporttitle,
  reportauthor,
  to_char(ESTSTARTDATE,''YYYY'') ESTSTARTDATE,
  contractor,
  contractnumber,
  reportnumber,
  actionsid,
  (select spd_activity_name from spd_t_activities actv, spd_t_action_planner ap where actv.activitiesid = ap.planned_activitiesid and ap.apid = rep.ACTIONSID) ACTIVITY_NAME,
  (select spd_activity_name from spd_t_activities actv, spd_t_action_planner ap where actv.activitiesid = ap.planned_activitiesid and ap.apid = rep.ACTIONSID) ACTIVITY_UIC,
  ceactionsid,
  city,
  state,
  country,
  location_custom,
  noteskey,
  folderid,
  productline_listid,
  (select displayvalue from spd_t_lists where listid = rep.productline_listid) productline_display,
  govprojengineer
from 
  spd_t_reports rep
where 
  reportid = ' || lc_nReportid;

  lc_jJson := json_dyn.executeObject(thesql);

  return lc_jJson;

exception
    when json_error then
        --htp.p('raised json_error in update func');
        lc_jlMessages := json_list();
        lc_jError := json();
        lc_jError.put('function','BB_F_R_REPORT_LIBRARY_REC');
        lc_jError.put('input',in_jJson.to_char);
        lc_jError.put('messages',lc_jlMessages);
        BB_P_ERROR(lc_jError);
        --htp.p('returning n');
        return json('e');
    when others then
        lc_jError := json();
        lc_jlMessages := json_list();
        lc_jError.put('function','BB_F_R_REPORT_LIBRARY_REC');
        lc_jError.put('input',in_jJson.to_char);
        lc_jlMessages.append(json_list('[{error:''RRLR.000X ' || SQLERRM || '''}]'));
        lc_jError.put('messages',lc_jlMessages);
        BB_P_ERROR(lc_jError);
        return json('e');


END BB_F_R_REPORT_LIBRARY_REC;
 

/
