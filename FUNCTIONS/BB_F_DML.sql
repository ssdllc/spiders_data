--------------------------------------------------------
--  DDL for Function BB_F_DML
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE FUNCTION "SPIDERS_DATA"."BB_F_DML" (in_jJson json, o_jMessages out json, in_nAutoCommit number default 1  ) RETURN VARCHAR2 AS

  lc_jJson json;
  lc_jlResults json_list;
  lc_rec_val json_value;
  lc_rec json;
  lc_cnt number;
  
  lc_vDML long;

  lc_jResults json;
  lc_jMessages json;
  lc_jlMessages json_list;
  lc_vReturn varchar2(1);
  
  --error stuff
  lc_jError json;
  json_error EXCEPTION;
  
  lc_vFunctionName varchar2(50):= 'BB_F_DML';
  
/*debugging*/
lc_nDebugCount number := 0;
/*end debugging*/  

/*EDITED 20170623 MR DEBUG: [SPD_BUG_FIX][03] Dry Dock Upload*/
/*EDITED 20170913 MR COMMENT DEBUG: [SPD_BUG_FIX][03] Dry Dock Upload*/


BEGIN

    

    lc_jMessages := json('{}');
    lc_jlMessages := json_list('[]');
    if(in_jJson.exist('results')) then
        if(in_jJson.get('results').is_array) then
            lc_jlResults := json_list(in_jJson.get('results'));
            for i in 1..lc_jlResults.count loop
                lc_rec_val := lc_jlResults.get(i);
                if (lc_rec_val is not null) then
                    --this is a valid object create the dml
                    --insert into debug (message) values ('dml execute ' || i || ':' ||  lc_rec_val.get_string);
/*debugging*/
--insert into debug (functionname, input, message) values ('BB_F_DML','[SPD_BUG_FIX][03] Dry Dock Upload', 'before-' || lc_rec_val.get_string);
/*end debugging*/ 
                    --lc_vDML := lc_rec_val.get_string;
                    execute immediate lc_rec_val.get_string;
                    
/*debugging*/
--insert into debug (functionname, input, message) values ('BB_F_DML','[SPD_BUG_FIX][03] Dry Dock Upload', lc_rec_val.get_string);
/*end debugging*/ 

                    if in_nAutoCommit = 1 then
                  
/*debugging*/
--insert into debug (functionname, input, message) values ('BB_F_DML','[SPD_BUG_FIX][03] Dry Dock Upload', 'autocommit = y');
/*end debugging*/ 

                      commit;
                    end if;
                else
                    lc_jlMessages.append(json_list('[{row:' || i || '},{error:''Error DML-0001 There has been an error processing the request''}]'));
                end if;
            end loop;
        else
            lc_jlMessages.append(json_list('[{error:''Error DML-0003 There has been an error processing the request''}]'));
        end if;
    else
        lc_jlMessages.append(json_list('[{error:''Error DML-0004 There has been an error processing the request''}]'));
    end if;

                   

    
    lc_jMessages.put('messages',lc_jlMessages);
    

    --if there are any messages this is not valid return false
    if (lc_jlMessages.count > 0) then
        lc_vReturn := 'n';
        lc_jMessages.put('status','error');
        raise json_error;
    else
        lc_vReturn := 'y';
        lc_jMessages.put('status','success');
    end if;
    
    --insert into debug (message) values ('dml lc_vReturn: ' || lc_vReturn);

    o_jMessages := lc_jMessages;

    return lc_vReturn;

  exception     
    when json_error then
        execute immediate 'insert into debug (message) values (''JSON error'')';
        return 'e';

    when others then
        execute immediate 'insert into debug (FUNCTIONNAME, INPUT, MESSAGE) values (''' || '' || lc_vFunctionName || ''', '''||  'An error occured in BB_F_DML '||lc_vDML|| ''',''' || 'sqlerrm: ' || sqlerrm || ''')';
        --execute immediate '--insert into debug (message) values (''dml error: ' || sqlerrm || ''')';
        return 'e';
      /*
        lc_jError := json();
        lc_jError.put('function','bb_f_dml');
        lc_jError.put('input',in_jJson.to_char);
        lc_jlMessages.append(json_list('[{error:''DML.000X ' || replace(replace(replace(SQLERRM,chr(10),''),chr(13),''),chr(9),'') || '''}]'));
        lc_jError.put('messages',lc_jlMessages);
        BB_P_ERROR(lc_jError);
        return 'e';*/
END;

/
