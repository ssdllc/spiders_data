--------------------------------------------------------
--  DDL for Function BB_F_GET_UNIT_GUID
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE FUNCTION "SPIDERS_DATA"."BB_F_GET_UNIT_GUID" (in_vVal varchar2) return varchar2 as 

lc_vReturn varchar2(255) := 'NONE';

begin

if in_vVal = 'EA' then lc_vReturn := '533e3620-c0e4-11e4-9f80-eb3fd8006b01'; end if;
if in_vVal = 'FT' then lc_vReturn := '533eab51-c0e4-11e4-9f80-eb3fd8006b01'; end if;
if in_vVal = 'HR' then lc_vReturn := '533ed261-c0e4-11e4-9f80-eb3fd8006b01'; end if;
if in_vVal = 'IN' then lc_vReturn := '533eab52-c0e4-11e4-9f80-eb3fd8006b01'; end if;
if in_vVal = 'LF' then lc_vReturn := '533e8440-c0e4-11e4-9f80-eb3fd8006b01'; end if;
if in_vVal = 'MAN' then lc_vReturn := '533ed260-c0e4-11e4-9f80-eb3fd8006b01'; end if;
if in_vVal = 'MN' then lc_vReturn := '533ed263-c0e4-11e4-9f80-eb3fd8006b01'; end if;
if in_vVal = 'SF' then lc_vReturn := '533eab50-c0e4-11e4-9f80-eb3fd8006b01'; end if;
if in_vVal = 'YR' then lc_vReturn := '533ed262-c0e4-11e4-9f80-eb3fd8006b01'; end if;

return lc_vReturn;

exception when others then
      execute immediate 'insert into debug (functionname, input, message) values (''BB_F_GET_UNIT_GUID'',''' || in_vVal || ''',''error: ' || sqlerrm || ''')';
      return 'e';

end;



/*

type = 'unit'
[
{'id':'533e3620-c0e4-11e4-9f80-eb3fd8006b01','key':['Each','EA','533e3620-c0e4-11e4-9f80-eb3fd8006b01'],'value':null},
{'id':'533eab51-c0e4-11e4-9f80-eb3fd8006b01','key':['Feet','FT','533eab51-c0e4-11e4-9f80-eb3fd8006b01'],'value':null},
{'id':'533ed261-c0e4-11e4-9f80-eb3fd8006b01','key':['Hour','HR','533ed261-c0e4-11e4-9f80-eb3fd8006b01'],'value':null},
{'id':'533eab52-c0e4-11e4-9f80-eb3fd8006b01','key':['Inches','IN','533eab52-c0e4-11e4-9f80-eb3fd8006b01'],'value':null},
{'id':'533e8440-c0e4-11e4-9f80-eb3fd8006b01','key':['Linear Feet','LF','533e8440-c0e4-11e4-9f80-eb3fd8006b01'],'value':null},
{'id':'533ed260-c0e4-11e4-9f80-eb3fd8006b01','key':['Manual Entry','MAN','533ed260-c0e4-11e4-9f80-eb3fd8006b01'],'value':null},
{'id':'533ed263-c0e4-11e4-9f80-eb3fd8006b01','key':['Month','MN','533ed263-c0e4-11e4-9f80-eb3fd8006b01'],'value':null},
{'id':'533eab50-c0e4-11e4-9f80-eb3fd8006b01','key':['Square Feet','SF','533eab50-c0e4-11e4-9f80-eb3fd8006b01'],'value':null},
{'id':'533ed262-c0e4-11e4-9f80-eb3fd8006b01','key':['Year','YR','533ed262-c0e4-11e4-9f80-eb3fd8006b01'],'value':null}
]

*/
 

/
