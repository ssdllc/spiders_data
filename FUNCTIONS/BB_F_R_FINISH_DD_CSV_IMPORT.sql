--------------------------------------------------------
--  DDL for Function BB_F_R_FINISH_DD_CSV_IMPORT
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE FUNCTION "SPIDERS_DATA"."BB_F_R_FINISH_DD_CSV_IMPORT" (in_jJson json, o_jMessages out json) return varchar2 as 


    lc_jJson json;
    lc_data json_list;
    lc_rec_val json_value;
    lc_rec json;
    --o_jMessages json;
   
    lc_jValue json;
    lc_jvValue json_value;
    lc_nFACID number;
    
    lc_nSequence number;
    lc_nContinue number :=0;
    lc_nCountOfRecords number :=0;
    lc_nXMLFileId number :=0;
    lc_nFileskey number :=0;
    
    lc_jlDML json_list;
    lc_jResults json;
    lc_jMessages json;
    lc_vDMLResult varchar2(255);
    lc_ret_str varchar(512);
    lc_ret json;

    thesql varchar2(2000) := '';

    lc_vWhere varchar2(2000);

    --error stuff
    lc_jError json;
    lc_jlMessages json_list;
    json_error EXCEPTION;



    lc_cClob clob;
    lc_bBlob blob;
    lc_nScenemodelid number;
    lc_vCreatedBy varchar2(255) :='';
    lc_nCount number :=1;
    lc_nSceneid number;
    
    lc_vReturn varchar2(4000) := 'sdfsdf';
    
/*EDITED 20170913 MR COMMENT DEBUG: [SPD_BUG_FIX][03] Dry Dock Upload*/

BEGIN


lc_jJson := json();

--insert into debug (input) values ('finish dd csv import 1');

if(in_jJson.exist('data')) then
    if(in_jJson.get('data').is_array) then
        lc_data := json_list(in_jJson.get('data'));        
        lc_rec_val := lc_data.get(1); --return null on outofbounds

        if (lc_rec_val is not null) then
        
            lc_jValue := json(lc_rec_val);
            
            if (lc_jValue.exist('FACID')) then
                lc_jvValue := lc_jValue.get('FACID');
                lc_nFACID := getVal(lc_jvValue);
            end if;
        end if;
    end if;
end if;

--insert into debug (input) values ('finish dd csv import 2');

lc_jlDML := json_list();
lc_jResults := json();
lc_jMessages := json();

lc_vCreatedBy := v('APP_USER');

thesql := '
insert into 
  spd_t_dry_dock_ci (
    FACID,
    UFII_CODE, 
    ITEMS_INSPECTED, 
    SATISFACTORY, 
    MARGINAL, 
    UNSATISFACTORY,
    CONDITION_INDEX, 
    EST_COST_TO_REPAIR, 
    INSPECTION_DATE,  
    REMARKS, 
    VISUAL_INSPECTION, 
    OPERATIONAL_INSPECTION, 
    FEMS_NUMBER,
    INITALS, 
    CREATED_BY,
    XMLFILEID
  ) 
select 
    ' || lc_nFACID || ',
    UFII_CODE, 
    ITEMS_INSPECTED, 
    SATISFACTORY, 
    MARGINAL, 
    UNSATISFACTORY,
    CONDITION_INDEX, 
    EST_COST_TO_REPAIR, 
    INSPECTION_DATE,  
    REMARKS, 
    VISUAL_INSPECTION, 
    OPERATIONAL_INSPECTION, 
    FEMS_NUMBER,
    INITALS, 
    CREATED_BY,
    XMLFILEID
from 
  spd_t_dry_dock_ci_tmp
where
  created_by = ''' || lc_vCreatedBy || '''';
  

--insert into debug (input) values (thesql);

  
lc_jlDML.append(thesql);


for rec in (select xmlfileid from spd_t_dry_dock_ci_tmp where created_by = lc_vCreatedBy) loop
  lc_nXMLFileId := rec.xmlfileid;
end loop;

for rec in (select fileskey from spd_t_facilities where facid = lc_nFACID) loop
  lc_nFileskey := rec.fileskey;
end loop;

thesql := 'insert into spd_t_inter_files (filesid,fileskey) values (' || lc_nXMLFileId || ',' || lc_nFileskey || ')';
--insert into debug (input) values (thesql);
lc_jlDML.append(thesql);

thesql := 'delete from spd_t_dry_dock_ci_tmp where created_by = ''' || lc_vCreatedBy || '''';
--insert into debug (input) values (thesql);
lc_jlDML.append(thesql);


      lc_jResults.put('results',lc_jlDML);
      lc_vDMLResult := BB_F_DML(lc_jResults, lc_jMessages);
      lc_jMessages.put('id',lc_nSequence);
      o_jMessages := lc_jMessages;
      

--insert into debug (input) values ('finish dd csv import complete');

lc_vReturn := lc_vDMLResult;
return lc_vReturn;

exception
when json_error then
    --htp.p('raised json_error in update func');
    lc_jError := json();
    lc_jError.put('function','BB_F_R_FINISH_DD_CSV_IMPORT');
    lc_jError.put('input','csv file ' || lc_nSceneid);
    lc_jError.put('messages',lc_jlMessages);
    BB_P_ERROR(lc_jError);
    return 'e';
    --htp.p('returning n');
when others then
    --htp.p('this ' || SQLERRM);
    lc_jError := json();
    lc_jlMessages := json_list();
    lc_jError.put('function','BB_F_R_FINISH_DD_CSV_IMPORT');
    lc_jError.put('input','');
    lc_jlMessages.append(json_list('[{error:''FDDCSVI.000X ' || replace(replace(replace(SQLERRM,chr(10),''),chr(13),''),chr(9),'') || '''}]'));
    lc_jError.put('messages',lc_jlMessages);
    BB_P_ERROR(lc_jError);
    return 'e';
end BB_F_R_FINISH_DD_CSV_IMPORT;
 

/
