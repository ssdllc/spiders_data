--------------------------------------------------------
--  DDL for Function F_SMOKE_GET_FILE_PATH
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE FUNCTION "SPIDERS_DATA"."F_SMOKE_GET_FILE_PATH" 
   (p_path IN VARCHAR2)
   RETURN varchar2

IS
   v_dir VARCHAR2(32000);
   lc_vPath long;
BEGIN

   lc_vPath := replace(p_path,'/','\'); 
   -- Parse string for UNIX system
   IF INSTR(lc_vPath,'/') > 0 THEN
      v_dir := SUBSTR(lc_vPath,1,(INSTR(lc_vPath,'/',-1,1)-1));

   -- Parse string for Windows system
   ELSIF INSTR(lc_vPath,'\') > 0 THEN
      v_dir := SUBSTR(lc_vPath,1,(INSTR(lc_vPath,'\',-1,1)-1));

   -- If no slashes were found, return the original string
   ELSE
      v_dir := lc_vPath;
   END IF;

  
   v_dir := replace(v_dir,'\','/');
   v_dir := replace(v_dir,'C:/','');
   v_dir := '/u02/SPIDERS_FILES/LOADED/' || upper(v_dir);

   RETURN v_dir;

END;
 

/
