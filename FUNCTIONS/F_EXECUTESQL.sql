--------------------------------------------------------
--  DDL for Function F_EXECUTESQL
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE FUNCTION "SPIDERS_DATA"."F_EXECUTESQL" (in_jlDML json_list, in_nAutoCommit number default 1) return varchar2 as
      lc_jResults json;
      lc_jMessages json;
      lc_vDMLResult varchar2(1);
      
  begin
      
      lc_jResults := json();
      lc_jResults.put('results',in_jlDML);
      
      lc_jMessages := json();
      lc_vDMLResult := BB_F_DML(lc_jResults, lc_jMessages, in_nAutoCommit);
      
      if lc_vDMLResult = 'Y' then
        return 1;
      else
        return 0;
      end if;
  end;
 

/
