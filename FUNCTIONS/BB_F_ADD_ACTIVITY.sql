create or replace FUNCTION                "BB_F_ADD_ACTIVITY" (in_jJson json, o_jResults out json, o_jMessages out json ) RETURN VARCHAR2 AS


--JSON stuff
lc_jJson json;
lc_data json_list;
lc_rec_val json_value;
lc_rec json;
lc_jValue json;
lc_jvValue json_value;


--procedure specific stuff
lc_nSequence number;
lc_nContinue number :=1;
lc_nCountOfRecords number :=0;
lc_nADOID number;
lc_vNFAID varchar2(2000);
lc_nPRODUCTLINE_LISTID number;
lc_vFACILITY_NAME varchar2(2000);
lc_vFACILITY_NO varchar2(2000);
lc_nDOFID number;

--DML stuff
lc_jlDML json_list;
lc_jResults json;
lc_jMessages json;
lc_vDMLResult varchar2(255);
lc_vResult varchar2(200);
lc_ret_str varchar(512);
lc_ret json;
thesql varchar2(2000) := '';
lc_vWhere varchar2(2000);
lc_vReturn varchar2(200);

--error stuff
lc_vErrorText varchar2(2000);
lc_jError json;
lc_jlMessages json_list;
json_error EXCEPTION;

procedure p (in_vInput varchar2, in_vMessage varchar2 default '') as

begin

insert into debug (functionname, input, message) values ('BB_F_ADD_ACTIVITY', in_vInput, in_vMessage);

end;

BEGIN
/*
stub data
*/

lc_jResults := json();
lc_jlDML := json_list();
lc_jMessages := json();
lc_jlMessages := json_list();
o_jResults := json();
o_jMessages := json();


if(in_jJson.exist('NFAID')) then
  lc_jvValue := in_jJson.get('NFAID');
  lc_vNFAID := getVal(lc_jvValue);
else
  lc_vErrorText := 'NFAID doesnt exist';
  lc_nContinue :=0;
  lc_jlMessages.append('error: ''Error XMLIFFM-0001 ' || lc_vErrorText);
end if;

p('lc_vNFAID',lc_vNFAID);

if lc_nContinue = 1 then
  
  --check if the NFA exists in the infads tables
  select count(*) into lc_nCountOfRecords from spd_mv_inf_facility inf where inf.facility_id = lc_vNFAID;
  
  if lc_nCountOfRecords = 0 then
    --if NFA does not exist in the infads tables we need to make a fake UIC, all facilities that dont fit into infads will go into this Fake UIC

      p('NFA doesnt exist in spd_mv_inf_facility',lc_vNFAID);
    
      select count(*) into lc_nCountOfRecords from spd_t_activities where spd_uic = 'N999999_UNK';
      
      p('lc_nCountOfRecords t',lc_nCountOfRecords);
      
      if lc_nCountOfRecords = 0 then
        --This unknown UIC does not exist, make it, all subsequent unknown uics will use this activities id
        select spd_s_activities.nextval into lc_nSequence from dual;
        --Insert record into table
        thesql := '
        insert into spd_t_activities 
        (
          activitiesid,
          spd_activity_name, 
          spd_uic, 
          state, 
          country
        ) 
        values 
        (
          ' || lc_nSequence || ',
          ''UNKNOWN_ACTIVITY'', 
          ''N999999_UNK'', 
          ''UNKOWN'', 
          ''UNKNOWN''
        )';
      else
        --tc did this 20180702
        --lc_nContinue := 0;
        select activitiesid into lc_nSequence from spd_t_activities where spd_uic = 'N999999_UNK';
        o_jResults.put('ACTIVITIESID',lc_nSequence);
        return 'y';
      end if;
      p('lc_nContinue t',lc_nContinue);
  else
    
    p('This NFA does exist in spd_mv_inf_facility',lc_vNFAID);
    --this NFA does exist in infads, get the UIC and see if we need to make that  
    select count(*) into lc_nCountOfRecords from spd_t_activities where spd_uic in (select inf.activity_uic from spd_mv_inf_facility inf where inf.facility_id = lc_vNFAID);
    p('Count of Activity records for this NFA',lc_nCountOfRecords);
    if lc_nCountOfRecords = 0 then
      --This uic needs to be made in spd_t_activities
      --this NFA exists in the infads tables, use the values from INFADS for the facility name, no and then get the UIC
      select spd_s_activities.nextval into lc_nSequence from dual;
      --Insert record into table
      thesql := '
      insert into spd_t_activities 
      (
        activitiesid,
        spd_activity_name, 
        spd_uic, 
        state, 
        country
      ) 
      select 
        ' || lc_nSequence || ',
        unit_title, 
        uic, 
        state, 
        country 
      from 
        spd_mv_inf_cur_activities 
      where 
        uic in (select activity_uic from spd_mv_inf_facility where facility_id = ''' || lc_vNFAID || ''')  and rownum =1 ';
    else
      lc_nContinue := 0;
    end if;
  
  end if;
  
  p('lc_nContinue line 149 t',lc_nContinue);
  if lc_nContinue = 1 then
    lc_jlDML.append(thesql);
    lc_jResults.put('results',lc_jlDML);
    lc_vDMLResult := BB_F_DML(lc_jResults, lc_jMessages);
    lc_jMessages.put('id',lc_nSequence);
    o_jMessages := lc_jMessages;
  
    o_jResults.put('ACTIVITIESID',lc_nSequence);
  else
    lc_vErrorText := 'Activity already in the spd_t_activities table';
    lc_nContinue :=0;
    lc_jlMessages.append('error: ''Error XMLIFFM-0001 ' || lc_vErrorText);
  end if;
end if;

lc_jMessages.put('messages',lc_jlMessages);


--if there are any messages this is not valid return false
if (lc_jlMessages.count > 0) then
    lc_vReturn := 'n';
    lc_jMessages.put('status','error');
    --raise json_error;
    p('lc_jlMessages',lc_jlMessages.count);
    p('lc_vReturn',lc_vReturn);
else
    lc_vReturn := 'y';
    lc_jMessages.put('status','success');
    p('lc_vReturn',lc_vReturn);
end if;

    o_jMessages := lc_jMessages;
p('lc_vReturn2',lc_vReturn);

return lc_vReturn;


exception
when json_error then
    --htp.p('raised json_error in update func');
    lc_jError := json();
    lc_jError.put('function','BB_F_ADD_ACTIVITY');
    lc_jError.put('input',json_printer.pretty_print(in_jJson));
    lc_jError.put('messages',lc_jlMessages);
    BB_P_ERROR(lc_jError);
    return 'e';
    --htp.p('returning n');
  when others then
      execute immediate 'insert into debug (functionname, input, message) values (''BB_P_XML_FACILITIES_MATCH'',''' || json_printer.pretty_print(in_jJson) || ''',''error: ' || sqlerrm || ''')';
      return 'e';

end;