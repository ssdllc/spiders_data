--------------------------------------------------------
--  DDL for Function BB_F_XML_INSERT_FACILITIES
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE FUNCTION "SPIDERS_DATA"."BB_F_XML_INSERT_FACILITIES" (in_jJson json, o_jResults out json, o_jMessages out json ) RETURN VARCHAR2 AS

--JSON stuff
lc_jJson json;
lc_data json_list;
lc_rec_val json_value;
lc_rec json;
lc_jValue json;
lc_jvValue json_value;


--procedure specific stuff
lc_nREPORTFILEID number;
lc_vTEXT varchar2(255);
lc_nSequence number;
lc_nContinue number :=1;
lc_nCountOfRecords number :=0;
lc_nADOID number;
lc_vNFAID varchar2(2000);
lc_nPRODUCTLINE_LISTID number;
lc_vFACILITY_NAME varchar2(2000);
lc_vFACILITY_NO varchar2(2000);
lc_nDOFID number;

--DML stuff
lc_jlDML json_list;
lc_jResults json;
lc_jMessages json;
lc_vDMLResult varchar2(255);
lc_ret_str varchar(512);
lc_ret json;
thesql varchar2(2000) := '';
lc_vWhere varchar2(2000);
lc_vReturn varchar2(2000);

--error stuff
lc_vErrorText varchar2(2000);
lc_jError json;
lc_jlMessages json_list;
json_error EXCEPTION;

/*EDITED 20170913 MR COMMENT DEBUG: [SPD_BUG_FIX][03] Dry Dock Upload*/

procedure p (in_vInput varchar2, in_vMessage varchar2 default '') as

begin

null;
--insert into debug (functionname, input, message) values ('BB_F_XML_INSERT_FACILITIES', in_vInput, in_vMessage);

end;


BEGIN
/*
stub data
*/

lc_jResults := json();
lc_jlDML := json_list();
lc_jMessages := json();
lc_jlMessages := json_list();
o_jResults := json();
o_jMessages := json();


p('BB_F_XML_INSERT_FACILITIES in_jJson',json_printer.pretty_print(in_jJson));

if(in_jJson.exist('ADOID')) then
  lc_jvValue := in_jJson.get('ADOID');
  lc_nADOID := getVal(lc_jvValue);
  p('lc_nADOID',lc_nADOID);
  
  if(in_jJson.exist('NFAID')) then
    lc_jvValue := in_jJson.get('NFAID');
    lc_vNFAID := getVal(lc_jvValue);
    p('lc_vNFAID',lc_vNFAID);
    
    if(in_jJson.exist('PRODUCTLINE_LISTID')) then
      lc_jvValue := in_jJson.get('PRODUCTLINE_LISTID');
      lc_nPRODUCTLINE_LISTID := getVal(lc_jvValue);
      p('lc_nPRODUCTLINE_LISTID',lc_nPRODUCTLINE_LISTID);
      
      if(in_jJson.exist('FACILITY_NAME')) then
        lc_jvValue := in_jJson.get('FACILITY_NAME');
        lc_vFACILITY_NAME := getVal(lc_jvValue);
        p('lc_vFACILITY_NAME',lc_vFACILITY_NAME);
        
        if(in_jJson.exist('FACILITY_NO')) then
          lc_jvValue := in_jJson.get('FACILITY_NO');
          lc_vFACILITY_NO := getVal(lc_jvValue);
          p('lc_vFACILITY_NO',lc_vFACILITY_NO);
          
          if(in_jJson.exist('DOFID')) then
            lc_jvValue := in_jJson.get('DOFID');
            lc_nDOFID := getVal(lc_jvValue);
            p('lc_nDOFID',lc_nDOFID);
            
          else
            lc_vErrorText := 'DOFID doesnt exist';
            lc_nContinue :=0;
            lc_jlMessages.append('error: ''Error XMLIFFM-0001 ' || lc_vErrorText);
          end if;
          
        else
          lc_vErrorText := 'FACILITY_NO doesnt exist';
          lc_nContinue :=0;
          lc_jlMessages.append('error: ''Error XMLIFFM-0001 ' || lc_vErrorText);
        end if;
        
      else
        lc_vErrorText := 'FACILITY_NAME doesnt exist';
        lc_nContinue :=0;
        lc_jlMessages.append('error: ''Error XMLIFFM-0001 ' || lc_vErrorText);
      end if;
      
    else
      lc_vErrorText := 'PRODUCTLINE_LISTID doesnt exist';
      lc_nContinue :=0;
      lc_jlMessages.append('error: ''Error XMLIFFM-0001 ' || lc_vErrorText);
    end if;
  else
    lc_vErrorText := 'NFAID doesnt exist';
    lc_nContinue :=0;
    lc_jlMessages.append('error: ''Error XMLIFFM-0001 ' || lc_vErrorText);
  end if;

else
  lc_vErrorText := 'ADOID doesnt exist';
  lc_nContinue :=0;
  lc_jlMessages.append('error: ''Error XMLIFFM-0001 ' || lc_vErrorText);
end if;


p('BB_F_XML_INSERT_FACILITIES lc_nContinue',lc_nContinue);
if lc_nContinue = 1 then

  select count(*) into lc_nCountOfRecords from spd_t_xml_facilities_match where dofid = lc_nDOFID;
  if lc_nCountOfRecords = 0 then
  --new record to add
    select spd_s_xml_facilities_match.nextval into lc_nSequence from dual;
    --Insert record into table
    thesql := '
      insert into spd_t_xml_facilities_match
      (
        XMLFACILITYMATCHID,
        XML_FACILITY_NAME, 
        XML_NFAID, 
        XML_FACILITY_NUMBER , 
        XML_SPIDERS_DOFID, 
        DOFID,
        adoid
      )
      values
        (
          ' || lc_nSequence || ',
          ''' || lc_vFACILITY_NAME || ''',
          ''' || lc_vNFAID || ''',
          ''' || lc_vFACILITY_NO || ''',
          ' || lc_nDOFID || ',
          ' || lc_nDOFID || ',
          ' || lc_nADOID || '
        )';
        
  
    lc_jlDML.append(thesql);
    lc_jResults.put('results',lc_jlDML);
    lc_vDMLResult := BB_F_DML(lc_jResults, lc_jMessages);
    lc_jMessages.put('id',lc_nSequence);
  end if;
  
  
  if lc_nCountOfRecords = 1 then
  --use existing/do nothing for now
    select xmlfacilitymatchid into lc_nSequence from spd_t_xml_facilities_match where dofid = lc_nDOFID;
  end if;
  
  
  if lc_nCountOfRecords > 1 then
  --this has too many records for this dofid, this is an issue that needs to be looked at in time.
    lc_vErrorText := 'too many xmlfacilitymatch ids for this dofid';
    lc_nContinue :=0;
    lc_jlMessages.append('error: ''Error XMLIFFM-0001 ' || lc_vErrorText);
  end if;
end if;


--if there are any messages this is not valid return false
if (lc_jlMessages.count > 0) then
    lc_vReturn := 'n';
    lc_jMessages.put('status','error');
else
    lc_vReturn := 'y';
    lc_jMessages.put('status','success');
    o_jResults.put('XMLFACILITYMATCHID',lc_nSequence);
end if;

o_jMessages := lc_jMessages;

return lc_vReturn;



exception
when json_error then
      execute immediate 'insert into debug (functionname, input, message) values (''BB_P_XML_FACILITIES_MATCH'',''' || json_printer.pretty_print(in_jJson) || ''',''error: Custom error'')';
    return 'e';
    --htp.p('returning n');
  when others then
      execute immediate 'insert into debug (functionname, input, message) values (''BB_P_XML_FACILITIES_MATCH'',''' || json_printer.pretty_print(in_jJson) || ''',''error: ' || sqlerrm || ''')';
      return 'e';

end;
 

/
