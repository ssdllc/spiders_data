--------------------------------------------------------
--  DDL for Function BB_F_READ_DO_XML_IMPORT_LOG
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE FUNCTION "SPIDERS_DATA"."BB_F_READ_DO_XML_IMPORT_LOG" (in_jJson json) RETURN json AS

  lc_jJson json;
  lc_data json_list;
  lc_rec_val json_value;
  lc_rec json;
  lc_adoid_val json_value; --
  lc_nAdoid number;

  lc_ret_str varchar(512);
  lc_ret json;

  thesql varchar2(2000) := '';

  --error stuff
  lc_jError json;
  lc_jlMessages json_list;
  json_error EXCEPTION;

BEGIN
/*
stub data
{"module":"xmlImportManager","target":"spidersDeliveryOrderFacs","data":[{"ADOID":330001385}]}
*/

if(in_jJson.exist('data')) then
    if(in_jJson.get('data').is_array) then
        lc_data := json_list(in_jJson.get('data'));
        lc_rec_val := lc_data.get(1); --return null on outofbounds
        if (lc_rec_val is not null) then
            lc_rec := json(lc_rec_val);
            if (lc_rec.exist('ADOID')) then
                lc_adoid_val := lc_rec.get('ADOID');
                lc_nAdoid := lc_adoid_val.get_number;
            else
                lc_jlMessages.append(json_list('[{error:''Error RDOXIL-0001 There has been an error processing the request''}]'));
            end if;
        else
            lc_jlMessages.append(json_list('[{error:''Error RDOXIL-0002 There has been an error processing the request''}]'));
        end if;
    else
        lc_jlMessages.append(json_list('[{error:''Error RDOXIL-0003 There has been an error processing the request''}]'));
    end if;
else
    lc_jlMessages.append(json_list('[{error:''Error RDOXIL-0004 There has been an error processing the request''}]'));
end if;

thesql := 'select
    importlogid,
    apid,
    adoid,
    dofid,
    filename,
    import_section,
    notes,
    TO_CHAR(timestamp, ''DD-MON-YYYY HH:MI:SS AM'') timestamp,
    run_date,
    count_of_records successful_records,
    total_records,
    grouping
from
    spd_t_import_log
where
    adoid = ' || lc_nADOID || '
    and total_records > 0
order by
    importlogid desc
     
';

  lc_jJson := json_dyn.executeObject(thesql);

  return lc_jJson;

exception
    when json_error then
        --htp.p('raised json_error in update func');
        
        lc_jError := json();
        lc_jError.put('function','BB_F_READ_DO_XML_IMPORT_LOG');
        lc_jError.put('input','');
        lc_jError.put('messages',lc_jlMessages);
        BB_P_ERROR(lc_jError);
        
        --htp.p('returning n');
        return json('e');
    when others then
        
        lc_jError := json();
        lc_jError.put('function','BB_F_READ_DO_XML_IMPORT_LOG');
        lc_jError.put('input','');
        lc_jlMessages.append(json_list('[{error:''RDOXIL.000X ' || SQLERRM || '''}]'));
        lc_jError.put('messages',lc_jlMessages);
        BB_P_ERROR(lc_jError);
        
        return json('e');


END BB_F_READ_DO_XML_IMPORT_LOG;
 

/
