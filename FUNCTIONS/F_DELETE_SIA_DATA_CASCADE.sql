--------------------------------------------------------
--  DDL for Function F_DELETE_SIA_DATA_CASCADE
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE FUNCTION "SPIDERS_DATA"."F_DELETE_SIA_DATA_CASCADE" (lc_nDOFID IN NUMBER ) RETURN VARCHAR2 AS 
  lc_jldataDML json_list;
  lc_jlstrDML json_list;
  lc_jdataResults json;
  lc_jstrResults json;
  lc_jdataMessages json;
  lc_jstrMessages json;
  lc_vdataDMLResult varchar2(255);
  lc_vstrDMLResult varchar2(255);
  siadatasql varchar2(2000) := '';
  siastructuressql varchar2(2000) := '';
  lc_vReturn varchar2(255);
BEGIN

  lc_jldataDML := json_list();
  lc_jlstrDML := json_list();

  siadatasql := 'delete from spd_t_sia_data where DOFID = ' || lc_nDOFID || ')';
  siastructuressql := 'delete from spd_t_sia_structures where DOFID = ' || lc_nDOFID || ')';

  lc_jldataDML.append(siadatasql);
  lc_jldataDML.append(siastructuressql);
  
  lc_jdataResults.put('results',lc_jldataDML);
  lc_jstrResults.put('results',lc_jlstrDML);
  
  lc_vdataDMLResult := BB_F_DML(lc_jdataResults, lc_jdataMessages);
  lc_vstrDMLResult := BB_F_DML(lc_jstrResults, lc_jstrMessages);
  
  lc_vReturn := 'success';
    
  RETURN lc_vReturn;
  
END F_DELETE_SIA_DATA_CASCADE;
 

/
