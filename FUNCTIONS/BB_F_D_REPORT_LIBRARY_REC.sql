--------------------------------------------------------
--  DDL for Function BB_F_D_REPORT_LIBRARY_REC
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE FUNCTION "SPIDERS_DATA"."BB_F_D_REPORT_LIBRARY_REC" (in_jJson json, o_jMessages out json) RETURN varchar2 AS

  lc_jJson json;  
  lc_data json_list;
  lc_rec_val json_value;
  lc_jValue json;
  lc_jvValue json_value;
  lc_nREPORTID number;
  lc_jlDML json_list;
  lc_jResults json;
  lc_vDMLResult varchar2(2000);
  lc_jMessages json;
  lc_jMessages2 json;
  
  thesql varchar2(2000) := '';

  --error stuff
  lc_jError json;
  lc_jlMessages json_list;
  json_error EXCEPTION;

BEGIN
/*
stub data
{"module":"reportLibrary","target":"deleteReportLibraryRecord","data":[{"REPORTID":11042}]}
*/

lc_jJson := json();
lc_jlDML := json_list();
lc_jResults := json();
lc_jMessages2 := json();

if(in_jJson.exist('data')) then
    if(in_jJson.get('data').is_array) then
        lc_data := json_list(in_jJson.get('data'));        
        lc_rec_val := lc_data.get(1); --return null on outofbounds

        if (lc_rec_val is not null) then
        
            lc_jValue := json(lc_rec_val);
            
            if (lc_jValue.exist('REPORTID')) then
                lc_jvValue := lc_jValue.get('REPORTID');
                lc_nREPORTID := getVal(lc_jvValue);
            end if;
        end if;
    end if;
end if;

if lc_nREPORTID > 0 then
  thesql := 'delete from spd_t_report_keywords where reportfileid in (select reportfileid from spd_t_report_files where reportid = ' || lc_nREPORTID || ')';
  
  lc_jlDML.append(thesql);
  lc_jResults.put('results',lc_jlDML);
  lc_vDMLResult := BB_F_DML(lc_jResults, lc_jMessages);
  lc_jMessages.put('statusDeleteKeyword','success');
  --lc_jJson.put('statusDeleteKeyword','success');
  
  
  thesql := 'delete from spd_t_report_files where reportid = ' || lc_nREPORTID;
  
  lc_jlDML.append(thesql);
  lc_jResults.put('results',lc_jlDML);
  lc_vDMLResult := BB_F_DML(lc_jResults, lc_jMessages);
  lc_jMessages.put('statusDeleteFile','success');
  --lc_jJson.put('statusDeleteFile','success');
  
  thesql := 'delete from spd_t_reports where reportid = ' || lc_nREPORTID;

  lc_jlDML.append(thesql);
  lc_jResults.put('results',lc_jlDML);
  lc_vDMLResult := BB_F_DML(lc_jResults, lc_jMessages);
  lc_jMessages.put('statusDeleteReportRec','success');
  --lc_jJson.put('statusDeleteReportLibraryRecord','success');
  
  --This is not properly checking for errors in the three deletes. this technical debt is low risk because we are dealing with deletes.
  lc_jlMessages := json_list('[{"status":"success"}]');
  lc_jJson.put('messages','');
  lc_jJson.put('status','success');
  o_jMessages := lc_jJson;
  
  

else
  raise json_error;
end if;
  
  return lc_vDMLResult;

exception
    when json_error then
        --htp.p('raised json_error in update func');
        lc_jlMessages := json_list();
        lc_jError := json();
        lc_jError.put('function','BB_F_D_REPORT_LIBRARY_REC');
        lc_jError.put('input',in_jJson.to_char);
        lc_jError.put('messages',lc_jlMessages);
        BB_P_ERROR(lc_jError);
        --htp.p('returning n');
        return 'e';
    when others then
        lc_jError := json();
        lc_jlMessages := json_list();
        lc_jError.put('function','BB_F_D_REPORT_LIBRARY_REC');
        lc_jError.put('input',in_jJson.to_char);
        lc_jlMessages.append(json_list('[{error:''DRLR.000X ' || SQLERRM || '''}]'));
        lc_jError.put('messages',lc_jlMessages);
        BB_P_ERROR(lc_jError);
        return 'e';


END BB_F_D_REPORT_LIBRARY_REC;
 

/
