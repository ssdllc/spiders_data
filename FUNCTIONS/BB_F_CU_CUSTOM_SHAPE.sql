--------------------------------------------------------
--  DDL for Function BB_F_CU_CUSTOM_SHAPE
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE FUNCTION "SPIDERS_DATA"."BB_F_CU_CUSTOM_SHAPE" (in_jJson json, o_jMessages out json) return varchar2 as 

    lc_jJson json;
    lc_data json_list;
    lc_rec_val json_value;
    lc_rec json;
    lc_jValue json;
    lc_jvValue json_value;
    
--CUSTOMIZE FOR TABLE FIELDS
    lc_nCUSTOMSHAPEID number :=0;
    lc_nSCENEMODELID number;
    lc_vCODE varchar(256) :='';
    lc_vDESCRIPTION varchar(256) :='';
    lc_nLENGTH number;
    lc_nWIDTH number;
    lc_nHEIGHT number;
    lc_nRADIUS number :=0;
    lc_nRED number;
    lc_nGREEN number;
    lc_nBLUE number;
    lc_nTRANSPARENCY number :=.5;
    lc_vRELATED_PLATFORM varchar(256) :='';
    lc_vCREATED_BY varchar(256) :='';
--END CUSTOMIZE FOR TABLE FIELDS
    
    
    lc_nSequence number;
    lc_nContinue number :=0;
    
    
    lc_jlDML json_list;
    lc_jResults json;
    lc_jMessages json;
    lc_vDMLResult varchar2(255);
    lc_ret_str varchar(512);
    lc_ret json;

    thesql varchar2(2000) := '';

    lc_vWhere varchar2(2000);

    --error stuff
    lc_jError json;
    lc_jlMessages json_list;
    json_error EXCEPTION;


BEGIN
/*
stub data
{module:"customShapes", target:"createCustomShape",data:[{{"ID":123,"CODE":"CL01","DESCRIPTION":"cleat","LENGTH":25,"WIDTH":3,"HEIGHT":5,"RED":0,"GREEN":255,"BLUE":0,"SCENEID":2222}]}
*/

lc_jResults := json();
lc_jlDML := json_list();


if(in_jJson.exist('data')) then
    if(in_jJson.get('data').is_array) then
        lc_data := json_list(in_jJson.get('data'));        
        lc_rec_val := lc_data.get(1); --return null on outofbounds

        if (lc_rec_val is not null) then
        
            lc_jValue := json(lc_rec_val);
            
--STANDARD SINGLE VALUE ITEMS
            if (lc_jValue.exist('CUSTOMSHAPEID')) then
                lc_jvValue := lc_jValue.get('CUSTOMSHAPEID');
                lc_nCUSTOMSHAPEID := getVal(lc_jvValue);
            end if;

            if (lc_jValue.exist('SCENEMODELID')) then
                lc_jvValue := lc_jValue.get('SCENEMODELID');
                lc_nSCENEMODELID := getVal(lc_jvValue);
            end if;
            if (lc_jValue.exist('CODE')) then
                lc_jvValue := lc_jValue.get('CODE');
                lc_vCODE := getVal(lc_jvValue);
            end if;
            if (lc_jValue.exist('DESCRIPTION')) then
                lc_jvValue := lc_jValue.get('DESCRIPTION');
                lc_vDESCRIPTION := getVal(lc_jvValue);
            end if; 
            if (lc_jValue.exist('LENGTH')) then
                lc_jvValue := lc_jValue.get('LENGTH');
                lc_nLENGTH := getVal(lc_jvValue);
            end if; 
            if (lc_jValue.exist('WIDTH')) then
                lc_jvValue := lc_jValue.get('WIDTH');
                lc_nWIDTH := getVal(lc_jvValue);
            end if; 
            if (lc_jValue.exist('HEIGHT')) then
                lc_jvValue := lc_jValue.get('HEIGHT');
                lc_nHEIGHT := getVal(lc_jvValue);
            end if;  
            if (lc_jValue.exist('RADIUS')) then
                lc_jvValue := lc_jValue.get('RADIUS');
                lc_nRADIUS := getVal(lc_jvValue);
            end if;            
            if (lc_jValue.exist('RED')) then
                lc_jvValue := lc_jValue.get('RED');
                lc_nRED := getVal(lc_jvValue);
            end if;  
            if (lc_jValue.exist('GREEN')) then
                lc_jvValue := lc_jValue.get('GREEN');
                lc_nGREEN := getVal(lc_jvValue);
            end if;  
            if (lc_jValue.exist('BLUE')) then
                lc_jvValue := lc_jValue.get('BLUE');
                lc_nBLUE := getVal(lc_jvValue);
            end if;  
            if (lc_jValue.exist('TRANSPARENCY')) then
                lc_jvValue := lc_jValue.get('TRANSPARENCY');
                lc_nTRANSPARENCY := getVal(lc_jvValue);
            end if;  
            if (lc_jValue.exist('RELATED_PLATFORM')) then
                lc_jvValue := lc_jValue.get('RELATED_PLATFORM');
                lc_vRELATED_PLATFORM := getVal(lc_jvValue);
            end if; 
--END STANDARD SINGLE VALUE ITEMS

            
--COMPLICATED ITEMS
--none
--END COMPLICATED ITEMS
            lc_nContinue := 1;
            
        end if;
    end if;
end if;


if lc_nContinue = 1 then

  if lc_nCUSTOMSHAPEID > 0 then
      --Update the table
      thesql := '
        update 
          spd_t_tele_custom_shapes 
        set 
          SCENEMODELID = ' ||  lc_nSCENEMODELID  || ',
          CODE = ''' ||  lc_vCODE   || ''',
          DESCRIPTION = ''' ||  lc_vDESCRIPTION   || ''',
          LENGTH = ''' ||  lc_nLENGTH   || ''',
          WIDTH    = ''' ||  lc_nWIDTH   || ''',
          HEIGHT = ''' ||  lc_nHEIGHT    || ''',
          RED = ''' ||  lc_nRED    || ''',
          GREEN = ''' ||  lc_nGREEN    || ''',
          BLUE = ''' ||  lc_nBLUE    || ''',
          TRANSPARENCY = ''' ||  lc_nTRANSPARENCY    || ''',
          RADIUS = ''' ||  lc_nRADIUS   || ''',
          RELATED_PLATFORM = ''' ||  lc_vRELATED_PLATFORM   || '''
        where
          CUSTOMSHAPEID = ' || lc_nCUSTOMSHAPEID;
  
        lc_jlDML.append(thesql);
        lc_jResults.put('results',lc_jlDML);
        lc_vDMLResult := BB_F_DML(lc_jResults, lc_jMessages);
        o_jMessages := lc_jMessages;
        
  else
  
  
      select spd_s_tele_custom_shapes.nextval into lc_nSequence from dual;
      

      --Insert record into table
      thesql := '
        insert into 
          spd_t_tele_custom_shapes (
            CUSTOMSHAPEID,
            SCENEMODELID,
            CODE,
            DESCRIPTION,
            LENGTH,
            WIDTH,
            HEIGHT,
            RADIUS,
            RED,
            GREEN,
            BLUE,
            TRANSPARENCY,
            RELATED_PLATFORM,
            CREATED_BY
          )
        
          values (
          ' || lc_nSequence || ',
          ' ||  lc_nSCENEMODELID  || ',
          ''' ||  lc_vCODE   || ''',
          ''' ||  lc_vDESCRIPTION   || ''',
          ''' ||  lc_nLENGTH   || ''',
          ' ||  lc_nWIDTH   || ',
          ' ||  lc_nHEIGHT   || ',
          ' ||  lc_nRADIUS    || ',
          ' ||  lc_nRED    || ',
          ' ||  lc_nGREEN    || ',
          ' ||  lc_nBLUE    || ',
          ' ||  lc_nTRANSPARENCY    || ',
          ''' ||  lc_vRELATED_PLATFORM   || ''',
          ''' ||APEX_UTIL.GET_SESSION_STATE('APP_USER') || '''
          )
      ';
      
      lc_jlDML.append(thesql);
      lc_jResults.put('results',lc_jlDML);
      lc_vDMLResult := BB_F_DML(lc_jResults, lc_jMessages);
      lc_jMessages.put('id',lc_nSequence);
      o_jMessages := lc_jMessages;
      
  end if;
  
  
  return lc_vDMLResult;
else 
  raise json_error;
end if;


exception
when json_error then
    --htp.p('raised json_error in update func');
    lc_jlMessages:=json_list();
    lc_jError := json();
    lc_jError.put('function','BB_F_CU_CUSTOM_SHAPE');
    lc_jError.put('input',in_jJson.to_char);
    lc_jError.put('messages',lc_jlMessages);
    BB_P_ERROR(lc_jError);
    return 'e';
    --htp.p('returning n');
when others then
    lc_jError := json();
    lc_jlMessages := json_list();
    lc_jError.put('function','BB_F_CU_CUSTOM_SHAPE');
    lc_jError.put('input','');
    lc_jlMessages.append(json_list('[{error:''CCS.000X ' || SQLERRM || '''}]'));
    lc_jError.put('messages',lc_jlMessages);
    BB_P_ERROR(lc_jError);
    return 'e';


END BB_F_CU_CUSTOM_SHAPE;
 

/
