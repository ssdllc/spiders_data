--------------------------------------------------------
--  DDL for Function BB_F_GET_MATERIAL_GUID
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE FUNCTION "SPIDERS_DATA"."BB_F_GET_MATERIAL_GUID" (in_vVal varchar2) return varchar2 as 

lc_vReturn varchar2(255) := 'NONE';

begin

if in_vVal = 'ALU' then lc_vReturn := '533f6ea0-c0e4-11e4-9f80-eb3fd8006b01'; end if;
if in_vVal = 'ASP' then lc_vReturn := '533f95b0-c0e4-11e4-9f80-eb3fd8006b01'; end if;
if in_vVal = 'BRZ' then lc_vReturn := '533f95b1-c0e4-11e4-9f80-eb3fd8006b01'; end if;
if in_vVal = 'CNC' then lc_vReturn := '533f95b2-c0e4-11e4-9f80-eb3fd8006b01'; end if;
if in_vVal = 'CPR' then lc_vReturn := '533f95b3-c0e4-11e4-9f80-eb3fd8006b01'; end if;
if in_vVal = 'DI' then lc_vReturn := '533f95b4-c0e4-11e4-9f80-eb3fd8006b01'; end if;
if in_vVal = 'FBG' then lc_vReturn := '533f95b5-c0e4-11e4-9f80-eb3fd8006b01'; end if;
if in_vVal = 'FILL' then lc_vReturn := '533f95b6-c0e4-11e4-9f80-eb3fd8006b01'; end if;
if in_vVal = 'FM' then lc_vReturn := '533fbcc0-c0e4-11e4-9f80-eb3fd8006b01'; end if;
if in_vVal = 'GS' then lc_vReturn := '533fbcc1-c0e4-11e4-9f80-eb3fd8006b01'; end if;
if in_vVal = 'HDPE' then lc_vReturn := '533fbcc2-c0e4-11e4-9f80-eb3fd8006b01'; end if;
if in_vVal = 'MSY' then lc_vReturn := '533fbcc3-c0e4-11e4-9f80-eb3fd8006b01'; end if;
if in_vVal = 'PLA' then lc_vReturn := '533fbcc5-c0e4-11e4-9f80-eb3fd8006b01'; end if;
if in_vVal = 'PE' then lc_vReturn := '533fbcc4-c0e4-11e4-9f80-eb3fd8006b01'; end if;
if in_vVal = 'PVC' then lc_vReturn := '533fbcc6-c0e4-11e4-9f80-eb3fd8006b01'; end if;
if in_vVal = 'RUB' then lc_vReturn := '533fe3d0-c0e4-11e4-9f80-eb3fd8006b01'; end if;
if in_vVal = 'SS' then lc_vReturn := '533fe3d1-c0e4-11e4-9f80-eb3fd8006b01'; end if;
if in_vVal = 'STL' then lc_vReturn := '533fe3d2-c0e4-11e4-9f80-eb3fd8006b01'; end if;
if in_vVal = 'STN' then lc_vReturn := '533fe3d3-c0e4-11e4-9f80-eb3fd8006b01'; end if;
if in_vVal = 'TTM' then lc_vReturn := '533fe3d4-c0e4-11e4-9f80-eb3fd8006b01'; end if;
if in_vVal = 'UTM' then lc_vReturn := '533fe3d5-c0e4-11e4-9f80-eb3fd8006b01'; end if;

return lc_vReturn;

exception
  when others then
      execute immediate 'insert into debug (functionname, input, message) values (''BB_F_GET_MATERIAL_GUID'',''' || in_vVal || ''',''error: ' || sqlerrm || ''')';
      return 'e';

end;


/*

type = 'material'
[
{'id':'533f6ea0-c0e4-11e4-9f80-eb3fd8006b01','key':['Aluminum','ALU','533f6ea0-c0e4-11e4-9f80-eb3fd8006b01'],'value':null},
{'id':'533f95b0-c0e4-11e4-9f80-eb3fd8006b01','key':['Asphalt','ASP','533f95b0-c0e4-11e4-9f80-eb3fd8006b01'],'value':null},
{'id':'533f95b1-c0e4-11e4-9f80-eb3fd8006b01','key':['Bronze','BRZ','533f95b1-c0e4-11e4-9f80-eb3fd8006b01'],'value':null},
{'id':'533f95b2-c0e4-11e4-9f80-eb3fd8006b01','key':['Concrete','CNC','533f95b2-c0e4-11e4-9f80-eb3fd8006b01'],'value':null},
{'id':'533f95b3-c0e4-11e4-9f80-eb3fd8006b01','key':['Copper','CPR','533f95b3-c0e4-11e4-9f80-eb3fd8006b01'],'value':null},
{'id':'533f95b4-c0e4-11e4-9f80-eb3fd8006b01','key':['Ductile Iron','DI','533f95b4-c0e4-11e4-9f80-eb3fd8006b01'],'value':null},
{'id':'533f95b5-c0e4-11e4-9f80-eb3fd8006b01','key':['Fiberglass','FBG','533f95b5-c0e4-11e4-9f80-eb3fd8006b01'],'value':null},
{'id':'533f95b6-c0e4-11e4-9f80-eb3fd8006b01','key':['Fill','FILL','533f95b6-c0e4-11e4-9f80-eb3fd8006b01'],'value':null},
{'id':'533fbcc0-c0e4-11e4-9f80-eb3fd8006b01','key':['Foam','FM','533fbcc0-c0e4-11e4-9f80-eb3fd8006b01'],'value':null},
{'id':'533fbcc1-c0e4-11e4-9f80-eb3fd8006b01','key':['Galvanized Steel','GS','533fbcc1-c0e4-11e4-9f80-eb3fd8006b01'],'value':null},


{'id':'533fbcc2-c0e4-11e4-9f80-eb3fd8006b01','key':['High Density Polyethyline','HDPE','533fbcc2-c0e4-11e4-9f80-eb3fd8006b01'],'value':null},
{'id':'533fbcc3-c0e4-11e4-9f80-eb3fd8006b01','key':['Masonry','MSY','533fbcc3-c0e4-11e4-9f80-eb3fd8006b01'],'value':null},
{'id':'533fbcc5-c0e4-11e4-9f80-eb3fd8006b01','key':['Plastic','PLA','533fbcc5-c0e4-11e4-9f80-eb3fd8006b01'],'value':null},
{'id':'533fbcc4-c0e4-11e4-9f80-eb3fd8006b01','key':['Polyethyline','PE','533fbcc4-c0e4-11e4-9f80-eb3fd8006b01'],'value':null},
{'id':'533fbcc6-c0e4-11e4-9f80-eb3fd8006b01','key':['Polyvinyl Chloride','PVC','533fbcc6-c0e4-11e4-9f80-eb3fd8006b01'],'value':null},
{'id':'533fe3d0-c0e4-11e4-9f80-eb3fd8006b01','key':['Rubber','RUB','533fe3d0-c0e4-11e4-9f80-eb3fd8006b01'],'value':null},
{'id':'533fe3d1-c0e4-11e4-9f80-eb3fd8006b01','key':['Stainless Steel','SS','533fe3d1-c0e4-11e4-9f80-eb3fd8006b01'],'value':null},
{'id':'533fe3d2-c0e4-11e4-9f80-eb3fd8006b01','key':['Steel','STL','533fe3d2-c0e4-11e4-9f80-eb3fd8006b01'],'value':null},
{'id':'533fe3d3-c0e4-11e4-9f80-eb3fd8006b01','key':['Stone','STN','533fe3d3-c0e4-11e4-9f80-eb3fd8006b01'],'value':null},
{'id':'533fe3d4-c0e4-11e4-9f80-eb3fd8006b01','key':['Treated Timber','TTM','533fe3d4-c0e4-11e4-9f80-eb3fd8006b01'],'value':null},
{'id':'533fe3d5-c0e4-11e4-9f80-eb3fd8006b01','key':['Untreated Timber','UTM','533fe3d5-c0e4-11e4-9f80-eb3fd8006b01'],'value':null}
]

*/
 

/
