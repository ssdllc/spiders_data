--------------------------------------------------------
--  DDL for Function BB_F_READ_DO_PROC_STATUS
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE FUNCTION "SPIDERS_DATA"."BB_F_READ_DO_PROC_STATUS" (in_jJson json) RETURN json AS

    lc_jJson json;
    lc_data json_list;
    lc_rec_val json_value;
    lc_rec json;
    lc_employeeid_val json_value; --
    lc_employeeid varchar(256);
    lc_cnt number;
    lc_nAdoid number :=0;
    lc_adoid_val json_value;

    lc_ret_str varchar(512);
    lc_ret json;

    thesql varchar2(2000) := '';

    lc_vWhere varchar2(2000);

    --error stuff
    lc_jError json;
    lc_jlMessages json_list;
    json_error EXCEPTION;


BEGIN
/*
stub data
{"module":"xmlImportManager","target":"spidersDeliveryOrderProcessingStatus","data":["ADOID":12345]}
*/

if(in_jJson.exist('data')) then
    if(in_jJson.get('data').is_array) then
        lc_data := json_list(in_jJson.get('data'));
        lc_rec_val := lc_data.get(1); --return null on outofbounds
        if (lc_rec_val is not null) then
            lc_rec := json(lc_rec_val);
            if (lc_rec.exist('ADOID')) then
                lc_adoid_val := lc_rec.get('ADOID');
                lc_nAdoid := lc_adoid_val.get_number;
            else
                lc_nAdoid :=0;
            end if;
        end if;
    end if;
end if;

if lc_nADOID > 0 then

    thesql := 'select
        (select count(adoid) from spd_t_import_log where adoid = ' || lc_nADOID || ' and upper(import_section) = ''FILE UPLOADED'') FILE_UPLOADED,
        (select count(adoid) from spd_t_import_log where adoid = ' || lc_nADOID || ' and upper(import_section) = ''IMPORT COMPLETE'' and notes = ''COMPLETE'') IMPORT_COMPLETE,
        (select count(adoid) from spd_t_import_log where adoid = ' || lc_nADOID || ' and upper(import_section) = ''FACILITY MATCH COMPLETE'') FACILITY_MATCH_COMPLETE,
        (select count(adoid) from spd_t_import_log where adoid = ' || lc_nADOID || ' and upper(import_section) = ''UPDATE DELIVERY ORDER TABLE'') IMPORT_STARTED,
        (select count(adoid) from spd_t_import_log where adoid = ' || lc_nADOID || ' and upper(import_section) = ''XML FACILITIES IMPORTED'') XML_FACILITIES_IMPORTED,
        (select sum(count_of_records) from spd_t_import_log where adoid = ' || lc_nADOID || ') SUCCESSFUL_COUNT,
        (select sum(total_records) from spd_t_import_log where adoid = ' || lc_nADOID || ') TOTAL_COUNT,
        (select count(ref_id) from debug where ref_id = ' || lc_nADOID || ') ERRORS
    from
        dual';
else
    thesql := '';
end if;

    lc_jJson := json_dyn.executeObject(thesql);

    return lc_jJson;

exception
    when json_error then
        --htp.p('raised json_error in update func');
        lc_jError := json();
        lc_jError.put('function','BB_F_READ_DO_PROC_STATUS');
        lc_jError.put('input',in_jJson.to_char);
        lc_jError.put('messages',lc_jlMessages);
        BB_P_ERROR(lc_jError);
        --htp.p('returning n');
        return json('e');
    when others then
        lc_jError := json();
        lc_jError.put('function','BB_F_READ_DO_PROC_STATUS');
        lc_jError.put('input',in_jJson.to_char);
        lc_jlMessages.append(json_list('[{error:''RDOWX.000X ' || SQLERRM || '''}]'));
        lc_jError.put('messages',lc_jlMessages);
        BB_P_ERROR(lc_jError);
        return json('e');


END BB_F_READ_DO_PROC_STATUS;
 

/
