--------------------------------------------------------
--  DDL for Function SAVEBLOBASBFILE
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE FUNCTION "SPIDERS_DATA"."SAVEBLOBASBFILE" (in_vFlowsFilesName in varchar2) return varchar2 IS

lc_bBlob BLOB;
lc_nStart NUMBER := 1;
lc_nBytelen NUMBER := 32000;
lc_nLen NUMBER;
lc_rRaw RAW(32000);
lc_nContinue number :=0;
x NUMBER;

l_output utl_file.file_type;

lc_jError json;
lc_jlMessages json_list;

lc_vFilename varchar2(2000);
lc_nSequence number :=0;

BEGIN

lc_nContinue := 1;

select
    wwv_flow_files.FILENAME,
    wwv_flow_files.BLOB_CONTENT,
    dbms_lob.getlength(wwv_flow_files.BLOB_CONTENT)
  into 
    lc_vFilename,
    lc_bBlob,
    lc_nLen
from
    wwv_flow_files
where
    name = in_vFlowsFilesName;
    

lc_nContinue := 2;

-- define output directory
l_output := utl_file.fopen('SPD_630000085', lc_vFilename,'wb', 32760);

lc_nStart := 1;
lc_nBytelen := 32000;

lc_nContinue := 3;
-- save blob length
x := lc_nLen;


lc_nContinue := 4;

-- if small enough for a single write
IF lc_nLen < 32760 THEN
    utl_file.put_raw(l_output,lc_bBlob);
    utl_file.fflush(l_output);
ELSE -- write in pieces
    lc_nStart := 1;
    WHILE lc_nStart < lc_nLen and lc_nBytelen > 0
    LOOP
       dbms_lob.read(lc_bBlob,lc_nBytelen,lc_nStart,lc_rRaw);
    
       utl_file.put_raw(l_output,lc_rRaw);
       utl_file.fflush(l_output); 
    
       -- set the start position for the next cut
       lc_nStart := lc_nStart + lc_nBytelen;
    
       -- set the end position if less than 32000 bytes
       x := x - lc_nBytelen;
       IF x < 32000 THEN
          lc_nBytelen := x;
       END IF;
    end loop;
END IF;
utl_file.fclose(l_output);

lc_nContinue := 5;

return lc_vFilename;

exception 
  when others then
      lc_jError := json();
      lc_jlMessages := json_list();
      lc_jError.put('function','SAVEBLOBASBFILE');
      lc_jError.put('input','');
      lc_jlMessages.append(json_list('[{error:''SBABF.000X ' || SQLERRM || '''}]'));
      lc_jError.put('messages',lc_jlMessages);
      BB_P_ERROR(lc_jError);
      htp.p(lc_nContinue);

end saveBlobAsBfile;
 

/
