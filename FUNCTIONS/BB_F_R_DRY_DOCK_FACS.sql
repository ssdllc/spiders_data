--------------------------------------------------------
--  DDL for Function BB_F_R_DRY_DOCK_FACS
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE FUNCTION "SPIDERS_DATA"."BB_F_R_DRY_DOCK_FACS" (in_jJson json) RETURN json AS

  lc_jJson json;
  lc_data json_list;
  lc_rec_val json_value;
  lc_jValue json;
  lc_rec json;
  lc_jvValue json_value; --
  lc_nSCENEMODELID number;
  lc_nPL number;

  lc_ret_str varchar(512);
  lc_ret json;

  thesql varchar2(2000) := '';

  --error stuff
  lc_jError json;
  lc_jlMessages json_list;
  json_error EXCEPTION;

  lc_jKeywords json;

  lc_jlAoData json_list;
  lc_vSearch varchar2(2000);
  lc_jvSearch json_value;

  lc_jValue2 json;
  lc_jvValue2 json_value;
  lc_jvValue3 json_value;
  lc_jvValue4 json_value;

  lc_jValue3 json;

  lc_jlValue2 json_list;
  lc_jlValue4 json_list;


  lc_vValue varchar2(2000);
  lc_vWhere varchar2(2000);
  lc_nStart number := 1;
  lc_nEnd number := 10;

  lc_nOrderByCol number;
  lc_vOrderByCol varchar2(2000);
  lc_vOrderBySort varchar2(2000);
  lc_vOrderBy varchar2(2000) := ' SPD_FAC_NAME asc ';

  lc_nGetValuesFromAodata number;


BEGIN
/*
stub data
{"module":"dryDock", "target": "homeDatatable", data:[]}
*/
lc_jJson := json();


/*!! Data is not needed at this moment !!*/
if(in_jJson.exist('data')) then
    if(in_jJson.get('data').is_array) then
        lc_data := json_list(in_jJson.get('data'));
        lc_rec_val := lc_data.get(1); --return null on outofbounds

        if (lc_rec_val is not null) then

            lc_jValue := json(lc_rec_val);

            /*
            if (lc_jValue.exist('SCENEMODELID')) then
                lc_jvValue := lc_jValue.get('SCENEMODELID');
                lc_nSCENEMODELID := getVal(lc_jvValue);
            end if;
            */
        end if;
    end if;
end if;

--tc updated this to use spd_t_productlines on 20170829
select productlineid into lc_nPL from spiders_data.spd_t_productlines where productline_display = 'Dry Docks';
--select listid into lc_nPL from spiders_data.spd_t_lists where listname = 'list_product_lines' and displayvalue = 'Dry Docks';

lc_vWhere := ' PRODUCT_LINE_LISTID = ' || lc_nPL;



lc_nGetValuesFromAodata := parseAoData(in_jJson, lc_vSearch, lc_nStart, lc_nEnd, lc_vOrderBy);

if(lc_nGetValuesFromAodata = 0) then
  lc_vSearch :='';
  lc_nStart :=1;
  lc_nEnd :=10;
  lc_vOrderBy :=' SPD_FAC_NAME asc ';
end if;

if length(lc_vSearch) is null then
  lc_vWhere := lc_vWhere;
else
  if length(lc_vWhere) is null then
    lc_vWhere := ' Where upper(CODE ||
    DESCRIPTION     ||
    CREATED_BY)
    like ''%' || upper(lc_vSearch) || '%''';
  else
    lc_vWhere := lc_vWhere || ' AND
     upper(CODE ||
    DESCRIPTION     ||
    CREATED_BY)
    like ''%' || upper(lc_vSearch) || '%''';
  end if;
end if;


/*select
    *
  from (
  row_number() over (order by ' || lc_vOrderBy || ') rn,
  count(*) over (partition by (select 1 from dual)) countOfRecords
*/
thesql := '
  select
      FACID,
      INFADS_FACILITY_ID,
      PRODUCT_LINE_LISTID,
      SPD_FAC_NAME,
      SPD_FAC_NO,
      FILESKEY,
      (select
            count(xmlfileid)
        from
            spiders_data.spd_t_xml_files xmlf,
            spiders_data.spd_t_inter_files inter
        where
            inter.FILESKEY = fac.fileskey
            and xmlf.xmlfileid = inter.filesid) count_of_csv_files,
      activity.ACTIVITY_TITLE
  from
      spiders_data.spd_t_facilities fac,
      spiders_data.spd_mv_inf_activity activity
  where
      fac.SPD_UIC = activity.UIC
  and
  ' || lc_vWhere || '
  order by
  ' || lc_vOrderBy; --|| ';
--)
--where rn between ' || lc_nStart || ' and ' || lc_nEnd;


  lc_jJson := json_dyn.executeObject(thesql);

  return lc_jJson;

exception
    when json_error then
        --htp.p('raised json_error in update func');
        lc_jError := json();
        lc_jError.put('function','BB_F_R_DRY_DOCK_FACS');
        lc_jError.put('input',in_jJson.to_char);
        lc_jError.put('messages',lc_jlMessages);
        BB_P_ERROR(lc_jError);
        --htp.p('returning n');
        return json('e');
    when others then
        lc_jError := json();
        lc_jError.put('function','BB_F_R_DRY_DOCK_FACS');
        lc_jError.put('input',in_jJson.to_char);
        lc_jlMessages.append(json_list('[{error:''RDDF.000X ' || SQLERRM || '''}]'));
        lc_jError.put('messages',lc_jlMessages);
        BB_P_ERROR(lc_jError);
        return json('e');


END BB_F_R_DRY_DOCK_FACS;

/
