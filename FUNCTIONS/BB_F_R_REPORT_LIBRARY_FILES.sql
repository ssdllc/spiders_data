--------------------------------------------------------
--  DDL for Function BB_F_R_REPORT_LIBRARY_FILES
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE FUNCTION "SPIDERS_DATA"."BB_F_R_REPORT_LIBRARY_FILES" (in_jJson json) RETURN json AS

  lc_jJson json;
  lc_data json_list;
  lc_rec_val json_value;
  lc_rec json;
  lc_REPORTID_val json_value; --
  lc_nREPORTID number;

  lc_ret_str varchar(512);
  lc_ret json;

  thesql varchar2(2000) := '';

  --error stuff
  lc_jError json;
  lc_jlMessages json_list;
  json_error EXCEPTION;
  
  lc_jKeywords json;

BEGIN
/*
stub data
{"module":"reportLibrary","target":"reportFiles","data":[{"REPORTID":11042}]}
*/

if(in_jJson.exist('data')) then
    if(in_jJson.get('data').is_array) then
        lc_data := json_list(in_jJson.get('data'));
        lc_rec_val := lc_data.get(1); --return null on outofbounds
        if (lc_rec_val is not null) then
            lc_rec := json(lc_rec_val);
            if (lc_rec.exist('REPORTID')) then
                lc_REPORTID_val := lc_rec.get('REPORTID');
                lc_nREPORTID := lc_REPORTID_val.get_number;
            else
                lc_jlMessages.append(json_list('[{error:''Error RDRLF-0001 There has been an error processing the request''}]'));
            end if;
        else
            lc_jlMessages.append(json_list('[{error:''Error RDRLF-0002 There has been an error processing the request''}]'));
        end if;
    else
        lc_jlMessages.append(json_list('[{error:''Error RDRLF-0003 There has been an error processing the request''}]'));
    end if;
else
    lc_jlMessages.append(json_list('[{error:''Error RDRLF-0004 There has been an error processing the request''}]'));
end if;



thesql := '

select
    reportfileid,
    report_name,
    GETFILENAMEFROMBFILE(the_bfile) FILENAME,
    GETFILESIZEFROMBFILE(the_bfile) FILESIZE
from
    spd_t_report_files
where
    reportid = ' || lc_nREPORTID || '
    and BFILEEXISTS(the_bfile) = 1
order by
    FILENAME asc
';

  lc_jJson := json_dyn.executeObject(thesql);

  return lc_jJson;

exception
    when json_error then
        --htp.p('raised json_error in update func');
        lc_jError := json();
        lc_jError.put('function','BB_F_R_REPORT_LIBRARY_FILES');
        lc_jError.put('input',in_jJson.to_char);
        lc_jError.put('messages',lc_jlMessages);
        BB_P_ERROR(lc_jError);
        --htp.p('returning n');
        return json('e');
    when others then
        lc_jError := json();
        lc_jError.put('function','BB_F_R_REPORT_LIBRARY_FILES');
        lc_jError.put('input',in_jJson.to_char);
        lc_jlMessages.append(json_list('[{error:''RDRLF.000X ' || SQLERRM || '''}]'));
        lc_jError.put('messages',lc_jlMessages);
        BB_P_ERROR(lc_jError);
        return json('e');


END BB_F_R_REPORT_LIBRARY_FILES;
 

/
