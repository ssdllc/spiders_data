--------------------------------------------------------
--  DDL for Function F_GETMOSTRECENTDOFID
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE FUNCTION "SPIDERS_DATA"."F_GETMOSTRECENTDOFID" (in_nDOFID number) return number as

lc_nReturn number :=0;
lc_nFACID number :=0;
lc_nCount number :=0;
lc_dStartDate date := null;
lc_dPreviousStartDate date := null;

begin

--we want the most recent dofid that isnt the current dofid and that has approved data
for rec in (select facid from spd_t_delivery_order_facs where deliveryorderfacilityid = in_nDOFID) loop
  lc_nFACID := rec.facid;
end loop;
dbms_output.put_line('lc_nFACID: '||lc_nFACID);

/* Date for this inspection of this facility */
if lc_nFACID > 0 then
  select count(*) into lc_nCount from (select DELIVERYORDERS.ONSITE_START_DATE
  from spd_t_delivery_order_facs DOFACS
  inner join spd_t_delivery_orders DELIVERYORDERS
  on DELIVERYORDERS.DELIVERYORDERID = DOFACS.DELIVERYORDERID
  and DOFACS.DELIVERYORDERFACILITYID = in_nDOFID);
end if;

if lc_nCount = 1 then
  select DELIVERYORDERS.ONSITE_START_DATE into lc_dStartDate
  from spd_t_delivery_order_facs DOFACS
  inner join spd_t_delivery_orders DELIVERYORDERS
  on DELIVERYORDERS.DELIVERYORDERID = DOFACS.DELIVERYORDERID
  and DOFACS.DELIVERYORDERFACILITYID = in_nDOFID;
end if;

lc_nCount := 0;

if lc_dStartDate IS NOT NULL then
  select count(*) into lc_nCount from (select MAX(DELIVERYORDERS.ONSITE_START_DATE)
  from spd_t_delivery_order_facs DOFACS
  inner join spd_t_delivery_orders DELIVERYORDERS
  on DELIVERYORDERS.DELIVERYORDERID = DOFACS.DELIVERYORDERID
  and DOFACS.facid = lc_nFACID
  and DELIVERYORDERS.ONSITE_START_DATE < lc_dStartDate
  and DOFACS.DELIVERYORDERFACILITYID != in_nDOFID
  group by DOFACS.FACID);
end if;

/* Getting maximum start date prior to the date of this inspection of this facility */
/* Grouped by facility id */
if lc_nCount = 1 then
  select MAX(DELIVERYORDERS.ONSITE_START_DATE) into lc_dPreviousStartDate
  from spd_t_delivery_order_facs DOFACS
  inner join spd_t_delivery_orders DELIVERYORDERS
  on DELIVERYORDERS.DELIVERYORDERID = DOFACS.DELIVERYORDERID
  and DOFACS.facid = lc_nFACID
  and DELIVERYORDERS.ONSITE_START_DATE < lc_dStartDate
  and DOFACS.DELIVERYORDERFACILITYID != in_nDOFID
  group by DOFACS.FACID;
else 
  dbms_output.put_line('lc_dPreviousStartDate not selected');
end if;

lc_nCount := 0;

if lc_dPreviousStartDate IS NOT NULL then
  select count(*) into lc_nCount from (select DOFACS.DELIVERYORDERFACILITYID
  from spd_t_delivery_order_facs DOFACS
  inner join spd_t_delivery_orders DELIVERYORDERS
  on DELIVERYORDERS.DELIVERYORDERID = DOFACS.DELIVERYORDERID
  and DOFACS.facid = lc_nFACID
  and DELIVERYORDERS.ONSITE_START_DATE = lc_dPreviousStartDate
  and DOFACS.DELIVERYORDERFACILITYID != in_nDOFID
  WHERE ROWNUM <= 1);
else 
  dbms_output.put_line('lc_nCount for DOFACS is zero');
end if;
 
if lc_nCount = 1 then
  select DOFACS.DELIVERYORDERFACILITYID into lc_nReturn
  from spd_t_delivery_order_facs DOFACS
  inner join spd_t_delivery_orders DELIVERYORDERS
  on DELIVERYORDERS.DELIVERYORDERID = DOFACS.DELIVERYORDERID
  and DOFACS.facid = lc_nFACID
  and DELIVERYORDERS.ONSITE_START_DATE = lc_dPreviousStartDate
  and DOFACS.DELIVERYORDERFACILITYID != in_nDOFID
  WHERE ROWNUM <= 1;
else 
  dbms_output.put_line('lc_nReturn not selected');
end if;

return lc_nReturn;

exception when others then
  lc_nReturn := -1;
  return lc_nReturn;

end;

/
