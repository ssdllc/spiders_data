create or replace FUNCTION                "BB_F_READ_DOS_WITH_XML_FILE" (in_jJson json) RETURN json AS

    lc_jJson json;
    lc_data json_list;
    lc_rec_val json_value;
    lc_rec json;
    lc_employeeid_val json_value; --
    lc_employeeid varchar(256);
    lc_cnt number;
    lc_nAdoid number :=0;
    lc_adoid_val json_value;
    
    lc_ret_str varchar(512);
    lc_ret json;

    thesql varchar2(2000) := '';
    
    lc_vWhere varchar2(2000);


    --error stuff
    lc_jError json;
    lc_jlMessages json_list;
    json_error EXCEPTION;


BEGIN
/*
stub data
{"module":"xmlImportManager","target":"deliveryOrdersWithXML","data":["ADOID":12345]}
*/
--htp.p('in_jJson = '||in_jJson.toString());
if(in_jJson.exist('data')) then
    --htp.p('IT HAS data');
    if(in_jJson.get('data').is_array) then
        lc_data := json_list(in_jJson.get('data'));
        --htp.p('got the data list');
        lc_rec_val := lc_data.get(1); --return null on outofbounds
        if (lc_rec_val is not null) then
            --htp.p('got the rec');
            lc_rec := json(lc_rec_val);
            if (lc_rec.exist('ADOID')) then
                --htp.p('ADOID does exist as an attribute');
                lc_adoid_val := lc_rec.get('ADOID');
                lc_nAdoid := lc_adoid_val.get_number;
            else
                lc_nAdoid :=0;
            end if;
        end if;
    end if;
end if;

if lc_nADOID = 0 then
  --htp.p('IT IS A ZERO');
  lc_vWhere := 'do.adoid = xf.adoid(+)';
else
  --htp.p('IT IS NOT A ZERO - '||lc_nADOID);
  lc_vWhere := 'do.adoid = xf.adoid(+) and do.adoid = ' || lc_nADOID;
end if;

--htp.p('DOES IT GET HERE? - '||lc_vWhere);
--was calculating facility count this way (before 20171220):
-- (select count(del_ord_facs.deliveryorderfacilityid) from spd_t_delivery_order_facs del_ord_facs, spd_t_do_facilities dof where del_ord_facs.dofid = dof.dofid and dof.ADOID = do.ADOID) Facilities,
thesql := 'select
    do.ADOID,
    0 APID,
    decode(do.PROJECTED_START_DATE,null,to_date(''01-JAN-1901'',''DD-MON-YYYY''),do.PROJECTED_START_DATE) START_DATE,
    do.DELIVERY_ORDER_NAME,
    (select count(dof.dofid) from spd_t_do_facilities dof where dof.ADOID = do.ADOID) Facilities,
    decode(xf.filename,null,''No File Uploaded'',xf.filename) filename,
    ''NA'' planned_year,
    ''NA'' planned_qtr
from
    spd_t_delivery_orders do,
    spd_t_xml_files xf
where   
    ' || lc_vWhere || '
order by
    delivery_order_name
';

    lc_jJson := json_dyn.executeObject(thesql);

    return lc_jJson;

exception
    when json_error then
        --htp.p('raised json_error in update func');
        lc_jError := json();
        lc_jError.put('function','BB_F_READ_DOS_WITH_XML_FILE');
        lc_jError.put('input',in_jJson.to_char);
        lc_jError.put('messages',lc_jlMessages);
        BB_P_ERROR(lc_jError);
        --htp.p('returning n');
        return json('e');
    when others then
        lc_jError := json();
        lc_jError.put('function','BB_F_READ_DOS_WITH_XML_FILE');
        lc_jError.put('input',in_jJson.to_char);
        lc_jlMessages.append(json_list('[{error:''RDOWX.000X ' || SQLERRM || '''}]'));
        lc_jError.put('messages',lc_jlMessages);
        BB_P_ERROR(lc_jError);
        return json('e');


END BB_F_READ_DOS_WITH_XML_FILE;