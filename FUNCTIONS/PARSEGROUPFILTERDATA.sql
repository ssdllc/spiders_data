--------------------------------------------------------
--  DDL for Function PARSEGROUPFILTERDATA
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE FUNCTION "SPIDERS_DATA"."PARSEGROUPFILTERDATA" (in_jJson json, in_nIncludeWhere number default 1) return varchar2 as
  lc_jJson json;
  lc_data json_list;
  lc_rec_val json_value;
  lc_rec json;
  lc_adoid_val json_value; --
  lc_nAdoid number;

  lc_ret_str varchar(512);
  lc_ret json;

  thesql varchar2(2000) := '';

  --error stuff
  lc_jError json;
  lc_jlMessages json_list;
  json_error EXCEPTION;
  
  
lc_jlData json_list;
lc_jvRec json_value;
lc_jlValue json_list;

lc_jvRec2 json_value;
lc_jValue json;

lc_jvValue json_value;
lc_vValue varchar2(2000) := '';
in_vGroup varchar2(2000) := 'PRODUCTLINE';
lc_vStatus varchar2(2000);


lc_jlGroupNames json_list;
lc_vGroupName varchar2(2000);
lc_jlDataValue json_list;
lc_vInClause varchar2(2000);

  lc_vWhere varchar2(2000);
  lc_vSearch varchar2(2000);
  lc_nStart number := 1;
  lc_nEnd number := 10;
  
  lc_nOrderByCol number;
  lc_vOrderByCol varchar2(2000);
  lc_vOrderBySort varchar2(2000);
  lc_vOrderBy varchar2(2000) := ' time asc ';
  
  lc_nGetValuesFromAodata number;


begin
/*
stubb data
'{"groupFilterData" : [
{"group_name" : "PRODUCTLINE" , "data" : [ 100085, 100086]},
{"group_name" : "YEAR" , "data" : [ 2014,2013,2012 ]},
{"group_name" : "STATE" , "data" : [ "HI","VA"]}
]}'
*/


lc_jJson := in_jJson;


if(lc_jJson.exist('groupFilterData')) then
    if(lc_jJson.get('groupFilterData').is_array) then
        
        
        lc_jlData := json_list(lc_jJson.get('groupFilterData'));
        
        
        if(lc_jlData.count>0) then
          if in_nIncludeWhere = 1 then
            lc_vWhere := ' where ';
          else
            lc_vWhere := ' ';
          end if;
          for rec in 1..lc_jlData.count loop
            lc_jvRec := lc_jlData.get(rec);
            lc_jValue := json(lc_jvRec);
            
            if (lc_jValue.exist('group_name')) then
                lc_jvValue := lc_jValue.get('group_name');
                lc_vGroupName := getVal(lc_jvValue); 
            end if;
            
            
            
            
            if (lc_jValue.exist('data')) then
                lc_jlDataValue := json_list(lc_jValue.get('data'));
                
                lc_vInClause := '' || lc_vGroupName || ' in (';
                for rec2 in 1..lc_jlDataValue.count loop
                  lc_jvValue := lc_jlDataValue.get(rec2);
                  
                  lc_vValue := getVal(lc_jvValue,1);
                  
                  if(rec2 = 1) then
                    lc_vInClause := lc_vInClause || lc_vValue;
                  else
                    lc_vInClause := lc_vInClause || ',' || lc_vValue;
                  end if;
                  
                end loop;
                lc_vInClause := lc_vInClause || ')';
                lc_vWhere := lc_vWhere || lc_vInClause;
            end if;
            
            
            if(rec<lc_jlData.count) then
              lc_vWhere := lc_vWhere || ' AND ';
            end if;
          end loop;
        else
          lc_vWhere := '';
        end if;
        
        
    end if;
end if;

  return lc_vWhere;
  
exception
    when json_error then
        --htp.p('raised json_error in update func');
        lc_jError := json();
        lc_jError.put('function','parseGroupFilterData');
        lc_jError.put('input',in_jJson.to_char);
        lc_jError.put('messages',lc_jlMessages);
        BB_P_ERROR(lc_jError);
        --htp.p('returning n');
        return 'e';
    when others then
        lc_jError := json();
        lc_jError.put('function','parseGroupFilterData');
        lc_jError.put('input',in_jJson.to_char);
        lc_jlMessages.append(json_list('[{error:''parseGroupFilterData.000X ' || SQLERRM || '''}]'));
        lc_jError.put('messages',lc_jlMessages);
        BB_P_ERROR(lc_jError);
        return 'e';
end parseGroupFilterData;
 

/
