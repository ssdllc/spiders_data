--------------------------------------------------------
--  DDL for Function SANITIZESTRING
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE FUNCTION "SPIDERS_DATA"."SANITIZESTRING" (str2 varchar2)
  RETURN varchar2
IS 
  str varchar2(2000);
BEGIN 
  str := str2;
  str := TRIM(str);
  str := REPLACE(str,'&','');
  str := REPLACE(str,'"','');
  str := REPLACE(str,'''','');
  
  RETURN str;
END;
 

/
