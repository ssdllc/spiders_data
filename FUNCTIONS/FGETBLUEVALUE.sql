--------------------------------------------------------
--  DDL for Function FGETBLUEVALUE
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE FUNCTION "SPIDERS_DATA"."FGETBLUEVALUE" (in_vColor varchar2) return number
as

lc_nReturn number;

begin

CASE upper(in_vColor)
  WHEN 'BLACK' then lc_nReturn := 0;
  WHEN 'BLUE' then lc_nReturn := 255;
  WHEN 'GRAY' then lc_nReturn := 128;
  WHEN 'GREEN' then lc_nReturn := 0;
  WHEN 'LIGHT BLUE' then lc_nReturn := 255;
  WHEN 'LIGHT GREY' then lc_nReturn := 192;
  WHEN 'LIME' then lc_nReturn := 0;
  WHEN 'MAGENTA' then lc_nReturn := 255;
  WHEN 'MAROON' then lc_nReturn := 0;
  WHEN 'NAVY' then lc_nReturn := 128;
  WHEN 'OLIVE' then lc_nReturn := 0;
  WHEN 'PURPLE' then lc_nReturn := 128;
  WHEN 'RED' then lc_nReturn := 0;
  WHEN 'TEAL' then lc_nReturn := 128;
  WHEN 'WHITE' then lc_nReturn := 255;
  WHEN 'YELLOW' then lc_nReturn := 0;
  ELSE lc_nReturn := round(dbms_random.value(1,255),0);
end case;

return lc_nReturn;

end;
 

/
