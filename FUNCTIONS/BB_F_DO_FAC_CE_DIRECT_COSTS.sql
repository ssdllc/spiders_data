--------------------------------------------------------
--  DDL for Function BB_F_DO_FAC_CE_DIRECT_COSTS
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE FUNCTION "SPIDERS_DATA"."BB_F_DO_FAC_CE_DIRECT_COSTS" (in_jJson json, o_jResults out json, o_jMessages out json ) RETURN VARCHAR2 AS



--JSON stuff
lc_jJson json;
lc_data json_list;
lc_rec_val json_value;
lc_rec json;
lc_jValue json;
lc_jvValue json_value;


--procedure specific stuff
lc_nSequence number;
lc_nContinue number :=0;
lc_nCountOfRecords number :=0;
lc_nADOID number;
lc_vNFAID varchar2(2000);
lc_nPRODUCTLINE_LISTID number;
lc_nWORKTYPE_LISTID number;
lc_vFACILITY_NAME varchar2(2000);
lc_vFACILITY_NO varchar2(2000);
lc_nDOFID number;
lc_vProcedureName varchar2(200) := 'BB_F_DO_FAC_CE_DIRECT_COSTS';

--DML stuff
lc_jlDML json_list;
lc_jResults json;
lc_jMessages json;
lc_vDMLResult varchar2(255);
lc_vResult varchar2(200);
lc_ret_str varchar(512);
lc_ret json;
thesql varchar2(2000) := '';
lc_vWhere varchar2(2000);
lc_vReturn varchar2(2000) :='default';

--error stuff
lc_vErrorText varchar2(2000);
lc_jError json;
lc_jlMessages json_list;
json_error EXCEPTION;




BEGIN
/*
stub data
*/

/*
lc_jResults := json();
lc_jlDML := json_list();
lc_jMessages := json();
lc_jlMessages := json_list;
o_jResults := json();
o_jMessages := json();

insert into debug (input,message) values ('here 1',lc_nContinue);

if(in_jJson.exist('PRODUCTLINE_LISTID')) then
  lc_jvValue := in_jJson.get('PRODUCTLINE_LISTID');
  lc_nPRODUCTLINE_LISTID := getVal(lc_jvValue);
else
  lc_vErrorText := 'PRODUCTLINE_LISTID doesnt exist';
  lc_nContinue :=0;
  lc_jlMessages.append('error: ''Error XMLIFFM-0001 ' || lc_vErrorText);
end if;
insert into debug (input,message) values ('lc_nPRODUCTLINE_LISTID',lc_nPRODUCTLINE_LISTID);

insert into debug (input,message) values ('here 2',to_char(lc_nContinue));
if(in_jJson.exist('WORKTYPE_LISTID')) then
  lc_jvValue := in_jJson.get('WORKTYPE_LISTID');
  lc_nWORKTYPE_LISTID := getVal(lc_jvValue);
else
  lc_vErrorText := 'WORKTYPE_LISTID doesnt exist';
  lc_nContinue :=0;
  lc_jlMessages.append('error: ''Error XMLIFFM-0001 ' || lc_vErrorText);
end if;

insert into debug (input,message) values ('lc_nWORKTYPE_LISTID',lc_nWORKTYPE_LISTID);

if(in_jJson.exist('DOFID')) then
  lc_jvValue := in_jJson.get('DOFID');
  lc_nDOFID := getVal(lc_jvValue);
else
  lc_vErrorText := 'DOFID doesnt exist';
  lc_nContinue :=0;
  lc_jlMessages.append('error: ''Error XMLIFFM-0001 ' || lc_vErrorText);
end if;

insert into debug (input,message) values ('lc_nDOFID',lc_nDOFID);

select count(*) into lc_nCountOfRecords from spd_t_do_facility_ce_dc where dofid = lc_nDOFID;
if lc_nCountOfRecords = 0 then
  insert into debug (input,message) values ('spd_t_do_facility_ce_dc',lc_nCountOfRecords);
  --Check if the productline listid is valid
  select count(*) into lc_nCountOfRecords from spd_t_lists where listname = 'list_product_lines' and listid = lc_nPRODUCTLINE_LISTID;
  insert into debug (input,message) values ('list_product_lines',lc_nCountOfRecords);
  if lc_nCountOfRecords =1  then
    --check if the worktype listid is good
    select count(*) into lc_nCountOfRecords from spd_t_lists where listname = 'list_facility_direct_cost_types' and productline_listid = lc_nPRODUCTLINE_LISTID and grouping = lc_nWORKTYPE_LISTID;
    if lc_nCountOfRecords > 0 then
      lc_nContinue := 1;
    else
      lc_vErrorText := 'Worktype List Id Invalid';
      lc_nContinue :=0;
      lc_jlMessages.append('error: ''error XMLIFFM-0001 ' || lc_vErrorText);
    end if;
  else
      lc_vErrorText := 'Productline Id Invalid';
      lc_nContinue :=0;
      lc_jlMessages.append('error: ''error XMLIFFM-0001 ' || lc_vErrorText);
  end if;
    
  insert into debug (input,message) values ('here 5',lc_nContinue);
  if lc_nContinue = 1 then
    --Add the records to the special insructions
    thesql := '
    insert into spd_t_do_facility_ce_dc (dofid, directcost_listid) 
    select 
      ' || lc_nDOFID || ', 
      listid  
    from 
      spd_t_lists 
    where 
      listname = ''list_facility_direct_cost_types'' 
      and productline_listid = ' || lc_nPRODUCTLINE_LISTID || '
      and grouping = ' || lc_nWORKTYPE_LISTID;
  
    lc_jlDML.append(thesql);
    lc_jResults.put('results',lc_jlDML);
    lc_vDMLResult := BB_F_DML(lc_jResults, lc_jMessages);
    lc_jMessages.put('id',lc_nSequence);
    o_jMessages := lc_jMessages;
  
  insert into debug (input,message) values ('here 6',lc_nContinue);
  end if;
else
  lc_vDMLResult := 'y';
  lc_vErrorText := 'Facility Cost Estimate exists';
  lc_nContinue :=0;
  lc_jlMessages.append('warning: ''warning XMLIFFM-0001 ' || lc_vErrorText);
end if;

insert into debug (input,message) values ('lc_jlMessages.count',lc_jlMessages.count);

lc_jMessages.put('messages',lc_jlMessages);

insert into debug (input,message) values ('here 8',json_printer.pretty_print(lc_jMessages));
--if there are any messages this is not valid return false
if (lc_jlMessages.count > 0) then

    lc_vReturn := 'n';
    lc_jMessages.put('status','error');
    raise json_error;
else
    lc_vReturn := 'y';
    lc_jMessages.put('status','success');
end if;

insert into debug (input,message) values ('here 8',lc_nContinue);

o_jMessages := lc_jMessages;
*/
o_jMessages := json();
o_jResults := json();
lc_vReturn := 'y';
return lc_vReturn;

exception
  when json_error then
      execute immediate 'insert into debug (functionname, input, message) values (''BB_F_DO_FAC_CE_DIRECT_COSTS'',''' || json_printer.pretty_print(in_jJson) || ''',''error: ' || sqlerrm || ''')';

    --lc_jError := json();
    --lc_jError.put('function','BB_F_DO_FAC_CE_DIRECT_COSTS');
    --lc_jError.put('input',json_printer.pretty_print(in_jJson));
    --lc_jError.put('messages',lc_jlMessages);
    --BB_P_ERROR(lc_jError);
    return 'e1';
    --htp.p('returning n');
  when others then
      execute immediate 'insert into debug (functionname, input, message) values (''BB_F_DO_FAC_CE_DIRECT_COSTS'',''' || json_printer.pretty_print(in_jJson) || ''',''error: ' || sqlerrm || ''')';
      return 'e2';

end;
 

/
