--------------------------------------------------------
--  DDL for Function GETTARGETFUNCTIONNAME
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE FUNCTION "SPIDERS_DATA"."GETTARGETFUNCTIONNAME" (in_vRest varchar2, in_vModule varchar2, in_vTarget varchar2) return varchar2 as 
  /* template variables */
  json_error EXCEPTION;
  spiders_error EXCEPTION;
  lc_vErrorDescription varchar2(255) := 'default';
  lc_vPrefix varchar2(255) := 'SPD_T_';
  lc_vFunctionName varchar2(255) := 'getTargetFunctionName';
  lc_vReturn varchar2(32000) := '{"error":"default"}'; --defaul the return to e for error
  lc_nContinue number :=0; --default to 0 to dissalow continuing
  
  /* function specific variables */
  lc_vName varchar2(2000);
  lc_vTemp varchar2(2000);
  lc_vComment varchar2(2000);
  
  /*EDITED 20170915 MR REMOVED DEBUG: [SPD3D][45] Sync*/
begin

  /* get name of object */
  --insert into debug (message) values ('select function_name from spd_t_rest_to_table_maps where upper(rest_endpoint) = upper(' || in_vRest || ') and upper(module) = upper(' || in_vModule || ') and upper(target) = upper(' || in_vTarget || ')');

  for rec in (select function_name from spd_t_rest_to_table_maps where upper(rest_endpoint) = upper(trim(in_vRest)) and upper(module) = upper(trim(in_vModule)) and upper(target) = upper(trim(in_vTarget))) loop  
    lc_vReturn := rec.function_name;
  end loop;
  
  return lc_vReturn;

exception     
  when spiders_error then
      execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' SPIDERS error ' || lc_vErrorDescription || ''')';
      return '{"error":"' || lc_vErrorDescription || '"}';
  when json_error then
      execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' JSON error'')';
      return '{"error":"json"}';
  when others then
      execute immediate 'insert into debug (message) values (''' || lc_vFunctionName || ' dml error: ' || sqlerrm || ''')';
      return '{"error":"' || sqlerrm || '"}';
end;
 

/
