--------------------------------------------------------
--  DDL for Function F_NEXT_SIA_STRUCTUREID
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE FUNCTION "SPIDERS_DATA"."F_NEXT_SIA_STRUCTUREID" (dummy CLOB) RETURN JSON AS
    l_rv NUMBER;
    lc_jResults json;
BEGIN
   execute immediate 'insert into debug (message) values ('' inside f_next_sia_structureid '')';
   SELECT SPD_S_SIASTRUCTUREID.nextval
     INTO l_rv
   FROM DUAL;
     
  lc_jResults := json();
  lc_jResults.put('results',l_rv); 
   
   RETURN lc_jResults;
END;
 

/
