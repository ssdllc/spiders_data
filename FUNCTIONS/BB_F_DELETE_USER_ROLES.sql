--------------------------------------------------------
--  DDL for Function BB_F_DELETE_USER_ROLES
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE FUNCTION "SPIDERS_DATA"."BB_F_DELETE_USER_ROLES" (in_jJson json) RETURN json AS

  /*
  in_jJson json:= json('{"target":"userRoles","data":[{"EMPLOYEEID":"larry@email.com","SECURITYGROUPID":"1","GROUP_NAME":"Administrators1","isMember":1}
                                                 ,{"EMPLOYEEID":"larry@email.com","SECURITYGROUPID":"2","GROUP_NAME":"Administrators2","isMember":0}
                                                 ,{"EMPLOYEEID":"larry@email.com","SECURITYGROUPID":"3","GROUP_NAME":"Administrators3","isMember":0}
                                                 ,{"EMPLOYEEID":"larry@email.com","SECURITYGROUPID":"4","GROUP_NAME":"Administrators4","isMember":1}
                                                ]}');
  */
  lc_jJson json;
  lc_data json_list;
  lc_rec_val json_value;
  lc_rec json;
  lc_employeeid_val json_value; --
  lc_employeeid varchar(256);
  lc_cnt number;

  lc_ret_str varchar(512);
  lc_ret json;

  lc_isMember_val json_value;
  lc_isMember number;

  lc_securitygroupid_val json_value;
  lc_securitygroupid varchar2(256);

BEGIN
  --POPLUTATE data ARRAY AND GET EMPLOYEEID OF FIRST ITEM
  if(in_jJson.exist('data')) then
    if(in_jJson.get('data').is_array) then
      lc_data := json_list(in_jJson.get('data'));
      lc_rec_val := lc_data.get(1); --return null on outofbounds
      if (lc_rec_val is not null) then
        lc_rec := json(lc_rec_val);
        if (lc_rec.exist('EMPLOYEEID')) then
          lc_employeeid_val := lc_rec.get('EMPLOYEEID');
          lc_employeeid := lc_employeeid_val.get_string;


          --CHECK IF EMPLOYEEID ALREADY EXISTS
            select count(SECURITYUSERID) into lc_cnt from SSD_SECURITY_USERS where EMPLOYEEID = lc_employeeid;

            if (lc_cnt > 0) then
              --EMPLOYEEID  EXISTS - REMOVE ALL
              DELETE FROM SSD_SECURITY_USERS WHERE EMPLOYEEID = lc_employeeid;
              commit;

              --CONFIRM THE EMPLOYEEID WAS SUCCESFULLY DELETED (Sodano's idea....not mine)
              select count(SECURITYUSERID) into lc_cnt from SSD_SECURITY_USERS where EMPLOYEEID = lc_employeeid;
            else
                lc_ret := json('{"error":"Error DUR.0006 There has been an error processing the request."}');
            end if;

            if (lc_data.count > 0 and lc_cnt = 0) then
                lc_ret := json('{"success":"User '||lc_employeeid||' has been deleted"}');
            else
                lc_ret := json('{"error":"Error DUR.0005 There has been an error processing the request."}');
            end if;
        else
            lc_ret := json('{"error":"Error DUR.0001 There has been an error processing the request."}');
        end if;
      else
        lc_ret := json('{"error":"Error DUR.0002 There has been an error processing the request."}');
      end if;
    else
        lc_ret := json('{"error":"Error DUR.0003 There has been an error processing the request."}');
    end if;
  else
    lc_ret := json('{"error":"Error DUR.0004 There has been an error processing the request."}');
  end if;



  return lc_ret;
END;
 

/
