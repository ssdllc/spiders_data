--------------------------------------------------------
--  DDL for Function BB_F_READ_DO_INFORMATION
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE FUNCTION "SPIDERS_DATA"."BB_F_READ_DO_INFORMATION" (in_jJson json) RETURN json AS

    lc_jJson json;
    lc_data json_list;
    lc_rec_val json_value;
    lc_rec json;
    lc_employeeid_val json_value; --
    lc_employeeid varchar(256);
    lc_cnt number;
    lc_nAdoid number :=0;
    lc_adoid_val json_value;

    lc_ret_str varchar(512);
    lc_ret json;

    thesql varchar2(2000) := '';

    lc_vWhere varchar2(2000);

    --error stuff
    lc_jError json;
    lc_jlMessages json_list;
    json_error EXCEPTION;


BEGIN
/*
stub data
{"module":"xmlImportManager","target":"deliveryOrdersWithXML","data":["ADOID":12345]}
*/

if(in_jJson.exist('data')) then
    if(in_jJson.get('data').is_array) then
        lc_data := json_list(in_jJson.get('data'));
        lc_rec_val := lc_data.get(1); --return null on outofbounds
        if (lc_rec_val is not null) then
            lc_rec := json(lc_rec_val);
            if (lc_rec.exist('ADOID')) then
                lc_adoid_val := lc_rec.get('ADOID');
                lc_nAdoid := lc_adoid_val.get_number;
            else
                lc_nAdoid :=0;
            end if;
        end if;
    end if;
end if;

if lc_nADOID > 0 then

    thesql := 'select
        do.ADOID,
        do.APID,
        decode(do.START_DATE,null,to_date(''01-JAN-1901'',''DD-MON-YYYY''),do.START_DATE) START_DATE,
        do.DELIVERY_ORDER_NAME,
        (select count(*) from spd_t_do_facilities where adoid = do.adoid) Facilities,
        xf.filename,
        ap.planned_year,
        ap.planned_qtr
    from
        spd_t_actn_dos do,
        spd_t_xml_files xf,
        spd_t_action_planner ap
    where
        ' || lc_vWhere || '
    order by
        ap.planned_year,
        ap.planned_qtr,
        delivery_order_name
    ';

else
    thesql := 'select '''' from dual';
end if;

    lc_jJson := json_dyn.executeObject(thesql);

    return lc_jJson;

exception
    when json_error then
        --htp.p('raised json_error in update func');
        lc_jError := json();
        lc_jError.put('function','BB_F_READ_DOS_WITH_XML_FILE');
        lc_jError.put('input',in_jJson.to_char);
        lc_jError.put('messages',lc_jlMessages);
        BB_P_ERROR(lc_jError);
        --htp.p('returning n');
        return json('e');
    when others then
        lc_jError := json();
        lc_jError.put('function','BB_F_READ_DOS_WITH_XML_FILE');
        lc_jError.put('input',in_jJson.to_char);
        lc_jlMessages.append(json_list('[{error:''RDOWX.000X ' || SQLERRM || '''}]'));
        lc_jError.put('messages',lc_jlMessages);
        BB_P_ERROR(lc_jError);
        return json('e');


END BB_F_READ_DO_INFORMATION;
 

/
