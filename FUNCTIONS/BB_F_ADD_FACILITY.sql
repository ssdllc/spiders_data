create or replace FUNCTION                "BB_F_ADD_FACILITY" (in_jJson json, o_jResults out json, o_jMessages out json ) RETURN VARCHAR2 AS



--JSON stuff
lc_jJson json;
lc_data json_list;
lc_rec_val json_value;
lc_rec json;
lc_jValue json;
lc_jvValue json_value;


--procedure specific stuff
lc_nSequence number;
lc_nContinue number :=1;
lc_nCountOfRecords number :=0;
lc_nADOID number;
lc_vNFAID varchar2(2000);
lc_nPRODUCTLINE_LISTID number;
lc_vFACILITY_NAME varchar2(2000);
lc_vFACILITY_NO varchar2(2000);
lc_nDOFID number;
lc_nACTIVITIESID number;

--DML stuff
lc_jlDML json_list;
lc_jResults json;
lc_jResultsSubProgram json;
lc_jMessages json;
lc_jMessagesSubProgram json;
lc_vDMLResult varchar2(255);
lc_vResult varchar2(200);
lc_vResultSubProgram varchar2(200);
lc_ret_str varchar(512);
lc_ret json;
thesql varchar2(2000) := '';
lc_vWhere varchar2(2000);
lc_vReturn varchar2(200);

--error stuff
lc_vErrorText varchar2(2000);
lc_jError json;
lc_jlMessages json_list;
json_error EXCEPTION;

procedure p (in_vInput varchar2, in_vMessage varchar2 default '') as

begin

insert into debug (functionname, input, message) values ('BB_F_ADD_FACILITY', in_vInput, in_vMessage);


end;

BEGIN
/*
stub data
*/

lc_jResults := json();
lc_jlDML := json_list();
lc_jMessages := json();
lc_jlMessages := json_list();
o_jResults := json();
o_jMessages := json();


if(in_jJson.exist('NFAID')) then
  lc_jvValue := in_jJson.get('NFAID');
  lc_vNFAID := getVal(lc_jvValue);
else
  lc_vErrorText := 'NFAID doesnt exist';
  lc_nContinue :=0;
  lc_jlMessages.append('error: ''Error XMLIFFM-0001 ' || lc_vErrorText);
end if;

p('lc_vNFAID',lc_vNFAID);    

if(in_jJson.exist('PRODUCTLINE_LISTID')) then
  lc_jvValue := in_jJson.get('PRODUCTLINE_LISTID');
  lc_nPRODUCTLINE_LISTID := getVal(lc_jvValue);
  
  --check to see if the productline is valid
  select count(*) into lc_nCountOfRecords from spd_t_lists where listname = 'list_product_lines' and listid = lc_nPRODUCTLINE_LISTID;
  p('select count(*) into lc_nCountOfRecords from spd_t_lists where listname = ''list_product_lines'' and listid =  ' || lc_nPRODUCTLINE_LISTID);
  if lc_nCountOfRecords = 1 then
    --Productline listid is valid
    lc_nContinue :=1;
  else
    lc_vErrorText := 'PRODUCTLINE_LISTID invalid value';
    lc_nContinue :=0;
    lc_jlMessages.append('error: ''Error XMLIFFM-0001 ' || lc_vErrorText);
  end if;
  
else
  lc_vErrorText := 'PRODUCTLINE_LISTID doesnt exist';
  lc_nContinue :=0;
  lc_jlMessages.append('error: ''Error XMLIFFM-0001 ' || lc_vErrorText);
end if;

p('lc_nPRODUCTLINE_LISTID',lc_nPRODUCTLINE_LISTID);  

if(in_jJson.exist('FACILITY_NAME')) then
  lc_jvValue := in_jJson.get('FACILITY_NAME');
  lc_vFACILITY_NAME := getVal(lc_jvValue);
else
  lc_vErrorText := 'FACILITY_NAME doesnt exist';
  lc_nContinue :=0;
  lc_jlMessages.append('error: ''Error XMLIFFM-0001 ' || lc_vErrorText);
end if;

p('lc_vFACILITY_NAME',lc_vFACILITY_NAME);  


if(in_jJson.exist('FACILITY_NO')) then
  lc_jvValue := in_jJson.get('FACILITY_NO');
  lc_vFACILITY_NO := getVal(lc_jvValue);
else
  lc_vErrorText := 'FACILITY_NO doesnt exist';
  lc_nContinue :=0;
  lc_jlMessages.append('error: ''Error XMLIFFM-0001 ' || lc_vErrorText);
end if;

p('lc_vFACILITY_NO',lc_vFACILITY_NO);  


  
if lc_nContinue = 1 then

  select count(*) into lc_nCountOfRecords from spd_t_facilities where infads_facility_id = lc_vNFAID;
  p('select count(*) into lc_nCountOfRecords from spd_t_facilities where infads_facility_id =' || lc_vNFAID,lc_nCountOfRecords);
  if lc_nCountOfRecords = 0 then
    --this facility does not exist in spd_t_facilities
    select count(*) into lc_nCountOfRecords from spd_mv_inf_facility where facility_id = lc_vNFAID;
    p('select count(*) into lc_nCountOfRecords from spd_mv_inf_facility where facility_id =' || lc_vNFAID,lc_nCountOfRecords);
    
    
    if lc_nCountOfRecords = 1 then
      --this NFA exists in the infads tables, use the values from INFADS for the facility name, no and then get the UIC
      select spd_s_facilities.nextval into lc_nSequence from dual;
      --Insert record into table
      thesql := '
      insert into spd_t_facilities 
      (
        FACID,
        infads_facility_id, 
        product_line_listid, 
        spd_fac_name, 
        spd_fac_no
      ) 
        select 
          ' || lc_nSequence || ',
          ''' || lc_vNFAID || ''',
          ' || lc_nPRODUCTLINE_LISTID || ',
          facility_name, 
          facility_no 
        from 
          spd_mv_inf_facility 
        where 
          facility_id = ''' || lc_vNFAID || '''';
  
    end if;
    
    
    if lc_nCountOfRecords = 0 then
      --this NFA does NOT exist in the infads tables, use the values from the JSON for the facility name, no and then Assign a UIC of WASHINGTON NAVY YARD, or PT HEUNEME/PT MUGU
      select spd_s_facilities.nextval into lc_nSequence from dual;
      --Insert record into table
      thesql := '
      insert into spd_t_facilities 
      (
        FACID,
        infads_facility_id, 
        product_line_listid, 
        spd_fac_name, 
        spd_fac_no
      ) 
      values
      (
        ''' || lc_nSequence || ''',
        ''' || lc_vNFAID || ''',
        ' || lc_nPRODUCTLINE_LISTID || ',
        ''' || lc_vFACILITY_NAME || ''', 
        ''' || lc_vFACILITY_NO || '''
      )';
    
    end if;
  
    p('thesql',thesql);  


    lc_jlDML.append(thesql);
    lc_jResults.put('results',lc_jlDML);

  
  else
    lc_vErrorText := 'Facility already exists in spd_t_facilities table';
    lc_nContinue :=0;
    lc_jlMessages.append('error: ''Error XMLIFFM-0001 ' || lc_vErrorText);
  end if;

end if;

lc_jMessages.put('messages',lc_jlMessages);


--if there are any messages this is not valid return false
if (lc_jlMessages.count > 0) then
    lc_vReturn := 'n';
    lc_jMessages.put('status','error');
    --raise json_error;
    p('lc_jlMessages',lc_jlMessages.count);
    p('lc_vReturn',lc_vReturn);
else

    p('running BB_F_DML',0);
    lc_vDMLResult := BB_F_DML(lc_jResults, lc_jMessages);
    p('lc_vDMLResult',lc_vDMLResult);
    
    if lc_vDMLResult = 'y' then
      
      
      lc_jMessages.put('id',lc_nSequence);
      o_jMessages := lc_jMessages;
      o_jResults.put('FACID',lc_nSequence);
    
      --run sub program for activities
      --this facility was added, we must now check to see if we need to make a new activity record
      lc_vResultSubProgram := BB_F_ADD_ACTIVITY(in_jJson, lc_jResultsSubProgram, lc_jMessagesSubProgram);
      
      p('lc_vResultSubProgram',lc_vResultSubProgram);
      
      if(lc_jResultsSubProgram.exist('ACTIVITIESID')) then
        lc_vReturn := 'y';
        lc_jvValue := lc_jResultsSubProgram.get('ACTIVITIESID');
        lc_nACTIVITIESID := getVal(lc_jvValue);
      else
        lc_vErrorText := 'ACTIVITIESID doesnt exist';
        lc_nACTIVITIESID := 0;
        lc_nContinue :=0;
        lc_jlMessages.append('error: ''Error XMLIFFM-0001 ' || lc_vErrorText);
      end if;

      o_jResults.put('ACTIVITIESID',lc_nACTIVITIESID);

    else
      lc_vReturn := 'n';
      lc_jMessages.put('status','error: DML did not complete as expected');
      p('lc_vReturn',lc_vReturn);    
    end if;
    
end if;

  o_jMessages := lc_jMessages;
  p('lc_vReturn2',lc_vReturn);

return lc_vReturn;

exception
when json_error then
    --htp.p('raised json_error in update func');
    lc_jError := json();
    lc_jError.put('function','BB_F_ADD_FACILITY');
    lc_jError.put('input',json_printer.pretty_print(in_jJson));
    lc_jError.put('messages',lc_jlMessages);
    BB_P_ERROR(lc_jError);
    return 'e';
    --htp.p('returning n');
  when others then
      execute immediate 'insert into debug (functionname, input, message) values (''BB_F_ADD_FACILITY'',''' || json_printer.pretty_print(in_jJson) || ''',''error: ' || sqlerrm || ''')';
      return 'e';

end;
 