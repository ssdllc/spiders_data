--------------------------------------------------------
--  DDL for Function BB_F_DELIVERY_ORDER_ADD_FAC
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE FUNCTION "SPIDERS_DATA"."BB_F_DELIVERY_ORDER_ADD_FAC" (in_jJson json, o_jResults out json, o_jMessages out json ) RETURN VARCHAR2 AS


--JSON stuff
lc_jJson json;
lc_data json_list;
lc_rec_val json_value;
lc_rec json;
lc_jValue json;
lc_jvValue json_value;


--procedure specific stuff
lc_nSequence number;
lc_nContinue number :=0;
lc_nCountOfRecords number :=0;
lc_nADOID number;
lc_nFACID number;
lc_vNFAID varchar2(2000);
lc_nPRODUCTLINE_LISTID number;
lc_nWORKTYPE_LISTID number;
lc_vFACILITY_NAME varchar2(2000);
lc_vFACILITY_NO varchar2(2000);
lc_nDOFID number;
lc_nDeliveryOrderID number;

--DML stuff
lc_jlDML json_list;
lc_jResults json;
lc_jMessages json;
lc_vDMLResult varchar2(255);
lc_vResult varchar2(200);
lc_ret_str varchar(512);
lc_ret json;
thesql varchar2(2000) := '';
lc_vWhere varchar2(2000);
lc_vReturn varchar2(200);

--error stuff
lc_vErrorText varchar2(2000);
lc_jError json;
lc_jlMessages json_list;
json_error EXCEPTION;

procedure p (in_vInput varchar2, in_vMessage varchar2 default '') as

begin

insert into debug (input, message) values (in_vInput, in_vMessage);

end;

BEGIN
/*
stub data
*/

lc_jResults := json();
lc_jlDML := json_list();
lc_jMessages := json();
lc_jlMessages := json_list();
o_jResults := json();
o_jMessages := json();

if(in_jJson.exist('ADOID')) then
  lc_jvValue := in_jJson.get('ADOID');
  lc_nADOID := getVal(lc_jvValue);
else
  lc_vErrorText := 'ADOID doesnt exist';
  lc_nContinue :=0;
  lc_jlMessages.append('error: ''Error XMLIFFM-0001 ' || lc_vErrorText);
end if;

p('lc_nADOID',lc_nADOID);



if(in_jJson.exist('FACID')) then
  lc_jvValue := in_jJson.get('FACID');
  lc_nFACID := getVal(lc_jvValue);
else
  lc_vErrorText := 'FACID doesnt exist';
  lc_nContinue :=0;
  lc_jlMessages.append('error: ''Error XMLIFFM-0001 ' || lc_vErrorText);
end if;
p('lc_nFACID',lc_nFACID);


--check to see if the adoid is valid
select count(*) into lc_nCountOfRecords from spd_t_actn_dos where adoid = lc_nADOID;

if lc_nCountOfRecords = 1 then


/*!!super sloppy, but we need to get the DELIVERYORDERID from the ADOID. The trigger then figures out the ADOID from the Deliveryorderid*/
for rec1 in (select deliveryorderid from spd_t_delivery_orders where adoid = lc_nADOID) loop
  lc_nDeliveryOrderID := rec1.deliveryorderid;
end loop;

/*What happens if this adoid doesnt have a new delivrey order associated?*/
if lc_nDeliveryOrderID = 0 then
  null; --for now;
end if;

  select spd_s_do_facilities.nextval into lc_nSequence from dual;
  /*!!NEW VERSION, This one makes a record in SPD_T_DELIVERY_ORDER_FACS which has a trigger to make the legacy SPD_T_DO_FACILITIES record*/
    thesql := '
    insert into
      spd_t_delivery_order_facs
        (
          dofid,
          deliveryorderid,
          facid
        )
    values
      (
          ' || lc_nSequence || ', 
          ' || lc_nDeliveryOrderID || ',
          ' || lc_nFACID || '        
        )';


  --check to see if the facid is valid
  select count(*) into lc_nCountOfRecords from spd_t_facilities where facid = lc_nFACID;
  if lc_nCountOfRecords = 1 then

    --Add the records to the special insructions
/*legacy code, removing
  select spd_s_do_facilities.nextval into lc_nSequence from dual;
    thesql := '
    insert into 
        spd_t_do_facilities 
          (
            dofid, 
            adoid, 
            facid
          ) 
      values 
        (
          ' || lc_nSequence || ', 
          ' || lc_nADOID || ',
          ' || lc_nFACID || '        
        )';
 */       
        
    lc_jlDML.append(thesql);
    lc_jResults.put('results',lc_jlDML);
    lc_vDMLResult := BB_F_DML(lc_jResults, lc_jMessages);
    lc_jMessages.put('id',lc_nSequence);
    
  
    o_jResults.put('DOFID',lc_nSequence);
    
    p('lc_vDMLResult2',lc_vDMLResult);

  else
    lc_vErrorText := 'FACID is not valid';
    lc_nContinue :=0;
    lc_jlMessages.append('error: ''Error XMLIFFM-0001 ' || lc_vErrorText);
  end if;
else
  lc_vErrorText := 'ADOID does not exist in Delivery Orders table';
  lc_nContinue :=0;
  lc_jlMessages.append('error: ''Error XMLIFFM-0001 ' || lc_vErrorText);
end if;



lc_jMessages.put('messages',lc_jlMessages);


--if there are any messages this is not valid return false
if (lc_jlMessages.count > 0) then
    lc_vReturn := 'n';
    lc_jMessages.put('status','error');
    --raise json_error;
    p('lc_jlMessages',lc_jlMessages.count);
    p('lc_vReturn',lc_vReturn);
else
    lc_vReturn := 'y';
    lc_jMessages.put('status','success');
    p('lc_vReturn',lc_vReturn);
end if;

    o_jMessages := lc_jMessages;
p('lc_vReturn',lc_vReturn);

return lc_vReturn;

exception
when json_error then
    --htp.p('raised json_error in update func');
    lc_jError := json();
    lc_jError.put('function','BB_F_DELIVERY_ORDER_ADD_FAC');
    lc_jError.put('input',json_printer.pretty_print(in_jJson));
    lc_jError.put('messages',lc_jlMessages);
    BB_P_ERROR(lc_jError);
    return 'n1';
    --htp.p('returning n');
  when others then
      execute immediate 'insert into debug (functionname, input, message) values (''BB_F_DELIVERY_ORDER_ADD_FAC'',''' || json_printer.pretty_print(in_jJson) || ''',''error: ' || sqlerrm || ''')';
      return 'e1';


end;
 

/
