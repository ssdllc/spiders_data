--------------------------------------------------------
--  DDL for Function F_UPDATE_SIA_DOFID
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE FUNCTION "SPIDERS_DATA"."F_UPDATE_SIA_DOFID" 
(in_nStrId IN NUMBER, in_nDofid IN NUMBER DEFAULT 0 ) RETURN JSON AS 
  lc_jResults json;
BEGIN
  execute immediate 'insert into debug (message) values ('' inside f_update_sia_dofid '')';
  
  execute immediate 'update spd_t_sia_data set dofid=''' || in_nDofid || '''where siastructureid= ''' || in_nStrId || '''';
  
  lc_jResults := json();
  lc_jResults.put('results',1); 

END F_UPDATE_SIA_DOFID;
 

/
