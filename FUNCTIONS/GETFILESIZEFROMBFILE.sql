--------------------------------------------------------
--  DDL for Function GETFILESIZEFROMBFILE
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE FUNCTION "SPIDERS_DATA"."GETFILESIZEFROMBFILE" (in_bfFile BFILE) RETURN VARCHAR2 is
    lc_nFileSize number;
    lc_vReturn varchar2(2000);
BEGIN
    IF in_bfFile is NULL THEN
      RETURN 0;
    ELSE
      lc_nFileSize := DBMS_LOB.GETLENGTH (in_bfFile);
      if lc_nFileSize > 1048576 then
        lc_nFileSize := lc_nFileSize/1048576;
        lc_nFileSize := round(lc_nFileSize);
        lc_vReturn := lc_nFileSize || ' MB';
      else
        lc_nFileSize := lc_nFileSize/1024;
        lc_nFileSize := round(lc_nFileSize);
        lc_vReturn := lc_nFileSize || ' KB';
      end if;
      
      
      
      RETURN lc_vReturn;
    END IF;
    
exception
    when others then
        return 'N/A';
END;
 

/
