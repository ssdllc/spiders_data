--------------------------------------------------------
--  DDL for Function BB_F_R_REPORT_LIBRARY_DD_STATE
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE FUNCTION "SPIDERS_DATA"."BB_F_R_REPORT_LIBRARY_DD_STATE" (in_jJson json) RETURN json AS

  lc_jJson json;

  thesql varchar2(2000) := '';

  --error stuff
  lc_jError json;
  lc_jlMessages json_list;
  json_error EXCEPTION;

BEGIN
/*
stub data
{"module":"reportLibrary","target":"reportFiles","data":[{"REPORTID":11042}]}
*/

thesql := '

select
  min(listname) listname,
  displayvalue,
  min(returnvalue) returnvalue,
  min(visualorder) visualorder
from 
(
  select
    ''STATES'' listname,
    STATE displayvalue,
    STATE returnvalue,
    1 visualorder
  from
    spd_mv_inf_cur_activities
  where
    country = ''UNITED STATES''
  union
  select
    ''STATES'' listname,
    COUNTRY displayvalue,
    COUNTRY returnvalue,
    2 visualorder
  from
    spd_mv_inf_cur_activities
  where
    state = ''NON-US''
)
group by 
  visualorder, displayvalue
order by
  visualorder, displayvalue

';

  lc_jJson := json_dyn.executeObject(thesql);

  return lc_jJson;

exception
    when json_error then
        --htp.p('raised json_error in update func');
        lc_jlMessages := json_list();
        lc_jError := json();
        lc_jError.put('function','BB_F_R_REPORT_LIBRARY_DD_STATE');
        lc_jError.put('input',in_jJson.to_char);
        lc_jError.put('messages',lc_jlMessages);
        BB_P_ERROR(lc_jError);
        --htp.p('returning n');
        return json('e');
    when others then
        lc_jError := json();
        lc_jError.put('function','BB_F_R_REPORT_LIBRARY_DD_STATE');
        lc_jError.put('input',in_jJson.to_char);
        lc_jlMessages.append(json_list('[{error:''RDRLA.000X ' || SQLERRM || '''}]'));
        lc_jError.put('messages',lc_jlMessages);
        BB_P_ERROR(lc_jError);
        return json('e');


END BB_F_R_REPORT_LIBRARY_DD_STATE;
 

/
