--------------------------------------------------------
--  DDL for Function BB_F_D_REPORT_LIBRARY_KEYWORD
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE FUNCTION "SPIDERS_DATA"."BB_F_D_REPORT_LIBRARY_KEYWORD" (in_jJson json, o_jMessages out json) return varchar2 as 

    lc_jJson json;
    lc_data json_list;
    lc_rec_val json_value;
    lc_rec json;
   
    lc_jValue json;
    lc_jvValue json_value;
    
    lc_nID number;
    lc_vTEXT varchar2(255);
    
    lc_nSequence number;
    lc_nContinue number :=0;
    lc_nCountOfRecords number :=0;
    
    
    lc_jlDML json_list;
    lc_jResults json;
    lc_jMessages json;
    lc_vDMLResult varchar2(255);
    lc_ret_str varchar(512);
    lc_ret json;

    thesql varchar2(2000) := '';

    lc_vWhere varchar2(2000);

    --error stuff
    lc_jError json;
    lc_jlMessages json_list;
    json_error EXCEPTION;


BEGIN
/*
stub data
{"module":"xmlImportManager","target":"itDoesntMatterNotBeingCalledFromTheBrowser","data":["ADOID":12345]}
*/

lc_jResults := json();
lc_jlDML := json_list();

if(in_jJson.exist('data')) then
    if(in_jJson.get('data').is_array) then
        lc_data := json_list(in_jJson.get('data'));        
        lc_rec_val := lc_data.get(1); --return null on outofbounds

        if (lc_rec_val is not null) then
        
            lc_jValue := json(lc_rec_val);
            
            if (lc_jValue.exist('REPORTKEYWORDID')) then
                lc_jvValue := lc_jValue.get('REPORTKEYWORDID');
                lc_nID := getVal(lc_jvValue);
            end if;

            lc_nContinue := 1;
            
        end if;
    end if;
end if;




if lc_nContinue = 1 then

  if lc_nID > 0 then
  
      thesql := 'delete from spd_t_report_keywords where reportkeywordid = ' || lc_nID;

      lc_jlDML.append(thesql);
      lc_jResults.put('results',lc_jlDML);
      lc_vDMLResult := BB_F_DML(lc_jResults, lc_jMessages);
      lc_jMessages.put('status','success');
      o_jMessages := lc_jMessages;
  
  end if;
  
  
  return lc_vDMLResult;
else 
  raise json_error;
end if;


exception
when json_error then
    --htp.p('raised json_error in update func');
    lc_jError := json();
    lc_jError.put('function','BB_F_D_REPORT_LIBRARY_KEYWORD');
    lc_jError.put('input',in_jJson.to_char);
    lc_jError.put('messages',lc_jlMessages);
    BB_P_ERROR(lc_jError);
    return 'e';
    --htp.p('returning n');
when others then
    lc_jError := json();
    lc_jlMessages := json_list();
    lc_jError.put('function','BB_F_D_REPORT_LIBRARY_KEYWORD');
    lc_jError.put('input','');
    lc_jlMessages.append(json_list('[{error:''DRLKW.000X ' || SQLERRM || '''}]'));
    lc_jError.put('messages',lc_jlMessages);
    BB_P_ERROR(lc_jError);
    return 'e';


END BB_F_D_REPORT_LIBRARY_KEYWORD;
 

/
