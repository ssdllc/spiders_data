--------------------------------------------------------
--  DDL for Function BB_F_READ_XML_FACILITIES
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE FUNCTION "SPIDERS_DATA"."BB_F_READ_XML_FACILITIES" (in_jJson json) RETURN json AS

    lc_jJson json;
    lc_data json_list;
    lc_rec_val json_value;
    lc_rec json;
    lc_adoid_val json_value; --
    lc_nAdoid number;

    lc_ret_str varchar(512);
    lc_ret json;

    thesql varchar2(2000) := '';


    lc_jMessages json;
    lc_jlMessages json_list;
    lc_bReturn varchar2(1);
    
    --error stuff
    lc_jError json;
    json_error EXCEPTION;
    

BEGIN
/*
stub data
{"module":"xmlImportManager","target":"xmlFacilities","data":[{"ADOID":1}]}
*/

    if(in_jJson.exist('data')) then
        if(in_jJson.get('data').is_array) then
            lc_data := json_list(in_jJson.get('data'));
            dbms_output.put_line(lc_data.count);
            lc_rec_val := lc_data.get(1); --return null on outofbounds
            if (lc_rec_val is not null) then
                lc_rec := json(lc_rec_val);
                if (lc_rec.exist('ADOID')) then
                    lc_adoid_val := lc_rec.get('ADOID');
                    lc_nAdoid := lc_adoid_val.get_number;
                else
                    lc_jlMessages.append(json_list('[{error:''Error RXF-0001 There has been an error processing the request''}]'));
                end if;
            else
                lc_jlMessages.append(json_list('[{error:''Error RXF-0002 There has been an error processing the request''}]'));
            end if;
        else
            lc_jlMessages.append(json_list('[{error:''Error RXF-0003 There has been an error processing the request''}]'));
        end if;
    else
        lc_jlMessages.append(json_list('[{error:''Error RXF-0004 There has been an error processing the request''}]'));
    end if;

thesql := 'select
    XMLFACILITYMATCHID,
    XML_FACILITY_NAME,
    XML_NFAID ,
    XML_FACILITY_NUMBER,
    XML_SPIDERS_DOFID,
    decode(DOFID,null,0,DOFID) DOFID,
    (select
          fac.spd_fac_name
      from
          spd_t_do_facilities dofac,
          spd_t_facilities fac
      where
          dofac.facid = fac.facid
          and dofac.dofid = xmlfac.dofid
    ) matched_fac_name,
    (select
          fac.spd_fac_no
      from
          spd_t_do_facilities dofac,
          spd_t_facilities fac
      where
          dofac.facid = fac.facid
          and dofac.dofid = xmlfac.dofid
    ) matched_fac_no,
    (select
          fac.infads_facility_id
      from
          spd_t_do_facilities dofac,
          spd_t_facilities fac
      where
          dofac.facid = fac.facid
          and dofac.dofid = xmlfac.dofid
    ) matched_NFAID
from
    spd_t_xml_facilities_match xmlfac
where
    adoid = ' || lc_nAdoid || '
order by
    XML_NFAID
';

  lc_jJson := json_dyn.executeObject(thesql);

  return lc_jJson;

 exception
    when json_error then
        --htp.p('raised json_error in update func');
        lc_jError := json();
        lc_jError.put('function','bb_f_val_update_user_roles');
        lc_jError.put('input',in_jJson.to_char);
        lc_jError.put('messages',lc_jlMessages);
        BB_P_ERROR(lc_jError);
        --htp.p('returning n');
        return json('e');
    when others then
        lc_jError := json();
        lc_jError.put('function','BB_F_READ_XML_FACILITIES');
        lc_jError.put('input',in_jJson.to_char);
        lc_jlMessages.append(json_list('[{error:''RXF.000X ' || SQLERRM || '''}]'));
        lc_jError.put('messages',lc_jlMessages);
        BB_P_ERROR(lc_jError);
        return json('e');

END BB_F_READ_XML_FACILITIES;
 

/
