--------------------------------------------------------
--  DDL for Function BB_F_TEST_DO_XML_IMPORT_ERRORS
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE FUNCTION "SPIDERS_DATA"."BB_F_TEST_DO_XML_IMPORT_ERRORS" (in_jJson json) RETURN json AS

  lc_jJson json;
  lc_data json_list;
  lc_rec_val json_value;
  lc_rec json;
  lc_adoid_val json_value; --
  lc_nAdoid number;

  lc_ret_str varchar(512);
  lc_ret json;

  thesql varchar2(2000) := '';

  --error stuff
  lc_jError json;
  lc_jlMessages json_list;
  json_error EXCEPTION;
  
  lc_jlAoData json_list;
  lc_vSearch varchar2(2000);
  lc_jvSearch json_value;
  lc_jValue json;
  lc_jvValue json_value;
  
  lc_jValue2 json;
  lc_jvValue2 json_value;
  lc_jvValue3 json_value;
  lc_jvValue4 json_value;
  
  lc_jValue3 json;
  
  lc_jlValue2 json_list;
  lc_jlValue4 json_list;


  lc_vValue varchar2(2000);
  lc_vWhere varchar2(2000);
  lc_nStart number := 1;
  lc_nEnd number := 10;
  
  lc_nOrderByCol number;
  lc_vOrderByCol varchar2(2000);
  lc_vOrderBySort varchar2(2000);
  lc_vOrderBy varchar2(2000) := ' time asc ';
  
  lc_nGetValuesFromAodata number;
  

BEGIN
/*
stub data
{"module":"xmlImportManager","target":"spidersDeliveryOrderFacs","data":[{"ADOID":330001385}]}
*/

if(in_jJson.exist('data')) then
    if(in_jJson.get('data').is_array) then
        lc_data := json_list(in_jJson.get('data'));
        lc_rec_val := lc_data.get(1); --return null on outofbounds
        if (lc_rec_val is not null) then
            lc_rec := json(lc_rec_val);
            if (lc_rec.exist('ADOID')) then
                lc_adoid_val := lc_rec.get('ADOID');
                lc_nAdoid := lc_adoid_val.get_number;
            else
                lc_jlMessages.append(json_list('[{error:''Error RDOXIE-0001 There has been an error processing the request''}]'));
            end if;
        else
            lc_jlMessages.append(json_list('[{error:''Error RDOXIE-0002 There has been an error processing the request''}]'));
        end if;
    else
        lc_jlMessages.append(json_list('[{error:''Error RDOXIE-0003 There has been an error processing the request''}]'));
    end if;
else
    lc_jlMessages.append(json_list('[{error:''Error RDOXIE-0004 There has been an error processing the request''}]'));
end if;

lc_vWhere := parseGroupFilterData(in_jJson);

lc_nGetValuesFromAodata := parseAoData(in_jJson, lc_vSearch, lc_nStart, lc_nEnd, lc_vOrderBy);

if length(lc_vSearch) is null then
  lc_vWhere := '';
else
  lc_vWhere := lc_vWhere || ' AND lower(functionname || input || message || username || messagetype || module || target || debugid) like ''%' || lc_vSearch || '%''';
end if;

thesql := '

select * from (
select
    time,
    column1,
    functionname,
    input,
    message,
    username,
    messagetype,
    debugid,
    module,
    target,
    ref_id,
    count(*) over (partition by column1) countOfRecords,
    ROW_NUMBER() OVER (ORDER BY time asc) rn
from
    debug
where
    ref_id = ' || lc_nAdoid || '
    ' || lc_vWhere || '
order by
    ' || lc_vOrderBy || '
)
where rn between ' || lc_nStart || ' and ' || lc_nEnd || '

    
    
';


  lc_jJson := json_dyn.executeObject(thesql);

  return lc_jJson;

exception
    when json_error then
        --htp.p('raised json_error in update func');
        lc_jError := json();
        lc_jError.put('function','BB_F_READ_DO_XML_IMPORT_ERRORS');
        lc_jError.put('input',in_jJson.to_char);
        lc_jError.put('messages',lc_jlMessages);
        BB_P_ERROR(lc_jError);
        --htp.p('returning n');
        return json('e');
    when others then
        lc_jError := json();
        lc_jError.put('function','BB_F_READ_DO_XML_IMPORT_ERRORS');
        lc_jError.put('input',in_jJson.to_char);
        lc_jlMessages.append(json_list('[{error:''RDOXIE.000X ' || SQLERRM || '''}]'));
        lc_jError.put('messages',lc_jlMessages);
        BB_P_ERROR(lc_jError);
        return json('e');


END BB_F_TEST_DO_XML_IMPORT_ERRORS;
 

/
