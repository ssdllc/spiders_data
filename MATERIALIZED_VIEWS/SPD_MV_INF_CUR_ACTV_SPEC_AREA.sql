--------------------------------------------------------
--  DDL for Materialized View SPD_MV_INF_CUR_ACTV_SPEC_AREA
--------------------------------------------------------

  CREATE MATERIALIZED VIEW "SPIDERS_DATA"."SPD_MV_INF_CUR_ACTV_SPEC_AREA" ("UIC", "SPECIAL_AREA_CODE", "SPECIAL_AREA_NAME", "LATITUDE", "LONGITUDE", "CITY_CODE", "STATE_CODE", "COUNTRY_CODE")
  ORGANIZATION HEAP PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 
 NOCOMPRESS LOGGING
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1
  BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "SPIDERS_DATA_TABLES" 
  BUILD IMMEDIATE
  USING INDEX 
  REFRESH COMPLETE ON DEMAND
  USING DEFAULT LOCAL ROLLBACK SEGMENT
  USING ENFORCED CONSTRAINTS DISABLE QUERY REWRITE
  AS SELECT uic,
    special_area_code,
    special_area_name,
    latitude,
    longitude,
    city_code,
    state_code,
    country_code
  FROM spd_mv_inf_special_area sa
  UNION
  SELECT uic,
    NULL,
    activity_title,
    latitude,
    longitude,
    city_code,
    state_code,
    country_code
  FROM spd_mv_inf_cur_activities;

   COMMENT ON MATERIALIZED VIEW "SPIDERS_DATA"."SPD_MV_INF_CUR_ACTV_SPEC_AREA"  IS 'snapshot table for snapshot SPIDERS_DATA.SPD_MV_INF_CUR_ACTV_SPEC_AREA';
