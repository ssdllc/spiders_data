--------------------------------------------------------
--  DDL for Type T_ROW_NUMBER
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE TYPE "SPIDERS_DATA"."T_ROW_NUMBER" AS TABLE OF NUMBER(3);

/
