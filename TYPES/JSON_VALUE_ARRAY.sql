--------------------------------------------------------
--  DDL for Type JSON_VALUE_ARRAY
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE TYPE "SPIDERS_DATA"."JSON_VALUE_ARRAY" as table of json_value;

/
