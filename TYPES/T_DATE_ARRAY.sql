--------------------------------------------------------
--  DDL for Type T_DATE_ARRAY
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE TYPE "SPIDERS_DATA"."T_DATE_ARRAY" as table of date;

/
