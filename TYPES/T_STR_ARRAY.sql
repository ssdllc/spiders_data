--------------------------------------------------------
--  DDL for Type T_STR_ARRAY
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE TYPE "SPIDERS_DATA"."T_STR_ARRAY" as table of varchar2(32000);

/
