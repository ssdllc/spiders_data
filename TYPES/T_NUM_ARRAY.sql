--------------------------------------------------------
--  DDL for Type T_NUM_ARRAY
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE TYPE "SPIDERS_DATA"."T_NUM_ARRAY" as table of number;

/
